from common.imports import opcodes
from common.objects.base_classes.flow import BaseFlow
from common.imports.types import ActionCommands, PointofControlCommands
from common.objects.messages.programming.point_of_control_messages import PointOfControlMessages
from common.objects.statuses.programming.point_of_control_statuses import PointOfControlStatuses
import warnings
from decimal import *
__author__ = 'baseline'


########################################################################################################################
# Base POC class
########################################################################################################################
class BasePOC(BaseFlow):
    """
    3200 Point of Control Object \n
    """

    # ----- Class Variables ------ #
    special_characters = "!@#$%^&*()_-{}[]|\:;\"'<,>.?/"

    mainline_objects = None

    #################################

    def __init__(self,
                 _controller,
                 _ad
                 ):
        """
        3200 Point of Control Constructor. \n

        :param _controller: Controller this object is "attached" to \n
        :type _controller:  common.objects.controllers.bl_32.BaseStation3200 |
                            common.objects.controllers.bl_10.BaseStation1000 \n

        :param _ad:     Address (Point of Control number) \n
        :type _ad:      int \n
        """
        BaseFlow.__init__(self, _controller=_controller, _ad=_ad, _identifiers=[[opcodes.point_of_control, _ad]],
                          _ty=opcodes.point_of_control)

        # High Flow Limit
        self.hf = 0

        # Shutdown on high flow
        self.hs = opcodes.false

        # Unscheduled flow limit
        self.uf = 0

        # Shutdown on unscheduled
        self.us = opcodes.false

        # Flow meter address
        self.fm = ''

        # Master valve address
        self.mv = ''

        # Pump decoder address
        self.pp = ''

        self.identifiers = [[opcodes.point_of_control, _ad]]

        self.statuses = PointOfControlStatuses(_point_of_control_object=self)  # POC Statuses
        """:type: common.objects.statuses.programming.point_of_control_statuses.PointOfControlStatuses"""

    #################################
    def set_high_flow_limit(self, _limit=None, with_shutdown_enabled=False):
        """
        Sets the 'High Flow Limit' for the Point of Control on the controller. \n

        :param _limit:    New 'High Flow Limit' for Point Of Connection \n
        :type _limit:     int| float \n

        :param with_shutdown_enabled:    Shutdown a point of connection when the limit is hit. \n
        :type with_shutdown_enabled:     bool \n
        """

        if _limit is not None:
            # Assign new 'High Flow Limit' to object attribute
            self.hf = _limit
        else:
            self.hf = 0

        if isinstance(with_shutdown_enabled, bool):
            if with_shutdown_enabled:
                self.hs = opcodes.true
            else:
                self.hs = opcodes.false
        else:
            e_msg = "High Flow Limit' {0} entered for Point of Control {1}. Must be True or False." \
                    "You passed in an incorrect type of: {2}.".format(
                        str(with_shutdown_enabled),  # {0} this is if shutdown will be enabled
                        str(self.identifiers[0][1]),                # {1} point of control address
                        type(with_shutdown_enabled)  # {2} type of passed in value
                    )
            raise ValueError(e_msg)

        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            ActionCommands.SET,                 # {0}
            opcodes.point_of_connection,        # {1}
            str(self.identifiers[0][1]),                       # {2}
            opcodes.high_flow_limit,            # {3}
            str(self.hf),                       # {4}
            opcodes.high_flow_shutdown,         # {5}
            self.hs)                            # {6}
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'High Flow Limit' to: '{1}' and 'High Flow Shutdown'" \
                    "to {2} -> {3}".format(
                        str(self.identifiers[0][1]),   # {0}
                        str(self.hf),   # {1}
                        str(self.hs),   # {2}
                        e.message       # {3}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'High Flow Limit' to: {1} and 'High Flow Shutdown'"
                  "to {2}".format(
                        str(self.identifiers[0][1]),   # {0}
                        str(self.hf),   # {1}
                        str(self.hs)))  # {2}

    #################################
    def set_unscheduled_flow_limit(self, _gallons=None, with_shutdown_enabled=False):
        """
        Sets the 'Unscheduled Flow Limit' for the Point of Control on the controller. \n

        :param _gallons:    New 'Unscheduled Flow Limit' for Point Of Connection \n
        :type _gallons:     float|int \n

        :param with_shutdown_enabled:    Shutdown a point of connection when the limit is hit. \n
        :type with_shutdown_enabled:     bool \n
        """
        if _gallons is not None:
            # Assign new 'Unscheduled Flow Limit' to object attribute
            self.uf = _gallons
        else:
            self.uf = 0

        if not isinstance(self.uf, float) and not isinstance(self.uf, int):
            raise TypeError('Gallons must be a float or int value')

        elif isinstance(self.uf, int):
            self.uf = float(self.uf)

        if isinstance(with_shutdown_enabled, bool):
            if with_shutdown_enabled:
                self.us = opcodes.true
            else:
                self.us = opcodes.false
        else:
            e_msg = "Invalid 'shutdown enabled' {0} entered for Point of Control {1}. Must be True or False." \
                    "You passed in an incorrect type of: {2}.".format(
                        str(with_shutdown_enabled),  # {0} this is if shutdown will be enabled
                        str(self.identifiers[0][1]),                # {1} point of control address
                        type(with_shutdown_enabled)  # {2} type of passed in value
                    )
            raise ValueError(e_msg)

        command = "{0},{1}={2},{3}={4},{5}={6}".format(ActionCommands.SET,                  # {0}
                                                       opcodes.point_of_connection,         # {1}
                                                       str(self.identifiers[0][1]),                        # {2}
                                                       opcodes.unscheduled_flow_limit,      # {3}
                                                       str(self.uf),                        # {4}
                                                       opcodes.unscheduled_flow_shutdown,   # {5}
                                                       self.us)                             # {6}
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set POC {0}'s 'Unscheduled Flow Limit' to: '{1}' and" \
                    " 'With Shutdown Enabled' to {2} -> {3}".format(
                        str(self.identifiers[0][1]),   # {0}
                        str(self.uf),   # {1}
                        str(self.us),   # {2}
                        e.message       # {3}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Unscheduled Flow Limit' to: {1}".format(str(self.identifiers[0][1]),
                                                                                       str(self.uf)))

    #################################
    def add_flow_meter_to_point_of_control(self, _flow_meter_address):
        """
        Sets the 'Flow meter' for the Point of Control on the controller. \n

        :param _flow_meter_address:    New 'flow meter' for Point Of Connection \n
        :type _flow_meter_address:     int \n
        """
        # Validate Flow meter address
        if _flow_meter_address not in self.controller.flow_meters.keys():
            e_msg = "Invalid 'flow meter address' {0} entered for Point of Control {1}" \
                .format(
                    str(_flow_meter_address),   # {0} this should be the flow meter address
                    str(self.identifiers[0][1])       # {1} point of control address
                )
            raise ValueError(e_msg)

        # Assign new 'Flow Meter' to object attribute
        else:
            _sn = self.controller.flow_meters[_flow_meter_address].sn
            self.fm = _flow_meter_address
            command = "{0},{1}={2},{3}={4}".format(
                ActionCommands.SET,
                opcodes.point_of_control,   # {0}
                str(self.identifiers[0][1]),               # {1}
                opcodes.flow_meter,         # {2}
                str(_sn)                    # {3} serial number
            )
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set flow meter {0} on point of control {1}. Exception: {2}".format(
                str(self.fm),   # {0} flow meter address
                str(self.identifiers[0][1]),   # {1}
                e.message       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Flow Meter' to: {1}".format(
                str(self.identifiers[0][1]),  # {0}
                str(self.fm),  # {1}
            ))

    #################################
    def add_pump_to_point_of_control(self, _pump_address):
        """
        Sets the 'Pump' for the Point of Control on the controller. \n

        :param _pump_address:    New 'pump' for Point Of Connection \n
        :type _pump_address:     int \n
        """
        # Validate pump address
        if _pump_address not in self.controller.pumps.keys():
            e_msg = "Invalid 'pump address' {0} entered for Point of Control {1}" \
                .format(
                    str(_pump_address),                 # {0} pump address
                    str(self.identifiers[0][1])                        # {1} point of control address
                )
            raise ValueError(e_msg)

        # Assign new 'Pump' to object attribute
        else:
            _sn = self.controller.pumps[_pump_address].sn
            self.pp = _pump_address
            command = "{0},{1}={2},{3}={4}".format(
                ActionCommands.SET,
                opcodes.point_of_control,                   # {0}
                str(self.identifiers[0][1]),                               # {1}
                PointofControlCommands.Attributes.PUMP,     # {2}
                str(_sn)                                    # {3} this needs to be the serial number
            )
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set pump {0} on point of control {1}. Exception: {2}".format(
                str(_pump_address),             # {0}
                str(self.identifiers[0][1]),                   # {1}
                e.message                       # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Pump' to: {1}".format(
                str(self.identifiers[0][1]),                   # {0}
                str(_pump_address),  # {1}
            ))

    #################################
    def add_master_valve_to_point_of_control(self, _master_valve_address):
        """
        Sets the 'Master Valve' for the Point of Control on the controller. \n

        :param _master_valve_address:    New 'master_valve' for Point Of Connection \n
        :type _master_valve_address:     int \n
        """
        # Validate master valve address
        if _master_valve_address not in self.controller.master_valves.keys():
            e_msg = "Invalid 'master valve address' {0} entered for Point of Control {1}" \
                .format(
                    str(_master_valve_address),                 # {0} master_valve address
                    str(self.identifiers[0][1])                        # {1} point of control address
                )
            raise ValueError(e_msg)

        # Assign new 'Master Valve' to object attribute
        else:
            _sn = self.controller.master_valves[_master_valve_address].sn
            self.mv = _master_valve_address
            command = "{0},{1}={2},{3}={4}".format(
                ActionCommands.SET,
                opcodes.point_of_control,       # {0}
                str(self.identifiers[0][1]),    # {1}
                opcodes.master_valve,           # {2}
                str(_sn)                        # {3} this needs to be the serial number
            )
        try:
            # Attempt to send command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set 'Master Valve' {0} on point of control {1}. Exception: " \
                    "{2}".format(
                        str(_master_valve_address),     # {0}
                        str(self.identifiers[0][1]),    # {1}
                        e.message                       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set POC {0}'s 'Master Valve' to: {1}".format(
                str(self.identifiers[0][1]),                   # {0}
                str(_master_valve_address),     # {1}
            ))

    #################################
    def verify_high_flow_limit(self):
        """
        Verifies the 'High Flow Limit' value for the Point of Control on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'High Flow Limit' value from the controller reply.
        -   Lastly, compares the instance 'High Flow Limit' value with the controller's programming.
        """

        high_flow_limit_from_controller = float(self.data.get_value_string_by_key(opcodes.high_flow_limit))

        # Compare status versus what is on the controller
        if self.hf != high_flow_limit_from_controller:
            e_msg = "Unable to verify POC {0}'s 'High Flow Limit' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                       # {0}
                        str(high_flow_limit_from_controller),               # {1}
                        str(self.hf)                                        # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'High Flow Limit' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.hf)    # {1}
            ))

    #################################
    def verify_shutdown_on_high_flow(self):
        """
        Verifies the 'Shutdown On High Flow' value for the Point of Control on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Shutdown On High Flow' value from the controller reply.
        -   Lastly, compares the instance 'Shutdown On High Flow' value with the controller's programming.
        """

        shutdown_on_high_flow_from_controller = str(self.data.get_value_string_by_key(opcodes.shutdown_on_high_flow))

        # Compare status versus what is on the controller
        if self.hs != shutdown_on_high_flow_from_controller:
            e_msg = "Unable to verify POC {0}'s 'Shutdown On High Flow' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                       # {0}
                        str(shutdown_on_high_flow_from_controller),         # {1}
                        str(self.hs)                                        # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Shutdown On High Flow' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.hs)    # {1}
            ))

    #################################
    def verify_unscheduled_flow_limit(self):
        """
        Verifies the 'Unscheduled Flow Limit' value for the Point of Control on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Unscheduled Flow Limit' value from the controller reply.
        -   Lastly, compares the instance 'Unscheduled Flow Limit' value with the controller's programming.
        """

        unscheduled_flow_limit_from_controller = float(self.data.get_value_string_by_key(
            opcodes.unscheduled_flow_limit))

        # Compare status versus what is on the controller
        if self.uf != unscheduled_flow_limit_from_controller:
            e_msg = "Unable to verify POC {0}'s 'Unscheduled Flow Limit' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                       # {0}
                        str(unscheduled_flow_limit_from_controller),       # {1}
                        str(self.uf)                                        # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Unscheduled Flow Limit' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.uf)    # {1}
            ))

    #################################
    def verify_shutdown_on_unscheduled(self):
        """
        Verifies the 'Shutdown on Unscheduled' value for the Point of Control on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Shutdown on Unscheduled' value from the controller reply.
        -   Lastly, compares the instance 'Shutdown on Unscheduled' value with the controller's programming.
        """

        unscheduled_flow_shutdown_from_controller = str(self.data.get_value_string_by_key(
            opcodes.unscheduled_flow_shutdown))

        # Compare status versus what is on the controller
        if self.us != unscheduled_flow_shutdown_from_controller:
            e_msg = "Unable to verify POC {0}'s 'Shutdown on Unscheduled' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                       # {0}
                        str(unscheduled_flow_shutdown_from_controller),     # {1}
                        str(self.us)                                        # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Shutdown on Unscheduled' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),   # {0}
                str(self.us)    # {1}
            ))

    #################################
    def verify_flow_meter_serial_number(self):
        """
        Verifies the 'Flow Meter Serial Number' value for the Point of Control on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Flow Meter Serial Number' value from the controller reply.
        -   Lastly, compares the instance 'Flow Meter Serial Number' value with the controller's programming.
        """

        flow_meter_from_controller = self.data.get_value_string_by_key(opcodes.flow_meter)

        # Get the current assigned serial number
        if self.fm:
            current_assigned_fm_serial = self.controller.flow_meters[self.fm].sn
        else:
            current_assigned_fm_serial = None

        # Compare status versus what is on the controller
        if current_assigned_fm_serial != flow_meter_from_controller:
            e_msg = "Unable to verify POC {0}'s 'Flow Meter Serial Number' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                       # {0}
                        str(flow_meter_from_controller),                    # {1}
                        str(current_assigned_fm_serial)                     # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Flow Meter Serial Number' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),                       # {0}
                str(current_assigned_fm_serial)     # {1}
            ))

    #################################
    def verify_master_valve_serial_number(self):
        """
        Verifies the 'Master Valve Serial Number' value for the Point of Control on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Master Valve Serial Number' value from the controller reply.
        -   Lastly, compares the instance 'Master Valve Serial Number' value with the controller's programming.
        """

        master_valve_from_controller = self.data.get_value_string_by_key(opcodes.master_valve)

        # Get the current assigned serial number
        if self.mv:
            current_assigned_mv_serial = self.controller.master_valves[self.mv].sn
        else:
            current_assigned_mv_serial = None

        # Compare status versus what is on the controller
        if current_assigned_mv_serial != master_valve_from_controller:
            e_msg = "Unable to verify POC {0}'s 'Master Valve Serial Number' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                       # {0}
                        str(master_valve_from_controller),                  # {1}
                        str(current_assigned_mv_serial)                     # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Master Valve Serial Number' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),                       # {0}
                str(current_assigned_mv_serial)     # {1}
            ))

    #################################
    def verify_pump_serial_number(self):
        """
        Verifies the 'Pump Serial Number' value for the Point of Control on the Controller. \n
        -   First, assumes that 'self.get_data()' has been called, which returns the controller's response into
            'self.data'.
        -   Second, retrieves the 'Pump Serial Number' value from the controller reply.
        -   Lastly, compares the instance 'Pump Serial Number' value with the controller's programming.
        """

        pump_from_controller = self.data.get_value_string_by_key("PP")

        # Get the current assigned serial number
        if self.pp:
            current_assigned_pp_serial = self.controller.pumps[self.pp].sn
        else:
            current_assigned_pp_serial = None

        # Compare status versus what is on the controller
        if pump_from_controller != current_assigned_pp_serial:
            e_msg = "Unable to verify POC {0}'s 'Pump Serial Number' value. Received: {1}, " \
                    "Expected: {2}".format(
                        str(self.identifiers[0][1]),                                       # {0}
                        str(pump_from_controller),                          # {1}
                        str(current_assigned_pp_serial)                     # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified POC {0}'s 'Pump Serial Number' value: '{1}' on controller".format(
                str(self.identifiers[0][1]),                       # {0}
                str(current_assigned_pp_serial)     # {1}
            ))

    #################################
    def verify_who_i_am(self, expected_status=None):
        """
        Verifies all attributes associated with this Point of Control . \n
        :param expected_status:     An expected status to verify for POC \n
        :type expected_status:      str \n
        """
        self.get_data()

        # verify attributes common to all controllers
        self.verify_description()
        self.verify_enabled_state()
        self.verify_target_flow()
        self.verify_high_flow_limit()
        self.verify_shutdown_on_high_flow()
        self.verify_unscheduled_flow_limit()
        self.verify_shutdown_on_unscheduled()
        self.verify_flow_meter_serial_number()
        self.verify_master_valve_serial_number()
        self.verify_pump_serial_number()

        if expected_status is not None:
            self.verify_status(_expected_status=expected_status)
