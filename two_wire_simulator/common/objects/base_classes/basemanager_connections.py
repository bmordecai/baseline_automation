import time
from datetime import datetime
from urlparse import urlparse
from common.imports import opcodes
from common.imports.types import ActionCommands
from common.objects.messages.basemanager.basemanager_messages import BasemanagerMessages
__author__ = 'tige'


########################################################################################################################
# Server Connection CLASS
########################################################################################################################
class BaseManagerConnection(object):
    """
    BaseManager Object \n
    """

    #############################
    def __init__(self, _controller, _json_url='', _json_fixed_ip=''):
        """
        Initialize a BaseManger instance with the specified parameters \n
        Base class for all controller objects. \n

        :param _controller: Controller this object is "attached" to \n
        :type _controller:  common.objects.base_classes.controller.BaseController\n

        :param _json_url:   The DNS url of the basemanager server you want to connect to. \n
        :type _json_url:    str \n

        :param _json_fixed_ip:   The fixed ip of the basemanager server you want to connect to. \n
        :type _json_fixed_ip:    str \n
        """

        # -------Server Attributes----------------#
        # Give access to the controller
        self.controller = _controller
        # create message reference to object
        self.messages = BasemanagerMessages(_basemanager_object=self)  # Basemanager messages
        """:type: common.objects.messages.basemanager.basemanager_messages.BasemanagerMessages"""

        self.ser = _controller.ser
        self.url = _json_url         # url passed in from json
        self.ai = _json_fixed_ip     # fixed ip address passed in from json
        self.ua = opcodes.false     # Use Alternate Server IP
        self.ky = ''                # Registration Key
        self.ss = ''                # Status
        self.ct = opcodes.disconnect_basemanager_ethernet
        users_parsed_url_tuple = urlparse(self.url)
        self.ad = users_parsed_url_tuple.netloc

    #################################
    def __str__(self):
        """
        Returns string representation of this BaseManger Object. \n
        :return:    String representation of this BaseManager Object
        :rtype:     str
        """
        return_str = "\n-----------------------------------------\n" \
                     "BaseManager Object:\n" \
                     "Alternate Server IP:      {0}\n" \
                     "Use Alternate Server IP:  {1}\n" \
                     "Current BM Server Address:{2}\n" \
                     "Status:                   {3}\n" \
                     "Registration Key:         {4}\n" \
                     "-----------------------------------------\n".format(
                         self.ai,       # {0}
                         self.ua,       # {1}
                         self.ad,       # {2}
                         self.ss,       # {3}
                         self.ky        # {4}
                     )
        return return_str

    #################################
    def set_default_values(self):
        """
        set the default values of BaseManager on the Controller
        :return:
        :rtype:
        """
        command = "{0},{1},{2}={3},{4}={5}".format(
            ActionCommands.SET,                     # {0}
            str(opcodes.basemanager),               # {1}
            str(opcodes.alternate_server_ip),       # {2}
            str(self.ai),                           # {3}
            str(opcodes.use_alternate_server_ip),   # {4}
            str(self.ua)                            # {5}
        )

        try:
            # Attempt to set BaseManger default values on the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set {0}'s 'Default values' to {1}".format(
                    str(opcodes.basemanager),               # {0}
                    str(command)                            # {1}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set {0}'s 'Default values' to: \n {1} = {2}\n {3} = {4} \n ".format(
                str(opcodes.basemanager),               # {0}
                str(opcodes.alternate_server_ip),       # {1}
                str(self.ai),                           # {2}
                str(opcodes.use_alternate_server_ip),   # {3}
                str(self.ua)                            # {4}
            ))

    #############################
    def set_ai_for_cn(self, ip_address, unit_testing=False):
        """
        The 1000, 3200 act differently in this category. The 3200 tries to reconnect to BaseManager automatically,
        whereas the 1000 needs to be disconnected and reconnected manually.
        1.  Sends a disconnect command to the controller telling it to disconnect from BaseManager. \n
        2.  Set the controllers alternate ip address to the user's entered ip address and configure the controller to
            use alternate address. \n
        3.  Reconnect the controller to BaseManager. \n
        4.  Verify controller is connected to the new ip address by pinging BM. \n

        :param ip_address:  This can be an actual ip address, or you can pass in one of our BaseManager DNS addresses
                            ex: "https://p2.basemanager.net", "p2.basemanager.net", "104.130.159.113"
        :type ip_address:   str \n
        """
        # Todo: Check format for IP address
        dictionary_for_basemanager_address_pointers = {
            'p1.basemanager.net': '104.130.159.113',
            'p2.basemanager.net': '104.130.246.18',
            'p3.basemanager.net': '104.130.242.33',
            '104.130.159.113': '104.130.159.113',
            '104.130.246.18': '104.130.246.18',
            '104.130.242.33': '104.130.242.33'
        }

        start = datetime.now()  # a set timer will start time once the firefox web browser opens
        try:
            # The 3200 can switch BM pointers without sending the disconnect from BM command, but the 1000 needs to be
            # told to disconnect and reconnect
            if self.controller.is1000():
                self.ser.send_and_wait_for_reply('DO,BM=DC')
        except Exception:
            e_msg = " <- unable to send disconnect from BaseManager to controller: 'DO,BM=DC' -> "
            raise Exception(e_msg)

        # Build set message string with current users ip address pointer
        url = urlparse(ip_address).netloc
        if url:
            set_message = 'SET,BM,AI=' + dictionary_for_basemanager_address_pointers[url] + ',UA=TR'
            self.ai = dictionary_for_basemanager_address_pointers[url]
            self.ua = opcodes.true
        else:
            set_message = 'SET,BM,AI=' + ip_address + ',UA=TR'
            self.ai = ip_address
            self.ua = opcodes.true

        try:
            # Set controllers AI to point to correct IP specified by the user, ADDED: Cast to String to ensure string
            # object stays in tact.
            self.ser.send_and_wait_for_reply(str(set_message))
        except Exception:
            e_msg = " <- BC received after issuing command: " + set_message + " -> "
            raise Exception(e_msg)
        else:
            print("Successfully set {0}'s 'Ip address on Controller' to: \n {1} = {2}\n {3} = {4} \n ".format(
                str(opcodes.basemanager),               # {0}
                str(opcodes.alternate_server_ip),       # {1}
                str(self.ai),                           # {2}
                str(opcodes.use_alternate_server_ip),   # {3}
                str(self.ua)                            # {4}
            ))

    ############################
    def set_controller_to_connect_using_ethernet(self, unit_testing=False):
        """
        Connect the controller to ethernet
        """
        self.ct = opcodes.ethernet
        print "Connect the controller to Ethernet"
        try:
            self.ser.send_and_wait_for_reply('{0},{1},{2}={3}'.format(
                opcodes.set_action,                 # {0}
                str(opcodes.basemanager),           # {1}
                str(opcodes.connect_to_ethernet),   # {2}
                str(self.ct)                        # {3}
            ))
        except Exception, e:
            e_msg = "Connect the controller to Ethernet Command Failed: " + e.message
            raise Exception(e_msg)


    ############################
    def connect_v12_cn_to_ethernet(self, unit_testing=False):
        """
        Connect the controller to ethernet
        """

        print "Connect the controller to Ethernet"
        try:
            self.ser.send_and_wait_for_reply('{0},{1}={2}'.format(
                opcodes.do_action,                  # {0}
                str(opcodes.basemanager),           # {1}
                str(opcodes.connected),   # {2}
            ))

        except Exception, e:
            e_msg = "Connect the controller to Ethernet Command Failed: " + e.message
            raise Exception(e_msg)

        number_of_seconds = 0
        time_to_sleep = 15
        while not self.verify_cn_connected_to_bm():
            if not unit_testing:
                time.sleep(time_to_sleep)
            print "Connecting to Ethernet"
            if number_of_seconds == 180:
                e_msg = "Your controller is not connected the Ethernet! Please connect it."
                raise AttributeError(e_msg)
            else:
                number_of_seconds += time_to_sleep

        print "-------------------------------------------------------"
        print "       | - Successfully Connected the Controller to the Ethernet.  - |       "
        print "-------------------------------------------------------"

    ############################
    def connect_cn_to_bm(self):
        """
        Connect the controller to Basemanager
        """
        print "Connect the controller to Basemanager"
        try:
            self.ser.send_and_wait_for_reply('DO,{0}={1}'.format(
                str(opcodes.basemanager),                   # {0}
                str(opcodes.connect_to_basemanager)         # {1}
            ))

            print "-------------------------------------------------------"
            print "       | - Successfully Connected the Controller to Basemanager.  - |       "
            print "-------------------------------------------------------"
        except Exception, e:
            e_msg = "Connect the controller to Basemanager Command Failed: " + e.message
            raise Exception(e_msg)

    #############################
    def disconnect_cn_from_bm(self):
        """
        Disconnect the controller from Basemanager
        """
        print "Disconnect the controller to Basemanager"
        try:
            self.ser.send_and_wait_for_reply('DO,{0}={1}'.format(
                str(opcodes.basemanager),                       # {0}
                str(opcodes.disconnect_from_basemanager)        # {1}
            ))
            print "-------------------------------------------------------"
            print "       | - Successfully Disconnected the Controller from Basemanager.  - |       "
            print "-------------------------------------------------------"
        except Exception, e:
            e_msg = "disconnect the controller to Basemanager Command Failed: " + e.message
            raise Exception(e_msg)

    #############################
    def ping_bm_from_cn(self):
        """
        Ping Basemanager from the controller
        """
        print "Ping Basemanager from the controller"
        try:
            self.ser.send_and_wait_for_reply('DO,{0}={1}'.format(
                str(opcodes.basemanager),       # {0}
                str(opcodes.ping_server)        # {1}
            ))
            print "-------------------------------------------------------"
            print "       | - Successfully Pinged Basemanager from the controller.  - |       "
            print "-------------------------------------------------------"
        except Exception, e:
            e_msg = "Pinging Basemanager from the controller Command Failed: " + e.message
            raise Exception(e_msg)



    ############################
    def set_dont_connect_to_bm(self):
        """
        Disconnect the controller to ethernet
        """
        print "Disconnect the controller to Ethernet"
        try:
            if self.controller.vr >= opcodes.firmware_version_V16:
                self.ser.send_and_wait_for_reply('{0},{1},{2}={3}'.format(
                    opcodes.set_action,                                 # {0}
                    str(opcodes.basemanager),                           # {1}
                    str(opcodes.connect_to_ethernet),                   # {2}
                    str(opcodes.disconnect_basemanager_ethernet)        # {3}
                ))
            else:
                self.ser.send_and_wait_for_reply('{0},{1}={2}'.format(
                    opcodes.do_action,              # {0}
                    str(opcodes.basemanager),       # {1}
                    str(opcodes.disconnected)       # {2}
                ))

            print "-------------------------------------------------------"
            print "       | - Successfully Disconnected the Controller to Ethernet.  - |       "
            print "-------------------------------------------------------"
        except Exception, e:
            e_msg = "Disconnect the controller to Ethernet Command Failed: " + e.message
            raise Exception(e_msg)

    #############################
    def verify_bm_ip_address_on_cn(self, unit_testing=False):
        """
        :return:
        :rtype:
        """
        try:
            controller_reply = self.ser.get_and_wait_for_reply("GET,BM")
            bm_ip_address = controller_reply.get_value_string_by_key(opcodes.alternate_server_ip)
            # If IP addresses are equal, check if we set the IP address to be blank, controller converts it to 0.0.0.0
            if bm_ip_address != self.ai and (self.ai != '' and bm_ip_address != '0.0.0.0'):
                message = "controller ip address doesnt match what is in the json file. json = {0} controller " \
                          "IP for BaseManager = {1}". format(
                            self.ai,                # this comes from the users json file
                            bm_ip_address           # this is what the controller has in it

                            )
                raise AssertionError(message)

        except AssertionError as ae:
            exception_message = "Exception occurred after sending command to controller: 'GET,BM'.\n" + \
                                "Controller Response: [" + str(ae.message) + "]"
            raise AssertionError(exception_message)

    #############################
    def verify_use_alternate_ip(self, unit_testing=False):
        """
        :return:
        :rtype:
        """
        try:
            controller_reply = self.ser.get_and_wait_for_reply("GET,BM")
            alt_state = controller_reply.get_value_string_by_key(opcodes.use_alternate_server_ip)
            if alt_state != self.ua:
                message = "controller use alternate ip address doesnt match what is in the json file. json = {0} " \
                          "controller IP for BaseManager = {1}".format(
                            self.ua,   # this comes from the users json file
                            alt_state  # this is what the controller has in it

                            )
                raise AssertionError(message)

        except AssertionError as ae:
            exception_message = "Exception occurred after sending command to controller: 'GET,BM'.\n" + \
                                "Controller Response: [" + str(ae.message) + "]"
            raise AssertionError(exception_message)

    #############################
    def verify_ip_address_state(self, unit_testing=False):
        """
        1.  Parse the url with build-in 'urlparse' python module . \n
        3.  Communicates with the controller and gets the current BaseManager settings from the controller. \n
        4.  Determine whether the controller is pointing to the same url provided in the user credentials. \n
        5.  If not pointing correctly, \n
                - Disconnects the controller from BM, verifies disconnected looking for a reply of SS=DC \n
                - Sends the correct ip address to the controller \n
                - Reconnects to BM looking for a SS=CN \n
                - Verifies connected by pinging BaseManager \n
        :return:
        :rtype:
        """
        # TODO this code will be used in the future to connect the controller to BMW if it is not
        try:
            # Need to get the current controller BM configuration for verification.
            controller_reply = self.ser.get_and_wait_for_reply("GET,BM")
            connection_status = controller_reply.get_value_string_by_key(opcodes.status_code)
            if connection_status == opcodes.disconnect_from_basemanager:
                self.set_controller_to_connect_using_ethernet()
        except AssertionError as ae:
            exception_message = "Exception occurred after sending command to controller: 'GET,BM'.\n" + \
                                "Controller Response: [" + str(ae.message) + "]"
            raise AssertionError(exception_message)

        # Verify that there is a connection to BaseManager before we check anything else
        # check for connection wait 1 second if than check again if not connected after 10 seconds than fail

        number_of_checks = 0
        time_to_sleep = 1
        while not self.verify_cn_connected_to_bm():
            if not unit_testing:
                time.sleep(time_to_sleep)
            number_of_checks += time_to_sleep
            if number_of_checks == 10:
                e_msg = "Your controller is not connected to BaseManager! Please connect it."
                raise AttributeError(e_msg)

        dictionary_for_basemanager_address_pointers = {
            'p1.basemanager.net': '104.130.159.113',
            'p2.basemanager.net': '104.130.246.18',
            'p3.basemanager.net': '104.130.242.33',
            '104.130.159.113': '104.130.159.113',
            '104.130.246.18': '104.130.246.18',
            '104.130.242.33': '104.130.242.33'
        }

        # without a defined url scheme, a.k.a 'http'/'https', the url parser won't separate anything on the left of the
        # slash in 10.11.12.12/login.php?debug=true from the right. In other words, the url on the left would parse to:
        # bad_result: parsed_address = 10.11.12.13/login.php
        # intended_result: parsed_address= 10.11.12.13

        try:
            # Need to get the current controller BM configuration for verification.
            controller_reply = self.ser.get_and_wait_for_reply("GET,BM")
            self.ad = controller_reply.get_value_string_by_key("AD")
        except AssertionError as ae:
            exception_message = "Exception occurred after sending command to controller: 'GET,BM'.\n" + \
                                "Controller Response: [" + str(ae.message) + "]"
            raise AssertionError(exception_message)

        # For a 1000, the DNS Host Name is equivalent to the ip address value, so use a dictionary to convert any p1,
        #  p2 and p3 url address' to their equivalent ip address values if possible.
        try:
            current_users_bm_ip_address = dictionary_for_basemanager_address_pointers[self.ad]
        except KeyError:
            # User is using a local address, so use the local address for comparisons
            current_users_bm_ip_address = self.ad

        # Determine if the controller is set up to use an alternate address, TR (true)/FA (false). If the user is trying
        # to switch ip address pointer of the controller, this value must be true.
        controllers_configured_to_use_ai_address = controller_reply.get_value_string_by_key('UA')

        # If we are a 1000, always force this condition
        if controllers_configured_to_use_ai_address == 'TR' or self.controller.controller_type == "10":
            # Controller is configured to use an alternate ip address for BaseManager, so get the controllers
            # configured alternate ip address for comparison
            controllers_configured_ai_key_value = controller_reply.get_value_string_by_key('AI')

            # If they aren't pointing to the same thing, we want to disconnect and re-point them
            if controllers_configured_ai_key_value != current_users_bm_ip_address:
                self.set_ai_for_cn(ip_address=current_users_bm_ip_address)

        # Else, the controller is configured as, BM,UA=FA, so check the pointer value for key value 'AD' because 'AI' is
        # being ignored.
        else:
            current_users_bm_dns_address = self.ad
            controllers_configured_ad_key_value = controller_reply.get_value_string_by_key('AD')

            # TODO: This if statement works for a 3200, but for the 1000, the controller's 'AD' value is an IP address,
            # TODO: not a DNS host name, thus, we need to fix this if statement to handle both cases,
            # TODO: it works regardless.
            # If they aren't pointing to the same thing, we want to disconnect and re-point them
            if controllers_configured_ad_key_value != current_users_bm_dns_address:
                self.set_ai_for_cn(ip_address=current_users_bm_ip_address)

    ############################
    def verify_controller_is_connecting_using_ethernet(self, unit_testing=False):
        """
        Connect the controller to ethernet
        """

        try:
            # Need to get the current controller BM configuration for verification.
            controller_reply = self.ser.get_and_wait_for_reply("GET,BM")
            connecting_using_ethernet = controller_reply.get_value_string_by_key(opcodes.connect_to_ethernet)
            if connecting_using_ethernet != self.ct:
                message = "controller is not set to use Ethernet to connect to BaseManager"
                raise message
        except AssertionError as ae:
            exception_message = "Exception occurred after sending command to controller: 'GET,BM'.\n" + \
                                "Controller Response: [" + str(ae.message) + "]"
            raise AssertionError(exception_message)

    ############################
    def verify_cn_connected_to_bm(self):
        try:
            # Need to get the current controller BM configuration for verification.
            controller_reply = self.ser.get_and_wait_for_reply("GET,BM")
        except AssertionError as ae:
            exception_message = "Exception occurred after sending command to controller: 'GET,BM'.\n" + \
                                "Controller Response: [" + str(ae.message) + "]"
            raise AssertionError(exception_message)

        # Get the connected status
        status_from_cn = controller_reply.get_value_string_by_key(opcodes.status_code)

        # are we connected?
        if status_from_cn != opcodes.connect_to_basemanager:
            print("| ------- Not Currently Connected to BaseManager -------|")
            return False
        else:
            print "|- Controller is connected to Basemanager.  - |"
            return True

    ############################
    def wait_for_bm_connection(self, unit_testing=False):
        """
        This waits for BaseManger to get connected if it doesnt connect in 6 trys we bomb out
        :param unit_testing:
        :type unit_testing:
        :return:
        :rtype:
        """
        number_of_retries = 0

        while not self.verify_cn_connected_to_bm():

            if number_of_retries == 20:
                e_msg = "The Connection to Basemanager failed was unable to connect after 3 minutes and 25 seconds: "
                raise Exception(e_msg)

            else:
                if not unit_testing:
                    # sleep to get the controller time to send the back up file
                    for sec in range(1, 16):
                        print "Retry {0} Waiting for BaseManger Connection, Sleeping {1} Seconds out of 15 Seconds" \
                              "............"\
                            .format(
                                number_of_retries,      # {0}
                                sec                     # {1}
                                )
                        time.sleep(1)
                number_of_retries += 1
        print "-------------------------------------------------------"
        print "       | - Successfully Connected the Controller to the Ethernet.  - |       "
        print "-------------------------------------------------------"

