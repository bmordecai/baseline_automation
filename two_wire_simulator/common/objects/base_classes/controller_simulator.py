from datetime import datetime, timedelta
import time
import os
from common.imports import opcodes
from common.date_package.date_resource import date_mngr
from common.objects.base_classes.devices import BaseDevices
from common.imports.types import ControllerCommands, ActionCommands, DeviceCommands, BiCoderCommands
from common.object_factory import create_bicoder_object
from abc import abstractmethod
from common import object_factory
from common.helper_methods import compare_versions
import file_transfer_handler
from common.objects.base_classes.basemanager_connections import BaseManagerConnection


__author__ = 'ben'


########################################################################################################################
# CONTROLLER CLASS
########################################################################################################################
class SimulatedControllerBaseClass(BaseDevices):
    """
    Controller Object \n
    """

    #############################
    def __init__(self, _type, _mac, _serial_port, _serial_number, _ad=None, _firmware_version=None, _description=''):
        """
        Initialize a controller instance with the specified parameters. \n

        :param _type:           Type of controller, '10' or '32' or 'FS' or 'SB' accepted. String.
        :type _type:            str \n
        :param _mac:            Mac address for controller. \n
        :type _mac:             str \n
        :param _serial_port:    Serial port of the controller to communicate with. \n
        :type _serial_port:     common.objects.base_classes.ser.Ser \n
        :param _firmware_version:    Firmware version we want on the controller. Does a GET on the controller if None. \n
        :type _firmware_version:     str
        :param _description:    Description to put on the controller. \n
        :type _description:     str

        :return:
        """
        # Specify the serial port this object will be using
        self.ser = _serial_port
        self.ser.controller_reference = self

        self.controller_type = _type
        self.ser2 = None
        self.old_data_group_packets = None
        """:type: common.objects.base_classes.old_data_group_packets.OldDataGroupPackets"""
        self.data_group_packets = None
        """:type: common.objects.base_classes.data_group_packets.DataGroupPackets"""
        self.mac = _mac
        self.mc = 0

        # Added (originally for UseCase52) to be used in two-wire attribute verifiers for bicoders to expand the range
        # being used to verify against. Default to True
        self.faux_io_enabled = True

        controller_id = opcodes.controller

        # Initialize parent class for inheritance
        BaseDevices.__init__(self,
                             _sn=_serial_number,                    # Serial number for controller
                             _identifiers=[[controller_id, _serial_number]],   # Identifier for the controller
                             _ty=DeviceCommands.Type.CONTROLLER,    # Type of controller
                             _ser=_serial_port,                     # Serial port the controller talks to
                             _la=0.0,
                             _lg=0.0,
                             _ad=_ad
                             )

        # If the description was passed in, overwrite the default one given by base_methods.py as the parent method
        if _description:
            self.ds = self.verify_description_length(_type=_type, _desc=_description)
        if type == ControllerCommands.Type.BASESTATION_3200:
            self.set_serial_number(_serial_num=_serial_number)

        # Controller Attributes
        self.data = None                # Data from `get_data()`

        self.check_and_update_firmware(_firmware_version=_firmware_version)

        # Date and time on the controller
        self.controller_object_current_date_time = date_mngr.controller_datetime

        if self.ty in [
            DeviceCommands.Type.CONTROLLER
        ]:
            pass
            # self.send_programming()



    #################################
    def update_identifiers(self, _test_engine_object):
        """
        This method updates the identifiers of the test engine object based on whether or not the object is shared
        with a Flow Station.
        :param: _test_engine_object:
        :type: common.objects.programming.ws.WaterSource| common.objects.programming.point_of_control.PointOfControl|
        common.objects.programming.ml.Mainline

        """
        if self.controller_type == opcodes.flow_station:
            _test_engine_object.identifiers[0][1] = _test_engine_object.flow_station_slot_number
        else:
            _test_engine_object.identifiers[0][1] = _test_engine_object.ad

    #############################
    def initialize_for_test(self, real_devices=False):
        """
        initiate controller to a known state so that it doesnt have a configuration or any devices loaded
        turn on echo so the commands are displayed in the console
        turn on sim mode so the clock can be stopped
        stop the clock
        Set the date and time so  that the controller is in a known state
        turn on faux IO
        clear all devices
        Clear all programming
        """

        # turn on echo
        try:
            self.turn_on_echo()
            self.set_sim_mode_to_on()
            self.stop_clock()
            self.set_date_and_time(_date='01/01/2017', _time='01:00:00')
            if real_devices:
                self.turn_off_faux_io()
            else:
                self.turn_on_faux_io()
            self.clear_all_devices()
            self.clear_all_programming()
        except Exception, ae:
            raise AssertionError("Initiate Controller Command Failed: " + ae.message)

        else:
            print "\nController: {0} ({1})\n-> Successfully Initialized.\n".format(
                self.mac,  # {0}
                self.ty    # {1}
            )

    #############################
    def reinitialize_controller(self):
        """
        re-initiate controller to a known state after event like backup before you do a restore
        turn on echo so the commands are displayed in the console
        turn on sim mode so the clock can be stopped
        stop the clock
        Set the date and time so  that the controller is in a known state
        turn on faux IO
        clear all devices
        Clear all programming
        """

        # turn on echo
        try:
            self.turn_on_echo()
            self.set_sim_mode_to_on()
            self.stop_clock()
            self.turn_on_faux_io()
            self.clear_all_devices()
            self.clear_all_programming()

        except Exception, ae:
            raise AssertionError("Initiate Controller Command Failed: " + ae.message)

        else:
            print "\nController: {0} ({1})\n-> Successfully Initialized.\n".format(
                self.mac,  # {0}
                self.ty  # {1}
            )



    #############################
    def load_device_to_simulator(self,
                                 _device_type,
                                 _list_of_decoder_serial_nums,
                                 _bicoder_version=None):
        """
        Pass in the decoder type and serial number
        Pass that value to the verify functions

        :param _device_type: Type of device to load. \n
        :type _device_type: str

        :param _list_of_decoder_serial_nums: List of all the serial numbers of a certain device type
        :type _list_of_decoder_serial_nums: list[str]

        :param _bicoder_version:    The version of a bicoder. Used to fix a discrepancy in flow bicoder versions.
        :type _bicoder_version:     str
        """
        # Validate decoder type passed in against accepted types
        if _device_type not in [opcodes.single_valve_decoder, opcodes.two_valve_decoder, opcodes.four_valve_decoder,
                           opcodes.twelve_valve_decoder, opcodes.flow_meter, opcodes.moisture_sensor,
                           opcodes.temperature_sensor, opcodes.four_to_20_milliamp_sensor_decoder,
                           opcodes.event_switch, opcodes.alert_relay, opcodes.analog_decoder]:
            raise ValueError("[CONTROLLER] incorrect decoder type: " + str(_device_type))

        # For each serial number for the decoder type passed in from the list.
        for serial_number in _list_of_decoder_serial_nums:

            # Validate Serial Number
            if len(serial_number) != 7:
                # count the number of characters in the string
                raise ValueError("Device id is not a valid serial number: " + str(serial_number))
            else:
                print "Loading device of type: " + str(_device_type) + " with serial: " + str(serial_number)
                try:
                    command = '{0},{1}={2}'.format(
                        ActionCommands.DEVICE,  # {0}
                        _device_type,                # {1}
                        str(serial_number))     # {2}
                    
                    # If we are a substation and loading a Flow BiCoder, set the flow bicoder version to a version
                    # that uses raw count based flow readings instead of pulse-width based flow readings. This was added
                    # due to the fact that the test-engine (python code) doesn't have the pulse-width based flow
                    # readings implemented. In the future, pulse-width based could be implemented and then this function
                    # could be used to set a flow bicoder version > 5.5

                    if _bicoder_version:
                        if _device_type == opcodes.flow_meter ():
                            command += ',{0}={1}'.format('VE', _bicoder_version)
                        if _device_type == opcodes.single_valve_decoder():
                            command += ',{0}={1}'.format('VE', _bicoder_version)
                        if _device_type == opcodes.two_valve_decoder():
                            command += ',{0}={1}'.format('VE', _bicoder_version)
                        if _device_type == opcodes.four_valve_decoder():
                            command += ',{0}={1}'.format('VE', _bicoder_version)
                        if _device_type == opcodes.twelve_valve_decoder():
                            command += ',{0}={1}'.format('VE', _bicoder_version)
                    self.ser.send_and_wait_for_reply(command)
                    create_bicoder_object(_controller=self, _type=_device_type,
                                          _serial_number=serial_number, _device_type=_device_type)
                except Exception as e:
                    print(e.message)
                    raise Exception("Load device(s) failed.. " + str(e.message))

    #############################
    def load_all_dv(self, d1_list=list(), mv_d1_list=list(), pm_d1_list=list(), d2_list=list(), mv_d2_list=list(),
                    pm_d2_list=list(), d4_list=list(), dd_list=list(), ms_list=list(), fm_list=list(),
                    ts_list=list(), sw_list=list(), an_list=list(), unit_test=False):
        """
        Helper method. Loads all devices from the configuration file into the controller. \n
        had to have mv_d1_list and mv_d2_list in order to loader decoder that can be used as master valves\n
        master valve can only be single or dual decoders
        :param d1_list:
        :param mv_d1_list
        :param pm_d1_list
        :param d2_list:
        :param mv_d2_list
        :param pm_d2_list
        :param d4_list:
        :param dd_list:
        :param ms_list:
        :param fm_list:
        :param ts_list:
        :param sw_list:
        :param an_list:
        :param unit_test:
        :return:
        """
        try:
            self.load_device_to_simulator(_device_type="D1", list_of_decoder_serial_nums=d1_list, device_type=opcodes.zone)
            self.load_device_to_simulator(_device_type="D1", list_of_decoder_serial_nums=mv_d1_list, device_type=opcodes.master_valve)
            self.load_device_to_simulator(_device_type="D1", list_of_decoder_serial_nums=pm_d1_list, device_type=opcodes.pump)
            self.load_device_to_simulator(_device_type="D2", list_of_decoder_serial_nums=d2_list, device_type=opcodes.zone)
            self.load_device_to_simulator(_device_type="D2", list_of_decoder_serial_nums=mv_d2_list, device_type=opcodes.master_valve)
            self.load_device_to_simulator(_device_type="D2", list_of_decoder_serial_nums=pm_d2_list, device_type=opcodes.pump)
            self.load_device_to_simulator(_device_type="D4", list_of_decoder_serial_nums=d4_list, device_type=opcodes.zone)
            self.load_device_to_simulator(_device_type="DD", list_of_decoder_serial_nums=dd_list, device_type=opcodes.zone)
            self.load_device_to_simulator(_device_type="MS", list_of_decoder_serial_nums=ms_list)
            self.load_device_to_simulator(_device_type="FM", list_of_decoder_serial_nums=fm_list)
            self.load_device_to_simulator(_device_type="TS", list_of_decoder_serial_nums=ts_list)
            self.load_device_to_simulator(_device_type="SW", list_of_decoder_serial_nums=sw_list)
            if self.controller_type != opcodes.substation:
                self.load_device_to_simulator(_device_type="AN", list_of_decoder_serial_nums=an_list)
        except Exception as e:
            # Re-raise caught exception with additional info plus the exception info.
            appended_msg = self.ty + ": " + self.mac + " - Exception occurred loading all available devices " \
                                                       "to controller: " + str(e.message)
            raise Exception(appended_msg)
        else:
            # Added a sleep here with a theory that the controller was attempting to search for devices when the thread
            # spun to load all of the devices wasn't finished. Thus when we attempted to search and assign the first
            # zone, a BC was returned
            #
            # UPDATE: 3/21/17 - Added unit test flag to reduce unit test time for CN object
            if not unit_test:
                time.sleep(10)
            print "\nController: {0} ({1})\n-> [SUCCESS] Loaded all available devices.".format(self.mac, self.ty)

    #############################
    def clear_all_devices(self):
        """
        Clears all devices on the Controller.
        """
        print "Clear Devices"
        command = "{0},{1}={2}".format(
            ActionCommands.DEVICE,  # {0}
            opcodes.clear_all,      # {1}
            opcodes.all)            # {2}

        try:
            self.send_command_with_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying Clear all Devices command Sent was: {0}".format(
                command         # {0}
            )
            raise Exception(e_msg)
        else:
            e_msg = "Successfully Cleared All Devices"
            print (e_msg)

    #############################
    def clear_all_programming(self):
        """
        Clears all programming on the Controller.
        """
        print "Clear Programing"
        command = "{0},{1}={2}".format(
            ActionCommands.DO,  # {0}
            opcodes.clear_all,  # {1}
            opcodes.all)        # {2}

        try:
            self.send_command_with_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying Clear all Programing command Sent was: {0}".format(
                command         # {0}
            )
            raise Exception(e_msg)
        else:
            e_msg = "Successfully Cleared All Programing"
            print (e_msg)

    #############################
    def turn_on_faux_io(self):
        """
        Turn on faux io so that the controller can simulate two-wired IO \n
        :return:
        """
        print("Enable faux IO devices")

        command = "{0},{1}={2}".format(
            ActionCommands.DO,      # {0}
            opcodes.faux_IO,   # {1}
            opcodes.true)      # {2}
        try:
            self.send_command_with_reply(tosend=command)
            self.faux_io_enabled = True
        except Exception:
            e_msg = "Exception occurred trying to Enable Faux IO Devices Command Sent was: {0}".format(
                command         # {0}
            )
            raise Exception(e_msg)
        else:
            e_msg = "Successfully Enabled Faux IO Devices"
            print (e_msg)

    #############################
    def turn_off_faux_io(self):
        """
        Turn off faux io so that the controller can simulate two-wired IO \n
        :return:
        """
        print "Disable faux IO devices"
        command = "{0},{1}={2}".format(
            ActionCommands.DO,   # {0}
            opcodes.faux_IO,    # {1}
            opcodes.false)      # {2}

        try:
            self.send_command_with_reply(tosend=command)
            self.faux_io_enabled = False
        except Exception:
            e_msg = "Exception occurred trying to Disable Faux IO Devices Command Sent was: {0}".format(
                command         # {0}
            )
            raise Exception(e_msg)
        else:
            e_msg = "Successfully Disabled Faux IO Devices"
            print (e_msg)

    #############################
    def turn_on_echo(self):
        """
        Turn on Echo this will tell the controller to echo the packet that was sent to the controller
        """
        print "Turn on Echo"
        command = "{0},{1}={2}".format(
            ActionCommands.DO,      # {0}
            opcodes.echo,   # {1}
            opcodes.true)      # {2}

        try:
            self.send_command_with_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to turn on Echo command Sent was: {0}".format(
                command         # {0}
            )
            raise Exception(e_msg)
        else:
            e_msg = "Successfully Turned on Echo"
            print (e_msg)

    #############################
    def turn_off_echo(self):
        """
        Turn off Echo this will tell the controller not to echo the packet that was sent to the controller
        """
        print "Turn off Echo"
        command = "{0},{1}={2}".format(
            ActionCommands.DO,      # {0}
            opcodes.echo,   # {1}
            opcodes.false)      # {2}

        try:
            self.send_command_with_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to turn off Echo command Sent was: {0}".format(
                command         # {0}
            )
            raise Exception(e_msg)
        else:
            e_msg = "Successfully Turned off Echo"
            print (e_msg)

    #############################
    def stop_clock(self):
        """
        stop clock for sim mod
        """
        stop = '0'
        print "Stop Clock"
        command = "{0},{1},{2}={3}".format(
            ActionCommands.DO,  # {0}
            opcodes.clock,      # {1}
            opcodes.time,       # {2}
            stop)               # {3}

        try:
            self.send_command_with_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to Stop the Clock command Sent was: {0}".format(
                command         # {0}
            )
            raise Exception(e_msg)
        else:
            e_msg = "Successfully stopped the Clock"
            print (e_msg)

    #############################
    def start_clock(self):
        """
        start clock for use in browser mode
        """
        start = '-1'
        print "Start Clock"
        command = "{0},{1},{2}={3}".format(
            ActionCommands.DO,  # {0}
            opcodes.clock,      # {1}
            opcodes.time,       # {2}
            start)              # {3}

        try:
            self.send_command_with_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to Start the Clock command Sent was: {0}".format(
                command         # {0}
            )
            raise Exception(e_msg)
        else:
            e_msg = "Successfully start the Clock"
            print (e_msg)

    #################################
    def set_serial_number(self, _serial_num):
        """
        Sets the serial number of the controller. The current instance value can be overwritten by
        passing in a value for _serial_num. \n
        :param _serial_num:  serial number of the controller. \n
        :type _serial_num:   str \n
        :return:
        """
        # take the serial number that is passed in from the use case strip off the last 5 characters
        # take the new serial number passed is in to overwrite the old and strip of the first two characters
        # add the two together giving you a new serial number this allow the first to character to be kept the same
        # value from what you passed in at the use case level

        # first take the serial number a see if it is a string
        if not isinstance(_serial_num, str):
            e_msg = "Serial number must be in a string format '{0}'".format(
                str(_serial_num))
            raise TypeError(e_msg)
        # serial number from controller to find out if it is an x or K "3K" #"X"
        if type is ControllerCommands.Type.BASESTATION_3200:
            self.get_data()
            controller_serial_number = self.data.get_value_string_by_key(ControllerCommands.Attributes.SERIAL_NUMBER)
            mn_serial = controller_serial_number[:2]
            serial_nm = _serial_num[2:]
            new_sn = str(mn_serial + serial_nm)
        else:
            mn_serial = self.sn[:2]
            serial_nm = _serial_num[2:]
            new_sn = str(mn_serial + serial_nm)

        # count the number of characters in the string
        if len(new_sn) != 7:
            raise ValueError("Controller serial number must be a seven digit string, instead was " +
                             str(new_sn))
        else:
            # Overwrite flow station serial number with new value
            self.sn = new_sn
        # Command for sending
        command = "{0},{1},{2}={3}".format(
            ActionCommands.SET,                             # {0} SET
            DeviceCommands.Type.CONTROLLER,                 # {1} CN
            ControllerCommands.Attributes.SERIAL_NUMBER,    # {2} SN
            self.sn                                         # {3} Serial number
        )

        try:
            # Attempt to set serial number of the controller
            self.send_command_with_reply(tosend=command)

        except Exception:
            e_msg = "Exception occurred trying to set serial number on controller command sent was: {0}".format(
                command  # {0}
            )
            raise Exception(e_msg)
        else:
            e_msg = "Successfully set serial number on controller"
            print (e_msg)

    #############################
    def set_date_and_time(self, _date, _time, update_date_mngr=True):
        """
        Set the date and time for the controller. \n
        :param _date:       Date for controller 'MM/DD/YYYY' format. \n
        :param _time:       Time for controller 'HH:MM:SS' format. \n
        :param update_date_mngr: Flag set when updating controller and substations at the same time.
        :return:
        """
        print ('Set controller date and time to ' + str(_date) + ' ' + str(_time))
        try:
            datetime.strptime(_date, '%m/%d/%Y')
        except ValueError:
            raise ValueError("Incorrect data format, should be MM/DD/YYYY")
        try:
            datetime.strptime(_time, '%H:%M:%S')
        except ValueError:
            raise ValueError("Incorrect data format, should be HH:MM:SS")
        # Command for sending
        command = "{0},{1}={2} {3}".format(
            ActionCommands.SET,                         # {0}
            ControllerCommands.Attributes.DATETIME,     # {1}
            str(_date),                                 # {2}
            str(_time)                                  # {3}
        )
        try:
            self.send_command_with_reply(tosend=command)

            if update_date_mngr:
                date = datetime.strptime(_date, '%m/%d/%Y').date()
                date_mngr.controller_datetime.set_from_datetime_obj(datetime_obj=date, time_string=_time)
                date_mngr.curr_day.set_from_datetime_obj(datetime_obj=date, time_string=_time)
        except Exception as e:
            e_msg = "Exception occurred trying to set Date and Time on the Controller command Sent was: {0}. " \
                    "Exception: {1}".format(
                        command,   # {0}
                        e.message  # {1}
                    )
            raise Exception(e_msg)
        else:
            e_msg = "Successfully set Date and Time on the Controller"
            print (e_msg)

    #############################
    def set_sim_mode_to_on(self):
        """
        put controller in sim mod
        """
        print "Put controller into sim mode"
        command = "{0},{1}={2}".format(
            ActionCommands.DO,          # {0}
            opcodes.simulation_mode,    # {1}
            opcodes.true)               # {2}
        try:
            self.send_command_with_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to turn on sim mode command Sent was: {0}".format(
                command         # {0}
            )
            raise Exception(e_msg)
        else:
            e_msg = "Successfully turned on Sim Mode"
            print (e_msg)

    #############################
    def set_sim_mode_to_off(self):
        """
        turn off sim mode this will also start the clock but doesnt send second
        command to restart clock because clock restarts when you exit sim mode
        """
        print "Turn off sim mode"
        command = "{0},{1}={2}".format(
            ActionCommands.DO,          # {0}
            opcodes.simulation_mode,    # {1}
            opcodes.false)              # {2}
        try:
            self.send_command_with_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to turn off sim mode command Sent was: {0}".format(
                command         # {0}
            )
            raise Exception(e_msg)
        else:
            e_msg = "Successfully turned off Sim Mode"
            print (e_msg)

    #################################
    def set_serial_port_timeout(self, timeout):
        """
        this is used to reset the length of time before the serial port times out
        """
        self.ser.serial_conn.setTimeout(timeout)

    #################################
    def get_date_and_time(self):
        """
        Command GET,DT
        :return:
        :rtype:
        """
        command = "{0},{1}".format(
            ActionCommands.GET,  # {0}
            opcodes.date_time  # {1}
        )
        try:
            controller_date_time = self.get_command_with_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to get the date and time form the controller command set was: {0}" \
                .format(
                    command  # {0}
                )
            raise Exception(e_msg)
        else:
            e_msg = "Successfully got the date and time from the controller"
            print (e_msg)
            return controller_date_time

    #################################
    def build_firmware_file_name(self, _firmware_version):
        """
        Build the file name for a firmware file given a specific firmware version

        Ex:
        "BL_32_V16.0.1"
        "FS_V2.0.384"

        :param _firmware_version:   The firmware version we want to search for
        :type _firmware_version:    str
        :return:    Filename for a firmware update file
        :rtype:     str
        """
        return '{0}_{1}_{2}{3}'.format(
            opcodes.baseLine,
            self.controller_type,
            str('v'),
            str(_firmware_version)
        )

    #################################
    def check_firmware_version(self, _firmware_version):
        # specify how i want to see if i have the correct version
        # where form could be bmw or josn or str

        # Get the version from the controller and set that in our object for later verification (Developer case)
        self.get_data()
        # this sets VR to what is currently on the controller so we have a way to verify
        current_firmware_on_cn = self.data.get_value_string_by_key(ControllerCommands.Attributes.FIRMWARE_VERSION)
        self.vr = current_firmware_on_cn

        # build file name str to pass into methods
        _file_name = self.build_firmware_file_name(_firmware_version=_firmware_version)

        if _firmware_version == 'latest':
            # check file and verify it really is the correct firmware version
            check_file = file_transfer_handler.get_latest_firmware_version_available(
                _cn_type=self.controller_type,
                _file_name=_file_name)
            if check_file != self.vr:
                return True

        elif _firmware_version and self.vr != _firmware_version:
            # check file and verify it really is the correct firmware version
            check_file = file_transfer_handler.get_latest_firmware_version_available(
                _cn_type=self.controller_type,
                _file_name=_file_name)
            if check_file != _firmware_version:
                e_msg = "Firmware in file doesnt match the firmware version passed in: Passed in firmware version {0}"\
                        " file contained firmware versions {1}".format(
                            _firmware_version,  # {0}
                            check_file          # {1}
                        )
                raise Exception(e_msg)
            return True
        # If the code did not trigger any of the above conditions to return False
        return False

    #################################
    def check_and_update_firmware(self, _firmware_version=None, unit_testing=False):
        """
        :param _firmware_version:
        :type _firmware_version: str | None
        :param unit_testing:
        :type unit_testing:     bool
        """
        if self.is_flowstation():
            if _firmware_version == "latest":
                # If there was no version specified we do an update from basemanager
                self.do_firmware_update(were_from=opcodes.basemanager,
                                        bm_id_number=opcodes.latest_flowstation_basemanager_firmware_id)
            else:
                # TODO we cannot update from local directory yet on a flowstation/substation/1000
                pass
        elif self.is_substation():
            if _firmware_version == "latest":
                # If there was no version specified we do a update from basemanager
                self.do_firmware_update(were_from=opcodes.basemanager,
                                        bm_id_number=opcodes.latest_substation_basemanager_firmware_id)
            else:
                # TODO we cannot update from local directory yet on a flowstation/substation/1000
                pass
        elif self.is1000():
            if _firmware_version == "latest":
                # If there was no version specified we do a update from basemanager
                self.do_firmware_update(were_from=opcodes.basemanager,
                                        bm_id_number=opcodes.latest_substation_basemanager_firmware_id)
            else:
                # TODO we cannot update from local directory yet on a flowstation/substation/1000
                pass
        elif self.is3200():
            # Check for either a new firmware version or if the user wants the latest firmware
            if self.check_firmware_version(_firmware_version=_firmware_version):
                file_name = self.build_firmware_file_name(_firmware_version=_firmware_version)
                # If we want to upgrade to the LATEST firmware
                if _firmware_version == "latest":
                    if self.vr >= opcodes.firmware_version_V16:
                        # If the firmware version on the controller is >=v16, we can do a local directory update
                        self.do_firmware_update(were_from=opcodes.local_directory,
                                                directory='common' + os.sep + 'firmware_update_files',
                                                file_name=file_name,
                                                unit_testing=unit_testing)
                    if self.vr <= opcodes.firmware_version_V16:
                        self.add_basemanager_connection_to_controller(use_basemanager=True, _fixed_ip='104.130.246.18')

                        # check current controller version against it
                        self.do_firmware_update(were_from=opcodes.basemanager,
                                                bm_id_number=opcodes.latest_3200_basemanager_firmware_id,
                                                unit_testing=unit_testing)
                # If we want to update/downgrade to a NEW firmware version
                else:
                    self.do_firmware_update(were_from=opcodes.local_directory,
                                            directory='common' + os.sep + 'firmware_update_files',
                                            file_name=file_name,
                                            unit_testing=unit_testing)

            self.verify_code_version(self.data)

    #################################
    def verify_code_version(self, _data=None):
        """
        Verifies the code version for this controller. Expects the controller's value returned and the version passed in
        as a parameter to be equal. \n
        :return:
        """
        # Get the current code version on the controller
        data = _data if _data else self.get_data()
        fw_from_cn = data.get_value_string_by_key(ControllerCommands.Attributes.FIRMWARE_VERSION)

        # Compare status versus what is on the controller
        if self.vr != fw_from_cn:
            e_msg = "Unable to verify Controller's 'Code Version'. Received: {0}, Expected: {1}".format(
                str(fw_from_cn),  # {0}
                str(self.vr)  # {1}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0} {1}'s 'Code Version': '{2}' on Controller".format(
                self.dv_type,  # {0}
                str(self.ad),  # {1}
                str(self.vr)  # {2}
            ))

    #################################
    def verify_code_version_with_boolean(self, _data=None):
        """
        Returns true or false depending on if the controller successfully updated
        :return:
        """
        # Get the current code version on the controller
        data = _data if _data else self.get_data()
        fw_from_cn = data.get_value_string_by_key(ControllerCommands.Attributes.FIRMWARE_VERSION)

        # Compare status versus what is on the controller
        if self.vr != fw_from_cn:
            return False
        else:
            return True


    #################################
    def verify_date_and_time(self, seconds_allowed=60):
        """
        Verify controller object date and time against the controller date and time

        :param seconds_allowed: The amount of seconds that we allow the controller to be off by
        :return:
        :rtype:
        """
        controller_date_and_time_str = self.get_date_and_time().get_value_string_by_key(opcodes.date_time)
        controller_date_and_time = datetime.strptime(controller_date_and_time_str, "%m/%d/%Y %H:%M:%S")
        controller_object_date_time = datetime.combine(date_mngr.controller_datetime.obj,
                                                       date_mngr.controller_datetime.time_obj.obj)

        if controller_date_and_time != controller_object_date_time:
            if abs(controller_object_date_time - controller_date_and_time) >= timedelta(days=0,
                                                                                        hours=0,
                                                                                        minutes=0,
                                                                                        seconds=seconds_allowed):
                e_msg = "The date and time of the controller didn't match the controller object:\n" \
                        "\tController Object Date Time:       \t\t'{0}'\n" \
                        "\tDate Time Received From Controller:\t\t'{1}'\n".format(
                            controller_object_date_time,  # {0} The date of the controller object
                            controller_date_and_time      # {1} The date the controller has
                        )
                raise ValueError(e_msg)
            # only print this if they were not exact
            e_msg = "############################  Date and time were not exact but were within +- 60 Seconds \n" \
                    "\tController Object Date Time:       \t\t'{0}'\n" \
                    "\tDate Time Received From Controller:\t\t'{1}'\n".format(
                        controller_object_date_time,  # {0} The date of the controller object
                        controller_date_and_time      # {1} The date the controller has
                    )
            print(e_msg)
        else:
            print "Verified controller current date and time against controller object date time:\n" \
                  "\tController Object Date Time:       \t\t'{0}'\n" \
                  "\tDate Time Received From Controller:\t\t'{1}'\n".format(
                        controller_object_date_time,  # {0} The date of the controller object
                        controller_date_and_time      # {1} The date the controller has
                    )

    #################################
    def compare_controller_date_time(self, controller_to_compare, seconds_allowed=0):
        """
        Verify controller object date and time against the controller date and time

        :param controller_to_compare: The controller we will be comparing this controller's date/time against
        :param seconds_allowed: The amount of seconds that we allow the controller to be off by
        :return:
        :rtype:
        """
        first_controller_date_and_time_str = self.get_date_and_time().get_value_string_by_key(opcodes.date_time)
        second_controller_date_and_time_str = controller_to_compare.get_date_and_time().get_value_string_by_key(
            opcodes.date_time)
        first_controller_date_and_time = datetime.strptime(first_controller_date_and_time_str, "%m/%d/%Y %H:%M:%S")
        second_controller_date_and_time = datetime.strptime(second_controller_date_and_time_str, "%m/%d/%Y %H:%M:%S")

        if first_controller_date_and_time != second_controller_date_and_time:
            if abs(first_controller_date_and_time - second_controller_date_and_time) >= timedelta(days=0,
                                                                                                  hours=0,
                                                                                                  minutes=0,
                                                                                                  seconds=seconds_allowed):
                e_msg = "The date and time of the controller didn't match the controller object:\n" \
                        "\t{0} ({1}) Date Time:       \t\t'{2}'\n" \
                        "\t{3} ({4}) Date Time:       \t\t'{5}'\n".format(
                            self.controller_type,               # {0}
                            self.ds,                            # {1}
                            first_controller_date_and_time,     # {2} The date of this controller object
                            controller_to_compare.controller_type,  # {3}
                            controller_to_compare.ds,               # {4}
                            second_controller_date_and_time         # {5} The date/time of controller comparing against
                        )
                raise ValueError(e_msg)
            # only print this if they were not exact
            e_msg = "############################  Date and time were not exact but were within +- {0} Seconds \n" \
                    "\t{1} ({2}) Date Time:       \t\t'{3}'\n" \
                    "\t{4} ({5}) Date Time:       \t\t'{6}'\n".format(
                        seconds_allowed,                    # {0}
                        self.controller_type,               # {1}
                        self.ds,                            # {2}
                        first_controller_date_and_time,     # {3} The date of this controller object
                        controller_to_compare.controller_type,  # {4}
                        controller_to_compare.ds,               # {5}
                        second_controller_date_and_time         # {6} The date/time of controller comparing against
                    )
            print(e_msg)
        else:
            print "Verified controller current date and time against controller object date time:\n" \
                  "\t{0} ({1}) Date Time:       \t\t'{2}'\n" \
                  "\t{3} ({4}) Date Time:       \t\t'{5}'\n".format(
                        self.controller_type,               # {0}
                        self.ds,                            # {1}
                        first_controller_date_and_time,     # {2} The date of this controller object
                        controller_to_compare.controller_type,  # {3}
                        controller_to_compare.ds,               # {4}
                        second_controller_date_and_time         # {5} The date/time of controller comparing against
                    )

    #################################
    def verify_description_length(self, _type, _desc):
        """
        Verify the length of the description before sending to the controller. \n
            - For a 3200: Throw an exception if the length of the description is greater than 42. \n
            - For a 10, SB, FS: Truncate the description to 31 characters if it is greater than 31. \n

        :return:    True or False depending on if variable length is okay. \n
        :rtype:     bool
        """
        length = len(_desc)
        if _type in ["FS", "SB", "10"]:
            max_chars = 31      # 1000, FlowStation, and Substation Max description length
            if length > max_chars:
                # e_msg = "Controller description '{0}' is too long. Max description length for FS, SB, or 1000: {1}, " \
                #         "Current length: {2}. Please check your user credentials file to make sure you aren't " \
                #         "including one of the above controllers in a use case they aren't meant for.".format(
                #             str(_desc),         # {0}
                #             str(max_chars),     # {1}
                #             str(length)         # {2}
                #         )
                print "{0} description is too long so it has been truncated to 31 characters. " \
                      "Old desc: {1}, new desc: {2}".format(_type, _desc, _desc[0:31])
                return _desc[0:31]

        # Compare lengths, if _desc is within length requirements, return True (valid description)
        if _type == "32":
            max_chars = 42      # 3200 Max description length
            if length > max_chars:
                e_msg = "Controller description '{0}' is too long. Max description length for 3200: {1}, " \
                        "Current length: {2}".format(
                            str(_desc),         # {0}
                            str(max_chars),     # {1}
                            str(length)         # {2}
                        )
                raise ValueError(e_msg)
        return _desc

    #################################
    def verify_serial_number(self, _get_data=False):
        """
        Verifies the Serial Number set on the controller for the specified device type. \n
        :return:

        """
        sn_on_cn = self.data.get_value_string_by_key(DeviceCommands.Attributes.SERIAL_NUMBER)
        # add this code because a controller can have either 3x or 3k and doesnt matter what we set on the controller
        if self.is3200():
            # Parse everything besides the last two characters, this parsed string remains constant the entire method
            # this return the first 2 characters
            sn_on_cn_first_half = sn_on_cn[:2]
            # Gets the int portion of the serial number to increment (the last two digits)
            # this return the last 5 characters
            sn_on_cn_int_half = sn_on_cn[2:]
            if not sn_on_cn_first_half.startswith(("3K", "3X")):
                e_msg = "Unable verify {0}: {1}'s 'Serial Number'. 3200 controller must start with a '3K' or a '3X' " \
                        "Received: {2}, Expected: {3}".format(
                            self.dv_type,  # {0}
                            str(self.ad),  # {1}
                            str(sn_on_cn),  # {2}
                            str(self.sn)  # {3}
                        )
                raise ValueError(e_msg)
            # Parse everything besides the last two integers, this parsed string remains constant the entire method
            # Gets the int portion of the serial number to increment (the last two digits)
            # this return the last 2 characters
            sn_int_half = self.sn[2:]
        else:
            # this is used for the 1000, substation and flow station
            sn_int_half = self.sn
            sn_on_cn_int_half = sn_on_cn
        # Compare Serial Numbers
        if sn_int_half != sn_on_cn_int_half:
            e_msg = "Unable verify {0}: {1}'s 'Serial Number'. Received: {2}, Expected: {3}".format(
                self.dv_type,  # {0}
                str(self.ad),  # {1}
                str(sn_on_cn),  # {2}
                str(self.sn)  # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0} {1}'s Serial Number: '{2}' on controller".format(
                self.dv_type,  # {0}
                str(self.ad),  # {1}
                str(self.sn)  # {2}
            ))

    #################################
    def verify_who_i_am(self, _expected_status=None):
        """
        Verifier wrapper which verifies all attributes for this 'Controller'. \n
        :param _expected_status:    Expected status to compare against. \n
        :type _expected_status:     str \n
        :return:
        """
        # Get all information about the device from the controller.
        data = self.get_data()

        # Verify base attributes
        # self.verify_date_and_time()  # verify date time object against current date time of the controller
        self.verify_description()

        self.verify_serial_number()
        self.verify_latitude()
        self.verify_longitude()

        if _expected_status is not None:
            self.verify_status(_expected_status=_expected_status)

        # Verify 'Controller' specific attributes
        self.verify_code_version(_data=data)

    #############################
    def set_controller_to_run(self,command=''):
        """
        the controller does most of its internal processing when the run button is selected
        in order to get the controller status to update after certain events are trigger the run button needs to be
        selected and then the clock needs to increment one second
        Items like the search or test function will set the controller to a paused state and not allow the zones to be
        added to the program until the run key is selected and the controller process the command
        """
        print "Set Controller To Run"
        # select run button
        try:
            self.ser.send_and_wait_for_reply(command)
            self.do_increment_clock(seconds=1)
            print "-------------------------------------------------------"
            print "       | - Successfully Set Controller to Run.  - |    "
            print "-------------------------------------------------------"
        except Exception as e:
            e_msg = "Setting Controller to Run Position Command Failed: " + str(e.message)
            raise Exception(e_msg)

    #############################
    def set_controller_to_off(self, command=''):
        """
        3200 specific Turn controller to off to stop all Programs running
        """
        print "Set Controller To Off"

        try:
            self.ser.send_and_wait_for_reply(command)
            self.do_increment_clock(seconds=1)
            print "-------------------------------------------------------"
            print "       | - Successfully Set Controller to off.  - |    "
            print "-------------------------------------------------------"
        except Exception as e:
            e_msg = "Setting Controller to Off Position Command Failed: " + str(e.message)
            raise Exception(e_msg)

    #################################
    def add_basemanager_connection_to_controller(self, use_basemanager=True, _url='', _fixed_ip=''):
        """
        Add a BaseManager connection to the controller. \n
            - first set the controller to not connect to BaseManager
            - than set the ip address to all zeros so the controller is in a known state

        :param _url:   The DNS url of the baseManager server you want to connect to. \n
        :type _url:    str \n

        :param _fixed_ip:   The fixed ip of the baseManager server you want to connect to. \n
        :type _fixed_ip:    str \n
        """
        # create program object here when this is called
        object_factory.create_basemanager_connection_object(controller=self, url=_url, fixed_ip=_fixed_ip)
        # first send dont connect to BM
        self.basemanager_connection[1].set_dont_connect_to_bm()
        # than blank out all values
        self.basemanager_connection[1].set_default_values()
        if use_basemanager:
            # Check if the fixed IP is set, and then if it is a non-default zeroed out IP
            if _fixed_ip and _fixed_ip != '0.0.0.0':
                self.basemanager_connection[1].set_ai_for_cn(ip_address=_fixed_ip)
            elif _url:
                self.basemanager_connection[1].set_ai_for_cn(ip_address=_url)

            if self.vr >= opcodes.firmware_version_V16:
                self.basemanager_connection[1].set_controller_to_connect_using_ethernet()
            else:
                self.basemanager_connection[1].connect_v12_cn_to_ethernet()

    def verify_controller_connected_to_bm(self):
        try:
            # Need to get the current controller BM configuration for verification.
            controller_reply = self.ser.get_and_wait_for_reply("GET,BM")
        except AssertionError as ae:
            exception_message = "Exception occurred after sending command to controller: 'GET,BM'.\n" + \
                                "Controller Response: [" + str(ae.message) + "]"
            raise AssertionError(exception_message)

        # Get the connected status
        status_from_cn = controller_reply.get_value_string_by_key(opcodes.status_code)

        # are we connected?
        if status_from_cn != opcodes.connect_to_basemanager:
            return False
        else:
            print "|- Controller is connected to Basemanager.  - |"
            return True

    ################################
    def do_firmware_update(self, were_from, bm_id_number=None, directory=None, file_name=None,
                           maxlinelen=0, file_type='', unit_testing=False):
        """
        :param were_from:       this is the location of the firmware to update either from BaseManager or USB Storage
                                device (BaseManager, USB, local directory)
        :type were_from:        str
        :param bm_id_number:    this is the database id from the server where the firmware is located
        :type bm_id_number:     int
        :param directory:       _directory = "common/firmware_update_files_files/test.zip"
        :type directory:        str
        :type bm_id_number:     int
        :param file_name:       this the the file name located on the USB
        :type file_name:        str
        :param maxlinelen:      The length of each packet (besides last one) from splitting up the firmware file we send
                                to the 3200.
        :type maxlinelen:       int
        :param file_type:       The file type that your firmware file is in. Usually '.zip'.
        :type file_type:        str
        :param unit_testing:    Parameter to make unit testing faster by ignoring sleeps
        :type unit_testing:     bool
        """
        # Fail Fast if user passes in an invalid location of the firmware update
        if were_from != opcodes.basemanager and were_from \
                != opcodes.usb_flash_storage and were_from \
                != opcodes.local_directory:
            e_msg = "where from is an incorrect location. Accepted locations are {0} or {1}.".format(
                opcodes.basemanager, opcodes.usb_flash_storage)
            raise Exception(e_msg)

        # 3200 must be a.zip file to do a firmware update
        file_type = file_type
        # this is the number of bytes the controller can handle in a single packet
        maxlinelen = maxlinelen

        # TODO find a way to update the firmware variable in the controller object
        if were_from == opcodes.basemanager:
            # if the controller is not connected to BaseManager throw an error
            if not self.basemanager_connection[1].verify_cn_connected_to_bm():
                e_msg = "Controller Not Connected to BaseManager unable to do firmware update"
                raise Exception(e_msg)
            # build command to send to controller to ask basemanager for firmware update
            command = "{0},{1}={2},{3}={4}".format(
                ActionCommands.DO,          # {0}
                opcodes.update_firmware,    # {1}
                opcodes.basemanager,        # {2}
                opcodes.file_name,          # {3}
                str(bm_id_number))          # {3)
            try:
                if self.vr <= '16.0.583':
                    self.ser.sendln(tosend=command)
                else:
                    self.ser.send_and_wait_for_reply(tosend=command)
                    self.wait_for_controller_after_reboot()
            except Exception as e:
                e_msg = "Unable to update firmware from BaseManager: " + e.message
                raise Exception(e_msg)

        if were_from == opcodes.usb_flash_storage:
            # verify controller has a USB inserted this will throw an error if there isn't one inserted
            # self.verify_usb_is_inserted()
            # build command to send to controller to ask for usb firmware update
            command = "{0},{1}={2},{3}={4}".format(
                ActionCommands.DO,                  # {0}
                opcodes.update_firmware,            # {1}
                opcodes.usb_flash_storage,          # {2}
                opcodes.file_name,                  # {3}
                file_name)                          # {4)
            try:
                self.ser.send_and_wait_for_reply(tosend=command)
                self.wait_for_controller_after_reboot()
            except Exception as e:
                e_msg = "Unable to update firmware from USB flash Storage: " + e.message
                raise Exception(e_msg)

        if were_from == opcodes.local_directory:
            if directory is None or file_name is None:
                e_msg = "if useing a directory than the directory, file name must be passed in: "
                raise Exception(e_msg)

            list_of_commands = file_transfer_handler.packet_to_send_to_cn(_cn_type=self.controller_type,
                                                                          _directory=directory,
                                                                          _file_name=file_name,
                                                                          _file_type=file_type,
                                                                          _maxlinelen=maxlinelen)
            # need to turn echo off because the data is to large for the reply command
            self.turn_off_echo()
            last_packet = len(list_of_commands)-1
            print list_of_commands[last_packet]
            for index, packet in enumerate(list_of_commands):
                packet_to_send = list_of_commands[index]
                print "index number: " + str(index)

                command = packet_to_send
                try:
                    self.ser.send_and_wait_for_reply(tosend=command)
                except Exception as e:
                    e_msg = "Unable to update firmware from file in directory: " + e.message
                    raise Exception(e_msg)
                if index == last_packet:
                    self.wait_for_controller_after_reboot()
            # self.stop_clock()
            # self.turn_on_echo()

        # Updates the controller object's firmware version variable to be the same as the the firmware version.
        self.set_sim_mode_to_on()
        self.stop_clock()
        # sleep to get the controller time to send the back up file
        if not unit_testing:
            for sec in range(1, 31):
                print "Sleeping for {0} Seconds out of 30 Seconds............".format(sec)
                time.sleep(1)

        self.get_data()
        self.vr = self.data.get_value_string_by_key(opcodes.firmware_version)

        # Print success message
        print("Successfully updated firmware from '{0}'.".format(were_from))

    #############################
    def wait_for_controller_after_reboot(self, _version_with_response_after_reboot=None, unit_testing=False):
        """
        Waits for the controller (3200 only) to come back to life.
        :return:
        """
        # Added to make unit tests faster.
        if unit_testing:
            # controller needs time to fully wakes up before more commands are sent to it
            seconds = 5
        else:
            if self.is3200():
                seconds = 30
            else:
                seconds = 15

        first_character_of_port_address = self.get_controller_port_address()

        if first_character_of_port_address == '1':
            time.sleep(seconds)
        elif first_character_of_port_address == '2':
            time.sleep(seconds)
            self.reset_ethernet_port()

        if compare_versions(self.vr, _version_with_response_after_reboot) <= 0:
            # Wait for the 'OK' response when the controller comes back alive
            result = self.ser.wait(waitfors=['OK'], _maxchars=13800)
            if result == 0:
                self.ser.working()
                print "Successfully waited for controller after reboot."
            else:
                raise ValueError("Reconnection to controller failed")

        print "Successfully waited for controller to wake up all the way."

    #############################
    def do_reboot_controller(self, _version_with_ok_response=None, _version_with_response_after_reboot=None):
        """
        reboot controller than wait for 30 seconds for the controller to wake back up
        flush the input buffer
        listen for the controller to tell you its serial port is back up and running
            wait for the controller to be full back up and running before  a new command is sent
            the controller will send a "ok /n/r" when is ready to start taking command
            The 1000 doesnt need to do this
        :return: 0 if timeout, position of string in list (starting from 1) if the string is received,
                 -1 if we received more than the max # of characters (currently 500) without getting a
                 match.
        """
        # If we have not requested data from the controller yet, we will now
        if not self.data:
            self.get_data()

        # Get the version before the FlowStation reboots so we know how to communicate with it when it comes up
        self.vr = self.data.get_value_string_by_key(opcodes.firmware_version)

        # set the serial port time out so we have time to reboot
        self.set_serial_port_timeout(timeout=480)

        command = '{0},{1}={2}'.format(ActionCommands.DO, ControllerCommands.REBOOT, opcodes.true)

        if compare_versions(self.vr, _version_with_ok_response) <= 0:
            # Will return an 'OK' after we send a reboot command to controller
            self.ser.send_and_wait_for_reply(command)
            # Wait for the FlowStation to come back up, it will respond with an 'OK' in versions 2.1.2 and up
        else:
            self.ser.sendln(tosend=command)

        self.wait_for_controller_after_reboot(_version_with_response_after_reboot=_version_with_response_after_reboot)

        # Reset the serial port timeout to a reasonable amount of time for a controller response
        self.set_serial_port_timeout(timeout=120)

        self.set_sim_mode_to_on()
        self.stop_clock()

        self.set_date_and_time(_date=self.date_mngr.controller_datetime.date_string_for_controller(),
                               _time=self.date_mngr.controller_datetime.time_string_for_controller())

    #############################
    def get_controller_port_address(self):
        """
        if the port number starts with a 2 than most likely the system is using an ethernet port not a serial
        use this method to get the first character of the ethernet port being used
        :return:
        :rtype:
        examples:
            "socket://10.11.13.110:21000"
            "socket://10.11.12.241:10001"
        """
        ethernet_ipa_and_port = self.ser.s_port
        port_str = ethernet_ipa_and_port[22:]
        first_character_of_port_address = port_str[:1]
        return first_character_of_port_address

    #############################
    def reset_ethernet_port(self):
        """
        if using an ethernet port instead of serial and you need to reboot the controller use this method to
        re-establish the ethernet connection
        """
        self.ser.init_ser_connection()
        self.ser.check_if_cn_connected()

    #################################
    def do_increment_clock(self, hours=0, minutes=0, seconds=0, update_date_mngr=True, clock_format='%H:%M:%S'):
        """
        compare each value of hours, minutes, seconds \n
        this allows to send in one of the parameter to increment the clock in the flowstation \n
        the clock must be stopped in order it increment it
        :param hours:
        :type hours:                int
        :param minutes:
        :type minutes:              int
        :param seconds:
        :type seconds:              int
        :param update_date_mngr:
        :type update_date_mngr:     bool
        :return:            ok from controller
        :rtype:             ok is a str
        """
        print('Increment Clock ' + str(hours) + ' Hours ' + str(minutes) + ' Minutes')
        if hours == 0 and minutes == 0 and seconds == 0:
            e_msg = "Failed trying to increment clock. Invalid type passed in for hours. Received: {0}, " \
                    "Expected: int ".format(str(hours))
            raise ValueError(e_msg)
        if hours != 0:
            if not isinstance(hours, int):
                e_msg = "Failed trying to increment clock. Invalid type passed in for hours. Received: {0}, " \
                        "Expected: int ".format(str(hours))
                raise TypeError(e_msg)
            elif hours not in range(24):
                e_msg = "Failed trying to increment clock. Invalid range for hours. Expected a number between " \
                        "0 - 24, Received: {0},".format(str(hours))
                raise ValueError(e_msg)
            else:
                hours = str(hours).zfill(2)
        else:
            hours = str(hours).zfill(2)
        if minutes != 0:
            if not isinstance(minutes, int):
                e_msg = "Failed trying to increment clock. Invalid type passed in for minutes. Received: {0}, " \
                        "Expected: int ".format(str(minutes))
                raise TypeError(e_msg)
            elif minutes not in range(60):
                e_msg = "Failed trying to increment clock. Invalid range for minutes. Expected a number between " \
                        "0 - 60, Received: {0},".format(str(minutes))
                raise ValueError(e_msg)
            else:
                minutes = str(minutes).zfill(2)
        else:
            minutes = str(minutes).zfill(2)
        if seconds != 0:
            if not isinstance(seconds, int):
                e_msg = "Failed trying to increment clock. Invalid type passed in for seconds. Received: {0}, " \
                        "Expected: int ".format(str(seconds))
                raise TypeError(e_msg)
            elif seconds not in range(60):
                e_msg = "Failed trying to increment clock. Invalid range for seconds. Expected a number between " \
                        "0 - 60, Received: {0},".format(str(seconds))
                raise ValueError(e_msg)
            else:
                seconds = str(seconds).zfill(2)
        else:
            seconds = str(seconds).zfill(2)
        if clock_format == '%H:%M':
            clock = str(hours) + ":" + str(minutes)
        else:
            clock = str(hours) + ":" + str(minutes) + ":" + str(seconds)
        datetime.strptime(clock, str(clock_format))

        # `update_date_mngr` flag is toggled when we want to control when date_mngr is incremented.
        if update_date_mngr:
            try:
                date_mngr.controller_datetime.increment_date_time(hours=int(hours),
                                                                  minutes=int(minutes))
                if date_mngr.curr_day:
                    date_mngr.curr_day.increment_date_time(hours=int(hours),
                                                           minutes=int(minutes),
                                                           seconds=int(seconds))
            except ValueError:
                e_msg = "Failed trying to increment clock. date time values are incorrect. Received: {0}, " \
                        "Expected: int ".format(str(hours))
                raise ValueError(e_msg)

        self.ser.send_and_wait_for_reply('DO,CK,TM=' + str(clock))
