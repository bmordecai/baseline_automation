import platform
import serial
import serial.urlhandler.protocol_socket as eth_serial
import time
import sys
import status_parser
import common.variables.common as common_vars
from common.logging_handler import log_handler
from common.date_package.date_resource import date_mngr

__author__ = 'bens'


class Ser(object):
    """
    Serial Object \n

    This object contains all methods needed for communication over the serial port. \n
    """
    # TODO need an error if the comport cant be opened
    controller_type = ''
    unit_testing = False

    #############################
    def __init__(self, comport=None, eth_port=None):
        self.serial_conn = None
        self.c_port = comport
        self.s_port = eth_port
        self.DEFAULT_TIMEOUT = common_vars.DEFAULT_TIMEOUT
        # self.init_ser_connection()
        # self.serial_conn = None
        
        # Reference to controller/flow station instance used when logging the
        # incoming data from get_data()
        self.controller_reference = None

    #############################
    def close_connection(self):
        """
        Closes the current serial connection. \n
        :return:
        """
        self.serial_conn.close()

    #############################
    def open_connection(self):
        """
        opens the current serial connection. \n
        :return:
        """
        # if port is open close port and reopen to reset port
        if self.serial_conn.isOpen():
            self.serial_conn.close()
        self.serial_conn.open()

    #############################
    def init_ser_connection(self, _baud_rate=115200):
        """
        Sets up a serial connection to a controller with specified port_address passed in.
        If running test on Mac, port input has different format, which is set accordingly.
        Need to pass in baud rate so different devices can connect
        :return ser     Serial connection object for use by calling test.
        """
        baud_rate = _baud_rate
        if self.c_port == 'None' or self.c_port is None:
            comport = self.s_port

            # Attempt to establish Computer to Controller communication through ethernet port
            self.DEFAULT_TIMEOUT = 10
            self.serial_conn = eth_serial.Serial(comport, baud_rate, timeout=self.DEFAULT_TIMEOUT)
            # self.check_if_cn_connected()
            self.serial_conn.setTimeout(120)

        else:
            # For non-mac machines
            comport = 'COM' + str(self.c_port)
            port_number = '/dev/tty.usbserial'

            # For ben's machine (apple)
            if str(platform.system()) == 'Darwin':
                comport = port_number

            self.DEFAULT_TIMEOUT = 30
            # Attempt to establish Computer to Controller communication through serial
            self.serial_conn = serial.Serial(comport, baud_rate, timeout=self.DEFAULT_TIMEOUT)
            # self.check_if_cn_connected()
            self.serial_conn.setTimeout(120)
        self.open_connection()

    #############################
    def check_if_cn_connected(self):
        """
        this method sends a command to the controller looking for a reply of "OK" if it doesnt get an "OK"
        it raises an error

        :return: "ok"
        :rtype:
        """
        try:
            self.send_and_wait_for_reply('DO,EC=TR')
        except Exception:
            raise Exception("Unable to communicate with controller during serial port initialization")

    #############################
    def get_and_wait_for_reply(self, tosend, called_by_get_data=False):  # this is used for programming
        """
        the buffer if the controller cant get over ran
        if the controller returns a 'BC'
        catch exceptions and try again to verify that it is a true BC and not just a full buffer
        """
        number_of_retries = 3
        retries = 0
        while True:
            try:
                self.sendln(tosend)
                
                if not log_handler.is_log_serial_port_io_enabled():
                    print tosend
                
                received = self.get_key_value_pairs(tosend)

                msg = '\n-------------------------------------\n' + \
                      'TO:       [ {0} ]\n'.format(self.s_port) + \
                      'COMMAND:  [ {0} ]\n'.format(tosend) + \
                      'RECEIVED: [ {0} ]\n'.format(received['result_string']) + \
                      '-------------------------------------\n'

                # print(msg)
                log_handler.log_serial_io(message=msg)
                
                if called_by_get_data:
                    
                    # If we are trying to log get data and date_mgr hasn't been
                    # updated with a date/time from controller yet, then set current
                    # date/time to match computer so that the code below is happy.
                    # Setting controller date/time will overwrite this value so no
                    # need to worry.
                    if date_mngr.curr_day.obj is None:
                        date_mngr.set_current_date_to_match_computer()
                    
                    msg_for_get_data_file = ''
                    # Get date string from controller
                    msg_for_get_data_file += date_mngr.curr_day.formatted_date_string(format_string='%m/%d/%y') + ','
                    # Get time string from controller
                    msg_for_get_data_file += date_mngr.curr_day.formatted_time_string(format_string='%I:%M:%S %p') + ','
                    # Add controller identification
                    msg_for_get_data_file += str(self.controller_reference.ad) + '-' + str(self.controller_reference.mac) + ','
                    # Append received msg from controller
                    msg_for_get_data_file += received['result_string'] + ','
                    # Log it
                    log_handler.log_incoming_get_data_response_as_csv(message=msg_for_get_data_file)
                
                return received['key_values']
            except AssertionError, ae:
                if ae.message == 'BC response received from controller':
                    print "number of retries for last method = " + str(retries+1)
                    retries += 1
                    
                    fail_msg = '\n-------------------------------------\n' + \
                               'FROM: [ {0} ]\n'.format(self.s_port) + \
                               'RECEIVED: [ {0} ]\n'.format(ae.message) + \
                               '-------------------------------------\n'
                    log_handler.log_serial_io(message=fail_msg)

                    # If we aren't unit testing -> Only the unit test change this attribute to True
                    if not self.unit_testing:
                        time.sleep(1)

                    if retries >= number_of_retries:
                        raise
                else:
                    raise

    #############################
    def get_and_wait_for_reply_with_reply_string(self, tosend, called_by_get_data=False):  # this is used for programming
        """
        Has the same behavior as get_and_wait_for_reply until the return statement. This method returns a complete GET
        string that the controller replies to us with.
        """
        number_of_retries = 3
        retries = 0
        while True:
            try:
                self.sendln(tosend)

                if not log_handler.is_log_serial_port_io_enabled():
                    print tosend

                received = self.get_key_value_pairs(tosend)

                msg = '\n-------------------------------------\n' + \
                      'TO:       [ {0} ]\n'.format(self.s_port) + \
                      'COMMAND:  [ {0} ]\n'.format(tosend) + \
                      'RECEIVED: [ {0} ]\n'.format(received['result_string']) + \
                      '-------------------------------------\n'

                # print(msg)
                log_handler.log_serial_io(message=msg)

                if called_by_get_data:
                    msg_for_get_data_file = ''
                    # Get date string from controller
                    msg_for_get_data_file += date_mngr.curr_day.formatted_date_string(format_string='%m/%d/%y') + ','
                    # Get time string from controller
                    msg_for_get_data_file += date_mngr.curr_day.formatted_time_string(format_string='%I:%M:%S %p') + ','
                    # Add controller identification
                    msg_for_get_data_file += str(self.controller_reference.ad) + '-' + str(self.controller_reference.mac) + ','
                    # Append received msg from controller
                    msg_for_get_data_file += received['result_string'] + ','
                    # Log it
                    log_handler.log_incoming_get_data_response_as_csv(message=msg_for_get_data_file)
                # Either return the string the controller replies with or key/value objects that can be operated on
                if return_with_reply_string:
                    return received
                else:
                    return received['key_values']
            except AssertionError, ae:
                if ae.message == 'BC response received from controller':
                    print "number of retries for last method = " + str(retries+1)
                    retries += 1

                    fail_msg = '\n-------------------------------------\n' + \
                               'FROM: [ {0} ]\n'.format(self.s_port) + \
                               'RECEIVED: [ {0} ]\n'.format(ae.message) + \
                               '-------------------------------------\n'
                    log_handler.log_serial_io(message=fail_msg)

                    # If we aren't unit testing -> Only the unit test change this attribute to True
                    if not self.unit_testing:
                        time.sleep(1)

                    if retries >= number_of_retries:
                        raise
                else:
                    raise

    #############################
    def get_key_value_pairs(self, ignore=None):
        """

        :param ignore:  String or character to ignore while receiving input. \n
        :type ignore:   str \n
        :return:        Key Value string received over serial connection \n
        :rtype:         dict(str, str | status_parser.KeyValues)
        """
        result = self.recvln(ignore)  # (['SS=DN,VA=0.23,VV=28.7,VT=1.7', 'ER', 'BC'])
        if result is None:
            raise AssertionError("No response received from controller")
        elif result == 'BC':
            raise AssertionError("BC response received from controller")
        elif result == 'ER':
            raise AssertionError("ER response received from controller")
        elif result == 'NM':
            raise AssertionError("NM No Message Found")
        else:
            self.working()
        return {'result_string': result, 'key_values': status_parser.KeyValues(result)}

    #############################
    def sendln(self, tosend):
        """

        :param tosend:
        :return:
        """
        self.serial_conn.write(tosend + '\r\n')

    #############################
    def waitln(self, waitfor):
        """

        :param waitfor:
        :return:
        """
        while True:
            line = self.serial_conn.readline()
            if line == waitfor:
                return

    #############################
    def recvln(self, ignore=None):
        """

        :param ignore:  Default to ignoring spaces. \n
        :return:
        """
        maxchars = 500
        numchars = 0
        instring = ''
        while True:
            # Get a character from the serial port, append to running string
            abyte = self.serial_conn.read()
            if len(abyte) != 0:
                if abyte == '\r' or abyte == '\n':
                    abyte = ''
                    if instring != '':
                        print
                        return instring
                sys.stdout.write(abyte)
                numchars += 1
                instring = instring + abyte

                # Scan the running string to see if any of the desired strings are in it
                if ignore is not None:
                    found = instring.find(ignore)
                    if found != -1:
                        print
                        instring = ''
                    
                if numchars >= maxchars:
                    print "Didn't get what we expected: " + instring
                    break
            else:
                return None

        return None

    #############################
    def wait(self, waitfors, _maxchars=500):
        """
        :param waitfors:    list of strings to look for from the serial port
        :type waitfors:     list \n
        :return: 0 if timeout, position of string in list (starting from 1) if the string is received,
                 -1 if we received more than the max # of characters (currently 500) without getting a
                 match.
        """
        maxchars = _maxchars
        numchars = 0
        instring = ''
        while True:

            # Get a character from the serial port, append to running string
            abyte = self.serial_conn.read()
            sys.stdout.write(abyte)

            if len(abyte) != 0:
                numchars += 1
                instring = instring + abyte

                # Scan the running string to see if any of the desired strings are in it
                for i, waitfor in enumerate(waitfors):
                    found = instring.find(waitfor)
                    # If one of the strings in the list is found, return the position (starting from 1) in the list,
                    # other go back and grab another character from the serial port
                    if found != -1:

                        msg = '\n-------------------------------------\n' + \
                              'FROM:     [ {0} ]\n'.format(self.s_port) + \
                              'REPLY:    [ {0} ]\n'.format(waitfor.strip('\r\n')) + \
                              '-------------------------------------\n'

                        # print(msg)
                        log_handler.log_serial_io(message=msg)
                        
                        # print ''

                        return i

                if numchars >= maxchars:
                    # the empty print statement forces console output to a new line instead of appending onto the
                    # same line as 'sys.stdout.write'
                    print ""
                    print "Didn't get what we expected: {0} after max chars read: {1}, received: {2}".format(
                        waitfors,
                        maxchars,
                        instring
                    )
                    break

            else:
                return -1

        # if reaching this point, we have reached max characters
        return -2

    #############################
    def send_and_wait_for_reply(self, tosend, _maxchars=500):
        """
        the buffer if the controller cant get over ran
        if the controller returns a 'BC'
        catch exceptions and try again to verify that it is a true BC and not just a full buffer
        """
        msg = '\n-------------------------------------\n' + \
              'TO:      [ {0} ]\n'.format(self.s_port) + \
              'COMMAND: [ {0} ]\n'.format(tosend) + \
              '-------------------------------------\n'
        
        number_of_retries = 3
        retries = 0
        while True:
            try:
                self.sendln(tosend)
                # print(msg)
                log_handler.log_serial_io(message=msg)
                
                if not log_handler.is_log_serial_port_io_enabled():
                    print tosend
                    
                self.controller_reply(_maxchars=_maxchars)
                print ''
                break
            except AssertionError as ae:
                if ae.message == 'BC response received from controller':
                    print "number of retries for last method = " + str(retries + 1)
                    retries += 1
                    
                    fail_msg = '\n-------------------------------------\n' + \
                               'FROM:     [ {0} ]\n'.format(self.s_port) + \
                               'RECEIVED: [ {0} ]\n'.format(ae.message) + \
                               '-------------------------------------\n'
                    log_handler.log_serial_io(message=fail_msg)

                    # If we aren't unit testing -> Only the unit test change this attribute to True
                    if not self.unit_testing:
                        time.sleep(1)

                    if retries >= number_of_retries:
                        raise
                else:
                    raise

    #############################
    def controller_reply(self, _maxchars=500):
        """

        :return:
        :rtype:
        """
        result = self.wait(['\nOK', '\nER', '\nBC', '\nNM', 'OK\r\n', 'OK\n', 'NM\r\n', 'BC\r\n'], _maxchars=_maxchars)

        if result == 0:
            self.working()

        # Case 1: Error
        elif result == 1:
            raise AssertionError("ER response received from controller")

        # Case 2: Bad Command
        elif result == 2:
            raise AssertionError("BC response received from controller")

        # Case 3: No Message
        elif result == 3:
            raise AssertionError("NM No Message Found")

        # Case 4: No Response
        elif result == -1:
            raise AssertionError("No response received from controller")

        # Case 5: Max Characters Reached
        elif result == -2:
            raise AssertionError("Max characters reached while receiving response from controller")

    #############################
    def working(self):
        """

        :return:
        """
        # time.sleep(1)
        # print('Working')
        pass

    #############################
    def flush_serial(self):
        """
        :return:
        :rtype:
        """
        # TODO need a unit test for this method
        if self.serial_conn is None:
            raise ValueError("Error in attempting to flush serial: No serial connection was found")
        else:
            self.serial_conn.flushOutput()

        # If we aren't unit testing -> Only the unit test change this attribute to True
        if not self.unit_testing:
            time.sleep(1)  # delay giving time to clean the buffer

    # #############################
    # def bu_wait(self):
    #     """
    #     :return: new_packet in a 14 byte array
    #     :rtype: bu_packet.BUSerialPacket
    #     """
    #     new_packet = bu_packet.BUSerialPacket()
    #     while True:
    #         self.serial_conn.setTimeout(1)
    #         abyte = self.serial_conn.read(size=14)
    #         if abyte:
    #             new_packet.unpack(reply_string=abyte)
    #         else:
    #             break
    #
    #     self.serial_conn.setTimeout(120)
    #     return new_packet

    # # TODO: Need to figure out if checking against a STATUS_OK is good  enough for retry loop or if we need another way
    # def bu_send_and_wait_for_reply(self, new_packet):
    #     """
    #     the buffer if the controller cant get over ran
    #     if the controller returns a 'BC'
    #     catch exceptions and try again to verify that it is a true BC and not just a full buffer
    #     :return: A BU Serial Packet received
    #     :rtype: bu_packet.BUSerialPacket
    #     """
    #         tosend = new_packet.pack()
    #         number_of_retries = 3
    #         retries = 1
    #         while True:
    #             # try:
    #
    #             # print "Sending: " + new_packet.text_to_csv()
    #             self.sendln(tosend)
    #             returned_packet = self.bu_wait()
    #             # print "Received: " + returned_packet.text_to_csv()
    #
    #             if returned_packet.status == bu_imports.BaseErrors.STATUS_OK:
    #                 # sleep needed so we can't send too many packets at once
    #                 if not self.unit_testing:
    #                     time.sleep(1)
    #                 return returned_packet
    #             else:
    #                 if retries <= number_of_retries:
    #                     print "Retrying to send packet to Base Unit. Retry #" + str(retries + 1)
    #                     retries += 1
    #
    #                     # If we aren't unit testing -> Only the unit test change this attribute to True
    #                     if not self.unit_testing:
    #                         time.sleep(1)
    #                 else:
    #                     raise ValueError("Error occurred waiting for reply from Base Unit: Error Code: {0}".format(
    #                         returned_packet.status
    #                     ))
    #             # except Exception:
    #             #     if retries >= number_of_retries:
    #             #         print "number of retries for last method = " + str(retries + 1)
    #             #         retries += 1
    #             #         time.sleep(1)
    #             #     else:
    #             #         raise
    #
    # def bu_send_with_no_reply(self, new_packet):
    #         """
    #         this sends the packet but there is know reply from the baseunit
    #         """
    #         tosend = new_packet.pack()
    #         number_of_retries = 3
    #         retries = 1
    #         sent = False
    #         while not sent:
    #             try:
    #                 # print new_packet.text_to_csv()
    #                 self.sendln(tosend)
    #                 sent = True
    #             except Exception:
    #                 if retries < number_of_retries:
    #                     print "number of retries for last method = " + str(retries)
    #                     retries += 1
    #                     if not self.unit_testing:
    #                         time.sleep(1)
    #                 else:
    #                     raise
