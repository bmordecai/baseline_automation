
"""
This class will handle data group operations and verifications
"""
import common.imports.opcodes as opcodes
import common.objects.base_classes.verify_te_packet as verify_te_packet
from threading import Thread, Event
import requests
import os
import time
from collections import deque
import hashlib


class DataGroupPackets(object):

    def __init__(self, controller, test_user_name, test_password, session_id=''):
        """
        :param controller:      controller which this DataGroupPackets objects is attached to
        :type                   common.objects.controllers.bl_32.BaseStation3200|
                                common.objects.controllers.bl_fs.FlowStation  \n
        :param test_user_name: test user name for connecting to Base Manager \n
        :type  test_user_name: str \n
        :param test_password: test password for connecting to Base Manager \n
        :type  test_password: str \n
        """
        self.controller = controller
        self.user_name = test_user_name
        self.password = hashlib.md5(test_password).hexdigest()
        self.log_file = None
        self.session_id = session_id
        self.te_packets = {}
        self.read_data_thread = None
        self.queue = deque()

        self.log_event = Event()
        self.wait_time = 2
        # Create thread for reading data from the second com port. Do not start yet.
        self.read_data_thread = Thread(target=self.read_from_com_port, args=(self.log_event, self.queue))

        # Create the file_path to the log_file for the current controller
        # path = os.path.abspath(__file__).partition('common')
        # self.log_file_path = os.path.join(path[0], 'common/log_files/' + 'controller_' + self.controller.mac + '_log.txt')
        if self.user_name != '' and self.password != '':
            if not self.session_id:
                payload = {"username":  self.user_name, "password": self.password}
                r = requests.post(url='https://p3.basemanager.net/baseservice2/login', json=payload)
                response_dictionary = r.json()
                try:
                    self.session_id = response_dictionary['sessionId']
                except KeyError:
                    e_msg = "Login authentication failed. Invalid username and password. Username: " + self.user_name + " "
                    e_msg += "Password " + self.password
            self.read_data_thread.setDaemon(True)
            self.read_data_thread.start()

    #################################
    def start_packet_logging(self):
        """
        This method opens the log file in read/write mode, sets the log_event to true so that the log_thread
        records data, and starts the logging thread.
        """
        # self.log_file = open(self.log_file_path, 'w+')
        # self.log_event.set()
        # set daemon to true so that the thread terminates when the main thread terminates


    #################################
    def resume_packet_logging(self):
        """
        This method resumes logging, deletes the data in the log_file, places the file pointer at the start of the file,
        and sets the log_event flag to true so the read_data_thread continues recording data
        """
        # self.log_file.truncate(0)
        # self.log_file.seek(0)
        self.log_event.set()
        self.te_packets = {}

    #################################
    def stop_packet_logging(self):
        """
        This method waits for the given wait time so that controller can finish sending data. It then clears the
        log_event flag so that the read_data_thread stops recording data.
        """
        time.sleep(self.wait_time)
        self.log_event.clear()

    #################################
    def parse_packet_log_file(self):
        """
        This method removes data from the queue and writes it to the log file which is then parsed and TE packets are
         extracted and placed in the te_packets dictionary if they are valid. Valid TE Packets are converted from
         a string representation to a key value pair object which is then used by the test engine object verifiers.
        """
        # if self.log_file is not None and not self.log_file.closed:
        while True:
            try:
                line = self.queue.popleft()
                # self.log_file.write(line)
                # self.log_file.write('\n')
            except IndexError:
                break
            # self.log_file.flush()
            # self.log_file.seek(0)
            self.te_packets = {}
            # for line in self.log_file:
            partitioned_line = line.partition('TX=')
            packet = partitioned_line[2]
            parsed_packet = None

            if "TE^DG:" in line:
                dg = packet.partition("/")
                dg = dg[0].partition(":")
                data_group = dg[2]

                try:
                    parsed_packet = verify_te_packet.verify_packet(packet)
                except Exception, e:
                    print e.message

                #  This will place the most recent valid packets that
                # appear in the log file in the te_packets dictionary
                if parsed_packet is not None:
                    self.te_packets[data_group] = parsed_packet
        # else:
        #     e_msg = "Log file does not exist or is not open. It must be open in order for the " \
        #             "log_data_thread to write to it."
        #     raise Exception(e_msg)

    #################################
    def close_logging_connections(self):
        """
        This method stops the read_data_thread, flushes the file buffer, closes the log_file, and then closes the
        serial connection on serial port 2
        :return:
        """
        # self.read_data_thread.join(0.0)
        # if self.log_file is not None and  not self.log_file.closed:
        #     self.log_file.flush()
        #     self.log_file.close()
        if self.controller.ser2 is not None:
            if self.controller.ser2.serial_conn.isOpen():
                self.controller.ser2.close_connection()
    #################################
    # def close_logging_file(self):
    #     self.log_file.flush()
    #     self.log_file.close()

    #################################
    def read_from_com_port(self, log_event, data_queue):
        """
        This method reads data from the com port.
        :param log_event: This is an event object which tells the thread to add data to the data queue or stop adding
        data
        :param data_queue: This is a queue that captures data as it comes through on serial port 2
        """
        if self.controller.ser2 is not None:
            ser2 = self.controller.ser2
            # This assumes that the log file has been opened prior to starting this method

            while True:
                if log_event.isSet():
                    new_line = ser2.recvln()
                    if new_line is not None:
                        data_queue.append(new_line)

    #################################
    def send_ts_packet_through_web_service(self, packet, controller_mac='',  session_id=''):
        """
        This method sends a ts packet to the given controller using a session_id by means of a HTTP call. This
        method opens table programming, sends the TS packet, and then closes table programming.
        :param packet: TQ packet to be send to the controller requesting data \n
        :param controller_mac: Controller to which this packet is to be sent \n
        :type controller_mac: string \n
        :param session_id: Valid session id to be used by the HTTP call \n
        :type session_id: String \n
        """

        if controller_mac == '':
            controller_mac = self.controller.mac

        if session_id == '':
            session_id = self.session_id

        enter_programming_packet = self.build_tp_packet_enter_table_programming()
        exit_programming_packet = self.build_tp_packet_exit_table_programming()

        enter_table_programming = 'https://p3.basemanager.net/baseservice2/sendcommand?sessionid=' + session_id
        enter_table_programming += '&controllermac=' + controller_mac + "&message=" + enter_programming_packet

        exit_table_programming = 'https://p3.basemanager.net/baseservice2/sendcommand?sessionid=' + session_id
        exit_table_programming += '&controllermac=' + controller_mac + "&message=" + exit_programming_packet

        packet_url = 'https://p3.basemanager.net/baseservice2/sendcommand?sessionid=' + session_id
        packet_url += '&controllermac=' + controller_mac + '&message=' + packet

        # send the post requests and throw exceptions if the status code is not between 200 and 299.
        r1 = requests.post(url=enter_table_programming, data={})
        if r1.status_code not in xrange(200, 300):
            raise Exception ("Enter Table programming failed. Received status code: " + str(r1.status_code))

        r2 = requests.post(url=packet_url, data={})
        if r2.status_code not in xrange(200, 300):
            raise Exception ("Send TS packet failed. Received status code: " + str(r2.status_code))

        r3 = requests.post(url=exit_table_programming, data={})
        if r3.status_code not in xrange(200, 300):
            raise Exception ("Exit Table programming failed. Received status code: " + str(r3.status_code))

        time.sleep(self.wait_time)

    #################################
    def send_tq_packet_through_web_service(self, packet, controller_mac='',  session_id=''):
        """
        This method sends a TQ packet to a controller using the given controller_mac and session_id. If none are give,
        the packet is sent to the controller set at initialization.
        :param packet: TQ packet to be send to the controller requesting data \n
        :param controller_mac: Controller to which this packet is to be sent \n
        :type controller_mac: string \n
        :param session_id: Valid session id to be used by the HTTP call \n
        :type session_id: String \n
        :return:
        """
        if controller_mac == '':
            controller_mac = self.controller.mac

        if session_id == '':
            session_id = self.session_id

        packet_url = 'https://p3.basemanager.net/baseservice2/sendcommand?sessionid=' + session_id
        packet_url += '&controllermac=' + controller_mac + "&message=" + packet

        r2 = requests.post(url=packet_url, data={})
        if r2.status_code not in xrange(200, 300):
            raise Exception ("Send TQ packet failed. Received status code: " + str(r2.status_code))
        time.sleep(self.wait_time)

    #################################
    def build_tp_packet_enter_table_programming(self, header_id=900):
        """
        This will build a TP packet for enter table programming
        :param header_id: This is the header id for the packet \n
        :type header_id: int
        :return: message \n
        :rtype: string \nr
        """
        message = "TP^MO:ET/{0}={1}".format(
            opcodes.message_id,
            str(header_id))
        return message

    def build_tp_packet_exit_table_programming(self, header_id=950):
        """
        This will build a TP packet to exit table programming
        :param header_id: This is the header id for the packet
        :type header_id: int
        :return: message \n
        :rtype: string \n
        """
        message = "TP^MO:EX/{0}={1}".format(
            opcodes.message_id,
            str(header_id))
        return message

    def build_tu_packet_exit_table_programming_success(self, header_id=951):
        """
        This will build a tu packet for a TU response from a controller when exit table programming is successful
        :param header_id: This is the header id for the packet
        :type header_id: int
        :return: message \n
        :rtype: string \n
        """
        message = "TU^MO:EX/{0}={1}^{2}:{3}/{4}=Success".format(
            opcodes.message_id,
            str(header_id),
            opcodes.status,
            opcodes.status_success,
            opcodes.message_text
        )
        return message

    def build_tu_packet_enter_table_programming_success(self, header_id=952):
        """
        This will create a TU packet for enter table programming success
        :param header_id: This is the header id for the packet
        :type header_id: int
        :return: message \n
        :rtype: string \n
        """
        message = "TU^MO:ET/{0}={1}^{2}:{3}/{4}=Success^DN:DN".format(
            opcodes.message_id,
            str(header_id),
            opcodes.status,
            opcodes.status_success,
            opcodes.message_text
        )
        return message
