import status_parser
from common.imports import opcodes
from common.imports.types import ActionCommands, ObjectTypes
from common.objects.base_classes import messages
from datetime import datetime, timedelta
import inspect

__author__ = 'baseline'


########################################################################################################################
# BaseMethods Base Class
########################################################################################################################
class BaseMethods(object):
    """
    methods that all objects can use
    """

    #################################
    def __init__(self, _identifiers, _type, _ser):
        """
        Base class for all controller objects. \n

        :param _identifiers:    Key value pairs that define this controller object. \n
        :type _identifiers:     list[list[str]] \n

        :param _type:           Type of controller object. \n
        :type _type:            str \n

        :param _ser:            Serial port of the controller this object is "attached" to \n
        :type _ser:             common.objects.base_classes.ser.Ser \n
        """
        self.ser = _ser

        # Base Attributes
        self.identifiers = _identifiers
        self.en = opcodes.true

        self.ss = ''
        self.last_ss = ''

        self.ty = _type       # Type (Ex: 'ZN', 'PG', 'ML', 'CN', etc.)

        # Place holder for program data from controller when needed.
        self.data = status_parser.KeyValues(string_in=None)

        self.build_message_string = dict()

        self.object_type = ObjectTypes.obj_dict[_type]
        self.ds = self.build_default_description()

    #################################
    def build_default_description(self):
        """
        Builds the description for the object. (Uses the long version of the object identifiers) \n
        """
        description = "Test"
        for identifier in self.identifiers:
            description += " {0} {1}".format(ObjectTypes.obj_dict[identifier[0]], identifier[1])
        return description

    #################################
    def get_id(self):
        """
        Returns the key-value pair identifier. \n
            - NOTE: Some objects have two identifiers, this method accounts for that. \n
                    Some objects have a single key identifier, such as a controller. ex: "get,cn"
        """
        identifier_string = ""
        for identifier in self.identifiers:
            if self.ty in [opcodes.basestation_3200]:
                identifier_string += ",{0}".format(identifier[0])
            else:
                identifier_string += ",{0}={1}".format(identifier[0], identifier[1])
        return identifier_string

    #################################
    def get_which_controller_is_calling_the_method(self):
        """
        Get a name of a caller in the format module.class.method

           `skip` specifies how many levels of stack to skip while getting caller
           name. skip=1 means "who calls me", skip=2 "who calls my caller" etc.

           An empty string is returned if skipped levels exceed stack height
        """
        pass
        # import sys
        # stack_size=10
        # from common.objects.base_classes.controller import BaseController
        # caller = None
        # try:
        #     stack = inspect.stack()
        #     start = 0
        #     while start < len(stack):
        #         parentframe = stack[start][0]
        #         start += 1
        #         if 'self' in parentframe.f_locals.keys():
        #             caller = parentframe.f_locals['self']
        #             print caller
        #             # print('\n'.join(callers))
        #             if not isinstance(caller, BaseController):
        #                 # TODO look at the next caller
        #                 caller2 = parentframe.f_locals['BaseController']
        #                 self.ser = caller2.ser
        #
        #             if isinstance(caller, BaseController):
        #                 self.ser = caller.ser
        #                 break
        #
        #         if start == len(stack):
        #             e_msg = "BaseController parent Name was not found "
        #             raise Exception(e_msg)
        # except Exception as e:
        #     e_msg = "Exception occurred trying to change the serial port from one controller to another " \
        #             "{0}".format(
        #              caller,     # {0}
        #              e.message   # {1}
        #             )
        #     raise Exception(e_msg)
        # else:
        #     print("Successfully set serial port for controller type: '{0}' with mac address: '{1}' and serial port "
        #           "' {2}".format(
        #             caller.controller_type,     # {0}
        #             caller.mac,                 # {1}
        #             caller.ser.s_port           # {2}
        #
        #             ))

    # #################################
    def build_obj_configuration_for_send(self):
        """
        Every object that inherits this method should rewrite it with its own version. This is here to enforce that.
        """
        raise NotImplementedError('build_obj_configuration_for_send should be implemented in each concrete device class')

    #################################
    def build_base_string_for_setters(self):
        base_set_command = "{0}{1}".format(
            ActionCommands.SET,                             # {0}
            self.get_id(),                                    # {1}
        )
        return base_set_command

    #################################
    def build_base_string_for_getters(self):
        base_get_command = "{0}{1}".format(
            ActionCommands.GET,                             # {0}
            self.get_id(),                                    # {1}
        )
        return base_get_command

    #################################
    def build_event_stop_dates_string(self, _ed_list):
        """
        Builds a list of event dates for sending to the controller \n
        :param _ed_list:        List of event stop dates \n
        :return:                Returns a formated list of semi-colon separated dates
        """
        string_for_return = ''

        # Iterate through each event stop date
        for index, each_date in enumerate(_ed_list):
            # ignore null strings
            if each_date != '':
                # Don't want to append a semi-colon at the front of this string
                if len(string_for_return) > 0:
                    string_for_return += str(";")

                string_for_return += str(each_date)

        # Example output for 3 event stop dates: '3/3/18;4/4/18;5/5/18'
        return string_for_return

    #################################
    def send_programming(self):
        """
        Sends the current programming for the object to the controller. \n
        """
        command = self.build_obj_configuration_for_send()

        try:
            # Attempt to set default values at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set {0}'s 'Values' to: '{1}'\n -> {2}".format(
                        str(self.ds),   # {0}
                        command,        # {1}
                        e.message       # {2}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully set {0}'s 'Values' to: {1}".format(
                str(self.ds),   # {0}
                command         # {1}
            ))

    #################################
    def set_description(self, _ds=None):
        """
        Sets the description of this object on the controller. If a description is specified in the parameter,
        it will overwrite the current set description and set the new one at the controller as well as in the object. \n

        :param _ds: Description to overwrite current object description. \n
        :type _ds:  str \n

        :return:
        """
        # If a description is passed in
        if _ds is not None:
            self.ds = str(_ds)

        # Command for sending to controller.
        command = "{0}{1},{2}={3}".format(
            ActionCommands.SET,     # {0}
            self.get_id(),          # {1} Builds the identifier for this object
            opcodes.description,    # {3}
            str(self.ds))           # {4}
        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to set {0}'s description to: {1}".format(
                str(self.ds),   # {0}
                self.ds         # {1}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set description for {0} to: {1}".format(
                str(self.ds),   # {0}
                self.ds         # {1}
            ))

    #################################
    def set_enabled(self):
        """
        Enable object. \n

        :return:
        """
        self.en = opcodes.true

        # Command for sending to controller.
        command = "{0}{1},{2}={3}".format(
            ActionCommands.SET,     # {0}
            self.get_id(),          # {1} Builds the identifier for this object
            opcodes.enabled,        # {2}
            str(self.en)            # {2}
        )
        try:
            self.send_command_with_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} enabled state to: {1}".format(self.ds, self.en)
            raise Exception(e_msg)
        else:
            print("Successfully set {0} enabled state to: {1}".format(self.ds, self.en))

    #################################
    def set_disabled(self):
        """
        Disable object. \n

        :return:
        """
        self.en = opcodes.false

        # Command for sending to controller.
        command = "{0}{1},{2}={3}".format(
            ActionCommands.SET,     # {0}
            self.get_id(),          # {1} Builds the identifier for this object
            opcodes.enabled,        # {2}
            str(self.en)            # {3}
            )

        try:
            self.send_command_with_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} enabled state to: {1}".format(self.ds, self.en)
            raise Exception(e_msg)
        else:
            print( "Successfully set {0} enabled state to: {1}".format(self.ds, self.en))

    #################################
    def set_message(self, _status_code):
        """
        Build message string gets a value from the message module. \n

        :param _status_code:    Status code of the message you want to build. \n
        :type _status_code:     str \n
        """
        try:
            build_message_string = messages.message_to_set(self,
                                                           _status_code=_status_code,
                                                           _ct_type=self.ty,
                                                           _use_to_verify=True)
            self.ser.send_and_wait_for_reply(tosend=build_message_string)
        except Exception as e:
            msg = "Exception caught in messages.set_message: " + str(e.message)
            raise Exception(msg)
        else:
            print("Successfully sent message: {msg}".format(msg=build_message_string))

    #################################
    def get_data(self):
        """
        Gets all data for the object from the controller. \n

        Updates the status for the current object.
        :return:
        """
        # Example command: "GET,PC=1"
        command = "{0}{1}".format(
            ActionCommands.GET,     # {0}
            self.get_id(),          # {1}
        )

        # Attempt to get data from controller
        try:
            self.data = self.ser.get_and_wait_for_reply(tosend=command, called_by_get_data=True)

            # ** Update - 11/1/17 - Ben
            #   Based on common uses in use cases, I added this for convenience sake. This allows us to always keep
            #   track of the previous status for all objects.
            #
            #   At the use case level, this removes the requirement for the user to remember to save last status. Now,
            #   it will always be saved.
            #
            #   This has no affect on current functionality and only enhances future functionality.
            self.last_ss = self.ss

            # ** Update - 11/1/17 - Ben
            #   Based on common uses in use cases, I added this for convenience sake. This allows us to not worry about
            #   calling:
            #
            #       status = self.config.BaseStation3200[1].zones[1].data.get_value_string_by_key(opcodes.status_code)
            #
            #   At the use case level, this removes the manual process for getting the status for an object after a
            #   get_data() call.
            #
            #   This has no affect on current functionality and only enhances future functionality.
            self.ss = self.data.get_value_string_by_key(opcodes.status_code)
            return self.data
        except AssertionError as ae:
            e_msg = "Unable to get data for {0} using command: '{1}'. Exception raised: {2}".format(
                        self.ds,            # {0}
                        command,            # {1}
                        str(ae.message)     # {2}
                    )
            raise ValueError(e_msg)

    # ---------------------------------------------------------------------------------------------------------------- #
    # Convenient Status Helpers
    # ---------------------------------------------------------------------------------------------------------------- #
    ####################################################################################################################
    def send_command_with_reply(self, tosend):
        """
        Wrapper for `self.send_command_with_reply` \n
        :param tosend: Command string to send.
        :param tosend: str
        :return:
        """
        self.get_which_controller_is_calling_the_method()
        return self.ser.send_and_wait_for_reply(tosend=tosend)

    ####################################################################################################################
    def get_command_with_reply(self, tosend):
        """
        Wrapper for `self.ser.get_and_wait_for_reply` \n
        :param tosend: Command string to send.
        :param tosend: str
        :return:
        :rtype: status_parser.KeyValues
        """
        self.get_which_controller_is_calling_the_method()
        return self.ser.get_and_wait_for_reply(tosend=tosend)

    #################################
    def get_data_if_necessary(self, _get_data):
        """ Shortcut function so we don't have to have this boilerplate at the top of each status_is... function """
        if _get_data or not self.data:
            self.get_data()

    #################################
    def get_data_and_verify_status(self, status):
        """
        Get the dat from the controller and than
        Verifies the status on the controller for the specified device type. \n
        :return:
        """
        self.get_data()
        self.verify_status(_expected_status=status)

    ##################################
    def get_message(self, _status_code):
        """
        Build message string gets a value from the message module

        :param _status_code:    Status code of the message you want to build. \n
        :type _status_code:     str \n
        """
        try:
            self.build_message_string = messages.message_to_get(self,
                                                                _status_code=_status_code,
                                                                _ct_type=self.ty,
                                                                _use_to_verify=True)

            command = messages.message_to_get(self, _status_code=_status_code,
                                              _ct_type=self.ty)

            cn_mg = self.ser.get_and_wait_for_reply(tosend=command[opcodes.message])
        except Exception as e:
            msg = "Exception caught in messages.get_message: " + str(e.message)
            raise Exception(msg)
        else:
            print("Successfully sent message: {msg}".format(msg=self.build_message_string[opcodes.message]))
            return cn_mg

    ##################################
    def clear_message(self, _status_code):
        """
        Build message string gets a value from the message module. \n

        :param _status_code:    Status code of the message you want to build. \n
        :type _status_code:     str \n
        """
        try:
            build_message_string = messages.message_to_clear(self,
                                                             _status_code=_status_code,
                                                             _ct_type=self.ty)
            self.ser.send_and_wait_for_reply(tosend=build_message_string)
        except Exception as e:
            msg = "Exception caught in messages.clear_message: " + str(e.message)
            raise Exception(msg)
        else:
            print( "Successfully sent message: {msg}".format(msg=build_message_string))

        # Verifies that the message was cleared by catching the 'NM' command that we would expect back
        try:
            self.build_message_string = messages.message_to_get(self,
                                                                _status_code=_status_code,
                                                                _ct_type=self.ty)
            self.ser.get_and_wait_for_reply(tosend=self.build_message_string[opcodes.message])
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message cleared and verified that the message is gone.")
            else:
                print("Message cleared, but was unable to verify if the message was gone.")

    #################################
    def verify_valid_event_stop_dates(self, _list_of_dates):
        """
        Verifies a valid list of event stop dates. \n
        :param _list_of_dates:      List of up to 8 string dates: format=mm/dd/yy
        :return:                    Valid list of string dates
        """

        # Check for valid number of times
        if _list_of_dates != '':
            if len(_list_of_dates) > 8:
                e_msg = "Invalid number of event dates to set for Controller. Expects 0 to 8 event dates, received: " \
                        "{0}".format(
                            str(len(_list_of_dates))    # {1}
                        )
                raise ValueError(e_msg)

            else:
                # Valid list of event dates, return
                return _list_of_dates

    ##################################
    # TODO: What exactly is this supposed to be doing?  Code logic is very odd...
    def verify_message_not_present(self, _status_code):
        """
        Build message string gets a value from the message module

        :param _status_code:    Status code of the message you want to build. \n
        :type _status_code:     str \n
        """
        try:
            self.build_message_string[opcodes.message] = messages.message_to_get(self,
                                                                                 _status_code=_status_code,
                                                                                 _ct_type=self.ty)
            self.ser.get_and_wait_for_reply(tosend=self.build_message_string[opcodes.message])
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_description(self):
        """
        Verifies the 'Description' for the object set on the controller. \n
        :return:
        """
        # expect string type
        ds_on_cn = self.data.get_value_string_by_key(opcodes.description)

        # Compare descriptions
        if self.ds != ds_on_cn:
            e_msg = "Unable to verify {0} 'Description'. Received: {1}, Expected: {2}".format(
                self.ds,        # {0}
                str(ds_on_cn),  # {1}
                self.ds         # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0} 'Description': '{1}' on controller".format(
                self.ds,  # {0}
                self.ds   # {1}
            ))

    #################################
    def verify_enabled_state(self):
        """
        Verifies the Object's 'Enabled State' set on the controller. \n
        """
        # expect string type
        en_state = self.data.get_value_string_by_key(opcodes.enabled)

        # Compare enabled state
        if self.en != en_state:
            e_msg = "Unable verify {0} 'Enabled State'. Received: {1}, Expected: {2}".format(
                self.ds,        # {0}
                str(en_state),  # {1}
                self.en         # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0}'s 'Enabled State': '{1}' on controller".format(
                self.ds,  # {0}
                self.en   # {1}
            ))

    #################################
    def verify_status(self, _expected_status):
        """
        Verifies the 'Status' for the object on the controller. \n

        :param _expected_status:    Expected status for program. \n
        :type _expected_status:     str \n
        """
        ss_on_cn = self.data.get_value_string_by_key(opcodes.status_code)

        # Since status is a 'get' only, we want to overwrite our object with the expected status passed in to retain
        # the state of our object.
        self.ss = _expected_status

        # Compare status versus what is on the controller
        if self.ss != ss_on_cn:
            e_msg = "Unable verify {0} 'Status'. Received: {1}, Expected: {2}".format(
                self.ds,        # {0}
                ss_on_cn,       # {1}
                str(self.ss)    # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0} 'Status': '{1}' on controller".format(
                self.ds,  # {0}
                self.ss   # {1}
            ))

    #################################
    def verify_message(self, _status_code):
        """
        the expected values are retrieved from the controller and then compared against the values that are returned
        from the message module
        :param _status_code
        :type _status_code :str
        :return:

        """
        mg_on_cn = self.get_message(_status_code=_status_code)
        expected_message_text_from_cn = mg_on_cn.get_value_string_by_key(opcodes.message_text)
        expected_message_id_from_cn = mg_on_cn.get_value_string_by_key(opcodes.message_id)
        expected_date_time_from_cn = mg_on_cn.get_value_string_by_key(opcodes.date_time)
        # Compare status versus what is on the controller
        if self.build_message_string[opcodes.message_id] != expected_message_id_from_cn:
            e_msg = "Created ID message did not match the ID received from the controller:\n" \
                    "\tCreated: \t\t'{0}'\n" \
                    "\tReceived:\t\t'{1}'\n".format(
                        self.build_message_string[opcodes.message_id],  # {0} The ID that was built
                        expected_message_id_from_cn                     # {1} The ID returned from controller
                    )
            raise ValueError(e_msg)

        mess_date_time = datetime.strptime(self.build_message_string[opcodes.date_time], '%m/%d/%y %H:%M:%S')
        controller_date_time = datetime.strptime(expected_date_time_from_cn, '%m/%d/%y %H:%M:%S')
        if mess_date_time != controller_date_time:
            if abs(controller_date_time - mess_date_time) >= timedelta(days=0, hours=0, minutes=30, seconds=0):
                e_msg = "The date and time of the message didn't match the controller:\n" \
                        "\tCreated: \t\t'{0}'\n" \
                        "\tReceived:\t\t'{1}'\n".format(
                            self.build_message_string[opcodes.date_time],  # {0} The date message that was built
                            expected_date_time_from_cn  # {1} The date message returned from controller
                        )
                raise ValueError(e_msg)
            # only print this if they were not exact
            e_msg = "############################  Date and time were not exact but were within +- 30 minutes \n" \
                    "############################  Controller Date and Time = {0} \n" \
                    "############################  Message Date time = {1}".format(
                        expected_date_time_from_cn,  # {0} Date time received from controller
                        self.build_message_string[opcodes.date_time]  # {1} Date time the message was verified
                    )
            print(e_msg)

        if self.build_message_string[opcodes.message_text] != expected_message_text_from_cn:
            e_msg = "Created TX message did not match the TX received from the controller:\n" \
                    "\tCreated: \t\t'{0}'\n" \
                    "\tReceived:\t\t'{1}'\n".format(
                        self.build_message_string[opcodes.message_text],   # {0} The TX message that was built
                        expected_message_text_from_cn                      # {1} The TX message returned from controller
                    )
            raise ValueError(e_msg)
        else:
            print("Successfully verified:\n"
                  "\tID: '{0}'\n"
                  "\tDT: '{1}'\n"
                  "\tTX: '{2}'\n".format(
                      self.build_message_string[opcodes.message_id],
                      self.build_message_string[opcodes.date_time],
                      self.build_message_string[opcodes.message_text]
                    ))

    #################################
    """
    Compare two floats within a tolerance.  Matches the python 3.5 math.isclose, so we can get rid of this if we
    update to a modern python version
    """
    @staticmethod
    def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
        return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)
