# Our modules
import status_parser
from common.variables import common as common_vars
from common.imports import opcodes
from common.imports.types import BiCoderCommands, DeviceCommands,  ActionCommands
from decimal import Decimal

from common.objects.base_classes import messages

# Python libraries
from datetime import datetime, timedelta

__author__ = 'bens'


class BiCoder(object):
    # pass controller to this method
    # also put all voltages and current here and share it with everone
    """
    Parent Class for other BiCoder object's to inherit from. \n
    
    :type substation: common.objects.controllers.bl_sb.Substation
    :type base_station: common.objects.controllers.bl_32.BaseStation3200
    """

    bicoder_types = BiCoderCommands.Type
    device_type = DeviceCommands.Type

    def __init__(self, _serial_number, _controller, _address=0):
        """
        The initializer for the base class of any bicoder. It holds the basic attributes that all bicoders share.

        :param _controller:     Controller this object is "attached" to \n
        :type _controller:      common.objects.controllers.bl_32.BaseStation3200 |
        :type _controller:      common.objects.controllers.bl_10.BaseStation1000 |
                                common.objects.controllers.bl_sb.Substation \n

        :param _serial_number:  Serial number to set for BiCoder. \n
        :type _serial_number:   str \n

        :param _address:        The address associated with this bicoder on the controller. \n
        :type _address:         int \n
        """
        self.controller = _controller   # The controller this BiCoder is "attached" to
        # self.ser = _controller.ser      # The serial port this object will talk through

        # We import here to avoid a circular import
        from common.objects.controllers.bl_32 import BaseStation3200
        from common.objects.controllers.bl_10 import BaseStation1000
        from common.objects.controllers.bl_sb import Substation

        self.substation = None
        self.base_station = None

        if isinstance(_controller, BaseStation3200):
            self.base_station = _controller
            self.controller_type = opcodes.basestation_3200
        elif isinstance(_controller, BaseStation1000):
            self.base_station = _controller
            self.controller_type = opcodes.basestation_1000
        elif isinstance(_controller, Substation):
            self.substation = _controller
            self.controller_type = opcodes.substation

        self.sn = _serial_number        # Serial Number for BiCoder
        self.ser = _controller.ser      # Serial Port connection instance rom Controller for BiCoder to communicate over
        self.ty = None                  # BiCoder "TYPE" from BiCoderTypes class.
        self.id = ''                    # The identifier of the BiCoder
        self.ad = _address
        self.nu = _address
        
        # Set by child classes
        self.decoder_type = ''

        # BiCoder Attributes
        self.vt = 1.7   # Two wire drop

        # Shared Read Only Attributes
        self.ss = ''

        # Attributes that we will send to the substation by default
        self.default_attributes = [
            (opcodes.two_wire_drop, 'vt')
        ]

        # Place holder for data from controller when needed.
        self.data = status_parser.KeyValues(string_in=None)

        # Attribute so we can store the messages
        self.build_message_string = ''  # place to store the message that is compared to the substation message

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Setter Methods                                                                                                   #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def set_default_values(self):
        """
        Set the default values of the device on the substation. \n
        :return:
        :rtype:
        """
        # Build the basic command that will be added on to
        command = "{0},{1}={2}".format(
            ActionCommands.SET,     # {0}
            self.id,                # {1} Identifier for the biCoder (ex: 'ZN', 'FM', etc.)
            str(self.sn),           # {2} Serial number for the biCoder
        )

        # Loops through the attribute list and adds the values to our string that we will send to the substation
        for attribute_tuple in self.default_attributes:
            # attribute_tuple[0] is the name of the attribute in the string we send to the substation
            attribute_name = attribute_tuple[0]

            # attribute_tuple[1] is the attribute for the device to update
            bicoder_attribute = getattr(self, attribute_tuple[1])

            # Add the attribute to the command string
            command += ",{0}={1}".format(
                attribute_name,     # {0}
                bicoder_attribute)  # {1}

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} ({1})'s 'Default values' to: '{2}'".format(
                self.ty,        # {0}
                str(self.sn),   # {0}
                command         # {1}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set {0} ({1})'s 'Default values' to: {2}".format(
                self.ty,        # {0}
                str(self.sn),   # {1}
                command)        # {2}
            )

    #################################
    def set_serial_number(self, _sn=None):
        """
        Write the serial number to a specific biCoder. \n
        this method specially writes the serial number to a biCoder

        :param _sn:   serial number to overwrite current serial number. \n
        :type _sn:    int \n
        :return:
        """

        if _sn is not None:
            self.sn = _sn

        # Example command: 'DO,AZ=TSD0001,NU=1'
        command = "{0},{1},{2}={3}".format(
            ActionCommands.DO,                                      # {0}
            BiCoderCommands.Dictionaries.TYPE_LOOK_UP[self.id],     # {1}
            BiCoderCommands.Attributes.SERIAL_NUMBER,               # {2}
            str(self.sn),                                           # {3}
        )
        try:
            print "Sending command: " + command
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to writer serial number of biCoder type: {0} to  serial number : {1} to"\
                .format(
                    BiCoderCommands.Dictionaries.TYPE_LONG[self.ty],    # {0}
                    self.sn                                             # {1}
                )
            raise Exception(e_msg)
        else:
            # TODO the biCoder type needs to look up the long wording not the two digit key
            print("Successfully wrote biCoder type {0} to serial number: {1} ".format(
                BiCoderCommands.Dictionaries.TYPE_LONG[self.ty],    # {0}
                str(self.sn)))                                      # {1}

    #################################
    def write_serial_number_to_bicoder(self, _serial_num):
        """
        Write the serial number to the biCoder.\n
        This is used in the manufacturing process
        :param _serial_num:  serial number. \n
        :type _serial_num:   str \n

        :return:
        """
        # take the serial number that is passed in from teh use case and write it the the bicoder

        # first take the serial number a see if it is a string
        if not isinstance(_serial_num, str):
            e_msg = "Serial number must be in a string format '{0}'".format(
                str(_serial_num))
            raise TypeError(e_msg)
        if _serial_num is not None:
            self.sn = _serial_num
        # count the number of characters in the string
        if len(self.sn) != 7:
            raise ValueError("Controller serial number must be a seven digit string, instead was " +
                             str(self.sn))

        # Example command: 'DO,D1,SN=TSD0001'
        command = "{0},{1},{2}={3}".format(
            ActionCommands.DO,                                      # {0}
            BiCoderCommands.Dictionaries.BICODER_TYPE[self.id],     # {1}
            BiCoderCommands.Attributes.SERIAL_NUMBER,               # {2}
            str(self.sn))
        try:
            # Attempt to set two wire drop for zone at the substation
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to Write the serial number on controller command sent was: {0}".format(
                command  # {0}
            )
            raise Exception(e_msg)
        else:
            e_msg = "Successfully Wrote serial number to {0}" . format(_serial_num)
            print (e_msg)

    #################################
    def set_two_wire_drop_value(self, _value=None):
        """
        Set the two wire drop value for the BiCoder (Faux IO Only) \n
        By passing in a value for '_value', method assumes this value to overwrite current value. \n

        :param _value: Value to set the Zone two wire drop value to \n
        :type _value: int, float \n
        """
        # Check if user wants to overwrite current value
        if _value is not None:

            # Verify the correct type is passed in.
            if not isinstance(_value, (int, float)):
                e_msg = "Failed trying to set BiCoder {0}'s two wire drop value. Invalid type passed in, expected " \
                        "int or float. Received type: {1}".format(
                            str(self.sn),   # {0}
                            type(_value)    # {1}
                        )
                raise TypeError(e_msg)
            else:
                # Valid type, overwrite current value with new value
                self.vt = _value

        # Command for sending
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                         # {0}
            str(self.id),                               # {1}
            str(self.sn),                               # {2}
            BiCoderCommands.Attributes.TWO_WIRE_DROP,   # {3}
            str(self.vt)                                # {4}
        )

        try:
            # Attempt to set two wire drop for zone at the substation
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} {1}'s 'Two Wire Drop Value' to: '{2}'".format(
                str(self.ty),   # {0}
                str(self.sn),   # {1}
                str(self.vt))   # {2}
            raise Exception(e_msg)
        else:
            print("Successfully set {0} {1}'s 'Two Wire Drop Value' to: {2}".format(str(
                self.ty),       # {0}
                str(self.sn),   # {1}
                str(self.vt)))  # {2}

    #################################
    def do_self_test(self):
        """
        Do a test on the biCoder
        """
        if self.id not in common_vars.list_of_device_types:
            e_msg = "Invalid device type: {0} entered while trying to do a self test.".format(
                str(self.id))
            raise ValueError(e_msg)

        # If we are a zone, MV or Pump and controller is a Substation, need to use Zone 'ZN' as key
        if self.id in [DeviceCommands.Type.ZONE, DeviceCommands.Type.MASTER_VALVE, DeviceCommands.Type.PUMP] and self.controller.is_substation():
            command = "{0},{1},{2}={3}".format(
                ActionCommands.DO,          # {0}
                ActionCommands.DEVICETEST,  # {1}
                DeviceCommands.Type.ZONE,   # {2}
                str(self.sn))               # {3}
        else:
            command = "{0},{1},{2}={3}".format(
                ActionCommands.DO,          # {0}
                ActionCommands.DEVICETEST,  # {1}
                self.id,                    # {2}
                str(self.sn))               # {3}

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to do a self test {0} {1}: {2}".format(
                self.ty,    # {0}
                self.sn,    # {1}
                e.message   # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully tested: {0} {1}".format(
                self.ty,    # {0}
                self.sn     # {1}
            ))
            
    #################################
    def do_bicoder_to_control_board_bit_timing_test(self):
        """
        Do a test on the biCoder that return the bit timing of the biCoder to the control board
        """
        bit_timing = ''

        if self.device_type not in common_vars.list_of_device_types:
            e_msg = "Invalid device type: {0} entered while trying to send longitude to cn".format(
                str(self.device_type))
            raise ValueError(e_msg)

        # If we are a zone, need to use address instead of serial number on set command
        if self.device_type == DeviceCommands.Type.ZONE:
            command = "{0},{1},{2}={3}".format(
                ActionCommands.DO,          # {0}
                ActionCommands.DEVICETEST,  # {1}
                self.device_type,           # {2}
                str(self.sn))               # {3}
        else:
            command = "{0},{1},{2}={3}".format(
                ActionCommands.DO,          # {0}
                ActionCommands.DEVICETEST,  # {1}
                self.device_type,           # {2}
                str(self.sn))               # {3}

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to test {0}'s".format(self.device_type)
            raise Exception(e_msg)
        else:
            print("Successfully tested: {0}".format(self.device_type))

        return bit_timing

    #################################
    def do_control_board_to_bicoder_bit_timing_test(self):
        """
        Do a test on the biCoder that return the bit timing of the control board to the biCoder
        """
        bit_timing = ''

        if self.device_type not in common_vars.list_of_device_types:
            e_msg = "Invalid device type: {0} entered while trying to send longitude to cn".format(
                str(self.device_type))
            raise ValueError(e_msg)

        # If we are a zone, need to use address instead of serial number on set command
        if self.device_type == DeviceCommands.Type.ZONE:
            command = "{0},{1},{2}={3}".format(
                ActionCommands.DO,          # {0}
                ActionCommands.DEVICETEST,  # {1}
                self.device_type,           # {2}
                str(self.sn))               # {3}
        else:
            command = "{0},{1},{2}={3}".format(
                ActionCommands.DO,          # {0}
                ActionCommands.DEVICETEST,  # {1}
                self.device_type,           # {2}
                str(self.sn))               # {3}

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to test {0}'s".format(self.device_type)
            raise Exception(e_msg)
        else:
            print("Successfully tested: {0}".format(self.device_type))
        return bit_timing

    #################################
    def set_message(self, _status_code, _helper_object=None):
        """
        Build message string gets a value from the message module
        :param _status_code
        :type _status_code :str
        :param _helper_object
        :type _helper_object : BaseStartStopPause
        """
        try:
            build_message_string = messages.message_to_set(self,
                                                           _status_code=_status_code,
                                                           _ct_type=self.id,
                                                           _helper_object=_helper_object)
            self.ser.send_and_wait_for_reply(tosend=build_message_string)
        except Exception as e:
            msg = "Exception caught in messages.set_message: " + str(e.message)
            raise Exception(msg)
        else:
            print "Successfully sent message: {msg}".format(msg=build_message_string)

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Verifier Methods                                                                                                 #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def verify_serial_numbers_of_bicoders(self, _data=None):
        """
        Verifies the Serial Number set on the BiCoder. \n

        :param _data:   Data Object that holds the substation's attributes. If it isn't passed in we call get_data \n
        :type _data:    status_parser.KeyValues

        :return: True if the value in our object matches the value on the Substation
        """
        # Calls get data if a data object was not passed in.
        # if self.ty == opcodes.single_valve_decoder:
        #     check one serial number so on and so forth
        data = _data if _data else self.get_data()

        # Get the serial number from the data object
        sn_on_sb = data.get_value_string_by_key(BiCoderCommands.Attributes.SERIAL_NUMBER)

        # Compare Serial Numbers
        if self.sn != sn_on_sb:
            e_msg = "Unable to verify {0} ({1})'s 'Serial Number'. Received: {2}, Expected: {3}".format(
                self.ty,        # {0}
                self.sn,        # {1}
                str(sn_on_sb),  # {2}
                str(self.sn)    # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0}'s ({1}) Serial Number: '{2}' on substation".format(
                self.ty,        # {0}
                self.sn,        # {1}
                str(self.sn)    # {2}
            ))
            return True

    #################################
    def verify_two_wire_drop_value(self, _data=None):
        """
        Verifies the two wire drop value for this BiCoder on the Substation. Expects the substations's value and this
        instance's value to be equal. \n

        We round the value from the substation to the hundredth place (.01). We then allow a variance of .2 units
        between our substation value and object value to account for the rounding that takes place in the substation. \n

        :param _data: Data Object that holds the substation's attributes. \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the Substation
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.get_data()

        # Compare status versus what is on the substation
        two_wire_drop_val = float(data.get_value_string_by_key(BiCoderCommands.Attributes.TWO_WIRE_DROP))
        rounded_vt = round(number=self.vt, ndigits=2)
        
        # If we are a faux device, use 0.2 as allowance
        if abs(rounded_vt - two_wire_drop_val) > 0.2 and self.controller_is_in_faux_io():
            e_msg = "Unable to verify {0} {1}'s two wire drop value. Received: {2}, Expected: {3}".format(
                str(self.ty),               # {0}
                str(self.sn),               # {1}
                str(two_wire_drop_val),     # {2}
                str(self.vt)                # {3}
            )
            raise ValueError(e_msg)
        # Else if we are a real device, the two wire drop should be between 0.5 and 3.0 volts
        elif (Decimal(0.5) > Decimal(two_wire_drop_val) or Decimal(two_wire_drop_val) > Decimal(3.0)) and not self.controller_is_in_faux_io():
            e_msg = "Unable to verify {0} {1}'s (real device) two wire drop value. Received: {2}, Expected: {3}".format(
                str(self.ty),               # {0}
                str(self.sn),               # {1}
                str(two_wire_drop_val),     # {2}
                str(self.vt)                # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0} {1}'s two wire drop value: '{2}' on substation".format(
                str(self.ty),     # {0}
                str(self.sn),     # {1}
                str(self.vt)      # {2}
            ))
            return True

    #################################
    def verify_serial_number(self, _data=None):
        """
        Verifies the Serial Number set on the BiCoder. \n

        :param _data: Data Object that holds the substation's attributes. If it isn't passed in we call get_data \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the Substation
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.get_data()

        # Get the serial number from the data object
        sn_on_sb = data.get_value_string_by_key(BiCoderCommands.Attributes.SERIAL_NUMBER)

        # Compare Serial Numbers
        if self.sn != sn_on_sb:
            e_msg = "Unable to verify {0} ({1})'s 'Serial Number'. Received: {2}, Expected: {3}".format(
                self.ty,        # {0}
                self.sn,        # {1}
                str(sn_on_sb),  # {2}
                str(self.sn)    # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0}'s ({1}) Serial Number: '{2}' on substation".format(
                self.ty,        # {0}
                self.sn,        # {1}
                str(self.sn)    # {2}
            ))
            return True

    #################################
    def verify_status(self, _status, _data=None):
        """
        Verifies the status on the substation for the specified biCoder type. \n

        :param _status: Status that we expect to be on the substation. \n
        :type _status: str

        :param _data: Data Object that holds the substation's attributes. If it isn't passed in we call get_data \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the Substation
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.get_data()

        if _status not in common_vars.dictionary_for_status_codes:
            e_msg = "Unable to verify {0} ({1})'s status. Received invalid expected status: ({2}),\n" \
                    "Expected one of the following: ({3})".format(
                        str(self.ty),                                   # {0}
                        str(self.sn),                                   # {1}
                        str(_status),                                   # {2}
                        common_vars.dictionary_for_status_codes.keys()  # {3}
                    )
            raise KeyError(e_msg)

        # Get the actual status from our data object
        dv_ss_on_cn = data.get_value_string_by_key(opcodes.status_code)

        # Since status is a 'get' only, we want to overwrite our object with the expected status passed in to retain
        # the state of our object.
        self.ss = _status

        # Compare status versus what is on the substation
        if self.ss != dv_ss_on_cn:
            e_msg = "Unable to verify {0} ({1})'s status. Received: {2}, Expected: {3}".format(
                self.ty,            # {0}
                self.sn,            # {1}
                str(dv_ss_on_cn),   # {2}
                str(self.ss)        # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0} ({1})'s status: '{2}' on substation".format(
                self.ty,    # {0}
                self.sn,    # {1}
                self.ss     # {2}
            ))
            return True

    #################################
    def verify_message(self, _status_code, _helper_object=None):
        """
        The expected values are retrieved from the substation and than compared against the values that are returned
        from the message module. \n

        :param _status_code: The status code we will pass to the substation
        :type _status_code: str

        :param _helper_object:
        :type _helper_object:
        :return:
        """
        mg_on_sb = self.get_message(_status_code=_status_code, _helper_object=_helper_object)
        expected_message_text_from_sb = mg_on_sb.get_value_string_by_key(opcodes.message_text)
        expected_date_time_from_sb = mg_on_sb.get_value_string_by_key(opcodes.date_time)

        mess_date_time = datetime.strptime(self.build_message_string[opcodes.date_time], '%m/%d/%y %H:%M:%S')
        substation_date_time = datetime.strptime(expected_date_time_from_sb, '%m/%d/%y %H:%M:%S')
        if mess_date_time != substation_date_time:
            if abs(substation_date_time - mess_date_time) >= timedelta(days=0, hours=0, minutes=60, seconds=0):
                e_msg = "The date and time of the message didn't match the substation:\n" \
                        "\tCreated: \t\t'{0}'\n" \
                        "\tReceived:\t\t'{1}'\n".format(
                            self.build_message_string[opcodes.date_time],  # {0} The date message that was built
                            expected_date_time_from_sb  # {1} The date message returned from substation
                        )
                raise ValueError(e_msg)
            # only print this if they were not exact
            e_msg = "############################  Date and time were not exact but were within +- 30 minutes \n" \
                    "############################  Substation Date and Time = {0} \n" \
                    "############################  Message Date time = {1}".format(
                        expected_date_time_from_sb,  # {0} Date time received from substation
                        self.build_message_string[opcodes.date_time]  # {1} Date time the message was verified
                    )
            print(e_msg)

        if self.build_message_string[opcodes.message_text] != expected_message_text_from_sb:
            e_msg = "Created TX message did not match the TX received from the substation:\n" \
                    "\tCreated: \t\t'{0}'\n" \
                    "\tReceived:\t\t'{1}'\n".format(
                        self.build_message_string[opcodes.message_text],  # {0} The TX message that was built
                        expected_message_text_from_sb  # {1} The TX message returned from substation
                    )
            raise ValueError(e_msg)
        else:
            print "Successfully verified:\n" \
                  "\tID: '{0}'\n" \
                  "\tDT: '{1}'\n" \
                  "\tTX: '{2}'\n".format(
                        self.build_message_string[opcodes.message_id],
                        self.build_message_string[opcodes.date_time],
                        self.build_message_string[opcodes.message_text]
                   )

    #################################
    def verify_device_address(self):
        """
        Verifies the address of the biCoder. \n
        :return:
        """
        # If we are on Substation two-wire, switch serial to the 3200's serial so that the "get_data()" command is sent
        # to the 3200 and not substation. Substation doesn't return addresses for devices.
        if self.controller.is_substation():
            self.ser = self.controller.base_station_3200.ser
            
        # Get data from 3200
        self.get_data()
        
        # Reset serial back to whatever controller we are attached to (two-wire)
        self.ser = self.controller.ser

        nu_on_cn = int(self.data.get_value_string_by_key(DeviceCommands.Attributes.DEVICE_ADDRESS))

        # Compare the device address in our object to the one on the controller
        if self.nu != nu_on_cn:
            e_msg = "Unable verify {0}: {1}'s 'device address'. Received: {2}, Expected: {3}".format(
                self.id,   # {0}
                self.sn,        # {1}
                str(nu_on_cn),  # {2}
                str(self.nu)    # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0} {1}'s device address: '{2}' on controller".format(
                self.id,   # {0}
                self.sn,        # {1}
                str(self.nu)    # {2}
            ))

    #################################
    def verify_bicoder_not_present(self):
        """
        Verifies that this bicoder is no longer on the controller. \n
        """
        #  is if bicoder is not present a false is returned
        if self.is_bicoder_present():
            e_msg = "A {0} biCoder with serial number {1} WAS on the controller, which was un-expected.".format(
                self.ty,
                self.sn
            )
            raise Exception (e_msg)
        else:
            print "A {0} biCoder with serial number {1} WAS not on the controller, which was expected.".format(
                self.ty,
                self.sn
            )

    #################################
    def verify_bicoder_present(self):
        """
        Verifies that this bicoder is on the controller. \n
        """
        #  is if bicoder is not present a false is returned
        if self.is_bicoder_present():
            print "A {0} biCoder with serial number {1} WAS on the controller, which was expected.".format(
                self.ty,
                self.sn
            )
        else:
            e_msg = "A {0} biCoder with serial number {1} WAS NOT on the controller, which was un-expected.".format(
                self.ty,
                self.sn
            )
            raise Exception (e_msg)
            
    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Other Methods                                                                                                    #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def controller_is_in_faux_io(self):
        """
        *CODED ON A FRIDAY*
        
        For ben's convenience and amusement :)
        
        <B3N/>
        
        :rtype: bool
        """
        return self.controller.faux_io_enabled

    #################################
    def is_bicoder_present(self):
        """
        Verifies that this bicoder is no longer on the controller. \n
        """
        try:
            # This get data should fail, as the bicoder should no longer be on the controller
            self.get_data()
        except ValueError:
            return False
        else:
            return True

    #################################
    def is_valve_bicoder(self):
        """
        Returns whether or not the bicoder is a valve bicoder.
        :rtype: bool
        """
        return self.ty == self.bicoder_types.VALVE
    
    #################################
    def is_flow_bicoder(self):
        """
        Returns whether or not the bicoder is a flow bicoder.
        :rtype: bool
        """
        return self.ty == self.bicoder_types.FLOW
    
    #################################
    def is_moisture_bicoder(self):
        """
        Returns whether or not the bicoder is a moisture bicoder.
        :rtype: bool
        """
        return self.ty == self.bicoder_types.MOISTURE
    
    #################################
    def is_temperature_bicoder(self):
        """
        Returns whether or not the bicoder is a temperature bicoder.
        :rtype: bool
        """
        return self.ty == self.bicoder_types.TEMPERATURE
    
    #################################
    def is_switch_bicoder(self):
        """
        Returns whether or not the bicoder is a switch bicoder.
        :rtype: bool
        """
        return self.ty == self.bicoder_types.SWITCH
    
    #################################
    def is_pump_bicoder(self):
        """
        Returns whether or not the bicoder is a pump bicoder.
        :rtype: bool
        """
        return self.ty == self.bicoder_types.PUMP
    
    #################################
    def is_analog_bicoder(self):
        """
        Returns whether or not the bicoder is a analog bicoder.
        :rtype: bool
        """
        return self.ty == self.bicoder_types.ANALOG
    
    #################################
    def is_alert_bicoder(self):
        """
        Returns whether or not the bicoder is a alert bicoder.
        :rtype: bool
        """
        return self.ty == self.bicoder_types.ALERT
    
    #################################
    def move_from_3200_to_substation(self, _substation):
        """
        Moves the bicoder object from the 3200 to the specified substation.
        
        (1) Load device onto substation without creating any objects
        (2) Add bicoder to substation bicoder dictionary
        (3) Update bicoder controller/substation references
        (4) Update bicoder serial port
        
        :param _substation: Substation object to move bicoder to.
        :type _substation: common.objects.controllers.bl_sb.Substation
        """
        # (1)
        _substation.load_dv_to_cn(dv_type=self.decoder_type, list_of_decoder_serial_nums=[self.sn])
        # (2)
        if self.is_valve_bicoder(): _substation.valve_bicoders[self.sn] = self
        elif self.is_flow_bicoder(): _substation.flow_bicoders[self.sn] = self
        elif self.is_moisture_bicoder(): _substation.moisture_bicoders[self.sn] = self
        elif self.is_temperature_bicoder(): _substation.temperature_bicoders[self.sn] = self
        elif self.is_switch_bicoder(): _substation.switch_bicoders[self.sn] = self
        elif self.is_analog_bicoder(): _substation.analog_bicoders[self.sn] = self
        # (3)
        # Here we don't need to update `self.base_station` because the bicoder originally existed on the 3200. That
        # means that during the "init()", `self.base_station` was set to 3200.
        self.controller = _substation
        self.substation = _substation
        # (4)
        self.ser = _substation.ser
        
    #################################
    def move_from_substation_to_3200(self, _basestation3200):
        """
        Moves the bicoder object from the substation to the specified BaseStation 3200.
        
        (1) Load device onto BaseStation 3200 without creating any objects
        (2) Remove bicoder from substation bicoder dictionary (no longer is on the substation's two-wire path)
        (3) Update bicoder controller/substation references
        (4) Update bicoder serial port
        
        :param _basestation3200: Substation object to move bicoder to.
        :type _basestation3200: common.objects.controllers.bl_32.BaseStation3200
        """
        # (1)
        _basestation3200.load_dv_to_cn(dv_type=self.decoder_type, list_of_decoder_serial_nums=[self.sn])
        # (2)
        #   -> Here .pop(self.sn, None) will remove the bicoder with the key: self.sn. None is passed in as the second
        #      argument to avoid an exception in case the bicoder with key: self.sn doesn't exist in the dictionary.
        if self.is_valve_bicoder(): self.substation.valve_bicoders.pop(self.sn, None)
        elif self.is_flow_bicoder(): self.substation.flow_bicoders.pop(self.sn, None)
        elif self.is_moisture_bicoder(): self.substation.moisture_bicoders.pop(self.sn, None)
        elif self.is_temperature_bicoder(): self.substation.temperature_bicoders.pop(self.sn, None)
        elif self.is_switch_bicoder(): self.substation.switch_bicoders.pop(self.sn, None)
        elif self.is_analog_bicoder(): self.substation.analog_bicoders.pop(self.sn, None)
        # (3)
        # Here we don't need to update `self.base_station` because the bicoder originally existed on the 3200. That
        # means that during the "init()", `self.base_station` was set to 3200.
        self.controller = _basestation3200
        self.base_station = _basestation3200
        self.substation = None
        # (4)
        self.ser = _basestation3200.ser

    #################################
    def get_message(self, _status_code, _helper_object=None):
        """
        Build message string gets a value from the message module

        :param _status_code
        :type _status_code :str

        :param _helper_object
        :type _helper_object: BaseStartStopPause
        """
        try:
            self.build_message_string = messages.message_to_get(self,
                                                                _status_code=_status_code,
                                                                _ct_type=self.id,
                                                                _helper_object=_helper_object)
            sb_mg = self.ser.get_and_wait_for_reply(tosend=self.build_message_string[opcodes.message])
        except Exception as e:
            msg = "Exception caught in messages.get_message: " + str(e.message)
            raise Exception(msg)
        else:
            print "Successfully sent message: {msg}".format(msg=self.build_message_string[opcodes.message])
            return sb_mg

    #################################
    def get_data(self, _for_substation=False):
        """
        Gets the data for the Substation and returns it as an object we can parse through. \n

        :return: status_parser.KeyValues
        """
        if self.id in [opcodes.pump, opcodes.master_valve] and not self.ad and not self.controller.is1000():
            # Build the command to get data from the Substation
            command = "{0},{1}={2}".format(
                ActionCommands.GET,     # {0}
                opcodes.zone,           # {1}
                self.sn                 # {2}
            )
        elif self.id == opcodes.zone and not self.ad:
            # Build the command to get data from the Substation
            command = "{0},{1}={2}".format(
                ActionCommands.GET,     # {0}
                self.id,                # {1}
                self.sn                 # {2}
            )
        # TODO: This was commented out because when we are in the use-case calling self.object.bicoder.self_test_and_update_object_attributes(),
        # TODO:     when we get to here, we don't have the ability to set the `_for_substation` flag. But since the v16
        # TODO:     3200 allows for us to get/set zones by serial number, ignore attempting to send address.
        # elif self.id == opcodes.zone and not _for_substation:
        #     # Build the command to get data from the Substation
        #     command = "{0},{1}={2}".format(
        #         ActionCommands.GET,     # {0}
        #         self.id,                # {1}
        #         self.ad                 # {2}
        #     )
        else:
            # Build the command to get data from the Substation
            command = "{0},{1}={2}".format(
                ActionCommands.GET,     # {0}
                self.id,                # {1}
                self.sn                 # {2}
            )

        # Attempt to get data from substation
        try:
            self.data = self.ser.get_and_wait_for_reply(tosend=command, called_by_get_data=True)
        except AssertionError as ae:
            e_msg = "Unable to get data for {0} ({1}) using command: '{2}'. Exception raised: {3}".format(
                self.ty,            # {0}
                self.sn,            # {1}
                command,            # {2}
                str(ae.message)     # {3}
            )
            raise ValueError(e_msg)
        else:
            return self.data
