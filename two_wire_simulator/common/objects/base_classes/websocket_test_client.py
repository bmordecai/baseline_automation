from threading import Thread
from ws4py.client.threadedclient import WebSocketClient
import time

class TestClient(WebSocketClient):

    def __init__(self, server_ip, port_number):
        self.message_handler = TestClientMessageHandler()
        url = "ws://" + str(server_ip) + ":" + str(port_number) + "/"
        super(self.__class__, self).__init__(url, protocols=['http-only', 'chat'])

    client_thread = None
    return_message = ""

    def opened(self):
        print "Test client web socket opened"

    def closed(self, code, reason=None):
        print "Test client web socket closed down", code, reason

    def received_message(self, m):
        self.return_message = m.data
        received_message_response = self.message_handler.process_message(m.data)
        self.send(received_message_response)

    def get_returned_message(self):
        """return the most recent message received by the client"""
        return self.return_message

    def start_client(self):
        client_started = True
        # connect the test_client to the server
        try:
            self.connect()
        except Exception, e:
            client_started = False

        # create and then start the test_client thread
        self.client_thread = Thread(target=self.run_forever)
        self.client_thread.start()
        return client_started

    def ping_server(self):
        ping_successful = False
        ping_response = self.send_message_wait_for_server_response("server ping")
        if ping_response == "server ok":
            ping_successful = True
        return ping_successful

    def close_client(self):
        # close down the connection to the server
        self.close_connection()

        # close test_client_thread
        self.client_thread.join()

    def send_message_wait_for_server_response(self, message):
        self.return_message = ""
        self.send(message)
        count = 0
        # keep checking the returned_message variable until it is either blank or 5 seconds have passed.
        while self.return_message == "" and count < 10:
            time.sleep(.5)
            count = count + 1
        return self.return_message


class TestClientMessageHandler(object):

    def process_message(self, message):
        # set message_response to the returned message variable which will be returned later.
        message_response = ""
        if message == "client ping":
            message_response = "client ok"

        if message == "0004A37650540000000028TQ^DG:403/ID=559^WS:AL^DN:DN":
            te_response_dg_403  = "0000000004A37650540248TE^DG:403/ID=559/SQ=0/MX=8^WS:1/EM=1|EN=TR|TM=3600.0^WS:2"
            te_response_dg_403 += "/EM=1|EN=FA|TM=3600.0^WS:3/EM=1|EN=FA|TM=3600.0^WS:4/EM=1|EN=FA|TM=3600.0^WS:5/EM=1"
            te_response_dg_403 += "|EN=FA|TM=3600.0^WS:6/EM=1|EN=FA|TM=3600.0^WS:7/EM=1|EN=FA|TM=3600.0^WS:8/EM=1|EN=FA"
            te_response_dg_403 += "|TM=3600.0^SL:TR^DN:DN"

            message_response = te_response_dg_403

        if message == "0004A37650540000000020TP^MO:ET/ID=25^DN:DN":
            message_response = "0000000004A37650540037TU^MO:ET/ID=25^ST:SU/TX=Success^DN:DN"

        if message == "0004A37650540000000097TS^DG:302/ID=559/SQ=0^MS:xxx0001/DS=test moisture sensor/LA=45.000/LG=116.44565/EN=TR^SL:TR^DN:DN":
            message_response = "0000000004A37650540037TU^DG:302/ID=559/SQ=0/MX=8^ST:SU/TX=Success^DN:DN"

        if message == "0004A37650540000000020TP^MO:EX/ID=26^DN:DN":
            message_response = '0000000004A37650540037TU^MO:EX/ID=26^ST:SU/TX=Success^DN:DN'

        return message_response