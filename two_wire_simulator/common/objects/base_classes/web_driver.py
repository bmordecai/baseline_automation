from datetime import datetime
import platform
import os
import time

import common.variables.common as common_vars
from common import helper_methods
from common.objects.basemanager.locators import *
from common.eventlistener import MyListener
from common.user_configuration import UserConfiguration

from selenium.webdriver.support.event_firing_webdriver import EventFiringWebDriver as EFWD
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import *
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait

__author__ = 'bens'


class WebDriver(object):
    """
    Browser Object \n
    """
    web_driver = webdriver
    
    # Get Today's date
    today = datetime.today()
    
    def __init__(self, conf, configuration=None):
        """
        the user_conf is passing in information from the login_user_configuration module
        :param conf:            url, user_name, user_password,
        :type conf:             UserConfiguration
        :param configuration:   Has all the references to initialized controllers
        :type configuration:    Configuration
        """
        self.name = ''
        self.timeout = common_vars.DEFAULT_TIMEOUT
        self.conf = conf
        self.controller_conf = configuration
        """:type: common.configuration.Configuration"""
        self.number_of_controllers_online = 0
        self.total_number_of_controllers = 0
        self.eventListener = MyListener(self.name)
    
    def open(self, _browser_name):
        """
        Maximizes the browser's window and goes to the url.
        """
        # Starts the browser (opens a web browser instance, ie: chrome, firefox)
        self.init_driver(_browser=_browser_name)
        
        # Displays the web browser's version onto the console
        print self.web_driver.capabilities['version']
        
        # Maximizes the window of the web browser
        self.web_driver.maximize_window()
        
        # Enters and uses the URL in order to access the login web page
        self.web_driver.get(self.conf.url)
        
        # This line will make the web browser focused
        self.web_driver.switch_to.window(self.web_driver.window_handles[0])
    
    def verify_file_path(self, file_path):
        """
        Pass in  the path of the driver and verify that it is located on the current PC. \n
        :param file_path: The path of the file intended for access. \n
        :type file_path: str \n
        :return:
        :rtype:
        """
        if os.path.isfile(file_path) and os.access(file_path, os.R_OK):
            print 'File exists and is readable'
        else:
            e_msg = "Either file is missing, is not readable or wrong file path specified in user .json " \
                    "file.\nReceived PATH: '{0}'. For more information about web_drivers and expected paths " \
                    "please visit: 'https://seleniumhq.github.io/docs/wd.html'".format(file_path)
            raise ValueError(e_msg)
    
    def init_driver(self, _browser):
        """
        Takes the input, browser_type, and assigns that browser type to the correct
        selenium web driver as long as the browser type is supported. Known supported
        browser types include: \n
            1. Chrome/Google Chrome (both are valid entries) \n
                * Known google chrome locations on MacMini \n
                    1) executable_path="/usr/bin/chromedriver.exe" \n
                    2) executable_path="/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome" \n
                    3) executable_path="/Users/scottnd/bin/chromedriver.exe" \n
                * * Note, for execution on windows machine, the chromedriver.exe location needs to be
                    manually entered down below on the right hand side of '=' on line 52 \n
            2. Firefox \n
            3. IE - NOT YET IMPLEMENTED CORRECTLY \n
        :param _browser: Web browser to use \n
        :type _browser: str \n
        :return:
        """
        web_driver = None
        supported_browsers = ["chrome", "google chrome", "firefox", "ie"]
        browser_lowercase = _browser.lower()
        # Checks if the user specified web_browser is supported, if not, don't waste more time
        # and raise a ValueError
        if browser_lowercase in supported_browsers:
            
            current_platform = str(platform.system())
            
            # User has entered a supported type, now check against google chrome
            if browser_lowercase in ('chrome', 'google chrome'):
                
                if current_platform == "Windows":
                    web_driver = self.init_chrome_webdriver(path_to_driver=self.conf.windows_chrome_webdriver_exe_path)
                
                elif current_platform == "Linux":
                    web_driver = self.init_chrome_webdriver(path_to_driver=self.conf.linux_chrome_webdriver_exe_path)
                
                elif current_platform == "Darwin":
                    web_driver = self.init_chrome_webdriver()
            
            # User has entered firefox
            elif browser_lowercase == "firefox":
                web_driver = self.init_firefox_webdriver()
            
            elif browser_lowercase == "ie":
                
                if current_platform == "Darwin" or current_platform == "Linux":
                    raise ValueError("Internet Explorer web driver is not supported on both Linux and "
                                     "Macintosh Systems.")
                
                else:
                    web_driver = self.init_ie_webdriver(path_to_driver=self.conf.windows_ie_webdriver_exe_path)
            
            # set selenium web_driver instance to our WebDriver instance
            self.web_driver = web_driver
            
            # Setting "implicit wait" configures the webdriver to allow up to this many seconds per attempt at locating
            # an element before raising an exception.
            #   -> For more details:
            #       https://docs.seleniumhq.org/docs/04_webdriver_advanced.jsp#explicit-and-implicit-waits-reference
            if callable(self.web_driver.implicitly_wait):
                self.web_driver.implicitly_wait(5)
        
        # User has entered an unsupported browser type, raise a ValueError to be caught
        else:
            raise ValueError('Webdriver for browser: {0} is currently not supported. Supported browsers: {1}'.format(
                browser_lowercase,
                supported_browsers
            ))
    
    def init_firefox_webdriver(self):
        """
        Initializes firefox webdriver. \n
        :return:        Firefox webdriver instance
        :rtype:         webdriver.Firefox \n
        """
        try:
            # profile = webdriver.FirefoxProfile("/Users/baseline/Library/Application Support/Firefox/Profiles/q186pr4v.QATests")
            # self.verify_file_path(file_path="/Users/baseline/Library/Application\\ Support/Firefox/Profiles/m27l7zv5.default")
            # profile = webdriver.FirefoxProfile("/Users/baseline/Library/Application Support/Firefox/Profiles/m27l7zv5.default")
            # driver = EFWD(webdriver.Firefox(firefox_profile=profile), self.eventListener)
            driver = EFWD(webdriver.Firefox(), self.eventListener)
        except Exception as e:
            if hasattr(e, 'msg'):
                e_text = e.msg
            else:
                e_text = e.message
            e_msg = "Exception occurred initializing FireFox webdriver. Exception received: {0}".format(e_text)
            raise Exception(e_msg)
        else:
            # Commented this out because of lack of knowledge around waits. We don't wait to wait up to 60
            # seconds for a element to be found. It should be found sooner or bomb out.
            # driver.implicitly_wait(60)
            return driver
    
    def init_chrome_webdriver(self, path_to_driver=""):
        """
        Initializes chrome webdriver. \n
        :param path_to_driver: Path to google chrome web driver \n
        :type path_to_driver: str \n
        :return: Google Chrome webdriver instance
        :rtype: webdriver.Chrome \n
        """
        try:
            if path_to_driver is not "":
                # For Linux & Windows - Verify the path
                self.verify_file_path(file_path=path_to_driver)
                driver = EFWD(webdriver.Chrome(executable_path=path_to_driver), event_listener=self.eventListener)
            else:
                # For Mac's - the driver comes with selenium, so don't need to verify any path
                driver = EFWD(webdriver.Chrome(), event_listener=self.eventListener)
        except Exception as e:
            if hasattr(e, 'msg'):
                e_text = e.msg
            else:
                e_text = e.message
            e_msg = "Exception occurred initializing Google Chrome webdriver. Exception received: {0}".format(e_text)
            raise Exception(e_msg)
        else:
            # Commented this out because of lack of knowledge around waits. We don't wait to wait up to 60
            # seconds for a element to be found. It should be found sooner or bomb out.
            # driver.implicitly_wait(60)
            return driver
    
    def init_ie_webdriver(self, path_to_driver):
        """
        Initializes a Internet Explorer web driver instance and returns it. \n
        :param path_to_driver: Path to internet explorer web driver \n
        :type path_to_driver: str \n
        :return: Internet Explorer Web Driver Instance \n
        :rtype: webdriver.Ie
        """
        try:
            self.verify_file_path(file_path=path_to_driver)
            driver = EFWD(webdriver.Ie(executable_path=path_to_driver), event_listener=self.eventListener)
        except Exception as e:
            if hasattr(e, 'msg'):
                e_text = e.msg
            else:
                e_text = e.message
            e_msg = "Exception occurred initializing Internet Explorer webdriver. Exception received: {0}".format(
                e_text
            )
            raise Exception(e_msg)
        else:
            # Commented this out because of lack of knowledge around waits. We don't wait to wait up to 60
            # seconds for a element to be found. It should be found sooner or bomb out.
            # driver.implicitly_wait(60)
            return driver
    
    def get_text_from_web_element(self, _locator):
        """
        Getter method to help getting text from footers ect. on the website.
        :param _locator:    Tuple containing Selector type, ie: By.ID, By.CSS, as well as a id to look for. \n
        :param _locator:    tuple \n
        :return:
        """
        try:
            self.wait_for_element_visible(_locator)
            web_element = self.web_driver.find_element(*_locator)
            received_text = str(web_element.text)
        except AttributeError:
            received_text = ''
        except NoSuchElementException:
            raise NoSuchElementException("Unable to locate web element [id=%s]" % str(_locator))
        
        if received_text is '':
            try:
                web_element = self.web_driver.find_element(*_locator)
                received_text = str(web_element.get_attribute('innerHTML'))
            except AttributeError:
                raise AttributeError("Unable to get 'innerHTML' text from web element [id=%s]" % str(_locator))
        return received_text
    
    def find_element_then_click(self, _locator):
        """
        Locates a web element and click's the element if it is found. Method tries 3 times before raising
        an exception.
        :param _locator:    Tuple containing Selector type, ie: By.ID, By.CSS, as well as a id to look for. \n
        :param _locator:    tuple \n
        :return:
        """
        try:
            self.wait_for_element_visible(_locator)
            element = self.web_driver.find_element(*_locator)
            
            # Firefox was having trouble with just the below call to click element:
            # element.click()
            
            # After further research, it looks like others are having more success with Firefox by using ActionChains
            # to click elements in Firefox.
            
            click_element_action = ActionChains(self.web_driver)
            click_element_action.move_to_element(to_element=element)
            click_element_action.click()
            click_element_action.perform()
        
        except WebDriverException as e:
            text = e.msg if e.msg else e.message
            e_msg = "Unable to locate element to click: {0}. Exception: {1}".format(
                _locator,
                text
            )
            raise Exception(e_msg)
        else:
            print "Found and clicked element with locator: {0}".format(_locator)
            
            if not self.conf.unit_testing:
                # Add sleep timer for any screen animations that occur
                time.sleep(0.5)
    
    def send_text_to_element(self, _locator, text_to_send, clear_field=False, drop_down_element=False):
        """

        :param _locator:
        :type _locator:

        :param text_to_send:
        :type text_to_send:

        :param clear_field:
        :type clear_field:

        :return:
        :rtype:
        """
        success = False
        count = 1
        while not success:
            
            try:
                # element = self.web_driver.find_element(by=By.ID,value='decoderselect_1')
                # click_element_action = ActionChains(self.web_driver)
                # click_element_action.move_to_element(to_element=element)
                # click_element_action.click()
                # click_element_action.perform()
                self.wait_for_element_visible(_locator)
                web_element = self.web_driver.find_element(*_locator)
                
                # New dropdown section
                if drop_down_element:
                    
                    # attempt 1 to click - Didn't work on Firefox - Worked on Chrome
                    # enter_text_into_element_action = ActionChains(self.web_driver)
                    # enter_text_into_element_action.move_to_element(to_element=web_element)
                    # enter_text_into_element_action.click(on_element=web_element)
                    # enter_text_into_element_action.perform()
                    
                    # attempt 2 to click - Didn't work on Firefox - Worked on Chrome
                    # self.web_driver.find_element_by_id(id_=_locator[1]).click()
                    
                    # attempt 3 to click - Didn't work on Firefox - Worked on Chrome
                    # web_element.click()
                    
                    # attempt 4 to click - Didn't work on Firefox or Chrome
                    # drop_down = Select(web_element)
                    # web_element.click()
                    # for option in drop_down.options:
                    #     value = option.get_attribute('value')
                    #     if value == text_to_send:
                    #         option.click()
                    #         web_element.click()
                    #         drop_down.select_by_index(1)
                    #         drop_down.select_by_visible_text(text_to_send)
                    #         drop_down.select_by_value(value=text_to_send)
                    
                    # Works on Chrome, not Firefox
                    # clear field first?
                    if clear_field:
                        web_element.clear()
                    # self.web_driver.find_elements_by_class_name('serial_select')[0].click()
                    # self.web_driver.find_elements_by_class_name('serial_select')[0].click()
                    # self.web_driver.find_element(_id_).send_keys(text_to_send)
                    # self.web_driver.find_element(_id_).send_keys(text_to_send)
                    # web_element.click()
                    
                    web_element.send_keys(text_to_send)
                    
                    # if we cleared the field first then entered text, sometimes the text doesn't "save" unless we hit
                    # enter
                    if clear_field is True:
                        web_element.send_keys(Keys.RETURN)
                    
                    success = True
                
                else:
                    # clear field first?
                    if clear_field:
                        web_element.clear()
                    
                    web_element.send_keys(text_to_send)
                    
                    # if we cleared the field first then entered text, sometimes the text doesn't "save" unless we hit
                    # enter
                    if clear_field is True:
                        web_element.send_keys(Keys.RETURN)
                    
                    # if we made it here, everything went according to plan
                    success = True
            
            # NoSuchElementException: Raised when browser cannot find the element.
            # NoSuchAttributeException: Raised when element cannot be clicked or cleared ect.
            # Exception: Handle all other exceptions that are not known to be raised at this point.
            except (NoSuchElementException, NoSuchAttributeException, Exception) as e:
                if count > 3:
                    raise ValueError("Failed to send text on third attempt to web element: {0}. '{1}'".format(
                        _locator,   # {0}
                        e.message
                    ))
                else:
                    print('retry #{0} to send text to element: {1}'.format(
                        count,      # {0}
                        _locator    # {1}
                    ))
                    
                    if not self.conf.unit_testing:
                        time.sleep(1)
                    
                    success = False
                    count += 1
    
    def handle_dialog_box(self):
        """
        Waits up to 60 seconds for a pop-up dialogue box to display.
        :return:
        """
        count = 1
        still_waiting = True
        try:
            while still_waiting:
                if self.is_visible(DialogBoxLocators.INFO_DIALOG):
                    still_waiting = False
                elif count == 10:
                    raise TimeoutException
                else:
                    count += 1
                    if not self.conf.unit_testing:
                        time.sleep(1)
                    still_waiting = True
            
            print "Pop-up alert found and recognized by automation, waiting for it to disappear."
        except TimeoutException:
            print "Pop-up alert was not found by automation, attempting to continue."
            pass
        count = 1
        while self.is_visible(DialogBoxLocators.INFO_DIALOG):
            # Added below so it doesn't print so many "Alert is present" messages to console
            if count % 100 == 0:
                print "Alert is still present"
                count = 1
            else:
                count += 1
        
        if not self.conf.unit_testing:
            time.sleep(1)
    
    def is_visible(self, _locator):
        """
        Returns if the element is both visible to the user and not click-able.
        :param _locator:    Element locator tuple which contains two values, a selector type (ie: 'id') and an id
                            value. \n
        :type _locator:     tuple \n
        :return:
        """
        try:
            return WebDriverWait(self.web_driver, 20).until(ec.visibility_of_element_located(_locator))
        except (TimeoutException, Exception):
            return False
    
    def is_clickable(self, _locator):
        """
        Returns if the element is both visible to the user and also click-able.
        :param _locator:    Element locator tuple which contains two values, a selector type (ie: 'id') and an id
                            value. \n
        :type _locator:     tuple \n
        :return:
        """
        try:
            return WebDriverWait(self.web_driver, 20).until(ec.element_to_be_clickable(_locator))
        except TimeoutException:
            return False
    
    def wait_for_element_clickable(self, _locator):
        """
        Wait for an element to be clickable for the user.
        :param _locator:    Element locator tuple which contains two values, a selector type (ie: 'id') and an id
                            value. \n
        :type _locator:     tuple \n
        :return:
        """
        first_wait = 10
        second_wait = 20
        third_wait = 30
        
        print "Waiting for element [%s] to be click-able" % str(_locator)
        for retry_number, each_wait_time in enumerate([first_wait, second_wait, third_wait]):
            try:
                # Attempt to locate it initially
                found = WebDriverWait(self.web_driver, each_wait_time).until(ec.element_to_be_clickable(_locator))
                if found:
                    print "[%s] element was found" % str(_locator)
                    return found
            except TimeoutException as e:
                if each_wait_time == third_wait:
                    exception_text = e.msg if e.msg != '' else e.message
                    raise TimeoutException("Webdriver timed out waiting for: '{0}' to be clickable. {1}".format(
                        _locator,
                        exception_text
                    ))
                else:
                    print "Failed waiting for element with ID: {id} to be click-able. Retrying.. [#{retry}]".format(
                        id=_locator[1],
                        retry=retry_number
                    )
    
    def wait_for_element_clickable_no_fail(self, _locator):
        """
        Wait for an element to be clickable for the user.
        :param _locator:    Element locator tuple which contains two values, a selector type (ie: 'id') and an id
                            value. \n
        :type _locator:     tuple \n
        :return:
        """
        first_wait = 1
        second_wait = 1
        third_wait = 2
        
        print "Waiting for element [%s] to be click-able" % str(_locator)
        for retry_number, each_wait_time in enumerate([first_wait, second_wait, third_wait]):
            try:
                # Attempt to locate it initially
                found = WebDriverWait(self.web_driver, each_wait_time).until(ec.element_to_be_clickable(_locator))
                if found:
                    print "[%s] element was found" % str(_locator)
                    return found
            except TimeoutException as e:
                if each_wait_time == third_wait:
                    # exception_text = e.msg if e.msg != '' else e.message
                    # raise TimeoutException("Webdriver timed out waiting for: '{0}' to be clickable. {1}".format(
                    #     _locator,
                    #     exception_text
                    # ))
                    return False
                else:
                    print "Failed waiting for element with ID: {id} to be click-able. Retrying.. [#{retry}]".format(
                        id=_locator[1],
                        retry=retry_number
                    )
    
    def wait_for_element_visible(self, _locator):
        """
        Waits for an element to both be visible to the user and present in the DOM. Returns the WebElement
        located if visible. Otherwise raises StaleElementReferenceException
        :param _locator:    Element locator tuple which contains two values, a selector type (ie: 'id') and an id
                            value. \n
        :type _locator:     tuple \n
        :return WebElement: Element to be located.
        :return StaleElementReferenceException: If the element was not located/not visible to the DOM/User
        """
        try:
            print "Waiting for element [%s] to be visible" % str(_locator)
            found = WebDriverWait(self.web_driver, 60).until(ec.visibility_of_element_located(_locator))
            print "[%s] element was found" % str(_locator)
            return found
        except (TimeoutException, StaleElementReferenceException) as e:
            exception_text = e.msg if e.msg != '' else e.message
            raise TimeoutException("Webdriver unable to locate element: '{0}'. {1}".format(_locator, exception_text))
    
    def wait_for_element_text_change(self, _locator, _text):
        """
        Waits for text in an element to change to the passed in text. \n

        :param _locator:    (By, Thing) - By=By.ID or 'id', Thing='id-string-to-look-for' \n
        :type _locator:     tuple \n

        :param _text:       Text to wait for \n
        :type _text:        str \n
        """
        element = ''
        
        first_wait = 10
        second_wait = 20
        third_wait = 30
        
        print "Waiting for element with ID: {id} to display text: {text}".format(id=_locator, text=_text)
        
        for retry_number, each_wait_time in enumerate([first_wait, second_wait, third_wait]):
            
            try:
                # Initially try to see if the text is present.
                element = self.web_driver.find_element(*_locator)
                ele_text = element.text if hasattr(element, "text") else ""
                if ele_text == _text:
                    return True
                
                # Attempt to wait for the text to change
                found = WebDriverWait(self.web_driver, each_wait_time).until(
                    ec.text_to_be_present_in_element(_locator, _text))
                
                # Found?
                if found:
                    print "Text changed for element with ID: {id} to: {text}".format(id=_locator, text=_text)
                    return True
            
            except TimeoutException as te:
                if each_wait_time == third_wait:
                    current_text = element.text if hasattr(element, "text") else ""
                    te_text = te.msg
                    raise TimeoutException("Timed Out: Webdriver unable to wait for element text to change to: " +
                                           "{expected}. Current text: {current}. Exception: {exception}".format(
                                               expected=_text,
                                               current=current_text,
                                               exception=te_text
                                           ))
                else:
                    print "Timed out waiting for element with ID: {id} to display: {text}. Retry #{retry} waiting " \
                          "{secs} seconds.".format(id=_locator[1], text=_text, retry=retry_number+1,
                                                   secs=each_wait_time)
    
    def get_status(self, _locator, css_property_for_status='background-color'):
        """
        Gets the status of the web element and returns the status in word form, ie: 'Done', 'Watering' ect. \n
        :param _locator:            (By, Thing) - By=By.ID or 'id', Thing='id-string-to-look-for' \n
        :type _locator:             tuple \n
        :return status_from_hex:    Status converted from rbg/hex into words, ie: 'Done', 'Watering' ect. \n
        :rtype status_from_hex:     str \n
        """
        try:
            css_prop_1 = css_property_for_status
            color_from_browser_in_rbg_tuple = self.web_driver.find_element(*_locator).value_of_css_property(css_prop_1)
        except (NoSuchElementException, NoSuchAttributeException) as e:
            # NoSuchElementException - Cannot find element.
            # NoSuchAttributeException - Element doesn't have a 'background-color' css attribute.
            nsee = NoSuchElementException
            nsae = NoSuchAttributeException
            exception_to_raise = nsee if type(e) == nsee else nsae
            text = e.msg
            e_msg = "Unable to get status for element: {0}. {1}".format(_locator, text)
            raise exception_to_raise(e_msg)
        else:
            # Convert rbg tuple into a hex value
            color_converted_to_hex = helper_methods.rbg_to_html_color(color_from_browser_in_rbg_tuple)
            
            # is a known color?
            if color_converted_to_hex not in common_vars.dictionary_for_status_colors:
                e_msg = "Color from browser not recognized: {0}. Known colors status: {1}".format(
                    str(color_converted_to_hex),
                    common_vars.dictionary_for_status_colors.keys()
                )
                raise KeyError(e_msg)
            
            # status 'wording' associated with hex color
            status_from_hex = common_vars.dictionary_for_status_colors[color_converted_to_hex]
            
            return status_from_hex
    
    def teardown_driver(self):
        try:
            self.web_driver.quit()
        except AttributeError as ae:
            # ignore trying to close the browser if it hasn't been opened yet.
            pass

    def close_window(self):
        try:
            self.web_driver.close()
        except AttributeError as ae:
            # ignore trying to close the browser if it hasn't been opened yet.
            pass
# menu-controllers-0008EE218C85 > div:nth-child(1)
# def verify_device_status_displayed(self, device_type, address, serial=None):
# """
#     Looks for the status to be displayed, if no text is received, tries for up to 30 seconds to wait for
#     the status to be displayed before timing out.
#     :return:
#     """
#     old_status = self.get_text_from_web_element(device_type=device_type, address_number=address,
#                                                     serial_number=serial, device_table_header='Status')
#     if old_status is "":
#         print "%s device status is not displayed yet." % device_type
#         new_status = old_status
#         count = 1
#         while new_status == old_status:
#             if count % 5 is 0:
#                 print "[%i seconds later] %s device status is not displayed yet." % (count, device_type)
#             elif count is 30:
#                 raise AssertionError("Timed out waiting for [%s] -> [Status]" % device_type)
#             time.sleep(1)
#             new_status = self.get_text_from_web_element(device_type=device_type, address_number=address,
#                                                             serial_number=serial, device_table_header='Status')
#             count += 1
#         print "%s status changed from [old status=%s] to [new status=%s]\n" % (device_type, old_status, new_status)

# <div class ="color" > & nbsp; < / div > background-color:  # 0155a4;
# < div class ="color" > & nbsp; < / div > background-color:  #a6aaa9;
# programRow-1 > td.status > span.statusColor.color.status-done
# menu-controllers-0008EE218C85 > div.color

#
# def verify_element_change_displayed(self, device_type, address, serial=None, device_header=None):
#     """
#     1. Gets the displayed text from the specified device_type > device_header column (ie, zone 1's description) \n
#     2. Tries for up to 30 seconds, continually trying to get the displayed text from the same element and comparing
#        the value received initially with the most recent value. \n
#     3. After 30 seconds, if the element text hasn't changed yet, execution attempts to continue. \n
#     :param device_type:
#     :param address:
#     :return:
#     """
#     # TODO: 1.  Currently this method is structured towards the 3200, where there are delays between when the 3200
#     # TODO:     controller sends information to the client and when the client receives and updates it's values.
#     # TODO: 2.  We Need a way to maybe pass in a max wait time (this would replace the "66" below, or decipher if we
#     # TODO:     are a 1000 act this way, a 3200 act that way
#     old_status = self.get_text_from_web_element(device_type=device_type, address_number=address,
#                                                     serial_number=serial, device_table_header=device_header)
#     new_status = old_status
#     count = 1
#     print "%s device %s is displayed but has not changed yet." % (device_type, device_header)
#     while new_status == old_status:
#         if count % 5 is 0:
#             print "[%i seconds later] %s device %s has not updated yet." % (count, device_type, device_header)
#         # Certain events on the 3200 occur at the top of the minute, so adjust our timeout time to be at least a
#         # minute long to account for any status change expected at the top of the minute.
#         elif count is 66:
#             break
#             # raise AssertionError("Timed out waiting for [%s] -> [%s] to change." % (device_type, device_header))
#         time.sleep(1)
#         new_status = self.get_text_from_web_element(device_type=device_type, address_number=address,
#                                                         serial_number=serial, device_table_header=device_header)
#         count += 1
#     print "%s -> %s value changed from [old status=%s] to [new status=%s]\n" % \
#           (device_type, device_header, old_status, new_status)
#     # Seems when we are waiting for an element to change, the screen 're-paints', so added a sleep to give it time
#     # to finish 're-painting' before we try accessing elements.
#     if not self.conf.unit_testing:
#         time.sleep(1)
