# Copyright 2012-2016 Baseline, Inc. (http://www.baselinesystems.com) All Rights Reserved.

import re
# from mylogging import logger.debug, logger.error, logger.info, logger.warn
import logging

# logger = logging.getLogger("basemanager")

_COMMAND_DELIMITER = r'~'
_OPCODE_DELIMITER = r'\^'
_PARAMETER_DELIMITER = r'\^'
_PROPERTY_DELIMITER = r'/'
_IDENTIFIER_DELIMITER = r':'
_VALUE_DELIMITER = r';'
_PROPERTY_ASSIGNMENT = r'='
_SUBPROPERTY_DELIMITER = r'\|'
_SUBPROPERTY_ASSIGNMENT = r'='
_ESCAPE = r'\\'

#############
_TO_ADDRESS_BYTES = 6
_FROM_ADDRESS_BYTES = 12  # The MAC of the sending controller
_BYTE_COUNT_BYTES = 4


#############################################
def split_with_escape(str_to_split, delimiter, maxsplit=0):
#############################################
    """
    This method does the same as a normal split, but uses a "negative lookbehind" regular expression
    to only split if the delimiter is *not* preceded by the _ESCAPE character
    (for example, passing in the "^" would split on all "^" characters, but would not split on a "\^" (assuming
    "\" is the _ESCAPE character)
    Errors or missing arguments result in the return of an empty list
    """
    if (str_to_split is None) or (delimiter is None):
        return []
    foo = r'(?<!' + _ESCAPE + r')' + delimiter
    regex = re.compile(foo)
    to_return = re.split(regex, str_to_split, maxsplit)
    return to_return


##############################
class Value:
##############################
    """
    A Value is zero or more strings
    """
    value_list = []

    ##################
    def __init__(self, string_in=None):
    ##################
        self.value_list = []
        self.parse(string_in)

    ##################
    def __eq__(self, other):
    ##################
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        else:
            return False

    ##################
    def __ne__(self, other):
    ##################
        return not self.__eq__(other)

    def showme(self):
        for v in self.value_list:
            try:
                pass
                # logger.debug(" \t\t\t\tvalue is " + str(v))
            except:
                pass
                # logger.error("FAILED showme value")

    def parse(self, string_in):
        """
        Parse something of the form:
            string;string;string...
        there are zero or more semicolon-seperated strings
        """
        self.value_list = []
        value_list = split_with_escape(string_in, _VALUE_DELIMITER)
        for value in value_list:
            # Don't allow a null parameter value
            if not(value is None or value == ""):
                # Unescape any escaped characters in a value string
                value = re.sub(r'[\\]', '', value)
                self.value_list.append(value)

    def getValueString(self):
        """
        Return the value list re-serialized with the value delimiter in between.
        None if there aren't any values in the list
        """
        toreturn = None
        for value in self.value_list:
            if toreturn is None:
                toreturn = value
            else:
                toreturn = toreturn + _VALUE_DELIMITER + value
        return toreturn


##############################
class Subproperty:
##############################
    """
    A subproperty has an ID string and a Value
    """

    ID = None
    value = None

    ##################
    def __init__(self, string_in=None):
    ##################
        self.ID = None
        self.value = None
        self.parse(string_in)

    ##################
    def __eq__(self, other):
    ##################
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        else:
            return False

    ##################
    def __ne__(self, other):
    ##################
        return not self.__eq__(other)

    ##################
    def showme(self, FromString=None):
    ##################
        if FromString is None:
            fromstr = ""
        else:
            fromstr = "From: \t" + FromString
        # logger.info("Subproperty " + fromstr)
        if self.ID is not None:
            # logger.debug("\t\t--------- Subproperty ID:" + self.ID)
            self.value.showme()

    ##################
    def parse(self, string_in):
    ##################
        fixedproperty = split_with_escape(string_in, _SUBPROPERTY_ASSIGNMENT)
        if len(fixedproperty) != 2:
            self.ID = None
            self.value = None
        else:
            self.ID = fixedproperty[0]
            self.value = Value(fixedproperty[1])

    ##################
    def getValueString(self):
    ##################
        return self.value.getValueString()


##############################
class Subproperties:
##############################
    """"
    Zero or more subproperties, separated by the subproperty delimiter ( | )
    SubpropertyID=Value | SubpropertyID=value
    """
    subproperty_list = []

    ##################
    def __init__(self, string_in=None):
    ##################
        self.subproperty_list = []
        self.parse(string_in)

    ##################
    def __eq__(self, other):
    ##################
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        else:
            return False

    ##################
    def __ne__(self, other):
    ##################
        return not self.__eq__(other)

    ##################
    def showme(self, from_string=None):
    ##################
        for singlep in self.subproperty_list:
            singlep.showme(from_string)

    ##################
    def parse(self, string_in):
    ##################
        self.subproperty_list = []
        a_subproperty_str_list = split_with_escape(string_in, _SUBPROPERTY_DELIMITER)
        for a_subprop_str in a_subproperty_str_list:
            self.subproperty_list.append(Subproperty(a_subprop_str))

    ################
    def getSubproperties(self):
    ################
        """
        Just return a list, maybe empty, of all the properties
        """
        return self.subproperty_list

    ################
    def getSubpropertyByID(self, id_string_in):
    ################
        """
        Find the subproperty matching the ID.   If found, return it, otherwise return None
        """
        for subprop in self.subproperty_list:
            if subprop.ID == id_string_in:
                return subprop
        return None

    ################
    def replaceSubpropertyByID(self, id_string_in, new_value_str):
    ################
        """
        Find the subproperty matching the ID.   If found, change it's value to the new value, and return a True.
        Otherwise, return False
        """
        newValue = Value(new_value_str)
        prop = self.getSubpropertyByID(id_string_in)
        if prop is not None:
            prop.value = newValue
            return True
        else:
            return False

    ################
    def getSubpropertyValueStringByID(self, id_string_in):
    ################
        prop = self.getSubpropertyByID(id_string_in)
        if prop is not None:
            return prop.getValueString()
        else:
            return None


##############################
class Property:
##############################
    """
    A property has an ID string, a Value object, and possibly a Subproperties object
    The basic form is:  PropertyID=Value | SubpropertyID=Value | SubpropertyID=Value...
    """

    ID = None
    value = None
    subproperties = None

    ##################
    def __init__(self, string_in=None):
    ##################
        self.ID = None
        self.value = None
        self.subproperties = None
        self.parse(string_in)

    ##################
    def __eq__(self, other):
    ##################
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        else:
            return False

    ##################
    def __ne__(self, other):
    ##################
        return not self.__eq__(other)

    ##################
    def showme(self, FromString=None):
    ##################
        if FromString is None:
            fromstr = ""
        else:
            fromstr = "From: \t" + FromString
        # logger.info("Property " + fromstr)
        if self.ID is not None:
            # logger.debug("\t\t--------- Property ID:" + self.ID)
            self.value.showme()

    ##################
    def parse(self, string_in):
    ##################
        self.ID = None
        self.value = None
        self.subproperties = None
        value_str = string_in
        # if there are subproperties, peel them off from the propertyID and Value
        value_and_subproperties = split_with_escape(string_in, _SUBPROPERTY_DELIMITER, maxsplit=1)
        if len(value_and_subproperties) == 2:
            # We've got some subproperties, so parse them out
            self.subproperties = Subproperties(value_and_subproperties[1])
            # Grab just the property ID and Value for later processing
            value_str = value_and_subproperties[0]

        # Parse out the property ID and Value
        fixedproperty = split_with_escape(value_str, _PROPERTY_ASSIGNMENT)
        if len(fixedproperty) == 2:
            self.ID = fixedproperty[0]
            self.value = Value(fixedproperty[1])

    ##################
    def getValueString(self):
    ##################
        return self.value.getValueString()


##############################
class Properties:
##############################
    """"
    Zero or more properties
    """
    property_list = []

    ##################
    def __init__(self, string_in=None):
    ##################
        self.property_list = []
        self.parse(string_in)

    ##################
    def __eq__(self, other):
    ##################
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        else:
            return False

    ##################
    def __ne__(self, other):
    ##################
        return not self.__eq__(other)

    ##################
    def showme(self, from_string=None):
    ##################
        for singlep in self.property_list:
            singlep.showme(from_string)

    ##################
    def parse(self, string_in):
    ##################
        self.property_list = []
        a_property_str_list = split_with_escape(string_in, _PROPERTY_DELIMITER)
        for a_prop_str in a_property_str_list:
            self.property_list.append(Property(a_prop_str))

    ################
    def getProperties(self):
    ################
        """
        Just return a list, maybe empty, of all the properties
        """
        return self.property_list

    ################
    def getPropertyByID(self, id_string_in):
    ################
        """
        Find the property matching the ID.   If found, return it, otherwise return None
        """
        for prop in self.property_list:
            if prop.ID == id_string_in:
                return prop
        return None

    ################
    def replacePropertyByID(self, id_string_in, new_value_str):
    ################
        """
        Find the property matching the ID.   If found, change it's value to the new value, and return a True.
        Otherwise, return False
        """
        newValue = Value(new_value_str)
        prop = self.getPropertyByID(id_string_in)
        if prop is not None:
            prop.value = newValue
            return True
        else:
            return False

    ################
    def getPropertyValueStringByID(self, id_string_in):
    ################
        prop = self.getPropertyByID(id_string_in)
        if prop is not None:
            return prop.getValueString()
        else:
            return None


##############################
class Parameter:
##############################
    ID = None
    value = None
    properties = None

    # TODO: Some of the processing code uses this for now.   Until I can come up with a rewrite, I'll leave it in
    raw_string = ""

    ##################
    def __init__(self, string_in=None):
    ##################
        self.ID = None
        self.value = None
        self.properties = None
        self.raw_string = string_in
        self.parse(string_in)

    ##################
    def __eq__(self, other):
    ##################
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        else:
            return False

    ##################
    def __ne__(self, other):
    ##################
        return not self.__eq__(other)

    ################
    def showme(self, FromString=None):
    ################
        if FromString is None:
            fromstr = ""
        else:
            fromstr = "From: \t" + FromString
        # logger.info("--- at showme of a Parameter " + fromstr)
        # logger.debug("---------- PARAMETER ID : " + self.ID)
        self.value.showme()
        # logger.debug(self.properties.showme(FromString))
        # logger.info("\n")

    ################
    def parse(self, string_in):
    ################
        """
        Parse a parameter of the form:
            ID:Value/Properties
        where ID and Value are required, and the /Properties is optional
        """
        parameter = split_with_escape(string_in, _IDENTIFIER_DELIMITER)
        if len(parameter) != 2:
            self.ID = None
            self.value = None
            self.properties = None
        else:
            self.ID = parameter[0]
            parameter_value_str = parameter[1]
            # The Parameter value is set apart the Properties with the same delimiter ("/") that seperates each of
            # the Properties.   So, do only the first split (if any), so that we're broken apart into the "value"
            # and the "properties"
            value_and_properties = split_with_escape(parameter_value_str, _PROPERTY_DELIMITER, maxsplit=1)
            if len(value_and_properties) == 1:
                self.value = Value(value_and_properties[0])
                self.properties = None
            elif len(value_and_properties) > 1:
                self.value = Value(value_and_properties[0])
                self.properties = Properties(value_and_properties[1])

    ################
    def getPropertyByID(self, id_string_in):
    ################
        """
        Find the property matching the ID.   If found, return it, otherwise return None
        """
        if self.properties is not None:
            return self.properties.getPropertyByID(id_string_in)
        else:
            return None

    ################
    def replacePropertyByID(self, id_string_in, newValue_str):
    ################
        """
        Find the property matching the ID.   If found, change it's value to the new value, and return a True.
        Otherwise, return False
        """
        if self.properties is not None:
            return self.properties.replacePropertyByID(id_string_in, newValue_str)
        else:
            return False

    ################
    def getRawString(self):
    ################
        return self.raw_string

    ################
    def getValueString(self):
    ################
        return self.value.getValueString()

    ################
    def getPropertyValueStringByID(self, id_string_in):
    ################
        if self.properties is not None:
            return self.properties.getPropertyValueStringByID(id_string_in)
        else:
            return None


##############################
class Parameters:
##############################
    """
    list of zero or more parameters
    """
    parameter_list = []

    # TODO: Some of the processing code uses this for now.   Until I can come up with a rewrite, I'll leave it in
    raw_string = ""

    ##################
    def __init__(self, string_in=None):
    ##################
        self.parameter_list = []
        self.raw_string = string_in
        self.parse(string_in)

    ##################
    def __eq__(self, other):
    ##################
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        else:
            return False

    ##################
    def __ne__(self, other):
    ##################
        return not self.__eq__(other)

    ################
    def showme(self, FromString=None):
    ################
        if FromString is None:
            fromstr = ""
        else:
            fromstr = "From: \t" + FromString

        # logger.debug("SHOWME of a Parameters, length of list is: " + str(len(self.parameter_list)))
        for singlep in self.parameter_list:
            try:
                singlep.showme(fromstr)
            except:
                pass
                # logger.error("FAILED to parameter.showme show_parameter multiple")

    ################
    def getParameterByID(self, id_string_in):
    ################
        """
        Find the parameter with the given ID.
        Return None if not found
        """
        result = None
        for param in self.parameter_list:
            if param.ID == id_string_in:
                result = param
                break
        return result

    ################
    def getParameterValueByID(self, id_string_in):
    ################
        """
        Find the parameter via it's ID, and return it's value
        Return empty string if not found
        """
        param = self.getParameterByID(id_string_in)
        if param is not None:
            return param.value
        else:
            return None

    ################
    def getParameterValueStringByID(self, id_string_in):
    ################
        param = self.getParameterByID(id_string_in)
        if param is not None:
            return param.getValueString()
        else:
            return None

    ################
    def parse(self, string_in):
    ################
        """
        Parse a list of parameters of the form:
           Parameter^Parameter...
        where there can be zero or more parameters
        """
        self.parameter_list = []

        parameter_token_list = split_with_escape(string_in, _PARAMETER_DELIMITER)
        for parameter_token in parameter_token_list:
            parameter = Parameter(parameter_token)
            self.parameter_list.append(parameter)

    ################
    def getParameters(self):
    ################
        return self.parameter_list

    ################
    def reorderbyID(self, sort_id_list):
    ################
        # Reorder the parameters in the ID-list order given in sort_id_list, appending any leftover parameters at
        # the end of the sorted list.
        # Yeah, the implementation sucks (a few list comprehensions would simplify the heck out of it),
        # but it's not a pain point, so I'm not going to waste time making it more efficient
        pickfrom = self.parameter_list
        ordered = []
        for theID in sort_id_list:
            elements_taken = []
            for i, param in enumerate(pickfrom):
                if param.ID == theID:
                    ordered.append(param)
                    elements_taken.append(i)
            # Remove all the "picked" elements from the parameter list, starting at the far end of the list
            for i in reversed(elements_taken):
                del pickfrom[i]

        # Any leftover parameters in the list get tacked on after the parameters in the sort_id_list
        while pickfrom:
            ordered.append(pickfrom.pop())
        self.parameter_list = ordered

    ################
    def getRawString(self):
    ################
        return self.raw_string


##############################
class Command:
##############################
    """
    A single command contains an opcode and parameters, of the form:
    Opcode^Parameters
    """
    opcode = None
    parameters = None

    ##################
    def __init__(self, string_in=None):
    ##################
        self.opcode = None
        self.parameters = None
        self.parse(string_in)

    ##################
    def __eq__(self, other):
    ##################
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        else:
            return False

    ##################
    def __ne__(self, other):
    ##################
        return not self.__eq__(other)

    #########
    def showme(self):
    #########
        # logger.debug("\tOPCODE is: " + str(self.opcode))
        self.parameters.showme()

    #########
    def parse(self, string_in):
    #########
        # In a fit of chowderheadedness, the opcode is seperated from the parameters with the same
        # character that seperates the parameters.   So first split on the first unescaped "^", and pass the
        # of the the string into the Parameters constructor for further processing
        c_list = split_with_escape(string_in, _OPCODE_DELIMITER, maxsplit=1)
        if len(c_list) == 2:
            self.opcode = c_list[0]
            self.parameters = Parameters(c_list[1])
        else:
            self.opcode = None
            self.parameters = None

    ################
    def getParameters(self):
    ################
        return self.parameters


##############################
class Message:
##############################
    """
    Message = Command~Command~Command...
    """
    commands = []
    first_opcode = None

    ##################
    def __init__(self, string_in=None):
    ##################
        self.commands = []
        self.first_opcode = None
        self.parse(string_in)

    ##################
    def __eq__(self, other):
    ##################
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        else:
            return False

    ##################
    def __ne__(self, other):
    ##################
        return not self.__eq__(other)

    #########
    def showme(self):
    #########
        # logger.debug("\tOPCODE is: " + str(self.first_opcode))
        # logger.debug("")
        for command in self.commands:
            command.showme()

    #########
    def parse(self, string_in):
    #########
        """
        Parse out a list of one or more Commands, of the form:
            Command~Command~Command...
        """
        command_list = split_with_escape(string_in, _COMMAND_DELIMITER)
        first = True
        for the_command_string in command_list:
            a_command = Command(the_command_string)
            if first:
                first = False
                self.first_opcode = a_command.opcode
            self.commands.append(a_command)

    ################
    def getCommands(self):
    ################
        return self.commands


# ##############################
# class Message:
# ##############################
#     """
#     One or more Commands
#     """
#     commands = Commands()
#
#     ##################
#     def __init__(self):
#     ##################
#         self.commands = Commands()
#
#     #########
#     def showme(self):
#     #########
#         self.commands.showme()
#
#     #########
#     def parse(self, string_in):
#     #########
#         logger.info("=============   At message parse string going in is: " + string_in)
#         self.commands = Commands()
#         # first element is the opcode, then the rest
#         try:
#             self.commands.parse(string_in)
#             # logger.info("FINISHED THE COMMANDS PARSE")
#         except:
#             logger.error("didnt complete the commands parse at Message parse")
#             self.commands.showme()


###############
class Packet:
###############
    """
    A Packet consists of:
       To Address + From Address + Message Length + Message
    """
    to_address = None
    from_address = None
    byte_count = int(0)
    message = None
    # Note -- only one of these is used.  Either the socket is to/from a controller
    # Or it's to/from a USER    the wss_common.this_connection knows for sure
    # controllerID = int(0)
    # userID = int(0)
    ID = None

    ##################
    def __init__(self, string_in=None):
    ##################
        self.to_address = None
        self.from_address = None
        self.byte_count = int(0)
        self.message = None
        self.ID = None
        self.valid = False
        self.parse(string_in)

    ##################
    def __eq__(self, other):
    ##################
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        else:
            return False

    ##################
    def __ne__(self, other):
    ##################
        return not self.__eq__(other)

    #########
    def showme(self):
    #########
        # logger.info("to_address is: " + str(self.to_address))
        # logger.info("from_address is: " + str(self.from_address))
        # logger.info("byte_count is: " + str(self.byte_count))
        self.message.showme()

    #########
    def parse(self, string_in):
    #########
        # This is pretty much the entire raw string
        # The format of the incoming packet is: ToAddress FromAddress ByteCount Message
        self.to_address = None
        self.from_address = None
        self.byte_count = int(0)
        self.message = None
        self.ID = None
        minLegalPacketLength = _TO_ADDRESS_BYTES + _FROM_ADDRESS_BYTES + _BYTE_COUNT_BYTES + 8
        if (string_in is not None) and (len(string_in) >= minLegalPacketLength):
            from_address_start = _TO_ADDRESS_BYTES
            from_address_end = from_address_start + _FROM_ADDRESS_BYTES
            byte_count_start = from_address_end
            byte_count_end = byte_count_start + _BYTE_COUNT_BYTES
            self.to_address = string_in[:_TO_ADDRESS_BYTES]
            self.from_address = string_in[from_address_start:from_address_end]
            packet_from_mac_address = self.from_address
            # logger.info("from: " + str(self.from_address))
            # logger.info("packet_from_mac is: " + str(packet_from_mac_address))
            self.byte_count = int(string_in[byte_count_start:byte_count_end])
            message_start = byte_count_end
            message_end = message_start + self.byte_count
            the_message_string = string_in[message_start:message_end]
            should_be = len(string_in) - message_start
            if self.byte_count != should_be:
                pass
            # logger.warn("\tFAILURE -- Byte count of packet didn't match actual length.  Expecting: " + str(
            # self.byte_count) + " Actual length:" + str(should_be))
            self.message = Message(the_message_string)
            self.ID = self.from_address
            self.valid = True

# vi:sts=4 sw=4 et 
