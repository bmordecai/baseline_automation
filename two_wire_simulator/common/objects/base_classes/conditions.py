from common import helper_methods
from decimal import Decimal
from common.imports import opcodes
from common.objects.base_classes.ser import Ser
from common.objects.base_classes.base_methods import BaseMethods
# from common.imports.types import StartStopPauseCondition, StartStopPauseEvent
# from common.objects.object_bucket import temperature_sensors, event_switches, moisture_sensors
from common.imports.types import ActionCommands, BiCoderCommands, ProgramCommands
from common.objects.base_classes import messages

__author__ = 'ben'


# This replaced the pf_START_STOP_PAUSE_object
# Base Class: Condition object
#
class BaseConditions(BaseMethods):
    """
    Creates a condition that causes a program to start/stop/pause.  \n

    Usage (must be done in this sequence): \n
        - creating start condition instance: \n
            new_instance_name = StartConditionFor1000(program_ad=1) \n

        - sending the programming to the controller: \n
            new_instance_name.set_moisture_condition_on_pg(serial_number="XXX", mode="XXX", threshold="XXX") \n

    Note: Only one of each type of condition can be set with one of each type: \n
        i.e. you can have one start condition for a moisture sensor and one start condition for a moisture sensor,
         but you can't have two start conditions for a moisture sensor. \n
    """

    def __init__(self, _controller, _event_type, _identifiers, _program_ad=None, _watersource_ad=None):
        """
        :param _controller:      The controller that will be passed in to each water source made by this method. \n
        :type _controller:       common.objects.controllers.bl_32.BaseStation3200 |
                                common.objects.controllers.bl_10.BaseStation1000 \n

        :param _program_ad:     The program that will be attached to this condition \n
        :type _program_ad:      int \n

        :param _watersource_ad: The water_source that will be attached to this condition \n
        :type _watersource_ad:  int \n

        :param _event_type:     Either a start stop or pause condition ('PP' for stop | 'PS' for pause | 'PT' for start) \n
                                or an empty/full condition ('EM' for empty | 'FU' for full) \n
        :type _event_type:      str \n

        :param _identifiers:    Key value pairs that define this controller object. \n
        :type _identifiers:     list[list[str]] \n
        """
        # Init parent class to inherit respective attributes
        BaseMethods.__init__(self, _ser=_controller.ser, _type=_event_type, _identifiers=_identifiers)

        self.controller = _controller

        # TODO not sure if these should be here just yet
        self.program_ad = _program_ad
        self.watersource_ad = _watersource_ad

        # # for 1000's only
        # self._ty = None         # event type

        # ---------------------------------------------- #
        # Init all condition specific attributes to None #
        # ---------------------------------------------- #
        # # STOP
        # self.si = None         # Stop immediately: TR | FA
        #
        # # PAUSE
        #
        # # START
        # # 1000 ONLY START:
        # self.cc = None  # Calibration cycle
        # self.dt = None  # Start on date & time (i.e., 'TR'=opcodes.true | 'FA'=opcodes.false)
        # self.ci = None  # Calendar interval (i.e., Even='EV', Odd='OD', Odd Skip 31='OS')
        # self.di = None  # Day Interval (i.e., 1 to xx days)
        # self.wd = None  # Week days: binary days of week SUN - SAT (i.e., '0111110'=water only on weekdays)
        # self.smi = None  # Semi-month Interval (i.e, a list with 24 int values)
        # self.st = None  # Start times - minutes past midnight (i.e., 8:00am = 480)
        #                  # If multiple start times, input as list: [480, 540, 600]=[8am, 9am, 10am]

        # Interval type (1000 ONLY) - only gets set if user add's a date/time attribute to start condition
        self.interval_ty = None

        # Place Holder for program data
        self.data = None

        # Holds the messages return value
        self.build_message_string = ''

    ###############################
    def verify_myself(self, expected_status=None, skip_get_data=False):
        """
        Verifies this condition's values against what the controller has. \n

        :param expected_status:     An expected status to verify against. \n
        :type expected_status:      str \n
        """
        if not skip_get_data:
            self.get_data()

        self.verify_enabled_state()
        if expected_status is not None:
            self.verify_status(_expected_status=expected_status)

