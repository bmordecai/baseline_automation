from common.imports import opcodes
from common.objects.messages.base_messages import BaseMessages
from common.imports.types import ActionCommands, ControllerCommands
from common.imports.types import Message
from datetime import datetime, timedelta

__author__ = 'Baseline'


class BL3200Messages(BaseMessages):

    #################################
    def __init__(self, _controller_object):
        """
        :param _controller_object:     Controller object that will be referenced throughout this module. \n
        :type _controller_object:      common.objects.controllers.bl_32.BaseStation3200 \n
        """
        self.bl_32 = _controller_object
        self.status_code = ''
        # Init parent class to inherit respective attributes
        BaseMessages.__init__(self, _controller=_controller_object)
        # Data Required
        self.restore_method = None  # This is either (BaseManager | USB Drive | Internal Flash)


    #################################
    def build_message_header(self):
        message_header = '{0}: {1}n'.format(
            str('Controller'),
            self.bl_32.ds
        )
        return message_header

    #################################
    def build_message_tx(self, _status_code):
        """
        :param _status_code:
        :type _status_code: str

        """
        tx_msg = self.build_message_header()

        # Create the title and header for the controller
        if _status_code is Message.restore_failed:
            tx_msg += "{0}{1}{2}{3}{4}".format(
                "Restore Programming Failed:n",     # {0}
                "The backup file is missingn",      # {1}
                "or is corrupt.n",                  # {2}
                "File from = ",                     # {3}
                self.restore_method                 # {4}
            )
        elif _status_code is Message.restore_successful:
            tx_msg += "{0}{1}{2}{3}{4}".format(
                "Restore Programming Successful:n",     # {0}
                "The backup file has beenn",            # {1}
                "successfully restored.n",              # {2}
                "File from = ",                         # {3}
                self.restore_method                     # {4}
            )
        elif _status_code is Message.commander_paused:
            tx_msg += "{0}{1}{2}{3}".format(
                "BL-Commander: ",                   # {0}
                "Run Zonen",                        # {1}
                "All watering has been pausedn",   # {2}
                "for manual operations.",           # {3}
            )
        elif _status_code is Message.boot_up:
            tx_msg += "{0}{1}{2}".format(
                "Controller has been reset:n",
                "Either from a power failure orn",
                "a controller reset operation."
            )
        elif _status_code is Message.usb_flash_storage_failure:
            tx_msg += "{0}{1}".format(
                "USB Error:n",
                "Unable to access the USB flash drive."
            )
        elif _status_code is Message.event_date_stop:
            tx_msg += "{0}{1}{2}".format(
                "Event Date Blocked:n",
                "All watering has been stoppedn",
                "for this date."
            )
        elif _status_code is Message.flow_jumper_stopped:
            tx_msg += "{0}{1}{2}".format(
                "Flow Contacts Triggered:n",
                "All watering has been stopped.n",
                "Value = Open"
            )
        elif _status_code is Message.two_wire_high_current_shutdown:
            tx_msg += "{0}{1}{2}{3}{4}".format(
                "Two-wire Failure:n",
                "High current indicates an",
                "short circuit.n",
                "Current = ",
                str(self.bl_32.va)
            )
        elif _status_code is Message.memory_usage:
            tx_msg += "{0}{1}{2}{3}{4}".format(
                "Controller Memory Low:n",
                "Program too large and complex.n",
                "Simplify or split program.n",
                "Memory Used: ",
                str(self.bl_32.mu) + "%"
            )
        elif _status_code is Message.off:
            tx_msg += "{0}{1}{2}{3}".format(
                "Dial turned to OFF:n",
                "All watering has been stopped.n",
                "All watering halted until dialn",
                "is turned to RUN."
            )
        elif _status_code is Message.pause_jumper:
            tx_msg += "{0}{1}{2}".format(
                "Pause Contacts Triggered:n",
                "All watering has been pausedn",
                "for four hours minimum."
            )
        elif _status_code is Message.rain_delay_stopped:
            # For this message, we need to get the current controller date and increment it by the number of pause days
            date_string = self.bl_32.date_mngr.controller_datetime.date_string_for_controller()
            current_date = datetime.strptime(date_string, '%m/%d/%Y')
            date_after_pause_days = current_date + timedelta(days=self.bl_32.rp)   # Increment by pause days

            tx_msg += "{0}{1}{2}{3}{4}{5}{6}".format(
                    "Rain Days Active:n",
                    "All watering has been stopped.n",
                    "Value = ",
                    str(self.bl_32.rp) + " daysn",
                    "Until = ",
                    date_after_pause_days.strftime(format='%m/%d/%y'),
                    " 12:00:00 AM"
                )
        elif _status_code is Message.rain_jumper_stopped:
            tx_msg += "{0}{1}{2}{3}".format(
                "Rain Stop Contacts Triggered:n",
                "All watering has been stopped.n",
                "Value = Openn",
                "Until = Closed"
            )
        return tx_msg

    #################################
    def build_alarm_id(self, _status_code):
        """
        This return the alarm string.

        :param _status_code:
        :type _status_code:
        :return:
        :rtype:
        """
        # If the status code is 'CE' we have a 'NC' in the status
        alarm_id = '{0}_{1}_{2}_{3}'.format(
            str(000).zfill(3),          # {0} controller id number
            opcodes.device_value,       # {1}
            self.bl_32.sn,              # {2} controller serial number
            _status_code                # {3}
        )
        return alarm_id

    # |----------------------------------------------------------|
    #         Message: Controller Restore Failed BaseManager
    # |----------------------------------------------------------|
    #################################
    def set_restore_failed_from_basemanager_message(self):
        self.restore_method = "BaseManager"
        required_data = "{0}={1}".format(opcodes.variable_2, self.restore_method)
        self.send_message(
            _action=ActionCommands.SET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.restore_failed,
            _data_required_string=required_data)

    #################################
    def get_restore_failed_from_basemanager_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.restore_failed)

    #################################
    def clear_restore_failed_from_basemanager_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.restore_failed)

    def check_for_restore_failed_from_basemanager_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=ControllerCommands.Controller,
                _identifier=None,
                _status_code=Message.restore_failed)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_restore_failed_from_basemanager_message(self):
        self.get_restore_failed_from_basemanager_message()
        self.verify_message(_status_code=Message.restore_failed)

    # |----------------------------------------------------------|
    #         Message: Controller Restore Failed USB Drive
    # |----------------------------------------------------------|
    #################################
    def set_restore_failed_from_usb_drive_message(self):
        self.restore_method = "USB Drive"
        required_data = "{0}={1}".format(opcodes.variable_2, self.restore_method)
        self.send_message(
            _action=ActionCommands.SET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.restore_failed,
            _data_required_string=required_data)

    #################################
    def get_restore_failed_from_usb_drive_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.restore_failed)

    #################################
    def clear_restore_failed_from_usb_drive_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.restore_failed)

    def check_for_restore_failed_from_usb_drive_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=ControllerCommands.Controller,
                _identifier=None,
                _status_code=Message.restore_failed)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_restore_failed_from_usb_drive_message(self):
        self.get_restore_failed_from_usb_drive_message()
        self.verify_message(_status_code=Message.restore_failed)

    # |-------------------------------------------------------------|
    #         Message: Controller Restore Failed Internal Flash
    # |-------------------------------------------------------------|
    #################################
    def set_restore_failed_from_internal_flash_message(self):
        self.restore_method = "Internal Flash"
        required_data = "{0}={1}".format(opcodes.variable_2, self.restore_method)
        self.send_message(
            _action=ActionCommands.SET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.restore_failed,
            _data_required_string=required_data)

    #################################
    def get_restore_failed_from_internal_flash_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.restore_failed)

    #################################
    def clear_restore_failed_from_internal_flash_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.restore_failed)

    def check_for_restore_failed_from_internal_flash_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=ControllerCommands.Controller,
                _identifier=None,
                _status_code=Message.restore_failed)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_restore_failed_from_internal_flash_message(self):
        self.get_restore_failed_from_internal_flash_message()
        self.verify_message(_status_code=Message.restore_failed)

    # |----------------------------------------------------------|
    #         Message: Controller Restore Success BaseManager
    # |----------------------------------------------------------|
    #################################
    def set_restore_successful_from_basemanager_message(self):
        self.restore_method = "BaseManager"
        required_data = "{0}={1}".format(opcodes.variable_2, self.restore_method)
        self.send_message(
            _action=ActionCommands.SET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.restore_successful,
            _data_required_string=required_data)

    #################################
    def get_restore_successful_from_basemanager_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.restore_successful)

    #################################
    def clear_restore_successful_from_basemanager_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.restore_successful)

    def check_for_restore_successful_from_basemanager_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=ControllerCommands.Controller,
                _identifier=None,
                _status_code=Message.restore_successful)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_restore_successful_from_basemanager_message(self):
        self.get_restore_successful_from_basemanager_message()
        self.verify_message(_status_code=Message.restore_successful)

    # |----------------------------------------------------------|
    #         Message: Controller Restore Success USB Drive
    # |----------------------------------------------------------|
    #################################
    def set_restore_successful_from_usb_drive_message(self):
        self.restore_method = "USB Drive"
        required_data = "{0}={1}".format(opcodes.variable_2, self.restore_method)
        self.send_message(
            _action=ActionCommands.SET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.restore_successful,
            _data_required_string=required_data)

    #################################
    def get_restore_successful_from_usb_drive_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.restore_successful)

    #################################
    def clear_restore_successful_from_usb_drive_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.restore_successful)

    def check_for_restore_successful_from_usb_drive_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=ControllerCommands.Controller,
                _identifier=None,
                _status_code=Message.restore_successful)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_restore_successful_from_usb_drive_message(self):
        self.get_restore_successful_from_usb_drive_message()
        self.verify_message(_status_code=Message.restore_successful)

    # |-------------------------------------------------------------|
    #         Message: Controller Restore Success Internal Flash
    # |-------------------------------------------------------------|
    #################################
    def set_restore_successful_from_internal_flash_message(self):
        self.restore_method = "Internal Flash"
        required_data = "{0}={1}".format(opcodes.variable_2, self.restore_method)
        self.send_message(
            _action=ActionCommands.SET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.restore_successful,
            _data_required_string=required_data)

    #################################
    def get_restore_successful_from_internal_flash_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.restore_successful)

    #################################
    def clear_restore_successful_from_internal_flash_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.restore_successful)

    def check_for_restore_successful_from_internal_flash_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=ControllerCommands.Controller,
                _identifier=None,
                _status_code=Message.restore_successful)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_restore_successful_from_internal_flash_message(self):
        self.restore_method = 'Internal Flash'
        self.get_restore_successful_from_internal_flash_message()
        self.verify_message(_status_code=Message.restore_successful, _verify_date_time=False)

    # |----------------------------------------------------|
    #         Message: Controller BL Commander Pause
    # |----------------------------------------------------|
    #################################
    def set_bl_commander_pause_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.commander_paused)

    #################################
    def get_bl_commander_pause_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.commander_paused)

    #################################
    def clear_bl_commander_pause_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.commander_paused)

    def check_for_bl_commander_pause_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=ControllerCommands.Controller,
                _identifier=None,
                _status_code=Message.commander_paused)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_bl_commander_pause_message(self):
        self.get_bl_commander_pause_message()
        self.verify_message(_status_code=Message.commander_paused)

    # |----------------------------------------------------|
    #         Message: Controller Boot Up
    # |----------------------------------------------------|
    #################################
    def set_boot_up_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.boot_up)

    #################################
    def get_boot_up_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.boot_up)

    #################################
    def clear_boot_up_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.boot_up)

    def check_for_boot_up_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=ControllerCommands.Controller,
                _identifier=None,
                _status_code=Message.boot_up)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_boot_up_message(self):
        self.get_boot_up_message()
        self.verify_message(_status_code=Message.boot_up)

    # |----------------------------------------------------|
    #         Message: Controller USB Storabe Failure
    # |----------------------------------------------------|
    #################################
    def set_usb_storage_failure_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.usb_flash_storage_failure)

    #################################
    def get_usb_storage_failure_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.usb_flash_storage_failure)

    #################################
    def clear_usb_storage_failure_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.usb_flash_storage_failure)

    def check_for_usb_storage_failure_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=ControllerCommands.Controller,
                _identifier=None,
                _status_code=Message.usb_flash_storage_failure)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_usb_storage_failure_message(self):
        self.get_usb_storage_failure_message()
        self.verify_message(_status_code=Message.usb_flash_storage_failure)

    # |----------------------------------------------------|
    #         Message: Controller Event Date Stop
    # |----------------------------------------------------|
    #################################
    def set_event_date_stop_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.event_date_stop)

    #################################
    def get_event_date_stop_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.event_date_stop)

    #################################
    def clear_event_date_stop_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.event_date_stop)

    def check_for_event_date_stop_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=ControllerCommands.Controller,
                _identifier=None,
                _status_code=Message.event_date_stop)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_event_date_stop_message(self):
        self.get_event_date_stop_message()
        self.verify_message(_status_code=Message.event_date_stop)

    # |----------------------------------------------------|
    #         Message: Controller Flow Stop Jumper
    # |----------------------------------------------------|
    #################################
    def set_flow_jumper_stopped_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.flow_jumper_stopped)

    #################################
    def get_flow_jumper_stopped_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.flow_jumper_stopped)

    #################################
    def clear_flow_jumper_stopped_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.flow_jumper_stopped)

    def check_for_flow_jumper_stopped_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=ControllerCommands.Controller,
                _identifier=None,
                _status_code=Message.flow_jumper_stopped)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_flow_jumper_stopped_message(self):
        self.get_flow_jumper_stopped_message()
        self.verify_message(_status_code=Message.flow_jumper_stopped)

    # |-----------------------------------------------------------------|
    #         Message: Controller Controller Two Wire Short Circuit
    # |-----------------------------------------------------------------|
    #################################
    def set_two_wire_short_circuit_message(self):
        data_required_string = "{0}={1}".format(opcodes.variable_2, self.bl_32.va)
        self.send_message(
            _action=ActionCommands.SET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.two_wire_high_current_shutdown,
            _data_required_string=data_required_string)

    #################################
    def get_two_wire_short_circuit_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.two_wire_high_current_shutdown)

    #################################
    def clear_two_wire_short_circuit_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.two_wire_high_current_shutdown)

    def check_for_two_wire_short_circuit_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=ControllerCommands.Controller,
                _identifier=None,
                _status_code=Message.two_wire_high_current_shutdown)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_two_wire_short_circuit_message(self):
        self.get_two_wire_short_circuit_message()
        self.verify_message(_status_code=Message.two_wire_high_current_shutdown)

    # |-----------------------------------------------------------------|
    #         Message: Controller Controller Two Wire Short Circuit
    # |-----------------------------------------------------------------|
    #################################
    def set_memory_usage_message(self):
        data_required_string = "{0}={1}".format(opcodes.variable_2, str(self.bl_32.mu) + "%")
        self.send_message(
            _action=ActionCommands.SET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.memory_usage,
            _data_required_string=data_required_string)

    #################################
    def get_memory_usage_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.memory_usage)

    #################################
    def clear_memory_usage_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.memory_usage)

    def check_for_memory_usage_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=ControllerCommands.Controller,
                _identifier=None,
                _status_code=Message.memory_usage)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_memory_usage_message(self):
        self.get_memory_usage_message()
        self.verify_message(_status_code=Message.memory_usage)

    # |------------------------------------------------|
    #         Message: Controller Controller Off
    # |------------------------------------------------|
    #################################
    def set_off_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.off)

    #################################
    def get_off_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.off)

    #################################
    def clear_off_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.off)

    def check_for_off_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=ControllerCommands.Controller,
                _identifier=None,
                _status_code=Message.off)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_off_message(self):
        self.get_off_message()
        self.verify_message(_status_code=Message.off)

    # |------------------------------------------------|
    #         Message: Controller Pause Jumper
    # |------------------------------------------------|
    #################################
    def set_pause_jumper_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.pause_jumper)

    #################################
    def get_pause_jumper_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.pause_jumper)

    #################################
    def clear_pause_jumper_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.pause_jumper)

    def check_for_pause_jumper_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=ControllerCommands.Controller,
                _identifier=None,
                _status_code=Message.pause_jumper)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_pause_jumper_message(self):
        self.get_pause_jumper_message()
        self.verify_message(_status_code=Message.pause_jumper)

    # |------------------------------------------------|
    #         Message: Controller Rain Days Stop
    # |------------------------------------------------|
    #################################
    def set_rain_days_stop_message(self):
        data_required_string = "{0}={1}".format(opcodes.variable_1, self.bl_32.rp)
        self.send_message(
            _action=ActionCommands.SET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.rain_delay_stopped,
            _data_required_string=data_required_string)

    #################################
    def get_rain_days_stop_message(self):
        data_required_string = "{0}={1}".format(opcodes.variable_1, self.bl_32.rp)
        self.get_message(
            _action=ActionCommands.GET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.rain_delay_stopped,
            _data_required_string=data_required_string)

    #################################
    def clear_rain_days_stop_message(self):
        data_required_string = "{0}={1}".format(opcodes.variable_1, self.bl_32.rp)
        self.send_message(
            _action=ActionCommands.DO,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.rain_delay_stopped,
            _data_required_string=data_required_string)

    def check_for_rain_days_stop_message_not_present(self):
        try:
            data_required_string = "{0}={1}".format(opcodes.variable_1, self.bl_32.rp)
            self.send_message(
                _action=ActionCommands.DO,
                _category=ControllerCommands.Controller,
                _identifier=None,
                _status_code=Message.rain_delay_stopped,
                _data_required_string=data_required_string)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_rain_days_stop_message(self):
        self.get_rain_days_stop_message()
        self.verify_message(_status_code=Message.rain_delay_stopped)

    # |------------------------------------------------|
    #         Message: Controller Rain Stop Jumper
    # |------------------------------------------------|
    #################################
    def set_rain_stop_jumper_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.rain_jumper_stopped)

    #################################
    def get_rain_stop_jumper_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.rain_jumper_stopped)

    #################################
    def clear_rain_stop_jumper_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.rain_jumper_stopped)

    def check_for_rain_stop_jumper_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=ControllerCommands.Controller,
                _identifier=None,
                _status_code=Message.rain_jumper_stopped)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_rain_stop_jumper_message(self):
        self.get_rain_stop_jumper_message()
        self.verify_message(_status_code=Message.rain_jumper_stopped)
