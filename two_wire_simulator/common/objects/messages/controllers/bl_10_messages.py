from common.imports import opcodes
from common.objects.messages.base_messages import BaseMessages
from common.imports.types import ActionCommands, ControllerCommands
from common.imports.types import Message
from datetime import datetime, timedelta

__author__ = 'Baseline'


class BL1000Messages(BaseMessages):

    #################################
    def __init__(self, _controller_object):
        """
        :param _controller_object:     Controller object that will be referenced throughout this module. \n
        :type _controller_object:      common.objects.controllers.bl_10.BaseStation1000 \n
        """
        self.bl_10 = _controller_object
        self.status_code = ''
        # Init parent class to inherit respective attributes
        BaseMessages.__init__(self, _controller=_controller_object)

    #################################
    def build_message_header(self):
        # 1000 controller messages do not have a header in the ERS
        message_header = ''
        return message_header

    #################################
    def build_message_tx(self, _status_code):
        """
        :param _status_code:
        :type _status_code: str

        """
        tx_msg = self.build_message_header()

        # Create the title and header for the controller
        if _status_code is Message.two_wire_over_current:
            tx_msg += "{0}".format(
                "Two-Wire Over Current/r/nOld ETo Used",     # {0}
            )
        return tx_msg

    #################################
    def build_alarm_id(self, _status_code):
        """
        This return the alarm string.

        :param _status_code:
        :type _status_code:
        :return:
        :rtype:
        """
        # will always create 0_SY_SY_(Status code)
        alarm_id = '{0}_{1}_{2}_{3}'.format(
            str(0).zfill(1),    # {0} controller id number
            opcodes.system,     # {1}
            opcodes.system,     # {2}
            _status_code        # {3} status code of the message
        )
        return alarm_id

    # |----------------------------------------------------------|
    #         Message: Two-Wire Over Current
    # |----------------------------------------------------------|
    #################################
    def set_two_wire_over_current_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.two_wire_over_current)

    #################################
    def get_two_wire_over_current_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.two_wire_over_current)

    #################################
    def clear_two_wire_over_current_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.two_wire_over_current)

    def check_for_two_wire_over_current_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=ControllerCommands.Controller,
                _identifier=None,
                _status_code=Message.two_wire_over_current)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_two_wire_over_current_message(self):
        self.get_two_wire_over_current_message()
        self.verify_message(_status_code=Message.two_wire_over_current)
