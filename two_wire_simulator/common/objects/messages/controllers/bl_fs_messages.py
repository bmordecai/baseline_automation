from common.imports import opcodes
from common.objects.messages.base_messages import BaseMessages
from common.imports.types import ActionCommands, ControllerCommands
from common.imports.types import Message
from datetime import datetime, timedelta

__author__ = 'Baseline'


class FlowStationMessages(BaseMessages):

    #################################
    def __init__(self, _controller_object):
        """
        :param _controller_object:     Controller object that will be referenced throughout this module. \n
        :type _controller_object:      common.objects.controllers.bl_fs.FlowStation \n
        """
        self.bl_fs = _controller_object
        self.status_code = ''
        # Init parent class to inherit respective attributes
        BaseMessages.__init__(self, _controller=_controller_object)
        # Data Required
        self.restore_method = None  # This is either (BaseManager | USB Drive | Internal Flash)


    #################################
    def build_message_header(self):
        message_header = '{0}: {1}n'.format(
            str('FlowStation'),
            self.bl_fs.ds
        )
        return message_header

    #################################
    def build_message_tx(self, _status_code):
        """
        :param _status_code:
        :type _status_code: str

        """
        tx_msg = self.build_message_header()


    #################################
    def build_alarm_id(self, _status_code):
        """
        This return the alarm string.

        :param _status_code:
        :type _status_code:
        :return:
        :rtype:
        """
        # If the status code is 'CE' we have a 'NC' in the status
        alarm_id = '{0}_{1}_{2}_{3}'.format(
            str(000).zfill(3),          # {0} controller id number
            opcodes.device_value,       # {1}
            self.fs_32.sn,              # {2} controller serial number
            _status_code                # {3}
        )
        return alarm_id

    # |----------------------------------------------------------|
    #         Message: Controller Restore Failed BaseManager
    # |----------------------------------------------------------|
    #################################
    def set_restore_failed_from_basemanager_message(self):
        pass
        # self.restore_method = "BaseManager"
        # required_data = "{0}={1}".format(opcodes.variable_2, self.restore_method)
        # self.send_message(
        #     _action=ActionCommands.SET,
        #     _category=ControllerCommands.Controller,
        #     _identifier=None,
        #     _status_code=Message.restore_failed,
        #     _data_required_string=required_data)

    #################################
    def get_restore_failed_from_basemanager_message(self):
        pass
        # self.get_message(
        #     _action=ActionCommands.GET,
        #     _category=ControllerCommands.Controller,
        #     _identifier=None,
        #     _status_code=Message.restore_failed)

    #################################
    def clear_restore_failed_from_basemanager_message(self):
        pass
        # self.send_message(
        #     _action=ActionCommands.DO,
        #     _category=ControllerCommands.Controller,
        #     _identifier=None,
        #     _status_code=Message.restore_failed)

    def check_for_restore_failed_from_basemanager_message_not_present(self):
        pass
        # try:
        #     self.send_message(
        #         _action=ActionCommands.DO,
        #         _category=ControllerCommands.Controller,
        #         _identifier=None,
        #         _status_code=Message.restore_failed)
        # except Exception as e:
        #     if e.message == "NM No Message Found":
        #         print("Message was to found on controller.")
        #     else:
        #         print("Was unable to verify if the message was gone.")

    #################################
    def verify_restore_failed_from_basemanager_message(self):
        pass
        # self.get_restore_failed_from_basemanager_message()
        # self.verify_message(_status_code=Message.restore_failed)
