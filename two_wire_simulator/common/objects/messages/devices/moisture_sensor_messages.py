from common.imports import opcodes
from common.objects.messages.base_messages import BaseMessages
from common.imports.types import ActionCommands, MoistureSensorCommands
from common.imports.types import Message

__author__ = 'Baseline'


class MoistureSensorMessages(BaseMessages):

    #################################
    def __init__(self, _moisture_sensor_object):
        """
        :param _moisture_sensor_object:     Controller this object is "attached" to \n
        :type _moisture_sensor_object:      common.objects.devices.ms.MoistureSensor
        """
        self.moisture_sensor = _moisture_sensor_object
        self.status_code = ''
        # Init parent class to inherit respective attributes
        BaseMessages.__init__(self, _controller=_moisture_sensor_object.controller)

    #################################
    def build_message_header(self):
        if self.moisture_sensor.controller.is3200():
            message_header = '{0}: {1}n'.format(
                str('biSensor'),
                self.moisture_sensor.ds
            )
        else:   # If the controller is a 1000
            message_header = '{0} {1}-{2}n'.format(
                str('Moisture Sensor'),
                self.moisture_sensor.ad,
                self.moisture_sensor.sn
            )
        return message_header

    #################################
    def build_message_tx(self, _status_code):
        """
        :param _status_code:
        :type _status_code: str

        """
        tx_msg = self.build_message_header()

        # Create the title and header for the moisture sensor
        if self.moisture_sensor.controller.is3200():
            if _status_code is Message.bad_serial:
                tx_msg += "{0}{1}{2}{3}".format(
                    "Device Error:n",
                    "Wrong device at this address.n",
                    "Device = ",
                    self.moisture_sensor.sn
                )
            elif _status_code is Message.sensor_disabled:
                tx_msg += "{0}{1}{2}{3}{4}".format(
                    "Device Disabled:n",
                    "This device will not be usedn",
                    "in watering decisions.n",
                    "Serial Number = ",
                    str(self.moisture_sensor.sn) + "n")

            elif _status_code is Message.no_response:
                tx_msg += "{0}{1}{2}{3}".format(
                    "Two-wire Communication Failure:n",
                    "Unable to talk with this device.n",
                    "Device = ",
                    self.moisture_sensor.sn
                )
            elif _status_code is Message.zero_reading:
                tx_msg += "{0}{1}{2}{3}{4}".format(
                    "Moisture biSensor Failure:n",
                    "Unable to get a valid moisturen",
                    "reading from biSensor.n",
                    "Moisture biSensor = ",
                    str(self.moisture_sensor.sn) + "n"
                )
        else:
            if _status_code is Message.checksum:
                tx_msg += "Comm Error"
            elif _status_code is Message.no_response:
                tx_msg += "No Reply"
            elif _status_code is Message.bad_serial:
                tx_msg += "Serial N. Mismatch"
        return tx_msg

    #################################
    def build_alarm_id(self, _status_code):
        """
        This returns the alarm string.

        :param _status_code:
        :type _status_code:
        :return:
        :rtype:
        """
        # If the controller type is 3200, use the 3200 ID formats
        if self.moisture_sensor.controller.is3200():
            alarm_id = '{0}_{1}_{2}_{3}'.format(
                str(210),                   # {0} this is the moisture sensor number address
                opcodes.moisture_sensor,    # {1}
                self.moisture_sensor.sn,    # {2} this is the serial number to the event switch
                _status_code                # {3}
            )
        else:
            alarm_id = '{0}_{1}_{2}_{3}'.format(
                str(self.moisture_sensor.ad + 158),     # {0} this is the moisture sensor number address
                opcodes.moisture_sensor,                # {1}
                self.moisture_sensor.sn,                # {2} this is the moisture sensor serial number
                _status_code                            # {3}
            )
        return alarm_id

    # |-------------------------------------------------|
    #         Message: Bad Serial Number
    # |-------------------------------------------------|
    #################################
    def set_bad_serial_number_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=MoistureSensorCommands.Moisture_Sensor,
            _identifier=self.moisture_sensor.sn,
            _status_code=Message.bad_serial)

    #################################
    def get_bad_serial_number_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=MoistureSensorCommands.Moisture_Sensor,
            _identifier=self.moisture_sensor.sn,
            _status_code=Message.bad_serial)

    #################################
    def clear_bad_serial_number_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=MoistureSensorCommands.Moisture_Sensor,
            _identifier=self.moisture_sensor.sn,
            _status_code=Message.bad_serial)

    def check_for_bad_serial_number_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=MoistureSensorCommands.Moisture_Sensor,
                _identifier=self.moisture_sensor.sn,
                _status_code=Message.bad_serial)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was not found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_bad_serial_number_message(self):
        self.get_bad_serial_number_message()
        self.verify_message(_status_code=Message.bad_serial)

    # |-------------------------------------------------|
    #        Message: No Response Messages
    # |-------------------------------------------------|
    #################################
    def set_no_response_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=MoistureSensorCommands.Moisture_Sensor,
            _identifier=self.moisture_sensor.sn,
            _status_code=Message.no_response)

    #################################
    def get_no_response_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=MoistureSensorCommands.Moisture_Sensor,
            _identifier=self.moisture_sensor.sn,
            _status_code=Message.no_response)

    #################################
    def clear_no_response_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=MoistureSensorCommands.Moisture_Sensor,
            _identifier=self.moisture_sensor.sn,
            _status_code=Message.no_response)

    def check_for_no_response_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=MoistureSensorCommands.Moisture_Sensor,
                _identifier=self.moisture_sensor.sn,
                _status_code=Message.no_response)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_no_response_message(self):
        self.get_no_response_message()
        self.verify_message(_status_code=Message.no_response)

    # |-------------------------------------------------|
    #        Message: Disabled Messages
    # |-------------------------------------------------|
    #################################
    def set_device_disabled_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=MoistureSensorCommands.Moisture_Sensor,
            _identifier=self.moisture_sensor.sn,
            _status_code=Message.device_disabled)

    #################################
    def get_device_disabled_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=MoistureSensorCommands.Moisture_Sensor,
            _identifier=self.moisture_sensor.sn,
            _status_code=Message.device_disabled)

    #################################
    def clear_device_disabled_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=MoistureSensorCommands.Moisture_Sensor,
            _identifier=self.moisture_sensor.sn,
            _status_code=Message.device_disabled)

    def check_for_device_disabled_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=MoistureSensorCommands.Moisture_Sensor,
                _identifier=self.moisture_sensor.sn,
                _status_code=Message.device_disabled)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_device_disabled_message(self):
        self.get_device_disabled_message()
        self.verify_message(_status_code=Message.device_disabled)

    # |-------------------------------------------------|
    #        Message: Zero Reading Messages
    # |-------------------------------------------------|
    #################################
    def set_zero_reading_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=MoistureSensorCommands.Moisture_Sensor,
            _identifier=self.moisture_sensor.sn,
            _status_code=Message.zero_reading)

    #################################
    def get_zero_reading_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=MoistureSensorCommands.Moisture_Sensor,
            _identifier=self.moisture_sensor.sn,
            _status_code=Message.zero_reading)

    #################################
    def clear_zero_reading_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=MoistureSensorCommands.Moisture_Sensor,
            _identifier=self.moisture_sensor.sn,
            _status_code=Message.zero_reading)

    def check_for_zero_reading_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=MoistureSensorCommands.Moisture_Sensor,
                _identifier=self.moisture_sensor.sn,
                _status_code=Message.zero_reading)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_zero_reading_message(self):
        self.get_zero_reading_message()
        self.verify_message(_status_code=Message.zero_reading)

    # |-------------------------------------------------|
    #        Message: Checksum Messages (1000 Only)
    # |-------------------------------------------------|
    #################################
    def set_checksum_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=MoistureSensorCommands.Moisture_Sensor,
            _identifier=self.moisture_sensor.sn,
            _status_code=Message.checksum)

    #################################
    def get_checksum_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=MoistureSensorCommands.Moisture_Sensor,
            _identifier=self.moisture_sensor.sn,
            _status_code=Message.checksum)

    #################################
    def clear_checksum_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=MoistureSensorCommands.Moisture_Sensor,
            _identifier=self.moisture_sensor.sn,
            _status_code=Message.checksum)

    def check_for_checksum_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=MoistureSensorCommands.Moisture_Sensor,
                _identifier=self.moisture_sensor.sn,
                _status_code=Message.checksum)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_checksum_message(self):
        self.get_checksum_message()
        self.verify_message(_status_code=Message.checksum)
