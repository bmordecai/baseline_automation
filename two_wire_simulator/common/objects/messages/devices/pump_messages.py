from datetime import datetime, timedelta
from common.date_package.date_resource import date_mngr

from common.imports import opcodes
from common.objects.messages.base_messages import BaseMessages
from common.imports.types import ActionCommands, PumpCommands
from common.imports.types import Message, MessageCategory, status_plus_priority_code_dict_3200, status_code_dict_1000, \
    program_1000_who_dict

__author__ = 'Baseline'


class PumpMessages(BaseMessages):

    #################################
    def __init__(self, _pump_object):
        """
        :param _pump_object:     Controller this object is "attached" to \n
        :type _pump_object:      common.objects.devices.pm.Pump
        """
        self.pump = _pump_object
        self.status_code = ''
        # Init parent class to inherit respective attributes
        BaseMessages.__init__(self, _controller=_pump_object.controller)
        # Data Required



    #################################
    def build_message_header(self):
        message_header = '{0} {1}: {2}n'.format(
            opcodes.pump,
            self.pump.ad,
            self.pump.ds
            )
        return message_header

    #################################
    def build_message_tx(self, _status_code):
        """
        :param _status_code:
        :type _status_code: str

        """
        tx_msg = self.build_message_header()
        # If the object passed in part of a 1000 controller, create 3200 specific text messages

        # Create the title and header for the flow meter(flow biCoder)
        if _status_code is Message.bad_serial:
            tx_msg += "{0}{1}{2}{3}".format(
                "Device Error:n",                       # {0}
                "Wrong device at this address.n",       # {1}
                "Device = ",                            # {2}
                self.pump.sn                            # {3}
            )

        elif _status_code is Message.device_disabled:
            tx_msg += "{0}{1}{2}{3}{4}".format(
                "Device Disabled:n",                    # {0}
                "This device will not be usedn",        # {1}
                "in watering decisions.n",              # {2}
                "Serial Number = ",                     # {3}
                self.pump.sn + "n"                      # {4}
            )
        elif _status_code is Message.no_response:
            tx_msg += "{0}{1}{2}{3}".format(
                "Two-wire Communication Failure:n",     # {0}
                "Unable to talk with this device.n",    # {1}
                "Device = ",                            # {2}
                self.pump.sn
            )
        elif _status_code is Message.open_circuit:
            tx_msg += "{0}{1}{2}{3}{4}{5}{6}".format(
                "Valve Failure:n",
                "Open Circuit Solenoid.n",
                "Unable to operate this valve.n",
                "Zone = ",
                str(self.pump.ad) + "n",
                "biCoder = ",
                str(self.pump.sn) + "n"
            )

        elif _status_code is Message.short_circuit:
            tx_msg += "{0}{1}{2}{3}{4}{5}{6}".format(
                "Valve Failure:n",
                "Short Circuit Solenoid.n",
                "Unable to operate this valve.n",
                "Zone = ",
                str(self.pump.ad) + "n",
                "biCoder = ",
                str(self.pump.sn) + "n"
                )

        return tx_msg

    #################################
    def build_alarm_id(self, _status_code):
        """
        This return the alarm string.

        :param _status_code:
        :type _status_code:
        :return:
        :rtype:
        """

        # If the controller type is 3200, use the 3200 ID formats
        alarm_id = '{0}_{1}_{2}_{3}'.format(
            str(209).zfill(3),          # {0} pump ID number
            PumpCommands.Pump,          # {1}
            self.pump.ad,               # {2} this is the serial number to the pump
            _status_code                # {3}
        )
        return alarm_id

# |-------------------------------------------------|
#         Message: Bad Serial Number
# |-------------------------------------------------|
    #################################
    def set_bad_serial_number_message(self, ):
        self.send_message(
            _action=ActionCommands.SET,
            _category=PumpCommands.Pump,
            _identifier=self.pump.sn,
            _status_code=Message.bad_serial)

    #################################
    def get_bad_serial_number_message(self, ):
        self.get_message(
            _action=ActionCommands.GET,
            _category=PumpCommands.Pump,
            _identifier=self.pump.sn,
            _status_code=Message.bad_serial)

    #################################
    def clear_bad_serial_number_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=PumpCommands.Pump,
            _identifier=self.pump.sn,
            _status_code=Message.bad_serial)

    def check_for_bad_serial_number_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=PumpCommands.Pump,
                _identifier=self.pump.sn,
                _status_code=Message.bad_serial)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_bad_serial_number_message(self):
        self.get_bad_serial_number_message()
        self.verify_message(_status_code=Message.bad_serial)

# |-------------------------------------------------|
#        Message: No Response Messages
# |-------------------------------------------------|
    #################################
    def set_no_response_message(self, ):
        self.send_message(
            _action=ActionCommands.SET,
            _category=PumpCommands.Pump,
            _identifier=self.pump.sn,
            _status_code=Message.no_response)

    #################################
    def get_no_response_message(self, ):
        self.get_message(
            _action=ActionCommands.GET,
            _category=PumpCommands.Pump,
            _identifier=self.pump.sn,
            _status_code=Message.no_response)

    #################################
    def clear_no_response_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=PumpCommands.Pump,
            _identifier=self.pump.sn,
            _status_code=Message.no_response)

    def check_for_no_response_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=PumpCommands.Pump,
                _identifier=self.pump.sn,
                _status_code=Message.no_response)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_no_response_message(self):
        self.get_no_response_message()
        self.verify_message(_status_code=Message.no_response)


# |-------------------------------------------------|
#        Message: Device Disabled
# |-------------------------------------------------|
    #################################
    def set_device_disabled_message(self, ):
        self.send_message(
            _action=ActionCommands.SET,
            _category=PumpCommands.Pump,
            _identifier=self.pump.sn,
            _status_code=Message.device_disabled)


    #################################
    def get_device_disabled_message(self, ):
        self.get_message(
            _action=ActionCommands.GET,
            _category=PumpCommands.Pump,
            _identifier=self.pump.sn,
            _status_code=Message.device_disabled)


    #################################
    def clear_device_disabled_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=PumpCommands.Pump,
            _identifier=self.pump.sn,
            _status_code=Message.device_disabled)

    #################################
    def check_for_device_disabled_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=PumpCommands.Pump,
                _identifier=self.pump.sn,
                _status_code=Message.device_disabled)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")


    #################################
    def verify_device_disabled_message(self):
        self.get_device_disabled_message()
        self.verify_message(_status_code=Message.device_disabled)


# |-------------------------------------------------|
#        Message: Open Circuit
# |-------------------------------------------------|
    #################################
    def set_open_circuit_message(self, ):
        self.send_message(
            _action=ActionCommands.SET,
            _category=PumpCommands.Pump,
            _identifier=self.pump.sn,
            _status_code=Message.open_circuit)


    #################################
    def get_open_circuit_message(self, ):
        self.get_message(
            _action=ActionCommands.GET,
            _category=PumpCommands.Pump,
            _identifier=self.pump.sn,
            _status_code=Message.open_circuit)


    #################################
    def clear_open_circuit_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=PumpCommands.Pump,
            _identifier=self.pump.sn,
            _status_code=Message.open_circuit)

    #################################
    def check_for_open_circuit_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=PumpCommands.Pump,
                _identifier=self.pump.sn,
                _status_code=Message.open_circuit)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")


    #################################
    def verify_open_circuit_message(self):
        self.get_open_circuit_message()
        self.verify_message(_status_code=Message.open_circuit)

# |-------------------------------------------------|
#        Message: Short Circuit
# |-------------------------------------------------|
    #################################
    def set_short_circuit_message(self, ):
        self.send_message(
            _action=ActionCommands.SET,
            _category=PumpCommands.Pump,
            _identifier=self.pump.sn,
            _status_code=Message.short_circuit)


    #################################
    def get_short_circuit_message(self, ):
        self.get_message(
            _action=ActionCommands.GET,
            _category=PumpCommands.Pump,
            _identifier=self.pump.sn,
            _status_code=Message.short_circuit)


    #################################
    def clear_short_circuit_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=PumpCommands.Pump,
            _identifier=self.pump.sn,
            _status_code=Message.short_circuit)

    #################################
    def check_for_short_circuit_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=PumpCommands.Pump,
                _identifier=self.pump.sn,
                _status_code=Message.short_circuit)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")


    #################################
    def verify_short_circuit_message(self):
        self.get_short_circuit_message()
        self.verify_message(_status_code=Message.short_circuit)