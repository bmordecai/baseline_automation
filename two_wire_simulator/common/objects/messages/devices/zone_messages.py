from datetime import datetime, timedelta
from common.date_package.date_resource import date_mngr

from common.imports import opcodes
from common.objects.messages.base_messages import BaseMessages
from common.imports.types import ActionCommands, ZoneCommands
from common.imports.types import Message

__author__ = 'Baseline'


class ZoneMessages(BaseMessages):

    #################################
    def __init__(self, _zone_object):
        """
        :param _zone_object:     Zone object that will have a reference to this messages object. \n
        :type _zone_object:      common.objects.devices.zn.Zone
        """
        self.zones = _zone_object
        self.status_code = ''
        # Init parent class to inherit respective attributes
        BaseMessages.__init__(self, _controller=_zone_object.controller)
        # Data Required
        self.flow_meter = None  # Flow meter of upstream POC for this zone

    #################################
    def update_values(self):
        """
        This method updates all of the values that this message will use. \n
        """
        if self.zones.ml:
            for poc in self.zones.controller.points_of_control.values():
                if poc.ml == self.zones.ml:
                    if poc.fm:
                        self.flow_meter = self.zones.controller.flow_meters[poc.fm]

    #################################
    def build_message_header(self):
        if self.zones.controller.is3200():
            message_header = '{0} {1}: {2}n'.format(
                str('Zone'),
                self.zones.ad,
                self.zones.ds
            )
        else:   # If the controller is a 1000
            message_header = '{0} {1}-{2}n'.format(
                str('Zone'),
                self.zones.ad,
                self.zones.sn
            )
        return message_header

    #################################
    def build_message_tx(self, _status_code):
        """
        :param _status_code:
        :type _status_code: str

        """
        tx_msg = self.build_message_header()
        # If the object passed in part of a 1000 controller, create 3200 specific text messages

        # Create the title and header for the flow meter(flow biCoder)
        if self.zones.controller.is3200():
            if _status_code is Message.bad_serial:
                tx_msg += "{0}{1}{2}{3}".format(
                    "Device Error:n",                   # {0}
                    "Wrong device at this address.n",   # {1}
                    "Device = ",                        # {2}
                    self.zones.sn                       # {3}
                )

            elif _status_code is Message.flow_learn_errors:
                # While doing a learn flow, if the first reading after the flow stable time is zero the zone will fail
                # immediately. The message should then have 0 for flow instead of the zones design flow
                if self.flow_meter.bicoder.vr == 0:
                    tx_msg += "{0}{1}{2}{3}{4}{5}".format(
                        "Zone Learn Flow Done:n",
                        "This zone had errors.n",
                        "Zone = ",
                        str(self.zones.ad) + "n",
                        "Learn Flow = ",
                        str(0.0)
                    )
                else:
                    tx_msg += "{0}{1}{2}{3}{4}{5}".format(
                        "Zone Learn Flow Done:n",
                        "This zone had errors.n",
                        "Zone = ",
                        str(self.zones.ad) + "n",
                        "Learn Flow = ",
                        str(self.zones.df)
                    )
            elif _status_code is Message.zone_learn_flow_complete_success:
                tx_msg += "{0}{1}{2}{3}{4}{5}".format(
                    "Zone Learn Flow Done:n",           # {0}
                    "Completed successfully.n",         # {1}
                    "Zone = ",                          # {2}
                    str(self.zones.ad) + "n",           # {3}
                    "Learn Flow = ",                    # {4}
                    str(self.zones.df)                  # {5}
                )
            elif _status_code is Message.missing_24_vac:
                tx_msg += "{0}{1}{2}{3}{4}".format(
                    "Powered Valve biCoder Failure:n",  # {0}
                    "Missing 24 VAC.n",                 # {1}
                    "Unable to operate this valve.n",   # {2}
                    "Zone = ",                          # {3}
                    str(self.zones.ad)                  # {4}
                )
            elif _status_code is Message.no_response:
                tx_msg += "{0}{1}{2}{3}".format(
                    "Two-wire Communication Failure:n",     # {0}
                    "Unable to talk with this device.n",    # {1}
                    "Device = ",                            # {2}
                    self.zones.sn                           # {3}
                )
            elif _status_code is Message.open_circuit:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}".format(
                    "Valve Failure:n",                  # {0}
                    "Open Circuit Solenoid.n",          # {1}
                    "Unable to operate this valve.n",   # {2}
                    "Zone = ",                          # {3}
                    str(self.zones.ad) + "n",           # {4}
                    "biCoder = ",                       # {5}
                    str(self.zones.sn) + "n"            # {6}
                )

            elif _status_code is Message.short_circuit:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}".format(
                    "Valve Failure:n",                  # {0}
                    "Short Circuit Solenoid.n",         # {1}
                    "Unable to operate this valve.n",   # {2}
                    "Zone = ",                          # {3}
                    str(self.zones.ad) + "n",           # {4}
                    "biCoder = ",                       # {5}
                    str(self.zones.sn) + "n"            # {6}
                )
        else:
            if _status_code is Message.checksum:
                tx_msg += "Comm Error"
            elif _status_code is Message.no_response:
                tx_msg += "No Reply"
            elif _status_code is Message.bad_serial:
                tx_msg += "Serial N. Mismatch"
            elif _status_code is Message.low_voltage:
                tx_msg += "Voltage Too Low"
            elif _status_code is Message.open_circuit_1000:
                tx_msg += "Solenoid Open"
            elif _status_code is Message.short_circuit_1000:
                tx_msg += "Solenoid Short"
            elif _status_code is Message.high_flow_variance_shutdown:
                tx_msg += "High Flow Variance"

        return tx_msg

    #################################
    def build_alarm_id(self, _status_code):
        """
        This return the alarm string.

        :param _status_code:
        :type _status_code:
        :return:
        :rtype:
        """

        # If the controller type is 3200, use the 3200 ID formats
        if self.zones.controller.is3200():
            alarm_id = '{0}_{1}_{2}_{3}'.format(
                str(self.zones.ad).zfill(3),    # {0} zone ID number
                ZoneCommands.Zones,             # {1}
                self.zones.ad,                  # {2} this is the address to the zone
                _status_code                    # {3}
            )
        else:
            alarm_id = '{0}_{1}_{2}_{3}'.format(
                self.zones.ad,      # {0} this is the zone number address
                opcodes.zone,       # {1}
                self.zones.ad,      # {2} this is the zone number address
                _status_code        # {3}
            )
        return alarm_id

    # |-------------------------------------------------|
    #         Message: Bad Serial Number
    # |-------------------------------------------------|
    #################################
    def set_bad_serial_number_message(self, ):
        if self.zones.controller.is1000():
            required_data = ""
        else:
            required_data = "{0}={1}".format(opcodes.device_value, self.zones.bicoder.sn)
        self.send_message(
            _action=ActionCommands.SET,
            _category=ZoneCommands.Zones,
            _identifier=self.zones.ad,
            _status_code=Message.bad_serial,
            _data_required_string=required_data)

    #################################
    def get_bad_serial_number_message(self, ):
        if self.zones.controller.is1000():
            required_data = ""
        else:
            required_data = "{0}={1}".format(opcodes.device_value, self.zones.bicoder.sn)
        self.get_message(
            _action=ActionCommands.GET,
            _category=ZoneCommands.Zones,
            _identifier=self.zones.ad,
            _status_code=Message.bad_serial,
            _data_required_string=required_data)

    #################################
    def clear_bad_serial_number_message(self):
        if self.zones.controller.is1000():
            required_data = ""
        else:
            required_data = "{0}={1}".format(opcodes.device_value, self.zones.bicoder.sn)
        self.send_message(
            _action=ActionCommands.DO,
            _category=ZoneCommands.Zones,
            _identifier=self.zones.ad,
            _status_code=Message.bad_serial,
            _data_required_string=required_data)

    def check_for_bad_serial_number_message_not_present(self):
        try:
            if self.zones.controller.is1000():
                required_data = ""
            else:
                required_data = "{0}={1}".format(opcodes.device_value, self.zones.bicoder.sn)
            self.send_message(
                _action=ActionCommands.DO,
                _category=ZoneCommands.Zones,
                _identifier=self.zones.ad,
                _status_code=Message.bad_serial,
                _data_required_string=required_data)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_bad_serial_number_message(self):
        self.get_bad_serial_number_message()
        self.verify_message(_status_code=Message.bad_serial)

    # |--------------------------------------------------------------|
    #         Message: Zone Program Learn Flow Complete Error
    # |--------------------------------------------------------------|
    #################################
    def set_learn_flow_complete_error_message(self):
        self.update_values()
        data_required_string = "{0}={1}".format(
            opcodes.variable_2,
            self.flow_meter.bicoder.vr
        )
        self.send_message(
            _action=ActionCommands.SET,
            _category=ZoneCommands.Zones,
            _identifier=self.zones.ad,
            _status_code=Message.zone_learn_flow_complete_errors,
            _data_required_string=data_required_string)

    #################################
    def get_learn_flow_complete_error_message(self):
        self.update_values()
        if self.zones.controller.is1000():
            required_data = ""
        else:
            required_data = ""
        self.get_message(
            _action=ActionCommands.GET,
            _category=ZoneCommands.Zones,
            _identifier=self.zones.ad,
            _status_code=Message.zone_learn_flow_complete_errors,
            _data_required_string=required_data)

    #################################
    def clear_learn_flow_complete_error_message(self):
        self.update_values()
        if self.zones.controller.is1000():
            required_data = ""
        else:
            required_data = ""
        self.send_message(
            _action=ActionCommands.DO,
            _category=ZoneCommands.Zones,
            _identifier=self.zones.ad,
            _status_code=Message.zone_learn_flow_complete_errors,
            _data_required_string=required_data)

    #################################
    def verify_learn_flow_complete_error_message(self):
        self.update_values()
        self.get_learn_flow_complete_error_message()
        self.verify_message(_status_code=Message.zone_learn_flow_complete_errors)

    # |--------------------------------------------------------------|
    #         Message: Zone Program Learn Flow Complete Success
    # |--------------------------------------------------------------|
    #################################
    def set_learn_flow_complete_success_message(self):
        self.update_values()
        data_required_string = "{0}={1}".format(
            opcodes.variable_2,
            self.flow_meter.bicoder.vr
        )
        self.send_message(
            _action=ActionCommands.SET,
            _category=ZoneCommands.Zones,
            _identifier=self.zones.ad,
            _status_code=Message.zone_learn_flow_complete_success,
            _data_required_string=data_required_string)

    #################################
    def get_learn_flow_complete_success_message(self):
        self.update_values()
        if self.zones.controller.is1000():
            required_data = ""
        else:
            required_data = ""
        self.get_message(
            _action=ActionCommands.GET,
            _category=ZoneCommands.Zones,
            _identifier=self.zones.ad,
            _status_code=Message.zone_learn_flow_complete_success,
            _data_required_string=required_data)

    #################################
    def clear_learn_flow_complete_success_message(self):
        self.update_values()
        if self.zones.controller.is1000():
            required_data = ""
        else:
            required_data = ""
        self.send_message(
            _action=ActionCommands.DO,
            _category=ZoneCommands.Zones,
            _identifier=self.zones.ad,
            _status_code=Message.zone_learn_flow_complete_success,
            _data_required_string=required_data)

    #################################
    def verify_learn_flow_complete_success_message(self):
        self.update_values()
        self.get_learn_flow_complete_success_message()
        self.verify_message(_status_code=Message.zone_learn_flow_complete_success)

    # |-------------------------------------------------|
    #        Message: No Response Messages
    # |-------------------------------------------------|
    #################################
    def set_no_response_message(self, ):
        if self.zones.controller.is1000():
            required_data = ""
        else:
            required_data = "{0}={1}".format(opcodes.device_value, self.zones.bicoder.sn)
        self.send_message(
            _action=ActionCommands.SET,
            _category=ZoneCommands.Zones,
            _identifier=self.zones.ad,
            _status_code=Message.no_response,
            _data_required_string=required_data)

    #################################
    def get_no_response_message(self, ):
        if self.zones.controller.is1000():
            required_data = ""
        else:
            required_data = "{0}={1}".format(opcodes.device_value, self.zones.bicoder.sn)
        self.get_message(
            _action=ActionCommands.GET,
            _category=ZoneCommands.Zones,
            _identifier=self.zones.ad,
            _status_code=Message.no_response,
            _data_required_string=required_data)

    #################################
    def clear_no_response_message(self):
        if self.zones.controller.is1000():
            required_data = ""
        else:
            required_data = "{0}={1}".format(opcodes.device_value, self.zones.bicoder.sn)
        self.send_message(
            _action=ActionCommands.DO,
            _category=ZoneCommands.Zones,
            _identifier=self.zones.ad,
            _status_code=Message.no_response,
            _data_required_string=required_data)

    def check_for_no_response_message_not_present(self):
        try:
            if self.zones.controller.is1000():
                required_data = ""
            else:
                required_data = "{0}={1}".format(opcodes.device_value, self.zones.bicoder.sn)
            self.send_message(
                _action=ActionCommands.DO,
                _category=ZoneCommands.Zones,
                _identifier=self.zones.ad,
                _status_code=Message.no_response,
                _data_required_string=required_data)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_no_response_message(self):
        self.get_no_response_message()
        self.verify_message(_status_code=Message.no_response)

    # |-------------------------------------------------|
    #        Message: Missing 24 VAC
    # |-------------------------------------------------|
    #################################
    def set_missing_24_vac_message(self, ):
        required_data = "{0}={1}".format(opcodes.device_value, self.zones.bicoder.sn)
        self.send_message(
            _action=ActionCommands.SET,
            _category=ZoneCommands.Zones,
            _identifier=self.zones.ad,
            _status_code=Message.missing_24_vac,
            _data_required_string=required_data)

    #################################
    def get_missing_24_vac_message(self, ):
        required_data = "{0}={1}".format(opcodes.device_value, self.zones.bicoder.sn)
        self.get_message(
            _action=ActionCommands.GET,
            _category=ZoneCommands.Zones,
            _identifier=self.zones.ad,
            _status_code=Message.missing_24_vac,
            _data_required_string=required_data)

    #################################
    def clear_missing_24_vac_message(self):
        required_data = "{0}={1}".format(opcodes.device_value, self.zones.bicoder.sn)
        self.send_message(
            _action=ActionCommands.DO,
            _category=ZoneCommands.Zones,
            _identifier=self.zones.ad,
            _status_code=Message.missing_24_vac,
            _data_required_string=required_data)

    #################################
    def check_for_missing_24_vac_message_not_present(self):
        try:
            required_data = "{0}={1}".format(opcodes.device_value, self.zones.bicoder.sn)
            self.send_message(
                _action=ActionCommands.DO,
                _category=ZoneCommands.Zones,
                _identifier=self.zones.ad,
                _status_code=Message.missing_24_vac,
                _data_required_string=required_data)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")


    #################################
    def verify_missing_24_vac_message(self):
        self.get_missing_24_vac_message()
        self.verify_message(_status_code=Message.missing_24_vac)


    # |-------------------------------------------------|
    #        Message: Open Circuit
    # |-------------------------------------------------|
    #################################
    def set_open_circuit_message(self, ):
        # The status code for a 3200 and 1000 are different for this message
        if self.zones.controller.is3200():
            _status_code = Message.open_circuit
        else:
            _status_code = Message.open_circuit_1000

        if self.zones.controller.is1000():
            required_data = ""
        else:
            required_data = "{0}={1}".format(opcodes.device_value, self.zones.bicoder.sn)
        self.send_message(
            _action=ActionCommands.SET,
            _category=ZoneCommands.Zones,
            _identifier=self.zones.ad,
            _status_code=_status_code,
            _data_required_string=required_data)

    #################################
    def get_open_circuit_message(self, ):
        # The status code for a 3200 and 1000 are different for this message
        if self.zones.controller.is3200():
            _status_code = Message.open_circuit
        else:
            _status_code = Message.open_circuit_1000

        if self.zones.controller.is1000():
            required_data = ""
        else:
            required_data = "{0}={1}".format(opcodes.device_value, self.zones.bicoder.sn)
        self.get_message(
            _action=ActionCommands.GET,
            _category=ZoneCommands.Zones,
            _identifier=self.zones.ad,
            _status_code=_status_code,
            _data_required_string=required_data)

    #################################
    def clear_open_circuit_message(self):
        # The status code for a 3200 and 1000 are different for this message
        if self.zones.controller.is3200():
            _status_code = Message.open_circuit
        else:
            _status_code = Message.open_circuit_1000

        if self.zones.controller.is1000():
            required_data = ""
        else:
            required_data = "{0}={1}".format(opcodes.device_value, self.zones.bicoder.sn)
        self.send_message(
            _action=ActionCommands.DO,
            _category=ZoneCommands.Zones,
            _identifier=self.zones.ad,
            _status_code=_status_code,
            _data_required_string=required_data)

    #################################
    def check_for_open_circuit_message_not_present(self):
        # The status code for a 3200 and 1000 are different for this message
        if self.zones.controller.is3200():
            _status_code = Message.open_circuit
        else:
            _status_code = Message.open_circuit_1000
        try:
            if self.zones.controller.is1000():
                required_data = ""
            else:
                required_data = "{0}={1}".format(opcodes.device_value, self.zones.bicoder.sn)
            self.send_message(
                _action=ActionCommands.DO,
                _category=ZoneCommands.Zones,
                _identifier=self.zones.ad,
                _status_code=_status_code,
                _data_required_string=required_data)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_open_circuit_message(self):
        # The status code for a 3200 and 1000 are different for this message
        if self.zones.controller.is3200():
            _status_code = Message.open_circuit
        else:
            _status_code = Message.open_circuit_1000
        self.get_open_circuit_message()
        self.verify_message(_status_code=_status_code)

    # |-------------------------------------------------|
    #        Message: Short Circuit
    # |-------------------------------------------------|
    #################################
    def set_short_circuit_message(self, ):
        # The status code for a 3200 and 1000 are different for this message
        if self.zones.controller.is3200():
            _status_code = Message.short_circuit
        else:
            _status_code = Message.short_circuit_1000

        if self.zones.controller.is1000():
            required_data = ""
        else:
            required_data = "{0}={1}".format(opcodes.device_value, self.zones.bicoder.sn)
        self.send_message(
            _action=ActionCommands.SET,
            _category=ZoneCommands.Zones,
            _identifier=self.zones.ad,
            _status_code=_status_code,
            _data_required_string=required_data)


    #################################
    def get_short_circuit_message(self, ):
        # The status code for a 3200 and 1000 are different for this message
        if self.zones.controller.is3200():
            _status_code = Message.short_circuit
        else:
            _status_code = Message.short_circuit_1000

        if self.zones.controller.is1000():
            required_data = ""
        else:
            required_data = "{0}={1}".format(opcodes.device_value, self.zones.bicoder.sn)
        self.get_message(
            _action=ActionCommands.GET,
            _category=ZoneCommands.Zones,
            _identifier=self.zones.ad,
            _status_code=_status_code,
            _data_required_string=required_data)


    #################################
    def clear_short_circuit_message(self):
        # The status code for a 3200 and 1000 are different for this message
        if self.zones.controller.is3200():
            _status_code = Message.short_circuit
        else:
            _status_code = Message.short_circuit_1000

        if self.zones.controller.is1000():
            required_data = ""
        else:
            required_data = "{0}={1}".format(opcodes.device_value, self.zones.bicoder.sn)
        self.send_message(
            _action=ActionCommands.DO,
            _category=ZoneCommands.Zones,
            _identifier=self.zones.ad,
            _status_code=_status_code,
            _data_required_string=required_data)

    #################################
    def check_for_short_circuit_message_not_present(self):
        # The status code for a 3200 and 1000 are different for this message
        if self.zones.controller.is3200():
            _status_code = Message.short_circuit
        else:
            _status_code = Message.short_circuit_1000
        try:

            if self.zones.controller.is1000():
                required_data = ""
            else:
                required_data = "{0}={1}".format(opcodes.device_value, self.zones.bicoder.sn)

            self.send_message(
                _action=ActionCommands.DO,
                _category=ZoneCommands.Zones,
                _identifier=self.zones.ad,
                _status_code=_status_code,
                _data_required_string=required_data)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_short_circuit_message(self):
        # The status code for a 3200 and 1000 are different for this message
        if self.zones.controller.is3200():
            _status_code = Message.short_circuit
        else:
            _status_code = Message.short_circuit_1000
        self.get_short_circuit_message()
        self.verify_message(_status_code=_status_code)

    # |-------------------------------------------------|
    #        Message: Checksum Messages (1000 Only)
    # |-------------------------------------------------|
    #################################
    def set_checksum_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=ZoneCommands.Zones,
            _identifier=self.zones.sn,
            _status_code=Message.checksum)

    #################################
    def get_checksum_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=ZoneCommands.Zones,
            _identifier=self.zones.sn,
            _status_code=Message.checksum)

    #################################
    def clear_checksum_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=ZoneCommands.Zones,
            _identifier=self.zones.sn,
            _status_code=Message.checksum)

    def check_for_checksum_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=ZoneCommands.Zones,
                _identifier=self.zones.sn,
                _status_code=Message.checksum)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_checksum_message(self):
        self.get_checksum_message()
        self.verify_message(_status_code=Message.checksum)

    # |-------------------------------------------------|
    #        Message: Low Voltage Messages (1000 Only)
    # |-------------------------------------------------|
    #################################
    def set_low_voltage_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=ZoneCommands.Zones,
            _identifier=self.zones.sn,
            _status_code=Message.low_voltage)

    #################################
    def get_low_voltage_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=ZoneCommands.Zones,
            _identifier=self.zones.sn,
            _status_code=Message.low_voltage)

    #################################
    def clear_low_voltage_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=ZoneCommands.Zones,
            _identifier=self.zones.sn,
            _status_code=Message.low_voltage)

    def check_for_low_voltage_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=ZoneCommands.Zones,
                _identifier=self.zones.sn,
                _status_code=Message.low_voltage)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_low_voltage_message(self):
        self.get_low_voltage_message()
        self.verify_message(_status_code=Message.low_voltage)

    # |------------------------------------------------------------|
    #        Message: Exceeds Design Flow Messages (1000 Only)
    # |------------------------------------------------------------|
    #################################
    def set_exceeds_design_flow_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=ZoneCommands.Zones,
            _identifier=self.zones.sn,
            _status_code=Message.exceeds_design_flow_1000)

    #################################
    def get_exceeds_design_flow_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=ZoneCommands.Zones,
            _identifier=self.zones.sn,
            _status_code=Message.exceeds_design_flow_1000)

    #################################
    def clear_exceeds_design_flow_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=ZoneCommands.Zones,
            _identifier=self.zones.sn,
            _status_code=Message.exceeds_design_flow_1000)

    def check_for_exceeds_design_flow_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=ZoneCommands.Zones,
                _identifier=self.zones.sn,
                _status_code=Message.exceeds_design_flow_1000)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_exceeds_design_flow_message(self):
        self.get_exceeds_design_flow_message()
        self.verify_message(_status_code=Message.exceeds_design_flow_1000)