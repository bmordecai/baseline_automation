from datetime import datetime, timedelta
from common.date_package.date_resource import date_mngr

from common.imports import opcodes
from common.objects.messages.base_messages import BaseMessages
from common.imports.types import ActionCommands, MasterValvesCommands
from common.imports.types import Message, MessageCategory, status_plus_priority_code_dict_3200, status_code_dict_1000, \
    program_1000_who_dict

__author__ = 'Baseline'


class MasterValvesMessages(BaseMessages):

    #################################
    def __init__(self, _master_vales_object):
        """
        :param _master_vales_object:     Controller this object is "attached" to \n
        :type _master_vales_object:      common.objects.devices.mv.MasterValve
        """
        self.master_valves = _master_vales_object
        self.status_code = ''
        # Init parent class to inherit respective attributes
        BaseMessages.__init__(self, _controller=_master_vales_object.controller)
        # Data Required



    #################################
    def build_message_header(self):
        if self.master_valves.controller.is3200():
            message_header = '{0}{1}: {2}n'.format(
                str('MV'),
                self.master_valves.ad,
                self.master_valves.ds
            )
        else:   # If the controller is a 1000
            message_header = '{0} {1}-{2}n'.format(
                str('MV/Pump'),
                self.master_valves.ad,
                self.master_valves.sn
            )
        return message_header

    #################################
    def build_message_tx(self, _status_code):
        """
        :param _status_code:
        :type _status_code: str

        """
        tx_msg = self.build_message_header()
        # If the object passed in part of a 1000 controller, create 3200 specific text messages

        # Create the title and header for the master valve.
        # NOTE: This is separated by is3200 first because the 1000 and 3200 messages have conflicting status codes
        #       in short (OC in 1000, SC in 3200) and open circuit (OP in 1000, OC in 3200).
        if self.master_valves.controller.is3200():
            if _status_code is Message.bad_serial:
                tx_msg += "{0}{1}{2}{3}".format(
                    "Device Error:n",                       # {0}
                    "Wrong device at this address.n",       # {1}
                    "Device = ",                            # {2}
                    self.master_valves.sn                   # {3}
                )

            elif _status_code is Message.device_disabled:
                tx_msg += "{0}{1}{2}{3}{4}".format(
                    "Device Disabled:n",                    # {0}
                    "This device will not be usedn",        # {1}
                    "in watering decisions.n",              # {2}
                    "Serial Number = ",                     # {3}
                    self.master_valves.sn + "n"             # {4}
                )
            elif _status_code is Message.no_response:
                tx_msg += "{0}{1}{2}{3}".format(
                    "Two-wire Communication Failure:n",     # {0}
                    "Unable to talk with this device.n",    # {1}
                    "Device = ",                            # {2}
                    self.master_valves.sn                   # {3}
                )
            elif _status_code is Message.open_circuit:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}".format(
                    "Valve Failure:n",
                    "Open Circuit Solenoid.n",
                    "Unable to operate this valve.n",
                    "Zone = ",
                    str(200 + self.master_valves.ad) + "n",
                    "biCoder = ",
                    str(self.master_valves.sn) + "n"
                )

            elif _status_code is Message.short_circuit:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}".format(
                    "Valve Failure:n",
                    "Short Circuit Solenoid.n",
                    "Unable to operate this valve.n",
                    "Zone = ",
                    str(200 + self.master_valves.ad) + "n",
                    "biCoder = ",
                    str(self.master_valves.sn) + "n"
                    )
        else:
            if _status_code is Message.checksum:
                tx_msg += "Comm Error"
            elif _status_code is Message.no_response:
                tx_msg += "No Reply"
            elif _status_code is Message.bad_serial:
                tx_msg += "Serial N. Mismatch"
            elif _status_code is Message.low_voltage:
                tx_msg += "Voltage Too Low"
            elif _status_code is Message.open_circuit_1000:
                tx_msg += "Solenoid Open"
            elif _status_code is Message.short_circuit_1000:
                tx_msg += "Solenoid Short"


        return tx_msg

    #################################
    def build_alarm_id(self, _status_code):
        """
        This return the alarm string.

        :param _status_code:
        :type _status_code:
        :return:
        :rtype:
        """

        # If the controller type is 3200, use the 3200 ID formats
        if self.master_valves.controller.is3200():
            alarm_id = '{0}_{1}_{2}_{3}'.format(
                str(200 + self.master_valves.ad).zfill(3),  # {0} master valve ID number
                MasterValvesCommands.MasterValves,          # {1}
                self.master_valves.ad,                      # {2} this is the address to the master valve
                _status_code                                # {3}
            )
        else:
            alarm_id = '{0}_{1}_{2}_{3}'.format(
                str(self.master_valves.ad + 150),   # {0} this is the master valve number address
                opcodes.master_valve,               # {1}
                self.master_valves.ad,              # {2} this is the master valve number address
                _status_code                        # {3}
            )
        return alarm_id

# |-------------------------------------------------|
#         Message: Bad Serial Number
# |-------------------------------------------------|
    #################################
    def set_bad_serial_number_message(self, ):
        self.send_message(
            _action=ActionCommands.SET,
            _category=MasterValvesCommands.MasterValves,
            _identifier=self.master_valves.sn,
            _status_code=Message.bad_serial)

    #################################
    def get_bad_serial_number_message(self, ):
        self.get_message(
            _action=ActionCommands.GET,
            _category=MasterValvesCommands.MasterValves,
            _identifier=self.master_valves.sn,
            _status_code=Message.bad_serial)

    #################################
    def clear_bad_serial_number_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=MasterValvesCommands.MasterValves,
            _identifier=self.master_valves.sn,
            _status_code=Message.bad_serial)

    def check_for_bad_serial_number_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=MasterValvesCommands.MasterValves,
                _identifier=self.master_valves.sn,
                _status_code=Message.bad_serial)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_bad_serial_number_message(self):
        self.get_bad_serial_number_message()
        self.verify_message(_status_code=Message.bad_serial)

    # |-------------------------------------------------|
    #        Message: No Response Messages
    # |-------------------------------------------------|
    #################################
    def set_no_response_message(self, ):
        self.send_message(
            _action=ActionCommands.SET,
            _category=MasterValvesCommands.MasterValves,
            _identifier=self.master_valves.sn,
            _status_code=Message.no_response)

    #################################
    def get_no_response_message(self, ):
        self.get_message(
            _action=ActionCommands.GET,
            _category=MasterValvesCommands.MasterValves,
            _identifier=self.master_valves.sn,
            _status_code=Message.no_response)

    #################################
    def clear_no_response_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=MasterValvesCommands.MasterValves,
            _identifier=self.master_valves.sn,
            _status_code=Message.no_response)

    def check_for_no_response_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=MasterValvesCommands.MasterValves,
                _identifier=self.master_valves.sn,
                _status_code=Message.no_response)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_no_response_message(self):
        self.get_no_response_message()
        self.verify_message(_status_code=Message.no_response)

    # |-------------------------------------------------|
    #        Message: Device Disabled
    # |-------------------------------------------------|
    #################################
    def set_device_disabled_message(self, ):
        self.send_message(
            _action=ActionCommands.SET,
            _category=MasterValvesCommands.MasterValves,
            _identifier=self.master_valves.sn,
            _status_code=Message.device_disabled)


    #################################
    def get_device_disabled_message(self, ):
        self.get_message(
            _action=ActionCommands.GET,
            _category=MasterValvesCommands.MasterValves,
            _identifier=self.master_valves.sn,
            _status_code=Message.device_disabled)


    #################################
    def clear_device_disabled_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=MasterValvesCommands.MasterValves,
            _identifier=self.master_valves.sn,
            _status_code=Message.device_disabled)

    #################################
    def check_for_device_disabled_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=MasterValvesCommands.MasterValves,
                _identifier=self.master_valves.sn,
                _status_code=Message.device_disabled)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")


    #################################
    def verify_device_disabled_message(self):
        self.get_device_disabled_message()
        self.verify_message(_status_code=Message.device_disabled)

    # |-------------------------------------------------|
    #        Message: Open Circuit
    # |-------------------------------------------------|
    #################################
    def set_open_circuit_message(self, ):
        # The status code for a 3200 and 1000 are different for this message
        if self.master_valves.controller.is3200():
            _status_code = Message.open_circuit
        else:
            _status_code = Message.open_circuit_1000
        self.send_message(
            _action=ActionCommands.SET,
            _category=MasterValvesCommands.MasterValves,
            _identifier=self.master_valves.sn,
            _status_code=_status_code)


    #################################
    def get_open_circuit_message(self, ):
        # The status code for a 3200 and 1000 are different for this message
        if self.master_valves.controller.is3200():
            _status_code = Message.open_circuit
        else:
            _status_code = Message.open_circuit_1000
        self.get_message(
            _action=ActionCommands.GET,
            _category=MasterValvesCommands.MasterValves,
            _identifier=self.master_valves.sn,
            _status_code=_status_code)


    #################################
    def clear_open_circuit_message(self):
        # The status code for a 3200 and 1000 are different for this message
        if self.master_valves.controller.is3200():
            _status_code = Message.open_circuit
        else:
            _status_code = Message.open_circuit_1000
        self.send_message(
            _action=ActionCommands.DO,
            _category=MasterValvesCommands.MasterValves,
            _identifier=self.master_valves.sn,
            _status_code=_status_code)

    #################################
    def check_for_open_circuit_message_not_present(self):
        # The status code for a 3200 and 1000 are different for this message
        if self.master_valves.controller.is3200():
            _status_code = Message.open_circuit
        else:
            _status_code = Message.open_circuit_1000
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=MasterValvesCommands.MasterValves,
                _identifier=self.master_valves.sn,
                _status_code=_status_code)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")


    #################################
    def verify_open_circuit_message(self):
        # The status code for a 3200 and 1000 are different for this message
        if self.master_valves.controller.is3200():
            _status_code = Message.open_circuit
        else:
            _status_code = Message.open_circuit_1000
        self.get_open_circuit_message()
        self.verify_message(_status_code=_status_code)

    # |-------------------------------------------------|
    #        Message: Short Circuit
    # |-------------------------------------------------|
    #################################
    def set_short_circuit_message(self, ):
        # The status code for a 3200 and 1000 are different for this message
        if self.master_valves.controller.is3200():
            _status_code = Message.short_circuit
        else:
            _status_code = Message.short_circuit_1000
        self.send_message(
            _action=ActionCommands.SET,
            _category=MasterValvesCommands.MasterValves,
            _identifier=self.master_valves.sn,
            _status_code=_status_code)


    #################################
    def get_short_circuit_message(self, ):
        # The status code for a 3200 and 1000 are different for this message
        if self.master_valves.controller.is3200():
            _status_code = Message.short_circuit
        else:
            _status_code = Message.short_circuit_1000
        self.get_message(
            _action=ActionCommands.GET,
            _category=MasterValvesCommands.MasterValves,
            _identifier=self.master_valves.sn,
            _status_code=_status_code)


    #################################
    def clear_short_circuit_message(self):
        # The status code for a 3200 and 1000 are different for this message
        if self.master_valves.controller.is3200():
            _status_code = Message.short_circuit
        else:
            _status_code = Message.short_circuit_1000
        self.send_message(
            _action=ActionCommands.DO,
            _category=MasterValvesCommands.MasterValves,
            _identifier=self.master_valves.sn,
            _status_code=_status_code)

    #################################
    def check_for_short_circuit_message_not_present(self):
        # The status code for a 3200 and 1000 are different for this message
        if self.master_valves.controller.is3200():
            _status_code = Message.short_circuit
        else:
            _status_code = Message.short_circuit_1000
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=MasterValvesCommands.MasterValves,
                _identifier=self.master_valves.sn,
                _status_code=_status_code)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")


    #################################
    def verify_short_circuit_message(self):
        # The status code for a 3200 and 1000 are different for this message
        if self.master_valves.controller.is3200():
            _status_code = Message.short_circuit
        else:
            _status_code = Message.short_circuit_1000
        self.get_short_circuit_message()
        self.verify_message(_status_code=_status_code)

    # |-------------------------------------------------|
    #        Message: Checksum Messages (1000 Only)
    # |-------------------------------------------------|
    #################################
    def set_checksum_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=MasterValvesCommands.MasterValves,
            _identifier=self.master_valves.sn,
            _status_code=Message.checksum)

    #################################
    def get_checksum_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=MasterValvesCommands.MasterValves,
            _identifier=self.master_valves.sn,
            _status_code=Message.checksum)

    #################################
    def clear_checksum_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=MasterValvesCommands.MasterValves,
            _identifier=self.master_valves.sn,
            _status_code=Message.checksum)

    def check_for_checksum_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=MasterValvesCommands.MasterValves,
                _identifier=self.master_valves.sn,
                _status_code=Message.checksum)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_checksum_message(self):
        self.get_checksum_message()
        self.verify_message(_status_code=Message.checksum)

    # |-------------------------------------------------|
    #        Message: Low Voltage Messages (1000 Only)
    # |-------------------------------------------------|
    #################################
    def set_low_voltage_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=MasterValvesCommands.MasterValves,
            _identifier=self.master_valves.sn,
            _status_code=Message.low_voltage)

    #################################
    def get_low_voltage_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=MasterValvesCommands.MasterValves,
            _identifier=self.master_valves.sn,
            _status_code=Message.low_voltage)

    #################################
    def clear_low_voltage_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=MasterValvesCommands.MasterValves,
            _identifier=self.master_valves.sn,
            _status_code=Message.low_voltage)

    def check_for_low_voltage_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=MasterValvesCommands.MasterValves,
                _identifier=self.master_valves.sn,
                _status_code=Message.low_voltage)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_low_voltage_message(self):
        self.get_low_voltage_message()
        self.verify_message(_status_code=Message.low_voltage)