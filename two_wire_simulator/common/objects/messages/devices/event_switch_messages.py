from common.imports import opcodes
from common.objects.messages.base_messages import BaseMessages
from common.imports.types import ActionCommands, EventSwitchCommands
from common.imports.types import Message

__author__ = 'Baseline'


class EventSwitchMessages(BaseMessages):

    #################################
    def __init__(self, _event_switch_object):
        """
        :param _event_switch_object:     Controller this object is "attached" to \n
        :type _event_switch_object:      common.objects.devices.sw.EventSwitch
        """
        self.event_switch = _event_switch_object
        self.status_code = ''
        # Init parent class to inherit respective attributes
        BaseMessages.__init__(self, _controller=_event_switch_object.controller)

    #################################
    def build_message_header(self):
        if self.event_switch.controller.is3200():
            message_header = '{0}: {1}n'.format(
                str('Event biCoder'),
                self.event_switch.ds
            )
        else:   # If the controller is a 1000
            message_header = '{0} {1}-{2}n'.format(
                str('Event Switch'),
                self.event_switch.ad,
                self.event_switch.sn
            )
        return message_header

    #################################
    def build_message_tx(self, _status_code):
        """
        :param _status_code:
        :type _status_code: str

        """
        tx_msg = self.build_message_header()

        # Create the title and header for the event switch
        if self.event_switch.controller.is3200():
            if _status_code is Message.bad_serial:
                tx_msg += "{0}{1}{2}{3}".format(
                    "Device Error:n",
                    "Wrong device at this address.n",
                    "Device = ",
                    self.event_switch.sn
                )
            elif _status_code is Message.sensor_disabled:
                tx_msg += "{0}{1}{2}{3}{4}".format(
                    "Device Disabled:n",
                    "This device will not be usedn",
                    "in watering decisions.n",
                    "Serial Number = ",
                    str(self.event_switch.sn) + "n"
                )
            elif _status_code is Message.no_response:
                tx_msg += "{0}{1}{2}{3}".format(
                    "Two-wire Communication Failure:n",
                    "Unable to talk with this device.n",
                    "Device = ",
                    self.event_switch.sn
                )
        else:
            if _status_code is Message.checksum:
                tx_msg += "Comm Error"
            elif _status_code is Message.no_response:
                tx_msg += "No Reply"
            elif _status_code is Message.bad_serial:
                tx_msg += "Serial N. Mismatch"

        return tx_msg

    #################################
    def build_alarm_id(self, _status_code):
        """
        This returns the alarm string.

        :param _status_code:
        :type _status_code:
        :return:
        :rtype:
        """
        # If the controller type is 3200, use the 3200 ID formats
        if self.event_switch.controller.is3200():
            alarm_id = '{0}_{1}_{2}_{3}'.format(
                str(230),               # {0} this is the event switch number address
                opcodes.event_switch,   # {1}
                self.event_switch.sn,   # {2} this is the serial number to the event switch
                _status_code            # {3}
            )
        else:
            alarm_id = '{0}_{1}_{2}_{3}'.format(
                str(206 + self.event_switch.ad),    # {0} this is the event switch number address
                opcodes.event_switch,               # {1}
                self.event_switch.sn,               # {2} this is the serial number to the event switch
                _status_code                        # {3}
            )

        return alarm_id

    # |-------------------------------------------------|
    #         Message: Bad Serial Number
    # |-------------------------------------------------|
    #################################
    def set_bad_serial_number_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=EventSwitchCommands.Event_Switch,
            _identifier=self.event_switch.sn,
            _status_code=Message.bad_serial)

    #################################
    def get_bad_serial_number_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=EventSwitchCommands.Event_Switch,
            _identifier=self.event_switch.sn,
            _status_code=Message.bad_serial)

    #################################
    def clear_bad_serial_number_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=EventSwitchCommands.Event_Switch,
            _identifier=self.event_switch.sn,
            _status_code=Message.bad_serial)

    def check_for_bad_serial_number_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=EventSwitchCommands.Event_Switch,
                _identifier=self.event_switch.sn,
                _status_code=Message.bad_serial)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was not found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_bad_serial_number_message(self):
        self.get_bad_serial_number_message()
        self.verify_message(_status_code=Message.bad_serial)

    # |-------------------------------------------------|
    #        Message: No Response Messages
    # |-------------------------------------------------|
    #################################
    def set_no_response_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=EventSwitchCommands.Event_Switch,
            _identifier=self.event_switch.sn,
            _status_code=Message.no_response)

    #################################
    def get_no_response_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=EventSwitchCommands.Event_Switch,
            _identifier=self.event_switch.sn,
            _status_code=Message.no_response)

    #################################
    def clear_no_response_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=EventSwitchCommands.Event_Switch,
            _identifier=self.event_switch.sn,
            _status_code=Message.no_response)

    def check_for_no_response_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=EventSwitchCommands.Event_Switch,
                _identifier=self.event_switch.sn,
                _status_code=Message.no_response)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_no_response_message(self):
        self.get_no_response_message()
        self.verify_message(_status_code=Message.no_response)

    # |-------------------------------------------------|
    #        Message: Disabled Messages
    # |-------------------------------------------------|
    #################################
    def set_disabled_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=EventSwitchCommands.Event_Switch,
            _identifier=self.event_switch.sn,
            _status_code=Message.device_disabled)

    #################################
    def get_disabled_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=EventSwitchCommands.Event_Switch,
            _identifier=self.event_switch.sn,
            _status_code=Message.device_disabled)

    #################################
    def clear_disabled_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=EventSwitchCommands.Event_Switch,
            _identifier=self.event_switch.sn,
            _status_code=Message.device_disabled)

    def check_for_disabled_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=EventSwitchCommands.Event_Switch,
                _identifier=self.event_switch.sn,
                _status_code=Message.device_disabled)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_disabled_message(self):
        self.get_disabled_message()
        self.verify_message(_status_code=Message.device_disabled)

    # |-------------------------------------------------|
    #        Message: Checksum Messages (1000 Only)
    # |-------------------------------------------------|
    #################################
    def set_checksum_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=EventSwitchCommands.Event_Switch,
            _identifier=self.event_switch.sn,
            _status_code=Message.checksum)

    #################################
    def get_checksum_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=EventSwitchCommands.Event_Switch,
            _identifier=self.event_switch.sn,
            _status_code=Message.checksum)

    #################################
    def clear_checksum_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=EventSwitchCommands.Event_Switch,
            _identifier=self.event_switch.sn,
            _status_code=Message.checksum)

    def check_for_checksum_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=EventSwitchCommands.Event_Switch,
                _identifier=self.event_switch.sn,
                _status_code=Message.checksum)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_checksum_message(self):
        self.get_checksum_message()
        self.verify_message(_status_code=Message.checksum)
