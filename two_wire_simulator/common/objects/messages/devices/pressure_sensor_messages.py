from common.imports import opcodes
from common.objects.messages.base_messages import BaseMessages
from common.imports.types import ActionCommands, PressureSensorCommands
from common.imports.types import Message

__author__ = 'Baseline'


class PressureSensorMessages(BaseMessages):

    #################################
    def __init__(self, _pressure_sensor_object):
        """
        :param _pressure_sensor_object:     Controller this object is "attached" to \n
        :type _pressure_sensor_object:      common.objects.devices.ps.PressureSensor
        """
        self.pressure_sensor = _pressure_sensor_object
        self.status_code = ''
        # Init parent class to inherit respective attributes
        BaseMessages.__init__(self, _controller=_pressure_sensor_object.controller)

    #################################
    def build_message_header(self, _status_code):
        if _status_code is Message.disabled:
            message_header = '{0}: {1}n'.format(
                str('Pressure Sensor'),
                self.pressure_sensor.ds
            )
        else:
            message_header = '{0}: {1}n'.format(
                str('Analog biCoder'),
                self.pressure_sensor.sn
            )
        return message_header

    #################################
    def build_message_tx(self, _status_code):
        """
        :param _status_code:
        :type _status_code: str

        """
        tx_msg = self.build_message_header(_status_code=_status_code)

        # Create the title and header for the moisture sensor
        if _status_code is Message.bad_serial:
            tx_msg += "{0}{1}{2}{3}".format(
                "Device Error:n",
                "Wrong device at this address.n",
                "Device = ",
                self.pressure_sensor.sn
            )
        elif _status_code is Message.sensor_disabled:
            tx_msg += "{0}{1}{2}{3}{4}".format(
                "Device Disabled:n",
                "This device will not be usedn",
                "in watering decisions.n",
                "Serial Number = ",
                str(self.pressure_sensor.sn) + "n"
            )
        elif _status_code is Message.no_response:
            tx_msg += "{0}{1}{2}{3}".format(
                "Two-wire Communication Failure:n",
                "Unable to talk with this device.n",
                "Device = ",
                self.pressure_sensor.sn
            )
        elif _status_code is Message.bad_reading:
            tx_msg += "{0}{1}{2}{3}{4}".format(
                "Pressure Sensor Failure:n",
                "Unable to get a valid pressuren",
                "reading from biCoder.n",
                "Pressure Sensor = ",
                str(self.pressure_sensor.sn) + "n"
            )
        return tx_msg

    #################################
    def build_alarm_id(self, _status_code):
        """
        This returns the alarm string.

        :param _status_code:
        :type _status_code:
        :return:
        :rtype:
        """
        # If the controller type is 3200, use the 3200 ID formats
        alarm_id = '{0}_{1}_{2}_{3}'.format(
            str(250),                   # {0} this is the pressuresensor number address
            opcodes.pressure_sensor,    # {1}
            self.pressure_sensor.sn,    # {2} this is the serial number to the event switch
            _status_code                # {3}
        )
        return alarm_id

    # |-------------------------------------------------|
    #         Message: Bad Serial Number
    # |-------------------------------------------------|
    #################################
    def set_bad_serial_number_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=PressureSensorCommands.Pressure_Sensor,
            _identifier=self.pressure_sensor.sn,
            _status_code=Message.bad_serial)

    #################################
    def get_bad_serial_number_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=PressureSensorCommands.Pressure_Sensor,
            _identifier=self.pressure_sensor.sn,
            _status_code=Message.bad_serial)

    #################################
    def clear_bad_serial_number_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=PressureSensorCommands.Pressure_Sensor,
            _identifier=self.pressure_sensor.sn,
            _status_code=Message.bad_serial)

    def check_for_bad_serial_number_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=PressureSensorCommands.Pressure_Sensor,
                _identifier=self.pressure_sensor.sn,
                _status_code=Message.bad_serial)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was not found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_bad_serial_number_message(self):
        self.get_bad_serial_number_message()
        self.verify_message(_status_code=Message.bad_serial)

    # |-------------------------------------------------|
    #        Message: No Response Messages
    # |-------------------------------------------------|
    #################################
    def set_no_response_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=PressureSensorCommands.Pressure_Sensor,
            _identifier=self.pressure_sensor.sn,
            _status_code=Message.no_response)

    #################################
    def get_no_response_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=PressureSensorCommands.Pressure_Sensor,
            _identifier=self.pressure_sensor.sn,
            _status_code=Message.no_response)

    #################################
    def clear_no_response_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=PressureSensorCommands.Pressure_Sensor,
            _identifier=self.pressure_sensor.sn,
            _status_code=Message.no_response)

    def check_for_no_response_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=PressureSensorCommands.Pressure_Sensor,
                _identifier=self.pressure_sensor.sn,
                _status_code=Message.no_response)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_no_response_message(self):
        self.get_no_response_message()
        self.verify_message(_status_code=Message.no_response)

    # |-------------------------------------------------|
    #        Message: Disabled Messages
    # |-------------------------------------------------|
    #################################
    def set_disabled_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=PressureSensorCommands.Pressure_Sensor,
            _identifier=self.pressure_sensor.sn,
            _status_code=Message.device_disabled)

    #################################
    def get_disabled_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=PressureSensorCommands.Pressure_Sensor,
            _identifier=self.pressure_sensor.sn,
            _status_code=Message.device_disabled)

    #################################
    def clear_disabled_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=PressureSensorCommands.Pressure_Sensor,
            _identifier=self.pressure_sensor.sn,
            _status_code=Message.device_disabled)

    def check_for_disabled_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=PressureSensorCommands.Pressure_Sensor,
                _identifier=self.pressure_sensor.sn,
                _status_code=Message.device_disabled)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_disabled_message(self):
        self.get_disabled_message()
        self.verify_message(_status_code=Message.device_disabled)

    # |-------------------------------------------------|
    #        Message: Invalid Pressure Reading
    # |-------------------------------------------------|
    #################################
    def set_invalid_pressure_reading_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=PressureSensorCommands.Pressure_Sensor,
            _identifier=self.pressure_sensor.sn,
            _status_code=Message.invalid_reading)

    #################################
    def get_invalid_pressure_reading_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=PressureSensorCommands.Pressure_Sensor,
            _identifier=self.pressure_sensor.sn,
            _status_code=Message.invalid_reading)

    #################################
    def clear_invalid_pressure_reading_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=PressureSensorCommands.Pressure_Sensor,
            _identifier=self.pressure_sensor.sn,
            _status_code=Message.invalid_reading)

    def check_for_invalid_pressure_reading_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=PressureSensorCommands.Pressure_Sensor,
                _identifier=self.pressure_sensor.sn,
                _status_code=Message.invalid_reading)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_invalid_pressure_reading_message(self):
        self.get_invalid_pressure_reading_message()
        self.verify_message(_status_code=Message.invalid_reading)
