from datetime import datetime, timedelta
from common.date_package.date_resource import date_mngr

from common.imports import opcodes
from common.objects.messages.base_messages import BaseMessages
from common.imports.types import ActionCommands, TemperatureSensorCommands
from common.imports.types import Message, MessageCategory, status_plus_priority_code_dict_3200, status_code_dict_1000, \
    program_1000_who_dict

__author__ = 'Baseline'


class TemperatureSensorMessages(BaseMessages):

    #################################
    def __init__(self, _temperature_sensor_object):
        """
        :param _temperature_sensor_object:     Controller this object is "attached" to \n
        :type _temperature_sensor_object:      common.objects.devices.ts.TemperatureSensor
        """
        self.temperature_sensor = _temperature_sensor_object
        self.status_code = ''
        # Init parent class to inherit respective attributes
        BaseMessages.__init__(self, _controller=_temperature_sensor_object.controller)

    #################################
    def build_message_header(self):
        if self.temperature_sensor.controller.is3200():
            message_header = '{0}{1}n'.format(
                str('Temperature biSensor: '),
                self.temperature_sensor.ds
                )
        else:   # If the controller is a 1000
            message_header = '{0} {1}-{2}n'.format(
                str('Temp. Sensor'),
                self.temperature_sensor.ad,
                self.temperature_sensor.sn
            )
        return message_header

    #################################
    def build_message_tx(self, _status_code):
        """
        :param _status_code:
        :type _status_code: str

        """
        tx_msg = self.build_message_header()
        # If the object passed in part of a 1000 controller, create 3200 specific text messages

        # Create the title and header for the temperature sensor
        if self.temperature_sensor.controller.is3200():
            if _status_code is Message.bad_serial:
                tx_msg += "{0}{1}{2}{3}".format(
                    "Device Error:n",                       # {0}
                    "Wrong device at this address.n",       # {1}
                    "Device = ",                            # {2}
                    self.temperature_sensor.sn              # {3}
                )
            elif _status_code is Message.device_disabled:
                tx_msg += "{0}{1}{2}{3}{4}".format(
                    "Device Disabled:n",                # {0}
                    "This device will not be usedn",    # {1}
                    "in watering decisions.n",          # {2}
                    "Serial Number = ",                 # {3}
                    self.temperature_sensor.sn + "n"    # {4}
                )

            elif _status_code is Message.no_response:
                tx_msg += "{0}{1}{2}{3}".format(
                    "Two-wire Communication Failure:n",     # {0}
                    "Unable to talk with this device.n",    # {1}
                    "Device = ",                            # {2}
                    self.temperature_sensor.sn              # {3}
                )
        else:
            if _status_code is Message.checksum:
                tx_msg += "Comm Error"
            elif _status_code is Message.no_response:
                tx_msg += "No Reply"
            elif _status_code is Message.bad_serial:
                tx_msg += "Serial N. Mismatch"
        return tx_msg

    #################################
    def build_alarm_id(self, _status_code):
        """
        This return the alarm string.

        :param _status_code:
        :type _status_code:
        :return:
        :rtype:
        """
        # If the controller type is 3200, use the 3200 ID formats
        if self.temperature_sensor.controller.is3200():
            alarm_id = '{0}_{1}_{2}_{3}'.format(
                str(240),                       # {0} this is the temperature sensor number address
                opcodes.temperature_sensor,     # {1}
                self.temperature_sensor.sn,     # {2} this is the serial number to the temperature sensor
                _status_code                    # {3}
            )
        else:
            alarm_id = '{0}_{1}_{2}_{3}'.format(
                str(self.temperature_sensor.ad + 216),  # {0} this is the temperature sensor number address
                opcodes.temperature_sensor,             # {1}
                self.temperature_sensor.sn,             # {2} this is the temperature sensor serial number
                _status_code                            # {3}
            )
        return alarm_id

    # |-------------------------------------------------|
    #         Message: Bad Serial Number
    # |-------------------------------------------------|
    #################################
    def set_bad_serial_number_message(self, ):
        self.send_message(
            _action=ActionCommands.SET,
            _category=TemperatureSensorCommands.Temperature_Sensor,
            _identifier=self.temperature_sensor.sn,
            _status_code=Message.bad_serial)

    #################################
    def get_bad_serial_number_message(self, ):
        self.get_message(
            _action=ActionCommands.GET,
            _category=TemperatureSensorCommands.Temperature_Sensor,
            _identifier=self.temperature_sensor.sn,
            _status_code=Message.bad_serial)

    #################################
    def clear_bad_serial_number_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=TemperatureSensorCommands.Temperature_Sensor,
            _identifier=self.temperature_sensor.sn,
            _status_code=Message.bad_serial)

    def check_for_bad_serial_number_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=TemperatureSensorCommands.Temperature_Sensor,
                _identifier=self.temperature_sensor.sn,
                _status_code=Message.bad_serial)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_bad_serial_number_message(self):
        self.get_bad_serial_number_message()
        self.verify_message(_status_code=Message.bad_serial)

    # |-------------------------------------------------|
    #        Message: Device Disabled
    # |-------------------------------------------------|
    #################################
    def set_device_disabled_message(self, ):
        self.send_message(
            _action=ActionCommands.SET,
            _category=TemperatureSensorCommands.Temperature_Sensor,
            _identifier=self.temperature_sensor.sn,
            _status_code=Message.device_disabled)

    #################################
    def get_device_disabled_message(self, ):
        self.get_message(
            _action=ActionCommands.GET,
            _category=TemperatureSensorCommands.Temperature_Sensor,
            _identifier=self.temperature_sensor.sn,
            _status_code=Message.device_disabled)

    #################################
    def clear_device_disabled_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=TemperatureSensorCommands.Temperature_Sensor,
            _identifier=self.temperature_sensor.sn,
            _status_code=Message.device_disabled)

    #################################
    def check_for_device_disabled_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=TemperatureSensorCommands.Temperature_Sensor,
                _identifier=self.temperature_sensor.sn,
                _status_code=Message.device_disabled)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_device_disabled_message(self):
        self.get_device_disabled_message()
        self.verify_message(_status_code=Message.device_disabled)

    # |-------------------------------------------------|
    #        Message: No Response Messages
    # |-------------------------------------------------|
    #################################
    def set_no_response_message(self, ):
        self.send_message(
            _action=ActionCommands.SET,
            _category=TemperatureSensorCommands.Temperature_Sensor,
            _identifier=self.temperature_sensor.sn,
            _status_code=Message.no_response)

    #################################
    def get_no_response_message(self, ):
        self.get_message(
            _action=ActionCommands.GET,
            _category=TemperatureSensorCommands.Temperature_Sensor,
            _identifier=self.temperature_sensor.sn,
            _status_code=Message.no_response)

    #################################
    def clear_no_response_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=TemperatureSensorCommands.Temperature_Sensor,
            _identifier=self.temperature_sensor.sn,
            _status_code=Message.no_response)

    def check_for_no_response_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=TemperatureSensorCommands.Temperature_Sensor,
                _identifier=self.temperature_sensor.sn,
                _status_code=Message.no_response)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_no_response_message(self):
        self.get_no_response_message()
        self.verify_message(_status_code=Message.no_response)

    # |-------------------------------------------------|
    #        Message: Checksum Messages (1000 Only)
    # |-------------------------------------------------|
    #################################
    def set_checksum_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=TemperatureSensorCommands.Temperature_Sensor,
            _identifier=self.temperature_sensor.sn,
            _status_code=Message.checksum)

    #################################
    def get_checksum_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=TemperatureSensorCommands.Temperature_Sensor,
            _identifier=self.temperature_sensor.sn,
            _status_code=Message.checksum)

    #################################
    def clear_checksum_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=TemperatureSensorCommands.Temperature_Sensor,
            _identifier=self.temperature_sensor.sn,
            _status_code=Message.checksum)

    def check_for_checksum_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=TemperatureSensorCommands.Temperature_Sensor,
                _identifier=self.temperature_sensor.sn,
                _status_code=Message.checksum)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_checksum_message(self):
        self.get_checksum_message()
        self.verify_message(_status_code=Message.checksum)

