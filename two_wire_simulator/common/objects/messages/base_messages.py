import status_parser

from common.date_package.date_resource import date_mngr
from datetime import datetime, timedelta
from common.imports import opcodes
from common.imports.types import ControllerCommands

from common.imports.types import ActionCommands
from common.imports.types import Message, MessageCategory, status_plus_priority_code_dict_3200, status_code_dict_1000, \
    program_1000_who_dict

__author__ = 'Baseline'


class BaseMessages(object):
    def __init__(self, _controller):
        self.ser = _controller.ser
        self.message_data = status_parser.KeyValues(string_in=None)

    #################################
    def build_command_string(self, _action, _category, _identifier, _status_code, _data_required_string=None):
        """
        Builds a string that will be set through the serial port. \n

        :param _action:         SET | GET | DO \n
        :type _action:          str \n

        :param _category:       The type of object (ZN, PG, etc.) \n
        :type _category:        str \n

        :param _identifier:     This is what is used to identify the object of this message. \n
        :type _identifier:      str | int \n

        :param _status_code:    Controller this object is "attached" to \n
        :type _status_code:     str \n

        :param _data_required_string:    Extra data that is appended on the end of the string \n
        :type _data_required_string:     str \n

        """
        if _category in [opcodes.controller, opcodes.basemanager]:
            command = "{0},{1},{2},{3}={4}".format(
                _action,                # {0}  Action Command
                opcodes.message,        # {1} 'MG'
                _category,              # {2} The type of object
                opcodes.status_code,    # {3} Status code opcode
                _status_code            # {4} Status code passed in
            )
        else:
            command = "{0},{1},{2}={3},{4}={5}".format(
                _action,                # {0}  Action Command
                opcodes.message,        # {1} 'MG'
                _category,              # {2} The type of object
                _identifier,            # {3} Identifier (Usually a  serial number or address)
                opcodes.status_code,    # {4} Status code opcode
                _status_code            # {5} Status code passed in
            )
        # This string could be one or more key value pairs that are required for a certain message
        if _data_required_string:
            command += ",{0}".format(_data_required_string)
        return command

    #################################
    def send_message(self, _action, _category, _identifier, _status_code, _data_required_string=None):
        """
        Sets a message through the serial port. \n

        :param _action:         SET | GET | DO \n
        :type _action:          str \n

        :param _category:       The type of object (ZN, PG, etc.) \n
        :type _category:        str \n

        :param _identifier:     This is what is used to identify the object of this message. \n
        :type _identifier:      str | int | None \n

        :param _status_code:    Controller this object is "attached" to \n
        :type _status_code:     str \n

        :param _data_required_string:    Extra data that is appended on the end of the string \n
        :type _data_required_string:     str \n

        """
        _command = self.build_command_string(
            _action=_action,
            _category=_category,
            _identifier=_identifier,
            _status_code=_status_code,
            _data_required_string=_data_required_string)
        try:
            # Attempt to set message on controller
            self.ser.send_and_wait_for_reply(tosend=_command)
        except Exception:
            e_msg = "Exception occurred trying to {0} a message on {1}' : {2}  to: '{3}'. " \
                    "Command was: {4} ".format(_action,_category,_identifier, _status_code, _command)
            raise Exception(e_msg)

    #################################
    def get_message(self, _action, _category, _identifier, _status_code, _data_required_string=None):
        """
        Sets a message through the serial port. \n

        :param _action:         SET | GET | DO \n
        :type _action:          str \n

        :param _category:       The type of object (ZN, PG, etc.) \n
        :type _category:        str \n

        :param _identifier:     This is what is used to identify the object of this message. \n
        :type _identifier:      str | int | None \n

        :param _status_code:    Controller this object is "attached" to \n
        :type _status_code:     str \n

        :param _data_required_string:    Extra data that is appended on the end of the string \n
        :type _data_required_string:     str \n

        """
        _command = self.build_command_string(
            _action=_action,
            _category=_category,
            _identifier=_identifier,
            _status_code=_status_code,
            _data_required_string=_data_required_string)
        try:
            # Attempt to set Controller default values
            self.message_data = self.ser.get_and_wait_for_reply(tosend=_command)
        except Exception:
            e_msg = "Exception occurred trying to {0} a message on {1}' : {2}  to: '{3}'. " \
                    "Command was: {4} ".format(_action, _category, _identifier, _status_code, _command)
            raise Exception(e_msg)

    #################################
    def build_date_time_string(self):
        get_datetime = date_mngr.controller_datetime.msg_date_string_for_controller_3200() + \
                       ' ' + date_mngr.controller_datetime.msg_time_string_for_controller_3200()
        date_time = '{0}'.format(
            str(get_datetime)
        )
        return date_time

    #################################
    def convert_op_cl_to_open_closed(self, _string):
        """
        Convert an "OP" string to "OPEN" and a "CL" string to "CLOSED".
        """
        if _string == opcodes.closed:
            return_string = "Closed"
        elif _string == opcodes.open:
            return_string = "Open"
        else:
            return_string = "None"

        return return_string

    #################################
    def build_message_tx(self, _status_code):
        pass

    #################################
    def build_alarm_id(self, _status_code):
        pass

    #################################
    def verify_message(self, _status_code, _verify_date_time=True):
        """
        the expected values are retrieved from the controller and then compared against the values that are returned
        from the message module
        :param _status_code
        :type _status_code :str
        :param _verify_date_time
        :type _verify_date_time :bool
        :return:

        """
        # These are our generated values to verify against the controller
        created_message_text_from_cn = self.build_message_tx(_status_code=_status_code)
        created_message_id_from_cn = self.build_alarm_id(_status_code=_status_code)
        created_date_time_from_cn = self.build_date_time_string()
        # These are the values returned from the controller
        actual_message_text_from_cn = self.message_data.get_value_string_by_key(opcodes.message_text)
        actual_message_id_from_cn = self.message_data.get_value_string_by_key(opcodes.message_id)
        actual_date_time_from_cn = self.message_data.get_value_string_by_key(opcodes.date_time)

        # first see if the 9 character is an X or a K
        parsed_out_character = str(actual_message_id_from_cn[8])
        if parsed_out_character == 'K' or parsed_out_character == 'X':
            # this goes through and strips out the x or the k out of the ID because we don't care about it
            actual_message_first_part = actual_message_id_from_cn[:8]
            actual_message_second_part = actual_message_id_from_cn[9:]
            created_message_first_part = created_message_id_from_cn[:8]
            created_message_second_part = created_message_id_from_cn[9:]

            new_actual_message_id_from_cn = str(actual_message_first_part + actual_message_second_part)
            new_created_message_id_from_cn = str(created_message_first_part + created_message_second_part)

            if new_actual_message_id_from_cn != new_created_message_id_from_cn:
                    e_msg = "Created ID message did not match the ID received from the controller:\n" \
                            "\tCreated: \t\t'{0}'\n" \
                            "\tReceived:\t\t'{1}'\n".format(
                                created_message_id_from_cn,     # {0} The ID that we built
                                actual_message_id_from_cn       # {1} The ID returned from controller
                            )
                    raise ValueError(e_msg)
        # Compare status versus what is on the controller
        elif actual_message_id_from_cn != created_message_id_from_cn:
                e_msg = "Created ID message did not match the ID received from the controller:\n" \
                        "\tCreated: \t\t'{0}'\n" \
                        "\tReceived:\t\t'{1}'\n".format(
                            created_message_id_from_cn,     # {0} The ID that we built
                            actual_message_id_from_cn       # {1} The ID returned from controller
                        )
                raise ValueError(e_msg)
        if _verify_date_time:
            mess_date_time = datetime.strptime(actual_date_time_from_cn, '%m/%d/%y %H:%M:%S')
            controller_date_time = datetime.strptime(created_date_time_from_cn, '%m/%d/%y %H:%M:%S')
            if mess_date_time != controller_date_time:
                if abs(controller_date_time - mess_date_time) >= timedelta(days=0, hours=0, minutes=45, seconds=0):
                    e_msg = "The date and time of the message didn't match the controller:\n" \
                            "\tCreated: \t\t'{0}'\n" \
                            "\tReceived:\t\t'{1}'\n".format(
                                created_date_time_from_cn,      # {0} The date message that we built
                                actual_date_time_from_cn        # {1} The date message returned from controller
                            )
                    raise ValueError(e_msg)
                # only print this if they were not exact
                e_msg = "############################  Date and time were not exact but were within +- 30 minutes \n" \
                        "############################  Controller Date and Time = {0} \n" \
                        "############################  Message Date time = {1}".format(
                            actual_date_time_from_cn,   # {0} Date time received from controller
                            created_date_time_from_cn   # {1} Date time the message was verified
                        )
                print(e_msg)

        if created_message_text_from_cn != actual_message_text_from_cn:
            e_msg = "Created TX message did not match the TX received from the controller:\n" \
                    "\tCreated: \t\t'{0}'\n" \
                    "\tReceived:\t\t'{1}'\n".format(
                        created_message_text_from_cn,   # {0} The TX message that we built
                        actual_message_text_from_cn     # {1} The TX message returned from controller
                    )
            raise ValueError(e_msg)
        else:
            print("Successfully verified:\n"
                  "\tID: '{0}'\n"
                  "\tDT: '{1}'\n"
                  "\tTX: '{2}'\n".format(
                        created_message_id_from_cn,     # {0} Message ID we created
                        created_date_time_from_cn,      # {1} Message DT we created
                        created_message_text_from_cn    # {2} Message TX we created
                    ))
