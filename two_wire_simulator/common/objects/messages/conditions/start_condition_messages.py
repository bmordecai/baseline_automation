from datetime import datetime, timedelta
from common.date_package.date_resource import date_mngr

from common.imports import opcodes
from common.objects.messages.base_messages import BaseMessages
from common.imports.types import ActionCommands, StartConditionCommands
from common.imports.types import Message, MessageCategory

__author__ = 'Baseline'


class StartConditionMessages(BaseMessages):

    #################################
    def __init__(self, _start_condition_object):
        """
        :param _start_condition_object: Object this message is attached to. \n
        :type _start_condition_object:  common.objects.programming.conditions.start_condition.StartCondition |
                                        common.objects.programming.conditions.start_condition.PressureStartCondition |
                                        common.objects.programming.conditions.start_condition.MoistureStartCondition |
                                        common.objects.programming.conditions.start_condition.SwitchStartCondition
        """
        self.start_condition = _start_condition_object
        self.status_code = ''
        # Init parent class to inherit respective attributes
        BaseMessages.__init__(self, _controller=_start_condition_object.controller)

        # Required Data
        self.device = None

    #################################
    def update_values(self):
        """
        This method updates all of the values that this message will use. \n
        """
        # Get the device on the empty condition
        if self.start_condition.identifiers[1][0] is opcodes.pressure_sensor_ps:
            for pressure_sensor in self.start_condition.controller.pressure_sensors.values():
                if pressure_sensor.sn == self.start_condition.device_serial:
                    self.device = pressure_sensor
        elif self.start_condition.identifiers[1][0] is opcodes.event_switch:
            for event_switch in self.start_condition.controller.event_switches.values():
                if event_switch.sn == self.start_condition.device_serial:
                    self.device = event_switch
        elif self.start_condition.identifiers[1][0] is opcodes.moisture_sensor:
            for moisture_sensor in self.start_condition.controller.moisture_sensors.values():
                if moisture_sensor.sn == self.start_condition.device_serial:
                    self.device = moisture_sensor
        elif self.start_condition.identifiers[1][0] is opcodes.temperature_sensor:
            for temperature_sensor in self.start_condition.controller.temperature_sensors.values():
                if temperature_sensor.sn == self.start_condition.device_serial:
                    self.device = temperature_sensor

    #################################
    def build_message_header(self):
        message_header = 'Program: {0}n'.format(
            self.start_condition.controller.programs[self.start_condition.program_ad].ds
        )
        return message_header

    #################################
    def build_message_tx(self, _status_code):
        """
        :param _status_code:
        :type _status_code: str

        """
        self.update_values()
        tx_msg = self.build_message_header()

        if _status_code is Message.started_event_switch:
            tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}".format(
                "Program Started:n",
                "Event biCoder has been triggeredn",
                "Program = ",
                str(self.start_condition.program_ad) + "n",
                "Event biCoder = ",
                str(self.start_condition.device_serial) + "n",
                "Limit/Value = ",
                str(self.convert_op_cl_to_open_closed(self.start_condition.mode)) + " / ",
                str(self.convert_op_cl_to_open_closed(self.device.bicoder.vc))
            )

        elif _status_code is Message.started_moisture_sensor:
            tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}".format(
                "Program Started:n",
                "Moisture limit has been reachedn",
                "Program = ",
                str(self.start_condition.program_ad) + "n",
                "Moisture biSensor = ",
                str(self.start_condition.device_serial) + "n",
                "Limit/Value = ",
                str(self.start_condition.threshold) + " / ",
                str(self.device.bicoder.vp)                           # moisture sensor moisture percent
            )

        elif _status_code is Message.started_temp_sensor:
            tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}".format(
                "Program Started:n",
                "Temperature limit has been reachedn",
                "Program = ",
                str(self.start_condition.program_ad) + "n",
                "Temperature biSensor = ",
                str(self.start_condition.device_serial) + "n",
                "Limit/Value = ",
                str(self.start_condition.threshold) + " / ",
                str(self.device.bicoder.vd)
            )

        elif _status_code is Message.started_pressure_sensor:
            tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}".format(
                "Program Started:n",
                "Pressure limit has been reachedn",
                "Program = ",
                str(self.start_condition.program_ad) + "n",
                "Pressure Sensor = ",
                str(self.start_condition.device_serial) + "n",
                "Limit/Value = ",
                str(self.start_condition.threshold) + " / ",
                str(self.device.bicoder.va)
            )

        return tx_msg

    #################################
    def build_alarm_id(self, _status_code):
        """
        This return the alarm string.

        :param _status_code:
        :type _status_code:
        :return:
        :rtype:
        """
        # Make an alarm ID for the start condition
        alarm_id = '{0}_{1}_{2}_{3}_{4}_{5}'.format(
            str(300 + self.start_condition.program_ad).zfill(3),    # {0} Program empty condition ID number
            StartConditionCommands.Program,                         # {1}
            self.start_condition.program_ad,                        # {2} address of the program
            opcodes.device_value,                                   # {3}
            self.start_condition.device_serial,                     # {4} Serial number of device on condition
            _status_code                                            # {5} status code
        )

        return alarm_id

    # |---------------------------------------------------------------|
    #         Message: Start Condition With a Pressure Sensor
    # |---------------------------------------------------------------|
    #################################
    def set_start_condition_with_pressure_sensor_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1},{2}={3},{4}={5}".format(
            opcodes.device_value,
            self.start_condition.device_serial,
            opcodes.variable_1,
            self.start_condition.threshold,
            opcodes.variable_2,
            self.device.bicoder.va)

        self.send_message(
            _action=ActionCommands.SET,
            _category=StartConditionCommands.Program,
            _identifier=self.start_condition.program_ad,
            _status_code=Message.started_pressure_sensor,
            _data_required_string=data_required_string)

    #################################
    def get_start_condition_with_pressure_sensor_message(self):
        # The get string requires extra data
        self.update_values()
        data_required_string = "{0}={1}".format(
            opcodes.device_value,
            self.device.bicoder.sn)

        self.get_message(
            _action=ActionCommands.GET,
            _category=StartConditionCommands.Program,
            _identifier=self.start_condition.program_ad,
            _status_code=Message.started_pressure_sensor,
            _data_required_string=data_required_string)

    #################################
    def clear_start_condition_with_pressure_sensor_message(self):
        # The clear string requires extra data
        self.update_values()
        data_required_string = "{0}={1}".format(
            opcodes.device_value,
            self.device.bicoder.sn)

        self.get_message(
            _action=ActionCommands.DO,
            _category=StartConditionCommands.Program,
            _identifier=self.start_condition.program_ad,
            _status_code=Message.started_pressure_sensor,
            _data_required_string=data_required_string)

    def check_for_start_condition_with_pressure_sensor_message_not_present(self):
        try:
            # The set string requires extra data
            self.update_values()
            data_required_string = "{0}={1}".format(
                opcodes.device_value,
                self.device.bicoder.sn)

            self.send_message(
                _action=ActionCommands.SET,
                _category=StartConditionCommands.Program,
                _identifier=self.start_condition.program_ad,
                _status_code=Message.started_pressure_sensor,
                _data_required_string=data_required_string)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_start_condition_with_pressure_sensor_message(self):
        self.get_start_condition_with_pressure_sensor_message()
        self.verify_message(_status_code=Message.started_pressure_sensor)

    # |---------------------------------------------------------------|
    #         Message: Start Condition With a Moisture Sensor
    # |---------------------------------------------------------------|
    #################################
    def set_start_condition_with_moisture_sensor_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1},{2}={3},{4}={5}".format(
            opcodes.device_value,
            self.start_condition.device_serial,
            opcodes.variable_1,
            self.start_condition.threshold,
            opcodes.variable_2,
            self.device.bicoder.vp)

        self.send_message(
            _action=ActionCommands.SET,
            _category=StartConditionCommands.Program,
            _identifier=self.start_condition.program_ad,
            _status_code=Message.started_moisture_sensor,
            _data_required_string=data_required_string)

    #################################
    def get_start_condition_with_moisture_sensor_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1}".format(
            opcodes.device_value,
            self.device.bicoder.sn)

        self.get_message(
            _action=ActionCommands.GET,
            _category=StartConditionCommands.Program,
            _identifier=self.start_condition.program_ad,
            _status_code=Message.started_moisture_sensor,
            _data_required_string=data_required_string)

    #################################
    def clear_start_condition_with_moisture_sensor_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1}".format(
            opcodes.device_value,
            self.device.bicoder.sn)

        self.get_message(
            _action=ActionCommands.DO,
            _category=StartConditionCommands.Program,
            _identifier=self.start_condition.program_ad,
            _status_code=Message.started_moisture_sensor,
            _data_required_string=data_required_string)

    def check_for_start_condition_with_moisture_sensor_message_not_present(self):
        try:
            # The set string requires extra data
            self.update_values()
            data_required_string = "{0}={1}".format(
                opcodes.device_value,
                self.device.bicoder.sn)

            self.send_message(
                _action=ActionCommands.SET,
                _category=StartConditionCommands.Program,
                _identifier=self.start_condition.program_ad,
                _status_code=Message.started_moisture_sensor,
                _data_required_string=data_required_string)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_start_condition_with_moisture_sensor_message(self):
        self.get_start_condition_with_moisture_sensor_message()
        self.verify_message(_status_code=Message.started_moisture_sensor)

    # |---------------------------------------------------------------|
    #         Message: Start Condition With a Temperature Sensor
    # |---------------------------------------------------------------|
    #################################
    def set_start_condition_with_temperature_sensor_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1},{2}={3},{4}={5}".format(
            opcodes.device_value,
            self.start_condition.device_serial,
            opcodes.variable_1,
            self.start_condition.threshold,
            opcodes.variable_2,
            self.device.bicoder.vd)

        self.send_message(
            _action=ActionCommands.SET,
            _category=StartConditionCommands.Program,
            _identifier=self.start_condition.program_ad,
            _status_code=Message.started_temp_sensor,
            _data_required_string=data_required_string)

    #################################
    def get_start_condition_with_temperature_sensor_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1}".format(
            opcodes.device_value,
            self.device.bicoder.sn)

        self.get_message(
            _action=ActionCommands.GET,
            _category=StartConditionCommands.Program,
            _identifier=self.start_condition.program_ad,
            _status_code=Message.started_temp_sensor,
            _data_required_string=data_required_string)

    #################################
    def clear_start_condition_with_temperature_sensor_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1}".format(
            opcodes.device_value,
            self.device.bicoder.sn)

        self.get_message(
            _action=ActionCommands.DO,
            _category=StartConditionCommands.Program,
            _identifier=self.start_condition.program_ad,
            _status_code=Message.started_temp_sensor,
            _data_required_string=data_required_string)

    def check_for_start_condition_with_temperature_sensor_message_not_present(self):
        try:
            # The set string requires extra data
            self.update_values()
            data_required_string = "{0}={1}".format(
                opcodes.device_value,
                self.device.bicoder.sn)

            self.send_message(
                _action=ActionCommands.SET,
                _category=StartConditionCommands.Program,
                _identifier=self.start_condition.program_ad,
                _status_code=Message.started_temp_sensor,
                _data_required_string=data_required_string)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_start_condition_with_temperature_sensor_message(self):
        self.get_start_condition_with_temperature_sensor_message()
        self.verify_message(_status_code=Message.started_temp_sensor)

    # |---------------------------------------------------------------|
    #         Message: Start Condition With an Event Switch
    # |---------------------------------------------------------------|
    #################################
    def set_start_condition_with_event_switch_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1},{2}={3},{4}={5}".format(
            opcodes.device_value,
            self.start_condition.device_serial,
            opcodes.variable_1,
            self.start_condition.mode,
            opcodes.variable_2,
            self.device.bicoder.vc)

        self.send_message(
            _action=ActionCommands.SET,
            _category=StartConditionCommands.Program,
            _identifier=self.start_condition.program_ad,
            _status_code=Message.started_event_switch,
            _data_required_string=data_required_string)

    #################################
    def get_start_condition_with_event_switch_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1}".format(
            opcodes.device_value,
            self.device.bicoder.sn)

        self.get_message(
            _action=ActionCommands.GET,
            _category=StartConditionCommands.Program,
            _identifier=self.start_condition.program_ad,
            _status_code=Message.started_event_switch,
            _data_required_string=data_required_string)

    #################################
    def clear_start_condition_with_event_switch_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1}".format(
            opcodes.device_value,
            self.device.bicoder.sn)

        self.get_message(
            _action=ActionCommands.DO,
            _category=StartConditionCommands.Program,
            _identifier=self.start_condition.program_ad,
            _status_code=Message.started_event_switch,
            _data_required_string=data_required_string)

    def check_for_start_condition_with_event_switch_message_not_present(self):
        try:
            # The set string requires extra data
            self.update_values()
            data_required_string = "{0}={1}".format(
                opcodes.device_value,
                self.device.bicoder.sn)

            self.send_message(
                _action=ActionCommands.SET,
                _category=StartConditionCommands.Program,
                _identifier=self.start_condition.program_ad,
                _status_code=Message.started_event_switch,
                _data_required_string=data_required_string)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was not found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_start_condition_with_event_switch_message(self):
        self.get_start_condition_with_event_switch_message()
        self.verify_message(_status_code=Message.started_event_switch)
