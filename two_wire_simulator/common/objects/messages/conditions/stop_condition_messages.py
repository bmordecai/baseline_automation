from datetime import datetime, timedelta
from common.date_package.date_resource import date_mngr

from common.imports import opcodes
from common.objects.messages.base_messages import BaseMessages
from common.imports.types import ActionCommands, StopConditionCommands, ControllerCommands
from common.imports.types import Message, MessageCategory

__author__ = 'Baseline'


class StopConditionMessages(BaseMessages):

    #################################
    def __init__(self, _stop_condition_object):
        """
        :param _stop_condition_object: Object this message is attached to. \n
        :type _stop_condition_object:  common.objects.programming.conditions.stop_condition.StopCondition |
                                        common.objects.programming.conditions.stop_condition.PressureStopCondition |
                                        common.objects.programming.conditions.stop_condition.MoistureStopCondition |
                                        common.objects.programming.conditions.stop_condition.SwitchStopCondition
        """
        self.stop_condition = _stop_condition_object
        self.status_code = ''
        # Init parent class to inherit respective attributes
        BaseMessages.__init__(self, _controller=_stop_condition_object.controller)

        # Required Data
        self.device = None
        self.controller_message = False

    #################################
    def update_values(self):
        """
        This method updates all of the values that this message will use. \n
        """
        # Get the device on the empty condition
        if self.stop_condition.identifiers[1][0] is opcodes.pressure_sensor_ps:
            for pressure_sensor in self.stop_condition.controller.pressure_sensors.values():
                if pressure_sensor.sn == self.stop_condition.device_serial:
                    self.device = pressure_sensor
        elif self.stop_condition.identifiers[1][0] is opcodes.event_switch:
            for event_switch in self.stop_condition.controller.event_switches.values():
                if event_switch.sn == self.stop_condition.device_serial:
                    self.device = event_switch
        elif self.stop_condition.identifiers[1][0] is opcodes.moisture_sensor:
            for moisture_sensor in self.stop_condition.controller.moisture_sensors.values():
                if moisture_sensor.sn == self.stop_condition.device_serial:
                    self.device = moisture_sensor
        elif self.stop_condition.identifiers[1][0] is opcodes.temperature_sensor:
            for temperature_sensor in self.stop_condition.controller.temperature_sensors.values():
                if temperature_sensor.sn == self.stop_condition.device_serial:
                    self.device = temperature_sensor

    #################################
    def build_message_header(self):
        if self.controller_message:
            message_header = 'Controller: {0}n'.format(
                self.stop_condition.controller.ds
            )
        else:
            message_header = 'Program: {0}n'.format(
                self.stop_condition.controller.programs[self.stop_condition.program_ad].ds
            )
        return message_header

    #################################
    def build_message_tx(self, _status_code):
        """
        :param _status_code:
        :type _status_code: str

        """
        self.update_values()
        tx_msg = self.build_message_header()

        if not self.controller_message:
            if _status_code is Message.stop_event_switch:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}".format(
                    "Program Stopped:n",
                    "Event biCoder has been triggeredn",
                    "Program = ",
                    str(self.stop_condition.program_ad) + "n",
                    "Event biCoder = ",
                    str(self.stop_condition.device_serial) + "n",
                    "Limit/Value = ",
                    str(self.convert_op_cl_to_open_closed(self.stop_condition.mode)) + " / ",
                    str(self.convert_op_cl_to_open_closed(self.device.bicoder.vc))
                )

            elif _status_code is Message.stop_moisture_sensor:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}".format(
                    "Program Stopped:n",
                    "Moisture limit has been reachedn",
                    "Program = ",
                    str(self.stop_condition.program_ad) + "n",
                    "Moisture biSensor = ",
                    str(self.stop_condition.device_serial) + "n",
                    "Limit/Value = ",
                    str(self.stop_condition.threshold) + " / ",
                    str(self.device.bicoder.vp)                           # moisture sensor moisture percent
                )

            elif _status_code is Message.stop_temp_sensor:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}".format(
                    "Program Stopped:n",
                    "Temperature limit has been reachedn",
                    "Program = ",
                    str(self.stop_condition.program_ad) + "n",
                    "Temperature biSensor = ",
                    str(self.stop_condition.device_serial) + "n",
                    "Limit/Value = ",
                    str(self.stop_condition.threshold) + " / ",
                    str(self.device.bicoder.vd)
                )

            elif _status_code is Message.stop_pressure_sensor:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}".format(
                    "Program Stopped:n",
                    "Pressure limit has been reachedn",
                    "Program = ",
                    str(self.stop_condition.program_ad) + "n",
                    "Pressure Sensor = ",
                    str(self.stop_condition.device_serial) + "n",
                    "Limit/Value = ",
                    str(self.stop_condition.threshold) + " / ",
                    str(self.device.bicoder.va)
                )

        else:
            if _status_code is Message.stop_event_switch:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}".format(
                    "Event biCoder Triggered:n",
                    "All watering has been stopped.n",
                    "Event biCoder = ",
                    str(self.stop_condition.device_serial) + "n",
                    "Limit/Value = ",
                    str(self.convert_op_cl_to_open_closed(self.stop_condition.mode)) + " / ",
                    str(self.convert_op_cl_to_open_closed(self.device.bicoder.vc))
                )

            elif _status_code is Message.stop_moisture_sensor:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}".format(
                    "Moisture Limit Reached:n",
                    "All watering has been stopped.n",
                    "Moisture biSensor = ",
                    str(self.stop_condition.device_serial) + "n",
                    "Limit/Value = ",
                    str(self.stop_condition.threshold) + " / ",
                    str(self.device.bicoder.vp)                     # moisture sensor moisture percent
                )

            elif _status_code is Message.stop_temp_sensor:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}".format(
                    "Temperature Limit Reached:n",
                    "All watering has been stopped.n",
                    "Temperature biSensor = ",
                    str(self.stop_condition.device_serial) + "n",
                    "Limit/Value = ",
                    str(self.stop_condition.threshold) + " / ",
                    str(self.device.bicoder.vd)
                )

            elif _status_code is Message.stop_pressure_sensor:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}".format(
                    "Pressure Limit Reached:n",
                    "All watering has been stopped.n",
                    "Pressure Sensor = ",
                    str(self.stop_condition.device_serial) + "n",
                    "Limit/Value = ",
                    str(self.stop_condition.threshold) + " / ",
                    str(self.device.bicoder.va)
                )

        return tx_msg

    #################################
    def build_alarm_id(self, _status_code):
        """
        This return the alarm string.

        :param _status_code:
        :type _status_code:
        :return:
        :rtype:
        """
        # If not a controller message, make a program ID
        if not self.controller_message:
            if _status_code == Message.stop_pressure_sensor:
                alarm_id = '{0}_{1}_{2}_{3}_{4}_{5}'.format(
                    str(300 + self.stop_condition.program_ad).zfill(3),     # {0} Program empty condition ID number
                    StopConditionCommands.Program,                          # {1} PG
                    self.stop_condition.program_ad,                         # {2} address of the program
                    opcodes.device_value,                                   # {3} DV
                    self.stop_condition.device_serial,                      # {4} Serial number of device on condition
                    opcodes.stop_pressure_sensor_id                         # {5} status code
                )
            else:
                alarm_id = '{0}_{1}_{2}_{3}_{4}_{5}'.format(
                    str(300 + self.stop_condition.program_ad).zfill(3),     # {0} Program empty condition ID number
                    StopConditionCommands.Program,                          # {1} PG
                    self.stop_condition.program_ad,                         # {2} address of the program
                    opcodes.device_value,                                   # {3} DV
                    self.stop_condition.device_serial,                      # {4} Serial number of device on condition
                    _status_code                                            # {5} status code
                )
        # If it is a controller message, make a controller ID
        else:
            if _status_code == Message.stop_pressure_sensor:
                alarm_id = '{0}_{1}_{2}_{3}'.format(
                    str(000).zfill(3),                  # {0} controller empty condition ID number
                    opcodes.device_value,               # {1} DV
                    self.stop_condition.device_serial,  # {2} serial number of the device
                    opcodes.stop_pressure_sensor_id     # {3} status code
                )
            else:
                alarm_id = '{0}_{1}_{2}_{3}'.format(
                    str(000).zfill(3),                  # {0} controller empty condition ID number
                    opcodes.device_value,               # {1} DV
                    self.stop_condition.device_serial,  # {2} serial number of the device
                    _status_code                        # {3} status code
                )

        return alarm_id

    # |---------------------------------------------------------------|
    #         Message: Stop Condition With a Pressure Sensor
    # |---------------------------------------------------------------|
    #################################
    def set_stop_condition_with_pressure_sensor_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1},{2}={3},{4}={5}".format(
            opcodes.device_value,
            self.stop_condition.device_serial,
            opcodes.variable_1,
            self.stop_condition.threshold,
            opcodes.variable_2,
            self.device.bicoder.va)

        self.send_message(
            _action=ActionCommands.SET,
            _category=StopConditionCommands.Program,
            _identifier=self.stop_condition.program_ad,
            _status_code=Message.stop_pressure_sensor,
            _data_required_string=data_required_string)

    #################################
    def get_stop_condition_with_pressure_sensor_message(self):
        # The get string requires extra data
        self.update_values()
        data_required_string = "{0}={1}".format(
            opcodes.device_value,
            self.device.bicoder.sn)

        self.get_message(
            _action=ActionCommands.GET,
            _category=StopConditionCommands.Program,
            _identifier=self.stop_condition.program_ad,
            _status_code=Message.stop_pressure_sensor,
            _data_required_string=data_required_string)

    #################################
    def clear_stop_condition_with_pressure_sensor_message(self):
        # The clear string requires extra data
        self.update_values()
        data_required_string = "{0}={1}".format(
            opcodes.device_value,
            self.device.bicoder.sn)

        self.get_message(
            _action=ActionCommands.DO,
            _category=StopConditionCommands.Program,
            _identifier=self.stop_condition.program_ad,
            _status_code=Message.stop_pressure_sensor,
            _data_required_string=data_required_string)

    def check_for_stop_condition_with_pressure_sensor_message_not_present(self):
        try:
            # The set string requires extra data
            self.update_values()
            data_required_string = "{0}={1}".format(
                opcodes.device_value,
                self.device.bicoder.sn)

            self.send_message(
                _action=ActionCommands.SET,
                _category=StopConditionCommands.Program,
                _identifier=self.stop_condition.program_ad,
                _status_code=Message.stop_pressure_sensor,
                _data_required_string=data_required_string)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_stop_condition_with_pressure_sensor_message(self):
        # This stop condition is on a program so we set this variable to false
        self.controller_message = False
        self.get_stop_condition_with_pressure_sensor_message()
        self.verify_message(_status_code=Message.stop_pressure_sensor)

    # |---------------------------------------------------------------|
    #         Message: Stop Condition With a Moisture Sensor
    # |---------------------------------------------------------------|
    #################################
    def set_stop_condition_with_moisture_sensor_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1},{2}={3},{4}={5}".format(
            opcodes.device_value,
            self.stop_condition.device_serial,
            opcodes.variable_1,
            self.stop_condition.threshold,
            opcodes.variable_2,
            self.device.bicoder.vp)

        self.send_message(
            _action=ActionCommands.SET,
            _category=StopConditionCommands.Program,
            _identifier=self.stop_condition.program_ad,
            _status_code=Message.stop_moisture_sensor,
            _data_required_string=data_required_string)

    #################################
    def get_stop_condition_with_moisture_sensor_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1}".format(
            opcodes.device_value,
            self.device.bicoder.sn)

        self.get_message(
            _action=ActionCommands.GET,
            _category=StopConditionCommands.Program,
            _identifier=self.stop_condition.program_ad,
            _status_code=Message.stop_moisture_sensor,
            _data_required_string=data_required_string)

    #################################
    def clear_stop_condition_with_moisture_sensor_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1}".format(
            opcodes.device_value,
            self.device.bicoder.sn)

        self.get_message(
            _action=ActionCommands.DO,
            _category=StopConditionCommands.Program,
            _identifier=self.stop_condition.program_ad,
            _status_code=Message.stop_moisture_sensor,
            _data_required_string=data_required_string)

    def check_for_stop_condition_with_moisture_sensor_message_not_present(self):
        try:
            # The set string requires extra data
            self.update_values()
            data_required_string = "{0}={1}".format(
                opcodes.device_value,
                self.device.bicoder.sn)

            self.send_message(
                _action=ActionCommands.SET,
                _category=StopConditionCommands.Program,
                _identifier=self.stop_condition.program_ad,
                _status_code=Message.stop_moisture_sensor,
                _data_required_string=data_required_string)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_stop_condition_with_moisture_sensor_message(self):
        # This stop condition is on a program so we set this variable to false
        self.controller_message = False
        self.get_stop_condition_with_moisture_sensor_message()
        self.verify_message(_status_code=Message.stop_moisture_sensor)

    # |---------------------------------------------------------------|
    #         Message: Stop Condition With a Temperature Sensor
    # |---------------------------------------------------------------|
    #################################
    def set_stop_condition_with_temperature_sensor_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1},{2}={3},{4}={5}".format(
            opcodes.device_value,
            self.stop_condition.device_serial,
            opcodes.variable_1,
            self.stop_condition.threshold,
            opcodes.variable_2,
            self.device.bicoder.vd)

        self.send_message(
            _action=ActionCommands.SET,
            _category=StopConditionCommands.Program,
            _identifier=self.stop_condition.program_ad,
            _status_code=Message.stop_temp_sensor,
            _data_required_string=data_required_string)

    #################################
    def get_stop_condition_with_temperature_sensor_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1}".format(
            opcodes.device_value,
            self.device.bicoder.sn)

        self.get_message(
            _action=ActionCommands.GET,
            _category=StopConditionCommands.Program,
            _identifier=self.stop_condition.program_ad,
            _status_code=Message.stop_temp_sensor,
            _data_required_string=data_required_string)

    #################################
    def clear_stop_condition_with_temperature_sensor_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1}".format(
            opcodes.device_value,
            self.device.bicoder.sn)

        self.get_message(
            _action=ActionCommands.DO,
            _category=StopConditionCommands.Program,
            _identifier=self.stop_condition.program_ad,
            _status_code=Message.stop_temp_sensor,
            _data_required_string=data_required_string)

    def check_for_stop_condition_with_temperature_sensor_message_not_present(self):
        try:
            # The set string requires extra data
            self.update_values()
            data_required_string = "{0}={1}".format(
                opcodes.device_value,
                self.device.bicoder.sn)

            self.send_message(
                _action=ActionCommands.SET,
                _category=StopConditionCommands.Program,
                _identifier=self.stop_condition.program_ad,
                _status_code=Message.stop_temp_sensor,
                _data_required_string=data_required_string)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_stop_condition_with_temperature_sensor_message(self):
        # This stop condition is on a program so we set this variable to false
        self.controller_message = False
        self.get_stop_condition_with_temperature_sensor_message()
        self.verify_message(_status_code=Message.stop_temp_sensor)

    # |---------------------------------------------------------------|
    #         Message: Stop Condition With a Event Switch
    # |---------------------------------------------------------------|
    #################################
    def set_stop_condition_with_event_switch_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1},{2}={3},{4}={5}".format(
            opcodes.device_value,
            self.stop_condition.device_serial,
            opcodes.variable_1,
            self.stop_condition.mode,
            opcodes.variable_2,
            self.device.bicoder.vc)

        self.send_message(
            _action=ActionCommands.SET,
            _category=StopConditionCommands.Program,
            _identifier=self.stop_condition.program_ad,
            _status_code=Message.stop_event_switch,
            _data_required_string=data_required_string)

    #################################
    def get_stop_condition_with_event_switch_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1}".format(
            opcodes.device_value,
            self.device.bicoder.sn)

        self.get_message(
            _action=ActionCommands.GET,
            _category=StopConditionCommands.Program,
            _identifier=self.stop_condition.program_ad,
            _status_code=Message.stop_event_switch,
            _data_required_string=data_required_string)

    #################################
    def clear_stop_condition_with_event_switch_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1}".format(
            opcodes.device_value,
            self.device.bicoder.sn)

        self.get_message(
            _action=ActionCommands.DO,
            _category=StopConditionCommands.Program,
            _identifier=self.stop_condition.program_ad,
            _status_code=Message.stop_event_switch,
            _data_required_string=data_required_string)

    def check_for_stop_condition_with_event_switch_message_not_present(self):
        try:
            # The set string requires extra data
            self.update_values()
            data_required_string = "{0}={1}".format(
                opcodes.device_value,
                self.device.bicoder.sn)

            self.send_message(
                _action=ActionCommands.SET,
                _category=StopConditionCommands.Program,
                _identifier=self.stop_condition.program_ad,
                _status_code=Message.stop_event_switch,
                _data_required_string=data_required_string)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was not found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_stop_condition_with_event_switch_message(self):
        # This stop condition is on a program so we set this variable to false
        self.controller_message = False
        self.get_stop_condition_with_event_switch_message()
        self.verify_message(_status_code=Message.stop_event_switch)

    # |-----------------------------------------------------------------------------|
    #         Message: Stop Condition With a Pressure Sensor on Controller
    # |-----------------------------------------------------------------------------|
    #################################
    def set_stop_condition_with_pressure_sensor_on_controller_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1},{2}={3},{4}={5}".format(
            opcodes.device_value,
            self.stop_condition.device_serial,
            opcodes.variable_1,
            self.stop_condition.threshold,
            opcodes.variable_2,
            self.device.bicoder.va)

        self.send_message(
            _action=ActionCommands.SET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.stop_pressure_sensor,
            _data_required_string=data_required_string)

    #################################
    def get_stop_condition_with_pressure_sensor_on_controller_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1}".format(
            opcodes.device_value,
            self.device.bicoder.sn)

        self.get_message(
            _action=ActionCommands.GET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.stop_pressure_sensor,
            _data_required_string=data_required_string)

    #################################
    def clear_stop_condition_with_pressure_sensor_on_controller_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1}".format(
            opcodes.device_value,
            self.device.bicoder.sn)

        self.get_message(
            _action=ActionCommands.DO,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.stop_pressure_sensor,
            _data_required_string=data_required_string)

    def check_for_stop_condition_with_pressure_sensor_on_controller_message_not_present(self):
        try:
            # The set string requires extra data
            self.update_values()
            data_required_string = "{0}={1}".format(
                opcodes.device_value,
                self.device.bicoder.sn)

            self.send_message(
                _action=ActionCommands.SET,
                _category=ControllerCommands.Controller,
                _identifier=None,
                _status_code=Message.stop_pressure_sensor,
                _data_required_string=data_required_string)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_stop_condition_with_pressure_sensor_on_controller_message(self):
        # This stop condition is on the controller so we set this variable to true
        self.controller_message = True
        self.get_stop_condition_with_pressure_sensor_on_controller_message()
        self.verify_message(_status_code=Message.stop_pressure_sensor)

    # |---------------------------------------------------------------------------|
    #         Message: Stop Condition With a Moisture Sensor on Controller
    # |---------------------------------------------------------------------------|
    #################################
    def set_stop_condition_with_moisture_sensor_on_controller_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1},{2}={3},{4}={5}".format(
            opcodes.device_value,
            self.stop_condition.device_serial,
            opcodes.variable_1,
            self.stop_condition.threshold,
            opcodes.variable_2,
            self.device.bicoder.vp)

        self.send_message(
            _action=ActionCommands.SET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.stop_moisture_sensor,
            _data_required_string=data_required_string)

    #################################
    def get_stop_condition_with_moisture_sensor_on_controller_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1}".format(
            opcodes.device_value,
            self.device.bicoder.sn)

        self.get_message(
            _action=ActionCommands.GET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.stop_moisture_sensor,
            _data_required_string=data_required_string)

    #################################
    def clear_stop_condition_with_moisture_sensor_on_controller_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1}".format(
            opcodes.device_value,
            self.device.bicoder.sn)

        self.get_message(
            _action=ActionCommands.DO,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.stop_moisture_sensor,
            _data_required_string=data_required_string)

    def check_for_stop_condition_with_moisture_sensor_on_controller_message_not_present(self):
        try:
            # The set string requires extra data
            self.update_values()
            data_required_string = "{0}={1}".format(
                opcodes.device_value,
                self.device.bicoder.sn)

            self.send_message(
                _action=ActionCommands.SET,
                _category=ControllerCommands.Controller,
                _identifier=None,
                _status_code=Message.stop_moisture_sensor,
                _data_required_string=data_required_string)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_stop_condition_with_moisture_sensor_on_controller_message(self):
        # This stop condition is on the controller so we set this variable to true
        self.controller_message = True
        self.get_stop_condition_with_moisture_sensor_on_controller_message()
        self.verify_message(_status_code=Message.stop_moisture_sensor)

    # |-----------------------------------------------------------------------------|
    #         Message: Stop Condition With a Temperature Sensor on Controller
    # |-----------------------------------------------------------------------------|
    #################################
    def set_stop_condition_with_temperature_sensor_on_controller_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1},{2}={3},{4}={5}".format(
            opcodes.device_value,
            self.stop_condition.device_serial,
            opcodes.variable_1,
            self.stop_condition.threshold,
            opcodes.variable_2,
            self.device.bicoder.vd)

        self.send_message(
            _action=ActionCommands.SET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.stop_temp_sensor,
            _data_required_string=data_required_string)

    #################################
    def get_stop_condition_with_temperature_sensor_on_controller_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1}".format(
            opcodes.device_value,
            self.device.bicoder.sn)

        self.get_message(
            _action=ActionCommands.GET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.stop_temp_sensor,
            _data_required_string=data_required_string)

    #################################
    def clear_stop_condition_with_temperature_sensor_on_controller_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1}".format(
            opcodes.device_value,
            self.device.bicoder.sn)

        self.get_message(
            _action=ActionCommands.DO,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.stop_temp_sensor,
            _data_required_string=data_required_string)

    def check_for_stop_condition_with_temperature_sensor_on_controller_message_not_present(self):
        try:
            # The set string requires extra data
            self.update_values()
            data_required_string = "{0}={1}".format(
                opcodes.device_value,
                self.device.bicoder.sn)

            self.send_message(
                _action=ActionCommands.SET,
                _category=ControllerCommands.Controller,
                _identifier=None,
                _status_code=Message.stop_temp_sensor,
                _data_required_string=data_required_string)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_stop_condition_with_temperature_sensor_on_controller_message(self):
        # This stop condition is on the controller so we set this variable to true
        self.controller_message = True
        self.get_stop_condition_with_temperature_sensor_on_controller_message()
        self.verify_message(_status_code=Message.stop_temp_sensor)

    # |------------------------------------------------------------------------|
    #         Message: Stop Condition With a Event Switch on Controller
    # |------------------------------------------------------------------------|
    #################################
    def set_stop_condition_with_event_switch_on_controller_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1},{2}={3},{4}={5}".format(
            opcodes.device_value,
            self.stop_condition.device_serial,
            opcodes.variable_1,
            self.stop_condition.mode,
            opcodes.variable_2,
            self.device.bicoder.vc)

        self.send_message(
            _action=ActionCommands.SET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.stop_event_switch,
            _data_required_string=data_required_string)

    #################################
    def get_stop_condition_with_event_switch_on_controller_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1}".format(
            opcodes.device_value,
            self.device.bicoder.sn)

        self.get_message(
            _action=ActionCommands.GET,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.stop_event_switch,
            _data_required_string=data_required_string)

    #################################
    def clear_stop_condition_with_event_switch_on_controller_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1}".format(
            opcodes.device_value,
            self.device.bicoder.sn)

        self.get_message(
            _action=ActionCommands.DO,
            _category=ControllerCommands.Controller,
            _identifier=None,
            _status_code=Message.stop_event_switch,
            _data_required_string=data_required_string)

    def check_for_stop_condition_with_event_switch_on_controller_message_not_present(self):
        try:
            # The set string requires extra data
            self.update_values()
            data_required_string = "{0}={1}".format(
                opcodes.device_value,
                self.device.bicoder.sn)

            self.send_message(
                _action=ActionCommands.SET,
                _category=ControllerCommands.Controller,
                _identifier=None,
                _status_code=Message.stop_event_switch,
                _data_required_string=data_required_string)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was not found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_stop_condition_with_event_switch_on_controller_message(self):
        # This stop condition is on the controller so we set this variable to true
        self.controller_message = True
        self.get_stop_condition_with_event_switch_on_controller_message()
        self.verify_message(_status_code=Message.stop_event_switch)
