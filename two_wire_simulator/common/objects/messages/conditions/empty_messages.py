from datetime import datetime, timedelta
from common.date_package.date_resource import date_mngr

from common.imports import opcodes
from common.objects.messages.base_messages import BaseMessages
from common.imports.types import ActionCommands, WaterSourceCommands
from common.imports.types import Message, MessageCategory, status_plus_priority_code_dict_3200, status_code_dict_1000, \
    program_1000_who_dict

__author__ = 'Baseline'


class EmptyMessages(BaseMessages):

    #################################
    def __init__(self, _empty_condition_object):
        """
        :param _empty_condition_object: Object this message is attached to. \n
        :type _empty_condition_object:  common.objects.programming.conditions.empty_condition.EmptyCondition |
                                        common.objects.programming.conditions.empty_condition.PressureEmptyCondition |
                                        common.objects.programming.conditions.empty_condition.MoistureEmptyCondition |
                                        common.objects.programming.conditions.empty_condition.SwitchEmptyCondition
        """
        self.empty_condition = _empty_condition_object
        self.status_code = ''
        # Init parent class to inherit respective attributes
        BaseMessages.__init__(self, _controller=_empty_condition_object.controller)

        # Required Data
        self.device = None

    #################################
    def update_values(self):
        """
        This method updates all of the values that this message will use. \n
        """
        # Get the device on the empty condition
        if self.empty_condition.device_type is opcodes.pressure_sensor_ps:
            for pressure_sensor in self.empty_condition.controller.pressure_sensors.values():
                if pressure_sensor.sn == self.empty_condition.device_serial_number:
                    self.device = pressure_sensor
        elif self.empty_condition.device_type is opcodes.event_switch:
            for event_switch in self.empty_condition.controller.event_switches.values():
                if event_switch.sn == self.empty_condition.device_serial_number:
                    self.device = event_switch
        elif self.empty_condition.device_type is opcodes.moisture_sensor:
            for moisture_sensor in self.empty_condition.controller.moisture_sensors.values():
                if moisture_sensor.sn == self.empty_condition.device_serial_number:
                    self.device = moisture_sensor

    #################################
    def build_message_header(self):
        message_header = 'WS {{{0}}}n'.format(
            self.empty_condition.controller.water_sources[self.empty_condition.watersource_ad].ds
        )
        return message_header

    #################################
    def build_message_tx(self, _status_code):
        """
        :param _status_code:
        :type _status_code: str

        """
        self.update_values()
        tx_msg = self.build_message_header()

        # Create the title and header for the mainline
        if _status_code is Message.empty_condition_with_pressure_sensor :
            tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}".format(
                "Water Source Empty: n",
                "The Water Source has been shutdownn",
                "by the pressure sensor.n",
                "Water Source = ",
                str(self.empty_condition.water_source_address) + "n",
                "Device = ",
                str(self.device.bicoder.sn) + "n",
                "Limit/Value = ",
                self.empty_condition.pl,
                " / ",
                self.device.bicoder.vr
            )
        if _status_code is Message.empty_condition_with_moisture_sensor :
            tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}".format(
                "Water Source Empty: n",
                "The Water Source has been shutdownn",
                "by the moisture sensor.n",
                "Water Source = ",
                str(self.empty_condition.water_source_address) + "n",
                "Device = ",
                str(self.device.bicoder.sn) + "n",
                "Limit/Value = ",
                self.empty_condition.ml,
                " / ",
                self.device.bicoder.vp
            )
        if _status_code is Message.empty_condition_with_event_switch:
            tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}".format(
                "Water Source Empty: n",
                "The Water Source has been shutdownn",
                "by the event switch.n",
                "Water Source = ",
                str(self.empty_condition.water_source_address) + "n",
                "Device = ",
                str(self.device.bicoder.sn) + "n",
                "Limit/Value = ",
                self.convert_op_cl_to_open_closed(self.empty_condition.lt),
                " / ",
                self.convert_op_cl_to_open_closed(self.device.bicoder.vc)
            )

        return tx_msg

    #################################
    def build_alarm_id(self, _status_code):
        """
        This return the alarm string.

        :param _status_code:
        :type _status_code:
        :return:
        :rtype:
        """
        # On status codes 'EA', 'EE', 'EM', the ID is built with an 'MT' status code. Otherwise use normal status code
        if _status_code in [Message.empty_condition_with_pressure_sensor, Message.empty_condition_with_event_switch, Message.empty_condition_with_moisture_sensor]:
            alarm_id = '{0}_{1}_{2}_{3}'.format(
                str(450 + self.empty_condition.watersource_ad).zfill(3),    # {0} Water Source empty condition ID number
                WaterSourceCommands.Water_source,                           # {1}
                self.empty_condition.water_source_address,                  # {2} address of the water source
                Message.empty_shutdown                                      # {3} empty shutdown
            )
        else:
            alarm_id = '{0}_{1}_{2}_{3}'.format(
                str(450 + self.empty_condition.watersource_ad).zfill(3),    # {0} Water Source empty condition ID number
                WaterSourceCommands.Water_source,                           # {1}
                self.empty_condition.water_source_address,                  # {2} address of the water source
                _status_code                                                # {3}
            )
        return alarm_id

# |-----------------------------------------------------------|
#         Message: Empty Condition With a Pressure Sensor
# |-----------------------------------------------------------|
    #################################
    def set_empty_condition_with_pressure_sensor_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1},{2}={3},{4}={5}".format(
            opcodes.device_value,
            self.device.bicoder.sn,
            opcodes.variable_1,
            self.empty_condition.pl,
            opcodes.variable_2,
            self.device.bicoder.vr)

        self.send_message(
            _action=ActionCommands.SET,
            _category=WaterSourceCommands.Water_source,
            _identifier=self.empty_condition.water_source_address,
            _status_code=Message.empty_condition_with_pressure_sensor,
            _data_required_string=data_required_string)

    #################################
    def get_empty_condition_with_pressure_sensor_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1}".format(
            opcodes.device_value,
            self.device.bicoder.sn)

        self.get_message(
            _action=ActionCommands.GET,
            _category=WaterSourceCommands.Water_source,
            _identifier=self.empty_condition.water_source_address,
            _status_code=Message.empty_condition_with_pressure_sensor,
            _data_required_string=data_required_string)

    #################################
    def clear_empty_condition_with_pressure_sensor_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1}".format(
            opcodes.device_value,
            self.device.bicoder.sn)

        self.get_message(
            _action=ActionCommands.DO,
            _category=WaterSourceCommands.Water_source,
            _identifier=self.empty_condition.water_source_address,
            _status_code=Message.empty_condition_with_pressure_sensor,
            _data_required_string=data_required_string)

    def check_for_empty_condition_with_pressure_sensor_message_not_present(self):
        try:
            # The set string requires extra data
            self.update_values()
            data_required_string = "{0}={1}".format(
                opcodes.device_value,
                self.device.bicoder.sn)

            self.send_message(
                _action=ActionCommands.DO,
                _category=WaterSourceCommands.Water_source,
                _identifier=self.empty_condition.water_source_address,
                _status_code=Message.empty_condition_with_pressure_sensor,
                _data_required_string=data_required_string)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_empty_condition_with_pressure_sensor_message(self):
        self.get_empty_condition_with_pressure_sensor_message()
        self.verify_message(_status_code=Message.empty_condition_with_pressure_sensor)

# |-----------------------------------------------------------|
#         Message: Empty Condition With a Moisture Sensor
# |-----------------------------------------------------------|
    #################################
    def set_empty_condition_with_moisture_sensor_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1},{2}={3},{4}={5}".format(
            opcodes.device_value,
            self.device.bicoder.sn,
            opcodes.variable_1,
            self.empty_condition.ml,
            opcodes.variable_2,
            self.device.bicoder.vp)

        self.send_message(
            _action=ActionCommands.SET,
            _category=WaterSourceCommands.Water_source,
            _identifier=self.empty_condition.water_source_address,
            _status_code=Message.empty_condition_with_moisture_sensor,
            _data_required_string=data_required_string)

    #################################
    def get_empty_condition_with_moisture_sensor_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1}".format(
            opcodes.device_value,
            self.device.bicoder.sn)

        self.get_message(
            _action=ActionCommands.GET,
            _category=WaterSourceCommands.Water_source,
            _identifier=self.empty_condition.water_source_address,
            _status_code=Message.empty_condition_with_moisture_sensor,
            _data_required_string=data_required_string)

    #################################
    def clear_empty_condition_with_moisture_sensor_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1}".format(
            opcodes.device_value,
            self.device.bicoder.sn)

        self.get_message(
            _action=ActionCommands.DO,
            _category=WaterSourceCommands.Water_source,
            _identifier=self.empty_condition.water_source_address,
            _status_code=Message.empty_condition_with_moisture_sensor,
            _data_required_string=data_required_string)

    def check_for_empty_condition_with_moisture_sensor_message_not_present(self):
        try:
            # The set string requires extra data
            self.update_values()
            data_required_string = "{0}={1}".format(
                opcodes.device_value,
                self.device.bicoder.sn)

            self.send_message(
                _action=ActionCommands.DO,
                _category=WaterSourceCommands.Water_source,
                _identifier=self.empty_condition.water_source_address,
                _status_code=Message.empty_condition_with_moisture_sensor,
                _data_required_string=data_required_string)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_empty_condition_with_moisture_sensor_message(self):
        self.get_empty_condition_with_moisture_sensor_message()
        self.verify_message(_status_code=Message.empty_condition_with_moisture_sensor)

# |-----------------------------------------------------------|
#         Message: Empty Condition With a Event Switch
# |-----------------------------------------------------------|
    #################################
    def set_empty_condition_with_event_switch_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1},{2}={3},{4}={5}".format(
            opcodes.device_value,
            self.device.bicoder.sn,
            opcodes.variable_1,
            self.empty_condition.lt,
            opcodes.variable_2,
            self.device.bicoder.vc)

        self.send_message(
            _action=ActionCommands.SET,
            _category=WaterSourceCommands.Water_source,
            _identifier=self.empty_condition.water_source_address,
            _status_code=Message.empty_condition_with_event_switch,
            _data_required_string=data_required_string)

    #################################
    def get_empty_condition_with_event_switch_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1}".format(
            opcodes.device_value,
            self.device.bicoder.sn)

        self.get_message(
            _action=ActionCommands.GET,
            _category=WaterSourceCommands.Water_source,
            _identifier=self.empty_condition.water_source_address,
            _status_code=Message.empty_condition_with_event_switch,
            _data_required_string=data_required_string)

    #################################
    def clear_empty_condition_with_event_switch_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1}".format(
            opcodes.device_value,
            self.device.bicoder.sn)

        self.get_message(
            _action=ActionCommands.DO,
            _category=WaterSourceCommands.Water_source,
            _identifier=self.empty_condition.water_source_address,
            _status_code=Message.empty_condition_with_event_switch,
            _data_required_string=data_required_string)

    def check_for_empty_condition_with_event_switch_message_not_present(self):
        try:
            # The set string requires extra data
            self.update_values()
            data_required_string = "{0}={1}".format(
                opcodes.device_value,
                self.device.bicoder.sn )

            self.send_message(
                _action=ActionCommands.DO,
                _category=WaterSourceCommands.Water_source,
                _identifier=self.empty_condition.water_source_address,
                _status_code=Message.empty_condition_with_event_switch,
                _data_required_string=data_required_string)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_empty_condition_with_event_switch_message(self):
        self.get_empty_condition_with_event_switch_message()
        self.verify_message(_status_code=Message.empty_condition_with_event_switch)
