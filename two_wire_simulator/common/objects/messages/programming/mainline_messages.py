from common.imports import opcodes
from common.objects.messages.base_messages import BaseMessages
from common.imports.types import ActionCommands, MainlineCommands
from common.imports.types import Message

__author__ = 'Baseline'


class MainlineMessages(BaseMessages):

    #################################
    def __init__(self, _mainline_object):
        """
        :param _mainline_object:     Object this message is attached to. \n
        :type _mainline_object:      common.objects.programming.ml.Mainline
        """
        self.mainline = _mainline_object
        self.status_code = ''
        # Init parent class to inherit respective attributes
        BaseMessages.__init__(self, _controller=_mainline_object.controller)

        # Required Data
        self.poc = None
        self.flow_meter = None
        self.zone = None

        # Optional data
        self.variance_expected_value = None

    #################################
    def update_values(self):
        """
        This method updates all of the values that this message will use. \n
        """
        # A mainline is on a point of control, that point of control should have a flow meter, and we get it here
        self.poc = None
        self.flow_meter = None
        for points_of_control in self.mainline.controller.points_of_control.values():
            if points_of_control.ml == self.mainline.ad:
                self.poc = points_of_control
                self.flow_meter = self.mainline.controller.flow_meters[points_of_control.fm]

        # A mainline is attached to a zone, get that zone
        self.zone = None
        for zones in self.mainline.controller.zones.values():
            if zones.ml == self.mainline.ad:
                self.zone = zones

    #################################
    def build_message_header(self):
        message_header = '{0}: {1}n'.format(
            str('Mainline'),
            self.mainline.ad
        )
        return message_header

    #################################
    def build_message_tx(self, _status_code):
        """
        :param _status_code:
        :type _status_code: str

        """
        self.update_values()
        tx_msg = self.build_message_header()

        # Create the title and header for the mainline
        if _status_code is Message.configuration_error:
            tx_msg += "{0}{1}{2}{3}{4}{5}".format(
                "Mainline Configuration Error:n",
                "This mainline will not operate.n",
                "Mainline = ",
                str(self.mainline.ad) + "n",
                "Problem area: ",
                "Point of Control"
            )

        elif _status_code is Message.device_disabled:
            tx_msg += "{0}{1}{2}{3}{4}".format(
                "Mainline Disabled:n",
                "This mainline has dependent zonesn",
                "which will not water.n",
                "Mainline = ",
                str(self.mainline.ad) + "n"
            )

        elif _status_code is Message.learn_flow_fail_flow_biCoders_disabled:
            tx_msg += "{0}{1}{2}{3}".format(
                "Flow biCoder Disabledn",
                "Unable to do a Learn Flow operation.n",
                "Flow biCoder = ",
                self.flow_meter.ad
            )
        elif _status_code is Message.learn_flow_fail_flow_biCoders_error:
            tx_msg += "{0}{1}{2}{3}{4}".format(
                "Flow biCoder Error:n",
                "Unable to read Flow biCoder.n",
                "Learn Flow operation terminated.n",
                "Flow biCoder = ",
                self.flow_meter.ad
            )
        elif _status_code is Message.high_flow_shutdown:
            # For each point of control on feeding into the mainline, add the flow meters gpm value to a total
            gpm_from_all_attached_points_of_control = 0.0
            for points_of_control in self.mainline.controller.points_of_control.values():
                if points_of_control.ml == self.mainline.ad:
                    # Get the GPM reading of the point of control that is attached to the mainline, add to total
                    flow_meter_gpm_value = self.mainline.controller.flow_meters[points_of_control.fm].bicoder.vr
                    gpm_from_all_attached_points_of_control += flow_meter_gpm_value

            # For each zone attached to the mainline, add it's design flow to a total if it is watering
            design_flow_from_running_attached_zones = 0.0
            for zones in self.mainline.controller.zones.values():
                if zones.ml == self.mainline.ad:
                    if zones.statuses.status_is_watering(_get_status=True):
                        # Get the design flow of the zone attached to the mainline, add to total
                        design_flow_from_running_attached_zones += zones.df

                    if zones.statuses.status_is_error(_get_status=True):
                        # Get the design flow of the zone attached to the mainline, add to total
                        design_flow_from_running_attached_zones += zones.df

            # If the user specified a value they believe should be in the message, overwrite the value we calculate
            if self.variance_expected_value:
                design_flow_from_running_attached_zones = self.variance_expected_value

            tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}".format(
                "High Flow Variance Detected:n",
                "Measured flow is above limit.n",
                "Shutdown Mainline = ",
                str(self.mainline.ad) + "n",
                "Limit/Value = ",
                float(design_flow_from_running_attached_zones),
                " / ",
                float(gpm_from_all_attached_points_of_control)
            )
        elif _status_code is Message.high_flow_variance_detected:
            # TODO Mainlines managed by a FlowStation generate their message with different values
            # For each point of control on feeding into the mainline, add the flow meters gpm value to a total
            gpm_from_all_attached_points_of_control = 0.0
            for points_of_control in self.mainline.controller.points_of_control.values():
                if points_of_control.ml == self.mainline.ad:
                    # Get the GPM reading of the point of control that is attached to the mainline, add to total
                    flow_meter_gpm_value = self.mainline.controller.flow_meters[points_of_control.fm].bicoder.vr
                    gpm_from_all_attached_points_of_control += flow_meter_gpm_value

            # For each zone attached to the mainline, add it's design flow to a total if it is watering
            design_flow_from_running_attached_zones = 0.0
            for zones in self.mainline.controller.zones.values():
                if zones.ml == self.mainline.ad:
                    if zones.statuses.status_is_watering(_get_status=True):
                        # Get the design flow of the zone attached to the mainline, add to total
                        design_flow_from_running_attached_zones += zones.df

            tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}".format(
                "High Flow Variance Detected:n",
                "Measured flow is above limit.n",
                "Mainline = ",
                str(self.mainline.ad) + "n",
                "Limit/Value = ",
                str(design_flow_from_running_attached_zones),
                " / ",
                str(gpm_from_all_attached_points_of_control)
            )

        elif _status_code is Message.low_flow_variance_shutdown:
            # For each point of control on feeding into the mainline, add the flow meters gpm value to a total
            gpm_from_all_attached_points_of_control = 0.0
            for points_of_control in self.mainline.controller.points_of_control.values():
                if points_of_control.ml == self.mainline.ad:
                    # Get the GPM reading of the point of control that is attached to the mainline, add to total
                    flow_meter_gpm_value = self.mainline.controller.flow_meters[points_of_control.fm].bicoder.vr
                    gpm_from_all_attached_points_of_control += flow_meter_gpm_value

            # For each zone attached to the mainline, add it's design flow to a total if it is watering
            design_flow_from_running_attached_zones = 0.0
            for zones in self.mainline.controller.zones.values():
                if zones.ml == self.mainline.ad:
                    if zones.statuses.status_is_watering(_get_status=True):
                        # Get the design flow of the zone attached to the mainline, add to total
                        design_flow_from_running_attached_zones += zones.df

                    if zones.statuses.status_is_error(_get_status=True):
                        # Get the design flow of the zone attached to the mainline, add to total
                        design_flow_from_running_attached_zones += zones.df

            # If the user specified a value they believe should be in the message, overwrite the value we calculate
            if self.variance_expected_value:
                design_flow_from_running_attached_zones = self.variance_expected_value

            tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}".format(
                "Low Flow Variance Detected:n",
                "Measured flow is below limit.n",
                "Shutdown Mainline = ",
                str(self.mainline.ad) + "n",
                "Limit/Value = ",
                str(design_flow_from_running_attached_zones),
                " / ",
                str(gpm_from_all_attached_points_of_control)
            )
        elif _status_code is Message.low_flow_variance_detected:
            # For each point of control on feeding into the mainline, add the flow meters gpm value to a total
            gpm_from_all_attached_points_of_control = 0.0
            for points_of_control in self.mainline.controller.points_of_control.values():
                if points_of_control.ml == self.mainline.ad:
                    # Get the GPM reading of the point of control that is attached to the mainline, add to total
                    flow_meter_gpm_value = self.mainline.controller.flow_meters[points_of_control.fm].bicoder.vr
                    gpm_from_all_attached_points_of_control += flow_meter_gpm_value

            # For each zone attached to the mainline, add it's design flow to a total if it is watering
            design_flow_from_running_attached_zones = 0.0
            for zones in self.mainline.controller.zones.values():
                if zones.ml == self.mainline.ad:
                    if zones.statuses.status_is_watering(_get_status=True):
                        # Get the design flow of the zone attached to the mainline, add to total
                        design_flow_from_running_attached_zones += zones.df

            tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}".format(
                "Low Flow Variance Detected:n",
                "Measured flow is below limit.n",
                "Mainline = ",
                str(self.mainline.ad) + "n",
                "Limit/Value = ",
                str(design_flow_from_running_attached_zones),
                " / ",
                str(gpm_from_all_attached_points_of_control)
            )
        return tx_msg

    #################################
    def build_alarm_id(self, _status_code):
        """
        This returns the alarm string.

        :param _status_code:
        :type _status_code:
        :return:
        :rtype:
        """
        # On status codes 'HS', 'LS' the ID is built with a 'VS' status code. Otherwise use normal status code
        if _status_code in [Message.high_flow_variance_shutdown, Message.low_flow_variance_shutdown]:
            alarm_id = '{0}_{1}_{2}_{3}'.format(
                str(self.mainline.ad + 500),    # {0} the id number using the mainline address
                opcodes.mainline,               # {1}
                self.mainline.ad,               # {2} address to the mainline
                Message.variance_shutdown       # {3} variance shutdown
            )
        else:
            alarm_id = '{0}_{1}_{2}_{3}'.format(
                str(self.mainline.ad + 500),    # {0} the id number using the mainline address
                opcodes.mainline,               # {1}
                self.mainline.ad,               # {2} address to the mainline
                _status_code                    # {3}
            )
        return alarm_id

    # |-------------------------------------------------|
    #         Message: Configuration Error
    # |-------------------------------------------------|
    #################################
    def set_configuration_error_number_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1}".format(opcodes.variable_2, str("Point of Control"))
        self.send_message(
            _action=ActionCommands.SET,
            _category=MainlineCommands.Mainline,
            _identifier=self.mainline.ad,
            _status_code=Message.configuration_error,
            _data_required_string=data_required_string)

    #################################
    def get_configuration_error_number_message(self):
        self.update_values()
        self.get_message(
            _action=ActionCommands.GET,
            _category=MainlineCommands.Mainline,
            _identifier=self.mainline.ad,
            _status_code=Message.configuration_error)

    #################################
    def clear_configuration_error_number_message(self):
        self.update_values()
        self.send_message(
            _action=ActionCommands.DO,
            _category=MainlineCommands.Mainline,
            _identifier=self.mainline.ad,
            _status_code=Message.configuration_error)

    def check_for_configuration_error_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=MainlineCommands.Mainline,
                _identifier=self.mainline.ad,
                _status_code=Message.configuration_error)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was not found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_configuration_error_number_message(self):
        self.get_configuration_error_number_message()
        self.verify_message(_status_code=Message.configuration_error)

    # |-------------------------------------------------|
    #        Message: Disabled Messages
    # |-------------------------------------------------|
    #################################
    def set_disabled_message(self):
        self.update_values()
        self.send_message(
            _action=ActionCommands.SET,
            _category=MainlineCommands.Mainline,
            _identifier=self.mainline.ad,
            _status_code=Message.device_disabled)

    #################################
    def get_disabled_message(self):
        self.update_values()
        self.get_message(
            _action=ActionCommands.GET,
            _category=MainlineCommands.Mainline,
            _identifier=self.mainline.ad,
            _status_code=Message.device_disabled)

    #################################
    def clear_disabled_message(self):
        self.update_values()
        self.send_message(
            _action=ActionCommands.DO,
            _category=MainlineCommands.Mainline,
            _identifier=self.mainline.ad,
            _status_code=Message.device_disabled)

    def check_for_disabled_message_not_present(self):
        self.update_values()
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=MainlineCommands.Mainline,
                _identifier=self.mainline.ad,
                _status_code=Message.device_disabled)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was not found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_disabled_message(self):
        self.get_disabled_message()
        self.verify_message(_status_code=Message.device_disabled)

    # |------------------------------------------------------------------|
    #        Message: Learn Flow Fail Flow BiCoder Disabled Messages
    # |------------------------------------------------------------------|
    #################################
    def set_learn_flow_fail_flow_bicoders_disabled_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1},{2}={3}".format(
            opcodes.point_of_control,
            self.poc.ad,
            opcodes.device_value,
            self.flow_meter.sn)

        self.send_message(
            _action=ActionCommands.SET,
            _category=MainlineCommands.Mainline,
            _identifier=self.mainline.ad,
            _status_code=Message.learn_flow_fail_flow_biCoders_disabled,
            _data_required_string=data_required_string)

    #################################
    def get_learn_flow_fail_flow_bicoders_disabled_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1},{2}={3}".format(
            opcodes.point_of_control,
            self.poc.ad,
            opcodes.device_value,
            self.flow_meter.sn)

        self.get_message(
            _action=ActionCommands.GET,
            _category=MainlineCommands.Mainline,
            _identifier=self.mainline.ad,
            _status_code=Message.learn_flow_fail_flow_biCoders_disabled,
            _data_required_string=data_required_string)

    #################################
    def clear_learn_flow_fail_flow_bicoders_disabled_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1},{2}={3}".format(
            opcodes.point_of_control,
            self.poc.ad,
            opcodes.device_value,
            self.flow_meter.sn)

        self.send_message(
            _action=ActionCommands.DO,
            _category=MainlineCommands.Mainline,
            _identifier=self.mainline.ad,
            _status_code=Message.learn_flow_fail_flow_biCoders_disabled,
            _data_required_string=data_required_string)

    def check_for_learn_flow_fail_flow_bicoders_disabled_message_not_present(self):
        self.update_values()
        try:
            # The set string requires extra data
            self.update_values()
            data_required_string = "{0}={1},{2}={3}".format(
                opcodes.point_of_control,
                self.poc.ad,
                opcodes.device_value,
                self.flow_meter.sn)

            self.send_message(
                _action=ActionCommands.DO,
                _category=MainlineCommands.Mainline,
                _identifier=self.mainline.ad,
                _status_code=Message.learn_flow_fail_flow_biCoders_disabled,
                _data_required_string=data_required_string)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was not found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_learn_flow_fail_flow_bicoders_disabled_message(self):
        self.get_learn_flow_fail_flow_bicoders_disabled_message()
        self.verify_message(_status_code=Message.learn_flow_fail_flow_biCoders_disabled)

    # |---------------------------------------------------------------|
    #        Message: Learn Flow Fail Flow BiCoder Error Messages
    # |---------------------------------------------------------------|
    #################################
    def set_learn_flow_fail_flow_bicoders_error_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1},{2}={3}".format(
            opcodes.point_of_control,
            self.poc.ad,
            opcodes.device_value,
            self.flow_meter.sn)

        self.send_message(
            _action=ActionCommands.SET,
            _category=MainlineCommands.Mainline,
            _identifier=self.mainline.ad,
            _status_code=Message.learn_flow_fail_flow_biCoders_error,
            _data_required_string=data_required_string)

    #################################
    def get_learn_flow_fail_flow_bicoders_error_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1},{2}={3}".format(
            opcodes.point_of_control,
            self.poc.ad,
            opcodes.device_value,
            self.flow_meter.sn)

        self.get_message(
            _action=ActionCommands.GET,
            _category=MainlineCommands.Mainline,
            _identifier=self.mainline.ad,
            _status_code=Message.learn_flow_fail_flow_biCoders_error,
            _data_required_string=data_required_string)

    #################################
    def clear_learn_flow_fail_flow_bicoders_error_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1},{2}={3}".format(
            opcodes.point_of_control,
            self.poc.ad,
            opcodes.device_value,
            self.flow_meter.sn)

        self.send_message(
            _action=ActionCommands.DO,
            _category=MainlineCommands.Mainline,
            _identifier=self.mainline.ad,
            _status_code=Message.learn_flow_fail_flow_biCoders_error,
            _data_required_string=data_required_string)

    def check_for_learn_flow_fail_flow_bicoders_error_message_not_present(self):
        self.update_values()
        try:
            # The set string requires extra data
            self.update_values()
            data_required_string = "{0}={1},{2}={3}".format(
                opcodes.point_of_control,
                self.poc.ad,
                opcodes.device_value,
                self.flow_meter.sn)

            self.send_message(
                _action=ActionCommands.DO,
                _category=MainlineCommands.Mainline,
                _identifier=self.mainline.ad,
                _status_code=Message.learn_flow_fail_flow_biCoders_error,
                _data_required_string=data_required_string)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was not found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_learn_flow_fail_flow_bicoders_error_message(self):
        self.get_learn_flow_fail_flow_bicoders_error_message()
        self.verify_message(_status_code=Message.learn_flow_fail_flow_biCoders_error)

    # |-------------------------------------------------------|
    #        Message: High Flow Variance Shutdown Messages
    # |-------------------------------------------------------|
    #################################
    def set_high_flow_variance_shutdown_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1},{2}={3}".format(
            opcodes.variable_1,
            self.zone.df,
            opcodes.variable_2,
            self.flow_meter.bicoder.vr)

        self.send_message(
            _action=ActionCommands.SET,
            _category=MainlineCommands.Mainline,
            _identifier=self.mainline.ad,
            _status_code=Message.high_flow_shutdown,
            _data_required_string=data_required_string)

    #################################
    def get_high_flow_variance_shutdown_message(self):
        self.update_values()
        self.get_message(
            _action=ActionCommands.GET,
            _category=MainlineCommands.Mainline,
            _identifier=self.mainline.ad,
            _status_code=Message.high_flow_shutdown)

    #################################
    def clear_high_flow_variance_shutdown_message(self):
        self.update_values()
        self.send_message(
            _action=ActionCommands.DO,
            _category=MainlineCommands.Mainline,
            _identifier=self.mainline.ad,
            _status_code=Message.high_flow_shutdown)

    def check_for_high_flow_variance_shutdown_message_not_present(self):
        try:
            self.update_values()
            self.send_message(
                _action=ActionCommands.DO,
                _category=MainlineCommands.Mainline,
                _identifier=self.mainline.ad,
                _status_code=Message.high_flow_shutdown)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was not found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_high_flow_variance_shutdown_message(self, _variance_expected_value=None):
        self.variance_expected_value = _variance_expected_value
        self.get_high_flow_variance_shutdown_message()
        self.verify_message(_status_code=Message.high_flow_shutdown)

    # |-------------------------------------------------------|
    #        Message: High Flow Variance Detected Messages
    # |-------------------------------------------------------|
    #################################
    def set_high_flow_variance_detected_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1},{2}={3}".format(
            opcodes.variable_1,
            self.zone.df,
            opcodes.variable_2,
            self.flow_meter.bicoder.vr)

        self.send_message(
            _action=ActionCommands.SET,
            _category=MainlineCommands.Mainline,
            _identifier=self.mainline.ad,
            _status_code=Message.high_flow_variance_detected,
            _data_required_string=data_required_string)

    #################################
    def get_high_flow_variance_detected_message(self):
        self.update_values()
        self.get_message(
            _action=ActionCommands.GET,
            _category=MainlineCommands.Mainline,
            _identifier=self.mainline.ad,
            _status_code=Message.high_flow_variance_detected)

    #################################
    def clear_high_flow_variance_detected_message(self):
        self.update_values()
        self.send_message(
            _action=ActionCommands.DO,
            _category=MainlineCommands.Mainline,
            _identifier=self.mainline.ad,
            _status_code=Message.high_flow_variance_detected)

    def check_for_high_flow_variance_detected_message_present(self):
        self.update_values()
        try:
            self.get_message(
                _action=ActionCommands.GET,
                _category=MainlineCommands.Mainline,
                _identifier=self.mainline.ad,
                _status_code=Message.high_flow_variance_detected)
        except Exception:
            print("Message was not found on controller.")
            return False
        else:
            print("Was unable to verify if the message was gone.")
            return True

    #################################
    def check_for_high_flow_variance_detected_message_not_present(self):
        self.update_values()
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=MainlineCommands.Mainline,
                _identifier=self.mainline.ad,
                _status_code=Message.high_flow_variance_detected)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("High Flow Variance Detected Message was not found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_high_flow_variance_detected_message(self):
        self.update_values()
        self.get_high_flow_variance_detected_message()
        self.verify_message(_status_code=Message.high_flow_variance_detected)

    # |-------------------------------------------------------|
    #        Message: Low Flow Variance Shutdown Messages
    # |-------------------------------------------------------|
    #################################
    def set_low_flow_variance_shutdown_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1},{2}={3}".format(
            opcodes.variable_1,
            self.zone.df,
            opcodes.variable_2,
            self.flow_meter.bicoder.vr)

        self.send_message(
            _action=ActionCommands.SET,
            _category=MainlineCommands.Mainline,
            _identifier=self.mainline.ad,
            _status_code=Message.low_flow_variance_shutdown,
            _data_required_string=data_required_string)

    #################################
    def get_low_flow_variance_shutdown_message(self):
        self.update_values()
        self.get_message(
            _action=ActionCommands.GET,
            _category=MainlineCommands.Mainline,
            _identifier=self.mainline.ad,
            _status_code=Message.low_flow_variance_shutdown)

    #################################
    def clear_low_flow_variance_shutdown_message(self):
        self.update_values()
        self.send_message(
            _action=ActionCommands.DO,
            _category=MainlineCommands.Mainline,
            _identifier=self.mainline.ad,
            _status_code=Message.low_flow_variance_shutdown)

    def check_for_low_flow_variance_shutdown_message_not_present(self):
        self.update_values()
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=MainlineCommands.Mainline,
                _identifier=self.mainline.ad,
                _status_code=Message.low_flow_variance_shutdown)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was not found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_low_flow_variance_shutdown_message(self, _variance_expected_value=None):
        self.variance_expected_value = _variance_expected_value
        self.update_values()
        self.get_low_flow_variance_shutdown_message()
        self.verify_message(_status_code=Message.low_flow_variance_shutdown)

    # |-------------------------------------------------------|
    #        Message: Low Flow Variance Detected Messages
    # |-------------------------------------------------------|
    #################################
    def set_low_flow_variance_detected_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1},{2}={3}".format(
            opcodes.variable_1,
            self.zone.df,
            opcodes.variable_2,
            self.flow_meter.bicoder.vr)

        self.send_message(
            _action=ActionCommands.SET,
            _category=MainlineCommands.Mainline,
            _identifier=self.mainline.ad,
            _status_code=Message.low_flow_variance_detected,
            _data_required_string=data_required_string)

    #################################
    def get_low_flow_variance_detected_message(self):
        self.update_values()
        self.get_message(
            _action=ActionCommands.GET,
            _category=MainlineCommands.Mainline,
            _identifier=self.mainline.ad,
            _status_code=Message.low_flow_variance_detected)

    #################################
    def clear_low_flow_variance_detected_message(self):
        self.update_values()
        self.send_message(
            _action=ActionCommands.DO,
            _category=MainlineCommands.Mainline,
            _identifier=self.mainline.ad,
            _status_code=Message.low_flow_variance_detected)

    #################################
    def check_for_low_flow_variance_detected_message_present(self):
        self.update_values()
        try:
            self.get_message(
                _action=ActionCommands.GET,
                _category=MainlineCommands.Mainline,
                _identifier=self.mainline.ad,
                _status_code=Message.low_flow_variance_detected)
        except Exception:
            print("Message was not found on controller.")
            return False
        else:
            print("Was unable to verify if the message was gone.")
            return True

    #################################
    def check_for_low_flow_variance_detected_message_not_present(self):
        self.update_values()
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=MainlineCommands.Mainline,
                _identifier=self.mainline.ad,
                _status_code=Message.low_flow_variance_detected)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Low Flow Variance Detected Message was not found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_low_flow_variance_detected_message(self):
        self.get_low_flow_variance_detected_message()
        self.verify_message(_status_code=Message.low_flow_variance_detected)
