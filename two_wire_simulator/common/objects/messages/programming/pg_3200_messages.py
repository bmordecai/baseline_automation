
from common.imports import opcodes
from common.objects.messages.base_messages import BaseMessages
from common.imports.types import ActionCommands, ProgramCommands
from common.imports.types import Message

__author__ = 'Baseline'


class ProgramMessages(BaseMessages):

    #################################
    def __init__(self, _program_object):
        """
        :param _program_object:     Object this message is attached to. \n
        :type _program_object:      common.objects.programming.pg_3200.PG3200
        """
        self.program = _program_object
        self.status_code = ''
        # Init parent class to inherit respective attributes
        BaseMessages.__init__(self, _controller=_program_object.controller)

        # Required Data


    #################################
    def update_values(self):
        """
        This method updates all of the values that this message will use. \n
        """

    #################################
    def build_message_header(self):
        message_header = 'Program: {0}n'.format(
            self.program.ds
        )
        return message_header

    #################################
    def build_message_tx(self, _status_code):
        """
        :param _status_code:
        :type _status_code: str

        """
        tx_msg = self.build_message_header()

        if _status_code is Message.event_date_stop:
            tx_msg += "{0}{1}{2}{3}".format(
                "Program Stopped:n",
                "Event Day is active for today.n",
                "Program = ",
                str(self.program.ad)
            )

        elif _status_code is Message.learn_flow_with_errors:
            tx_msg += "{0}{1}{2}{3}".format(
                "Program Learn Flow Done:n",
                "Completed with Errors.n",
                "Program = ",
                str(self.program.ad)
            )

        elif _status_code is Message.learn_flow_success:
            tx_msg += "{0}{1}{2}{3}".format(
                "Program Learn Flow Done:n",
                "Completed successfully.n",
                "Program = ",
                str(self.program.ad)
            )

        elif _status_code is Message.over_run_start_event:
            tx_msg += "{0}{1}{2}{3}{4}".format(
                "Program Overrun:n",
                "Program was running when an",
                "start time was hit.n",
                "Program = ",
                str(self.program.ad) + "n"
            )

        elif _status_code is Message.priority_paused:
            tx_msg += "{0}{1}{2}{3}{4}".format(
                "Program Paused:n",
                "A higher priority program hasn",
                "preempted this program.n",
                "Program = ",
                str(self.program.ad)
            )

        elif _status_code is Message.restricted_time_by_water_ration:
            tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}".format(
                "Program Time Restricted:n",
                "Water Rationing is Activen",
                "Program water time has been reduced.n",
                "Program = ",
                str(self.program.ad) + "n",
                "Run Times = ",
                str(self.program.wr),
                " %"
            )

        elif _status_code is Message.water_window_paused:
            tx_msg += "{0}{1}{2}{3}".format(
                "Program Paused:n",
                "A water window is closedn",
                "Program = ",
                str(self.program.ad)
            )

        return tx_msg

    #################################
    def build_alarm_id(self, _status_code):
        """
        This return the alarm string.

        :param _status_code:
        :type _status_code:
        :return:
        :rtype:
        """
        # If the controller type is 3200, use the 3200 ID formats
        alarm_id = '{0}_{1}_{2}_{3}_{4}_{5}'.format(
            str(300 + self.program.ad).zfill(3),    # {0} Program ID number
            ProgramCommands.Program,                # {1}
            self.program.ad,                        # {2} this is the address fof the program
            opcodes.device_value,                   # {3} 'DV'
            self.program.ad,                        # {4}
            _status_code                            # {5}
        )
        return alarm_id

    # |-------------------------------------------------|
    #         Message: Program Stop Event Day
    # |-------------------------------------------------|
    #################################
    def set_stop_event_day_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.stop_event_day)

    #################################
    def get_stop_event_day_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.stop_event_day)

    #################################
    def clear_stop_event_day_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.stop_event_day)

    def check_for_stop_event_day_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=ProgramCommands.Program,
                _identifier=self.program.ad,
                _status_code=Message.stop_event_day)

        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_stop_event_day_message(self):
        self.get_stop_event_day_message()
        self.verify_message(_status_code=Message.stop_event_day)

    # |-----------------------------------------------------------|
    #         Message: Program Learn Flow Complete Errors
    # |-----------------------------------------------------------|
    #################################
    def set_learn_flow_complete_errors_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.learn_flow_complete_errors)

    #################################
    def get_learn_flow_complete_errors_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.learn_flow_complete_errors)

    #################################
    def clear_learn_flow_complete_errors_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.learn_flow_complete_errors)

    def check_for_learn_flow_complete_errors_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=ProgramCommands.Program,
                _identifier=self.program.ad,
                _status_code=Message.learn_flow_complete_errors)

        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_learn_flow_complete_errors_message(self):
        self.get_learn_flow_complete_errors_message()
        self.verify_message(_status_code=Message.learn_flow_complete_errors)

    # |-----------------------------------------------------------|
    #         Message: Program Learn Flow Complete Success
    # |-----------------------------------------------------------|
    #################################
    def set_learn_flow_complete_success_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.learn_flow_complete_success)

    #################################
    def get_learn_flow_complete_success_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.learn_flow_complete_success)

    #################################
    def clear_learn_flow_complete_success_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.learn_flow_complete_success)

    def check_for_learn_flow_complete_success_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=ProgramCommands.Program,
                _identifier=self.program.ad,
                _status_code=Message.learn_flow_complete_success)

        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_learn_flow_complete_success_message(self):
        self.get_learn_flow_complete_success_message()
        self.verify_message(_status_code=Message.learn_flow_complete_success)

    # |----------------------------------------------|
    #         Message: Program Overrun
    # |----------------------------------------------|
    #################################
    def set_program_overrun_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.program_overrun)

    #################################
    def get_program_overrun_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.program_overrun)

    #################################
    def clear_program_overrun_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.program_overrun)

    def check_for_program_overrun_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=ProgramCommands.Program,
                _identifier=self.program.ad,
                _status_code=Message.program_overrun)

        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_program_overrun_message(self):
        self.get_program_overrun_message()
        self.verify_message(_status_code=Message.program_overrun)

    # |------------------------------------------------------|
    #         Message: Program Priority Preempted
    # |------------------------------------------------------|
    #################################
    def set_priority_preempted_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.priority_paused)

    #################################
    def get_priority_preempted_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.priority_paused)

    #################################
    def clear_priority_preempted_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.priority_paused)

    def check_for_priority_preempted_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=ProgramCommands.Program,
                _identifier=self.program.ad,
                _status_code=Message.priority_paused)

        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_priority_preempted_message(self):
        self.get_priority_preempted_message()
        self.verify_message(_status_code=Message.priority_paused)

    # |------------------------------------------------------|
    #         Message: Program Water Rationed
    # |------------------------------------------------------|
    #################################
    def set_water_rationed_message(self):
        data_required_string = "{0}={1}".format(opcodes.variable_1, self.program.wr)
        self.send_message(
            _action=ActionCommands.SET,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.restricted_time_by_water_ration,
            _data_required_string=data_required_string)

    #################################
    def get_water_rationed_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.restricted_time_by_water_ration)

    #################################
    def clear_water_rationed_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.restricted_time_by_water_ration)

    def check_for_water_rationed_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=ProgramCommands.Program,
                _identifier=self.program.ad,
                _status_code=Message.restricted_time_by_water_ration)

        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_water_rationed_message(self):
        self.get_water_rationed_message()
        self.verify_message(_status_code=Message.restricted_time_by_water_ration)

    # |------------------------------------------------------|
    #         Message: Program Paused Water Window
    # |------------------------------------------------------|
    #################################
    def set_paused_water_window_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.water_window_paused)

    #################################
    def get_paused_water_window_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.water_window_paused)

    #################################
    def clear_paused_water_window_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.water_window_paused)

    def check_for_paused_water_window_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=ProgramCommands.Program,
                _identifier=self.program.ad,
                _status_code=Message.water_window_paused)

        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_paused_water_window_message(self):
        self.get_paused_water_window_message()
        self.verify_message(_status_code=Message.water_window_paused)
