from datetime import datetime, timedelta
from common.date_package.date_resource import date_mngr

from common.imports import opcodes
from common.objects.messages.base_messages import BaseMessages
from common.imports.types import ActionCommands, WaterSourceCommands
from common.imports.types import Message, MessageCategory, status_plus_priority_code_dict_3200, status_code_dict_1000, \
    program_1000_who_dict

__author__ = 'Baseline'


class WaterSourceMessages(BaseMessages):

    #################################
    def __init__(self, _water_source_object):
        """
        :param _water_source_object:     Object this message is attached to. \n
        :type _water_source_object:      common.objects.programming.ws.WaterSource
        """
        self.water_source = _water_source_object
        self.status_code = ''
        # Init parent class to inherit respective attributes
        BaseMessages.__init__(self, _controller=_water_source_object.controller)

        self.poc = None
        self.flow_meter = None
        
        # NEW: Store usage data
        self.total_usage = 0

    #################################
    def update_values(self):
        """
        This method updates all of the values that this message will use. \n
        """
        # A water source has a point of control, that point of control should have a flow meter, and we get it here
        self.poc = None
        self.flow_meter = None
        self.total_usage = 0

        if self.water_source.is_managed_by_flowstation():
            # If WS is managed by the FS, its POC might reside on a different controller. So use the FlowStation to
            # get the assigned POC for the WS and get the POC's controller through the object which then has access
            # to the correct flow meter.
            for poc_obj in self.water_source.flowstation.points_of_control.values():
                if poc_obj.ad in self.water_source.flow_station_points_of_control:
                    flow_meter = poc_obj.controller.flow_meters[poc_obj.fm]
                    self.total_usage += float(flow_meter.bicoder.vg)
        else:
            for points_of_control in self.water_source.controller.points_of_control.values():
                if points_of_control.ad == self.water_source.pc:
                    self.poc = points_of_control
                    self.flow_meter = self.water_source.controller.flow_meters[points_of_control.fm]

    #################################
    def build_message_header(self):
        message_header = 'WS {{{0}}}n'.format(
            self.water_source.ds
        )
        return message_header

    #################################
    def build_message_tx(self, _status_code):
        """
        :param _status_code:
        :type _status_code: str

        """
        tx_msg = self.build_message_header()

        # If the water source is shared, we may need to get values from a different POC
        if not self.water_source.is_managed_by_flowstation():
            used_value_for_budget = str(float(self.flow_meter.bicoder.vg))
        else:
            used_value_for_budget = self.total_usage

        # Create the title and header for the water source
        if _status_code is Message.budget_exceeded:
            tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}".format(
                "Monthly Budget Exceeded:n",
                "Watering will continue.n",
                "WS = ",
                str(self.water_source.ad) + "n",
                "Budget/Used = ",
                str(float(self.water_source.wb)),
                " / ",
                used_value_for_budget
            )

        elif _status_code is Message.budget_exceeded_shutdown:
            tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}".format(
                "Monthly Budget Exceeded:n",
                "Watering has been shut down.n",
                "WS = ",
                str(self.water_source.ad) + "n",
                "Budget/Used = ",
                str(float(self.water_source.wb)),
                " / ",
                used_value_for_budget
            )

        return tx_msg

    #################################
    def build_alarm_id(self, _status_code):
        """
        This return the alarm string.

        :param _status_code:
        :type _status_code:
        :return:
        :rtype:
        """

        # If the controller type is 3200, use the 3200 ID formats
        alarm_id = '{0}_{1}_{2}_{3}'.format(
            str(450 + self.water_source.ad).zfill(3),   # {0} Water Source ID number
            WaterSourceCommands.Water_source,           # {1}
            self.water_source.ad,                       # {2} this is the address fof the water source
            _status_code                                # {3}
        )
        return alarm_id

# |----------------------------------------------------------|
#         Message: Water Source Exceeds Monthly Budget
# |-----------------------------------------------------------|
    #################################
    def set_exceed_monthly_budget_message(self, ):
        # The set string requires extra data
        self.update_values()

        # If the water source is shared, we may need to get values from a different POC
        if not self.water_source.is_managed_by_flowstation():
            used_value_for_budget = str(float(self.flow_meter.bicoder.vg))
        else:
            used_value_for_budget = self.total_usage

        data_required_string = "{0}={1},{2}={3}".format(
            opcodes.variable_1,
            str(float(self.water_source.wb)),
            opcodes.variable_2,
            used_value_for_budget)

        self.send_message(
            _action=ActionCommands.SET,
            _category=WaterSourceCommands.Water_source,
            _identifier=self.water_source.ad,
            _status_code=Message.budget_exceeded,
            _data_required_string=data_required_string)

    #################################
    def get_exceed_monthly_budget_message(self, ):
        self.get_message(
            _action=ActionCommands.GET,
            _category=WaterSourceCommands.Water_source,
            _identifier=self.water_source.ad,
            _status_code=Message.budget_exceeded)

    #################################
    def clear_exceed_monthly_budget_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=WaterSourceCommands.Water_source,
            _identifier=self.water_source.ad,
            _status_code=Message.budget_exceeded)

    #################################
    def check_for_exceed_monthly_budget_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=WaterSourceCommands.Water_source,
                _identifier=self.water_source.ad,
                _status_code=Message.budget_exceeded)

        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_exceed_monthly_budget_message(self):
        self.update_values()
        self.get_exceed_monthly_budget_message()
        self.verify_message(_status_code=Message.budget_exceeded)

# |-------------------------------------------------------------------------|
#        Message: Water Source Exceeds Monthly Budget and was Shut Down
# |-------------------------------------------------------------------------|
    #################################
    def set_exceed_monthly_budget_with_shutdown_message(self, ):
        # The set string requires extra data
        self.update_values()

        # If the water source is shared, we may need to get values from a different POC
        if not self.water_source.is_managed_by_flowstation():
            used_value_for_budget = str(float(self.flow_meter.bicoder.vg))
        else:
            used_value_for_budget = self.total_usage

        data_required_string = "{0}={1},{2}={3}".format(
            opcodes.variable_1,
            str(float(self.water_source.wb)),
            opcodes.variable_2,
            used_value_for_budget)

        self.send_message(
            _action=ActionCommands.SET,
            _category=WaterSourceCommands.Water_source,
            _identifier=self.water_source.ad,
            _status_code=Message.budget_exceeded_shutdown,
            _data_required_string=data_required_string)

    #################################
    def get_exceed_monthly_budget_with_shutdown_message(self, ):
        self.get_message(
            _action=ActionCommands.GET,
            _category=WaterSourceCommands.Water_source,
            _identifier=self.water_source.ad,
            _status_code=Message.budget_exceeded_shutdown)

    #################################
    def clear_exceed_monthly_budget_with_shutdown_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=WaterSourceCommands.Water_source,
            _identifier=self.water_source.ad,
            _status_code=Message.budget_exceeded_shutdown)

    #################################
    def check_for_exceed_monthly_budget_with_shutdown_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=WaterSourceCommands.Water_source,
                _identifier=self.water_source.ad,
                _status_code=Message.budget_exceeded_shutdown)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_exceed_monthly_budget_with_shutdown_message(self):
        self.update_values()
        self.get_exceed_monthly_budget_with_shutdown_message()
        self.verify_message(_status_code=Message.budget_exceeded_shutdown)

