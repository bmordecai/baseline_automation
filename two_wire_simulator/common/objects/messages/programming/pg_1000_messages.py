
from common.imports import opcodes
from common.objects.messages.base_messages import BaseMessages
from common.imports.types import ActionCommands, ProgramCommands, program_1000_who_dict
from common.imports.types import Message
from common.date_package.date_resource import date_mngr

__author__ = 'Baseline'


class ProgramMessages(BaseMessages):

    #################################
    def __init__(self, _program_object):
        """
        :param _program_object:     Object this message is attached to. \n
        :type _program_object:      common.objects.programming.pg_1000.PG1000
        """
        self.program = _program_object
        self.status_code = ''
        self.who_caused_condition = None
        # Init parent class to inherit respective attributes
        BaseMessages.__init__(self, _controller=_program_object.controller)

    #################################
    def build_message_header(self):
        message_header = 'Program {0}n'.format(
            self.program.ad
        )
        return message_header

    #################################
    def build_message_tx(self, _status_code):
        """
        :param _status_code:
        :type _status_code: str

        """
        tx_msg = self.build_message_header()

        # Checks to see if the date has a leading zero, if it does, cut it out. Also cut out 'M' from 'PM' or 'AM'
        if date_mngr.controller_datetime.time_obj.obj.strftime('%I:%M%p')[0] == '0':
            date = "{0}, {1}".format(

                date_mngr.controller_datetime.formatted_date_string('%b %d'),            # Date in a specified format
                date_mngr.controller_datetime.formatted_time_string('%I:%M%p')[1:-1]     # Time in specified format
            )
        else:
            date = "{0}, {1}".format(
                date_mngr.controller_datetime.formatted_date_string('%b %d'),        # Date in a specified format
                date_mngr.controller_datetime.formatted_time_string('%I:%M%p')[:-1]  # Time cuts out the 'M' from 'PM'
            )

        # This checks to see if the date has a leading zero after the month. ex: Jun 09 becomes June 9
        if date[4] == '0':
            date = date[0:4] + date[5:]

        if _status_code is Message.over_run_start_event:
            tx_msg += "Status: Program Overrun {0}".format(
                date
            )
        elif _status_code is Message.learn_flow_with_errors:
            tx_msg += "Status: Learn Flow Failed {0}".format(
                date
            )
        elif _status_code is Message.calibrate_failure_no_change:
            tx_msg += "Status: Calibration Failed {0}".format(
                date
            )
        elif _status_code is Message.start:
            tx_msg += "Status: Not Tested Yet {0}nStart: {1} {2}".format(
                date,
                program_1000_who_dict[_status_code][self.who_caused_condition],
                date
            )
        elif _status_code is Message.pause:
            tx_msg += "Status: Not Tested Yet {0}nPause: {1} {2}".format(
                date,
                program_1000_who_dict[_status_code][self.who_caused_condition],
                date
            )
        elif _status_code is Message.stop:
            tx_msg += "Status: Not Tested Yet {0}nStop: {1} {2}".format(
                date,
                program_1000_who_dict[_status_code][self.who_caused_condition],
                date
            )

        return tx_msg

    #################################
    def build_alarm_id(self, _status_code):
        """
        This return the alarm string.

        :param _status_code:
        :type _status_code:
        :return:
        :rtype:
        """
        if _status_code in [opcodes.start, opcodes.stop, opcodes.pause]:
            _status_code = opcodes.okay
        # If the controller type is 3200, use the 3200 ID formats
        alarm_id = '{0}_{1}_{2}_{3}'.format(
            str(299 + self.program.ad).zfill(3),    # {0} Program ID number
            ProgramCommands.Program,                # {1}
            self.program.ad,                        # {2} this is the address fof the program
            _status_code                            # {4}
        )
        return alarm_id

    # |-------------------------------------------------|
    #         Message: Program Stop Event Day
    # |-------------------------------------------------|
    #################################
    def set_overrun_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.program_overrun)

    #################################
    def get_overrun_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.program_overrun)

    #################################
    def clear_overrun_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.program_overrun)

    def check_for_overrun_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=ProgramCommands.Program,
                _identifier=self.program.ad,
                _status_code=Message.program_overrun)

        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_overrun_message(self):
        self.get_overrun_message()
        self.verify_message(_status_code=Message.program_overrun)

    # |-----------------------------------------------------------|
    #         Message: Program Learn Flow Complete Errors
    # |-----------------------------------------------------------|
    #################################
    def set_learn_flow_complete_errors_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.learn_flow_complete_errors)

    #################################
    def get_learn_flow_complete_errors_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.learn_flow_complete_errors)

    #################################
    def clear_learn_flow_complete_errors_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.learn_flow_complete_errors)

    def check_for_learn_flow_complete_errors_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=ProgramCommands.Program,
                _identifier=self.program.ad,
                _status_code=Message.learn_flow_complete_errors)

        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_learn_flow_complete_errors_message(self):
        self.get_learn_flow_complete_errors_message()
        self.verify_message(_status_code=Message.learn_flow_complete_errors)

    # |-----------------------------------------------------------|
    #         Message: Program Calibration Failure
    # |-----------------------------------------------------------|
    #################################
    def set_calibration_failure_message(self):
        self.send_message(
            _action=ActionCommands.SET,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.calibrate_failure_no_change)

    #################################
    def get_calibration_failure_message(self):
        self.get_message(
            _action=ActionCommands.GET,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.calibrate_failure_no_change)

    #################################
    def clear_calibration_failure_message(self):
        self.send_message(
            _action=ActionCommands.DO,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.calibrate_failure_no_change)

    def check_for_calibration_failure_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=ProgramCommands.Program,
                _identifier=self.program.ad,
                _status_code=Message.calibrate_failure_no_change)

        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_calibration_failure_message(self):
        self.get_calibration_failure_message()
        self.verify_message(_status_code=Message.calibrate_failure_no_change)

    # |-----------------------------------------------------------|
    #         Message: Program Start
    # |-----------------------------------------------------------|
    #################################
    def set_start_message(self, _who_started_the_program):
        """
        Create a start message on a 1000 program
        :param _who_started_the_program:     The thing that started this program. \n
            EX:
                BM - BaseManager
                US - User
                OP - Operator
                PR - Programmer
                AD - Admin
                MS - Moisture
                SW - Event Switch
                TS - Temperature
                DT - Date and Time
        :type _who_started_the_program:      str
        """
        self.who_caused_condition = _who_started_the_program
        data_required_string = "{0}={1}".format(
            opcodes.who,
            _who_started_the_program
        )
        self.send_message(
            _action=ActionCommands.SET,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.start,
            _data_required_string=data_required_string
        )

    #################################
    def get_start_message(self, _who_started_the_program):
        """
        Create a start message on a 1000 program
        :param _who_started_the_program:     The thing that started this program. \n
            EX:
                BM - BaseManager
                US - User
                OP - Operator
                PR - Programmer
                AD - Admin
                MS - Moisture
                SW - Event Switch
                TS - Temperature
                DT - Date and Time
        :type _who_started_the_program:      str
        """
        self.who_caused_condition = _who_started_the_program
        data_required_string = "{0}={1}".format(
            opcodes.who,
            _who_started_the_program
        )
        self.get_message(
            _action=ActionCommands.GET,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.start,
            _data_required_string=data_required_string
        )

    #################################
    def clear_start_message(self, _who_started_the_program):
        """
        Create a start message on a 1000 program
        :param _who_started_the_program:     The thing that started this program. \n
            EX:
                BM - BaseManager
                US - User
                OP - Operator
                PR - Programmer
                AD - Admin
                MS - Moisture
                SW - Event Switch
                TS - Temperature
                DT - Date and Time
        :type _who_started_the_program:      str
        """
        self.who_caused_condition = _who_started_the_program
        data_required_string = "{0}={1}".format(
            opcodes.who,
            _who_started_the_program
        )
        self.send_message(
            _action=ActionCommands.DO,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.start,
            _data_required_string=data_required_string
        )

    def check_for_start_message_not_present(self, _who_started_the_program):
        """
        Create a start message on a 1000 program
        :param _who_started_the_program:     The thing that started this program. \n
            EX:
                BM - BaseManager
                US - User
                OP - Operator
                PR - Programmer
                AD - Admin
                MS - Moisture
                SW - Event Switch
                TS - Temperature
                DT - Date and Time
        :type _who_started_the_program:      str
        """
        self.who_caused_condition = _who_started_the_program
        data_required_string = "{0}={1}".format(
            opcodes.who,
            _who_started_the_program
        )
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=ProgramCommands.Program,
                _identifier=self.program.ad,
                _status_code=Message.start,
                _data_required_string=data_required_string
            )

        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_start_message(self, _who_started_the_program):
        """
        Create a start message on a 1000 program
        :param _who_started_the_program:     The thing that started this program. \n
            EX:
                BM - BaseManager
                US - User
                OP - Operator
                PR - Programmer
                AD - Admin
                MS - Moisture
                SW - Event Switch
                TS - Temperature
                DT - Date and Time
        :type _who_started_the_program:      str
        """
        self.who_caused_condition = _who_started_the_program
        self.get_start_message(_who_started_the_program=_who_started_the_program)
        self.verify_message(_status_code=Message.start)

    # |-----------------------------------------------------------|
    #         Message: Program Pause
    # |-----------------------------------------------------------|
    #################################
    def set_pause_message(self, _who_paused_the_program):
        """
        Create a pause message on a 1000 program
        :param _who_paused_the_program:     The thing that started this program. \n
            EX:
                SY - System
                MS - Moisture
                SW - Event Switch
                TS - Temperature
                WW - Water Window
                ED - Event Day
        :type _who_paused_the_program:      str
        """
        self.who_caused_condition = _who_paused_the_program
        data_required_string = "{0}={1}".format(
            opcodes.who,
            _who_paused_the_program
        )
        self.send_message(
            _action=ActionCommands.SET,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.pause,
            _data_required_string=data_required_string
        )

    #################################
    def get_pause_message(self, _who_paused_the_program):
        """
        Create a pause message on a 1000 program
        :param _who_paused_the_program:     The thing that paused this program. \n
            EX:
                SY - System
                MS - Moisture
                SW - Event Switch
                TS - Temperature
                WW - Water Window
                ED - Event Day
        :type _who_paused_the_program:      str
        """
        self.who_caused_condition = _who_paused_the_program
        data_required_string = "{0}={1}".format(
            opcodes.who,
            _who_paused_the_program
        )
        self.get_message(
            _action=ActionCommands.GET,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.pause,
            _data_required_string=data_required_string
        )

    #################################
    def clear_pause_message(self, _who_paused_the_program):
        """
        Create a pause message on a 1000 program
        :param _who_paused_the_program:     The thing that paused this program. \n
            EX:
                SY - System
                MS - Moisture
                SW - Event Switch
                TS - Temperature
                WW - Water Window
                ED - Event Day
        :type _who_paused_the_program:      str
        """
        self.who_caused_condition = _who_paused_the_program
        data_required_string = "{0}={1}".format(
            opcodes.who,
            _who_paused_the_program
        )
        self.send_message(
            _action=ActionCommands.DO,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.pause,
            _data_required_string=data_required_string
        )

    def check_for_pause_message_not_present(self, _who_paused_the_program):
        """
        Create a pause message on a 1000 program
        :param _who_paused_the_program:     The thing that paused this program. \n
            EX:
                SY - System
                MS - Moisture
                SW - Event Switch
                TS - Temperature
                WW - Water Window
                ED - Event Day
        :type _who_paused_the_program:      str
        """
        self.who_caused_condition = _who_paused_the_program
        data_required_string = "{0}={1}".format(
            opcodes.who,
            _who_paused_the_program
        )
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=ProgramCommands.Program,
                _identifier=self.program.ad,
                _status_code=Message.pause,
                _data_required_string=data_required_string
            )

        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_pause_message(self, _who_paused_the_program):
        """
        Create a pause message on a 1000 program
        :param _who_paused_the_program:     The thing that paused this program. \n
            EX:
                SY - System
                MS - Moisture
                SW - Event Switch
                TS - Temperature
                WW - Water Window
                ED - Event Day
        :type _who_paused_the_program:      str
        """
        self.who_caused_condition = _who_paused_the_program
        self.get_pause_message(_who_paused_the_program=_who_paused_the_program)
        self.verify_message(_status_code=Message.pause)

    # |-----------------------------------------------------------|
    #         Message: Program Stop
    # |-----------------------------------------------------------|
    #################################
    def set_stop_message(self, _who_stopped_the_program):
        """
        Create a stop message on a 1000 program
        :param _who_stopped_the_program:     The thing that started this program. \n
            EX:
                BM - BaseManager
                US - User
                OP - Operator
                PR - Programmer
                AD - Admin
                MS - Moisture
                SW - Event Switch
                TS - Temperature
                DT - Date and Time
                DL - System Off
                RJ - Rain Switch
                RA - Rain Delay
        :type _who_stopped_the_program:      str
        """
        self.who_caused_condition = _who_stopped_the_program
        data_required_string = "{0}={1}".format(
            opcodes.who,
            _who_stopped_the_program
        )
        self.send_message(
            _action=ActionCommands.SET,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.stop,
            _data_required_string=data_required_string
        )

    #################################
    def get_stop_message(self, _who_stopped_the_program):
        """
        Create a stop message on a 1000 program
        :param _who_stopped_the_program:     The thing that stopd this program. \n
            EX:
                BM - BaseManager
                US - User
                OP - Operator
                PR - Programmer
                AD - Admin
                MS - Moisture
                SW - Event Switch
                TS - Temperature
                DT - Date and Time
                DL - System Off
                RJ - Rain Switch
                RA - Rain Delay
        :type _who_stopped_the_program:      str
        """
        self.who_caused_condition = _who_stopped_the_program
        data_required_string = "{0}={1}".format(
            opcodes.who,
            _who_stopped_the_program
        )
        self.get_message(
            _action=ActionCommands.GET,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.stop,
            _data_required_string=data_required_string
        )

    #################################
    def clear_stop_message(self, _who_stopped_the_program):
        """
        Create a stop message on a 1000 program
        :param _who_stopped_the_program:     The thing that stopd this program. \n
            EX:
                BM - BaseManager
                US - User
                OP - Operator
                PR - Programmer
                AD - Admin
                MS - Moisture
                SW - Event Switch
                TS - Temperature
                DT - Date and Time
                DL - System Off
                RJ - Rain Switch
                RA - Rain Delay
        :type _who_stopped_the_program:      str
        """
        self.who_caused_condition = _who_stopped_the_program
        data_required_string = "{0}={1}".format(
            opcodes.who,
            _who_stopped_the_program
        )
        self.send_message(
            _action=ActionCommands.DO,
            _category=ProgramCommands.Program,
            _identifier=self.program.ad,
            _status_code=Message.stop,
            _data_required_string=data_required_string
        )

    def check_for_stop_message_not_present(self, _who_stopped_the_program):
        """
        Create a stop message on a 1000 program
        :param _who_stopped_the_program:     The thing that stopd this program. \n
            EX:
                BM - BaseManager
                US - User
                OP - Operator
                PR - Programmer
                AD - Admin
                MS - Moisture
                SW - Event Switch
                TS - Temperature
                DT - Date and Time
                DL - System Off
                RJ - Rain Switch
                RA - Rain Delay
        :type _who_stopped_the_program:      str
        """
        self.who_caused_condition = _who_stopped_the_program
        data_required_string = "{0}={1}".format(
            opcodes.who,
            _who_stopped_the_program
        )
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=ProgramCommands.Program,
                _identifier=self.program.ad,
                _status_code=Message.stop,
                _data_required_string=data_required_string
            )

        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_stop_message(self, _who_stopped_the_program):
        """
        Create a stop message on a 1000 program
        :param _who_stopped_the_program:     The thing that stopd this program. \n
            EX:
                BM - BaseManager
                US - User
                OP - Operator
                PR - Programmer
                AD - Admin
                MS - Moisture
                SW - Event Switch
                TS - Temperature
                DT - Date and Time
                DL - System Off
                RJ - Rain Switch
                RA - Rain Delay
        :type _who_stopped_the_program:      str
        """
        self.who_caused_condition = _who_stopped_the_program
        self.get_stop_message(_who_stopped_the_program=_who_stopped_the_program)
        self.verify_message(_status_code=Message.stop)