
from common.imports import opcodes
from common.objects.messages.base_messages import BaseMessages
from common.imports.types import ActionCommands, PointofControlCommands
from common.imports.types import Message
from common.date_package.date_resource import date_mngr

__author__ = 'Baseline'


class PointOfControlMessages(BaseMessages):

    #################################
    def __init__(self, _point_of_control_object):
        """
        :param _point_of_control_object:    Object this message is attached to. \n
        :type _point_of_control_object:     common.objects.programming.point_of_control.PointOfControl |
                                            common.objects.programming.poc_1000.POC1000
        """
        self.point_of_control = _point_of_control_object
        self.status_code = ''
        # Init parent class to inherit respective attributes
        BaseMessages.__init__(self, _controller=_point_of_control_object.controller)

        # Required Data
        self.device_string = ""  # A device name that is disabled ("Flow biCoder" | "Master Valve" | etc.)
        self.flow_meter = None
        self.pressure_sensor = None
        self.master_valve = None
        self.pump = None

    #################################
    def update_values(self):
        """
        This method updates all of the values that this message will use. \n
        """
        # A point of control has a flow meter, pressure sensor, master valve, and pump, we get them here.
        for master_valve in self.point_of_control.controller.master_valves.values():
            if master_valve.ad == self.point_of_control.mv:
                self.master_valve = master_valve
        for flow_meter in self.point_of_control.controller.flow_meters.values():
            if flow_meter.ad == self.point_of_control.fm:
                self.flow_meter = flow_meter
        for pressure_sensor in self.point_of_control.controller.pressure_sensors.values():
            if pressure_sensor.ad == self.point_of_control.ps:
                self.pressure_sensor = pressure_sensor
        for pump in self.point_of_control.controller.pumps.values():
            if pump.ad == self.point_of_control.pp:
                self.pump = pump

    #################################
    def build_message_header(self):
        if self.point_of_control.controller.is3200():
            message_header = 'CP {0}n'.format(
                self.point_of_control.ds
            )
        else:
            message_header = "Water Source WS-{0}n".format(
                self.point_of_control.ad,     # {0} Point of connection address
            )
        return message_header

    #################################
    def build_message_tx(self, _status_code):
        """
        :param _status_code:
        :type _status_code: str

        """
        tx_msg = self.build_message_header()

        # Create the title and header for the water source
        if self.point_of_control.controller.is3200():
            if _status_code is Message.configuration_error:
                tx_msg += "{0}{1}{2}{3}{4}{5}".format(
                    "CP Configuration Error:n",
                    "This CP will not operate.n",
                    "CP = ",
                    str(self.point_of_control.ad) + "n",
                    "Problem area: ",
                    self.device_string
                )

            elif _status_code is Message.disabled:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}".format(
                    "Control Point Disabled:n",
                    "This CP has dependent zonesn",
                    "which will not water.n",
                    "CP = ",
                    str(self.point_of_control.ad) + "n",
                    "Mainline = ",
                    str(self.point_of_control.ml) + "n",
                    )

            elif _status_code is Message.high_flow_detected:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}".format(
                    "CP High Flow Detected:n",
                    "Measured flow exceeds limit.n",
                    "CP = ",
                    str(self.point_of_control.ad) + "n",
                    "Flow biCoder = ",
                    str(self.flow_meter.bicoder.sn) + "n",
                    "Limit/Value = ",
                    str(self.point_of_control.hf),
                    " / ",
                    str(self.flow_meter.bicoder.vr),
                    )

            elif _status_code is Message.high_flow_shutdown:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}".format(
                    "CP High Flow Detected:n",
                    "CP water has been shut down.n",
                    "CP = ",
                    str(self.point_of_control.ad) + "n",
                    "Flow biCoder = ",
                    str(self.flow_meter.bicoder.sn) + "n",
                    "Limit/Value = ",
                    str(self.point_of_control.hf),
                    " / ",
                    str(self.flow_meter.bicoder.vr),
                    )

            elif _status_code is Message.high_pressure_detected:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}".format(
                    "CP High Pressure Detected:n",
                    "Measured pressure exceeds limit.n",
                    "CP = ",
                    str(self.point_of_control.ad) + "n",
                    "Pressure Sensor = ",
                    str(self.pressure_sensor.bicoder.sn) + "n",
                    "Limit/Value = ",
                    str(self.point_of_control.hp),
                    " / ",
                    str(self.pressure_sensor.bicoder.vr),
                    )

            elif _status_code is Message.high_pressure_shutdown:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}".format(
                    "CP High Pressure Detected:n",
                    "CP water has been shut down.n",
                    "CP = ",
                    str(self.point_of_control.ad) + "n",
                    "Pressure Sensor = ",
                    str(self.pressure_sensor.bicoder.sn) + "n",
                    "Limit/Value = ",
                    str(self.point_of_control.hp),
                    " / ",
                    str(self.pressure_sensor.bicoder.vr),
                    )

            elif _status_code is Message.low_pressure_detected:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}".format(
                    "CP Low Pressure Detected:n",
                    "Measured pressure exceeds limit.n",
                    "CP = ",
                    str(self.point_of_control.ad) + "n",
                    "Pressure Sensor = ",
                    str(self.pressure_sensor.bicoder.sn) + "n",
                    "Limit/Value = ",
                    str(self.point_of_control.lp),
                    " / ",
                    str(self.pressure_sensor.bicoder.vr),
                    )

            elif _status_code is Message.low_pressure_shutdown:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}".format(
                    "CP Low Pressure Detected:n",
                    "CP water has been shut down.n",
                    "CP = ",
                    str(self.point_of_control.ad) + "n",
                    "Pressure Sensor = ",
                    str(self.pressure_sensor.bicoder.sn) + "n",
                    "Limit/Value = ",
                    str(self.point_of_control.lp),
                    " / ",
                    str(self.pressure_sensor.bicoder.vr),
                    )

            elif _status_code is Message.unscheduled_flow_detected:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}".format(
                    "Flow biCoder Unexpected Flow:n",
                    "Measured flow is above limit.n",
                    "CP = ",
                    str(self.point_of_control.ad) + "n",
                    "Flow biCoder = ",
                    str(self.flow_meter.sn) + "n",
                    "Limit/Value = ",
                    str(self.point_of_control.uf),
                    " / ",
                    self.flow_meter.bicoder.vr
                )

            elif _status_code is Message.unscheduled_flow_shutdown:
                tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}".format(
                    "Flow biCoder Unexpected Flow:n",
                    "Measured flow is above limit.n",
                    "Watering has been shut down.n",
                    "CP = ",
                    str(self.point_of_control.ad) + "n",
                    "Flow biCoder = ",
                    str(self.flow_meter.bicoder.sn) + "n",
                    "Limit/Value = ",
                    str(self.point_of_control.uf),
                    " / ",
                    str(self.flow_meter.bicoder.vr)
                )
        else:
            if _status_code is Message.device_error:
                tx_msg += "Device Error"
            elif _status_code is Message.high_flow_shutdown:
                tx_msg += "High Flow Fault"
            elif _status_code is Message.unscheduled_flow_shutdown:
                tx_msg += "Unexpected Flow Fault"

        return tx_msg

    #################################
    def build_alarm_id(self, _status_code):
        """
        This return the alarm string.

        :param _status_code:
        :type _status_code:
        :return:
        :rtype:
        """

        # If the controller type is 3200, use the 3200 ID formats
        if self.point_of_control.controller.is3200():
            alarm_id = '{0}_{1}_{2}_{3}'.format(
                str(400 + self.point_of_control.ad).zfill(3),   # {0} Point of control ID number
                PointofControlCommands.PointOfControl,          # {1}
                self.point_of_control.ad,                       # {2} this is the address fof the point of control
                _status_code                                    # {3}
            )
        else:
            alarm_id = '{0}_{1}_{2}_{3}'.format(
                str(self.point_of_control.ad + 399),    # {0} this is the point of control number address
                opcodes.point_of_connection,            # {1}
                self.point_of_control.ad,               # {2} this is the point of control number address
                _status_code                            # {3}
            )

        return alarm_id

    # |-----------------------------------------------------------|
    #         Message: Point of Control Configuration Error
    # |-----------------------------------------------------------|
    #################################
    def set_configuration_error_message(self, _device_string="Flow Meter"):
        # The set string requires extra data
        self.update_values()
        self.device_string = _device_string
        data_required_string = "{0}={1}".format(
            opcodes.variable_2,
            self.device_string)

        self.send_message(
            _action=ActionCommands.SET,
            _category=PointofControlCommands.PointOfControl,
            _identifier=self.point_of_control.ad,
            _status_code=Message.configuration_error,
            _data_required_string=data_required_string)

    #################################
    def get_configuration_error_message(self):
        self.update_values()
        self.get_message(
            _action=ActionCommands.GET,
            _category=PointofControlCommands.PointOfControl,
            _identifier=self.point_of_control.ad,
            _status_code=Message.configuration_error)

    #################################
    def clear_configuration_error_message(self):
        self.update_values()
        self.send_message(
            _action=ActionCommands.DO,
            _category=PointofControlCommands.PointOfControl,
            _identifier=self.point_of_control.ad,
            _status_code=Message.configuration_error)

    def check_for_configuration_error_message_not_present(self):
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=PointofControlCommands.PointOfControl,
                _identifier=self.point_of_control.ad,
                _status_code=Message.configuration_error)

        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_configuration_error_message(self):
        self.get_configuration_error_message()
        self.verify_message(_status_code=Message.configuration_error)

    # |-----------------------------------------------------------|
    #         Message: Point of Control Disabled
    # |-----------------------------------------------------------|
    #################################
    def set_disabled_message(self):
        self.update_values()
        self.send_message(
            _action=ActionCommands.SET,
            _category=PointofControlCommands.PointOfControl,
            _identifier=self.point_of_control.ad,
            _status_code=Message.disabled)

    #################################
    def get_disabled_message(self):
        self.update_values()
        self.get_message(
            _action=ActionCommands.GET,
            _category=PointofControlCommands.PointOfControl,
            _identifier=self.point_of_control.ad,
            _status_code=Message.disabled)

    #################################
    def clear_disabled_message(self):
        self.update_values()
        self.send_message(
            _action=ActionCommands.DO,
            _category=PointofControlCommands.PointOfControl,
            _identifier=self.point_of_control.ad,
            _status_code=Message.disabled)

    def check_for_disabled_message_not_present(self):
        self.update_values()
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=PointofControlCommands.PointOfControl,
                _identifier=self.point_of_control.ad,
                _status_code=Message.disabled)

        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_disabled_message(self):
        self.get_disabled_message()
        self.verify_message(_status_code=Message.disabled)

    # |-----------------------------------------------------------|
    #         Message: Point of Control High Flow Detected
    # |-----------------------------------------------------------|
    #################################
    def set_high_flow_detected_message(self):
        self.update_values()
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1},{2}={3}".format(
            opcodes.variable_1,
            self.point_of_control.hf,
            opcodes.variable_2,
            self.flow_meter.bicoder.vg)

        self.send_message(
            _action=ActionCommands.SET,
            _category=PointofControlCommands.PointOfControl,
            _identifier=self.point_of_control.ad,
            _status_code=Message.high_flow_detected,
            _data_required_string=data_required_string)

    #################################
    def get_high_flow_detected_message(self):
        self.update_values()
        self.get_message(
            _action=ActionCommands.GET,
            _category=PointofControlCommands.PointOfControl,
            _identifier=self.point_of_control.ad,
            _status_code=Message.high_flow_detected)

    #################################
    def clear_high_flow_detected_message(self):
        self.update_values()
        self.send_message(
            _action=ActionCommands.DO,
            _category=PointofControlCommands.PointOfControl,
            _identifier=self.point_of_control.ad,
            _status_code=Message.high_flow_detected)

    def check_for_high_flow_detected_message_not_present(self):
        self.update_values()
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=PointofControlCommands.PointOfControl,
                _identifier=self.point_of_control.ad,
                _status_code=Message.high_flow_detected)

        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_high_flow_detected_message(self):
        self.get_high_flow_detected_message()
        self.verify_message(_status_code=Message.high_flow_detected)

    # |-----------------------------------------------------------|
    #         Message: Point of Control High Flow Shutdown
    # |-----------------------------------------------------------|
    #################################
    def set_high_flow_shutdown_message(self):
        # The set string requires extra data
        self.update_values()
        if self.point_of_control.controller.is1000():
            data_required_string = ''
        else:
            data_required_string = "{0}={1},{2}={3}".format(
                opcodes.variable_1,
                self.point_of_control.hf,
                opcodes.variable_2,
                self.flow_meter.bicoder.vg)

        self.send_message(
            _action=ActionCommands.SET,
            _category=PointofControlCommands.PointOfControl,
            _identifier=self.point_of_control.ad,
            _status_code=Message.high_flow_shutdown,
            _data_required_string=data_required_string)

    #################################
    def get_high_flow_shutdown_message(self):
        self.update_values()
        self.get_message(
            _action=ActionCommands.GET,
            _category=PointofControlCommands.PointOfControl,
            _identifier=self.point_of_control.ad,
            _status_code=Message.high_flow_shutdown)

    #################################
    def clear_high_flow_shutdown_message(self):
        self.update_values()
        self.send_message(
            _action=ActionCommands.DO,
            _category=PointofControlCommands.PointOfControl,
            _identifier=self.point_of_control.ad,
            _status_code=Message.high_flow_shutdown)

    def check_for_high_flow_shutdown_message_not_present(self):
        self.update_values()
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=PointofControlCommands.PointOfControl,
                _identifier=self.point_of_control.ad,
                _status_code=Message.high_flow_shutdown)

        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_high_flow_shutdown_message(self):
        self.get_high_flow_shutdown_message()
        self.verify_message(_status_code=Message.high_flow_shutdown)

    # |-------------------------------------------------------------|
    #         Message: Point of Control High Pressure Detected
    # |-------------------------------------------------------------|
    #################################
    def set_high_pressure_detected_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1},{2}={3}".format(
            opcodes.variable_1,
            self.point_of_control.hp,
            opcodes.variable_2,
            self.pressure_sensor.bicoder.va)

        self.send_message(
            _action=ActionCommands.SET,
            _category=PointofControlCommands.PointOfControl,
            _identifier=self.point_of_control.ad,
            _status_code=Message.high_pressure_detected,
            _data_required_string=data_required_string)

    #################################
    def get_high_pressure_detected_message(self):
        self.update_values()
        self.get_message(
            _action=ActionCommands.GET,
            _category=PointofControlCommands.PointOfControl,
            _identifier=self.point_of_control.ad,
            _status_code=Message.high_pressure_detected)

    #################################
    def clear_high_pressure_detected_message(self):
        self.update_values()
        self.send_message(
            _action=ActionCommands.DO,
            _category=PointofControlCommands.PointOfControl,
            _identifier=self.point_of_control.ad,
            _status_code=Message.high_pressure_detected)

    def check_for_high_pressure_detected_message_not_present(self):
        self.update_values()
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=PointofControlCommands.PointOfControl,
                _identifier=self.point_of_control.ad,
                _status_code=Message.high_pressure_detected)

        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_high_pressure_detected_message(self):
        self.get_high_pressure_detected_message()
        self.verify_message(_status_code=Message.high_pressure_detected)

    # |-------------------------------------------------------------|
    #         Message: Point of Control Low Pressure Detected
    # |-------------------------------------------------------------|
    #################################
    def set_low_pressure_detected_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1},{2}={3}".format(
            opcodes.variable_1,
            self.point_of_control.lp,
            opcodes.variable_2,
            self.pressure_sensor.bicoder.va)

        self.send_message(
            _action=ActionCommands.SET,
            _category=PointofControlCommands.PointOfControl,
            _identifier=self.point_of_control.ad,
            _status_code=Message.low_pressure_detected,
            _data_required_string=data_required_string)

    #################################
    def get_low_pressure_detected_message(self):
        self.update_values()
        self.get_message(
            _action=ActionCommands.GET,
            _category=PointofControlCommands.PointOfControl,
            _identifier=self.point_of_control.ad,
            _status_code=Message.low_pressure_detected)

    #################################
    def clear_low_pressure_detected_message(self):
        self.update_values()
        self.send_message(
            _action=ActionCommands.DO,
            _category=PointofControlCommands.PointOfControl,
            _identifier=self.point_of_control.ad,
            _status_code=Message.low_pressure_detected)

    def check_for_low_pressure_detected_message_not_present(self):
        self.update_values()
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=PointofControlCommands.PointOfControl,
                _identifier=self.point_of_control.ad,
                _status_code=Message.low_pressure_detected)

        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_low_pressure_detected_message(self):
        self.get_low_pressure_detected_message()
        self.verify_message(_status_code=Message.low_pressure_detected)

    # |-------------------------------------------------------------|
    #         Message: Point of Control High Pressure Shutdown
    # |-------------------------------------------------------------|
    #################################
    def set_high_pressure_shutdown_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1},{2}={3}".format(
            opcodes.variable_1,
            self.point_of_control.hp,
            opcodes.variable_2,
            self.pressure_sensor.bicoder.va)

        self.send_message(
            _action=ActionCommands.SET,
            _category=PointofControlCommands.PointOfControl,
            _identifier=self.point_of_control.ad,
            _status_code=Message.high_pressure_shutdown,
            _data_required_string=data_required_string)

    #################################
    def get_high_pressure_shutdown_message(self):
        self.update_values()
        self.get_message(
            _action=ActionCommands.GET,
            _category=PointofControlCommands.PointOfControl,
            _identifier=self.point_of_control.ad,
            _status_code=Message.high_pressure_shutdown)

    #################################
    def clear_high_pressure_shutdown_message(self):
        self.update_values()
        self.send_message(
            _action=ActionCommands.DO,
            _category=PointofControlCommands.PointOfControl,
            _identifier=self.point_of_control.ad,
            _status_code=Message.high_pressure_shutdown)

    def check_for_high_pressure_shutdown_message_not_present(self):
        self.update_values()
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=PointofControlCommands.PointOfControl,
                _identifier=self.point_of_control.ad,
                _status_code=Message.high_pressure_shutdown)

        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_high_pressure_shutdown_message(self):
        self.get_high_pressure_shutdown_message()
        self.verify_message(_status_code=Message.high_pressure_shutdown)

    # |-------------------------------------------------------------|
    #         Message: Point of Control Low Pressure Shutdown
    # |-------------------------------------------------------------|
    #################################
    def set_low_pressure_shutdown_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1},{2}={3}".format(
            opcodes.variable_1,
            self.point_of_control.hp,
            opcodes.variable_2,
            self.pressure_sensor.bicoder.va)

        self.send_message(
            _action=ActionCommands.SET,
            _category=PointofControlCommands.PointOfControl,
            _identifier=self.point_of_control.ad,
            _status_code=Message.low_pressure_shutdown,
            _data_required_string=data_required_string)

    #################################
    def get_low_pressure_shutdown_message(self):
        self.update_values()
        self.get_message(
            _action=ActionCommands.GET,
            _category=PointofControlCommands.PointOfControl,
            _identifier=self.point_of_control.ad,
            _status_code=Message.low_pressure_shutdown)

    #################################
    def clear_low_pressure_shutdown_message(self):
        self.update_values()
        self.send_message(
            _action=ActionCommands.DO,
            _category=PointofControlCommands.PointOfControl,
            _identifier=self.point_of_control.ad,
            _status_code=Message.low_pressure_shutdown)

    def check_for_low_pressure_shutdown_message_not_present(self):
        self.update_values()
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=PointofControlCommands.PointOfControl,
                _identifier=self.point_of_control.ad,
                _status_code=Message.low_pressure_shutdown)

        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_low_pressure_shutdown_message(self):
        self.get_low_pressure_shutdown_message()
        self.verify_message(_status_code=Message.low_pressure_shutdown)

    # |----------------------------------------------------------------|
    #         Message: Point of Control Unscheduled Flow Detected
    # |----------------------------------------------------------------|
    #################################
    def set_unscheduled_flow_detected_message(self):
        # The set string requires extra data
        self.update_values()
        data_required_string = "{0}={1},{2}={3}".format(
            opcodes.variable_1,
            self.point_of_control.uf,
            opcodes.variable_2,
            self.flow_meter.bicoder.vr)

        self.send_message(
            _action=ActionCommands.SET,
            _category=PointofControlCommands.PointOfControl,
            _identifier=self.point_of_control.ad,
            _status_code=Message.unscheduled_flow_detected,
            _data_required_string=data_required_string)

    #################################
    def get_unscheduled_flow_detected_message(self):
        self.update_values()
        self.get_message(
            _action=ActionCommands.GET,
            _category=PointofControlCommands.PointOfControl,
            _identifier=self.point_of_control.ad,
            _status_code=Message.unscheduled_flow_detected)

    #################################
    def clear_unscheduled_flow_detected_message(self):
        self.update_values()
        self.send_message(
            _action=ActionCommands.DO,
            _category=PointofControlCommands.PointOfControl,
            _identifier=self.point_of_control.ad,
            _status_code=Message.unscheduled_flow_detected)

    def check_for_unscheduled_flow_detected_message_not_present(self):
        self.update_values()
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=PointofControlCommands.PointOfControl,
                _identifier=self.point_of_control.ad,
                _status_code=Message.unscheduled_flow_detected)

        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_unscheduled_flow_detected_message(self):
        self.get_unscheduled_flow_detected_message()
        self.verify_message(_status_code=Message.unscheduled_flow_detected)

    # |----------------------------------------------------------------|
    #         Message: Point of Control Unscheduled Flow Shutdown
    # |----------------------------------------------------------------|
    #################################
    def set_unscheduled_flow_shutdown_message(self):
        # The set string requires extra data
        self.update_values()
        if self.point_of_control.controller.is1000():
            data_required_string = ''
        else:
            data_required_string = "{0}={1},{2}={3}".format(
                opcodes.variable_1,
                self.point_of_control.uf,
                opcodes.variable_2,
                self.flow_meter.bicoder.vr)

        self.send_message(
            _action=ActionCommands.SET,
            _category=PointofControlCommands.PointOfControl,
            _identifier=self.point_of_control.ad,
            _status_code=Message.unscheduled_flow_shutdown,
            _data_required_string=data_required_string)

    #################################
    def get_unscheduled_flow_shutdown_message(self):
        self.update_values()
        self.get_message(
            _action=ActionCommands.GET,
            _category=PointofControlCommands.PointOfControl,
            _identifier=self.point_of_control.ad,
            _status_code=Message.unscheduled_flow_shutdown)

    #################################
    def clear_unscheduled_flow_shutdown_message(self):
        self.update_values()
        self.send_message(
            _action=ActionCommands.DO,
            _category=PointofControlCommands.PointOfControl,
            _identifier=self.point_of_control.ad,
            _status_code=Message.unscheduled_flow_shutdown)

    def check_for_unscheduled_flow_shutdown_message_not_present(self):
        self.update_values()
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=PointofControlCommands.PointOfControl,
                _identifier=self.point_of_control.ad,
                _status_code=Message.unscheduled_flow_shutdown)

        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_unscheduled_flow_shutdown_message(self):
        self.get_unscheduled_flow_shutdown_message()
        self.verify_message(_status_code=Message.unscheduled_flow_shutdown)

    # |-----------------------------------------------------------|
    #         Message: Device Error
    # |-----------------------------------------------------------|
    #################################
    def set_device_error_message(self):
        self.update_values()
        self.send_message(
            _action=ActionCommands.SET,
            _category=PointofControlCommands.PointOfControl,
            _identifier=self.point_of_control.ad,
            _status_code=Message.device_error)

    #################################
    def get_device_error_message(self):
        self.update_values()
        self.get_message(
            _action=ActionCommands.GET,
            _category=PointofControlCommands.PointOfControl,
            _identifier=self.point_of_control.ad,
            _status_code=Message.device_error)

    #################################
    def clear_device_error_message(self):
        self.update_values()
        self.send_message(
            _action=ActionCommands.DO,
            _category=PointofControlCommands.PointOfControl,
            _identifier=self.point_of_control.ad,
            _status_code=Message.device_error)

    def check_for_device_error_message_not_present(self):
        self.update_values()
        try:
            self.send_message(
                _action=ActionCommands.DO,
                _category=PointofControlCommands.PointOfControl,
                _identifier=self.point_of_control.ad,
                _status_code=Message.device_error)

        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_device_error_message(self):
        self.get_device_error_message()
        self.verify_message(_status_code=Message.device_error)
