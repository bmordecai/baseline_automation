
from common.imports import opcodes
from common.objects.messages.base_messages import BaseMessages
from common.imports.types import ActionCommands, ZoneProgramCommands
from common.imports.types import Message

__author__ = 'Baseline'


class ZoneProgramMessages(BaseMessages):

    #################################
    def __init__(self, _zone_program_object):
        """
        :param _zone_program_object:     Object this message is attached to. \n
        :type _zone_program_object:      common.objects.programming.zp.ZoneProgram
        """
        self.zone_program = _zone_program_object
        self.status_code = ''
        # Init parent class to inherit respective attributes
        BaseMessages.__init__(self, _controller=_zone_program_object.controller)

        # Required Data
        self.moisture_sensor = None
        self.point_of_control = None
        self.flow_meter = None

    #################################
    def update_values(self):
        """
        This method updates all of the values that this message will use. \n
        """
        if self.zone_program.ms:
            for moisture_sensor in self.zone_program.controller.moisture_sensors.values():
                if moisture_sensor.ad == self.zone_program.ms:
                    self.moisture_sensor = moisture_sensor

        if self.zone_program.zone.ml:
            for poc in self.zone_program.controller.points_of_control.values():
                if poc.ml == self.zone_program.zone.ml:
                    self.point_of_control = poc
                    if poc.fm:
                        self.flow_meter = self.zone_program.controller.flow_meters[poc.fm]

    #################################
    def build_message_header(self):
        message_header = 'Zone {0}: {1}n'.format(
            self.zone_program.zone.ad,
            self.zone_program.zone.ds
        )
        return message_header

    #################################
    def build_message_tx(self, _status_code):
        """
        :param _status_code:
        :type _status_code: str

        """
        self.update_values()
        tx_msg = self.build_message_header()

        if _status_code is Message.calibrate_failure_no_change:
            tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}".format(
                "Moisture biSensor Calibration Failed:n",
                "Moisture did not increase.n",
                "Primary Zone = ",
                str(self.zone_program.zone.ad) + "n",
                "Moisture biSensor = ",
                str(self.moisture_sensor.sn) + "n",
                "Moisture = ",
                str(self.moisture_sensor.bicoder.vp)
            )
        elif _status_code is Message.calibrate_failure_no_saturation:
            tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}".format(
                "Moisture biSensor Calibration Failed:n",
                "Did not reach Saturation.n",
                "Primary Zone = ",
                str(self.zone_program.zone.ad) + "n",
                "Moisture biSensor = ",
                str(self.moisture_sensor.bicoder.sn) + "n",
                "Moisture = ",
                str(self.moisture_sensor.bicoder.vp) + "n"
            )
        elif _status_code is Message.calibrate_successful:
            tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}".format(
                "Moisture biSensor Calibration Done:n",
                "New watering limits set.n",
                "Primary Zone = ",
                str(self.zone_program.zone.ad) + "n",
                "Moisture biSensor = ",
                str(self.moisture_sensor.bicoder.sn) + "n",
                "Limit Lower/Upper = ",
                str(self.zone_program.ll) + "n",
                " / ",
                str(self.zone_program.ul)
            )

        elif _status_code is Message.exceeds_design_flow:
            tx_msg += "{0}{1}{2}{3}{4}{5}{6}".format(
                "Zone High Flow:n",
                "Zone design flow exceeds available.n",
                "It will be run by itself.n",
                "Zone = ",
                str(self.zone_program.zone.ad) + "n",
                "Design Flow = ",
                str(self.zone_program.zone.df)
            )

        elif _status_code is Message.flow_learn_errors:
            # While doing a learn flow, if the first reading after the flow stable time is zero the zone will fail
            # immediately. The message should then have 0 for flow instead of the zones design flow
            if self.flow_meter.bicoder.vr == 0:
                tx_msg += "{0}{1}{2}{3}{4}{5}".format(
                    "Zone Learn Flow Done:n",
                    "This zone had errors.n",
                    "Zone = ",
                    str(self.zone_program.zone.ad) + "n",
                    "Learn Flow = ",
                    str(0.0)
                )
            else:
                tx_msg += "{0}{1}{2}{3}{4}{5}".format(
                    "Zone Learn Flow Done:n",
                    "This zone had errors.n",
                    "Zone = ",
                    str(self.zone_program.zone.ad) + "n",
                    "Learn Flow = ",
                    str(self.zone_program.zone.df)
                )

        elif _status_code is Message.flow_learn_ok:

            tx_msg += "{0}{1}{2}{3}{4}{5}".format(
                "Zone Learn Flow Done:n",
                "Completed successfully.n",
                "Zone = ",
                str(self.zone_program.zone.ad) + "n",
                "Learn Flow = ",
                str(self.zone_program.zone.df)
            )

        elif _status_code is Message.high_flow_shutdown_by_flow_station:
            tx_msg += "{0}{1}{2}{3}{4}".format(
                "FlowStation High Flow:n",
                "Measured flow exceeds limit.n",
                "Zone has been shut down.n",
                "Zone = ",
                str(self.zone_program.zone.ad) + "n"
            )

        elif _status_code is Message.high_flow_variance_shutdown:
            tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}".format(
                "Zone High Flow Variance:n",
                "Measured flow exceeds limit.n",
                "Zone has been shut down.n",
                "Zone = ",
                str(self.zone_program.zone.ad) + "n",
                "Limit/Value = ",
                str(self.zone_program.zone.df),
                " / ",
                str(self.flow_meter.bicoder.vr)
            )

        elif _status_code is Message.low_flow_shutdown_by_flow_station:
            tx_msg += "{0}{1}{2}{3}{4}".format(
                "FlowStation Low Flow:n",
                "Measured flow is below lower limit.n",
                "Zone has been shut down.n",
                "Zone = ",
                str(self.zone_program.zone.ad) + "n"
            )

        elif _status_code is Message.low_flow_variance_shutdown:
            tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}{8}".format(
                "Zone Low Flow Variance:n",
                "Measured flow is below lower limit.n",
                "Zone has been shut down.n",
                "Zone = ",
                str(self.zone_program.zone.ad) + "n",
                "Limit/Value = ",
                str(self.zone_program.zone.df),
                " / ",
                str(self.flow_meter.bicoder.vr)
            )

        elif _status_code is Message.high_flow_variance_detected:
            tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}".format(
                "Zone High Flow Variance:n",
                "Measured flow exceeds limit.n",
                "Zone = ",
                str(self.zone_program.zone.ad) + "n",
                "Limit/Value = ",
                str(self.zone_program.zone.df),
                " / ",
                str(self.flow_meter.bicoder.vr)
            )
        elif _status_code is Message.low_flow_variance_detected:
            tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}".format(
                "Zone Low Flow Variance:n",
                "Measured flow is below lower limit.n",
                "Zone = ",
                str(self.zone_program.zone.ad) + "n",
                "Limit/Value = ",
                str(self.zone_program.zone.df),
                " / ",
                str(self.flow_meter.bicoder.vr)
            )

        elif _status_code is Message.requires_soak_cycle:
            tx_msg += "{0}{1}{2}{3}{4}{5}{6}{7}".format(
                "Moisture biSensor Requirement:n",
                "Soak Cycles must be used.n",
                "Primary Zone = ",
                str(self.zone_program.zone.ad) + "n",
                "Program = ",
                str(self.zone_program.program.ad) + "n",
                "Moisture biSensor = ",
                str(self.moisture_sensor.sn) + "n",
            )

        return tx_msg

    #################################
    def build_alarm_id(self, _status_code):
        """
        This return the alarm string.

        :param _status_code:
        :type _status_code:
        :return:
        :rtype:
        """
        # If the controller type is 3200, use the 3200 ID formats
        alarm_id = '{0}_{1}_{2}_{3}'.format(
            str(self.zone_program.zone.ad).zfill(3),    # {0} Zone Program ID number
            ZoneProgramCommands.Zones,                  # {1}
            self.zone_program.zone.ad,                  # {2} this is the address for the zone program
            _status_code                                # {3}
        )
        return alarm_id

    # |--------------------------------------------------------------------------------|
    #         Message: Zone Program Moisture Sensor Calibration Failure No Change
    # |--------------------------------------------------------------------------------|
    #################################
    def set_moisture_sensor_calibration_failure_no_change_message(self):
        self.update_values()
        data_required_string = "{0}={1},{2}={3},{4}={5},{6}={7}".format(
            ZoneProgramCommands.Programs,
            self.zone_program.program.ad,
            opcodes.device_value,
            self.zone_program.zone.sn,
            opcodes.moisture_sensor,
            self.moisture_sensor.sn,
            opcodes.variable_1,
            self.moisture_sensor.bicoder.vp
        )
        self.send_message(
            _action=ActionCommands.SET,
            _category=ZoneProgramCommands.Zones,
            _identifier=self.zone_program.zone.ad,
            _status_code=Message.moisture_sensor_calibration_failure_no_change,
            _data_required_string=data_required_string)

    #################################
    def get_moisture_sensor_calibration_failure_no_change_message(self):
        self.update_values()
        data_required_string = "{0}={1},{2}={3}".format(
            ZoneProgramCommands.Programs, self.zone_program.program.ad, opcodes.device_value, self.zone_program.zone.sn
        )
        self.get_message(
            _action=ActionCommands.GET,
            _category=ZoneProgramCommands.Zones,
            _identifier=self.zone_program.zone.ad,
            _status_code=Message.moisture_sensor_calibration_failure_no_change,
            _data_required_string=data_required_string)

    #################################
    def clear_moisture_sensor_calibration_failure_no_change_message(self):
        self.update_values()
        data_required_string = "{0}={1},{2}={3}".format(
            ZoneProgramCommands.Programs, self.zone_program.program.ad, opcodes.device_value, self.zone_program.zone.sn
        )
        self.send_message(
            _action=ActionCommands.DO,
            _category=ZoneProgramCommands.Zones,
            _identifier=self.zone_program.zone.ad,
            _status_code=Message.moisture_sensor_calibration_failure_no_change,
            _data_required_string=data_required_string)

    def check_for_moisture_sensor_calibration_failure_no_change_message_not_present(self):
        self.update_values()
        try:
            data_required_string = "{0}={1},{2}={3}".format(
                ZoneProgramCommands.Programs,
                self.zone_program.program.ad,
                opcodes.device_value,
                self.zone_program.zone.sn
            )
            self.send_message(
                _action=ActionCommands.DO,
                _category=ZoneProgramCommands.Zones,
                _identifier=self.zone_program.zone.ad,
                _status_code=Message.moisture_sensor_calibration_failure_no_change,
                _data_required_string=data_required_string)

        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_moisture_sensor_calibration_failure_no_change_message(self):
        self.get_moisture_sensor_calibration_failure_no_change_message()
        self.verify_message(_status_code=Message.moisture_sensor_calibration_failure_no_change)

    # |--------------------------------------------------------------------------------|
    #         Message: Zone Program Moisture Sensor Calibration Failure No Saturation
    # |--------------------------------------------------------------------------------|
    #################################
    def set_moisture_sensor_calibration_failure_no_saturation_message(self):
        self.update_values()
        data_required_string = "{0}={1},{2}={3},{4}={5},{6}={7}".format(
            ZoneProgramCommands.Programs,
            self.zone_program.program.ad,
            opcodes.device_value,
            self.zone_program.zone.sn,
            opcodes.moisture_sensor,
            self.moisture_sensor.sn,
            opcodes.variable_1,
            self.moisture_sensor.bicoder.vp
        )
        self.send_message(
            _action=ActionCommands.SET,
            _category=ZoneProgramCommands.Zones,
            _identifier=self.zone_program.zone.ad,
            _status_code=Message.moisture_sensor_calibration_failure_no_saturation,
            _data_required_string=data_required_string)

    #################################
    def get_moisture_sensor_calibration_failure_no_saturation_message(self):
        self.update_values()
        data_required_string = "{0}={1},{2}={3}".format(
            ZoneProgramCommands.Programs, self.zone_program.program.ad, opcodes.device_value, self.zone_program.zone.sn
        )
        self.get_message(
            _action=ActionCommands.GET,
            _category=ZoneProgramCommands.Zones,
            _identifier=self.zone_program.zone.ad,
            _status_code=Message.moisture_sensor_calibration_failure_no_saturation,
            _data_required_string=data_required_string)

    #################################
    def clear_moisture_sensor_calibration_failure_no_saturation_message(self):
        self.update_values()
        data_required_string = "{0}={1},{2}={3}".format(
            ZoneProgramCommands.Programs, self.zone_program.program.ad, opcodes.device_value, self.zone_program.zone.sn
        )
        self.send_message(
            _action=ActionCommands.DO,
            _category=ZoneProgramCommands.Zones,
            _identifier=self.zone_program.zone.ad,
            _status_code=Message.moisture_sensor_calibration_failure_no_saturation,
            _data_required_string=data_required_string)

    def check_for_moisture_sensor_calibration_failure_no_saturation_message_not_present(self):
        self.update_values()
        try:
            data_required_string = "{0}={1},{2}={3}".format(
                ZoneProgramCommands.Programs,
                self.zone_program.program.ad,
                opcodes.device_value,
                self.zone_program.zone.sn
            )
            self.send_message(
                _action=ActionCommands.DO,
                _category=ZoneProgramCommands.Zones,
                _identifier=self.zone_program.zone.ad,
                _status_code=Message.moisture_sensor_calibration_failure_no_saturation,
                _data_required_string=data_required_string)

        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_moisture_sensor_calibration_failure_no_saturation_message(self):
        self.get_moisture_sensor_calibration_failure_no_saturation_message()
        self.verify_message(_status_code=Message.moisture_sensor_calibration_failure_no_saturation)

    # |--------------------------------------------------------------------------------|
    #         Message: Zone Program Moisture Sensor Calibration Success
    # |--------------------------------------------------------------------------------|
    #################################
    def set_moisture_sensor_calibration_success_message(self):
        self.update_values()
        data_required_string = "{0}={1},{2}={3},{4}={5},{6}={7},{8}={9}".format(
            ZoneProgramCommands.Programs,
            self.zone_program.program.ad,
            opcodes.device_value,
            self.zone_program.zone.sn,
            opcodes.moisture_sensor,
            self.moisture_sensor.sn,
            opcodes.variable_1,
            self.zone_program.ll,
            opcodes.variable_2,
            self.zone_program.ul
        )
        self.send_message(
            _action=ActionCommands.SET,
            _category=ZoneProgramCommands.Zones,
            _identifier=self.zone_program.zone.ad,
            _status_code=Message.moisture_sensor_calibration_success,
            _data_required_string=data_required_string)

    #################################
    def get_moisture_sensor_calibration_success_message(self):
        self.update_values()
        data_required_string = "{0}={1},{2}={3}".format(
            ZoneProgramCommands.Programs, self.zone_program.program.ad, opcodes.device_value, self.zone_program.zone.sn
        )
        self.get_message(
            _action=ActionCommands.GET,
            _category=ZoneProgramCommands.Zones,
            _identifier=self.zone_program.zone.ad,
            _status_code=Message.moisture_sensor_calibration_success,
            _data_required_string=data_required_string)

    #################################
    def clear_moisture_sensor_calibration_success_message(self):
        self.update_values()
        data_required_string = "{0}={1},{2}={3}".format(
            ZoneProgramCommands.Programs, self.zone_program.program.ad, opcodes.device_value, self.zone_program.zone.sn
        )
        self.send_message(
            _action=ActionCommands.DO,
            _category=ZoneProgramCommands.Zones,
            _identifier=self.zone_program.zone.ad,
            _status_code=Message.moisture_sensor_calibration_success,
            _data_required_string=data_required_string)

    def check_for_moisture_sensor_calibration_success_message_not_present(self):
        self.update_values()
        try:
            data_required_string = "{0}={1},{2}={3}".format(
                ZoneProgramCommands.Programs,
                self.zone_program.program.ad,
                opcodes.device_value,
                self.zone_program.zone.sn
            )
            self.send_message(
                _action=ActionCommands.DO,
                _category=ZoneProgramCommands.Zones,
                _identifier=self.zone_program.zone.ad,
                _status_code=Message.moisture_sensor_calibration_success,
                _data_required_string=data_required_string)

        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_moisture_sensor_calibration_success_message(self):
        self.get_moisture_sensor_calibration_success_message()
        self.verify_message(_status_code=Message.moisture_sensor_calibration_success)

    # |--------------------------------------------------------------|
    #         Message: Zone Program Exceeds Available Flow
    # |--------------------------------------------------------------|
    #################################
    def set_exceeds_available_flow_message(self):
        self.update_values()
        data_required_string = "{0}={1},{2}={3},{4}={5},{6}={7},{8}={9}".format(
            ZoneProgramCommands.PointsOfControl,
            self.point_of_control.ad,
            ZoneProgramCommands.Programs,
            self.zone_program.program.ad,
            opcodes.device_value,
            self.zone_program.zone.sn,
            opcodes.variable_1,
            self.flow_meter.bicoder.vr,
            opcodes.variable_2,
            self.zone_program.zone.df
        )
        self.send_message(
            _action=ActionCommands.SET,
            _category=ZoneProgramCommands.Zones,
            _identifier=self.zone_program.zone.ad,
            _status_code=Message.exceeds_design_flow,
            _data_required_string=data_required_string)

    #################################
    def get_exceeds_available_flow_message(self):
        self.update_values()
        data_required_string = "{0}={1},{2}={3},{4}={5}".format(
            ZoneProgramCommands.PointsOfControl,
            self.point_of_control.ad,
            ZoneProgramCommands.Programs,
            self.zone_program.program.ad,
            opcodes.device_value,
            self.zone_program.zone.sn,
        )
        self.get_message(
            _action=ActionCommands.GET,
            _category=ZoneProgramCommands.Zones,
            _identifier=self.zone_program.zone.ad,
            _status_code=Message.exceeds_design_flow,
            _data_required_string=data_required_string)

    #################################
    def clear_exceeds_available_flow_message(self):
        self.update_values()
        data_required_string = "{0}={1},{2}={3},{4}={5}".format(
            ZoneProgramCommands.PointsOfControl,
            self.point_of_control.ad,
            ZoneProgramCommands.Programs,
            self.zone_program.program.ad,
            opcodes.device_value,
            self.zone_program.zone.sn,
        )
        self.send_message(
            _action=ActionCommands.DO,
            _category=ZoneProgramCommands.Zones,
            _identifier=self.zone_program.zone.ad,
            _status_code=Message.exceeds_design_flow,
            _data_required_string=data_required_string)

    def check_for_exceeds_available_flow_message_not_present(self):
        self.update_values()
        try:
            data_required_string = "{0}={1},{2}={3},{4}={5}".format(
                ZoneProgramCommands.PointsOfControl,
                self.point_of_control.ad,
                ZoneProgramCommands.Programs,
                self.zone_program.program.ad,
                opcodes.device_value,
                self.zone_program.zone.sn,
            )
            self.send_message(
                _action=ActionCommands.DO,
                _category=ZoneProgramCommands.Zones,
                _identifier=self.zone_program.zone.ad,
                _status_code=Message.exceeds_design_flow,
                _data_required_string=data_required_string)

        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_exceeds_available_flow_message(self):
        self.get_exceeds_available_flow_message()
        self.verify_message(_status_code=Message.exceeds_design_flow)

    # # |--------------------------------------------------------------|
    # #         Message: Zone Program Learn Flow Complete Error
    # # |--------------------------------------------------------------|
    # #################################
    # def set_learn_flow_complete_error_message(self):
    #     self.update_values()
    #     data_required_string = "{0}={1},{2}={3},{4}={5}".format(
    #         ZoneProgramCommands.Programs,
    #         self.zone_program.program.ad,
    #         opcodes.device_value,
    #         self.zone_program.zone.sn,
    #         opcodes.variable_2,
    #         self.flow_meter.bicoder.vr
    #     )
    #     self.send_message(
    #         _action=ActionCommands.SET,
    #         _category=ZoneProgramCommands.Zones,
    #         _identifier=self.zone_program.zone.ad,
    #         _status_code=Message.zone_learn_flow_complete_errors,
    #         _data_required_string=data_required_string)
    #
    # #################################
    # def get_learn_flow_complete_error_message(self):
    #     self.update_values()
    #     data_required_string = "{0}={1},{2}={3}".format(
    #         ZoneProgramCommands.Programs,
    #         self.zone_program.program.ad,
    #         opcodes.device_value,
    #         self.zone_program.zone.sn,
    #     )
    #     self.get_message(
    #         _action=ActionCommands.GET,
    #         _category=ZoneProgramCommands.Zones,
    #         _identifier=self.zone_program.zone.ad,
    #         _status_code=Message.zone_learn_flow_complete_errors,
    #         _data_required_string=data_required_string)
    #
    # #################################
    # def clear_learn_flow_complete_error_message(self):
    #     self.update_values()
    #     data_required_string = "{0}={1},{2}={3}".format(
    #         ZoneProgramCommands.Programs,
    #         self.zone_program.program.ad,
    #         opcodes.device_value,
    #         self.zone_program.zone.df
    #     )
    #     self.send_message(
    #         _action=ActionCommands.DO,
    #         _category=ZoneProgramCommands.Zones,
    #         _identifier=self.zone_program.zone.ad,
    #         _status_code=Message.zone_learn_flow_complete_errors,
    #         _data_required_string=data_required_string)
    #
    # def check_for_learn_flow_complete_error_message_not_present(self):
    #     self.update_values()
    #     try:
    #         data_required_string = "{0}={1},{2}={3}".format(
    #             ZoneProgramCommands.Programs,
    #             self.zone_program.program.ad,
    #             opcodes.device_value,
    #             self.zone_program.zone.sn,
    #         )
    #         self.send_message(
    #             _action=ActionCommands.DO,
    #             _category=ZoneProgramCommands.Zones,
    #             _identifier=self.zone_program.zone.ad,
    #             _status_code=Message.zone_learn_flow_complete_errors,
    #             _data_required_string=data_required_string)
    #
    #     except Exception as e:
    #         if e.message == "NM No Message Found":
    #             print("Message was to found on controller.")
    #         else:
    #             print("Was unable to verify if the message was gone.")
    #
    # #################################
    # def verify_learn_flow_complete_error_message(self):
    #     self.get_learn_flow_complete_error_message()
    #     self.verify_message(_status_code=Message.zone_learn_flow_complete_errors)
    #
    # # |--------------------------------------------------------------|
    # #         Message: Zone Program Learn Flow Complete Success
    # # |--------------------------------------------------------------|
    # #################################
    # def set_learn_flow_complete_success_message(self):
    #     self.update_values()
    #     data_required_string = "{0}={1},{2}={3},{4}={5}".format(
    #         ZoneProgramCommands.Programs,
    #         self.zone_program.program.ad,
    #         opcodes.device_value,
    #         self.zone_program.zone.sn,
    #         opcodes.variable_2,
    #         self.zone_program.zone.df
    #     )
    #     self.send_message(
    #         _action=ActionCommands.SET,
    #         _category=ZoneProgramCommands.Zones,
    #         _identifier=self.zone_program.zone.ad,
    #         _status_code=Message.zone_learn_flow_complete_success,
    #         _data_required_string=data_required_string)
    #
    # #################################
    # def get_learn_flow_complete_success_message(self):
    #     self.update_values()
    #     data_required_string = "{0}={1},{2}={3}".format(
    #         ZoneProgramCommands.Programs,
    #         self.zone_program.program.ad,
    #         opcodes.device_value,
    #         self.zone_program.zone.sn,
    #     )
    #     self.get_message(
    #         _action=ActionCommands.GET,
    #         _category=ZoneProgramCommands.Zones,
    #         _identifier=self.zone_program.zone.ad,
    #         _status_code=Message.zone_learn_flow_complete_success,
    #         _data_required_string=data_required_string)
    #
    # #################################
    # def clear_learn_flow_complete_success_message(self):
    #     self.update_values()
    #     data_required_string = "{0}={1},{2}={3}".format(
    #         ZoneProgramCommands.Programs,
    #         self.zone_program.program.ad,
    #         opcodes.device_value,
    #         self.zone_program.zone.sn,
    #     )
    #     self.send_message(
    #         _action=ActionCommands.DO,
    #         _category=ZoneProgramCommands.Zones,
    #         _identifier=self.zone_program.zone.ad,
    #         _status_code=Message.zone_learn_flow_complete_success,
    #         _data_required_string=data_required_string)
    #
    # def check_for_learn_flow_complete_success_message_not_present(self):
    #     self.update_values()
    #     try:
    #         data_required_string = "{0}={1},{2}={3}".format(
    #             ZoneProgramCommands.Programs,
    #             self.zone_program.program.ad,
    #             opcodes.device_value,
    #             self.zone_program.zone.sn,
    #         )
    #         self.send_message(
    #             _action=ActionCommands.DO,
    #             _category=ZoneProgramCommands.Zones,
    #             _identifier=self.zone_program.zone.ad,
    #             _status_code=Message.zone_learn_flow_complete_success,
    #             _data_required_string=data_required_string)
    #
    #     except Exception as e:
    #         if e.message == "NM No Message Found":
    #             print("Message was to found on controller.")
    #         else:
    #             print("Was unable to verify if the message was gone.")
    #
    # #################################
    # def verify_learn_flow_complete_success_message(self):
    #     self.get_learn_flow_complete_success_message()
    #     self.verify_message(_status_code=Message.zone_learn_flow_complete_success)

    # |-------------------------------------------------------------------------|
    #         Message: Zone Program Shutdown On High Flow By Flowstation
    # |-------------------------------------------------------------------------|
    #################################
    def set_shutdown_on_high_flow_by_flowstation_message(self):
        self.update_values()
        data_required_string = "{0}={1},{2}={3}".format(
            ZoneProgramCommands.Programs,
            self.zone_program.program.ad,
            opcodes.device_value,
            self.zone_program.zone.sn,
        )
        self.send_message(
            _action=ActionCommands.SET,
            _category=ZoneProgramCommands.Zones,
            _identifier=self.zone_program.zone.ad,
            _status_code=Message.high_flow_shutdown_by_flow_station,
            _data_required_string=data_required_string)

    #################################
    def get_shutdown_on_high_flow_by_flowstation_message(self):
        self.update_values()
        data_required_string = "{0}={1},{2}={3}".format(
            ZoneProgramCommands.Programs,
            self.zone_program.program.ad,
            opcodes.device_value,
            self.zone_program.zone.sn,
        )
        self.get_message(
            _action=ActionCommands.GET,
            _category=ZoneProgramCommands.Zones,
            _identifier=self.zone_program.zone.ad,
            _status_code=Message.high_flow_shutdown_by_flow_station,
            _data_required_string=data_required_string)

    #################################
    def clear_shutdown_on_high_flow_by_flowstation_message(self):
        self.update_values()
        data_required_string = "{0}={1},{2}={3}".format(
            ZoneProgramCommands.Programs,
            self.zone_program.program.ad,
            opcodes.device_value,
            self.zone_program.zone.sn,
        )
        self.send_message(
            _action=ActionCommands.DO,
            _category=ZoneProgramCommands.Zones,
            _identifier=self.zone_program.zone.ad,
            _status_code=Message.high_flow_shutdown_by_flow_station,
            _data_required_string=data_required_string)

    def check_for_shutdown_on_high_flow_by_flowstation_message_not_present(self):
        self.update_values()
        try:
            data_required_string = "{0}={1},{2}={3}".format(
                ZoneProgramCommands.Programs,
                self.zone_program.program.ad,
                opcodes.device_value,
                self.zone_program.zone.sn,
            )
            self.send_message(
                _action=ActionCommands.DO,
                _category=ZoneProgramCommands.Zones,
                _identifier=self.zone_program.zone.ad,
                _status_code=Message.high_flow_shutdown_by_flow_station,
                _data_required_string=data_required_string)

        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_shutdown_on_high_flow_by_flowstation_message(self):
        self.get_shutdown_on_high_flow_by_flowstation_message()
        self.verify_message(_status_code=Message.high_flow_shutdown_by_flow_station)

    # |-------------------------------------------------------------------------|
    #         Message: Zone Program Shutdown On Low Flow By Flowstation
    # |-------------------------------------------------------------------------|
    #################################
    def set_shutdown_on_low_flow_by_flowstation_message(self):
        self.update_values()
        data_required_string = "{0}={1},{2}={3}".format(
            ZoneProgramCommands.Programs,
            self.zone_program.program.ad,
            opcodes.device_value,
            self.zone_program.zone.sn,
        )
        self.send_message(
            _action=ActionCommands.SET,
            _category=ZoneProgramCommands.Zones,
            _identifier=self.zone_program.zone.ad,
            _status_code=Message.low_flow_shutdown_by_flow_station,
            _data_required_string=data_required_string)

    #################################
    def get_shutdown_on_low_flow_by_flowstation_message(self):
        self.update_values()
        data_required_string = "{0}={1},{2}={3}".format(
            ZoneProgramCommands.Programs,
            self.zone_program.program.ad,
            opcodes.device_value,
            self.zone_program.zone.sn,
        )
        self.get_message(
            _action=ActionCommands.GET,
            _category=ZoneProgramCommands.Zones,
            _identifier=self.zone_program.zone.ad,
            _status_code=Message.low_flow_shutdown_by_flow_station,
            _data_required_string=data_required_string)

    #################################
    def clear_shutdown_on_low_flow_by_flowstation_message(self):
        self.update_values()
        data_required_string = "{0}={1},{2}={3}".format(
            ZoneProgramCommands.Programs,
            self.zone_program.program.ad,
            opcodes.device_value,
            self.zone_program.zone.sn,
        )
        self.send_message(
            _action=ActionCommands.DO,
            _category=ZoneProgramCommands.Zones,
            _identifier=self.zone_program.zone.ad,
            _status_code=Message.low_flow_shutdown_by_flow_station,
            _data_required_string=data_required_string)

    def check_for_shutdown_on_low_flow_by_flowstation_message_not_present(self):
        self.update_values()
        try:
            data_required_string = "{0}={1},{2}={3}".format(
                ZoneProgramCommands.Programs,
                self.zone_program.program.ad,
                opcodes.device_value,
                self.zone_program.zone.sn,
            )
            self.send_message(
                _action=ActionCommands.DO,
                _category=ZoneProgramCommands.Zones,
                _identifier=self.zone_program.zone.ad,
                _status_code=Message.low_flow_shutdown_by_flow_station,
                _data_required_string=data_required_string)

        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_shutdown_on_low_flow_by_flowstation_message(self):
        self.get_shutdown_on_low_flow_by_flowstation_message()
        self.verify_message(_status_code=Message.low_flow_shutdown_by_flow_station)

    # |--------------------------------------------------------------|
    #         Message: Zone Program Shutdown on High Flow Variance
    # |--------------------------------------------------------------|
    #################################
    def set_shutdown_on_high_flow_variance_message(self):
        self.update_values()
        data_required_string = "{0}={1},{2}={3},{4}={5},{6}={7},{8}={9}".format(
            ZoneProgramCommands.PointsOfControl,
            self.point_of_control.ad,
            ZoneProgramCommands.Programs,
            self.zone_program.program.ad,
            opcodes.device_value,
            self.zone_program.zone.sn,
            opcodes.variable_1,
            self.zone_program.zone.df,
            opcodes.variable_2,
            self.flow_meter.bicoder.vr
        )
        self.send_message(
            _action=ActionCommands.SET,
            _category=ZoneProgramCommands.Zones,
            _identifier=self.zone_program.zone.ad,
            _status_code=Message.high_flow_variance_shutdown,
            _data_required_string=data_required_string)

    #################################
    def get_shutdown_on_high_flow_variance_message(self):
        self.update_values()
        data_required_string = "{0}={1},{2}={3},{4}={5}".format(
            ZoneProgramCommands.PointsOfControl,
            self.point_of_control.ad,
            ZoneProgramCommands.Programs,
            self.zone_program.program.ad,
            opcodes.device_value,
            self.zone_program.zone.sn,
        )
        self.get_message(
            _action=ActionCommands.GET,
            _category=ZoneProgramCommands.Zones,
            _identifier=self.zone_program.zone.ad,
            _status_code=Message.high_flow_variance_shutdown,
            _data_required_string=data_required_string)

    #################################
    def clear_shutdown_on_high_flow_variance_message(self):
        self.update_values()
        data_required_string = "{0}={1},{2}={3},{4}={5}".format(
            ZoneProgramCommands.PointsOfControl,
            self.point_of_control.ad,
            ZoneProgramCommands.Programs,
            self.zone_program.program.ad,
            opcodes.device_value,
            self.zone_program.zone.sn,
        )
        self.send_message(
            _action=ActionCommands.DO,
            _category=ZoneProgramCommands.Zones,
            _identifier=self.zone_program.zone.ad,
            _status_code=Message.high_flow_variance_shutdown,
            _data_required_string=data_required_string)

    def check_for_shutdown_on_high_flow_variance_message_not_present(self):
        self.update_values()
        try:
            data_required_string = "{0}={1},{2}={3},{4}={5}".format(
                ZoneProgramCommands.PointsOfControl,
                self.point_of_control.ad,
                ZoneProgramCommands.Programs,
                self.zone_program.program.ad,
                opcodes.device_value,
                self.zone_program.zone.sn,
            )
            self.send_message(
                _action=ActionCommands.DO,
                _category=ZoneProgramCommands.Zones,
                _identifier=self.zone_program.zone.ad,
                _status_code=Message.high_flow_variance_shutdown,
                _data_required_string=data_required_string)

        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_shutdown_on_high_flow_variance_message(self):
        self.get_shutdown_on_high_flow_variance_message()
        self.verify_message(_status_code=Message.high_flow_variance_shutdown)

    # |--------------------------------------------------------------|
    #         Message: Zone Program Shutdown on High Flow Variance
    # |--------------------------------------------------------------|
    #################################
    def set_shutdown_on_low_flow_variance_message(self):
        self.update_values()
        data_required_string = "{0}={1},{2}={3},{4}={5},{6}={7},{8}={9}".format(
            ZoneProgramCommands.PointsOfControl,
            self.point_of_control.ad,
            ZoneProgramCommands.Programs,
            self.zone_program.program.ad,
            opcodes.device_value,
            self.zone_program.zone.sn,
            opcodes.variable_1,
            self.zone_program.zone.df,
            opcodes.variable_2,
            self.flow_meter.bicoder.vr
        )
        self.send_message(
            _action=ActionCommands.SET,
            _category=ZoneProgramCommands.Zones,
            _identifier=self.zone_program.zone.ad,
            _status_code=Message.low_flow_variance_shutdown,
            _data_required_string=data_required_string)

    #################################
    def get_shutdown_on_low_flow_variance_message(self):
        self.update_values()
        data_required_string = "{0}={1},{2}={3},{4}={5}".format(
            ZoneProgramCommands.PointsOfControl,
            self.point_of_control.ad,
            ZoneProgramCommands.Programs,
            self.zone_program.program.ad,
            opcodes.device_value,
            self.zone_program.zone.sn,
        )
        self.get_message(
            _action=ActionCommands.GET,
            _category=ZoneProgramCommands.Zones,
            _identifier=self.zone_program.zone.ad,
            _status_code=Message.low_flow_variance_shutdown,
            _data_required_string=data_required_string)

    #################################
    def clear_shutdown_on_low_flow_variance_message(self):
        self.update_values()
        data_required_string = "{0}={1},{2}={3},{4}={5}".format(
            ZoneProgramCommands.PointsOfControl,
            self.point_of_control.ad,
            ZoneProgramCommands.Programs,
            self.zone_program.program.ad,
            opcodes.device_value,
            self.zone_program.zone.sn,
        )
        self.send_message(
            _action=ActionCommands.DO,
            _category=ZoneProgramCommands.Zones,
            _identifier=self.zone_program.zone.ad,
            _status_code=Message.low_flow_variance_shutdown,
            _data_required_string=data_required_string)

    def check_for_shutdown_on_low_flow_variance_message_not_present(self):
        self.update_values()
        try:
            data_required_string = "{0}={1},{2}={3},{4}={5}".format(
                ZoneProgramCommands.PointsOfControl,
                self.point_of_control.ad,
                ZoneProgramCommands.Programs,
                self.zone_program.program.ad,
                opcodes.device_value,
                self.zone_program.zone.sn,
            )
            self.send_message(
                _action=ActionCommands.DO,
                _category=ZoneProgramCommands.Zones,
                _identifier=self.zone_program.zone.ad,
                _status_code=Message.low_flow_variance_shutdown,
                _data_required_string=data_required_string)

        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_shutdown_on_low_flow_variance_message(self):
        self.get_shutdown_on_low_flow_variance_message()
        self.verify_message(_status_code=Message.low_flow_variance_shutdown)

    # |--------------------------------------------------------------------------------|
    #         Message: Zone Program Moisture Sensor Requires Soak Cycle
    # |--------------------------------------------------------------------------------|
    #################################
    def set_moisture_sensor_requires_soak_cycle_message(self):
        self.update_values()
        data_required_string = "{0}={1},{2}={3},{4}={5}".format(
            ZoneProgramCommands.Programs,
            self.zone_program.program.ad,
            opcodes.device_value,
            self.zone_program.zone.sn,
            opcodes.moisture_sensor,
            self.moisture_sensor.sn
        )
        self.send_message(
            _action=ActionCommands.SET,
            _category=ZoneProgramCommands.Zones,
            _identifier=self.zone_program.zone.ad,
            _status_code=Message.requires_soak_cycle,
            _data_required_string=data_required_string)

    #################################
    def get_moisture_sensor_requires_soak_cycle_message(self):
        self.update_values()
        data_required_string = "{0}={1},{2}={3},{4}={5}".format(
            ZoneProgramCommands.Programs,
            self.zone_program.program.ad,
            opcodes.device_value,
            self.zone_program.zone.sn,
            opcodes.moisture_sensor,
            self.moisture_sensor.sn
        )
        self.get_message(
            _action=ActionCommands.GET,
            _category=ZoneProgramCommands.Zones,
            _identifier=self.zone_program.zone.ad,
            _status_code=Message.requires_soak_cycle,
            _data_required_string=data_required_string)

    #################################
    def clear_moisture_sensor_requires_soak_cycle_message(self):
        self.update_values()
        data_required_string = "{0}={1},{2}={3},{4}={5}".format(
            ZoneProgramCommands.Programs,
            self.zone_program.program.ad,
            opcodes.device_value,
            self.zone_program.zone.sn,
            opcodes.moisture_sensor,
            self.moisture_sensor.sn
        )
        self.send_message(
            _action=ActionCommands.DO,
            _category=ZoneProgramCommands.Zones,
            _identifier=self.zone_program.zone.ad,
            _status_code=Message.requires_soak_cycle,
            _data_required_string=data_required_string)

    def check_for_moisture_sensor_requires_soak_cycle_message_not_present(self):
        self.update_values()
        try:
            data_required_string = "{0}={1},{2}={3},{4}={5}".format(
                ZoneProgramCommands.Programs,
                self.zone_program.program.ad,
                opcodes.device_value,
                self.zone_program.zone.sn,
                opcodes.moisture_sensor,
                self.moisture_sensor.sn
            )
            self.send_message(
                _action=ActionCommands.DO,
                _category=ZoneProgramCommands.Zones,
                _identifier=self.zone_program.zone.ad,
                _status_code=Message.requires_soak_cycle,
                _data_required_string=data_required_string)

        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_moisture_sensor_requires_soak_cycle_message(self):
        self.get_moisture_sensor_requires_soak_cycle_message()
        self.verify_message(_status_code=Message.requires_soak_cycle)
