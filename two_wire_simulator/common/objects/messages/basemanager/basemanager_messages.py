from common.imports import opcodes
from common.objects.messages.base_messages import BaseMessages
from common.imports.types import ActionCommands, BasemanagerCommands
from common.imports.types import Message

__author__ = 'Baseline'


class BasemanagerMessages(BaseMessages):

    #################################
    def __init__(self, _basemanager_object):
        """
        :param _basemanager_object:     Basemanager object that will be referenced throughout this module. \n
        :type _basemanager_object:      common.objects.base_classes.basemanager_connections.BaseManagerConnection \n
        """
        self.basemanager = _basemanager_object
        self.status_code = ''
        # Init parent class to inherit respective attributes
        BaseMessages.__init__(self, _controller=_basemanager_object.controller)
        # Data Required
        self.message_from_basemanager = ''


    #################################
    def build_message_header(self):
        message_header = '{0}: {1}n'.format(
            str('BaseManager'),
            str('Message')
        )
        return message_header

    #################################
    def build_message_tx(self, _status_code):
        """
        :param _status_code:
        :type _status_code: str

        """
        tx_msg = self.build_message_header()

        # Create the title and header for the basemanager (BaseManager)
        if _status_code is Message.connection_error:
            tx_msg += "{0}{1}{2}{3}{4}".format(
                "BaseManager Connect Failure:n",    # {0}
                "Unable to establish a networkn",   # {1}
                "connection to the BaseManagern",   # {2}
                "server = ",                         # {3}
                self.basemanager.ai                 # {4}
            )

        elif _status_code is Message.no_eto_data:
            tx_msg += "{0}{1}{2}{3}{4}".format(
                "Weather ETo Data Not Available:n",     # {0}
                "Updated ETo data not receivedn",       # {1}
                "from BaseManager.n",                   # {2}
                "Used Previous Date = ",                # {3}
                str(self.basemanager.controller.date_mngr.controller_datetime.date_string_for_controller()) + "n"  # {4}
                "Used Previous ETo = ",                 # {5}
                str(self.basemanager.controller.ei)     # {6}
            )
        elif _status_code is Message.message:
            tx_msg += "{0}".format(
                self.message_from_basemanager   # {0}
            )

        return tx_msg

    #################################
    def build_alarm_id(self, _status_code):
        """
        This return the alarm string.

        :param _status_code:
        :type _status_code:
        :return:
        :rtype:
        """
        # If the status code is 'CE' we have a 'NC' in the status
        if _status_code is Message.connection_error:
            alarm_id = '{0}_{1}_{2}'.format(
                str(701).zfill(3),                  # {0} basemanager id number
                BasemanagerCommands.BaseManager,    # {1}
                opcodes.no_connection               # {2} no connection status
            )
        else:
            alarm_id = '{0}_{1}_{2}'.format(
                str(701).zfill(3),                  # {0} basemanager id number
                BasemanagerCommands.BaseManager,    # {1}
                _status_code                        # {2}
            )
        return alarm_id

    # |-------------------------------------------------|
    #         Message: BaseManager Connection Error
    # |-------------------------------------------------|
    #################################
    def set_connection_error_number_message(self, ):
        required_data = "{0}={1}".format(opcodes.device_value, self.basemanager.ai)
        self.send_message(
            _action=ActionCommands.SET,
            _category=BasemanagerCommands.BaseManager,
            _identifier=None,
            _status_code=Message.connection_error,
            _data_required_string=required_data)

    #################################
    def get_connection_error_number_message(self, ):
        required_data = "{0}={1}".format(opcodes.device_value, self.basemanager.ai)
        self.get_message(
            _action=ActionCommands.GET,
            _category=BasemanagerCommands.BaseManager,
            _identifier=None,
            _status_code=Message.connection_error,
            _data_required_string=required_data)

    #################################
    def clear_connection_error_number_message(self):
        required_data = "{0}={1}".format(opcodes.device_value, self.basemanager.ai)
        self.send_message(
            _action=ActionCommands.DO,
            _category=BasemanagerCommands.BaseManager,
            _identifier=None,
            _status_code=Message.connection_error,
            _data_required_string=required_data)

    def check_for_connection_error_number_message_not_present(self):
        try:
            required_data = "{0}={1}".format(opcodes.device_value, self.basemanager.ai)
            self.send_message(
                _action=ActionCommands.DO,
                _category=BasemanagerCommands.BaseManager,
                _identifier=None,
                _status_code=Message.connection_error,
                _data_required_string=required_data)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_connection_error_number_message(self):
        self.get_connection_error_number_message()
        self.verify_message(_status_code=Message.connection_error)

    # |-------------------------------------------------|
    #        Message: No ETo data
    # |-------------------------------------------------|
    #################################
    def set_no_eto_data_message(self, ):
        required_data = "{0}={1},{2}={3},{4}={5}".format(
            opcodes.device_value,
            self.basemanager.ai,
            opcodes.variable_1,
            self.basemanager.controller.date_mngr.controller_datetime.date_string_for_controller(),
            opcodes.variable_2,
            self.basemanager.controller.ei
        )
        self.send_message(
            _action=ActionCommands.SET,
            _category=BasemanagerCommands.BaseManager,
            _identifier=None,
            _status_code=Message.no_eto_data,
            _data_required_string=required_data)

    #################################
    def get_no_eto_data_message(self, ):
        required_data = "{0}={1}".format(
            opcodes.device_value,
            self.basemanager.ai
        )
        self.get_message(
            _action=ActionCommands.GET,
            _category=BasemanagerCommands.BaseManager,
            _identifier=None,
            _status_code=Message.no_eto_data,
            _data_required_string=required_data)

    #################################
    def clear_no_eto_data_message(self):
        required_data = "{0}={1}".format(
            opcodes.device_value,
            self.basemanager.ai
        )
        self.send_message(
            _action=ActionCommands.DO,
            _category=BasemanagerCommands.BaseManager,
            _identifier=None,
            _status_code=Message.no_eto_data,
            _data_required_string=required_data)

    def check_for_no_eto_data_message_not_present(self):
        try:
            required_data = "{0}={1}".format(
                opcodes.device_value,
                self.basemanager.ai
            )
            self.send_message(
                _action=ActionCommands.DO,
                _category=BasemanagerCommands.BaseManager,
                _identifier=None,
                _status_code=Message.no_eto_data,
                _data_required_string=required_data)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")

    #################################
    def verify_no_eto_data_message(self):
        self.get_no_eto_data_message()
        self.verify_message(_status_code=Message.no_eto_data)


    # |-------------------------------------------------|
    #        Message: Message from basemanager
    # |-------------------------------------------------|
    #################################
    def set_message_from_basemanager_message(self, _message_from_basemanager):
        self.message_from_basemanager = _message_from_basemanager
        required_data = "{0}={1},{2}={3}".format(
            opcodes.device_value,
            self.basemanager.ai,
            opcodes.variable_1,
            self.message_from_basemanager
        )
        self.send_message(
            _action=ActionCommands.SET,
            _category=BasemanagerCommands.BaseManager,
            _identifier=None,
            _status_code=Message.message,
            _data_required_string=required_data)


    #################################
    def get_message_from_basemanager_message(self, ):
        required_data = "{0}={1}".format(
            opcodes.device_value,
            self.basemanager.ai
        )
        self.get_message(
            _action=ActionCommands.GET,
            _category=BasemanagerCommands.BaseManager,
            _identifier=None,
            _status_code=Message.message,
            _data_required_string=required_data)


    #################################
    def clear_message_from_basemanager_message(self):
        required_data = "{0}={1}".format(
            opcodes.device_value,
            self.basemanager.ai
        )
        self.send_message(
            _action=ActionCommands.DO,
            _category=BasemanagerCommands.BaseManager,
            _identifier=None,
            _status_code=Message.message,
            _data_required_string=required_data)

    #################################
    def check_for_message_from_basemanager_message_not_present(self):
        try:
            required_data = "{0}={1}".format(
                opcodes.device_value,
                self.basemanager.ai
            )
            self.send_message(
                _action=ActionCommands.DO,
                _category=BasemanagerCommands.BaseManager,
                _identifier=None,
                _status_code=Message.message,
                _data_required_string=required_data)
        except Exception as e:
            if e.message == "NM No Message Found":
                print("Message was to found on controller.")
            else:
                print("Was unable to verify if the message was gone.")


    #################################
    def verify_message_from_basemanager_message(self):
        self.get_message_from_basemanager_message()
        self.verify_message(_status_code=Message.message)
