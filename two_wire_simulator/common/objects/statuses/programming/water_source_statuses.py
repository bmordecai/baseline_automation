from common.imports import opcodes
from common.objects.statuses.base_statuses import BaseStatuses
from common.imports.types import ActionCommands, ControllerCommands
# from common.imports.types import Statuses
from datetime import datetime, timedelta

__author__ = 'Baseline'


class WaterSourceStatuses(BaseStatuses):

    #################################
    def __init__(self, _water_source_object):
        """
        :param _water_source_object:     Object this message is attached to. \n
        :type _water_source_object:      common.objects.programming.ws.WaterSource
        """
        self.water_source = _water_source_object
        # Init parent class to inherit respective attributes
        BaseStatuses.__init__(self, _controller=_water_source_object.controller,
                              _identifiers=[[opcodes.water_source, _water_source_object.ad]],
                              _object=_water_source_object)

    #################################
    def verify_status_is_disabled(self):
        if not self.status_is_disabled(_get_status=True):
            e_msg = "WaterSource: {0}: assigned to address '{1}' should be disabled, " \
                    "Status received was: {2}".format(
                        self.water_source.ds,       # {0}
                        str(self.water_source.ad),  # {1}
                        self.water_source.ss,       # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, WaterSource: {0}: assigned to address '{1}' is Disabled".format(
                self.water_source.ds,       # {0}
                str(self.water_source.ad),  # {1}

            ))

    #################################
    def verify_status_is_unassigned(self):
        if not self.status_is_unassigned(_get_status=True):
            e_msg = "WaterSource: {0}: assigned to address '{1}' should be unassigned, " \
                    "Status received was: {2}".format(
                        self.water_source.ds,       # {0}
                        str(self.water_source.ad),  # {1}
                        self.water_source.ss,       # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, WaterSource: {0}: assigned to address '{1}' is unassigned".format(
                self.water_source.ds,       # {0}
                str(self.water_source.ad),  # {1}
            ))

    #################################
    def verify_status_is_ok(self):
        if not self.status_is_ok(_get_status=True):
            e_msg = "WaterSource: {0}: assigned to address '{1}' should be OK, " \
                    "Status received was: {2}".format(
                        self.water_source.ds,       # {0}
                        str(self.water_source.ad),  # {1}
                        self.water_source.ss,       # {2}
                    )
            raise ValueError(e_msg)
        else:
            print(
                "Verified, WaterSource: {0}: assigned to address '{1}' is OK".format(
                    self.water_source.ds,       # {0}
                    str(self.water_source.ad),  # {1}
                  ))

    #################################
    def verify_status_is_over_budget(self):
        if not self.status_is_over_budget(_get_status=True):
            e_msg = "Point Of Control: {0}: assigned to address '{1}' should be Over Budget, " \
                    "Status received was: {2}".format(
                        self.water_source.ds,       # {0}
                        str(self.water_source.ad),  # {1}
                        self.water_source.ss,       # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Point Of Control: {0}: assigned to address '{1}' is Over Budget".format(
                self.water_source.ds,       # {0}
                str(self.water_source.ad),  # {1}
                ))

    #################################
    def verify_status_is_running(self):
        if not self.status_is_running(_get_status=True):
            e_msg = "WaterSource: {0}: assigned to address '{1}' should be Running, " \
                    "Status received was: {2}".format(
                        self.water_source.ds,       # {0}
                        str(self.water_source.ad),  # {1}
                        self.water_source.ss,       # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, WaterSource: {0}: assigned to address '{1}' is Running".format(
                self.water_source.ds,           # {0}
                str(self.water_source.ad),      # {1}
            ))

    #################################
    def verify_status_is_water_empty(self):
        if not self.status_is_water_empty(_get_status=True):
            e_msg = "WaterSource: {0}: assigned to address '{1}' should be Water Empty, " \
                    "Status received was: {2}".format(
                        self.water_source.ds,       # {0}
                        str(self.water_source.ad),  # {1}
                        self.water_source.ss,       # {2}
                    )
            raise ValueError(e_msg)
        else:
            print(
                "Verified, WaterSource: {0}: assigned to address '{1}' is Water Empty".format(
                    self.water_source.ds,       # {0}
                    str(self.water_source.ad),  # {1}
                    ))

    #################################
    def verify_status_is_error(self):
        if not self.status_is_error(_get_status=True):
            e_msg = "WaterSource: {0}: assigned to address '{1}' should be Error, " \
                    "Status received was: {2}".format(
                        self.water_source.ds,       # {0}
                        str(self.water_source.ad),  # {1}
                        self.water_source.ss,       # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, WaterSource: {0}: assigned to address '{1}' is Error".format(
                self.water_source.ds,       # {0}
                str(self.water_source.ad),  # {1}
                ))
