from common.imports import opcodes
from common.objects.statuses.base_statuses import BaseStatuses
from common.imports.types import ActionCommands, ControllerCommands
# from common.imports.types import Statuses
from datetime import datetime, timedelta

__author__ = 'Baseline'


class PointOfControlStatuses(BaseStatuses):

    #################################
    def __init__(self, _point_of_control_object):
        """
        :param _point_of_control_object:    Object this message is attached to. \n
        :type _point_of_control_object:     common.objects.base_classes.poc.BasePOC |
                                            common.objects.programming.point_of_control.PointOfControl
        """
        self.point_of_control = _point_of_control_object
        # Init parent class to inherit respective attributes
        BaseStatuses.__init__(self, _controller=_point_of_control_object.controller,
                              _identifiers=[[opcodes.point_of_control, _point_of_control_object.ad]],
                              _object=_point_of_control_object)

    #################################
    def verify_status_is_disabled(self):
        if not self.status_is_disabled(_get_status=True):
            e_msg = "Point Of Control: {0}: assigned to address '{1}' should be disabled, " \
                    "Status received was: {2}".format(
                        self.point_of_control.ds,       # {0}
                        str(self.point_of_control.ad),  # {1}
                        self.point_of_control.ss,       # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Point Of Control: {0}: assigned to address '{1}' is Disabled".format(
                self.point_of_control.ds,       # {0}
                str(self.point_of_control.ad),  # {1}

            ))

    #################################
    def verify_status_is_unassigned(self):
        if not self.status_is_unassigned(_get_status=True):
            e_msg = "Point Of Control: {0}: assigned to address '{1}' should be unassigned, " \
                    "Status received was: {2}".format(
                        self.point_of_control.ds,       # {0}
                        str(self.point_of_control.ad),  # {1}
                        self.point_of_control.ss,       # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Point Of Control: {0}: assigned to address '{1}' is unassigned".format(
                self.point_of_control.ds,       # {0}
                str(self.point_of_control.ad),  # {1}
            ))

    #################################
    def verify_status_is_running(self):
        if not self.status_is_running(_get_status=True):
            e_msg = "Point Of Control: {0}: assigned to address '{1}' should be Running, " \
                    "Status received was: {2}".format(
                        self.point_of_control.ds,       # {0}
                        str(self.point_of_control.ad),  # {1}
                        self.point_of_control.ss,       # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Point Of Control: {0}: assigned to address '{1}' is Running".format(
                self.point_of_control.ds,       # {0}
                str(self.point_of_control.ad),  # {1}
            ))

    #################################
    def verify_status_is_learning_flow(self):
        if not self.status_is_learning_flow(_get_status=True):
            e_msg = "Point of Control : {0}: assigned to address '{1}' should be Learning " \
                    "Flow Status received was: {2}".format(
                        self.point_of_control.ds,       # {0}
                        str(self.point_of_control.ad),  # {1}
                        self.point_of_control.ss,       # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Point of Control: {0}: assigned to address '{1}' is Learning Flow".format(
                self.point_of_control.ds,       # {0}
                str(self.point_of_control.ad),  # {1}

            ))

    #################################
    def verify_status_is_paused(self):
        if not self.status_is_paused(_get_status=True):
            e_msg = "Point of Control: {0}: assigned to address '{1}' should be Paused, " \
                    "Status received was: {2}".format(
                        self.point_of_control.ds,       # {0}
                        str(self.point_of_control.ad),  # {1}
                        self.point_of_control.ss,       # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Point of Control: {0}: assigned to address '{1}' is Paused".format(
                self.point_of_control.ds,       # {0}
                str(self.point_of_control.ad),  # {1}
                self.point_of_control.ss,       # {2}
            ))

    #################################
    def verify_status_is_off(self):
        if not self.status_is_off(_get_status=True):
            e_msg = "Point Of Control: {0}: assigned to address '{1}' should be Off, " \
                    "Status received was: {2}".format(
                        self.point_of_control.ds,       # {0}
                        str(self.point_of_control.ad),  # {1}
                        self.point_of_control.ss,       # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Point Of Control: {0}: assigned to address '{1}' is Off".format(
                self.point_of_control.ds,       # {0}
                str(self.point_of_control.ad),  # {1}
            ))

    #################################
    def verify_status_is_error(self):
        if not self.status_is_error(_get_status=True):
            e_msg = "Point Of Control: {0}: assigned to address '{1}' should be Error, " \
                    "Status received was: {2}".format(
                        self.point_of_control.ds,       # {0}
                        str(self.point_of_control.ad),  # {1}
                        self.point_of_control.ss,       # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Point Of Control: {0}: assigned to address '{1}' is Error".format(
                self.point_of_control.ds,       # {0}
                str(self.point_of_control.ad),  # {1}
                ))

    #################################
    def verify_status_is_flow_fault(self):
        if not self.status_is_flow_jumper_fault(_get_status=True):
            e_msg = "Point Of Control: {0}: assigned to address '{1}' should be flow fault, " \
                    "Status received was: {2}".format(
                        self.point_of_control.ds,       # {0}
                        str(self.point_of_control.ad),  # {1}
                        self.point_of_control.ss,       # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Point Of Control: {0}: assigned to address '{1}' is flow fault".format(
                self.point_of_control.ds,  # {0}
                str(self.point_of_control.ad),  # {1}
            ))

    #################################
    def verify_status_is_pressure_fault(self):
        if not self.status_is_pressure_fault(_get_status=True):
            e_msg = "Point Of Control: {0}: assigned to address '{1}' should be Pressure Fault, " \
                    "Status received was: {2}".format(
                        self.point_of_control.ds,       # {0}
                        str(self.point_of_control.ad),  # {1}
                        self.point_of_control.ss,       # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Point Of Control: {0}: assigned to address '{1}' is Pressure Fault".format(
                self.point_of_control.ds,       # {0}
                str(self.point_of_control.ad),  # {1}
                ))