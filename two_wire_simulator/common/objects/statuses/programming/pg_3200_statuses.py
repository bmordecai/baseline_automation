from common.imports import opcodes
from common.objects.statuses.base_statuses import BaseStatuses
from common.imports.types import ActionCommands, ControllerCommands
# from common.imports.types import Statuses
from datetime import datetime, timedelta

__author__ = 'Baseline'


class ProgramStatuses(BaseStatuses):

    #################################
    def __init__(self, _program_object):
        """
        :param _program_object:     Object this message is attached to. \n
        :type _program_object:      common.objects.programming.pg_3200.PG3200
        """
        self.program = _program_object
        self.status_code = ''
        # Init parent class to inherit respective attributes
        BaseStatuses.__init__(self, _controller=_program_object.controller,
                              _identifiers=[[opcodes.program, _program_object.ad]],
                              _object=_program_object)

    #################################
    def verify_status_is_disabled(self):
        if not self.status_is_disabled(_get_status=True):
            e_msg = "Program: {0}: assigned to address '{1}' should be Disabled, " \
                    "Status received was: {2}".format(
                        self.program.ds,          # {0}
                        str(self.program.ad),     # {1}
                        self.program.ss,          # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Program: {0}: assigned to address '{1}' is Disabled".format(
                    self.program.ds,          # {0}
                    str(self.program.ad),     # {1}
                  ))

    #################################
    def verify_status_is_unassigned(self):
        if not self.status_is_unassigned(_get_status=True):
            e_msg = "Program: {0}: assigned to address '{1}' should be Unassigned, " \
                    "Status received was: {2}".format(
                        self.program.ds,          # {0}
                        str(self.program.ad),     # {1}
                        self.program.ss,          # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Program: {0}: assigned to address '{1}' is Unassigned".format(
                    self.program.ds,          # {0}
                    str(self.program.ad),     # {1}
                  ))

    #################################
    def verify_status_is_done(self):
        if not self.status_is_done_watering(_get_status=True):
            e_msg = "Program: {0}: assigned to address '{1}' should be Done Watering, " \
                    "Status received was: {2}".format(
                        self.program.ds,          # {0}
                        str(self.program.ad),     # {1}
                        self.program.ss,          # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Program: {0}: assigned to address '{1}' is Done Watering".format(
                    self.program.ds,          # {0}
                    str(self.program.ad),     # {1}
                  ))

    #################################
    def verify_status_is_manually_running(self):
        if not self.status_is_manually_running(_get_status=True):
            e_msg = "Program: {0}: assigned to address '{1}' should be Manually Running , " \
                    "Status received was: {2}".format(
                        self.program.ds,          # {0}
                        str(self.program.ad),     # {1}
                        self.program.ss,          # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Program: {0}: assigned to address '{1}' is Manually Running".format(
                    self.program.ds,          # {0}
                    str(self.program.ad),     # {1}
                  ))

    #################################
    def verify_status_is_running(self):
        if not self.status_is_running(_get_status=True):
            e_msg = "Program: {0}: assigned to address '{1}' should be Running, " \
                    "Status received was: {2}".format(
                        self.program.ds,          # {0}
                        str(self.program.ad),     # {1}
                        self.program.ss,          # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Program: {0}: assigned to address '{1}' is Running".format(
                    self.program.ds,          # {0}
                    str(self.program.ad),     # {1}
                  ))

    #################################
    def verify_status_is_learning_flow(self):
        if not self.status_is_learning_flow(_get_status=True):
            e_msg = "Program: {0}: assigned to address '{1}' should be Learning Flow, " \
                    "Status received was: {2}".format(
                        self.program.ds,          # {0}
                        str(self.program.ad),     # {1}
                        self.program.ss,          # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Program: {0}: assigned to address '{1}' is Learning Flow".format(
                    self.program.ds,          # {0}
                    str(self.program.ad),     # {1}
                  ))

    #################################
    def verify_status_is_waiting_to_run(self):
        if not self.status_is_waiting_to_water(_get_status=True):
            e_msg = "Program: {0}: assigned to address '{1}' should be Waiting to Run, " \
                    "Status received was: {2}".format(
                        self.program.ds,          # {0}
                        str(self.program.ad),     # {1}
                        self.program.ss,          # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Program: {0}: assigned to address '{1}' is Waiting to Run".format(
                    self.program.ds,          # {0}
                    str(self.program.ad),     # {1}
                  ))

    #################################
    def verify_status_is_paused(self):
        if not self.status_is_paused(_get_status=True):
            e_msg = "Program: {0}: assigned to address '{1}' should be Paused, " \
                    "Status received was: {2}".format(
                        self.program.ds,          # {0}
                        str(self.program.ad),     # {1}
                        self.program.ss,          # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Program: {0}: assigned to address '{1}' is Paused".format(
                    self.program.ds,          # {0}
                    str(self.program.ad),     # {1}
                  ))

        #################################
    def verify_status_is_error(self):
        if not self.status_is_error(_get_status=True):
            e_msg = "Program: {0}: assigned to address '{1}' should be Error, " \
                    "Status received was: {2}".format(
                        self.program.ds,          # {0}
                        str(self.program.ad),     # {1}
                        self.program.ss,          # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Program: {0}: assigned to address '{1}' is Error".format(
                    self.program.ds,          # {0}
                    str(self.program.ad),     # {1}
                  ))

