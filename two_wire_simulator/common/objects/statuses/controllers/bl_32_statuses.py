from common.imports import opcodes
from common.objects.statuses.base_statuses import BaseStatuses
from common.imports.types import ActionCommands, ControllerCommands, BasemanagerCommands
# from common.imports.types import Statuses
from datetime import datetime, timedelta

__author__ = 'Baseline'


class BL3200Statuses(BaseStatuses):

    #################################
    def __init__(self, _controller_object):
        """
        :param _controller_object:     Controller object that will be referenced throughout this module. \n
        :type _controller_object:      common.objects.controllers.bl_32.BaseStation3200 \n
        """
        self.bl_32 = _controller_object
        # Init parent class to inherit respective attributes
        BaseStatuses.__init__(self, _controller=_controller_object,
                              _identifiers=[[opcodes.controller, _controller_object.sn]],
                              _object=_controller_object)

    #################################
    def verify_status_is_ok(self):
        if not self.status_is_ok(_get_status=True):
            e_msg = "BaseStation 3200: {0}: assigned to address '{1}'  with serial number '{2}' should be OK, " \
                    "Status received was: {3}".format(
                        self.bl_32.ds,          # {0}
                        str(self.bl_32.ad),     # {1}
                        self.bl_32.sn,          # {2}
                        self.bl_32.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, BaseStation 3200: {0}: assigned to address '{1}'  with serial number '{2}'  is OK".format(
                self.bl_32.ds,          # {0}
                str(self.bl_32.ad),     # {1}
                self.bl_32.sn,          # {2}
            ))

    #################################
    def verify_status_is_running(self):
        if not self.status_is_running(_get_status=True):
            e_msg = "BaseStation 3200: {0}: assigned to address '{1}'  with serial number '{2}' should be Running, " \
                    "Status received was: {3}".format(
                        self.bl_32.ds,          # {0}
                        str(self.bl_32.ad),     # {1}
                        self.bl_32.sn,          # {2}
                        self.bl_32.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, BaseStation 3200: {0}: assigned to address '{1}'  with serial number '{2}'  "
                  "is Running".format(
                    self.bl_32.ds,          # {0}
                    str(self.bl_32.ad),     # {1}
                    self.bl_32.sn,          # {2}
                  ))

    #################################
    def verify_status_is_paused(self):
        if not self.status_is_paused(_get_status=True):
            e_msg = "BaseStation 3200: {0}: assigned to address '{1}'  with serial number '{2}' should be Paused, " \
                    "Status received was: {3}".format(
                        self.bl_32.ds,          # {0}
                        str(self.bl_32.ad),     # {1}
                        self.bl_32.sn,          # {2}
                        self.bl_32.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, BaseStation 3200: {0}: assigned to address '{1}'  with serial number '{2}'  is "
                  "Paused".format(
                        self.bl_32.ds,          # {0}
                        str(self.bl_32.ad),     # {1}
                        self.bl_32.sn,          # {2}
                    ))

    #################################
    def verify_status_is_rain_delay(self):
        if not self.status_is_rain_delay(_get_status=True):
            e_msg = "BaseStation 3200: {0}: assigned to address '{1}'  with serial number '{2}' should in Rain Delay" \
                    ", Status received was: {3}".format(
                        self.bl_32.ds,          # {0}
                        str(self.bl_32.ad),     # {1}
                        self.bl_32.sn,          # {2}
                        self.bl_32.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, BaseStation 3200: {0}: assigned to address '{1}'  with serial number '{2}' "
                  "is in Rain Delay".format(
                    self.bl_32.ds,          # {0}
                    str(self.bl_32.ad),     # {1}
                    self.bl_32.sn,          # {2}
                  ))

    #################################
    def verify_status_is_rain_jumper_triggered(self):
        if not self.status_is_rain_jumper(_get_status=True):
            e_msg = "BaseStation 3200: {0}: assigned to address '{1}'  with serial number '{2}' should be Rain Jumper" \
                    "Triggered, Status received was: {3}".format(
                        self.bl_32.ds,          # {0}
                        str(self.bl_32.ad),     # {1}
                        self.bl_32.sn,          # {2}
                        self.bl_32.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, BaseStation 3200: {0}: assigned to address '{1}'  with serial number '{2}' Rain Jumper "
                  "Triggered".format(
                    self.bl_32.ds,          # {0}
                    str(self.bl_32.ad),     # {1}
                    self.bl_32.sn,          # {2}
                  ))

    #################################
    def verify_status_is_off(self):
        if not self.status_is_off(_get_status=True):
            e_msg = "BaseStation 3200: {0}: assigned to address '{1}'  with serial number '{2}' should be Off, " \
                    "Status received was: {3}".format(
                        self.bl_32.ds,          # {0}
                        str(self.bl_32.ad),     # {1}
                        self.bl_32.sn,          # {2}
                        self.bl_32.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, BaseStation 3200: {0}: assigned to address '{1}'  with serial number '{2}' is Off".format(
                self.bl_32.ds,          # {0}
                str(self.bl_32.ad),     # {1}
                self.bl_32.sn,          # {2}
            ))

    #################################
    def verify_status_is_error(self):
        if not self.status_is_error(_get_status=True):
            e_msg = "BaseStation 3200: {0}'s should be in error, Status received was: {1}".format(
                str(self.bl_32.ad),  # {0}
                self.bl_32.ss,  # {1}
            )
            raise ValueError(e_msg)
        else:
            print("Verified BaseStation 3200 {0}'s is error".format(
                str(self.bl_32.ad),  # {0}
            ))

    #################################
    def verify_status_is_flow_jumper_fault(self):
        if not self.status_is_flow_jumper_fault(_get_status=True):
            e_msg = "BaseStation 3200: {0}: assigned to address '{1}' should be flow jumper fault, " \
                    "Status received was: {2}".format(
                        self.bl_32.ds,          # {0}
                        str(self.bl_32.ad),     # {1}
                        self.bl_32.ss,          # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, BaseStation 3200: {0}: assigned to address '{1}' is flow jumper fault".format(
                self.bl_32.ds,          # {0}
                str(self.bl_32.ad),     # {1}
            ))

    #################################
    def verify_connected_to_substation(self, _substation_number):
        """
        Verifies the substation connection against an expected status.

        :return:
        :rtype: bool
        """
        _expected_status = opcodes.connected

        command = "{0},{1}={2}".format(
            ActionCommands.GET,                     # {0}
            ControllerCommands.Type.SUBSTATION,     # {1}
            str(_substation_number)                 # {2}
        )

        try:
            data = self.ser.get_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to get Controller's 'Substation Connection Status' for " \
                    "Substation: '{0}'.".format(str(_substation_number))
            raise Exception(e_msg)

        else:
            sb_ss_on_cn = data.get_value_string_by_key(opcodes.status_code)

            if _expected_status != sb_ss_on_cn:
                e_msg = "Exception occurred trying to verify Controller's 'Substation Connection Status' for " \
                        "Substation: '{0}'. Expected Status: '{1}', Received: '{2}'.".format(
                            str(_substation_number),
                            _expected_status,
                            sb_ss_on_cn
                        )
                raise ValueError(e_msg)
            else:
                print "Successfully verified Controller's 'Substation Connection Status' for Substation: '{0}', " \
                      "Status: '{1}'.".format(str(_substation_number), _expected_status)
                return True

    #################################
    def verify_not_connected_to_substation(self, _substation_number):
        """
        Verifies the substation connection against an expected status.

        :return:
        :rtype: bool
        """
        _expected_status = opcodes.disconnected

        command = "{0},{1}={2}".format(
            ActionCommands.GET,                     # {0}
            ControllerCommands.Type.SUBSTATION,     # {1}
            str(_substation_number)                 # {2}
        )

        try:
            data = self.ser.get_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to get Controller's 'Substation Connection Status' for " \
                    "Substation: '{0}'.".format(str(_substation_number))
            raise Exception(e_msg)

        else:
            sb_ss_on_cn = data.get_value_string_by_key(opcodes.status_code)

            if _expected_status != sb_ss_on_cn:
                e_msg = "Exception occurred trying to verify Controller's 'Substation Connection Status' for " \
                        "Substation: '{0}'. Expected Status: '{1}', Received: '{2}'.".format(
                            str(_substation_number),
                            _expected_status,
                            sb_ss_on_cn
                        )
                raise ValueError(e_msg)
            else:
                print "Successfully verified Controller's 'Substation Connection Status' for Substation: '{0}', " \
                      "Status: '{1}'.".format(str(_substation_number), _expected_status)
                return True

    #################################
    def verify_connected_to_flowstation(self):
        """
        Verifies the Controller connection status to the FlowStation against an expected status.

        :return:
        :rtype: bool
        """

        _expected_status = opcodes.connected
        command = "{0},{1}={2}".format(
            ActionCommands.GET,                     # {0}
            ControllerCommands.Type.FLOWSTATION,    # {1}
            "1"                                     # {2}
        )

        try:
            data = self.ser.get_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to get Controller's 'FlowStation Connection Status' for " \
                    "FlowStation: '{0}'.".format("1")
            raise Exception(e_msg)

        else:
            sb_ss_on_cn = data.get_value_string_by_key(opcodes.status_code)

            if _expected_status != sb_ss_on_cn:
                e_msg = "Exception occurred trying to verify Controller's 'FlowStation Connection Status' for " \
                        "Substation: '{0}'. Expected Status: '{1}', Received: '{2}'.".format(
                            "1",
                            _expected_status,
                            sb_ss_on_cn
                        )
                raise ValueError(e_msg)
            else:
                print "Successfully verified Controller's ({0})'FlowStation Connection Status' for BL3200: '{1}', " \
                      "Status: '{2}'.".format(self.bl_32.mac, self.bl_32.ser.s_port, _expected_status)
                return True

    #################################
    def verify_not_connected_to_flowstation(self):
        """
        Verifies the Controller connection status to the FlowStation against an expected status.

        :return:
        :rtype: bool
        """
        if self.ty in [ControllerCommands.Type.BASESTATION_1000, ControllerCommands.Type.BASESTATION_3200]:
            _expected_status = opcodes.disconnected
            command = "{0},{1}={2}".format(
                ActionCommands.GET,                     # {0}
                ControllerCommands.Type.FLOWSTATION,    # {1}
                "1"                                     # {2}
            )

            try:
                data = self.ser.get_and_wait_for_reply(tosend=command)
            except Exception:
                e_msg = "Exception occurred trying to get Controller's 'FlowStation Connection Status' for " \
                        "Substation: '{0}'.".format("1")
                raise Exception(e_msg)

            else:
                sb_ss_on_cn = data.get_value_string_by_key(opcodes.status_code)

                if _expected_status != sb_ss_on_cn:
                    e_msg = "Exception occurred trying to verify Controller's 'FlowStation Connection Status' for " \
                            "Substation: '{0}'. Expected Status: '{1}', Received: '{2}'.".format(
                                "1",
                                _expected_status,
                                sb_ss_on_cn
                            )
                    raise ValueError(e_msg)
                else:
                    print "Successfully verified Controller's 'FlowStation Connection Status' for Substation: '{0}', " \
                          "Status: '{1}'.".format("1", _expected_status)
                    return True

        else:
            raise ValueError(
                "Attempting to verify 'Connection from Controller to FlowStation' on a Substation which "
                "is not supported.")

    #################################
    def verify_connected_to_basemanager(self):
        """
        Verifies the Controller connection status to BaseManager against an expected status.

        :return:
        :rtype: bool
        """
        if self.bl_32.ty in [ControllerCommands.Type.BASESTATION_1000, ControllerCommands.Type.BASESTATION_3200,
                             ControllerCommands.Type.FLOWSTATION]:
            _expected_status = opcodes.connected
            command = "{0},{1}".format(
                ActionCommands.GET,                 # {0}
                BasemanagerCommands.BaseManager,    # {1}
            )

            try:
                data = self.ser.get_and_wait_for_reply(tosend=command)
            except Exception:
                e_msg = "Exception occurred trying to get Controller's ({0}) 'BaseManager Connection Status'".format(
                    self.bl_32.mac
                )
                raise Exception(e_msg)

            else:
                sb_ss_on_cn = data.get_value_string_by_key(opcodes.status_code)

                if _expected_status != sb_ss_on_cn:
                    e_msg = "Exception occurred trying to verify Controller's 'BaseManager Connection Status' for " \
                            "BaseStation3200: '{0}'. Expected Status: '{1}', Received: '{2}'.".format(
                                str(self.bl_32.mac),
                                _expected_status,
                                sb_ss_on_cn
                            )
                    raise ValueError(e_msg)
                else:
                    print "Successfully verified Controller's 'BaseManager Connection Status' for BaseStation3200: " \
                          "'{0}', Status: '{1}'.".format(str(self.bl_32.mac), _expected_status)
                    return True

        else:
            raise ValueError(
                "Attempting to verify 'Connection from Controller to BaseManager' on a Substation which "
                "is not supported.")

    #################################
    def verify_not_connected_to_basemanager(self):
        """
        Verifies the Controller connection status to the BaseManager against an expected status.

        :return:
        :rtype: bool
        """
        if self.bl_32.ty in [ControllerCommands.Type.BASESTATION_1000, ControllerCommands.Type.BASESTATION_3200,
                             ControllerCommands.Type.FLOWSTATION]:
            _expected_status = opcodes.disconnected
            command = "{0},{1}".format(
                ActionCommands.GET,                 # {0}
                BasemanagerCommands.BaseManager,    # {1}
            )

            try:
                data = self.ser.get_and_wait_for_reply(tosend=command)
            except Exception:
                e_msg = "Exception occurred trying to get Controller's ({0}) 'BaseManager Connection Status'".format(
                    self.bl_32.mac
                )
                raise Exception(e_msg)

            else:
                sb_ss_on_cn = data.get_value_string_by_key(opcodes.status_code)

                if _expected_status != sb_ss_on_cn:
                    e_msg = "Exception occurred trying to verify Controller's 'BaseManager Connection Status' for " \
                            "BaseStation3200: '{0}'. Expected Status: '{1}', Received: '{2}'.".format(
                                str(self.bl_32.mac),
                                _expected_status,
                                sb_ss_on_cn
                            )
                    raise ValueError(e_msg)
                else:
                    print "Successfully verified Controller's 'BaseManager Connection Status' for BaseStation3200: " \
                          "'{0}', Status: '{1}'.".format(str(self.bl_32.mac), _expected_status)
                    return True

        else:
            raise ValueError(
                "Attempting to verify 'Connection from Controller to BaseManager' on a Substation which "
                "is not supported.")
