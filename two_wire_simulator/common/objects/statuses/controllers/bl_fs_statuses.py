from common.imports import opcodes
from common.objects.statuses.base_statuses import BaseStatuses
from common.imports.types import ActionCommands, ControllerCommands, BasemanagerCommands
# from common.imports.types import Statuses
from datetime import datetime, timedelta

__author__ = 'Baseline'


class FlowStationStatuses(BaseStatuses):

    #################################
    def __init__(self, _controller_object):
        """
        :param _controller_object:     Controller object that will be referenced throughout this module. \n
        :type _controller_object:      common.objects.controllers.bl_fs.FlowStation \n
        """
        self.bl_fs = _controller_object
        # Init parent class to inherit respective attributes
        BaseStatuses.__init__(self, _controller=_controller_object,
                              _identifiers=[[opcodes.controller, _controller_object.sn]],
                              _object=_controller_object)

    #################################
    def verify_status_is_ok(self):
        if not self.status_is_ok(_get_status=True):
            e_msg = "FlowStation: {0}: with mac address '{1}' and serial number '{2}' should be OK, " \
                    "Status received was: {3}".format(
                        self.bl_fs.ds,          # {0}
                        str(self.bl_fs.mac),    # {1}
                        self.bl_fs.sn,          # {2}
                        self.bl_fs.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, FlowStation: {0}: with mac address '{1}' and serial number '{2}'  is OK".format(
                self.bl_fs.ds,          # {0}
                str(self.bl_fs.mac),    # {1}
                self.bl_fs.sn,          # {2}
            ))

    #################################
    def verify_status_is_running(self):
        if not self.status_is_running(_get_status=True):
            e_msg = "FlowStation: {0}: with mac address '{1}' and serial number '{2}' should be Running, " \
                    "Status received was: {3}".format(
                        self.bl_fs.ds,          # {0}
                        str(self.bl_fs.mac),    # {1}
                        self.bl_fs.sn,          # {2}
                        self.bl_fs.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, FlowStation: {0}: with mac address '{1}' and serial number '{2}' is Running".format(
                    self.bl_fs.ds,          # {0}
                    str(self.bl_fs.mac),    # {1}
                    self.bl_fs.sn,          # {2}
                  ))

    #################################
    def verify_status_is_paused(self):
        if not self.status_is_paused(_get_status=True):
            e_msg = "FlowStation: {0}: with mac address '{1}' and serial number '{2}' should be Paused, " \
                    "Status received was: {3}".format(
                        self.bl_fs.ds,          # {0}
                        str(self.bl_fs.mac),    # {1}
                        self.bl_fs.sn,          # {2}
                        self.bl_fs.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, FlowStation: {0}: with mac address '{1}' and serial number '{2}' is Paused".format(
                    self.bl_fs.ds,          # {0}
                    str(self.bl_fs.mac),    # {1}
                    self.bl_fs.sn,          # {2}
                  ))

    #################################
    def verify_status_is_rain_delay(self):
        if not self.status_is_rain_delay(_get_status=True):
            e_msg = "FlowStation: {0}: with mac address '{1}' and serial number '{2}' should in Rain Delay" \
                    ", Status received was: {3}".format(
                        self.bl_fs.ds,          # {0}
                        str(self.bl_fs.mac),    # {1}
                        self.bl_fs.sn,          # {2}
                        self.bl_fs.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, FlowStation: {0}: with mac address '{1}' and serial number '{2}' is in Rain Delay".format(
                    self.bl_fs.ds,          # {0}
                    str(self.bl_fs.mac),     # {1}
                    self.bl_fs.sn,          # {2}
                  ))

    #################################
    def verify_status_is_rain_jumper_triggered(self):
        if not self.status_is_rain_jumper(_get_status=True):
            e_msg = "FlowStation: {0}: with mac address '{1}' and serial number '{2}' should be Rain Jumper" \
                    "Triggered, Status received was: {3}".format(
                        self.bl_fs.ds,          # {0}
                        str(self.bl_fs.mac),    # {1}
                        self.bl_fs.sn,          # {2}
                        self.bl_fs.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, FlowStation: {0}: with mac address '{1}' and serial number '{2}' Rain Jumper "
                  "Triggered".format(
                    self.bl_fs.ds,          # {0}
                    str(self.bl_fs.mac),    # {1}
                    self.bl_fs.sn,          # {2}
                  ))

    #################################
    def verify_status_is_off(self):
        if not self.status_is_off(_get_status=True):
            e_msg = "FlowStation: {0}: with mac address '{1}' and serial number '{2}' should be Off, " \
                    "Status received was: {3}".format(
                        self.bl_fs.ds,          # {0}
                        str(self.bl_fs.mac),    # {1}
                        self.bl_fs.sn,          # {2}
                        self.bl_fs.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, FlowStation: {0}: with mac address '{1}' and serial number '{2}' Off".format(
                self.bl_fs.ds,          # {0}
                str(self.bl_fs.mac),    # {1}
                self.bl_fs.sn,          # {2}
            ))

    #################################
    def verify_status_is_error(self):
        if not self.status_is_error(_get_status=True):
            e_msg = "FlowStation: {0}: with mac address '{1}' and serial number '{2}' should be in error, " \
                    "Status received was: {1}".format(
                        self.bl_fs.ds,          # {0}
                        str(self.bl_fs.mac),    # {1}
                        self.bl_fs.sn,          # {2}
                        self.bl_fs.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified FlowStation: {0}: with mac address '{1}' and serial number '{2}' is error".format(
                str(self.bl_fs.ad),     # {0}
                str(self.bl_fs.mac),    # {1}
                self.bl_fs.sn,          # {2}
            ))

    #################################
    def verify_status_is_flow_jumper_fault(self):
        if not self.status_is_flow_jumper_fault(_get_status=True):
            e_msg = "FlowStation: {0}: with mac address '{1}' and serial number '{2}' should be flow jumper fault, " \
                    "Status received was: {1}".format(
                        self.bl_fs.ds,          # {0}
                        str(self.bl_fs.mac),    # {1}
                        self.bl_fs.sn,          # {2}
                        self.bl_fs.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified FlowStation: {0}: with mac address '{1}' and serial number '{2}' is flow jumper "
                  "fault".format(
                        self.bl_fs.ds,          # {0}
                        str(self.bl_fs.mac),    # {1}
                        str(self.bl_fs.sn),     # {2}
                    ))

    #################################
    def verify_connected_to_controller(self, _controller_number):
        """
        Verifies the connection to a controller from a FlowStation.

        """
        command = "{0},{1}={2}".format(
            ActionCommands.GET,                     # {0}
            ControllerCommands.Type.CONTROLLER,     # {1}
            str(_controller_number)                 # {2}
        )
        _expected_status = [opcodes.okay, opcodes.running]
        try:
            data = self.ser.get_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to get FlowStation {0}'s 'Controller Connection " \
                    "Status' for its controller number {1}.".format(self.bl_fs.mac, str(_controller_number))
            raise Exception(e_msg)

        else:
            cn_ss_on_cn = data.get_value_string_by_key(opcodes.status_code)

            if cn_ss_on_cn not in _expected_status:
                e_msg = "Exception occurred trying to verify FlowStation {0}'s 'Controller Connection Status' " \
                        "for controller number {1}. Expected Status: '{2}', Received: '{3}'.".format(
                            self.bl_fs.mac,
                            _controller_number,
                            _expected_status,
                            cn_ss_on_cn
                        )
                raise ValueError(e_msg)
            else:
                print "Successfully verified FlowStation {0}'s 'Controller Connection Status' for controller number " \
                      "{1}. Status Received: '{2}'.".format(self.bl_fs.mac, _controller_number, _expected_status)
                return True

    #################################
    def verify_not_connected_to_controller(self, _controller_number):
        """
        Verifies the connection to a controller from a FlowStation.

        """
        command = "{0},{1}={2}".format(
            ActionCommands.GET,                     # {0}
            ControllerCommands.Type.CONTROLLER,     # {1}
            str(_controller_number)                 # {2}
        )
        _expected_status = opcodes.connected
        try:
            data = self.ser.get_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to get FlowStation {0}'s 'Controller Connection " \
                    "Status' for its controller number {1}.".format(self.bl_fs.mac, str(_controller_number))
            raise Exception(e_msg)

        else:
            cn_ss_on_cn = data.get_value_string_by_key(opcodes.status_code)

            if _expected_status != cn_ss_on_cn:
                e_msg = "Exception occurred trying to verify FlowStation {0}'s 'Controller Connection Status' " \
                        "for controller number {1}. Expected Status: '{2}', Received: '{3}'.".format(
                            self.bl_fs.mac,
                            _controller_number,
                            _expected_status,
                            cn_ss_on_cn
                        )
                raise ValueError(e_msg)
            else:
                print "Successfully verified FlowStation {0}'s 'Controller Connection Status' for controller number " \
                      "{1}. Status Received: '{2}'.".format(self.bl_fs.mac, _controller_number, _expected_status)
                return True

    #################################
    def verify_connected_to_basemanager(self):
        """
        Verifies the FlowStation connection status to BaseManager against an expected status.

        :return:
        :rtype: bool
        """
        if self.bl_fs.ty in [ControllerCommands.Type.BASESTATION_1000, ControllerCommands.Type.BASESTATION_3200,
                             ControllerCommands.Type.FLOWSTATION]:
            _expected_status = opcodes.connected
            command = "{0},{1}".format(
                ActionCommands.GET,                 # {0}
                BasemanagerCommands.BaseManager,    # {1}
            )

            try:
                data = self.ser.get_and_wait_for_reply(tosend=command)
            except Exception:
                e_msg = "Exception occurred trying to get FlowStation's ({0}) 'BaseManager Connection Status'".format(
                    self.bl_fs.mac
                )
                raise Exception(e_msg)

            else:
                sb_ss_on_cn = data.get_value_string_by_key(opcodes.status_code)

                if _expected_status != sb_ss_on_cn:
                    e_msg = "Exception occurred trying to verify FlowStation's 'BaseManager Connection Status' for " \
                            "FlowStation: '{0}'. Expected Status: '{1}', Received: '{2}'.".format(
                                str(self.bl_fs.mac),
                                _expected_status,
                                sb_ss_on_cn
                            )
                    raise ValueError(e_msg)
                else:
                    print "Successfully verified FlowStation's 'BaseManager Connection Status' for FlowStation: " \
                          "'{0}', Status: '{1}'.".format(str(self.bl_fs.mac), _expected_status)
                    return True

        else:
            raise ValueError(
                "Attempting to verify 'Connection from Controller to BaseManager' on a Substation which "
                "is not supported.")

    #################################
    def verify_not_connected_to_basemanager(self):
        """
        Verifies the BaseManager connection status to the FlowStation against an expected status.

        :return:
        :rtype: bool
        """
        if self.bl_fs.ty in [ControllerCommands.Type.BASESTATION_1000, ControllerCommands.Type.BASESTATION_3200,
                             ControllerCommands.Type.FLOWSTATION]:
            _expected_status = opcodes.disconnected
            command = "{0},{1}".format(
                ActionCommands.GET,                 # {0}
                BasemanagerCommands.BaseManager,    # {1}
            )

            try:
                data = self.ser.get_and_wait_for_reply(tosend=command)
            except Exception:
                e_msg = "Exception occurred trying to get FlowStation's ({0}) 'BaseManager Connection Status'".format(
                    self.bl_fs.mac
                )
                raise Exception(e_msg)

            else:
                sb_ss_on_cn = data.get_value_string_by_key(opcodes.status_code)

                if _expected_status != sb_ss_on_cn:
                    e_msg = "Exception occurred trying to verify FlowStation's 'BaseManager Connection Status' for " \
                            "FlowStation: '{0}'. Expected Status: '{1}', Received: '{2}'.".format(
                                str(self.bl_fs.mac),
                                _expected_status,
                                sb_ss_on_cn
                            )
                    raise ValueError(e_msg)
                else:
                    print "Successfully verified FlowStation's 'BaseManager Connection Status' for FlowStation: " \
                          "'{0}', Status: '{1}'.".format(str(self.bl_fs.mac), _expected_status)
                    return True

        else:
            raise ValueError(
                "Attempting to verify 'Connection from Controller to BaseManager' on a Substation which "
                "is not supported.")
