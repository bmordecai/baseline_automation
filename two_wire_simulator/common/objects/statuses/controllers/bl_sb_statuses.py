from common.imports import opcodes
from common.objects.statuses.base_statuses import BaseStatuses
from common.imports.types import ActionCommands, ControllerCommands, BasemanagerCommands
# from common.imports.types import Statuses
from datetime import datetime, timedelta

__author__ = 'Baseline'


class SubStationStatuses(BaseStatuses):

    #################################
    def __init__(self, _controller_object):
        """
        :param _controller_object:     Controller object that will be referenced throughout this module. \n
        :type _controller_object:      common.objects.controllers.bl_sb.SubStation \n
        """
        self.bl_sb = _controller_object
        # Init parent class to inherit respective attributes
        BaseStatuses.__init__(self, _controller=_controller_object,
                              _identifiers=[[opcodes.controller, _controller_object.sn]],
                              _object=_controller_object)

    #################################
    def verify_status_is_ok(self):
        if not self.status_is_ok(_get_status=True):
            e_msg = "Substation: {0}: with mac address '{1}' and serial number '{2}' should be OK, " \
                    "Status received was: {3}".format(
                        self.bl_sb.ds,          # {0}
                        str(self.bl_sb.mac),    # {1}
                        self.bl_sb.sn,          # {2}
                        self.bl_sb.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Substation 3200: {0}: assigned to address '{1}'  with serial number '{2}'  is OK".format(
                self.bl_sb.ds,          # {0}
                str(self.bl_sb.ad),     # {1}
                self.bl_sb.sn,          # {2}
            ))

    #################################
    def verify_status_is_running(self):
        if not self.status_is_running(_get_status=True):
            e_msg = "Substation: {0}: with mac address '{1}' and serial number '{2}' should be Running, " \
                    "Status received was: {3}".format(
                        self.bl_sb.ds,          # {0}
                        str(self.bl_sb.mac),    # {1}
                        self.bl_sb.sn,          # {2}
                        self.bl_sb.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, BaseStation 3200: {0}: assigned to address '{1}'  with serial number '{2}'  "
                  "is Running".format(
                    self.bl_sb.ds,          # {0}
                    str(self.bl_sb.ad),     # {1}
                    self.bl_sb.sn,          # {2}
                ))

    #################################
    def verify_status_is_paused(self):
        if not self.status_is_paused(_get_status=True):
            e_msg = "Substation: {0}: with mac address '{1}' and serial number '{2}' should be Paused, " \
                    "Status received was: {3}".format(
                        self.bl_sb.ds,          # {0}
                        str(self.bl_sb.mac),    # {1}
                        self.bl_sb.sn,          # {2}
                        self.bl_sb.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, BaseStation 3200: {0}: assigned to address '{1}'  with serial number '{2}'  is Paused".format(
                self.bl_sb.ds,          # {0}
                str(self.bl_sb.ad),     # {1}
                self.bl_sb.sn,          # {2}
            ))

    #################################
    def verify_status_is_off(self):
        if not self.status_is_off(_get_status=True):
            e_msg = "Substation: {0}: with mac address '{1}' and serial number '{2}' should be Off, " \
                    "Status received was: {3}".format(
                        self.bl_sb.ds,          # {0}
                        str(self.bl_sb.mac),    # {1}
                        self.bl_sb.sn,          # {2}
                        self.bl_sb.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, BaseStation 3200: {0}: assigned to address '{1}'  with serial number '{2}' is Off".format(
                self.bl_sb.ds,          # {0}
                str(self.bl_sb.ad),     # {1}
                self.bl_sb.sn,          # {2}
            ))

    #################################
    def verify_status_is_error(self):
        if not self.status_is_error(_get_status=True):
            e_msg = "Substation: {0}: with mac address '{1}' and serial number '{2}' should be in error, " \
                    "Status received was: {1}".format(
                        self.bl_sb.ds,          # {0}
                        str(self.bl_sb.mac),    # {1}
                        self.bl_sb.sn,          # {2}
                        self.bl_sb.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified BaseStation 3200 {0}'s is error".format(
                str(self.bl_sb.ad),  # {0}
            ))

    #################################
    def verify_connected_to_controller(self):
        """
        Verifies the connection to a controller from a 3200.

        """
        command = "{0},{1}={2}".format(
            ActionCommands.GET,                     # {0}
            ControllerCommands.Type.FLOWSTATION,    # {1}
            str(self.ad)                            # {2}
        )
        _expected_status = opcodes.connected
        try:
            data = self.ser.get_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to get FlowStation {0}'s 'Controller Connection " \
                    "Status'.".format(self.ad)
            raise Exception(e_msg)

        else:
            cn_ss_on_cn = data.get_value_string_by_key(opcodes.status_code)

            if _expected_status != cn_ss_on_cn:
                e_msg = "Exception occurred trying to verify FlowStation {0}'s 'Controller Connection Status'. " \
                        "Expected Status: '{1}', Received: '{2}'.".format(
                            self.ad,
                            _expected_status,
                            cn_ss_on_cn
                        )
                raise ValueError(e_msg)
            else:
                print "Successfully verified FlowStation {0}'s 'Controller Connection Status'. Status Received: " \
                      "'{1}'.".format(self.ad, _expected_status)
                return True

    #################################
    def verify_not_connected_to_controller(self):
        """
        Verifies the connection to a controller from a FlowStation.

        """
        command = "{0},{1}={2}".format(
            ActionCommands.GET,                     # {0}
            ControllerCommands.Type.FLOWSTATION,    # {1}
            str(self.ad)                            # {2}
        )
        _expected_status = opcodes.disconnected
        try:
            data = self.ser.get_and_wait_for_reply(tosend=command)
        except Exception as e:
            e_msg = "Exception occurred trying to get FlowStation {0}'s 'Controller Connection " \
                    "Status'.".format(self.ad)
            raise Exception(e_msg)

        else:
            cn_ss_on_cn = data.get_value_string_by_key(opcodes.status_code)

            if _expected_status != cn_ss_on_cn:
                e_msg = "Exception occurred trying to verify FlowStation {0}'s 'Controller Connection Status'. " \
                        "Expected Status: '{1}', Received: '{2}'.".format(
                            self.ad,
                            _expected_status,
                            cn_ss_on_cn
                        )
                raise ValueError(e_msg)
            else:
                print "Successfully verified FlowStation {0}'s 'Controller Connection Status'. Status Received: " \
                      "'{1}'.".format(self.ad, _expected_status)
                return True
