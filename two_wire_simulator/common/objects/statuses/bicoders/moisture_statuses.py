from common.objects.statuses.base_statuses import BaseStatuses

__author__ = 'Baseline'


class MoistureStatuses(BaseStatuses):
    """
    Available Moisture biCoder status' per SubStation ERS:
        (1) OK
        (2) ER
    """
    
    #################################
    def __init__(self, _bicoder_object):
        """
        :param _bicoder_object:     Moisture bicoder object that will have a reference to this statuses object. \n
        :type _bicoder_object:      common.objects.bicoders.moisture_bicoder.MoistureBicoder
        """
        self.bicoder = _bicoder_object
        
        # Init parent class to inherit respective attributes
        BaseStatuses.__init__(self, _controller=_bicoder_object.controller,
                              _identifiers=[[_bicoder_object.id, _bicoder_object.sn]],
                              _object=_bicoder_object)
    
    #################################
    def verify_status_is_ok(self):
        if not self.status_is_ok(_get_status=True):
            e_msg = "Moisture biCoder {0} with serial number '{1}' should be Okay, Status received was: {2}".format(
                self.bicoder.ty,  # {0}
                self.bicoder.sn,  # {1}
                self.bicoder.ss  # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified, Moisture biCoder {0} with serial number '{1}' has Okay status".format(
                self.bicoder.ty,  # {0}
                self.bicoder.sn  # {1}
            ))
    
    #################################
    def verify_status_is_error(self):
        if not self.status_is_error(_get_status=True):
            e_msg = "Moisture biCoder {0} with serial number '{1}' should be in Error, " \
                    "Status received was: {2}".format(
                        self.bicoder.ty,  # {0}
                        self.bicoder.sn,  # {1}
                        self.bicoder.ss  # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Moisture biCoder {0} with serial number '{1}' has Error status.".format(
                self.bicoder.ty,  # {0}
                self.bicoder.sn  # {1}
            ))

