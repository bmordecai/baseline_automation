from common.objects.statuses.base_statuses import BaseStatuses

__author__ = 'Baseline'


class ValveStatuses(BaseStatuses):
    """
    Available Valve biCoder status' per SubStation ERS:
        (1) DN
        (2) ER
        (3) PA (Zones only)
        (4) WT
    """

    #################################
    def __init__(self, _bicoder_object):
        """
        :param _bicoder_object:     Valve bicoder object that will have a reference to this statuses object. \n
        :type _bicoder_object:      common.objects.bicoders.valve_bicoder.ValveBicoder
        """
        self.bicoder = _bicoder_object
        
        # Init parent class to inherit respective attributes
        BaseStatuses.__init__(self, _controller=_bicoder_object.controller,
                              _identifiers=[[_bicoder_object.id, _bicoder_object.sn]],
                              _object=_bicoder_object)

    #################################
    def verify_status_is_done(self):
        if not self.status_is_done_watering(_get_status=True):
            e_msg = "Valve biCoder {0} with serial number '{1}' should be Done, " \
                    "Status received was: {2}".format(
                        self.bicoder.ty,          # {0}
                        self.bicoder.sn,          # {1}
                        self.bicoder.ss           # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Valve biCoder {0} with serial number '{1}'  is Done Running".format(
                self.bicoder.ty,          # {0}
                self.bicoder.sn           # {1}
            ))

    #################################
    def verify_status_is_watering(self):
        if not self.status_is_watering(_get_status=True):
            e_msg = "Valve biCoder {0} with serial number '{1}'  should be Watering, " \
                    "Status received was: {2}".format(
                        self.bicoder.ty,          # {0}
                        self.bicoder.sn,          # {1}
                        self.bicoder.ss           # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Valve biCoder {0} with serial number '{1}'  is Watering".format(
                self.bicoder.ty,          # {0}
                str(self.bicoder.ad),     # {1}
                self.bicoder.sn,          # {2}
            ))

    #################################
    def verify_status_is_paused(self):
        if not self.status_is_paused(_get_status=True):
            e_msg = "Valve biCoder {0} with serial number '{1}' should be Paused, " \
                    "Status received was: {2}".format(
                        self.bicoder.ty,          # {0}
                        self.bicoder.sn,          # {1}
                        self.bicoder.ss           # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Valve biCoder {0} with serial number '{1}'  is Paused".format(
                self.bicoder.ty,          # {0}
                self.bicoder.sn           # {1}
            ))

    #################################
    def verify_status_is_error(self):
        if not self.status_is_error(_get_status=True):
            e_msg = "Valve biCoder {0} with serial number '{1}' should be in Error, " \
                    "Status received was: {2}".format(
                        self.bicoder.ty,          # {0}
                        self.bicoder.sn,          # {1}
                        self.bicoder.ss           # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Valve biCoder {0} with serial number '{1}' has Error status.".format(
                self.bicoder.ty,          # {0}
                self.bicoder.sn           # {1}
            ))
