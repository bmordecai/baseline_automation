from common.imports import opcodes
from common.objects.statuses.base_statuses import BaseStatuses
from common.imports.types import ActionCommands, ControllerCommands
# from common.imports.types import Statuses
from datetime import datetime, timedelta

__author__ = 'Baseline'


class PressureSensorStatuses(BaseStatuses):

    #################################
    def __init__(self, _pressure_sensor_object):
        """
        :param _pressure_sensor_object:     PressureSensor object that will have a reference to this messages object. \n
        :type _pressure_sensor_object:      common.objects.devices.ps.PressureSensor
        """
        self.pressure_sensor = _pressure_sensor_object
        # Init parent class to inherit respective attributes
        BaseStatuses.__init__(self, _controller=_pressure_sensor_object.controller,
                              _identifiers=[[opcodes.pressure_sensor, _pressure_sensor_object.bicoder.sn]],
                              _object=_pressure_sensor_object)


    #################################
    def verify_status_is_disabled(self):
        if not self.status_is_disabled(_get_status=True):
            e_msg = "Pressure Sensor: {0}: assigned to address '{1}'  with serial number '{2}'  should be disabled, " \
                    "Status received was: {3}".format(
                        self.pressure_sensor.ds,          # {0}
                        str(self.pressure_sensor.ad),     # {1}
                        self.pressure_sensor.sn,          # {2}
                        self.pressure_sensor.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Pressure Sensor: {0}: assigned to address '{1}'  with serial number '{2}'  is Disabled".format(
                self.pressure_sensor.ds,          # {0}
                str(self.pressure_sensor.ad),     # {1}
                self.pressure_sensor.sn,          # {2}
            ))

    #################################
    def verify_status_is_ok(self):
        if not self.status_is_ok(_get_status=True):
            e_msg = "Pressure Sensor: {0}: assigned to address '{1}'  with serial number '{2}' should be OK, " \
                    "Status received was: {3}".format(
                        self.pressure_sensor.ds,          # {0}
                        str(self.pressure_sensor.ad),     # {1}
                        self.pressure_sensor.sn,          # {2}
                        self.pressure_sensor.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Pressure Sensor: {0}: assigned to address '{1}'  with serial number '{2}'  is OK".format(
                self.pressure_sensor.ds,          # {0}
                str(self.pressure_sensor.ad),     # {1}
                self.pressure_sensor.sn,          # {2}
            ))

    #################################
    def verify_status_is_running(self):
        if not self.status_is_running(_get_status=True):
            e_msg = "Pressure Sensor: {0}: assigned to address '{1}'  with serial number '{2}' should be Running, " \
                    "Status received was: {3}".format(
                        self.pressure_sensor.ds,          # {0}
                        str(self.pressure_sensor.ad),     # {1}
                        self.pressure_sensor.sn,          # {2}
                        self.pressure_sensor.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Pressure Sensor: {0}: assigned to address '{1}'  with serial number '{2}'  is Running".format(
                    self.pressure_sensor.ds,          # {0}
                    str(self.pressure_sensor.ad),     # {1}
                    self.pressure_sensor.sn,          # {2}
                ))

    #################################
    def verify_status_is_error(self):
        if not self.status_is_error(_get_status=True):
            e_msg = "Pressure Sensor: {0}'s should be in error, Status received was: {1}".format(
                str(self.pressure_sensor.ad),     # {0}
                self.pressure_sensor.ss,          # {1}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Pressure Sensor {0}'s is error".format(
                str(self.pressure_sensor.ad),  # {0}
                ))