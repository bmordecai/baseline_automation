from common.imports import opcodes
from common.objects.statuses.base_statuses import BaseStatuses
from common.imports.types import ActionCommands, ControllerCommands
# from common.imports.types import Statuses
from datetime import datetime, timedelta

__author__ = 'Baseline'


class MoistureSensorStatuses(BaseStatuses):

    #################################
    def __init__(self, _moisture_sensor_object):
        """
        :param _moisture_sensor_object:     EventSwitch object that will have a reference to this messages object. \n
        :type _moisture_sensor_object:      common.objects.devices.ms.MoistureSensor
        """
        self.moisture_sensor = _moisture_sensor_object
        # Init parent class to inherit respective attributes
        BaseStatuses.__init__(self, _controller=_moisture_sensor_object.controller,
                              _identifiers=[[opcodes.moisture_sensor, _moisture_sensor_object.bicoder.sn]],
                              _object=_moisture_sensor_object)


    #################################
    def verify_status_is_disabled(self):
        if not self.status_is_disabled(_get_status=True):
            e_msg = "Moisture Sensor: {0}: assigned to address '{1}'  with serial number '{2}'  should be disabled, " \
                    "Status received was: {3}".format(
                        self.moisture_sensor.ds,          # {0}
                        str(self.moisture_sensor.ad),     # {1}
                        self.moisture_sensor.sn,          # {2}
                        self.moisture_sensor.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Moisture Sensor: {0}: assigned to address '{1}'  with serial number '{2}'  is Disabled".format(
                self.moisture_sensor.ds,          # {0}
                str(self.moisture_sensor.ad),     # {1}
                self.moisture_sensor.sn,          # {2}
            ))

    #################################
    def verify_status_is_ok(self):
        if not self.status_is_ok(_get_status=True):
            e_msg = "Moisture Sensor: {0}: assigned to address '{1}'  with serial number '{2}' should be OK, " \
                    "Status received was: {3}".format(
                        self.moisture_sensor.ds,          # {0}
                        str(self.moisture_sensor.ad),     # {1}
                        self.moisture_sensor.sn,          # {2}
                        self.moisture_sensor.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Moisture Sensor: {0}: assigned to address '{1}'  with serial number '{2}'  is OK".format(
                    self.moisture_sensor.ds,          # {0}
                    str(self.moisture_sensor.ad),     # {1}
                    self.moisture_sensor.sn,          # {2}
                ))

    #################################
    def verify_status_is_error(self):
        if not self.status_is_error(_get_status=True):
            e_msg = "Moisture Sensor: {0}'s should be in error, Status received was: {1}".format(
                str(self.moisture_sensor.ad),     # {0}
                self.moisture_sensor.ss,          # {1}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Moisture Sensor {0}'s is error".format(
                str(self.moisture_sensor.ad),  # {0}
            ))