from common.imports import opcodes
from common.objects.statuses.base_statuses import BaseStatuses
from common.imports.types import ActionCommands, ControllerCommands
# from common.imports.types import Statuses
from datetime import datetime, timedelta

__author__ = 'Baseline'


class EventSwitchStatuses(BaseStatuses):

    #################################
    def __init__(self, _event_switch_object):
        """
        :param _event_switch_object:     EventSwitch object that will have a reference to this messages object. \n
        :type _event_switch_object:      common.objects.devices.sw.EventSwitch
        """

        self.event_switch = _event_switch_object
        # Init parent class to inherit respective attributes
        BaseStatuses.__init__(self, _controller=_event_switch_object.controller,
                              _identifiers=[[opcodes.event_switch, _event_switch_object.bicoder.sn]],
                              _object=_event_switch_object)


    #################################
    def verify_status_is_disabled(self):
        if not self.status_is_disabled(_get_status=True):
            e_msg = "Event Switch: {0}: assigned to address '{1}'  with serial number '{2}'  should be disabled, " \
                    "Status received was: {3}".format(
                        self.event_switch.ds,          # {0}
                        str(self.event_switch.ad),     # {1}
                        self.event_switch.sn,          # {2}
                        self.event_switch.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Event Switch: {0}: assigned to address '{1}'  with serial number '{2}'  is Disabled".format(
                self.event_switch.ds,          # {0}
                str(self.event_switch.ad),     # {1}
                self.event_switch.sn,          # {2}
            ))

    #################################
    def verify_status_is_ok(self):
        if not self.status_is_ok(_get_status=True):
            e_msg = "Event Switch: {0}: assigned to address '{1}'  with serial number '{2}' should be OK, " \
                    "Status received was: {3}".format(
                        self.event_switch.ds,          # {0}
                        str(self.event_switch.ad),     # {1}
                        self.event_switch.sn,          # {2}
                        self.event_switch.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Event Switch: {0}: assigned to address '{1}'  with serial number '{2}'  is OK".format(
                    self.event_switch.ds,          # {0}
                    str(self.event_switch.ad),     # {1}
                    self.event_switch.sn,          # {2}
                ))

    #################################
    def verify_status_is_error(self):
        if not self.status_is_error(_get_status=True):
            e_msg = "Event Switch: {0}'s should be in error, Status received was: {1}".format(
                str(self.event_switch.ad),     # {0}
                self.event_switch.ss,          # {1}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Event Switch {0}'s is error".format(
                str(self.event_switch.ad),  # {0}
            ))