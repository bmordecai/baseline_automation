from common.imports import opcodes
from common.objects.statuses.base_statuses import BaseStatuses
from common.imports.types import ActionCommands, ControllerCommands
# from common.imports.types import Statuses
from datetime import datetime, timedelta

__author__ = 'Baseline'


class ZoneStatuses(BaseStatuses):

    #################################
    def __init__(self, _zone_object):
        """
        :param _zone_object:     Zone object that will have a reference to this messages object. \n
        :type _zone_object:      common.objects.devices.zn.Zone
        """
        self.zones = _zone_object
        # Init parent class to inherit respective attributes
        BaseStatuses.__init__(self, _controller=_zone_object.controller,
                              _identifiers=[[opcodes.zone, _zone_object.ad]],
                              _object=_zone_object)


    #################################
    def verify_status_is_disabled(self):
        if not self.status_is_disabled(_get_status=True):
            e_msg = "Zone: {0}: assigned to address '{1}'  with serial number '{2}'  should be disabled, " \
                    "Status received was: {3}".format(
                        self.zones.ds,          # {0}
                        str(self.zones.ad),     # {1}
                        self.zones.sn,          # {2}
                        self.zones.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Zone: {0}: assigned to address '{1}'  with serial number '{2}'  is Disabled".format(
                self.zones.ds,          # {0}
                str(self.zones.ad),     # {1}
                self.zones.sn,          # {2}
            ))

    #################################
    def verify_status_is_unassigned(self):
        if not self.status_is_unassigned(_get_status=True):
            e_msg = "Zone: {0}: assigned to address '{1}'  with serial number '{2}'  should be unassinged, " \
                    "Status received was: {3}".format(
                        self.zones.ds,          # {0}
                        str(self.zones.ad),     # {1}
                        self.zones.sn,          # {2}
                        self.zones.ss,                # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Zone: {0}: assigned to address '{1}'  with serial number '{2}'  is unassigned".format(
                self.zones.ds,          # {0}
                str(self.zones.ad),     # {1}
                self.zones.sn,          # {2}
            ))

    #################################
    def verify_status_is_done(self):
        if not self.status_is_done_watering(_get_status=True):
            e_msg = "Zone: {0}: assigned to address '{1}'  with serial number '{2}' should be Done, " \
                    "Status received was: {3}".format(
                        self.zones.ds,          # {0}
                        str(self.zones.ad),     # {1}
                        self.zones.sn,          # {2}
                        self.zones.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Zone: {0}: assigned to address '{1}'  with serial number '{2}'  is Done Running".format(
                self.zones.ds,          # {0}
                str(self.zones.ad),     # {1}
                self.zones.sn,          # {2}
            ))

    #################################
    def verify_status_is_manually_running(self):
        if not self.status_is_manually_running(_get_status=True):
            e_msg = "Zone: {0}: assigned to address '{1}'  with serial number '{2}' should be Manually Running, " \
                    "Status received was: {3}".format(
                        self.zones.ds,          # {0}
                        str(self.zones.ad),     # {1}
                        self.zones.sn,          # {2}
                        self.zones.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Zone: {0}: assigned to address '{1}'  with serial number '{2}'  is Manually Running".format(
                    self.zones.ds,          # {0}
                    str(self.zones.ad),     # {1}
                    self.zones.sn,          # {2}
                ))

    #################################
    def verify_status_is_watering(self):
        if not self.status_is_watering(_get_status=True):
            e_msg = "Zone: {0}: assigned to address '{1}'  with serial number '{2}'  should be Watering, " \
                    "Status received was: {3}".format(
                        self.zones.ds,          # {0}
                        str(self.zones.ad),     # {1}
                        self.zones.sn,          # {2}
                        self.zones.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Zone: {0}: assigned to address '{1}'  with serial number '{2}'  is Watering".format(
                self.zones.ds,          # {0}
                str(self.zones.ad),     # {1}
                self.zones.sn,          # {2}
            ))

    #################################
    def verify_status_is_learning_flow(self):
        if not self.status_is_learning_flow(_get_status=True):
            e_msg = "Zone: {0}: assigned to address '{1}'  with serial number '{2}' should be Learning Flow " \
                    "Status received was: {3}".format(
                        self.zones.ds,          # {0}
                        str(self.zones.ad),     # {1}
                        self.zones.sn,          # {2}
                        self.zones.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Zone: {0}: assigned to address '{1}'  with serial number '{2}'  is Learning Flow".format(
                    self.zones.ds,          # {0}
                    str(self.zones.ad),     # {1}
                    self.zones.sn,          # {2}
                ))

    #################################
    def verify_status_is_soaking(self):
        if not self.status_is_soaking(_get_status=True):
            e_msg = "Zone: {0}: assigned to address '{1}'  with serial number '{2}' should be Soaking," \
                    " Status received was: {3}".format(
                        self.zones.ds,          # {0}
                        str(self.zones.ad),     # {1}
                        self.zones.sn,          # {2}
                        self.zones.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Zone: {0}: assigned to address '{1}'  with serial number '{2}'  is Soaking".format(
                self.zones.ds,          # {0}
                str(self.zones.ad),     # {1}
                self.zones.sn,          # {2}
            ))

    #################################
    def verify_status_is_waiting_to_water(self):
        if not self.status_is_waiting_to_water(_get_status=True):
            e_msg = "Zone: {0}: assigned to address '{1}'  with serial number '{2}' should be Waiting, " \
                    "Status received was: {3}".format(
                        self.zones.ds,          # {0}
                        str(self.zones.ad),     # {1}
                        self.zones.sn,          # {2}
                        self.zones.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Zone: {0}: assigned to address '{1}'  with serial number '{2}' is Waiting To Run".format(
                self.zones.ds,          # {0}
                str(self.zones.ad),     # {1}
                self.zones.sn,          # {2}
            ))

    #################################
    def verify_status_is_paused(self):
        if not self.status_is_paused(_get_status=True):
            e_msg = "Zone: {0}: assigned to address '{1}'  with serial number '{2}' should be Paused, " \
                    "Status received was: {3}".format(
                        self.zones.ds,          # {0}
                        str(self.zones.ad),     # {1}
                        self.zones.sn,          # {2}
                        self.zones.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Zone: {0}: assigned to address '{1}'  with serial number '{2}'  is Paused".format(
                self.zones.ds,          # {0}
                str(self.zones.ad),     # {1}
                self.zones.sn,          # {2}
            ))


    #################################
    def verify_status_is_error(self):
        if not self.status_is_error(_get_status=True):
            e_msg = "Zone: {0}'s should be in error, Status received was: {1}".format(
                str(self.zones.ad),     # {0}
                self.zones.ss,          # {1}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Zone {0}'s is error".format(
                str(self.zones.ad),  # {0}
            ))