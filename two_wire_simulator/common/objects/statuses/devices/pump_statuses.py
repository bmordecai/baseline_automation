from common.imports import opcodes
from common.objects.statuses.base_statuses import BaseStatuses
from common.imports.types import ActionCommands, ControllerCommands
# from common.imports.types import Statuses
from datetime import datetime, timedelta

__author__ = 'Baseline'


class PumpStatuses(BaseStatuses):

    #################################
    def __init__(self, _pump_object):
        """
        :param _pump_object:     Pump object that will have a reference to this messages object. \n
        :type _pump_object:      common.objects.devices.pm.Pump
        """
        self.pumps = _pump_object
        # Init parent class to inherit respective attributes
        BaseStatuses.__init__(self, _controller=_pump_object.controller,
                              _identifiers=[[opcodes.pump, _pump_object.bicoder.sn]],
                              _object=_pump_object)

    #################################
    def verify_status_is_disabled(self):
        if not self.status_is_disabled(_get_status=True):
            e_msg = "Pump: {0}: assigned to address '{1}'  with serial number '{2}'  should be disabled, " \
                    "Status received was: {3}".format(
                        self.pumps.ds,          # {0}
                        str(self.pumps.ad),     # {1}
                        self.pumps.sn,          # {2}
                        self.pumps.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Pump: {0}: assigned to address '{1}'  with serial number '{2}'  is Disabled".format(
                self.pumps.ds,          # {0}
                str(self.pumps.ad),     # {1}
                self.pumps.sn,          # {2}
            ))

    #################################
    def verify_status_is_manually_running(self):
        if not self.status_is_manually_running(_get_status=True):
            e_msg = "Pump: {0}: assigned to address '{1}'  with serial number '{2}' should be Manually Running, " \
                    "Status received was: {3}".format(
                        self.pumps.ds,  # {0}
                        str(self.pumps.ad),  # {1}
                        self.pumps.sn,  # {2}
                        self.pumps.ss,  # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Pump: {0}: assigned to address '{1}'  with serial number '{2}'  is Manually Running"
                .format(
                    self.pumps.ds,  # {0}
                    str(self.pumps.ad),  # {1}
                    self.pumps.sn,  # {2}
                ))

    #################################
    def verify_status_is_watering(self):
        if not self.status_is_watering(_get_status=True):
            e_msg = "Pump: {0}: assigned to address '{1}'  with serial number '{2}' should be Watering, " \
                    "Status received was: {3}".format(
                        self.pumps.ds,          # {0}
                        str(self.pumps.ad),     # {1}
                        self.pumps.sn,          # {2}
                        self.pumps.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Pump: {0}: assigned to address '{1}'  with serial number '{2}'  is Watering".format(
                self.pumps.ds,          # {0}
                str(self.pumps.ad),     # {1}
                self.pumps.sn,          # {2}
            ))

    #################################
    def verify_status_is_off(self):
        if not self.status_is_off(_get_status=True):
            e_msg = "Pump: {0}: assigned to address '{1}'  with serial number '{2}' should be Off, " \
                    "Status received was: {3}".format(
                        self.pumps.ds,          # {0}
                        str(self.pumps.ad),     # {1}
                        self.pumps.sn,          # {2}
                        self.pumps.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Pump: {0}: assigned to address '{1}'  with serial number '{2}'  is Off".format(
                self.pumps.ds,          # {0}
                str(self.pumps.ad),     # {1}
                self.pumps.sn,          # {2}
            ))

    #################################
    def verify_status_is_error(self):
        if not self.status_is_error(_get_status=True):
            e_msg = "Pump: {0}'s should be in error, Status received was: {1}".format(
                str(self.pumps.ad),     # {0}
                self.pumps.ss,          # {1}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Pump {0}'s is error".format(
                str(self.pumps.ad),  # {0}
            ))