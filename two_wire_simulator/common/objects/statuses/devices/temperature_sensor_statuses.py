from common.imports import opcodes
from common.objects.statuses.base_statuses import BaseStatuses
from common.imports.types import ActionCommands, ControllerCommands
# from common.imports.types import Statuses
from datetime import datetime, timedelta

__author__ = 'Baseline'


class TemperatureSensorStatuses(BaseStatuses):

    #################################
    def __init__(self, _temperature_sensor_object):
        """
        :param _temperature_sensor_object:     TemperatureSensor object that will have a reference to this messages object. \n
        :type _temperature_sensor_object:      common.objects.devices.ts.TemperatureSensor
        """
        self.temperature_sensor = _temperature_sensor_object
        # Init parent class to inherit respective attributes
        BaseStatuses.__init__(self, _controller=_temperature_sensor_object.controller,
                              _identifiers=[[opcodes.temperature_sensor,
                                             _temperature_sensor_object.bicoder.sn]],
                              _object=_temperature_sensor_object)


    #################################
    def verify_status_is_disabled(self):
        if not self.status_is_disabled(_get_status=True):
            e_msg = "Temperature Sensor: {0}: assigned to address '{1}'  with serial number '{2}'  should be disabled, " \
                    "Status received was: {3}".format(
                        self.temperature_sensor.ds,          # {0}
                        str(self.temperature_sensor.ad),     # {1}
                        self.temperature_sensor.sn,          # {2}
                        self.temperature_sensor.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Temperature Sensor: {0}: assigned to address '{1}'  with serial number '{2}'  is Disabled".format(
                self.temperature_sensor.ds,          # {0}
                str(self.temperature_sensor.ad),     # {1}
                self.temperature_sensor.sn,          # {2}
            ))

    #################################
    def verify_status_is_ok(self):
        if not self.status_is_ok(_get_status=True):
            e_msg = "Temperature Sensor: {0}: assigned to address '{1}'  with serial number '{2}' should be OK, " \
                    "Status received was: {3}".format(
                        self.temperature_sensor.ds,          # {0}
                        str(self.temperature_sensor.ad),     # {1}
                        self.temperature_sensor.sn,          # {2}
                        self.temperature_sensor.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Temperature Sensor: {0}: assigned to address '{1}'  with serial number '{2}'  is OK".format(
                    self.temperature_sensor.ds,          # {0}
                    str(self.temperature_sensor.ad),     # {1}
                    self.temperature_sensor.sn,          # {2}
                ))

    #################################
    def verify_status_is_error(self):
        if not self.status_is_error(_get_status=True):
            e_msg = "Temperature Sensor: {0}'s should be in error, Status received was: {1}".format(
                str(self.temperature_sensor.ad),     # {0}
                self.temperature_sensor.ss,          # {1}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Temperature Sensor {0}'s is error".format(
                str(self.temperature_sensor.ad),  # {0}
            ))