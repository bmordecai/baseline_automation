from common.imports import opcodes
from common.objects.statuses.base_statuses import BaseStatuses
from common.imports.types import ActionCommands, ControllerCommands
# from common.imports.types import Statuses
from datetime import datetime, timedelta

__author__ = 'Baseline'


class FlowMeterStatuses(BaseStatuses):

    #################################
    def __init__(self, _flow_meter_object):
        """
        :param _flow_meter_object:     FlowMeter object that will have a reference to this messages object. \n
        :type _flow_meter_object:      common.objects.devices.fm.FlowMeter
        """
        self.flow_meters = _flow_meter_object
        # Init parent class to inherit respective attributes
        BaseStatuses.__init__(self, _controller=_flow_meter_object.controller,
                              _identifiers=[[opcodes.flow_meter, _flow_meter_object.bicoder.sn]],
                              _object=_flow_meter_object)


    #################################
    def verify_status_is_disabled(self):
        if not self.status_is_disabled(_get_status=True):
            e_msg = "Flow Meter: {0}: assigned to address '{1}'  with serial number '{2}'  should be disabled, " \
                    "Status received was: {3}".format(
                        self.flow_meters.ds,          # {0}
                        str(self.flow_meters.ad),     # {1}
                        self.flow_meters.sn,          # {2}
                        self.flow_meters.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Flow Meter: {0}: assigned to address '{1}'  with serial number '{2}'  is Disabled".format(
                self.flow_meters.ds,          # {0}
                str(self.flow_meters.ad),     # {1}
                self.flow_meters.sn,          # {2}
            ))

    #################################
    def verify_status_is_ok(self):
        if not self.status_is_ok(_get_status=True):
            e_msg = "Flow Meter: {0}: assigned to address '{1}'  with serial number '{2}' should be OK, " \
                    "Status received was: {3}".format(
                        self.flow_meters.ds,          # {0}
                        str(self.flow_meters.ad),     # {1}
                        self.flow_meters.sn,          # {2}
                        self.flow_meters.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Flow Meter: {0}: assigned to address '{1}'  with serial number '{2}'  is OK".format(
                self.flow_meters.ds,          # {0}
                str(self.flow_meters.ad),     # {1}
                self.flow_meters.sn,          # {2}
            ))

    #################################
    def verify_status_is_running(self):
        if not self.status_is_running(_get_status=True):
            e_msg = "Flow Meter: {0}: assigned to address '{1}'  with serial number '{2}' should be Running, " \
                    "Status received was: {3}".format(
                        self.flow_meters.ds,          # {0}
                        str(self.flow_meters.ad),     # {1}
                        self.flow_meters.sn,          # {2}
                        self.flow_meters.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Flow Meter: {0}: assigned to address '{1}'  with serial number '{2}'  is Running".format(
                    self.flow_meters.ds,          # {0}
                    str(self.flow_meters.ad),     # {1}
                    self.flow_meters.sn,          # {2}
                ))

    #################################
    def verify_status_is_error(self):
        if not self.status_is_error(_get_status=True):
            e_msg = "Flow Meter: {0}'s should be in error, Status received was: {1}".format(
                str(self.flow_meters.ad),     # {0}
                self.flow_meters.ss,          # {1}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Flow Meter {0}'s is error".format(
                str(self.flow_meters.ad),  # {0}
                ))