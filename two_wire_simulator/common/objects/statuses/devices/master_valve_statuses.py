from common.imports import opcodes
from common.objects.statuses.base_statuses import BaseStatuses
from common.imports.types import ActionCommands, ControllerCommands
# from common.imports.types import Statuses
from datetime import datetime, timedelta

__author__ = 'Baseline'


class MasterValveStatuses(BaseStatuses):

    #################################
    def __init__(self, _master_valve_object):
        """
        :param _master_valve_object:     MasterValve object that will have a reference to this messages object. \n
        :type _master_valve_object:      common.objects.devices.mv.MasterValve
        """
        self.master_valves = _master_valve_object
        # Init parent class to inherit respective attributes
        BaseStatuses.__init__(self, _controller=_master_valve_object.controller,
                              _identifiers=[[opcodes.master_valve, _master_valve_object.bicoder.sn]],
                              _object=_master_valve_object)

    #################################
    def verify_status_is_disabled(self):
        if not self.status_is_disabled(_get_status=True):
            e_msg = "MasterValve: {0}: assigned to address '{1}'  with serial number '{2}'  should be disabled, " \
                    "Status received was: {3}".format(
                        self.master_valves.ds,          # {0}
                        str(self.master_valves.ad),     # {1}
                        self.master_valves.sn,          # {2}
                        self.master_valves.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Master Valve: {0}: assigned to address '{1}'  with serial number '{2}'  is Disabled".format(
                self.master_valves.ds,          # {0}
                str(self.master_valves.ad),     # {1}
                self.master_valves.sn,          # {2}
            ))

    #################################
    def verify_status_is_manually_running(self):
        if not self.status_is_manually_running(_get_status=True):
            e_msg = "MasterValve: {0}: assigned to address '{1}'  with serial number '{2}'  should be Manually" \
                    " Running, Status received was: {3}".format(
                        self.master_valves.ds,          # {0}
                        str(self.master_valves.ad),     # {1}
                        self.master_valves.sn,          # {2}
                        self.master_valves.ss,                # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Master Valve: {0}: assigned to address '{1}'  with serial number '{2}'  is Manually"
                  " Running".format(
                        self.master_valves.ds,          # {0}
                        str(self.master_valves.ad),     # {1}
                        self.master_valves.sn,          # {2}
                    ))

    #################################
    def verify_status_is_watering(self):
        if not self.status_is_watering(_get_status=True):
            e_msg = "Master Valve: {0}: assigned to address '{1}'  with serial number '{2}' should be Watering, " \
                    "Status received was: {3}".format(
                        self.master_valves.ds,          # {0}
                        str(self.master_valves.ad),     # {1}
                        self.master_valves.sn,          # {2}
                        self.master_valves.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Master Valve: {0}: assigned to address '{1}'  with serial number '{2}'  is Watering".format(
                    self.master_valves.ds,          # {0}
                    str(self.master_valves.ad),     # {1}
                    self.master_valves.sn,          # {2}
                ))

    #################################
    def verify_status_is_off(self):
        if not self.status_is_off(_get_status=True):
            e_msg = "Master Valve: {0}: assigned to address '{1}'  with serial number '{2}' should be Off, " \
                    "Status received was: {3}".format(
                        self.master_valves.ds,          # {0}
                        str(self.master_valves.ad),     # {1}
                        self.master_valves.sn,          # {2}
                        self.master_valves.ss,          # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified, Master Valve: {0}: assigned to address '{1}'  with serial number '{2}'  is Off".format(
                self.master_valves.ds,          # {0}
                str(self.master_valves.ad),     # {1}
                self.master_valves.sn,          # {2}
            ))

    #################################
    def verify_status_is_error(self):
        if not self.status_is_error(_get_status=True):
            e_msg = "Master Valve: {0}'s should be in error, Status received was: {1}".format(
                str(self.master_valves.ad),     # {0}
                self.master_valves.ss,          # {1}
            )
            raise ValueError(e_msg)
        else:
            print("Verified MasterValve {0}'s is error".format(
                str(self.master_valves.ad),  # {0}
            ))