import status_parser

from common.date_package.date_resource import date_mngr
from datetime import datetime, timedelta
from common.imports import opcodes
from common.imports import statuscodes
from common.imports.types import ActionCommands
from common.imports.types import status_plus_priority_code_dict_3200, status_code_dict_1000, \
    program_1000_who_dict

__author__ = 'Baseline'


class BaseStatuses(object):
    def __init__(self, _controller, _identifiers, _object):
        self.ser = _controller.ser
        self.status_data = status_parser.KeyValues(string_in=None)
        self.identifiers = _identifiers
        self.ds = "{0} {1}".format(_identifiers[0][0], _identifiers[0][1])
        self.ty = _identifiers[0][0]
        self.object = _object
        self.status = None
    
    #################################
    def get_status_if_necessary(self, _get_status):
        """ Shortcut function so we don't have to have this boilerplate at the top of each status_is... function """
        if _get_status or not self.status:
            self.get_status()

    #################################
    def get_status(self):
        """
        Gets all data for the object from the controller. \n

        Updates the status for the current object.
        :return:
        """
        # Example command: "GET,PC=1"
        command = "{0}{1}".format(
            ActionCommands.GET,  # {0}
            self.get_id(),  # {1}
        )

        # Attempt to get data from controller
        try:
            self.status = self.ser.get_and_wait_for_reply(tosend=command)

            # ** Update - 11/1/17 - Ben
            #   Based on common uses in use cases, I added this for convenience sake. This allows us to always keep
            #   track of the previous status for all objects.
            #
            #   At the use case level, this removes the requirement for the user to remember to save last status. Now,
            #   it will always be saved.
            #
            #   This has no affect on current functionality and only enhances future functionality.
            self.object.last_ss = self.object.ss

            # ** Update - 11/1/17 - Ben
            #   Based on common uses in use cases, I added this for convenience sake. This allows us to not worry about
            #   calling:
            #
            #       status = self.config.BaseStation3200[1].zones[1].data.get_value_string_by_key(opcodes.status_code)
            #
            #   At the use case level, this removes the manual process for getting the status for an object after a
            #   get_status() call.
            #
            #   This has no affect on current functionality and only enhances future functionality.
            self.object.ss = self.status.get_value_string_by_key(opcodes.status_code)
            return self.object.ss
        except AssertionError as ae:
            e_msg = "Unable to get data for {0} using command: '{1}'. Exception raised: {2}".format(
                self.ds,  # {0}
                command,  # {1}
                str(ae.message)  # {2}
            )
            raise ValueError(e_msg)
    #################################
    def get_id(self):
        """
        Returns the key-value pair identifier. \n
            - NOTE: Some objects have two identifiers, this method accounts for that. \n
                    Some objects have a single key identifier, such as a controller. ex: "get,cn"
        """
        identifier_string = ""
        try:
            for identifier in self.identifiers:
                if self.ty in [opcodes.basestation_3200]:
                    identifier_string += ",{0}".format(identifier[0])
                else:
                    identifier_string += ",{0}={1}".format(identifier[0], identifier[1])

        except:
            identifier_string += ",{0}={1}".format(self.object.id, self.object.sn)
            return identifier_string

        return identifier_string

    #################################
    def status_is_disabled(self, _get_status=False):
        """
        Convenience wrapper
        :param _get_status:     if true than get data
        :type _get_status:      bool \n
        Returns whether or not the device has a status of disabled.
    
        usage:
    
            if self.config.BaseStation3200[1].zones[1].status_is_watering():
                /* DO SOMETHING */
    
        :rtype: bool
        """
        self.get_status_if_necessary(_get_status)
        return self.object.ss == statuscodes.disabled
    
    #################################
    def status_is_unassigned(self,_get_status=False):
        """
        Convenience wrapper
        :param _get_status:     if true than get data
        :type _get_status:      bool \n
        Returns whether or not the device has a status of unassigned.
    
        usage:
    
            if self.config.BaseStation3200[1].zones[1].status_is_watering():
                /* DO SOMETHING */
    
        :rtype: bool
        """
        self.get_status_if_necessary(_get_status)
        return self.object.ss == statuscodes.unassigned
    
    #################################
    def status_is_done_watering(self, _get_status=False):
        """
        Convenience wrapper
        :param _get_status:     if true than get data
        :type _get_status:      bool \n
        Returns whether or not the device has a status of done watering.
    
        usage:
    
            if self.config.BaseStation3200[1].zones[1].status_is_done_watering():
                /* DO SOMETHING */
    
        :rtype: bool
        """
        self.get_status_if_necessary(_get_status)
        return self.object.ss == statuscodes.done
    
    #################################
    def status_is_not_done_watering(self, _get_status=False):
        """
        Convenience wrapper
        :param _get_status:     if true than get data
        :type _get_status:      bool \n
        Returns whether or not the device has a status of not done watering.
    
        usage:
    
            if self.config.BaseStation3200[1].zones[1].status_is_not_done_watering():
                /* DO SOMETHING */
    
        :rtype: bool
        """
        return not self.status_is_done_watering(_get_status)
    
    #################################
    def status_is_ok(self, _get_status=False):
        """
        Convenience wrapper
        :param _get_status:     if true than get data
        :type _get_status:      bool \n
        Returns whether or not the device has a status of ok.
    
        usage:
    
            if self.config.BaseStation3200[1].zones[1].status_is_watering():
                /* DO SOMETHING */
    
        :rtype: bool
        """
        self.get_status_if_necessary(_get_status)
        return self.object.ss == statuscodes.okay

    #################################
    def status_is_manually_running(self, _get_status=False):
        """
        Convenience wrapper
        :param _get_status:     if true than get data
        :type _get_status:      bool \n
        Returns whether or not the device has a status of manually running.
    
        usage:
    
            if self.config.BaseStation3200[1].zones[1].status_is_watering():
                /* DO SOMETHING */
    
        :rtype: bool
        """
        self.get_status_if_necessary(_get_status)
        return self.object.ss == statuscodes.manually_running
    
    #################################
    def status_is_running(self, _get_status=False):
        """
        Convenience wrapper
        :param _get_status:     if true than get data
        :type _get_status:      bool \n
        Returns whether or not the device has a status of running.
    
        usage:
    
            if self.config.BaseStation3200[1].zones[1].status_is_watering():
                /* DO SOMETHING */
    
        :rtype: bool
        """
        self.get_status_if_necessary(_get_status)
        return self.object.ss == statuscodes.running

    #################################
    def status_is_watering(self, _get_status=False):
        """
        Convenience wrapper
        :param _get_status:     if true than get data
        :type _get_status:      bool \n
        Returns whether or not the device has a status of watering.
    
        usage:
    
            if self.config.BaseStation3200[1].zones[1].status_is_watering():
                /* DO SOMETHING */
    
        :rtype: bool
        """
        self.get_status_if_necessary(_get_status)
        return self.object.ss == statuscodes.watering
    
    #################################
    def status_is_learning_flow(self, _get_status=False):
        """
        Convenience method to return if the device is not in an error state.
        :param _get_status:     if true than get data
        :type _get_status:      bool \n
        usage:
    
            if self.config.BaseStation3200[1].zones[1].status_is_not_error():
                /* DO SOMETHING */
    
        :rtype: bool
        """
        if _get_status or not self.status:
            self.get_status()
    
        return self.object.ss == statuscodes.learning_flow
    
    #################################
    def status_is_soaking(self, _get_status=False):
        """
        Convenience wrapper
        :param _get_status:     if true than get data
        :type _get_status:      bool \n
        Returns whether or not the device has a status of watering.
    
        usage:
    
            if self.config.BaseStation3200[1].zones[1].status_is_watering():
                /* DO SOMETHING */
    
        :rtype: bool
        """
        self.get_status_if_necessary(_get_status)
        return self.object.ss == statuscodes.soaking
    
    #################################
    def status_is_waiting_to_water(self, _get_status=False):
        """
        Convenience wrapper
        :param _get_status:     if true than get data
        :type _get_status:      bool \n
        Returns whether or not the device has a status of waiting to water.
    
        usage:
    
            if self.config.BaseStation3200[1].zones[1].status_is_waiting_to_water():
                /* DO SOMETHING */
    
        :rtype: bool
        """
        self.get_status_if_necessary(_get_status)
        return self.object.ss == statuscodes.waiting
    
    #################################
    def status_is_water_empty(self, _get_status=False):
        """
        Convenience wrapper
        :param _get_status:     if true than get data
        :type _get_status:      bool \n
        Returns whether or not the device has a status of water empty.
    
        usage:
    
            if self.config.BaseStation3200[1].zones[1].status_is_waiting_to_water():
                /* DO SOMETHING */
    
        :rtype: bool
        """
        self.get_status_if_necessary(_get_status)
        return self.object.ss == statuscodes.water_empty
    
    #################################
    def status_is_paused(self, _get_status=False):
        """
        Convenience wrapper
        :param _get_status:     if true than get data
        :type _get_status:      bool \n
        Returns whether or not the device has a status of paused.
    
        usage:
    
            if self.config.BaseStation3200[1].zones[1].status_is_watering():
                /* DO SOMETHING */
    
        :rtype: bool
        """
        self.get_status_if_necessary(_get_status)
        return self.object.ss == statuscodes.paused
    
    #################################
    def status_is_rain_delay(self, _get_status=False):
        """
        Convenience wrapper
        :param _get_status:     if true than get data
        :type _get_status:      bool \n
        Returns whether or not the device has a status of rain delay.
    
        usage:
    
            if self.config.BaseStation3200[1].zones[1].status_is_watering():
                /* DO SOMETHING */
    
        :rtype: bool
        """
        self.get_status_if_necessary(_get_status)
        return self.object.ss == statuscodes.rain_delay
    
    #################################
    def status_is_rain_jumper(self, _get_status=False):
        """
        Convenience wrapper
        :param _get_status:     if true than get data
        :type _get_status:      bool \n
        Returns whether or not the device has a status of rain jumper.
    
        usage:
    
            if self.config.BaseStation3200[1].zones[1].status_is_watering():
                /* DO SOMETHING */
    
        :rtype: bool
        """
        self.get_status_if_necessary(_get_status)
        return self.object.ss == statuscodes.rain_jumper
    
    #################################
    def status_is_off(self, _get_status=False):
        """
        Convenience wrapper
        :param _get_status:     if true than get data
        :type _get_status:      bool \n
        Returns whether or not the device has a status of off.
    
        usage:
    
            if self.config.BaseStation3200[1].zones[1].status_is_not_done_watering():
                /* DO SOMETHING */
    
        :rtype: bool
        """
        self.get_status_if_necessary(_get_status)
        return self.object.ss == statuscodes.off
    
    #################################
    def status_is_error(self, _get_status=False):
        """
        Convenience method to return if the device is not in an error state.
        :param _get_status:     if true than get data
        :type _get_status:      bool \n
        usage:
    
            if self.config.BaseStation3200[1].zones[1].status_is_error():
                /* DO SOMETHING */
    
        :rtype: bool
        """
        self.get_status_if_necessary(_get_status)
        return self.object.ss == statuscodes.error
    
    #################################
    def status_is_not_error(self, _get_status=False):
        """
        Convenience method to return if the device is not in an error state.
        :param _get_status:     if true than get data
        :type _get_status:      bool \n
        usage:
    
            if self.config.BaseStation3200[1].zones[1].status_is_not_error():
                /* DO SOMETHING */
    
        :rtype: bool
        """
        self.get_status_if_necessary(_get_status)
        # use "not" here to negate the equality.
        #   Thus, if:
        #           "status == statuscodes.error" IS TRUE
        #   then,
        #           "not status == statuscodes.error" IS FALSE
        return not self.object.ss == statuscodes.error
    
    #################################
    def status_is_flow_jumper_fault(self, _get_status=False):
        """
        Convenience method to return if the device is not in a flow jumper fault.
        :param _get_status:     if true than get data
        :type _get_status:      bool \n
        usage:
    
            if self.config.BaseStation3200[1].zones[1].status_is_error():
                /* DO SOMETHING */
    
        :rtype: bool
        """
        self.get_status_if_necessary(_get_status)
        return self.object.ss == statuscodes.flow_fault

    #################################
    def status_is_flow_fault(self, _get_status=False):
        """
        Convenience method to return if the device is not in a flow jumper fault.
        :param _get_status:     if true than get data
        :type _get_status:      bool \n
        usage:

            if self.config.BaseStation3200[1].zones[1].status_is_error():
                /* DO SOMETHING */

        :rtype: bool
        """
        self.get_status_if_necessary(_get_status)
        return self.object.ss == statuscodes.flow_fault

    #################################
    def status_is_over_budget(self, _get_status=False):
        """
        Convenience method to return if the device is not in a over budget.
        :param _get_status:     if true than get data
        :type _get_status:      bool \n
        usage:
    
            if self.config.BaseStation3200[1].zones[1].status_is_error():
                /* DO SOMETHING */
    
        :rtype: bool
        """
        self.get_status_if_necessary(_get_status)
        return self.object.ss == statuscodes.over_budget
    
    #################################
    def status_is_pressure_fault(self, _get_status=False):
        """
        Convenience method to return if the device is not in a over budget.
        :param _get_status:     if true than get data
        :type _get_status:      bool \n
        usage:
    
            if self.config.BaseStation3200[1].zones[1].status_is_error():
                /* DO SOMETHING */
    
        :rtype: bool
        """
        self.get_status_if_necessary(_get_status)
        return self.object.ss == statuscodes.pressure_fault
    
    #################################
    def status_transitioned_from_watering_to_soaking(self, _get_status=False):
        """
        Convenience method to return if the device went from watering to soaking.
        :param _get_status:     if true than get data
        :type _get_status:      bool \n
        usage:
    
            if self.config.BaseStation3200[1].zones[1].status_transitioned_from_watering_to_soaking():
                /* DO SOMETHING */
    
        :rtype: bool
        """
        # Get data if we haven't already
        self.get_status_if_necessary(_get_status)
        return self.object.last_ss == statuscodes.watering and self.object.ss in [statuscodes.soaking]
    
    #################################
    def status_transitioned_from_soaking_to_watering(self, _get_status=False):
        """
        Convenience method to return if the device went from soaking to watering.
        :param _get_status:     if true than get data
        :type _get_status:      bool \n
        usage:
    
            if self.config.BaseStation3200[1].zones[1].status_transitioned_from_soaking_to_watering():
                /* DO SOMETHING */
    
        :rtype: bool
        """
        # Get data if we haven't already
        self.get_status_if_necessary(_get_status)
        return self.object.last_ss == statuscodes.watering and self.object.ss == statuscodes.soaking

    #################################
    def status_transitioned_from_running_to_soaking(self, _get_status=False):
        """
        Convenience method to return if the device went from watering to soaking.
        :param _get_status:     if true than get data
        :type _get_status:      bool \n
        usage:

            if self.config.BaseStation3200[1].zones[1].status_transitioned_from_watering_to_soaking():
                /* DO SOMETHING */

        :rtype: bool
        """
        # Get data if we haven't already
        self.get_status_if_necessary(_get_status)
        return self.object.last_ss == statuscodes.running and self.object.ss in [statuscodes.soaking]

    #################################
    def status_transitioned_from_soaking_to_running(self, _get_status=False):
        """
        Convenience method to return if the device went from soaking to watering.
        :param _get_status:     if true than get data
        :type _get_status:      bool \n
        usage:

            if self.config.BaseStation3200[1].zones[1].status_transitioned_from_soaking_to_watering():
                /* DO SOMETHING */

        :rtype: bool
        """
        # Get data if we haven't already
        self.get_status_if_necessary(_get_status)
        return self.object.last_ss == statuscodes.running and self.object.ss == statuscodes.soaking

    #################################
    def status_transitioned_from_waiting_to_running(self, _get_status=False):
        """
        Convenience method to return if the device went from soaking to watering.
        :param _get_status:     if true than get data
        :type _get_status:      bool \n
        usage:

            if self.config.BaseStation3200[1].zones[1].status_transitioned_from_soaking_to_watering():
                /* DO SOMETHING */

        :rtype: bool
        """
        # Get data if we haven't already
        self.get_status_if_necessary(_get_status)
        return self.object.last_ss == statuscodes.waiting and self.object.ss == statuscodes.running
