from common.objects.base_classes.bicoders import BiCoder
from decimal import Decimal
from common.helper_methods import truncate_float_value

from common.imports import opcodes
from common.objects.statuses.bicoders.valve_statuses import ValveStatuses

from common.imports.types import BiCoderCommands, DeviceCommands,  ActionCommands

__author__ = 'bens'


class ValveBicoder(BiCoder):

    def __init__(self, _sn, _controller, _id, _address=0, _decoder_type=None):
        """
        :param _sn:         Serial number to set for BiCoder. \n
        :type _sn:          str \n

        :param _controller: Controller this object is "attached" to \n
        :type _controller:  common.objects.controllers.bl_32.BaseStation3200 |
                            common.objects.controllers.bl_sb.Substation \n

        :param _id:         The type of device this bicoder will be "attached" to. \n
        :type _id:          str \n

        :param _address:    The address associated with this bicoder on the controller. \n
        :type _address:     int \n
        
        :param _decoder_type: The type of decoder added to the two-wire.
        :type _decoder_type: str
        """
        # This is equivalent to: BiCoder.__init(self, _serial_number=_sn, _serial_connection=_ser)
        # This seems to be the "new classy" way in Python to init base classes.
        super(ValveBicoder, self).__init__(_serial_number=_sn, _controller=_controller, _address=_address)
        
        self.ty = self.bicoder_types.VALVE
        self.id = _id

        self.va = 0.23  # Solenoid Current
        self.vv = 28.7  # Solenoid Voltage
        
        # Used when moving bicoder from 3200 two-wire to substation two-wire. Only valve bicoders have this passed
        # in.
        self.decoder_type = _decoder_type

        # Attributes that we will send to the controller by default
        self.default_attributes = [
            (opcodes.solenoid_current, 'va'),
            (opcodes.solenoid_voltage, 'vv'),
            (opcodes.two_wire_drop, 'vt')
        ]

        # Valve biCoder status' module
        self.statuses = ValveStatuses(_bicoder_object=self)
        """:type: common.objects.statuses.bicoders.valve_statuses.ValveStatuses"""

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Setter Methods                                                                                                   #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def set_solenoid_current(self, _solenoid_current=None):
        """
        Sets the solenoid's current value. \n
        By passing in a value for '_va', method assumes this value to overwrite current value. \n

        :param _solenoid_current: Value to set for solenoid current.
        :type _solenoid_current: Integer, Float
        """
        # Check if user wants to overwrite current value
        if _solenoid_current is not None:

            # Verifies the solenoid current passed in is an integer / float type
            if not isinstance(_solenoid_current, (int, float)):
                e_msg = "Failed trying to set {0} {1}'s solenoid current. Invalid type passed in, expected int or " \
                        "float. Received type: {2}".format(
                            self.ty,                    # {0}
                            str(self.sn),               # {1}
                            type(_solenoid_current)     # {2}
                        )
                raise TypeError(e_msg)
            else:
                # Valid type, overwrite current value with new value
                self.va = _solenoid_current

        # Command for sending
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,         # {0}
            self.id,                    # {1}
            str(self.sn),               # {2}
            opcodes.solenoid_current,   # {3}
            str(self.va)                # {4}
        )

        try:
            # Attempt to set solenoid current for zone at the substation
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} {1}'s 'Solenoid Current' to: '{2}'".format(
                self.ty,        # {0}
                str(self.sn),   # {1}
                str(self.va)    # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set {0} {1}'s 'Solenoid Current' to: {2}".format(
                self.ty,        # {0}
                str(self.sn),   # {1}
                str(self.va)    # {2}
            ))

    #################################
    def set_solenoid_voltage(self, _solenoid_voltage=None):
        """
        Sets the solenoid's voltage value for this BiCoder. \n
        By passing in a value for '_solenoid_voltage', method assumes this value to overwrite current value. \n

        :param _solenoid_voltage: Value to set for solenoid voltage.
        :type _solenoid_voltage: Integer, Float
        """
        # Check if user wants to overwrite current value
        if _solenoid_voltage is not None:

            # Verify the correct type is passed in.
            if not isinstance(_solenoid_voltage, (int, float)):
                e_msg = "Failed trying to set {0} {1}'s solenoid voltage. Invalid type passed in, expected int or " \
                        "float. Received type: {2}".format(
                            self.ty,                    # {0}
                            str(self.sn),               # {1}
                            type(_solenoid_voltage)     # {2}
                        )
                raise TypeError(e_msg)
            else:
                # Valid type, overwrite current value with new value
                self.vv = _solenoid_voltage

        # Command for sending
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,         # {0}
            self.id,                    # {1}
            str(self.sn),               # {2}
            opcodes.solenoid_voltage,   # {3}
            str(self.vv)                # {4}
        )

        try:
            # Attempt to set solenoid voltage for zone at the substation
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} ({1})'s 'Solenoid Voltage' to: '{2}'".format(
                self.ty,        # {0}
                str(self.sn),   # {1}
                str(self.vv)    # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set {0} ({1})'s 'Solenoid Voltage' to: {2}".format(
                self.ty,        # {0}
                str(self.sn),   # {1}
                str(self.vv))   # {2}
            )

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Verifier Methods                                                                                                 #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    ################################
    def verify_solenoid_current(self, _data=None):
        """
        Verifies the solenoid current set for this Valve BiCoder. Expects the Substation's value and this
        instance's value to be equal.

        :param _data: Data Object that holds the substation's attributes. \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the Substation
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.get_data()

        # Compare status versus what is on the substation
        zn_va_on_sb = Decimal(data.get_value_string_by_key(opcodes.solenoid_current))
        rounded_va = Decimal(round(number=self.va, ndigits=2))
        # If we are a faux device, use 0.02 as allowance
        if Decimal(abs(rounded_va - zn_va_on_sb)) > Decimal(0.02) and self.controller_is_in_faux_io():
            e_msg = "Unable to verify {0} {1}'s solenoid current. Received: {2}, Expected: {3}".format(
                self.ty,            # {0}
                str(self.sn),       # {1}
                str(zn_va_on_sb),   # {2}
                str(self.va)        # {3}
            )
            raise ValueError(e_msg)
        # Else if we are a real device, the current must be between 0.1 amps and 0.3 amps for a healthy solenoid
        elif (Decimal(0.1) > Decimal(zn_va_on_sb) or Decimal(zn_va_on_sb) > Decimal(0.3)) and not self.controller_is_in_faux_io():
            e_msg = "Unable to verify {0} {1}'s (real device) solenoid current. Received: {2}, Expected: {3}".format(
                self.ty,            # {0}
                str(self.sn),       # {1}
                str(zn_va_on_sb),   # {2}
                str(self.va)        # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0} {1}'s solenoid current value: '{2}' on substation".format(
                self.ty,        # {0}
                str(self.sn),   # {1}
                str(self.va)    # {2}
            ))
            return True

    #################################
    def verify_solenoid_voltage(self, _data=None):
        """
        Verifies the solenoid voltage set for this Valve BiCoder. Expects the Substation's value and this
        instance's value to be equal.

        :param _data: Data Object that holds the substation's attributes. \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the Substation
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.get_data()

        # Compare status versus what is on the substation
        zn_vv_on_sb = Decimal(data.get_value_string_by_key(opcodes.solenoid_voltage))
        rounded_vv = Decimal(round(number=self.vv, ndigits=2))
        # If we are a faux device, use 0.02 as allowance
        if Decimal(abs(rounded_vv - zn_vv_on_sb)) > Decimal(0.02) and self.controller_is_in_faux_io():
            e_msg = "Unable to verify {0} {1}'s solenoid voltage. Received: {2}, Expected: {3}".format(
                self.ty,            # {0}
                str(self.sn),       # {1}
                str(zn_vv_on_sb),   # {2}
                str(self.vv)        # {3}
            )
            raise ValueError(e_msg)
        # Else if we are a real device, use 25.0 as the lower limit for solenoid voltage
        elif Decimal(zn_vv_on_sb) < Decimal(25.0) and not self.controller_is_in_faux_io():
            e_msg = "Unable to verify {0} {1}'s (real device) solenoid voltage. Received: {2}, Expected: {3}".format(
                self.ty,            # {0}
                str(self.sn),       # {1}
                str(zn_vv_on_sb),   # {2}
                str(self.vv)        # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0} {1}'s solenoid voltage value: '{2}' on substation".format(
                self.ty,        # {0}
                str(self.sn),   # {1}
                str(self.vv)    # {2}
            ))
            return True

    #################################
    def verify_who_i_am(self, _expected_status=None, _for_substation=False):
        """
        Verifier wrapper which verifies all attributes for this biCoder. \n
        Get all information about the biCoder from the substation. \n

        :param _expected_status: The status code we want to verify against. (optional) \n
        :type _expected_status: str
        """
        data = self.get_data(_for_substation=_for_substation)

        if _expected_status is not None:
            self.verify_status(_status=_expected_status, _data=data)

        # Verify valve specific attributes
        self.verify_serial_number(_data=data)
        self.verify_solenoid_current(_data=data)
        self.verify_solenoid_voltage(_data=data)
        self.verify_two_wire_drop_value(_data=data)

        return True

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Other Methods                                                                                                    #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def self_test_and_update_object_attributes(self):
        """
        this does a self test on the device
        than does a get date a resets the attributes of the device to match the controller
        """
        try:
            self.do_self_test()
            self.get_data()
            self.va = float(self.data.get_value_string_by_key(opcodes.solenoid_current))
            self.vv = float(self.data.get_value_string_by_key(opcodes.solenoid_voltage))
            self.vt = float(self.data.get_value_string_by_key(opcodes.two_wire_drop))
        except Exception as e:
            e_msg = "Exception occurred trying to updating attributes of the Valve Bicoder {0} object." \
                    " Solenoid Current: '{1}', Solenoid Voltage '{2}'.\n\tException: {3}".format(
                        str(self.sn),    # {0}
                        str(self.va),    # {1}
                        str(self.vv),    # {2}
                        e.message        # {3}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully updated attributes of the Valve Bicoder {0} object. "
                  "Solenoid Current Value: '{1}', "
                  "Solenoid Voltage'Value: '{2}': ".format(
                        str(self.sn),  # {0}
                        str(self.va),  # {1}
                        str(self.vv),  # {2}
                    ))

    #############################
    def add_to_zone(self, _address):
        """
        Add this bicoder to a zone.

        :param _address: Address of the new zone. \n
        :type _address:  int \n

        :return:
        :rtype:
        """
        if not isinstance(_address, int):
            e_msg = "Exception occurred trying to add a Valve Bicoder {0} to Zone {1}: The value '{2}' must " \
                    "be an int".format(
                        str(self.sn),   # {0}
                        str(_address),  # {1}
                        str(_address)   # {2}
                    )
            raise Exception(e_msg)

        else:
            # If a zone with the specified address has not already been used, create a new zone object
            if _address not in self.controller.base_station_3200.zones.keys():
                # If we were originally created for the Substation, then the bicoder doesn't get a "base_station"
                # reference until after this code completes. So, if thats the case, then access the "base_station"
                # through the Substation, otherwise, access through our local "base_station" reference.
                if self.controller.is_substation():
                    self.controller.base_station_3200.add_zone_to_controller(_address=_address, _serial_number=self.sn)
                else:
                    self.base_station.add_zone_to_controller(_address=_address, _serial_number=self.sn)
                
            else:
                e_msg = "A zone with address {0} has already been created, valve bicoder ({1}) could not" \
                        "be addressed.".format(_address, self.sn)
                raise ValueError(e_msg)

    #############################
    def add_to_master_valve(self, _address):
        """
        Add this bicoder to a master valve.

        :param _address: Address of the new master valve. \n
        :type _address:  int \n

        :return:
        :rtype:
        """
        if not isinstance(_address, int):
            e_msg = "Exception occurred trying to add a Valve Bicoder {0} to Master Valve {1}: The value '{2}' must " \
                    "be an int".format(
                str(self.sn),   # {0}
                str(_address),  # {1}
                str(_address)   # {2}
            )
            raise Exception(e_msg)

        else:
            # If a master valve with the specified address has not already been used, create a new master valve object
            if _address not in self.controller.base_station_3200.master_valves.keys():
                # If we were originally created for the Substation, then the bicoder doesn't get a "base_station"
                # reference until after this code completes. So, if thats the case, then access the "base_station"
                # through the Substation, otherwise, access through our local "base_station" reference.
                if self.controller.is_substation():
                    self.controller.base_station_3200.add_master_valve_to_controller(_address=_address, _serial_number=self.sn)
                else:
                    self.base_station.add_master_valve_to_controller(_address=_address, _serial_number=self.sn)
            else:
                e_msg = "A master valve with address {0} has already been created, valve bicoder ({1}) could not" \
                        "be addressed.".format(_address, self.sn)
                raise ValueError(e_msg)

    #############################
    def add_to_pump(self, _address):
        """
        Add this bicoder to a pump.

        :param _address: Address of the new pump. \n
        :type _address:  int \n

        :return:
        :rtype:
        """
        if not isinstance(_address, int):
            e_msg = "Exception occurred trying to add a Valve Bicoder {0} to Pump {1}: The value '{2}' must " \
                    "be an int".format(
                        str(self.sn),   # {0}
                        str(_address),  # {1}
                        str(_address)   # {2}
                    )
            raise Exception(e_msg)

        else:
            # If a pump with the specified address has not already been used, create a new pump object
            if _address not in self.controller.base_station_3200.pumps.keys():
                # If we were originally created for the Substation, then the bicoder doesn't get a "base_station"
                # reference until after this code completes. So, if thats the case, then access the "base_station"
                # through the Substation, otherwise, access through our local "base_station" reference.
                if self.controller.is_substation():
                    self.controller.base_station_3200.add_pump_to_controller(_address=_address, _serial_number=self.sn)
                else:
                    self.base_station.add_pump_to_controller(_address=_address, _serial_number=self.sn)
            else:
                e_msg = "A pump with address {0} has already been created, valve bicoder ({1}) could not" \
                        "be addressed.".format(_address, self.sn)
                raise ValueError(e_msg)
