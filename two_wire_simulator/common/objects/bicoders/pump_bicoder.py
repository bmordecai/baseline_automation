from common.objects.base_classes.bicoders import BiCoder
from common.imports import opcodes
from common.objects.statuses.bicoders.pump_statuses import PumpStatuses
from common.imports.types import BiCoderCommands, DeviceCommands,  ActionCommands
__author__ = 'bens'


class PumpBicoder(BiCoder):

    def __init__(self, _sn, _controller, _address=0):
        """
        :param _sn:         Serial number to set for BiCoder. \n
        :type _sn:          str \n

        :param _controller: Controller this object is "attached" to \n
        :type _controller:  common.objects.controllers.bl_32.BaseStation3200 |
                            common.objects.controllers.bl_sb.Substation \n

        :param _address:    The address associated with this bicoder on the controller. \n
        :type _address:     int \n
        """
        # This is equivalent to: BiCoder.__init(self, _serial_number=_sn, _serial_connection=_ser)
        # This seems to be the "new classy" way in Python to init base classes.
        super(PumpBicoder, self).__init__(_serial_number=_sn, _controller=_controller, _address=_address)
        self.ty = self.bicoder_types.PUMP
        self.id = self.device_type.PUMP

        self.va = 0.23  # Solenoid Current
        self.vv = 28.7  # Solenoid Voltage

        # Pump biCoder status' module
        self.statuses = PumpStatuses(_bicoder_object=self)
        """:type: common.objects.statuses.bicoders.pump_statuses.PumpStatuses"""

    #################################
    def self_test_and_update_object_attributes(self):
        """
        this does a self test on the device
        than does a get date a resets the attributes of the device to match the controller
        self.va         # Solenoid Current
        self.vv         # Solenoid Voltage
        self.vt         # Two wire drop
        """
        try:
            self.do_self_test()
            self.get_data()
            self.va = float(self.data.get_value_string_by_key(opcodes.solenoid_current))
            self.vv = float(self.data.get_value_string_by_key(opcodes.solenoid_voltage))
            self.vt = float(self.data.get_value_string_by_key(opcodes.two_wire_drop))
        except Exception as e:
            e_msg = "Exception occurred trying to update attributes of the pump {0} object." \
                    " Solenoid Current Value: '{1}'," \
                    " Solenoid Voltage'Value: '{2}'," \
                    " Two Wire Drop Value: '{3}'".format(
                        str(self.sn),   # {0}
                        str(self.va),   # {1}
                        str(self.vv),   # {2}
                        str(self.vt)    # {3}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully updated attributes of the pump {0} object. "
                  "Solenoid Current Value: '{1}', "
                  "Solenoid Voltage'Value: '{2}', "
                  "Two Wire Drop Value: '{3}'".format(
                        str(self.sn),  # {0}
                        str(self.va),  # {1}
                        str(self.vv),  # {2}
                        str(self.vt)   # {3}
                    ))

    #############################
    def add_to_pump(self, _address):
        """
        Add this bicoder to a pump.

        :param _address: Address of the new pump. \n
        :type _address:  int \n

        :return:
        :rtype:
        """
        if not isinstance(_address, int):
            e_msg = "Exception occurred trying to add a Valve Bicoder {0} to Pump {1}: The value '{2}' must " \
                    "be an int".format(
                        str(self.sn),   # {0}
                        str(_address),  # {1}
                        str(_address)   # {2}
                    )
            raise Exception(e_msg)

        else:
            # If a pump with the specified address has not already been used, create a new pump object
            if _address not in self.controller.base_station_3200.pumps.keys():
                # If we were originally created for the Substation, then the bicoder doesn't get a "base_station"
                # reference until after this code completes. So, if thats the case, then access the "base_station"
                # through the Substation, otherwise, access through our local "base_station" reference.
                if self.controller.is_substation():
                    self.controller.base_station_3200.add_pump_to_controller(_address=_address, _serial_number=self.sn)
                else:
                    self.base_station.add_pump_to_controller(_address=_address, _serial_number=self.sn)
            else:
                e_msg = "A pump with address {0} has already been created, valve bicoder ({1}) could not" \
                        "be addressed.".format(_address, self.sn)
                raise ValueError(e_msg)
