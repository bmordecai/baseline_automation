from common.objects.base_classes.bicoders import BiCoder
from common.objects.statuses.bicoders.analog_statuses import AnalogStatuses

from common.imports import opcodes
from common.imports.types import BiCoderCommands, DeviceCommands,  ActionCommands

from decimal import Decimal

__author__ = 'bens'


class AnalogBicoder(BiCoder):

    def __init__(self, _sn, _controller, _address=0):
        """
        :param _sn:         Serial number to set for BiCoder. \n
        :type _sn:          str \n

        :param _controller: Controller this object is "attached" to \n
        :type _controller:  common.objects.controllers.bl_32.BaseStation3200 |
                            common.objects.controllers.bl_sb.Substation \n

        :param _address:    The address associated with this bicoder on the controller. \n
        :type _address:     int \n
        """
        # This is equivalent to: BiCoder.__init(self, _serial_number=_sn, _serial_connection=_ser)
        # This seems to be the "new classy" way in Python to init base classes.
        super(AnalogBicoder, self).__init__(_serial_number=_sn, _controller=_controller, _address=_address)
        self.ty = self.bicoder_types.ANALOG
        self.id = self.device_type.PRESSURE_SENSOR
        
        # Used when moving device from 3200 two-wire to substation two-wire
        self.decoder_type = self.id  # AN
        self.vu = None  # (PSI / GPM)
        self.vl = 0.00  # {units for 4 mA reading}
        self.vh = 150.00  # {units for 20 mA reading}
        self.vr = 0.00  # {reading converted to units} read only
        self.va = 0.00  # {raw value of mA reading}

        # Attributes that we will send to the controller by default
        self.default_attributes = [
            (opcodes.low_milli_amp_setting, 'VL'),       # {units for 4 mA reading}
            (opcodes.high_milli_amp_setting, 'VH'),      # {units for 20 mA reading}
            (opcodes.analog_converted_to_units, 'VR'),   # {reading converted to units}
            (opcodes.raw_analog_readings, 'VA'),          # {raw value of mA reading}
            (opcodes.two_wire_drop, 'vt')
        ]

        # Analog biCoder status' module
        self.statuses = AnalogStatuses(_bicoder_object=self)
        """:type: common.objects.statuses.bicoders.analog_statuses.AnalogStatuses"""

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Setter Methods                                                                                                   #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def set_pressure_value_in_psi(self, _psi=None):
        """
        Set the pressure for the analog BiCoder (Faux IO Only) \n

        :param _psi: Value to set the analog BiCoder pressure reading to (in psi) \n
        :type _psi: Integer, Float
        """
        # If a presure is passed in for overwrite
        self.vu = 'PSI'
        if _psi is not None:

            # Verifies the moisture biCoder reading passed in is an int or float value
            if not isinstance(_psi, (int, float)):
                e_msg = "Failed trying to set {0} ({1})'s pressure. Invalid argument type, expected an int " \
                        "or float, received: {2}".format(self.ty, self.sn, type(_psi))
                raise TypeError(e_msg)
            # Verifies the moisture biCoder reading passed in is an int or float value
            if 0.0 > _psi > 150.0:
                e_msg = "Failed trying to set {0} ({1})'s pressure. value must be between 0.0 and 150.0 " \
                        "or float, received: {2}".format(self.ty, self.sn, type(_psi))
                raise TypeError(e_msg)
            else:
                # Overwrite current value with new value
                self.vr = float(_psi)

        # Round the float to the hundredths place
        self.vr = round(self.vr, 2)

        # Ensure that this value will be sent with a number in the tenths and hundredths place
        self.vr = Decimal(self.vr).quantize(Decimal(".00"))

        # Command for sending 'SET,AN=PFS0001,
        command = "{0},{1}={2},{3}={4},{5}={6}".format(
            ActionCommands.SET,                 # {0}
            self.id,                            # {1}
            str(self.sn),                       # {2}
            opcodes.sensor_value,               # {3}
            str(self.vu),                       # {4}
            opcodes.analog_converted_to_units,  # {5}
            str(self.vr)                        # {6}
        )

        try:
            # Attempt to set pressure for analog biCoder
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} ({1})'s 'Pressure' to: '{2}'".format(
                self.ty,        # {0}
                str(self.sn),   # {1}
                str(self.vr)    # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set {0} ({1})'s 'Pressure' to: {2}".format(
                self.ty,        # {0}
                str(self.sn),   # {1}
                str(self.vr))   # {2}
            )

    #################################
    def set_pressure_value_in_milliamps(self, _milliamps=None):
        """
        Set the pressure for the analog BiCoder (Faux IO Only) \n

        :param _milliamps: Value to set the analog BiCoder pressure reading to (in psi) \n
        :type _milliamps: Integer, Float
        """
        # If a presure is passed in for overwrite
        self.vu = 'PSI'
        if _milliamps is not None:

            # Verifies the moisture biCoder reading passed in is an int or float value
            if not isinstance(_milliamps, (int, float)):
                e_msg = "Failed trying to set {0} ({1})'s pressure. Invalid argument type, expected an int " \
                        "or float, received: {2}".format(self.ty, self.sn, type(_milliamps))
                raise TypeError(e_msg)
            # Verifies the moisture biCoder reading passed in is an int or float value
            if 0.0 >= _milliamps >= 150.0:
                e_msg = "Failed trying to set {0} ({1})'s pressure. value must be between 4 ma and 20 ma " \
                        "or float, received: {2}".format(self.ty, self.sn, type(_milliamps))
                raise TypeError(e_msg)
            else:
                # Overwrite current value with new value
                self.va = _milliamps

        # Command for sending
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,             # {0}
            self.id,                        # {1}
            str(self.sn),                   # {2}
            opcodes.pressure_sensor_ps,     # {3}
            str(self.va))                   # {4}

        try:
            # Attempt to set pressure for analog biCoder
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} ({1})'s 'Pressure' to: '{2}'"\
                .format(
                    self.ty,        # {0}
                    str(self.sn),   # {1}
                    str(self.va)    # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set {0} ({1})'s 'Pressure' to: {2}".format(
                self.ty,        # {0}
                str(self.sn),   # {1}
                str(self.va))   # {2}
            )

    #################################
    def set_analog_four_milli_reading(self, _four_milli_amps=None):
        """
        Set the analog units for 4 mA reading for the analog BiCoder (Faux IO Only) \n

        :param _four_milli_amps: Value to set the analog BiCoder analog reading to (in milli_amps) \n
        :type _four_milli_amps: Integer, Float
        """
        # If a analog percent is passed in for overwrite
        if _four_milli_amps is not None:

            # Verifies the analog biCoder reading passed in is an int or float value
            if not isinstance(_four_milli_amps, (int, float)):
                e_msg = "Failed trying to set {0} ({1})'s analog percent. Invalid argument type, expected an int " \
                        "or float, received: {2}"\
                    .format(
                        self.ty,                # {0}
                        self.sn,                # {1}
                        type(_four_milli_amps)  # {2}
                    )
                raise TypeError(e_msg)
            else:
                # Overwrite current value with new value
                self.vl = _four_milli_amps

        # Command for sending
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                 # {0}
            self.id,                            # {1}
            str(self.sn),                       # {2}
            opcodes.low_milli_amp_setting,      # {3}
            str(self.vl)                        # {4}
        )

        try:
            # Attempt to set analog _four_mili_amps for analog biCoder at the substation in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} ({1})'s '_four_milli_amp reading' to: '{2}'".format(
                self.ty,        # {0}
                str(self.sn),   # {1}
                str(self.vl)    # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set {0} ({1})'s '_four_milli_amp_reading' to: {2}".format(
                self.ty,        # {0}
                str(self.sn),   # {1}
                str(self.vl))   # {2}
            )

    #################################
    def set_analog_twenty_milli_reading(self, _twenty_milli_amps=None):
        """
        Set the analog units for 20 mA reading for the analog BiCoder (Faux IO Only) \n

        :param _twenty_mili_amps: Value to set the analog BiCoder analog reading to (in float) \n
        :type _twenty_mili_amps: Integer, Float
        """
        # If a analog percent is passed in for overwrite
        if _twenty_milli_amps is not None:

            # Verifies the analog biCoder reading passed in is an int or float value
            if not isinstance(_twenty_milli_amps, (int, float)):
                e_msg = "Failed trying to set {0} ({1})'s 'twenty_milli_amp reading'. " \
                        "Invalid argument type, expected an int or float, received: {2}".format(
                                                                    self.ty,                    # {0}
                                                                    self.sn,                    # {1}
                                                                    type(_twenty_milli_amps))   # {2}
                raise TypeError(e_msg)
            else:
                # Overwrite current value with new value
                self.vh = _twenty_milli_amps

        # Command for sending
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                  # {0}
            self.id,                             # {1}
            str(self.sn),                        # {2}
            opcodes.high_milli_amp_setting,      # {3}
            str(self.vh)                         # {4}
        )
        try:
            # Attempt to set analog percent for analog biCoder at the substation in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} ({1})'s 'twenty_milli_amp reading' to: '{2}'".format(
                                            self.ty,        # {0}
                                            str(self.sn),   # {1}
                                            str(self.vh))   # {2}
            raise Exception(e_msg)
        else:
            print("Successfully set {0} ({1})'s 'Twenty_milli_amp_readung' to: {2}".format(
                self.ty,        # {0}
                str(self.sn),   # {1}
                str(self.vh))   # {2}
            )

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Verifier Methods                                                                                                 #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def verify_analog_four_milli_reading(self, _data=None):
        """
        Verifies the analog 4_milli_reading set for this Analog BiCoder. Expects the substation's value
        and this instance's value to be equal.

        :param _data: Data Object that holds the substation's attributes. \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the Substation
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.get_data()

        # Gets the analog percent from the data object
        four_milli_reading = float(data.get_value_string_by_key(opcodes.low_milli_amp_setting))

        rounded_vl = round(number=self.vl, ndigits=2)
        # The allowable difference between the controller value and the value in the test object is .02. However,
        # when using a float, extra numbers are added to the number, for example .02000000000012. In order to account
        # the possibility of extra numbers, the restraint was changed to .021.
        if abs(rounded_vl - four_milli_reading) > 0.021:
            # Compare status versus what is on the substation
            if self.vl != four_milli_reading:
                e_msg = "Unable to verify {0} ({1})'s analog four milli_amp setting. Received: {2}, Expected: {3}".format(
                            self.ty,                    # {0}
                            str(self.sn),               # {1}
                            str(four_milli_reading),    # {2}
                            str(self.vl)                # {3}
                        )
                raise ValueError(e_msg)
        # If no exception is thrown, print a success message
        print("Verified {0} ({1})'s analog reading: '{2}' on substation".format(
            self.ty,          # {0}
            str(self.sn),     # {1}
            str(self.vl)      # {2}
        ))
        return True

    #################################
    def verify_analog_twenty_milli_reading(self, _data=None):
        """
        Verifies the analog twenty_milli_reading set for this Analog BiCoder. Expects the substation's value
        and this instance's value to be equal.

        :param _data: Data Object that holds the substation's attributes. \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the Substation
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.get_data()

        # Gets the analog percent from the data object
        twenty_milli_reading= float(data.get_value_string_by_key(opcodes.high_milli_amp_setting))

        rounded_vh = round(number=self.vh, ndigits=2)
        # The allowable difference between the controller value and the value in the test object is .02. However,
        # when using a float, extra numbers are added to the number, for example .02000000000012. In order to account
        # the possibility of extra numbers, the restraint was changed to .021.
        if abs(rounded_vh - twenty_milli_reading) > 0.021:
            # Compare status versus what is on the substation
            if self.vh != twenty_milli_reading:
                e_msg = "Unable to verify {0} ({1})'s analog twenty milli_amp setting. Received: {2}, Expected: {3}"\
                    .format(
                            self.ty,                    # {0}
                            str(self.sn),               # {1}
                            str(twenty_milli_reading),  # {2}
                            str(self.vh)                # {3}
                        )
                raise ValueError(e_msg)
        # If no exception is thrown, print a success message
        print("Verified {0} ({1})'s analog reading: '{2}' on substation".format(
            self.ty,          # {0}
            str(self.sn),     # {1}
            str(self.vh)      # {2}
        ))
        return True

    #################################
    def verify_pressure_reading(self, _data=None):
        """
        Verifies the pressure value set for this Analog BiCoder. Expects the substation's value
        and this instance's value to be equal.

        :param _data: Data Object that holds the substation's attributes. \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the Controller
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.get_data()

        # Gets the analog percent from the data object
        pressure = float(data.get_value_string_by_key(opcodes.analog_converted_to_units))

        rounded_vr = round(number=self.vr, ndigits=2)
        # The allowable difference between the controller value and the value in the test object is .02. However,
        # when using a float, extra numbers are added to the number, for example .02000000000012. In order to account
        # the possibility of extra numbers, the restraint was changed to .021.
        if abs(rounded_vr - pressure) > opcodes.tolerance:
            # Compare status versus what is on the substation
            if self.vr != pressure:
                e_msg = "Unable to verify {0} ({1})'s pressure setting. Received: {2}, Expected: {3}".format(
                    self.ty,        # {0}
                    str(self.sn),   # {1}
                    str(pressure),  # {2}
                    str(self.vr)    # {3}
                )
                raise ValueError(e_msg)
        # If no exception is thrown, print a success message
        print("Verified {0} ({1})'s pressure: '{2}' on substation".format(
            self.ty,          # {0}
            str(self.sn),     # {1}
            str(self.vr)      # {2}
        ))
        return True

    #################################
    def verify_who_i_am(self, _expected_status=None):
        """
        Verifier wrapper which verifies all attributes for this biCoder. \n
        Get all information about the biCoder from the substation. \n

        :param _expected_status: The status code we want to verify against. (optional) \n
        :type _expected_status: str
        """
        data = self.get_data()

        if _expected_status is not None:
            self.verify_status(_status=_expected_status, _data=data)

        # Verify analog specific attributes
        self.verify_serial_number(_data=data)
        self.verify_analog_four_milli_reading(_data=data)
        self.verify_analog_twenty_milli_reading(_data=data)
        self.verify_two_wire_drop_value(_data=data)

        return True

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Other Methods                                                                                                    #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def self_test_and_update_object_attributes(self):
        """
        this does a self test on the device
        than does a get date a resets the attributes of the device to match the controller
        """
        try:
            self.do_self_test()
            self.get_data()
            self.vl = float(self.data.get_value_string_by_key(opcodes.low_milli_amp_setting))
            self.vh = float(self.data.get_value_string_by_key(opcodes.high_milli_amp_setting))
            self.vr = float(self.data.get_value_string_by_key(opcodes.analog_converted_to_units))
            self.vr = Decimal(self.vr).quantize(Decimal(".00"))
            self.va = float(self.data.get_value_string_by_key(opcodes.raw_analog_readings))
            self.vt = float(self.data.get_value_string_by_key(opcodes.two_wire_drop))
        except Exception as e:
            e_msg = "Exception occurred trying to update attributes of the pressure sensor {0} object." \
                    " Units for 4 mA reading: '{1}'," \
                    " Units for 20 mA reading: '{2}'," \
                    " Reading converted to Units: '{3}'," \
                    " Raw value of mA reading: '{4}'," \
                    " Two Wire Drop Value: '{5}'".format(
                        str(self.sn),   # {0}
                        str(self.vl),   # {1}
                        str(self.vh),   # {2}
                        str(self.vr),   # {3}
                        str(self.va),   # {4}
                        str(self.vt)    # {5}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully updated attributes of the pressure sensor {0} object. "
                  " Units for 4 mA reading: '{1}',"
                  " Units for 20 mA reading: '{2}',"
                  " Reading converted to Units: '{3}',"
                  " Raw value of mA reading: '{4}',"
                  " Two Wire Drop Value: '{5}'".format(
                        str(self.sn),   # {0}
                        str(self.vl),   # {1}
                        str(self.vh),   # {2}
                        str(self.vr),   # {3}
                        str(self.va),   # {4}
                        str(self.vt)    # {5}
                    ))

    #############################
    def add_to_pressure_sensor(self, _address):
        """
        Add this bicoder to a pressure sensor.

        :param _address: Address of the new pressure sensor. \n
        :type _address:  int \n

        :return:
        :rtype:
        """
        if not isinstance(_address, int):
            e_msg = "Exception occurred trying to add a Pressure Bicoder {0} to Pressure Sensor {1}: The value '{2}' " \
                    "must be an int".format(
                        str(self.sn),   # {0}
                        str(_address),  # {1}
                        str(_address)   # {2}
                    )
            raise Exception(e_msg)

        else:
            # If a pressure sensor with the specified address has not already been used, create a new object
            if _address not in self.controller.base_station_3200.pressure_sensors.keys():
                # If we were originally created for the Substation, then the bicoder doesn't get a "base_station"
                # reference until after this code completes. So, if thats the case, then access the "base_station"
                # through the Substation, otherwise, access through our local "base_station" reference.
                if self.controller.is_substation():
                    self.controller.base_station_3200.add_pressure_sensor_to_controller(_address=_address, _serial_number=self.sn)
                else:
                    self.base_station.add_pressure_sensor_to_controller(_address=_address, _serial_number=self.sn)
            else:
                e_msg = "A pressure sensor with address {0} has already been created, pressure bicoder ({1}) could " \
                        "not be addressed.".format(_address, self.sn)
                raise ValueError(e_msg)
