from common.objects.base_classes.bicoders import BiCoder

from common.imports import opcodes
from common.imports.types import BiCoderCommands, DeviceCommands,  ActionCommands

__author__ = 'bens'


class AlertRelayBicoder(BiCoder):

    def __init__(self, _sn, _controller, _address=0):
        """
        :param _sn:         Serial number to set for BiCoder. \n
        :type _sn:          str \n

        :param _controller: Controller this object is "attached" to \n
        :type _controller:  common.objects.controllers.bl_10.BaseStation1000 \n

        :param _address:    The address associated with this bicoder on the controller. \n
        :type _address:     int \n
        """
        # This is equivalent to: BiCoder.__init(self, _serial_number=_sn, _serial_connection=_ser)
        # This seems to be the "new classy" way in Python to init base classes.
        super(AlertRelayBicoder, self).__init__(_serial_number=_sn, _controller=_controller, _address=_address)
        self.ty = self.bicoder_types.ALERT
        self.id = self.device_type.ALERT_RELAY

        # Used when moving device from 3200 two-wire to substation two-wire
        self.decoder_type = self.id  # SW

        self.vc = opcodes.contacts_closed       # Contact State

        # Attributes that we will send to the controller by default
        self.default_attributes = [
            (opcodes.contacts_state, 'vc'),
            (opcodes.two_wire_drop, 'vt')
        ]

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Setter Methods                                                                                                   #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def set_contact_state(self, _contact_state=None):
        """
        Set the contact for the alert relay BiCoder (Faux IO Only) \n

        :param _contact_state: Open ('OP') or Closed ('CL') \n
        :type _contact_state: str
        """
        # If a contact alert relay value is passed in for overwrite
        if _contact_state is not None:

            # Verifies the contact alert relay value passed in is an int or float value
            if not isinstance(_contact_state, str):
                e_msg = "Failed trying to set {0} ({1})'s contact state. Invalid argument type, expected a " \
                        "str, received: {2}".format(self.ty, str(self.sn), type(_contact_state))
                raise TypeError(e_msg)
            else:
                # Overwrite current value
                self.vc = _contact_state

        # Command for sending
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,         # {0}
            self.id,                    # {1}
            str(self.sn),               # {2}
            opcodes.contacts_state,     # {3}
            str(self.vc)                # {4}
        )

        try:
            # Attempt to set contact state for alert relay biCoder at the substation in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} ({1})'s 'Contact State' to: '{2}'". \
                format(self.ty, str(self.sn), str(self.vc))
            raise Exception(e_msg)
        else:
            print("Successfully set {0} ({1})'s 'Contact State' to: {2}".format(
                self.ty, str(self.sn), str(self.vc)))

    #################################
    def set_contact_open(self):
        """
        Set the contact for the alert relay BiCoder (Faux IO Only) to 'Closed' \n

        """
        # Overwrite current value
        self.vc = opcodes.open

        # Command for sending
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,         # {0}
            self.id,                    # {1}
            str(self.sn),               # {2}
            opcodes.contacts_state,     # {3}
            str(self.vc)                # {4}
        )

        try:
            # Attempt to set contact state for alert relay biCoder at the substation in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} ({1})'s 'Contact State' to: '{2}'". \
                format(
                        self.ty,        # {0}
                        str(self.sn),   # {1}
                        str(self.vc)    # {2}
                    )
            raise Exception(e_msg)
        print("Successfully set {0} ({1})'s 'Contact State' to: {2}".format(
            self.ty,        # {0}
            str(self.sn),   # {1}
            str(self.vc))   # {2}
        )

    #################################
    def set_contact_closed(self):
        """
        Set the contact for the alert relay BiCoder (Faux IO Only) to 'Closed' \n

        """
        # Overwrite current value
        self.vc = opcodes.closed

        # Command for sending
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,         # {0}
            self.id,                    # {1}
            str(self.sn),               # {2}
            opcodes.contacts_state,     # {3}
            str(self.vc)                # {4}
        )

        try:
            # Attempt to set contact state for alert relay biCoder at the substation in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} ({1})'s 'Contact State' to: '{2}'". \
                format(
                    self.ty,        # {0}
                    str(self.sn),   # {1}
                    str(self.vc)    # {2}
                )
            raise Exception(e_msg)
        print("Successfully set {0} ({1})'s 'Contact State' to: {2}".format(
            self.ty,        # {0}
            str(self.sn),   # {1}
            str(self.vc))   # {2}
        )
    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Verifier Methods                                                                                                 #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def verify_contact_state(self, _data=None):
        """
        Verifies the contact state for this Alert relay BiCoder. Expects the substation's value
        and this instance's value to be equal.

        :param _data: Data Object that holds the substation's attributes. \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the Substation
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.get_data()

        # Gets the contact state from the data object
        contact_state = data.get_value_string_by_key(opcodes.contacts_state)

        # Compare status versus what is on the controller
        if self.vc != contact_state and self.controller_is_in_faux_io():
            e_msg = "Unable to verify {0} ({1})'s contact state reading. Received: {2}, Expected: {3}".format(
                self.ty,                # {0}
                str(self.sn),           # {1}
                str(contact_state),     # {2}
                str(self.vc)            # {3}
            )
            raise ValueError(e_msg)
        # Else if controller is using real devices, if we don't at least get a value, assume unable to communicate with
        # device, and bomb out
        elif self.vc != contact_state and not self.controller_is_in_faux_io():
            e_msg = "Unable to verify {0} ({1})'s (real device) contact state reading. Received: {2}, " \
                    "Expected: {3}".format(
                        self.ty,                # {0}
                        str(self.sn),           # {1}
                        str(contact_state),     # {2}
                        self.vc                 # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified {0} ({1})'s contact state: '{2}' on substation".format(
                self.ty,          # {0}
                str(self.sn),     # {1}
                str(self.vc)      # {2}
            ))
            return True

    #################################
    def verify_who_i_am(self, _expected_status=None):
        """
        Verifier wrapper which verifies all attributes for this biCoder. \n
        Get all information about the biCoder from the substation. \n

        :param _expected_status: The status code we want to verify against. (optional) \n
        :type _expected_status: str
        """
        data = self.get_data()

        if _expected_status is not None:
            self.verify_status(_status=_expected_status, _data=data)

        # Verify alert relay specific attributes
        self.verify_serial_number(_data=data)
        self.verify_contact_state(_data=data)
        self.verify_two_wire_drop_value(_data=data)

        return True

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Other Methods                                                                                                    #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def self_test_and_update_object_attributes(self):
        """
        This does a self test on the device.
        Than does a get date a resets the attributes of the device to match the controller.
        """
        try:
            self.do_self_test()
            self.get_data()
            self.vc = self.data.get_value_string_by_key(opcodes.contacts_state)
            self.vt = float(self.data.get_value_string_by_key(opcodes.two_wire_drop))
        except Exception as e:
            e_msg = "Exception occurred trying to update attributes of the Alert relay Bicoder {0} object." \
                    "Contact State: '{1}'," \
                    "Two Wire Drop Value: '{2}'.\n\tException: {3}".format(
                        str(self.sn),   # {0}
                        str(self.vc),   # {1}
                        str(self.vt),   # {2}
                        e.message       # {3}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully updated attributes of the Alert relay Bicoder {0} object. "
                  "Contact State: '{1}', "
                  "Two Wire Drop Value: '{2}'".format(
                        str(self.sn),  # {0}
                        str(self.vc),  # {1}
                        str(self.vt)   # {2}
                    ))

    #############################
    def add_to_alert_relay(self, _address):
        """
        Add this bicoder to an alert relay.

        :param _address: Address of the new alert relay. \n
        :type _address:  int \n

        :return:
        :rtype:
        """
        if not isinstance(_address, int):
            e_msg = "Exception occurred trying to add a Alert relay Bicoder {0} to alert relay {1}: The value '{2}' " \
                    "must be an int".format(
                        str(self.sn),   # {0}
                        str(_address),  # {1}
                        str(_address)   # {2}
                    )
            raise Exception(e_msg)

        else:
            # If an alert relay with the specified address has not already been used, create a new pump object
            if _address not in self.controller.base_station_3200.alert_relayes.keys():
                # If we were originally created for the Substation, then the bicoder doesn't get a "base_station"
                # reference until after this code completes. So, if thats the case, then access the "base_station"
                # through the Substation, otherwise, access through our local "base_station" reference.
                self.base_station.add_alert_relay_to_controller(_address=_address, _serial_number=self.sn)
            else:
                e_msg = "An alert relay with address {0} has already been created, alert relay bicoder ({1}) could not" \
                        "be addressed.".format(_address, self.sn)
                raise ValueError(e_msg)
