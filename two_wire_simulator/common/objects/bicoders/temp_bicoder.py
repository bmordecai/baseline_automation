from common.objects.base_classes.bicoders import BiCoder

from common.imports import opcodes
from common.objects.statuses.bicoders.temp_statuses import TempStatuses
from common.imports.types import BiCoderCommands, DeviceCommands,  ActionCommands

__author__ = 'bens'


class TempBicoder(BiCoder):

    def __init__(self, _sn, _controller, _address=0):
        """
        :param _sn:         Serial number to set for BiCoder. \n
        :type _sn:          str \n

        :param _controller: Controller this object is "attached" to \n
        :type _controller:  common.objects.controllers.bl_32.BaseStation3200 |
                            common.objects.controllers.bl_sb.Substation \n

        :param _address:    The address associated with this bicoder on the controller. \n
        :type _address:     int \n
        """
        # This is equivalent to: BiCoder.__init(self, _serial_number=_sn, _serial_connection=_ser)
        # This seems to be the "new classy" way in Python to init base classes.
        super(TempBicoder, self).__init__(_serial_number=_sn, _controller=_controller, _address=_address)
        self.ty = self.bicoder_types.TEMPERATURE
        self.id = self.device_type.TEMPERATURE_SENSOR

        # Used when moving device from 3200 two-wire to substation two-wire
        self.decoder_type = self.id  # TS

        self.vd = 93.1      # Temperature

        # Attributes that we will send to the controller by default
        self.default_attributes = [
            (opcodes.temperature_value, 'vd'),
            (opcodes.two_wire_drop, 'vt')
        ]

        # Temp biCoder status' module
        self.statuses = TempStatuses(_bicoder_object=self)
        """:type: common.objects.statuses.bicoders.temp_statuses.TempStatuses"""

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Setter Methods                                                                                                   #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def set_temperature_reading(self, _degrees=None):
        """
        Set the temperature reading for the temperature BiCoder (Faux IO Only) \n

        :param _degrees: Value to set the temperature BiCoder temperature reading to \n
        :type _degrees: Integer, Float
        """
        # If a temperature value is passed in for overwrite
        if _degrees is not None:

            # Verifies the temperature reading passed in is an int or float value
            if not isinstance(_degrees, (int, float)):
                e_msg = "Failed trying to set {0} ({1})'s temperature reading. Invalid argument type, expected an " \
                        "int or float, received: {2}".format(self.ty, str(self.sn), type(_degrees))
                raise TypeError(e_msg)
            else:
                # Overwrite current value
                self.vd = _degrees

        # Command for sending
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,         # {0}
            self.id,                    # {1}
            str(self.sn),               # {2}
            opcodes.temperature_value,  # {3}
            str(self.vd)                # {4}
        )

        try:
            # Attempt to set temperature reading for temperature biCoder at the substation in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} ({1})'s 'Temperature Reading' to: '{2}'". \
                format(
                    self.ty,        # {0}
                    str(self.sn),   # {1}
                    str(self.vd)    # {2}
                )
            raise Exception(e_msg)
        else:
            print("Successfully set {0} ({1})'s 'Temperature Reading' to: {2}".format(
                self.ty,        # {0}
                str(self.sn),   # {1}
                str(self.vd))   # {2}
            )

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Verifier Methods                                                                                                 #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def verify_temperature_reading(self, _data=None):
        """
        Verifies the temperature reading for this Temperature BiCoder. Expects the substation's value
        and this instance's value to be equal.

        :param _data: Data Object that holds the substation's attributes. \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the Substation
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.get_data()

        # Gets the temperature from the data object
        temperature = float(data.get_value_string_by_key(opcodes.temperature_value))
        rounded_vd = round(number=self.vd, ndigits=2)

        # If we are a faux device, use 0.2 as allowance
        if abs(rounded_vd - temperature) > 0.2 and self.controller_is_in_faux_io():
            # Compare status versus what is on the substation
            if self.vd != temperature:
                e_msg = "Unable to verify {0} ({1})'s temperature reading. Received: {2}, Expected: {3}". \
                    format(
                        self.ty,            # {0}
                        str(self.sn),       # {1}
                        str(temperature),   # {2}
                        str(self.vd)        # {3}
                    )
                print(e_msg)
                raise ValueError(e_msg)
        # Else if the temperature received from a real device was at least 1 degree (should be a lot higher), then
        # assume unable to get correct reading from device and raise exception.
        elif temperature < 1.0 and not self.controller_is_in_faux_io():
            e_msg = "Unable to verify {0} ({1})'s (real device) temperature reading. Received: {2}, Expected: {3}". \
                format(
                    self.ty,                               # {0}
                    str(self.sn),                          # {1}
                    str(temperature),                      # {2}
                    "Value greater than 1 degrees."        # {3}
                )
            raise ValueError(e_msg)
        # If no exception is thrown, print a success message
        print("Verified {0} ({1})'s temperature reading: '{2}' on substation".format(
            self.ty,          # {0}
            str(self.sn),     # {1}
            str(self.vd)      # {2}
        ))
        return True

    #################################
    def verify_who_i_am(self, _expected_status=None):
        """
        Verifier wrapper which verifies all attributes for this biCoder. \n
        Get all information about the biCoder from the substation. \n

        :param _expected_status: The status code we want to verify against. (optional) \n
        :type _expected_status: str
        """
        data = self.get_data()

        if _expected_status is not None:
            self.verify_status(_status=_expected_status, _data=data)

        # Verify temperature specific attributes
        self.verify_serial_number(_data=data)
        self.verify_temperature_reading(_data=data)
        self.verify_two_wire_drop_value(_data=data)

        return True

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Other Methods                                                                                                    #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def self_test_and_update_object_attributes(self):
        """
        This does a self test on the device.
        Than does a get date a resets the attributes of the device to match the controller.
        """
        try:
            self.do_self_test()
            self.get_data()
            self.vd = float(self.data.get_value_string_by_key(opcodes.temperature_value))
            self.vt = float(self.data.get_value_string_by_key(opcodes.two_wire_drop))
        except Exception as e:
            e_msg = "Exception occurred trying to update attributes of the temperature sensor {0} object." \
                    "Temperature Value: '{1}'," \
                    "Two Wire Drop Value: '{2}'.\n\tException: {3}".format(
                        str(self.sn),   # {0}
                        str(self.vd),   # {1}
                        str(self.vt),   # {2}
                        e.message       # {3}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully updated attributes of the temperature sensor {0} object. "
                  "Temperature Value: '{1}', "
                  "Two Wire Drop Value: '{2}'".format(
                        str(self.sn),  # {0}
                        str(self.vd),  # {1}
                        str(self.vt)   # {2}
                    ))

    #############################
    def add_to_temperature_sensor(self, _address):
        """
        Add this bicoder to a temperature sensor.

        :param _address: Address of the new temperature sensor. \n
        :type _address:  int \n

        :return:
        :rtype:
        """
        if not isinstance(_address, int):
            e_msg = "Exception occurred trying to add a Temperature Bicoder {0} to Temperature Sensor {1}: The " \
                    "value '{2}' must be an int".format(
                        str(self.sn),   # {0}
                        str(_address),  # {1}
                        str(_address)   # {2}
                    )
            raise Exception(e_msg)

        else:
            # If a temperature sensor with the specified address has not already been used, create a new object
            if _address not in self.controller.base_station_3200.temperature_sensors.keys():
                # If we were originally created for the Substation, then the bicoder doesn't get a "base_station"
                # reference until after this code completes. So, if thats the case, then access the "base_station"
                # through the Substation, otherwise, access through our local "base_station" reference.
                if self.controller.is_substation():
                    self.controller.base_station_3200.add_temperature_sensor_to_controller(_address=_address, _serial_number=self.sn)
                else:
                    self.base_station.add_temperature_sensor_to_controller(_address=_address, _serial_number=self.sn)
            else:
                e_msg = "A temperature sensor with address {0} has already been created, temperature bicoder ({1}) " \
                        "could not be addressed.".format(_address, self.sn)
                raise ValueError(e_msg)
