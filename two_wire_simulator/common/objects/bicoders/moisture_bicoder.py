from common.objects.base_classes.bicoders import BiCoder

from common.imports import opcodes
from common.objects.statuses.bicoders.moisture_statuses import MoistureStatuses
from common.imports.types import BiCoderCommands, DeviceCommands,  ActionCommands

__author__ = 'bens'


class MoistureBicoder(BiCoder):

    def __init__(self, _sn, _controller, _address=0):
        """
        :param _sn:         Serial number to set for BiCoder. \n
        :type _sn:          str \n

        :param _controller: Controller this object is "attached" to \n
        :type _controller:  common.objects.controllers.bl_32.BaseStation3200 |
                            common.objects.controllers.bl_sb.Substation \n

        :param _address:    The address associated with this bicoder on the controller. \n
        :type _address:     int \n
        """
        # This is equivalent to: BiCoder.__init(self, _serial_number=_sn, _serial_connection=_ser)
        # This seems to be the "new classy" way in Python to init base classes.
        super(MoistureBicoder, self).__init__(_serial_number=_sn, _controller=_controller, _address=_address)
        self.ty = self.bicoder_types.MOISTURE
        self.id = self.device_type.MOISTURE_SENSOR
        
        # Used when moving device from 3200 two-wire to substation two-wire
        self.decoder_type = self.id  # MS

        self.vt = 1.7   # Two-Wire Drop
        self.vp = 21.3  # Moisture Percent
        self.vd = 71.5  # Temperature

        # Attributes that we will send to the controller by default
        self.default_attributes = [
            (opcodes.moisture_percent, 'vp'),
            (opcodes.temperature_value, 'vd'),
            (opcodes.two_wire_drop, 'vt')
        ]

        # Moisture biCoder status' module
        self.statuses = MoistureStatuses(_bicoder_object=self)
        """:type: common.objects.statuses.bicoders.moisture_statuses.MoistureStatuses"""

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Setter Methods                                                                                                   #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def set_moisture_percent(self, _percent=None):
        """
        Set the moisture percent for the moisture BiCoder (Faux IO Only) \n

        :param _percent: Value to set the moisture BiCoder moisture reading to (in percent) \n
        :type _percent: Integer, Float
        """
        # If a moisture percent is passed in for overwrite
        if _percent is not None:

            # Verifies the moisture biCoder reading passed in is an int or float value
            if not isinstance(_percent, (int, float)):
                e_msg = "Failed trying to set {0} ({1})'s moisture percent. Invalid argument type, expected an int " \
                        "or float, received: {2}".format(
                            self.ty,            # {0}
                            self.sn,            # {1}
                            type(_percent)      # {2}
                        )
                raise TypeError(e_msg)
            else:
                # Overwrite current value with new value
                self.vp = _percent

        # Command for sending
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,         # {0}
            self.id,                    # {1}
            str(self.sn),               # {2}
            opcodes.moisture_percent,   # {3}
            str(self.vp)                # {4}
        )

        try:
            # Attempt to set moisture percent for moisture biCoder at the substation in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} ({1})'s 'Moisture Percent' to: '{2}'".format(
                self.ty,        # {0}
                str(self.sn),   # {1}
                str(self.vp)    # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set {0} ({1})'s 'Moisture Percent' to: {2}".format(
                self.ty,        # {0}
                str(self.sn),   # {1}
                str(self.vp))   # {2}
            )

    #################################
    def set_temperature_reading(self, _temp=None):
        """
        Set the temperature reading for the moisture BiCoder (Faux IO Only) \n

        :param _temp: Value to set the moisture BiCoder temperature reading to \n
        :type _temp: Integer, Float
        """
        # If a temperature value is passed in for overwrite
        if _temp is not None:

            # Verifies the temperature reading passed in is an int or float value
            if not isinstance(_temp, (int, float)):
                e_msg = "Failed trying to set {0} ({1})'s temperature reading. Invalid argument type, expected an " \
                        "int or float, received: {2}".format(
                            self.ty,            # {0}
                            str(self.sn),       # {1}
                            type(_temp)         # {2}
                        )
                raise TypeError(e_msg)
            else:
                # Overwrite current value
                self.vd = _temp

        # Command for sending
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,         # {0}
            self.id,                    # {1}
            str(self.sn),               # {2}
            opcodes.temperature_value,  # {3}
            str(self.vd)                # {4}
        )

        try:
            # Attempt to set temperature reading for moisture biCoder at the substation in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} ({1})'s 'Temperature Reading' to: '{2}'". \
                format(
                    self.ty,        # {0}
                    str(self.sn),   # {1}
                    str(self.vd)    # {2}
                )
            raise Exception(e_msg)
        else:
            print("Successfully set {0} ({1})'s 'Temperature Reading' to: {2}".format(
                self.ty,        # {0}
                str(self.sn),   # {1}
                str(self.vd))   # {2}
            )

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Verifier Methods                                                                                                 #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def verify_moisture_percent(self, _data=None):
        """
        Verifies the moisture percent set for this Moisture BiCoder. Expects the substation's value
        and this instance's value to be equal.

        :param _data: Data Object that holds the substation's attributes. \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the Substation
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.get_data()

        # Gets the moisture percent from the data object
        moisture_percent = float(data.get_value_string_by_key(opcodes.moisture_percent))
        rounded_vp = round(number=self.vt, ndigits=2)
        # If controller is using faux io, use 0.02 as allowable difference
        if abs(rounded_vp - moisture_percent) > 0.02 and self.controller_is_in_faux_io():
            # Compare status versus what is on the substation
            if self.vp != moisture_percent:
                e_msg = "Unable to verify {0} ({1})'s moisture percent. Received: {2}, Expected: {3}".format(
                            self.ty,                # {0}
                            str(self.sn),           # {1}
                            str(moisture_percent),  # {2}
                            str(self.vp)            # {3}
                        )
                raise ValueError(e_msg)
        # Else if the controller is using real devices, bomb out when receiving a non-positive value
        elif moisture_percent < 0.0 and not self.controller_is_in_faux_io():
            e_msg = "Unable to verify {0} ({1})'s (real device) moisture percent. Received: {2}, Expected: {3}"\
                .format(
                    self.ty,                            # {0}
                    str(self.sn),                       # {1}
                    str(moisture_percent),              # {2}
                    'Value between 0 and 5 degrees.'    # {3}
                )
            raise ValueError(e_msg)
        else:
            # If no exception is thrown, print a success message
            print("Verified {0} ({1})'s moisture reading: '{2}' on substation".format(
                self.ty,          # {0}
                str(self.sn),     # {1}
                str(self.vp)      # {2}
            ))
            return True

    #################################
    def verify_temperature_reading(self, _data=None):
        """
        Verifies the temperature reading for this Moisture BiCoder. Expects the substation's value
        and this instance's value to be equal.

        :param _data: Data Object that holds the substation's attributes. \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the Substation
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.get_data()

        # Gets the temperature from the data object
        temperature = float(data.get_value_string_by_key(opcodes.temperature_value))
        rounded_vd = round(number=self.vd, ndigits=2)

        # If we are a faux device, use 0.2 as allowance
        if abs(rounded_vd - temperature) > 0.2 and self.controller_is_in_faux_io():
            # Compare status versus what is on the substation
            if self.vd != temperature:
                e_msg = "Unable to verify {0} ({1})'s temperature reading. Received: {2}, Expected: {3}". \
                    format(
                        self.ty,            # {0}
                        str(self.sn),       # {1}
                        str(temperature),   # {2}
                        str(self.vd)        # {3}
                    )
                print(e_msg)
                raise ValueError(e_msg)
        # Else if the temperature received from a real device was at least 1 degree (should be a lot higher), then
        # assume unable to get correct reading from device and raise exception.
        elif temperature < 1.0 and not self.controller_is_in_faux_io():
            e_msg = "Unable to verify {0} ({1})'s (real device) temperature reading. Received: {2}, Expected: {3}". \
                    format(
                        self.ty,                               # {0}
                        str(self.sn),                          # {1}
                        str(temperature),                      # {2}
                        "Value greater than 1 degrees."        # {3}
                    )
            raise ValueError(e_msg)
        # If no exception is thrown, print a success message
        print("Verified {0} ({1})'s temperature reading: '{2}' on substation".format(
            self.ty,          # {0}
            str(self.sn),     # {1}
            str(self.vd)      # {2}
        ))
        return True

    #################################
    def verify_who_i_am(self, _expected_status=None):
        """
        Verifier wrapper which verifies all attributes for this biCoder. \n
        Get all information about the biCoder from the substation. \n

        :param _expected_status: The status code we want to verify against. (optional) \n
        :type _expected_status: str
        """
        data = self.get_data()

        if _expected_status is not None:
            self.verify_status(_status=_expected_status, _data=data)

        # Verify moisture specific attributes
        self.verify_serial_number(_data=data)
        self.verify_moisture_percent(_data=data)
        self.verify_temperature_reading(_data=data)
        self.verify_two_wire_drop_value(_data=data)

        return True

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Other Methods                                                                                                    #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def self_test_and_update_object_attributes(self):
        """
        this does a self test on the device
        than does a get date a resets the attributes of the device to match the controller
        """
        try:
            self.do_self_test()
            self.get_data()
            self.vp = float(self.data.get_value_string_by_key(opcodes.moisture_percent))
            self.vd = float(self.data.get_value_string_by_key(opcodes.temperature_value))
            self.vt = float(self.data.get_value_string_by_key(opcodes.two_wire_drop))
        except Exception as e:
            e_msg = "Exception occurred trying to update attributes of the Moisture Bicoder {0} object." \
                    " Moisture Percent: '{1}'," \
                    " Temperature: '{2}'," \
                    " Two Wire Drop Value: '{3}'.\n\tException: {4}".format(
                        str(self.sn),   # {0}
                        str(self.vp),   # {1}
                        str(self.vd),   # {2}
                        str(self.vt),   # {3}
                        e.message       # {4}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully updated attributes of the Moisture Bicoder {0} object. "
                  "Moisture Percent: '{1}', "
                  "Temperature: '{2}', "
                  "Two Wire Drop Value: '{3}'".format(
                        str(self.sn),  # {0}
                        str(self.vp),  # {1}
                        str(self.vd),  # {2}
                        str(self.vt)   # {3}
                    ))

    #############################
    def add_to_moisture_sensor(self, _address):
        """
        Add this bicoder to a moisture sensor.

        :param _address: Address of the new moisture sensor. \n
        :type _address:  int \n

        :return:
        :rtype:
        """
        if not isinstance(_address, int):
            e_msg = "Exception occurred trying to add a Moisture Bicoder {0} to Moisture Sensor {1}: The value '{2}' " \
                    "must be an int".format(
                        str(self.sn),   # {0}
                        str(_address),  # {1}
                        str(_address)   # {2}
                    )
            raise Exception(e_msg)

        else:
            # If a moisture sensor with the specified address has not already been used, create a new object
            if _address not in self.controller.base_station_3200.moisture_sensors.keys():
                # If we were originally created for the Substation, then the bicoder doesn't get a "base_station"
                # reference until after this code completes. So, if thats the case, then access the "base_station"
                # through the Substation, otherwise, access through our local "base_station" reference.
                if self.controller.is_substation():
                    self.controller.base_station_3200.add_moisture_sensor_to_controller(_address=_address, _serial_number=self.sn)
                else:
                    self.base_station.add_moisture_sensor_to_controller(_address=_address, _serial_number=self.sn)
            else:
                e_msg = "A moisture sensor with address {0} has already been created, moisture bicoder ({1}) could " \
                        "not be addressed.".format(_address, self.sn)
                raise ValueError(e_msg)
