from common.objects.base_classes.bicoders import BiCoder

from common.imports import opcodes
from common.imports.types import ActionCommands, FlowMeterCommands
from common import helper_equations
from common.objects.statuses.bicoders.flow_statuses import FlowStatuses
from common.imports.types import BiCoderCommands, DeviceCommands,  ActionCommands

__author__ = 'bens'


class FlowBicoder(BiCoder):
    """
    :type fm_on_cn: common.objects.controller.fm.FlowMeter
    """

    def __init__(self, _sn, _controller, _address=0):
        """
        :param _sn:         Serial number to set for BiCoder.
        :type _sn:          str

        :param _controller: Controller this object is "attached" to \n
        :type _controller:  common.objects.controllers.bl_32.BaseStation3200 |
                            common.objects.controllers.bl_sb.Substation \n

        :param _address:    The address associated with this bicoder on the controller. \n
        :type _address:     int \n
        """
        # This is equivalent to: BiCoder.__init(self, _serial_number=_sn, _serial_connection=_ser)
        # This seems to be the "new classy" way in Python to init base classes.
        super(FlowBicoder, self).__init__(_serial_number=_sn, _controller=_controller, _address=_address)
        self.ty = self.bicoder_types.FLOW
        self.id = self.device_type.FLOW_METER
        
        # Used when moving device from 3200 two-wire to substation two-wire
        self.decoder_type = self.id  # FM

        self.kv = 2.0  # K Value

        self.vr = 0.0   # Flow rate in Gallons Per Minute
        self.vg = 0.0   # Flow reported as usage in Total Gallons

        self.cr = 0     # Flow Rate Count
        self.cp = 0     # Flow Pulse Count
        self.cu = 0     # Total Usage Count

        # Attributes that we will send to the controller by default
        self.default_attributes = [
            # (opcodes.flow_rate, 'vr'),
            # (opcodes.total_usage, 'vg'),
            (opcodes.flow_rate_count, 'cr'),
            (opcodes.flow_pulse_count, 'cp'),
            (opcodes.flow_usage_count, 'cu'),
            (opcodes.two_wire_drop, 'vt')
        ]

        # Flow Meter object reference from Controller objects.
        # NOTE:
        #   This is set when the substation shares a device with the controller in `configuration.py`.
        self.fm_on_cn = None
        """:type: common.objects.devices.fm.FlowMeter"""

        # Flow biCoder status' module
        self.statuses = FlowStatuses(_bicoder_object=self)
        """:type: common.objects.statuses.bicoders.flow_statuses.FlowStatuses"""

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Setter Methods                                                                                                   #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def set_k_value(self, _value=None):
        """
        Set the k_value for the Flow Meter (Faux IO Only) \n
        :param _value:        Value to set the Flow Meter k value as a float \n
        :return:
        """

        # if is None, use current value
        if _value is not None:

            # Verifies the k_value passed in is an float value
            if not isinstance(_value, float):
                e_msg = "Failed trying to set FM {0} ({1})'s k value. Invalid argument type, expected a float, " \
                        "received: {2}".format(self.sn, str(self.ad), type(_value))
                raise TypeError(e_msg)
            else:
                self.kv = _value

        # Command for sending
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                     # {0}
            FlowMeterCommands.Flow_Meter,           # {1}
            self.sn,                                # {2}
            FlowMeterCommands.Attributes.K_VALUE,   # {3}
            str(self.kv)                            # {4}
        )

        try:
            # If our bicoder was originally created on the Substation and then "added" to a flowmeter on the 3200, then
            # our serial port is the substation's. The problem is that K-value can only be set through the 3200. So
            # if our "controller" is a substation, then use our FlowMeter back-reference to get the 3200's serial port
            # to send the command.
            if self.controller.is_substation():
                self.fm_on_cn.send_command_with_reply(tosend=command)
            else:
                # BiCoder resides on 3200, send away
                self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Flow Meter {0} ({1})'s 'K Value' to: '{2}'".format(
                self.sn,        # {0}
                str(self.ad),   # {1}
                str(self.kv)    # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Flow Meter {0} ({1})'s 'K Value' to: {2}".format(
                self.sn,        # {0}
                str(self.ad),   # {1}
                str(self.kv))   # {2}
            )

    #################################
    def set_flow_rate(self, _gallons_per_minute=None):
        """
        Set the flow rate for the Flow Meter (Faux IO Only) \n

        :param _gallons_per_minute: Value to set the Flow Meter flow rate as a float in gallon per minute\n
        :type _gallons_per_minute: int, float
        """

        # If a flow rate is passed in for overwrite
        if _gallons_per_minute is not None:

            # Verifies the flow rate passed in is an int or float value
            if not isinstance(_gallons_per_minute, (int, float)):
                e_msg = "Failed trying to set {0} ({1})'s Flow Rate. Invalid argument type, expected an int " \
                        "or float. Received type: {2}".format(self.ty, self.sn, type(_gallons_per_minute))
                raise TypeError(e_msg)
            else:
                self.vr = _gallons_per_minute
        # if the flow bicoder is attached to a substation than you must send pulses not gpm
        if self.controller.is_substation():

            # Calculate flow rate count
            flow_rate_count = helper_equations.calculate_flow_rate_count_from_gpm(gpm=self.vr, kval=self.kv)
            new_vr = flow_rate_count
            flow_opcode = opcodes.flow_rate_count
            
        else:
            new_vr = self.vr
            flow_opcode = opcodes.flow_rate
            
        # Command for sending
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,             # {0}
            self.id,                        # {1}
            self.sn,                        # {2}
            flow_opcode,                    # {3}
            new_vr)                         # {4}

        try:
            # Attempt to set flow rate for Flow Meter at the substation in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} ({1})'s 'Flow Rate' to: '{2}'".format(
                self.ty, self.sn, str(self.vr))
            raise Exception(e_msg)
        else:
            print("Successfully set {0} ({1})'s 'Flow Rate' to: {2}".format(
                self.sn,        # {0}
                str(self.ad),   # {1}
                str(self.kv))   # {2}
            )

    #################################
    def set_water_usage(self, _water_usage=None):
        """
        Set the water usage for the Flow Meter (Faux IO Only) \n

        :param _water_usage: Value to set the Flow Meter total water used as a float in gallons\n
        :type _water_usage: int | float
        """
        # If a water usage is passed in for overwrite
        if _water_usage is not None:

            # Verifies the water usage passed in is an int or float value
            if not isinstance(_water_usage, (int, float)):
                e_msg = "Failed trying to set {0} ({1})'s Water Usage. Invalid argument type, expected an int or " \
                        "float. Received type: {2}".format(self.ty, self.sn, type(_water_usage))
                raise TypeError(e_msg)
            else:
                self.vg = _water_usage
        # if the flow bicoder is attached to a substation than you must send pulses not gpm
        if self.controller.is_substation():

            # Calculate flow usage count
            flow_usage_count = helper_equations.calculate_flow_usage_count_from_gallons(gal=self.vg, kval=self.kv)
            new_usage = flow_usage_count
            usage_opcode = opcodes.flow_usage_count
        else:
            new_usage = int(self.vg)
            usage_opcode = opcodes.total_usage

        # Command for sending
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,         # {0}
            self.id,                    # {1}
            str(self.sn),               # {2}
            usage_opcode,               # {3}
            new_usage)                  # {4}

        try:
            # Attempt to set water usage for Flow Meter at the substation in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set {0} ({1})'s 'water usage' to: '{2}'".format(
                self.ty,        # {0}
                self.sn,        # {1}
                str(self.vg)    # {2}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set {0} ({1})'s 'water usage' to: {2}".format(
                self.ty,        # {0}
                self.sn,        # {1}
                str(self.vg)    # {2}
            ))

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Verifier Methods                                                                                                 #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def verify_k_value(self):
        """
        Verifies k value for this Flow Meter on the Controller. Expects the controller's value
        and this instance's value to be equal.
        :return:
        """
        k_value = float(self.data.get_value_string_by_key(opcodes.k_value))

        # Compare status versus what is on the controller
        if self.kv != k_value:
            e_msg = "Unable verify Flow Meter {0} ({1})'s k_value. Received: {2}, Expected: {3}".format(
                self.sn,        # {0}
                str(self.ad),   # {1}
                str(k_value),   # {2}
                str(self.kv)    # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Flow Meter {0} ({1})'s k_value: '{2}' on controller".format(
                self.sn,        # {0}
                str(self.ad),   # {1}
                str(self.kv)    # {2}
            ))

    #################################
    def verify_flow_rate(self, _data=None):
        """
        Verifies flow rate for this Flow Meter on the Substation. Expects the substation's value
        and this instance's value to be equal.

        :param _data: Data Object that holds the substation's attributes. \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the Substation
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.get_data()

        # if the flow bicoder is attached to a substation than you must send pulses not gpm
        flow_rate_count = 0
        flow_rate = 0.0

        if self.controller.is_substation():
            # Get the flow rate from the data object (we cast to float first because a string with '0.00' cannot be
            # converted to an int
            flow_rate_count = int(float(data.get_value_string_by_key(opcodes.flow_rate_count)))

            # Compute GPM value from flow rate count returned from the Substation Flow BiCoder
            calculated_flow_rate = helper_equations.calculate_gpm_from_flow_rate_count(count=flow_rate_count,
                                                                                       kval=self.kv)
        else:
            # Removed "int" cast here and moved it below so that the "real device" case will use a float and the
            # test engine will continue to use "int" cast. 3200 returns both Rate and Usage as floats.
            flow_rate = float(data.get_value_string_by_key(opcodes.flow_rate))
            calculated_flow_rate = int(flow_rate)

        # If controller is in Faux IO, use 0.5 GPM allowance
        if abs(float(self.vr) - float(calculated_flow_rate)) > .5 and self.controller_is_in_faux_io():
            e_msg = "Unable to verify {0} ({1})'s flow rate. Received: {2}, Expected: {3}".format(
                self.ty,                    # {0}
                self.sn,                    # {1}
                str(calculated_flow_rate),  # {2}
                str(self.vr)                # {3}
            )
            raise ValueError(e_msg)
        # Otherwise, if controller is using real devices, allow for a 1GPM tolerance
        elif abs(float(self.vr) - flow_rate) > 1.0 and not self.controller_is_in_faux_io():
            e_msg = "Unable to verify {0} ({1})'s (real device) flow rate. Received: {2}, Expected: {3}".format(
                self.ty,    # {0}
                self.sn,    # {1}
                flow_rate,  # {2}
                self.vr     # {3}
            )
            raise ValueError(e_msg)
        else:
            print("Verified {0} ({1})'s flow rate: '{2}' on substation".format(
                self.ty,          # {0}
                self.sn,          # {1}
                str(self.vr)      # {2}
            ))
            return True

    #################################
    def verify_water_usage(self, _data=None):
        """
        Verifies water usage for this Flow Meter on the Substation. Expects the substation's value
        and this instance's value to be equal.

        :param _data: Data Object that holds the substation's attributes. \n
        :type _data: status_parser.KeyValues

        :return: True if the value in our object matches the value on the Substation
        :return:
        """
        # Calls get data if a data object was not passed in.
        data = _data if _data else self.get_data()
        # if the flow bicoder is attached to a substation than you must send pulses not gpm
        water_usage_count = 0
        water_usage = 0.0
        if self.controller.is_substation():
            # Gets the water usage from the data object (we cast to float first because a string with '0.00' cannot be
            # converted to an int
            water_usage_count = int(float(data.get_value_string_by_key(opcodes.flow_usage_count)))

            calculated_water_usage = helper_equations.calculate_gallons_from_flow_usage_count(count=water_usage_count,
                                                                                              kval=self.kv)
        else:
            # Removed "int" cast here and moved it below so that the "real device" case will use a float and the
            # test engine will continue to use "int" cast. 3200 returns both Rate and Usage as floats.
            water_usage = float(data.get_value_string_by_key(opcodes.total_usage))
            calculated_water_usage = int(water_usage)

        # Compare status versus what is on the substation
        if abs(self.vg - calculated_water_usage) > .5 and self.controller_is_in_faux_io():
            e_msg = "Unable to verify {0} ({1})'s water usage. Received: {2}, Expected: {3}".format(
                self.ty,                        # {0}
                self.sn,                        # {1}
                str(calculated_water_usage),    # {2}
                str(self.vg)                    # {3}
            )
            raise ValueError(e_msg)
        # Else if we are a real device, expect at least 1 gallon used to be returned
        elif water_usage < 1.0 and not self.controller_is_in_faux_io():
            e_msg = "Unable to verify {0} ({1})'s (real device) water usage. Received: {2}, Expected: Value greater " \
                    "than zero.".format(
                        self.ty,                        # {0}
                        self.sn,                        # {1}
                        str(calculated_water_usage)     # {2}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified {0} ({1})'s water usage: '{2}'".format(
                self.ty,          # {0}
                self.sn,          # {1}
                str(self.vg)      # {2}
            ))
            return True

    #################################
    def verify_who_i_am(self, _expected_status=None):
        """
        Verifier wrapper which verifies all attributes for this biCoder. \n
        Get all information about the biCoder from the substation. \n

        :param _expected_status: The status code we want to verify against. (optional) \n
        :type _expected_status: str
        """
        data = self.get_data()

        if _expected_status is not None:
            self.verify_status(_status=_expected_status, _data=data)

        # Verify flow specific attributes
        self.verify_serial_number(_data=data)
        self.verify_flow_rate(_data=data)
        self.verify_water_usage(_data=data)
        self.verify_two_wire_drop_value(_data=data)
        return True

    ####################################################################################################################
    #                                                                                                                  #
    #                                                                                                                  #
    # Other Methods                                                                                                    #
    #                                                                                                                  #
    #                                                                                                                  #
    ####################################################################################################################

    #################################
    def self_test_and_update_object_attributes(self):
        """
        this does a self test on the device
        than does a get date a resets the attributes of the device to match the controller
        """
        try:
            self.do_self_test()
            self.get_data()
            
            # UPDATE - BEN
            #
            #   When doing a self test + update object attributes and the BiCoder resides on the Substation, we needed
            #   to take into account the different opcodes/attributes returned on our "get_data()" call.
            #
            #   get_data() was also modified to automatically determine what "identifier" to use. If substation, use
            #   serial numbers, otherwise, use addresses (if available).

            if self.controller.is_substation():
                # Get the flow rate from the data object (we cast to float first because a string with '0.00' cannot be
                # converted to an int
                flow_rate_count = int(float(self.data.get_value_string_by_key(opcodes.flow_rate_count)))
                # Compute GPM value from flow rate count returned from the Substation Flow BiCoder
                calculated_flow_rate = helper_equations.calculate_gpm_from_flow_rate_count(count=flow_rate_count,
                                                                                           kval=self.kv)
                
                # Calculate flow usage count
                flow_usage_count = int(float(self.data.get_value_string_by_key(opcodes.flow_usage_count)))
                # Compute TOTAL GAL USED from usage count returned from the Substation Flow BiCoder
                calculated_flow_usage = helper_equations.calculate_gallons_from_flow_usage_count(count=flow_usage_count,
                                                                                                 kval=self.kv)
            else:
                # Removed "int" cast here and moved it below so that the "real device" case will use a float and the
                # test engine will continue to use "int" cast. 3200 returns both Rate and Usage as floats.
                flow_rate = float(self.data.get_value_string_by_key(opcodes.flow_rate))
                calculated_flow_rate = flow_rate
                
                flow_usage = float(self.data.get_value_string_by_key(opcodes.total_usage))
                calculated_flow_usage = flow_usage
                
            self.vr = float(calculated_flow_rate)
            self.vg = float(calculated_flow_usage)
            self.vt = float(self.data.get_value_string_by_key(opcodes.two_wire_drop))
        except Exception as e:
            e_msg = "Exception occurred trying to update attributes of the Flow Bicoder {0} object." \
                    " Flow Rate: '{1}'," \
                    " Total Usage: '{2}'," \
                    " Two Wire Drop Value: '{3}'.\n\tException: {4}".format(
                        str(self.sn),   # {0}
                        str(self.vr),   # {1}
                        str(self.vg),   # {2}
                        str(self.vt),   # {3}
                        e.message       # {4}
                    )
            raise Exception(e_msg)
        else:
            print("Successfully updated attributes of the Flow Bicoder {0} object. "
                  "Flow Rate: '{1}', "
                  "Total Usage: '{2}', "
                  "Two Wire Drop Value: '{3}'".format(
                        str(self.sn),  # {0}
                        str(self.vr),  # {1}
                        str(self.vg),  # {2}
                        str(self.vt)   # {3}
                    ))

    #############################
    def add_to_flow_meter(self, _address):
        """
        Add this bicoder to a flow meter.

        :param _address: Address of the new flow meter. \n
        :type _address:  int \n

        :return:
        :rtype:
        """
        if not isinstance(_address, int):
            e_msg = "Exception occurred trying to add a Flow Bicoder {0} to Flow Meter {1}: The value '{2}' " \
                    "must be an int".format(
                        str(self.sn),   # {0}
                        str(_address),  # {1}
                        str(_address)   # {2}
                    )
            raise Exception(e_msg)

        else:
            # If a flow meter with the specified address has not already been used, create a new object
            if _address not in self.controller.base_station_3200.flow_meters.keys():
                # If we were originally created for the Substation, then the bicoder doesn't get a "base_station"
                # reference until after this code completes. So, if thats the case, then access the "base_station"
                # through the Substation, otherwise, access through our local "base_station" reference.
                if self.controller.is_substation():
                    self.controller.base_station_3200.add_flow_meter_to_controller(_address=_address, _serial_number=self.sn)
                else:
                    self.base_station.add_flow_meter_to_controller(_address=_address, _serial_number=self.sn)
            else:
                e_msg = "A flow meter with address {0} has already been created, flow bicoder ({1}) could " \
                        "not be addressed.".format(_address, self.sn)
                raise ValueError(e_msg)
