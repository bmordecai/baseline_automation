from common.imports import opcodes
from decimal import Decimal
from common.objects.base_classes.devices import BaseDevices
from common.objects.messages.devices.pump_messages import PumpMessages
from common.objects.statuses.devices.pump_statuses import PumpStatuses
from common.imports.types import ActionCommands, PumpCommands
__author__ = 'tige'


########################################################################################################################
# Pump
########################################################################################################################
class Pump(BaseDevices):
    """
    Pump Object \n
    """

    starting_lat = Decimal(BaseDevices.controller_lat)
    starting_long = Decimal(BaseDevices.controller_long) + Decimal('.000600')

    #################################
    def __init__(self, _controller, _pump_bicoder, _address, _shared_with_sb=False):
        """
        Initializer for a Pump object. \n

        :param _controller:     Controller this object is "attached" to \n
        :type _controller:      common.objects.controllers.bl_32.BaseStation3200 |
                                common.objects.controllers.bl_10.BaseStation1000 \n

        :param _pump_bicoder:   Bicoder of this device. \n
        :type _pump_bicoder:    common.objects.bicoders.valve_bicoder.ValveBicoder \n

        :param _address:        The address of this object on the controller \n
        :type _address:         int \n

        :param _shared_with_sb:
        :type _shared_with_sb:
        """
        # Give access to the controller
        self.controller = _controller
        # create message reference to object
        self.messages = PumpMessages(_pump_object=self)  # Flow Meter messages
        """:type: common.objects.messages.devices.pump_messages.PumpMessages"""

        # create a lat and long for the device that is offset to the controller
        lat = float((Decimal(str(self.starting_lat)) + (Decimal('0.000005') * _address)))
        lng = float((Decimal(str(self.starting_long)) + (Decimal('0.000005') * _address)))

        # Init parent class to inherit respective attributes
        BaseDevices.__init__(self, _ser=_controller.ser, _identifiers=[[opcodes.pump, _pump_bicoder.sn]],
                             _la=lat, _lg=lng, _sn=_pump_bicoder.sn, _ad=_address, _ty=opcodes.pump,
                             is_shared=_shared_with_sb)

        # Assign the bicoder to this device
        self.bicoder = _pump_bicoder

        # Set device type to inheriting class type (aka, set it to "MV" for Pump object)
        self.dv_type = 'PM'

        # Zone specific attributes for both controllers
        self.en = opcodes.true           # Enabled / Disabled (TR | FA) default to TR
        self.vt = 0.0           # Two Wire Drop

        # 3200 Specific attributes
        self.bp = opcodes.false           # Booster enable (TR | FA)
        # create status reference to object

        self.statuses = PumpStatuses(_pump_object=self)  # Pump Sensor Statuses
        """:type: common.objects.statuses.devices.pump_statuses.PumpStatuses"""

    #################################
    def set_default_values(self):
        """
        set the default values of the device on the controller
        :return:
        :rtype:
        """
        command = "{0},{1}={2},{3}={4},{5}={6},{7}={8},{9}={10}".format(
            ActionCommands.SET,                     # {0}
            PumpCommands.Pump,                      # {1}
            str(self.sn),                           # {2}
            PumpCommands.Attributes.ENABLED,        # {3}
            str(self.en),                           # {4}
            PumpCommands.Attributes.DESCRIPTION,    # {5}
            str(self.ds),                           # {6}
            PumpCommands.Attributes.LATITUDE,       # {7}
            str(self.la),                           # {8}
            PumpCommands.Attributes.LONGITUDE,      # {9}
            str(self.lg)                            # {10}
        )

        if self.controller.controller_type == "32":
            bp_string = ",{0}={1}".format(
                PumpCommands.Attributes.BOOSTER,
                str(self.bp)
            )
            command += bp_string
        try:
            # Attempt to set event switch default values at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Pump {0} ({1})'s 'Default values' to: '{2}'".format(
                    self.sn, str(self.ad), command)
            raise Exception(e_msg)
        else:
            print("Successfully set Pump {0} ({1})'s 'Default values' to: {2}".format(
                  self.sn, str(self.ad), command))

    #################################
    def set_booster_enabled(self):
        """
        Enables the Booster Pump on the controller. \n
        :return:
        """

        self.bp = opcodes.true

        # Command for sending
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,
            PumpCommands.Pump,
            str(self.sn),
            PumpCommands.Attributes.BOOSTER,
            self.bp)

        try:
            # Attempt to enable booster state
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Pump {0} ({1})'s 'Booster Pump State' to: {2}".format(
                    self.sn, str(self.ad), str(self.bp))
            raise Exception(e_msg)
        else:
            print("Successfully set Pump {0} ({1})'s 'Booster Pump State' to: {2}".format(
                  self.sn, str(self.ad), str(self.bp)))

    #################################
    def set_booster_disabled(self):
        """
        Disables the Booster Pump on the controller. \n
        :return:
        """

        self.bp = opcodes.false

        # Command for sending
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,
            PumpCommands.Pump,
            str(self.sn),
            PumpCommands.Attributes.BOOSTER,
            self.bp)
        try:
            # Attempt to disable booster state
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Pump {0} ({1})'s 'Booster Pump State' to: {2}".format(
                self.sn, str(self.ad), str(self.bp))
            raise Exception(e_msg)
        else:
            print("Successfully set Pump {0} ({1})'s 'Booster Pump State' to: {2}".format(
                self.sn, str(self.ad), str(self.bp)))

    #################################
    def verify_booster_enable_state(self):
        """
        Verifies the booster enabled state on the controller. \n
        :return:
        """
        booster_enable_state = self.data.get_value_string_by_key(PumpCommands.Attributes.BOOSTER)

        # Compare status versus what is on the controller
        if self.bp != booster_enable_state:
            e_msg = "Unable verify Pump {0} ({1})'s Booster pump enabled state. Received: {2}, Expected: " \
                    "{3}".format(
                        self.sn,                    # {0}
                        str(self.ad),               # {1}
                        str(booster_enable_state),  # {2}
                        str(self.bp)                # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Pump {0} ({1})'s 'Booster Enabled State': {2} on controller".format(
                  self.sn,          # {0}
                  str(self.ad),     # {1}
                  str(self.bp)      # {2}
                  ))

    #################################
    def replace_single_valve_bicoder(self, _new_serial_number):
        """
        Replaces the bicoder with a new one with the specified new serial number. \n

        :param _new_serial_number:  The serial number that will be loaded onto the controller. \n
        :type _new_serial_number:   str \n
        """
        # Load the device on the controller, and in the process making the new bicoder object
        if _new_serial_number not in self.controller.valve_bicoders.keys():
            self.controller.load_dv(opcodes.single_valve_decoder, [_new_serial_number], device_type=opcodes.pump)
            self.controller.do_search_for_pumps()

        # Assign the new bicoder and update the zones serial number
        self.bicoder = self.controller.valve_bicoders[_new_serial_number]
        self.sn = self.bicoder.sn
        self.identifiers[0][1] = _new_serial_number
        self.set_address()

        # Update all the objects attributes to stay consistent
        self.bicoder.self_test_and_update_object_attributes()

    #################################
    def replace_dual_valve_bicoder(self, _new_serial_number):
        """
        Replaces the bicoder with a new one with the specified new serial number. \n

        :param _new_serial_number:  The serial number that will be loaded onto the controller. \n
        :type _new_serial_number:   str \n
        """
        # Load the device on the controller, and in the process making the new bicoder object
        if _new_serial_number not in self.controller.valve_bicoders.keys():
            self.controller.load_dv(opcodes.two_valve_decoder, [_new_serial_number], device_type=opcodes.pump)
            self.controller.do_search_for_pumps()

        # Assign the new bicoder and update the device serial number
        self.bicoder = self.controller.valve_bicoders[_new_serial_number]
        self.sn = self.bicoder.sn
        self.identifiers[0][1] = _new_serial_number
        self.set_address()

        # Update all the objects attributes to stay consistent
        self.bicoder.self_test_and_update_object_attributes()

    #################################
    def verify_who_i_am(self, _expected_status=None):
        """
        Verifier wrapper which verifies all attributes for this 'Flow Meter'. \n
        :return:
        """
        # Get all information about the device from the controller.
        self.get_data()

        # Verify base attributes
        self.verify_description()
        self.verify_serial_number()
        self.verify_latitude()
        self.verify_longitude()

        if _expected_status is not None:
            self.verify_status(_expected_status=_expected_status)

        # Verify Pump specific attributes
        self.verify_enabled_state()

        # 3200 controller specific attributes for Pumps
        if self.controller.controller_type == "32":
            self.verify_booster_enable_state()

        return True

