from decimal import Decimal
from common.imports import opcodes
from common.objects.base_classes.devices import BaseDevices
from common.imports.types import ActionCommands, MoistureSensorCommands
from common.objects.messages.devices.moisture_sensor_messages import MoistureSensorMessages
from common.objects.statuses.devices.moisture_sensor_statuses import MoistureSensorStatuses
from common.objects.packets.devices.moisture_sensor_packets import MoistureSensorPackets

__author__ = 'tige'

########################################################################################################################
# MOISTURE SENSOR
########################################################################################################################


class MoistureSensor(BaseDevices):
    """
    Moisture Sensor Object \n
    """

    starting_lat = Decimal(BaseDevices.controller_lat)
    starting_long = Decimal(BaseDevices.controller_long) + Decimal('.000500')

    #################################
    def __init__(self, _controller, _moisture_bicoder, _address, _shared_with_sb=False):
        """
        Initializer for a Moisture Sensor object.

        :param _controller:     Controller this object is "attached" to \n
        :type _controller:      common.objects.controllers.bl_32.BaseStation3200 |
                                common.objects.controllers.bl_10.BaseStation1000 \n

        :param _moisture_bicoder:   Serial number for this device. \n
        :type _moisture_bicoder:    common.objects.bicoders.moisture_bicoder.MoistureBicoder \n

        :param _address:            The address of this object on the controller \n
        :type _address:             int \n

        :param _shared_with_sb:
        :type _shared_with_sb:
        """
        # Give access to the controller
        self.controller = _controller

        # create message reference to object
        self.messages = MoistureSensorMessages(_moisture_sensor_object=self)  # Moisture Sensor messages
        self.packets = MoistureSensorPackets(_moisture_sensor_object=self)  # Moisture Sensor packets

        # create a lat and long for the device that is offset to the controller
        lat = float((Decimal(str(self.starting_lat)) + (Decimal('0.000005') * _address)))
        lng = float((Decimal(str(self.starting_long)) + (Decimal('0.000005') * _address)))

        # Init parent class to inherit respective attributes
        BaseDevices.__init__(self, _ser=_controller.ser, _identifiers=[[opcodes.moisture_sensor, _moisture_bicoder.sn]],
                             _la=lat, _lg=lng, _sn=_moisture_bicoder.sn, _ad=_address, _ty=opcodes.moisture_sensor,
                             is_shared=_shared_with_sb)

        # Assign the bicoder to this device
        self.bicoder = _moisture_bicoder

        self.dv_type = opcodes.moisture_sensor
        # TODO: This was commented out because it was overwritting the substation's bicoder values and causing the verifier to fail.
        # Moisture Sensor specific attributes
        # self.bicoder.vp = 21.3  # Moisture Percent
        # self.bicoder.vd = 71.5  # Temperature
        # self.bicoder.vt = 0.0   # Two Wire Drop

        # create status reference to object
        self.statuses = MoistureSensorStatuses(_moisture_sensor_object=self)  # Moisture Sensor Statuses
        """:type: common.objects.statuses.devices.moisture_sensor_statuses.MoistureSensorStatuses"""

    #################################
    def set_default_values(self):
        """
        set the default values of the device on the controller
        :return:
        :rtype:
        """
        command = "{0}{1},{2}={3},{4}={5},{6}={7}".format(
            ActionCommands.SET,                                 # {0}
            self.get_id(),                                      # {1}
            MoistureSensorCommands.Attributes.DESCRIPTION,      # {2}
            self.ds,                                            # {3}
            MoistureSensorCommands.Attributes.LATITUDE,         # {4}
            self.la,                                            # {5}
            MoistureSensorCommands.Attributes.LONGITUDE,        # {6}
            self.lg,                                            # {7}
        )
        # TODO Needs to be moved to bi-coder tests.
        # # If the device is not shared with the substation, set these values because we own them.
        # if not self.is_shared_with_substation:
        #     command += ",{0}={1},{2}={3},{4}={5}".format(
        #         opcodes.moisture_percent,   # {0}
        #         self.bicoder.vp,            # {1}
        #         opcodes.temperature_value,  # {2}
        #         self.bicoder.vd,            # {3}
        #         opcodes.two_wire_drop,      # {4}
        #         self.bicoder.vt             # {5}
        #     )

        try:
            # Attempt to set moisture sensor default values at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Moisture Sensor {0} ({1})'s 'Default values' to: '{2}'".format(
                    self.sn, str(self.ad), command)
            raise Exception(e_msg)
        else:
            print("Successfully set Moisture Sensor {0} ({1})'s 'Default values' to: {2}".format(
                  self.sn, str(self.ad), command))

    #################################
    def replace_moisture_bicoder(self, _new_serial_number):
        """
        Replaces the bicoder with a new one with the specified new serial number. \n

        :param _new_serial_number:  The serial number that will be loaded onto the controller. \n
        :type _new_serial_number:   str \n
        """
        # Load the device on the controller, and in the process making the new bicoder object
        if _new_serial_number not in self.controller.moisture_bicoders.keys():
            self.controller.load_dv(opcodes.moisture_sensor, [_new_serial_number])
            self.controller.do_search_for_moisture_sensor()

        # Assign the new bicoder and update the device serial number
        self.bicoder = self.controller.moisture_bicoders[_new_serial_number]
        self.sn = self.bicoder.sn
        self.identifiers[0][1] = _new_serial_number
        self.set_address()

        # Update all the objects attributes to stay consistent
        self.bicoder.self_test_and_update_object_attributes()

    #################################
    def verify_who_i_am(self, _expected_status=None):
        """
        Verifier wrapper which verifies all attributes for this 'Flow Meter'. \n
        :return:
        """
        # # Get all information about the device from the controller.

        self.get_data()

        # Verify base attributes
        if self.data is not None:
            self.verify_description()
            self.verify_serial_number()
            self.verify_latitude()
            self.verify_longitude()
            if _expected_status is not None:
                self.verify_status(_expected_status=_expected_status)


