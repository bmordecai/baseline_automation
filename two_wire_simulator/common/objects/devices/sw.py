from decimal import Decimal
from common.imports import opcodes
from common.objects.base_classes.devices import BaseDevices
from common.objects.messages.devices.event_switch_messages import EventSwitchMessages
from common.objects.statuses.devices.event_switch_statuses import EventSwitchStatuses

__author__ = 'tige'


########################################################################################################################
# EVENT SWITCH
########################################################################################################################
class EventSwitch(BaseDevices):
    """
    Event Switch Object \n
    """

    starting_lat = Decimal(BaseDevices.controller_lat)
    starting_long = Decimal(BaseDevices.controller_long) + Decimal('.000700')

    #################################
    def __init__(self, _controller, _switch_bicoder, _address, _shared_with_sb=False):
        """
        Initializer for an Event Switch object. \n

        :param _controller:     Controller this object is "attached" to \n
        :type _controller:      common.objects.controllers.bl_32.BaseStation3200 |
                                common.objects.controllers.bl_10.BaseStation1000 \n

        :param _switch_bicoder: Bicoder of this device. \n
        :type _switch_bicoder:  common.objects.bicoders.switch_bicoder.SwitchBicoder \n

        :param _address:        The address of this object on the controller \n
        :type _address:         int \n

        :param _shared_with_sb:
        :type _shared_with_sb:
        """
        # Give access to the controller
        self.controller = _controller

        # create message reference to object
        self.messages = EventSwitchMessages(_event_switch_object=self)  # Event switch messages

        # create a lat and long for the device that is offset to the controller
        lat = float((Decimal(str(self.starting_lat)) + (Decimal('0.000005') * _address)))
        lng = float((Decimal(str(self.starting_long)) + (Decimal('0.000005') * _address)))

        # TODO review this line of code? Doesn't seem correct
        # self.sn = None

        # Init parent class to inherit respective attributes
        BaseDevices.__init__(self, _ser=_controller.ser, _identifiers=[[opcodes.event_switch, _switch_bicoder.sn]],
                             _la=lat, _lg=lng, _sn=_switch_bicoder.sn, _ad=_address, _ty=opcodes.event_switch,
                             is_shared=_shared_with_sb)

        # Assign the bicoder to this device
        self.bicoder = _switch_bicoder

        self.dv_type = opcodes.event_switch
        # Event Switch specific attributes
        self.en = 'TR'     # Enabled / Disabled (TR | FA)
        self.vc = opcodes.contacts_closed       # Contact State
        self.vt = 1.7       # Two Wire Drop
        # self.set_default_values()

        # create status reference to object
        self.statuses = EventSwitchStatuses(_event_switch_object=self)  # Event Switch Statuses
        """:type: common.objects.statuses.devices.event_switch_statuses.EventSwitchStatuses"""

    #################################
    def set_default_values(self):
        """
        set the default values of the device on the controller
        :return:
        :rtype:
        """
        command = "SET,SW={0},EN={1},DS={2},LA={3},LG={4}".format(
            str(self.sn),   # {0}
            str(self.en),   # {1}
            str(self.ds),   # {2}
            self.la,        # {3}
            self.lg         # {4}
        )

        try:
            # Attempt to set event switch default values at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Event Switch {0} ({1})'s 'Default values' to: '{2}'".format(
                    self.sn, str(self.ad), command)
            raise Exception(e_msg)
        else:
            print("Successfully set Event Switch {0} ({1})'s 'Default values' to: {2}".format(
                  self.sn, str(self.ad), command))

    # #################################
    # def set_enable_state_on_cn(self, _state=None):
    #     """
    #     Enables the Event Switch on the controller. \n
    #     :return:
    #     """
    #     if _state is not None:
    #
    #         # If Event Switch isn't enabled, make sure to update instance variable to match what the controller will
    #         # show.
    #         if _state not in ['TR', 'FA']:
    #             e_msg = "Invalid enabled state entered for Event Switch {0} ({1}). Received: {2}, Expected: 'TR' or " \
    #                     "'FA'".format(self.sn, str(self.ad), _state)
    #             raise ValueError(e_msg)
    #         else:
    #             self.en = _state
    #
    #     # Command for sending
    #     command = "SET,SW={0},EN={1}".format(str(self.sn), self.en)
    #
    #     try:
    #         # Attempt to enable Event Switch
    #         self.ser.send_and_wait_for_reply(tosend=command)
    #     except Exception:
    #         e_msg = "Exception occurred trying to set Event Switch {0} ({1})'s Enable State".format(
    #                 self.sn, str(self.ad))
    #         raise Exception(e_msg)
    #     else:
    #         print("Successfully set Event Switch {0} ({1})'s 'Enabled State' to: {2}".format(
    #               self.sn, str(self.ad), str(self.en)))
    #
    #
    #
    # #################################
    # def verify_enable_state_on_cn(self):
    #     """
    #     Verifies that the enabled state event switch on the controller. \n
    #     :return:
    #     """
    #     enable_state = self.data.get_value_string_by_key('EN')
    #     # Compare status versus what is on the controller
    #     if self.en != enable_state:
    #         e_msg = "Unable verify Event Switch {0} ({1})'s enabled state. Received: {2}, Expected: {3}".format(
    #                 self.sn,            # {0}
    #                 str(self.ad),       # {1}
    #                 str(enable_state),  # {2}
    #                 str(self.en)        # {3}
    #         )
    #         raise ValueError(e_msg)
    #     else:
    #         print("Verified Event Switch {0} ({1})'s state: '{2}' on controller".format(
    #               self.sn,          # {0}
    #               str(self.ad),     # {1}
    #               str(self.en)      # {2}
    #               ))

    #################################
    def replace_switch_bicoder(self, _new_serial_number):
        """
        Replaces the bicoder with a new one with the specified new serial number. \n

        :param _new_serial_number:  The serial number that will be loaded onto the controller. \n
        :type _new_serial_number:   str \n
        """
        # Load the device on the controller, and in the process making the new bicoder object
        if _new_serial_number not in self.controller.switch_bicoders.keys():
            self.controller.load_dv(opcodes.event_switch, [_new_serial_number])
            self.controller.do_search_for_event_switches()

        # Assign the new bicoder and update the device serial number
        self.bicoder = self.controller.switch_bicoders[_new_serial_number]
        self.sn = self.bicoder.sn
        self.identifiers[0][1] = _new_serial_number
        self.set_address()

        # Update all the objects attributes to stay consistent
        self.bicoder.self_test_and_update_object_attributes()

    #################################
    def verify_who_i_am(self, _expected_status=None):
        """
        Verifier wrapper which verifies all attributes for this 'Flow Meter'. \n
        :return:
        """
        # Get all information about the device from the controller.
        self.get_data()

        # Verify base attributes
        self.verify_description()
        self.verify_serial_number()
        self.verify_latitude()
        self.verify_longitude()
        if _expected_status is not None:
            self.verify_status(_expected_status=_expected_status)

        # Verify Event Switch specific attributes
        self.verify_enabled_state()
