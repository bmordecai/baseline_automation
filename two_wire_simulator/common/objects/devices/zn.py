import math
import common.epa_package.equations as equations
from common import helper_methods
from decimal import Decimal
from common.imports import opcodes
from common.imports.types import ZoneCommands
from common.imports.types import ActionCommands, ZoneCommands
from common.objects.messages.devices.zone_messages import ZoneMessages
from common.objects.statuses.devices.zone_statuses import ZoneStatuses

from common.objects.base_classes.devices import BaseDevices
# from common.objects.object_bucket import mainlines32
from common.epa_package.wbw_imports import *
from common.objects.base_classes import messages
# # from common.objects.base_classes.messages import Messages
from common.imports import types
from common.objects.packets.devices.zone_packets import ZonePackets

__author__ = 'ben'


########################################################################################################################
# ZONE
########################################################################################################################
class Zone(BaseDevices):
    """
    Zone Object \n
    """
    # starting_lat = Decimal(dev_mod.Devices.controller_lat)
    # starting_long = Decimal(dev_mod.Devices.controller_long) + Decimal('.000300')
    # Water-Based Watering Additional Attributes
    ei = None              # initial ETo value for each zone
    date_of_et = 00000000         # Date ETo was sent to controller for each zone
    starting_lat = Decimal(str(BaseDevices.controller_lat))
    starting_long = Decimal(str(BaseDevices.controller_long)) + Decimal('.000300')
    mainline_objects = None
    # TODO I think I need to move MB up here
    # zp_objects = {1: zp.ZoneProgram}

    #################################
    def __init__(self, _controller, _address, _valve_bicoder, _shared_with_sb=False, _ml=1, mainline_objects=None):
        """
        Initializer for a Zone object. \n

        :param _controller:     Controller this object is "attached" to \n
        :type _controller:      common.objects.controllers.bl_32.BaseStation3200 |
                                common.objects.controllers.bl_10.BaseStation1000 \n

        :param _valve_bicoder:  Bicoder of this device. \n
        :type _valve_bicoder:   common.objects.bicoders.valve_bicoder.ValveBicoder \n

        :param _address:        The address of this object on the controller \n
        :type _address:         int \n

        :param _shared_with_sb:
        :type _shared_with_sb:

        :param _ml:             Mainline for program (1 through 8) \n
        :type _ml:              int \n
        """
        # Give access to the controller
        self.controller = _controller
        # create message reference to object
        self.messages = ZoneMessages(_zone_object=self)  # Zone messages
        """:type: common.objects.messages.devices.zone_messages.ZoneMessages"""

        self.packets = ZonePackets(self)
        """:type: common.objects.packets.devices.zones_packets.ZonePackets"""

        # create a lat and long for the device that is offset to the controller
        lat = float((Decimal(str(self.starting_lat)) + (Decimal('0.000005') * _address)))
        lng = float((Decimal(str(self.starting_long)) + (Decimal('0.000005') * _address)))

        # Init parent class to inherit respective attributes
        BaseDevices.__init__(self, _ser=_controller.ser, _identifiers=[[opcodes.zone, _address]], _sn=_valve_bicoder.sn,
                             _la=lat, _lg=lng, _ad=_address, _ty=opcodes.zone, is_shared=_shared_with_sb)

        # Give access to the controller
        self.controller = _controller

        # Assign the bicoder to this device
        self.bicoder = _valve_bicoder

        # Assign objects at init time
        self.mainlines = _controller.mainlines

        # Set device type to inheriting class type (aka, set it to "ZN" for Zone object)
        self.dv_type = opcodes.zone

        # Zone specific attributes for both controllers
        self.en = opcodes.true      # Enabled / Disabled (TR | FA)
        self.ml = _ml               # Mainline Number
        self.df = 0.0               # Design Flow (GPM)
        self.last_ss = ''           # last known status

        # Weather-Based Watering Additional Attributes
        self.kc = 0.00              # Crop coefficient for hydrozone
        self.pr = 0.00              # Precipitation rate for hydrozone
        self.du = 0.00              # Distribution Uniformity for hydrozone
        self.rz = 0.00              # Root zone working water storage capacity in as inches per foot

        # Attributes that are not sent to the zone where there is no setters or verifiers for these attributes
        self.allowable_depletion = 0.00  # Percentage of allowable depletion assigned to the zone
        self.sun_exposure = 0.00         # Percentage of Sun exposure assigned to the zone
        self.slope = 0.00                # Percentage of Slope assigned to the zone
        self.soil_type = ''              # Soil Type pass in a string
        self.gross_area = 0              # Gross Area returned in square feet
        self.root_depth = 0.00           # Root Depth in as inches per foot
        # this is set by the set enable et state on cn from the ZP.py object
        self.enable_et = opcodes.false   # set zone to use ET based watering

        # Attributes that are not sent to the zone where there is no setters but there is verifiers for these attributes
        self.yesterdays_mb = 0.00   # Yesterdays Moisture Balance assigned to a zone
        self.mb = 0.00              # Moisture Balance assigned to a zone
        self.rn = 0.00              # rainfall assigned to a zone
        self.yesterdays_rt = 0.00   # Yesterdays runtime assigned to a zone
        self.rt = 0.00              # runtime assigned to a zone
        self.etc = 0.00             # ETC assigned to a zone

        # 1000 controller specific attributes
        # TODO: Need to implement methods below for 1000 implementation
        self.mr = 0                 # Mirror Zone (Zone # | 0=none)
        self.mv = 1                 # Pointer to a master valve to be ran when the zone is ran
        self.ff = opcodes.false     # High Flow Variance Fault (TR | FA)
        self.so = 0                 # zone soak time for a 1000 only
        self.ct = 0                 # zone cycle time for a 1000 only
        
        # these are place holders to keep track of the status of the zone for each minute
        self.seconds_zone_ran = 0
        self.seconds_zone_soaked = 0
        self.seconds_zone_waited = 0
        self.seconds_zone_paused = 0
        # these are place holders to keep track of the status of the zone for each minute
        self.seconds_for_each_cycle_duration = []  # this is the minutes a zone cycled before it soaked
        self.seconds_for_each_soak_duration = []   # this is the minutes a zone soaked before it went to waiting or watering

        self.statuses = ZoneStatuses(_zone_object=self)  # Zone Statuses
        """:type: common.objects.statuses.devices.zones_statuses.ZoneStatuses"""

        self.ef = 0.00
        self.fd = '20000101'
        self.fn = 0.00

    #################################
    def set_default_values(self):
        """
        set the default values of the device on the controller
        :return:
        :rtype:
        """

        if self.controller.controller_type == "10":
            command = "{0},{1}={2},{3}={4},{5}={6},{7}={8},{9}={10},{11}={12},{13}={14},{15}={16},{17}={18}," \
                      "{19}={20},{21}={22},{23}={24},{25}={26}".format(
                          ActionCommands.SET,                               # {0}
                          ZoneCommands.Type.ZONE,                           # {1}
                          str(self.ad),                                     # {2}
                          ZoneCommands.Attributes.ENABLED,                  # {3}
                          str(self.en),                                     # {4}
                          ZoneCommands.Attributes.DESCRIPTION,              # {5}
                          str(self.ds),                                     # {6}
                          ZoneCommands.Attributes.LATITUDE,                 # {7}
                          str(self.la),                                     # {8}
                          ZoneCommands.Attributes.LONGITUDE,                # {9}
                          str(self.lg),                                     # {10}
                          ZoneCommands.Attributes.DESIGN_FLOW,              # {11}
                          str(self.df),                                     # {12}
                          ZoneCommands.Attributes.MIRRORED_ZONE,            # {13}
                          str(self.mr),                                     # {14}
                          ZoneCommands.Attributes.MASTER_VALVE,             # {15}
                          str(self.mv),                                     # {16}
                          ZoneCommands.Attributes.HIGH_FLOW_VARIANCE,       # {17}
                          str(self.ff),                                     # {18}
                          WaterSenseCodes.Crop_Coefficient,                 # {19}
                          str(self.kc),                                     # {20}
                          WaterSenseCodes.Precipitation_Rate,               # {21}
                          str(self.pr),                                     # {22}
                          WaterSenseCodes.Efficiency_Percentage,            # {23}
                          str(self.du),                                     # {24}
                          WaterSenseCodes.Root_Zone_Working_Water_Storage,  # {25}
                          str(self.rz)                                      # {26}
                      )
        else:
            command = "{0},{1}={2},{3}={4},{5}={6},{7}={8},{9}={10},{11}={12},{13}={14},{15}={16},{17}={18}," \
                      "{19}={20},{21}={22}".format(
                          ActionCommands.SET,                               # {0}
                          opcodes.zone,                                     # {1}
                          str(self.ad),                                     # {2}
                          opcodes.enabled,                                  # {3}
                          str(self.en),                                     # {4}
                          opcodes.description,                              # {5}
                          str(self.ds),                                     # {6}
                          opcodes.latitude,                                 # {7}
                          str(self.la),                                     # {8}
                          opcodes.longitude,                                # {9}
                          str(self.lg),                                     # {10}
                          opcodes.design_flow,                              # {11}
                          str(self.df),                                     # {12}
                          WaterSenseCodes.Crop_Coefficient,                 # {13}
                          str(self.kc),                                     # {14}
                          WaterSenseCodes.Precipitation_Rate,               # {15}
                          str(self.pr),                                     # {16}
                          WaterSenseCodes.Efficiency_Percentage,            # {17}
                          str(self.du),                                     # {18}
                          WaterSenseCodes.Root_Zone_Working_Water_Storage,  # {19}
                          str(self.rz),                                     # {20}
                          opcodes.mainline,                                 # {21}
                          str(self.ml),                                     # {22}
                      )

        try:
            # Attempt to set flow meter default values at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Zone {0}'s 'Default values' to: '{1}'".format(
                str(self.ad),   # {0}
                command         # {1}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Zone {0}'s 'Default values' to: {1}".format(
                str(self.ad),   # {0}
                command)        # {1}
            )

    #################################
    def set_design_flow(self, _gallons_per_minute=None):
        """
        Sets the design flow for the zone on the controller to the value contained in 'self.df'. If the user passes
        in a design flow value as a parameter ('_df'), then it will overwrite the value that the instance contains as
        well as set the new value on the controller. \n
        :param _gallons_per_minute:     Design flow value specified by user to overwrite current value.
        :type _df:      Integer, Float.
        :return:
        """

        # Set instance attribute to design flow passed in prior to setting at the controller
        if _gallons_per_minute is not None:
            # if a int is passed in convert to a float because the controller will return a float value
            if isinstance(_gallons_per_minute, int):
                _gallons_per_minute = float(_gallons_per_minute)
                # Verifies the design flow passed in is an integer or float value
            if not isinstance(_gallons_per_minute, (int, float)):
                e_msg = "Failed trying to set ZN {0} design flow. Invalid design flow type, Received type: {1}, " \
                        "Expected: int or float.".format(
                            str(self.ad),   # {0}
                            type(_gallons_per_minute)       # {1}
                        )
                raise TypeError(e_msg)
            else:
                # Valid type, overwrite current value with new value
                self.df = _gallons_per_minute

        # get base string for set command for sending to controller.
        command = self.build_base_string_for_setters()
        # Command for sending to controller.
        command += ",{0}={1}".format(
            ZoneCommands.Attributes.DESIGN_FLOW,    # {0}
            str(self.df)                            # {1}
        )

        try:
            # Attempt to set design flow at the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Zone {0}'s 'DF' to: '{1}'".format(str(self.ad), str(self.df))
            raise Exception(e_msg)
        else:
            print("Successfully set Zone {0}'s 'Design Flow' to: {1}".format(
                str(self.ad),   # {0}
                str(self.df)    # {1}
            ))

    #################################
    def set_mirror_zone_number(self, _mirrored_zone):
        """
        1000 Specific Method \n
        Sets the zone to mirror the specified zone number. \n
        :param _mirrored_zone:      Zone in which to mirror the current zone with. \n
        :type _mirrored_zone:       Integer \n
        :return:
        """
        # this is a 1000 specific method
        if self.controller.controller_type == "10":

            # Verify integer is passed in
            if not isinstance(_mirrored_zone, int):
                e_msg = "Failed trying to set ZN {0} to mirror a zone. Not an integer passed in. Received type: " \
                        "{1}".format(
                            str(self.ad),           # {0}
                            type(_mirrored_zone)    # {1}
                        )
                raise TypeError(e_msg)

            # Set zone attribute before setting at the controller
            self.mr = _mirrored_zone

            # get base string for set command for sending to controller.
            command = self.build_base_string_for_setters()
            # Command for sending to controller.
            command += ",{0}={1}".format(
                ZoneCommands.Attributes.MIRRORED_ZONE,  # {0}
                str(self.mr)                            # {1}
            )
            try:
                # Send command
                self.ser.send_and_wait_for_reply(tosend=command)
            except Exception:
                e_msg = "Exception occurred trying to set Zone {0} to Mirror Zone: {1}".format(str(self.ad),
                                                                                               str(self.mr))
                raise Exception(e_msg)
            else:
                print("Successfully set Zone {0} to Mirror Zone: {1}".format(str(self.ad), str(self.mr)))
        else:
            raise ValueError("Attempting to Mirror a Zone on a 3200, which is currently not supported.")

    #################################
    def set_master_valve_pointer(self, _mv_number):
        """
        Sets the master valve number passed in as a reference for a master valve object for the zone. It's a pointer,
        which points to the address of the master valve.
        :param _mv_number:  A master valve address to point to.
        :return:
        """
        # this is a 1000 specific method
        if self.controller.controller_type == "10":

            # Verify integer is passed in
            if not isinstance(_mv_number, int):
                e_msg = "Failed trying to set ZN {0}'s master valve. Not an integer passed in. Received type: " \
                        "{1}".format(
                            str(self.ad),           # {0}
                            type(_mv_number)        # {1}
                        )
                raise TypeError(e_msg)

            # Verify value is within accepted master valve address's
            elif _mv_number not in range(1, 8):
                e_msg = "Attempting to set reference to a master valve outside of available MV addresses: " \
                        "{0}".format(str(_mv_number))
                raise ValueError(e_msg)
            else:
                self.mv = _mv_number

            # get base string for set command for sending to controller.
            command = self.build_base_string_for_setters()
            # Command for sending to controller.
            command += ",{0}={1}".format(
                ZoneCommands.Attributes.MASTER_VALVE,   # {0}
                str(self.mv)                            # {1}
            )

            try:
                # Send command
                self.ser.send_and_wait_for_reply(tosend=command)
            except Exception:
                e_msg = "Exception occurred trying to set Zone {0}'s MV to: MV {1}".format(str(self.ad), str(self.mv))
                raise Exception(e_msg)
            else:
                print("Successfully set Zone {0}'s MV to: MV {1}".format(str(self.ad), str(self.mv)))
        else:
            raise ValueError("Attempting to assign MV to a ZN on a 3200, which is currently not supported.")

    #################################
    def set_learn_flow_to_start(self, _time_delay=None):
        """
        - Turns on the learn flow for a particular program or zone
        """
        command = "{0},{1},{2}={3}".format(
            ActionCommands.DO,
            opcodes.learn_flow,
            opcodes.zone,
            self.ad
        )

        # Check if the zone address passed in was an int
        if _time_delay is not None and not isinstance(_time_delay, int):
            e_msg = "Exception occurred trying to enable learn flow. Time delay passed in was an Invalid " \
                    "argument type, expected an int, received: {0}" \
                .format(type(_time_delay))
            raise TypeError(e_msg)

        elif _time_delay is not None:
            command += ",{0}={1}".format(
                opcodes.time_delay,
                _time_delay
            )
        try:
            # Send the learn flow command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set a learn flow on zone '{0}' to start'.".format(
                self.ad)
            raise Exception(e_msg)
        else:
            print "Successfully set the learn flow on zone '{0}' to start.".format(self.ad)

    #################################
    def set_learn_flow_to_start_advanced(self, _time_delay=None):
        """
        - Turns on the learn flow for a particular zone, advanced mode
        """
        command = "{0},{1},{2}={3},TY=AD".format(
            ActionCommands.DO,
            opcodes.learn_flow,
            opcodes.zone,
            self.ad
        )

        # Check if the zone address passed in was an int
        if _time_delay is not None and not isinstance(_time_delay, int):
            e_msg = "Exception occurred trying to enable learn flow. Time delay passed in was an Invalid " \
                    "argument type, expected an int, received: {0}" \
                .format(type(_time_delay))
            raise TypeError(e_msg)

        elif _time_delay is not None:
            command += ",{0}={1}".format(
                opcodes.time_delay,
                _time_delay
            )
        try:
            # Send the learn flow command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set a learn flow on zone '{0}' to start'.".format(
                self.ad)
            raise Exception(e_msg)
        else:
            print "Successfully set the learn flow on zone '{0}' to start.".format(self.ad)

    #################################
    def set_learn_flow_to_start_quick(self, _time_delay=None):
        """
        - Turns on the learn flow for a particular zone, quick mode
        """
        command = "{0},{1},{2}={3},TY=QK".format(
            ActionCommands.DO,
            opcodes.learn_flow,
            opcodes.zone,
            self.ad
        )

        # Check if the zone address passed in was an int
        if _time_delay is not None and not isinstance(_time_delay, int):
            e_msg = "Exception occurred trying to enable learn flow. Time delay passed in was an Invalid " \
                    "argument type, expected an int, received: {0}" \
                .format(type(_time_delay))
            raise TypeError(e_msg)

        elif _time_delay is not None:
            command += ",{0}={1}".format(
                opcodes.time_delay,
                _time_delay
            )
        try:
            # Send the learn flow command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set a learn flow on zone '{0}' to start'.".format(
                self.ad)
            raise Exception(e_msg)
        else:
            print "Successfully set the learn flow on zone '{0}' to start.".format(self.ad)

    #################################
    def set_learn_flow_to_start_standard(self, _time_delay=None):
        """
        - Turns on the learn flow for a particular zone, standard mode
        """
        command = "{0},{1},{2}={3},TY=SD".format(
            ActionCommands.DO,
            opcodes.learn_flow,
            opcodes.zone,
            self.ad
        )

        # Check if the zone address passed in was an int
        if _time_delay is not None and not isinstance(_time_delay, int):
            e_msg = "Exception occurred trying to enable learn flow. Time delay passed in was an Invalid " \
                    "argument type, expected an int, received: {0}" \
                .format(type(_time_delay))
            raise TypeError(e_msg)

        elif _time_delay is not None:
            command += ",{0}={1}".format(
                opcodes.time_delay,
                _time_delay
            )
        try:
            # Send the learn flow command to the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set a learn flow on zone '{0}' to start'.".format(
                self.ad)
            raise Exception(e_msg)
        else:
            print "Successfully set the learn flow on zone '{0}' to start.".format(self.ad)

    #################################
    def update_design_flow_value_after_learn_flow(self, _flow_meter_address):
        """
        - takes in the flow meter reading and updates the zone design flow
        """
        # get the serial number of the flow meter
        flow_meter = self.controller.flow_meters[_flow_meter_address]

        command = "{0},{1}={2}".format(
            ActionCommands.GET,
            opcodes.flow_meter,
            flow_meter.sn
        )
        data = self.ser.get_and_wait_for_reply(tosend=command)
        fm_vr_value = data.get_value_string_by_key(opcodes.flow_rate)

        # update df with flow meter value
        self.df = float(fm_vr_value)

        print "Successfully set a new design flow value for zone {0} flow meter value was: '{1} New design flow value " \
              "is {2}'"\
            .format(
                self.ad,
                fm_vr_value,
                self.df
            )

    #################################
    def set_high_flow_variance(self, _value=None):
        """
        1000 Specific method \n
        Sets the high flow variance value for the zone on the controller. \n
        By passing in a value for '_value', method assumes this value to overwrite current value. \n
        :rtype : object
        :param _value:  'TR' for a true, 'FA' for a false
        :type _value:   int \n
        """
        # this is a 1000 specific method
        if self.controller.controller_type == "10":

            # Check if user wants to overwrite current value
            if _value is not None:

                # Verify integer is passed in
                if not isinstance(_value, str):
                    e_msg = "Failed trying to set ZN {0}'s high flow variance. Not a string passed in. Received " \
                            "type: {1}".format(str(self.ad), type(_value))
                    raise TypeError(e_msg)

                # Make sure entered correct value
                elif _value not in ["FA", "TR"]:
                    e_msg = "Exception occurred attempting to set incorrect 'High Flow Variance' value for " \
                            "controller: {0}. Expects 'TR' or 'FA'.".format(str(_value))
                    raise ValueError(e_msg)
                else:
                    # Valid type, overwrite current value with new value
                    self.ff = _value

            # get base string for set command for sending to controller.
            command = self.build_base_string_for_setters()
            # Command for sending to controller.
            command += ",{0}={1}".format(
                ZoneCommands.Attributes.HIGH_FLOW_VARIANCE,     # {0}
                str(self.ff)                                    # {1}
            )

            try:
                # Send command
                self.ser.send_and_wait_for_reply(tosend=command)
            except Exception:
                e_msg = "Exception occurred trying to set Zone {0}'s high flow variance to: {1}".format(
                    str(self.ad), str(self.ff))
                raise Exception(e_msg)
            else:
                print("Successfully set Zone {0}'s high flow variance to: {1}".format(str(self.ad), str(self.ff)))
        else:
            raise ValueError("Attempting to set high flow variance to a ZN on a 3200, which is currently not "
                             "supported.")

    #################################
    def set_crop_coefficient_value(self, _kc_value=None, _ks_value=None, _kd_value=None, _kmc_value=None):
        """
        - set a crop coefficient value on the zone on the controller \n
            - Send an KC value
        :param _kc_value:     KC value range between (0.00 and 5.00) and only 2 decimals 3.45 \n
        :type  _kc_value:     float

        :param _ks_value:     Species factor value range between (0.00 and 5.00) and only 2 decimals 3.45 \n
        :type  _ks_value:     float

        :param _kd_value:     Density factor value range between (0.00 and 5.00) and only 2 decimals 3.45 \n
        :type  _kd_value:     float

        :param _kmc_value:    Micro Climate value range between (0.00 and 5.00) and only 2 decimals 3.45 \n
        :type  _kmc_value:    float

        """
        # check to see if we are using_kc_value or _ks_value,_kd_value, _kmc_value
        # if _kc_value is none than _ks_value, _kd_value, _kmc_value must not be none
        if _kc_value is None and _ks_value is not None and _kd_value is not None and _kmc_value is not None:

            # Check if  _ks, and _kd_kmc are in range 0.00 - 5.00
            if (_ks_value < 0.00 or _ks_value > 5.00) \
                    or (_kd_value < 0.00 or _kd_value > 5.00) \
                    or (_kmc_value < 0.00 or _kmc_value > 5.00):
                e_msg = "Exception occurred trying to set the crop coefficient value. \n" \
                        "Species factor received was and Invalid was not between (0.00 and 5.00) received: {0}. \n" \
                        "Density factor received was and Invalid was not between (0.00 and 5.00) received: {1}. \n" \
                        "Micro Climate received was and Invalid was not between (0.00 and 5.00) received: {2}.\n"\
                        .format(
                            _ks_value,          # {0}
                            _kd_value,          # {1}
                            _kmc_value,         # {2}
                        )
                raise ValueError(e_msg)

            else:
                 # this returns the _kl which is set to the kc value
                cal_kl_value = equations.calculate_landscape_coefficient(_ks=_ks_value, _kd=_kd_value, _kmc=_kmc_value)

                # this just verifies after the math we make sure we still make the kc value a two decimal number
                self.kc = cal_kl_value

        else:
            # Check if  _kc is in range 0.00 - 5.00
            if (_kc_value < 0.00 or _kc_value > 5.00):
                e_msg = "Exception occurred trying to set the crop coefficient value. \n" \
                        "crop coefficient received was and Invalid was not between (0.00 and 5.00) received: {0}. \n" \
                        .format(
                            _kc_value,          # {0}
                        )
                raise ValueError(e_msg)
            self.kc = _kc_value

        # get base string for set command for sending to controller.
        command = self.build_base_string_for_setters()
        # Command for sending to controller.
        command += ",{0}={1}".format(
            WaterSenseCodes.Crop_Coefficient,   # {0}
            self.kc                             # {1}
        )

        try:
            # Attempt to set ETo and date stamp on the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set the crop coefficient value on the zone: value = '{0}' " \
                    .format(self.kc)
            raise Exception(e_msg)
        print("Successfully set the initial crop coefficient value on controller and all zones = '{0}' "
              .format(self.kc))

    #################################
    def set_precipitation_rate_value(self, _pr_value):
        """
        - set a precipitation_rate value on the zone on the controller \n
            - Send an precipitation rate value
        :param _pr_value:     Precipitation rate value range between (0.00 and 5.00) and only 2 decimals 3.45 \n
        :type  _pr_value:     float \n
        """
        # if not _pr_value in range(0.00, 5.00):
        if _pr_value < 0.0 or _pr_value > 5.0:
            e_msg = "Exception occurred trying to set the precipitation rate value. Value received {0} was not between"\
                    "(0.00 and 5.00)"\
                    .format(_pr_value)
            raise ValueError(e_msg)
        self.pr = helper_methods.truncate_float_value(_float_value=_pr_value)

        # get base string for set command for sending to controller.
        command = self.build_base_string_for_setters()
        # Command for sending to controller.
        command += ",{0}={1}".format(
            ZoneCommands.Attributes.PRECIPITATION_RATE,     # {0}
            self.pr)                                        # {1}

        try:
            # Attempt to set ETo and date stamp on the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set the precipitation rate value on the zone " \
                    "value = '{0}' " \
                    .format(self.pr)
            raise Exception(e_msg)
        print("Successfully set the initial precipitation rate value on zone = '{0}' "
              .format(self.pr))

    #################################
    def set_distribution_uniformity_value(self, _du_value , web=True):
        """
        - set a Distribution Uniformity value on the zone on the controller \n
            - Send an Distribution Uniformity value
        :param _du_value:     Distribution Uniformity value range between (0.00 and 1.00) and only 2 decimals 3.45 \n
        :type  _du_value:     float
        """
        # Check if in range 0.00 - 1.0
        if int(_du_value < 1 or _du_value > 100):
            e_msg = "Exception occurred trying to set the Distribution Uniformity value. Value received {0} was not " \
                    "between(1 and 100) "\
                    .format(_du_value)
            raise ValueError(e_msg)

        self.du = _du_value

        # get base string for set command for sending to controller.
        command = self.build_base_string_for_setters()
        # Command for sending to controller.
        command += ",{0}={1}".format(
            WaterSenseCodes.Efficiency_Percentage,      # {0}
            self.du                                     # {1}
        )

        try:
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set the Distribution Uniformity value on the zone " \
                    "value = '{0}' " \
                    .format(self.du)
            raise Exception(e_msg)
        else:
            print("Successfully set the initial Distribution Uniformity value on zone = '{0}' "
                  .format(self.du))

    #################################
    def set_root_zone_working_storage_capacity(self):
        """
        - set a Root Zone Water Holding Capacity  value on the zone on the controller \n
            - Send an Root zone water holding capacity  value
        :return self.rz:        Root zone water holding capacity value range between (0.00 and 1.00) and only 2 \n
                                decimals 3.45 \n
        :rtype  self.rz:      float \n
        """
        # Recalculate rz value (with current zone attributes)
        self.rz = equations.return_calculate_root_zone_working_water_storage(
            _sl=self.soil_type,     # the soil Type is set at the script
            _ad=self.allowable_depletion,     # the allowable depletion is set at the script
            _rd=self.root_depth)         # the root_depth is set at the script

        # Overwrite current attribute value
        self.rz = helper_methods.truncate_float_value(_float_value=self.rz)

        # get base string for set command for sending to controller.
        command = self.build_base_string_for_setters()
        # Command for sending to controller.
        command += ",{0}={1}".format(
            WaterSenseCodes.Root_Zone_Working_Water_Storage,    # {0}
            self.rz                                             # {1}
        )

        try:
            # Attempt to set Root Zone Water Holding Capacity on the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set the Root Zone Water Holding Capacity value on the zone " \
                    "value = '{0}' " \
                    .format(self.rz)
            raise Exception(e_msg)
        else:
            print("Successfully set the initial Root Zone Water Holding Capacity value on zone = '{0}' "
                  .format(self.rz))

    #################################
    def set_cycle_time(self, _cycle_time=None, _use_calculated_cycle_time=False):
        """
        Sets the 'Cycle Time' for the Zone Program on the controller. \n
        :param _cycle_time:                     Cycle time to set \n
        :type _cycle_time:                      int \n

        :param _use_calculated_cycle_time:      Cycle time to set \n
        :type _use_calculated_cycle_time:       bool  | float\n
        """

        if self.controller.controller_type == "10":
            # this goes out to the equations module and returns a calculated Cycle time to the zp.py object
            if _use_calculated_cycle_time is True:
                _cycle_time = self.get_calculated_cycle_time()
            # If trying to overwrite current value
            if _cycle_time is not None:

                # Overwrite current attribute value
                # take the value in seconds divide by 60 to convert it to minutes convert it to an integer and than
                # convert back to seconds this way you are dropping of the fractional part of the seconds
                self.ct = 60*(int(_cycle_time/60))

            # get base string for set command for sending to controller.
            command = self.build_base_string_for_setters()
            # Command for sending to controller.
            command += ",{0}={1}".format(
                ZoneCommands.Attributes.CYCLE_TIME,         # {0}
                str(self.ct)                                # {1}
            )
            try:
                self.ser.send_and_wait_for_reply(tosend=command)
            except AssertionError as ae:
                e_msg = "Exception occurred trying to set (Zone {0}, 'Cycle Time' to: {1}\n " \
                        "{2}".format(
                            str(self.ad),               # {0}
                            str(self.ct),               # {1}
                            str(ae.message)             # {2}
                        )
                raise Exception(e_msg)
            else:
                print("Successfully set 'Cycle Time' for (Zone {0}, to: {1}".format(
                      str(self.ad),      # {0}
                      str(self.ct)       # {1}
                      ))
        else:
            raise ValueError("Attempting to set Zone Cycle Time not Zone Program Cycle Time for 3200 "
                             "which is NOT SUPPORTED")

    #################################
    def set_soak_time(self, _soak_time=None, _use_calculated_soak_time=False):
        """
        Sets the 'Soak Time' for the Zone Program on the controller. \n
        :param _soak_time:                      Soak time to set \n
        :type _soak_time:                       int  | float\n

        :param _use_calculated_soak_time:       Soak time to set \n
        :type _use_calculated_soak_time:        bool  | float\n
        """
        if self.controller.controller_type == "10":
            # this goes out to the equations module and returns a calculated soak time to the zp.py object
            if _use_calculated_soak_time is True:
                _soak_time = self.get_calculated_soak_time()

            # If trying to overwrite current value
            if _soak_time is not None:

                # Overwrite current attribute value
                # take the value in seconds divide by 60 to convert it to minutes convert it to an integer and than
                # add 1 to the value to always round up and then convert back to seconds
                if _soak_time == 0:
                    self.so = 0
                else:
                    self.so = 60*(math.ceil(_soak_time/60.0))

            # get base string for set command for sending to controller.
            command = self.build_base_string_for_setters()
            # Command for sending to controller.
            command += ",{0}={1}".format(
                ZoneCommands.Attributes.SOAK_TIME,      # {0}
                str(self.so)                            # {1}
            )
            try:
                self.ser.send_and_wait_for_reply(tosend=command)
            except AssertionError as ae:
                e_msg = "Exception occurred trying to set (Zone {0}, 'Soak Time' to: {1}\n " \
                        "{2}".format(
                            str(self.ad),               # {0}
                            str(self.so),               # {1}
                            str(ae.message)             # {2}
                        )
                raise Exception(e_msg)
            else:
                print("Successfully set 'Soak Time' for (Zone {0}, to: {1}".format(
                      str(self.ad),      # {0}
                      str(self.so)       # {1}
                      ))
        else:
            raise ValueError("Attempting to set zone Soak Time for a Zone for 3200 which is NOT SUPPORTED")

    #################################
    def do_run_manual_zone_type_1(self, _hours=None, _minutes=None, _seconds=None, _delay_first=None, _delay_between = None):
        """
        - Start a manual run for a zone, run zone for a certain amount of minutes
        - if a zone another zone is running this will stop that zone to run this zone
        :param _hours:              hours to run each zone
        :type _hours:               int
        :param _minutes:            time to set in seconds
        :type _minutes:             int
        :param _seconds:            time to set in seconds
        :type _seconds:             int
        :param _delay_first:        seconds to delay before turning on first zone
        :type _delay_first:         int
        :param _delay_between:      seconds to delay between each zone
        :type _delay_between        int
        """

        time_to_run = 0
        delay_1 = 0
        delay_2 = 0

        if _seconds:
            if not int(_seconds):
                e_msg = "Second must be an integer value"
                raise ValueError(e_msg)
            time_to_run = _seconds

        if _minutes:
            if not int(_minutes):
                e_msg = "Minutes must be an integer value"
                raise ValueError(e_msg)
            time_to_run += 60 * _minutes

        if _hours:
            if not int(_hours):
                e_msg = "Hours must be an integer value"
                raise ValueError(e_msg)
            time_to_run += 60 * 60 * _hours

        if _delay_first:
            if int(_delay_first):
                delay_1 = int(_delay_first)

        if _delay_between:
            if int(_delay_between):
                delay_2 = int(_delay_between)

        # Command for sending
        command = "{0},{1},{2}={3},{4}={5},{6}={7},D1={8},D2={9}".format(
            ActionCommands.DO,          # {0}
            opcodes.manual_running,     # {1}
            opcodes.type,               # {2}
            str(1),                     # {3} only run one zone at a time
            opcodes.zone,               # {4}
            self.ad,                    # {5}
            opcodes.run_time,           # {6}
            str(time_to_run),           # {7}
            str(delay_1),               # {8}
            str(delay_2),               # {9}
        )
        try:
            # Attempt to start a program
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to start zone '{0}'".format(self.ad)
            raise Exception(e_msg)
        print("Successfully started zone'{0}'.".format(self.ad))

    #################################
    def do_run_manual_zone_type_5(self, _hours=None, _minutes=None, _seconds=None):
        """
        - Start a manual run for a zone, run zone for a certain amount of minutes
        - if a zone another zone is running it will not be stopped this will continue to run the zone
        and add start the next zone
        :param _hours:              hours to run each zone
        :type _hours:               int
        :param _minutes:            time to set in minutes
        :type _minutes:             int
        :param _seconds:            time to set in seconds
        :type _seconds:             int
        """

        time_to_run = 0

        if _seconds:
            if not int(_seconds):
                e_msg = "Second must be an integer value"
                raise ValueError(e_msg)
            time_to_run = _seconds

        if _minutes:
            if not int(_minutes):
                e_msg = "Minutes must be an integer value"
                raise ValueError(e_msg)
            time_to_run += 60 * _minutes

        if _hours:
            if not int(_hours):
                e_msg = "Hours must be an integer value"
                raise ValueError(e_msg)
            time_to_run += 60 * 60 * _hours

        # Command for sending
        command = "{0},{1},{2}={3},{4}={5},{6}={7}".format(
            ActionCommands.DO,          # {0}
            opcodes.manual_running,     # {1}
            opcodes.type,               # {2}
            str(5),                     # {3} continually add zones
            opcodes.zone,               # {4}
            self.ad,                    # {5}
            opcodes.run_time,           # {6}
            str(time_to_run),           # {7}
        )
        try:
            # Attempt to start a program
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to start zone '{0}'".format(self.ad)
            raise Exception(e_msg)
        print("Successfully started zone'{0}'.".format(self.ad))

    #################################
    def set_to_manually_stop_a_single_zone(self):
        """
        - Stop a manual run for a zone
        """
        time_to_run = 0 # by setting to zero you are telling the zone to stop

        # Command for sending
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.DO,  # {0}
            opcodes.manual_running,  # {1}
            opcodes.type,  # {2}
            str(1),  # {3} only run one zone at a time
            self.ad,  # {4}
            opcodes.run_time,  # {5}
            str(time_to_run),  # {6}
        )
        try:
            # Attempt to start a program
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to stop zone '{0}'".format(self.ad)
            raise Exception(e_msg)
        print("Successfully stoped zone'{0}'.".format(self.ad))

    #################################
    def set_to_manually_stop_all_zones(self):
        """
        - Stop a manual run for all zones currently running
        """
        time_to_run = 0 # by setting to zero you are telling the zone to stop

        # Command for sending
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.DO,  # {0}
            opcodes.manual_running,  # {1}
            opcodes.type,  # {2}
            str(5),  # {3} only run one zone at a time
            self.ad,  # {4}
            opcodes.run_time,  # {5}
            str(time_to_run),  # {6}
        )
        try:
            # Attempt to start a program
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to stop zone '{0}'".format(self.ad)
            raise Exception(e_msg)
        print("Successfully stoped zone'{0}'.".format(self.ad))

    #################################
    def get_calculated_cycle_time(self):

        _calculated_cycle_time = equations.return_calculated_cycle_time(
            _sl=self.soil_type,     # the soil type is set at the script
            _pr=self.pr,            # the precipitation rate is set at the script
            _sp=self.slope)         # the soil is set at the script
        return _calculated_cycle_time

    #################################
    def get_calculated_soak_time(self):

        _calculated_soak_time = equations.return_calculated_soak_time(
            _sl=self.soil_type,     # the soil type is set at the script
            _pr=self.pr,            # the precipitation rate is set at the script
            _sp=self.slope,         # the soil is set at the script
            _du=self.du)            # the distribution uniformity is set at the script
        return _calculated_soak_time

    #################################
    def verify_crop_coefficient_value(self):
        """
        Verifies the crop coefficient set for this Zone on the Controller. Expects the controller's value and this
        instance's value to be equal.
        :return:
        """
        zn_kc_on_cn = float(self.data.get_value_string_by_key(WaterSenseCodes.Crop_Coefficient))

        # Compare status versus what is on the controller
        if abs(self.kc - zn_kc_on_cn) > .0001:
            e_msg = "Unable to verify Zone {0}'s crop coefficient. Received: {1}, Expected: {2}".format(
                str(self.ad),       # {0}
                str(zn_kc_on_cn),   # {1}
                str(self.kc)        # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Zone {0}'s crop coefficient: '{1}' on controller".format(
                  str(self.ad),     # {0}
                  str(self.kc)      # {1}
                  ))

    #################################
    def verify_precipitation_rate_value(self):
        """
        Verifies the precipitation rate set for this Zone on the Controller. Expects the controller's value and this
        instance's value to be equal.
        :return:
        """
        zn_pr_on_cn = float(self.data.get_value_string_by_key(opcodes.precipitation_rate))

        # Compare status versus what is on the controller
        if self.pr != zn_pr_on_cn:
            e_msg = "Unable to verify Zone {0}'s precipitation rate. Received: {1}, Expected: {2}".format(
                str(self.ad),       # {0}
                str(zn_pr_on_cn),   # {1}
                str(self.pr)        # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Zone {0}'s precipitation rate: '{1}' on controller".format(
                  str(self.ad),     # {0}
                  str(self.pr)      # {1}
                  ))

    #################################
    def verify_distribution_uniformity_value(self):
        """
        Verifies the Distribution Uniformity set for this Zone on the Controller. Expects the controller's value and
        this instance's value to be equal.
        :return:
        """
        zn_du_on_cn = float(self.data.get_value_string_by_key(WaterSenseCodes.Efficiency_Percentage))

        # Compare status versus what is on the controller
        if self.du != zn_du_on_cn:
            e_msg = "Unable to verify Zone {0}'s Distribution Uniformity. Received: {1}, Expected: {2}".format(
                str(self.ad),       # {0}
                str(zn_du_on_cn),   # {1}
                str(self.du)        # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Zone {0}'s Distribution Uniformity: '{1}' on controller".format(
                  str(self.ad),     # {0}
                  str(self.du)      # {1}
                  ))

    #################################
    def verify_root_zone_working_storage_capacity(self):
        """
        Verifies the Root Zone Water Holding Capacity set for this Zone on the Controller. Expects the controller's
        value and this instance's value to be equal.
        :return:
        """
        zn_rz_on_cn = float(self.data.get_value_string_by_key(WaterSenseCodes.Root_Zone_Working_Water_Storage))

        # Compare status versus what is on the controller
        if self.rz != zn_rz_on_cn:
            e_msg = "Unable to verify Zone {0}'s Root Zone Water Holding Capacity . Received: {1}, Expected: {2}"\
                .format(
                    str(self.ad),       # {0}
                    str(zn_rz_on_cn),   # {1}
                    str(self.rz)        # {2}
                )
            raise ValueError(e_msg)
        else:
            print("Verified Zone {0}'s Root Zone Water Holding Capacity: '{1}' on controller".format(
                  str(self.ad),     # {0}
                  str(self.rz)      # {1}
                  ))

    #################################
    def verify_moisture_balance(self):
        """
        Verifies the moisture balance set for this Zone on the Controller. Expects the controller's value and this
        instance's value to be equal.
        :return:
        """
        self.mb = equations.return_todays_calculated_moisture_balance(
            _yesterdays_mb=self.yesterdays_mb,
            _yesterdays_rt=self.yesterdays_rt,
            _pr=self.pr,
            _du=self.du,
            _ra=self.ra,
            _etc=self.etc,
        )
        # check Moisture balance against the root zone working storage capacity
        # - if the calculate moisture balance is higher than the root zone working storage capacity
        # -  than set the moisture balance equal to the root zone working storage capacity so that there is not
        # -  more water applied than the root zone can handle
        # rz is a positive number after calculation mb is a neg number
        # this makes rz a negative for comparison
        neg_rz = -self.rz
        if self.mb < neg_rz:
            self.mb = neg_rz
            print "Moisture Balance was adjusted due to the fact that the root zone couldn't hold that much water"
        print("Print numbers use to Calculate MB  yesterdays_mb: '{0}' yesterdays_rt: '{1}' kc: '{2}'  "
              "pr: '{3} du: '{4}' ra: '{5}'  etc: '{6}' Calculated mb: '{7}' on controller".format(
                  str(self.yesterdays_mb),      # {0}
                  str(self.yesterdays_rt),      # {1}
                  str(self.kc),                 # {2}
                  str(self.pr),                 # (3)
                  str(self.du),                 # {4}
                  str(self.ra),                 # {5}
                  str(self.etc),                # {6}
                  str(self.mb),))               # {7}
        # Compare status versus what is on the controller
        # Todo add if runtime is less than 4 minutes make runtime = to zero
        zn_mb_on_cn = float(self.data.get_value_string_by_key(WaterSenseCodes.Daily_Moisture_Balance))
        rounded_mb = round(number=self.mb, ndigits=3)
        if abs(rounded_mb - zn_mb_on_cn) > 0.01:
            e_msg = "Unable to verify Zone {0}'s daily moisture balance. Received: {1}, Expected: {2}".format(
                str(self.ad),       # {0}
                str(zn_mb_on_cn),   # {1}
                str(rounded_mb)        # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Zone {0}'s calculated daily moisture balance: '{1}' against daily moisture balance on "
                  "controller: '{2}'".format(
                    str(self.ad),     # {0}
                    str(self.mb),     # {1}
                    str(zn_mb_on_cn)  # {2}
                    ))

    #################################
    def verify_design_flow(self):
        """
        Verifies the design flow set for this Zone on the Controller. Expects the controller's value and this
        instance's value to be equal.
        :return:
        """
        zn_df_on_cn = float(self.data.get_value_string_by_key('DF'))

        # Compare status versus what is on the controller
        if abs(self.df - zn_df_on_cn) > 0.01:
            e_msg = "Unable to verify Zone {0}'s design flow. Received: {1}, Expected: {2}".format(
                str(self.ad),       # {0}
                str(zn_df_on_cn),   # {1}
                str(self.df)        # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Zone {0}'s design flow: '{1}' on controller".format(
                  str(self.ad),     # {0}
                  str(self.df)      # {1}
                  ))

    #################################
    def verify_mainline_assignment(self):
        """
        Verifies the mainline assignment set for this Zone on the Controller. Expects the controller's value and this
        instance's value to be equal.
        :return:
        """
        zn_ml_on_cn = int(self.data.get_value_string_by_key('ML'))

        # Compare status versus what is on the controller
        if self.ml != zn_ml_on_cn:
            e_msg = "Unable to verify Zone {0}'s mainline assignment. Received: {1}, Expected: {2}".format(
                str(self.ad),       # {0}
                str(zn_ml_on_cn),   # {1}
                str(self.ml)        # {2}
            )
            raise ValueError(e_msg)
        else:
            print("Verified Zone {0}'s mainline assignment: '{1}' on controller".format(
                str(self.ad),     # {0}
                str(self.ml)      # {1}
            ))

    #################################
    def verify_learn_flow(self, _expected_flow_value):
        """
        Verifies the design flow on the controller matches after a learn flow the expected value of the object.
        :param _expected_flow_value: flow value expected after learn flow
        :type _expected_flow_value:  float, int
        """
        # if a int is passed in convert to a float because the controller will return a float value
        if isinstance(_expected_flow_value, int):
            _expected_flow_value = float(_expected_flow_value)
        # Verify float is passed in
        if not isinstance(_expected_flow_value, float):
            e_msg = "The value passed in as expected flow must be a float. Value passed in = {0}'s  Received " \
                    "type: {1}".format(
                        str(_expected_flow_value),  # value passed in for expected flow value
                        type(_expected_flow_value)  # The type of the value passed in for expected flow value
            )
            raise TypeError(e_msg)

        # do a get data here so that we get the zone data from the controller and update .data
        self.get_data()
        # get the Design flow value of the object from .data
        zn_df_on_cn = float(self.data.get_value_string_by_key(opcodes.design_flow))

        # set the expected flow valued to the design flow value of the zone object
        self.df = _expected_flow_value

        # Compare value of the object to what the controller returned
        if self.df != zn_df_on_cn:
            e_msg = "Zone {0}'s did not learn flow correctly the design flow. Received: {1}, Expected: {2} did not" \
                    "match.".format(
                        str(self.ad),       # {0} address of zone
                        str(zn_df_on_cn),   # {1} design flow returned from the controller
                        str(self.df)        # {2} design flow expected
            )
            raise ValueError(e_msg)
        else:
            print("Verified Zone {0}'s learned flow and set the design flow to: '{1}' on controller".format(
                str(self.ad),  # {0}
                str(self.df)  # {1}
            ))

    #################################
    def verify_mirrored_zone(self):
        """
        Verifies the mirrored zone value associated with this zone on the controller.
        :return:
        """
        # this is a 1000 specific method
        if self.controller.controller_type == "10":
            mirrored_zone_val = int(self.data.get_value_string_by_key(opcodes.mirror_zone))

            # Compare mirrored zone value returned versus what is assigned to the zone currently
            if self.mr != mirrored_zone_val:
                e_msg = "Unable to verify Zone {0}'s mirrored zone. Received: {1}, Expected: {2}".format(
                    str(self.ad),               # {0}
                    str(mirrored_zone_val),     # {1}
                    str(self.mr)                # {2}
                )
                raise ValueError(e_msg)
            else:
                print("Verified Zone {0}'s mirrored zone value: '{1}' on controller".format(
                    str(self.ad),     # {0}
                    str(self.mr)      # {1}
                    ))
        else:
            raise ValueError("Attempting to verify mirrored zone for a zone on a 3200, which is currently not "
                             "supported.")

    #################################
    def verify_master_valve_assigned(self):
        """
        Verifies the master valve assigned to this zone. \n
        :return:
        """
        # this is a 1000 specific method
        if self.controller.controller_type == "10":
            assigned_mv_zone_val = self.data.get_value_string_by_key('MV')
            if assigned_mv_zone_val is not None:
                # TODO this is giving me a not literal int type in some cases (running 1000 2015 use case 6)
                # assigned_mv_zone_val = int(assigned_mv_zone_val)
                # Compare master valve assignment returned versus what is assigned to the zone currently
                if self.mv != int(assigned_mv_zone_val):
                    e_msg = "Unable to verify Zone {0}'s Master Valve pointer. Received: {1}, Expected: {2}".format(
                        str(self.ad),               # {0}
                        str(assigned_mv_zone_val),  # {1}
                        str(self.mv)                # {2}
                    )
                    raise ValueError(e_msg)
                else:
                    print("Verified Zone {0}'s assignment to master value: '{1}' on controller".format(
                        str(self.ad),     # {0}
                        str(self.mv)      # {1}
                    ))
            else:
                print("Verified Zone {0}'s master value: '{1}' on controller is set to None".format(
                    str(self.ad),     # {0}
                    str(self.mv)      # {1}
                ))
        else:
            raise ValueError("Attempting to verify Master valve assignment for a zone on a 3200, which is "
                             "currently not supported.")

    #################################
    def verify_high_flow_variance_val(self):
        """
        verifies the high flow variance value on the controller versus what the object currently has set for a value. \n
        :return:
        """
        # this is a 1000 specific method
        if self.controller.controller_type == "10":
            high_flow_variance_val = self.data.get_value_string_by_key(opcodes.high_flow_variance)
            if high_flow_variance_val != 'FA':
                high_flow_variance_val = int(high_flow_variance_val)
                # Compare high flow variance value returned versus what is assigned to the zone currently
                if self.ff != high_flow_variance_val:
                    e_msg = "Unable to verify Zone {0}'s high flow variance. Received: {1}, Expected: {2}".format(
                        str(self.ad),                   # {0}
                        str(high_flow_variance_val),    # {1}
                        str(self.ff)                    # {2}
                    )
                    raise ValueError(e_msg)
                else:
                    print("Verified Zone {0}'s high flow variance value: '{1}' on controller".format(
                        str(self.ad),     # {0}
                        str(self.ff)      # {1}
                        ))
            else:
                print("Verified Zone {0}'s high flow variance value: '{1}' on controller is set to False".format(
                    str(self.ad),     # {0}
                    str(self.ff)      # {1}
                ))
        else:
            raise ValueError("Attempting to verify high flow variance for a zone on a 3200, which is currently "
                             "not supported.")

    #################################
    def replace_single_valve_bicoder(self, _new_serial_number):
        """
        Replaces the bicoder with a new one with the specified new serial number. \n

        :param _new_serial_number:  The serial number that will be loaded onto the controller. \n
        :type _new_serial_number:   str \n
        """
        # Load the device on the controller, and in the process making the new bicoder object
        if _new_serial_number not in self.controller.valve_bicoders.keys():
            self.controller.load_dv(opcodes.single_valve_decoder, [_new_serial_number], device_type=opcodes.zone)
            self.controller.do_search_for_zones()

        # Assign the new bicoder and update the device serial number
        self.bicoder = self.controller.valve_bicoders[_new_serial_number]
        self.sn = self.bicoder.sn
        self.bicoder.ad = self.ad
        self.set_address()

        # Update all the objects attributes to stay consistent
        self.bicoder.self_test_and_update_object_attributes()

    #################################
    def replace_dual_valve_bicoder(self, _new_serial_number):
        """
        Replaces the bicoder with a new one with the specified new serial number. \n

        :param _new_serial_number:  The serial number that will be loaded onto the controller. \n
        :type _new_serial_number:   str \n
        """
        # Load the device on the controller, and in the process making the new bicoder object
        if _new_serial_number not in self.controller.valve_bicoders.keys():
            self.controller.load_dv(opcodes.two_valve_decoder, [_new_serial_number], device_type=opcodes.zone)
            self.controller.do_search_for_zones()

        # Assign the new bicoder and update the device serial number
        self.bicoder = self.controller.valve_bicoders[_new_serial_number]
        self.sn = self.bicoder.sn
        self.bicoder.ad = self.ad
        self.set_address()

        # Update all the objects attributes to stay consistent
        self.bicoder.self_test_and_update_object_attributes()

    #################################
    def replace_quad_valve_bicoder(self, _new_serial_number):
        """
        Replaces the bicoder with a new one with the specified new serial number. \n

        :param _new_serial_number:  The serial number that will be loaded onto the controller. \n
        :type _new_serial_number:   str \n
        """
        # Load the device on the controller, and in the process making the new bicoder object
        if _new_serial_number not in self.controller.valve_bicoders.keys():
            self.controller.load_dv(opcodes.four_valve_decoder, [_new_serial_number], device_type=opcodes.zone)
            self.controller.do_search_for_zones()

        # Assign the new bicoder and update the device serial number
        self.bicoder = self.controller.valve_bicoders[_new_serial_number]
        self.bicoder.ad = self.ad
        self.sn = self.bicoder.sn
        self.set_address()

        # Update all the objects attributes to stay consistent
        self.bicoder.self_test_and_update_object_attributes()

    #################################
    def replace_twelve_valve_bicoder(self, _new_serial_number):
        """
        Replaces the bicoder with a new one with the specified new serial number. \n

        :param _new_serial_number:  The serial number that will be loaded onto the controller. \n
        :type _new_serial_number:   str \n
        """
        # Load the device on the controller, and in the process making the new bicoder object
        if _new_serial_number not in self.controller.valve_bicoders.keys():
            self.controller.load_dv(opcodes.twelve_valve_decoder, [_new_serial_number], device_type=opcodes.zone)
            self.controller.do_search_for_zones()

        # Assign the new bicoder and update the device serial number
        self.bicoder = self.controller.valve_bicoders[_new_serial_number]
        self.bicoder.ad = self.ad
        self.sn = self.bicoder.sn
        self.set_address()

        # Update all the objects attributes to stay consistent
        self.bicoder.self_test_and_update_object_attributes()

    #################################
    def verify_who_i_am(self, _expected_status=None, _expected_moisture_balance=None, include_messages=False,
                        _status_code=None):
        """

        TODO how do i verify status or where is it passed in from
        TODO need to add verify for moisture balance its a read only
        Verifier wrapper which verifies all attributes for this 'Zone'. \n
        :return:
        Get all information about the device from the controller.
        :param _expected_status
        :type _expected_status
        :param _expected_moisture_balance
        :type _expected_moisture_balance
        :param include_messages
        :type include_messages
        :param _status_code
        :type _status_code
        """

        self.get_data()

        # Verify base attributes
        self.verify_description()
        self.verify_latitude()
        self.verify_longitude()

        if _expected_status is not None:
            self.verify_status(_expected_status=_expected_status)

        # if the device is shared by a substation than do a self test and update object attributes
        # if self.is_shared_with_substation:
        #     self.self_test_and_update_object_attributes()

        # Verify Zone specific attributes
        self.verify_serial_number()
        self.verify_enabled_state()
        self.verify_design_flow()

        # For 1000 controller
        if self.controller.controller_type == "10":

            # Verify Zone specific attributes for a 1000
            self.verify_mirrored_zone()
            self.verify_master_valve_assigned()
            self.verify_high_flow_variance_val()

        # For 3200 controller
        elif self.controller.controller_type == "32":

            # Verify the mainline the zone it assigned to
            self.verify_device_address()
            self.verify_mainline_assignment()

        # For Water-Based Watering - If we set a value for 'ei', then verify all pertinent data
        if self.enable_et != opcodes.false:
            #self.verify_etc_value_on_cn() # not sure where this is at
            self.verify_crop_coefficient_value()
            self.verify_precipitation_rate_value()
            self.verify_distribution_uniformity_value()
            self.verify_root_zone_working_storage_capacity()
            # self.verify_moisture_balance()
