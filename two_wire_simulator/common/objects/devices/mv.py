from common.imports import opcodes
from decimal import Decimal
from common.objects.base_classes.devices import BaseDevices
from common.objects.messages.devices.master_valve_messages import MasterValvesMessages
from common.objects.statuses.devices.master_valve_statuses import MasterValveStatuses

from common.imports.types import ActionCommands,MasterValvesCommands
from common.objects.packets.devices.master_valve_packets import MasterValvePackets
__author__ = 'tige'


########################################################################################################################
# Master Valve
########################################################################################################################
class MasterValve(BaseDevices):
    """
    Master Valve Object \n
    """

    starting_lat = Decimal(BaseDevices.controller_lat)
    starting_long = Decimal(BaseDevices.controller_long) + Decimal('.000600')

    #################################
    def __init__(self, _controller, _valve_bicoder, _address, _shared_with_sb=False):
        """
        Initializer for a Master Valve object. \n

        :param _controller:     Controller this object is "attached" to \n
        :type _controller:      common.objects.controllers.bl_32.BaseStation3200 |
                                common.objects.controllers.bl_10.BaseStation1000 \n

        :param _valve_bicoder:  Bicoder of this device. \n
        :type _valve_bicoder:   common.objects.bicoders.valve_bicoder.ValveBicoder \n

        :param _address:    The address of this object on the controller \n
        :type _address:     int \n

        :param _shared_with_sb:
        :type _shared_with_sb:
        """
        # Give access to the controller
        self.controller = _controller

        # create message reference to object
        self.messages = MasterValvesMessages(_master_vales_object=self)  # master valve messages
        """:type: common.objects.messages.devices.master_valve_messages.MasterValveMessages"""

        # create packet reference to object
        self.packets = MasterValvePackets(self)
        """:type: common.objects.packets.devices.master_valve_packets.MasterValvePackets"""

        # Create a lat and long for the device that is offset to the controller
        lat = float((Decimal(str(self.starting_lat)) + (Decimal('0.000005') * _address)))
        lng = float((Decimal(str(self.starting_long)) + (Decimal('0.000005') * _address)))

        # Init parent class to inherit respective attributes
        BaseDevices.__init__(self, _ser=_controller.ser, _identifiers=[[opcodes.master_valve, _valve_bicoder.sn]],
                             _la=lat, _lg=lng, _sn=_valve_bicoder.sn, _ad=_address, _ty=opcodes.master_valve,
                             is_shared=_shared_with_sb)

        # Assign the bicoder to this device
        self.bicoder = _valve_bicoder

        # Set device type to inheriting class type (aka, set it to "MV" for Master Valve object)
        self.dv_type = opcodes.master_valve

        # Zone specific attributes for both controllers
        self.en = 'TR'            # Enabled / Disabled (TR | FA) default to TR
        self.no = 'FA'            # Normally Open (TR | FA) default to FA
        #moved to bicoder
        # self.vt = 0.0           # Two Wire Drop

        # 3200 Specific attributes
        self.bp = 'FA'            # Booster enable (TR | FA)

        # create status reference to object
        self.statuses = MasterValveStatuses(_master_valve_object=self)  # master valve Statuses
        """:type: common.objects.statuses.devices.master_valve_statuses.MasterValveStatuses"""

    #################################
    def set_default_values(self):
        """
        set the default values of the device on the controller
        :return:
        :rtype:
        """
        command = "{0},{1}={2},{3}={4},{5}={6},{7}={8},{9}={10},{11}={12}".format(
            ActionCommands.SET,                                 # {0}
            MasterValvesCommands.MasterValves,                  # {1}
            str(self.sn),                                       # {2}
            MasterValvesCommands.Attributes.ENABLED,            # {3}
            str(self.en),                                       # {4}
            MasterValvesCommands.Attributes.DESCRIPTION,        # {5}
            str(self.ds),                                       # {6}
            MasterValvesCommands.Attributes.LATITUDE,           # {7}
            str(self.la),                                       # {8}
            MasterValvesCommands.Attributes.LONGITUDE,          # {9}
            str(self.lg),                                       # {10}
            MasterValvesCommands.Attributes.NORMALLY_OPEN,      # {11}
            str(self.no)                                        # {12}
        )

        if self.controller.controller_type == "32":
            bp_string = ",{0}={1}".format(
                MasterValvesCommands.Attributes.BOOSTER,
                str(self.bp)
            )
            command += bp_string
        try:
            # Attempt to set event switch default values at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Master Valve {0} ({1})'s 'Default values' to: '{2}'".format(
                    self.sn, str(self.ad), command)
            raise Exception(e_msg)
        else:
            print("Successfully set Master Valve {0} ({1})'s 'Default values' to: {2}".format(
                  self.sn, str(self.ad), command))

    #################################
    def set_normally_open_state(self, _normally_open=None):
        """
        Sets the normally open attribute of Master Valve to true on the controller. \n
        
        :param _normally_open: Whether or not to make MV normally open.
        :type _normally_open: opcodes.true | opcodes.false
        :return:
        """
        # If a different normally open state specified
        if _normally_open is not None:

            # Verify valid states
            if _normally_open not in [opcodes.true, opcodes.false]:
                e_msg = "Invalid normally open state for Master Valve {0} ({1}). Received: {2}, expected: 'TR' or " \
                        "'FA'".format(str(self.sn), str(self.ad), str(_normally_open))
                raise ValueError(e_msg)
            else:
                self.no = _normally_open

        # If current master valve instance is not set to true or doesn't have a value, set the value for the object.
        # Command for sending
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,                             # {0}
            MasterValvesCommands.MasterValves,              # {1}
            self.sn,                                        # {2}
            MasterValvesCommands.Attributes.NORMALLY_OPEN,  # {3}
            self.no                                         # {4}
        )

        try:
            # Attempt to set master valve's normally open attribute at the controller
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Master Valve {0} ({1})'s normally open to {2}".format(
                self.sn, str(self.ad), str(_normally_open))
            raise Exception(e_msg)
        else:
            print("Successfully set Master Valve {0} ({1})'s 'Normally Open State' to: {2}".format(
                self.sn, str(self.ad), str(self.no)))

    #################################
    def set_booster_enabled(self):
        """
        Enables the Booster Master Valve on the controller. \n
        :return:
        """

        self.bp = opcodes.true

        # Command for sending
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,
            MasterValvesCommands.MasterValves,
            str(self.sn),
            MasterValvesCommands.Attributes.BOOSTER,
            self.bp)
        try:
            # Attempt to disable booster state
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Master Valve {0} ({1})'s 'Booster Pump State' to: {2}".format(
                self.sn, str(self.ad), str(self.bp))
            raise Exception(e_msg)
        else:
            print("Successfully set Master Valve {0} ({1})'s 'Booster Pump State' to: {2}".format(
                self.sn, str(self.ad), str(self.bp)))


    #################################
    def set_booster_disabled(self):
        """
        Disables the Booster Master Valve on the controller. \n
        :return:
        """

        self.bp = opcodes.false

        # Command for sending
        command = "{0},{1}={2},{3}={4}".format(
            ActionCommands.SET,
            MasterValvesCommands.MasterValves,
            str(self.sn),
            MasterValvesCommands.Attributes.BOOSTER,
            self.bp)
        try:
            # Attempt to disable booster state
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Master Valve {0} ({1})'s 'Booster Pump State' to: {2}".format(
                self.sn, str(self.ad), str(self.bp))
            raise Exception(e_msg)
        else:
            print("Successfully set Master Valve {0} ({1})'s 'Booster Pump State' to: {2}".format(
                self.sn, str(self.ad), str(self.bp)))

    #################################
    def verify_normally_open_state(self):
        """
        Verifies that the Master Valve's attribute normally open matches what is expected on the controller. \n
        :return:
        """
        # Get the normally open  state value from controller
        no_on_cn = self.data.get_value_string_by_key(MasterValvesCommands.Attributes.NORMALLY_OPEN)

        # Compare status versus what is on the controller
        if self.no != no_on_cn:
            e_msg = "Unable verify Master Valve {0} ({1})'s normally open state. Received: {2}, Expected: {3}".format(
                    self.sn,        # {0}
                    str(self.ad),   # {1}
                    no_on_cn,       # {2}
                    self.no)        # {3}
            raise ValueError(e_msg)
        else:
            print("Verified  Master Valve {0} ({1})'s normally open state on controller: {2}".format(
                  self.sn,          # {0}
                  str(self.ad),     # {1}
                  self.no           # {2}
                  ))

    #################################
    def verify_booster_enable_state(self):
        """
        Verifies that the booster enabled state on the controller. \n
        :return:
        """
        booster_enable_state = self.data.get_value_string_by_key(MasterValvesCommands.Attributes.BOOSTER)

        # Compare status versus what is on the controller
        if self.bp != booster_enable_state:
            e_msg = "Unable verify Master Valve {0} ({1})'s Booster pump enabled state. Received: {2}, Expected: " \
                    "{3}".format(
                        self.sn,                    # {0}
                        str(self.ad),               # {1}
                        str(booster_enable_state),  # {2}
                        str(self.bp)                # {3}
                    )
            raise ValueError(e_msg)
        else:
            print("Verified Master Valve {0} ({1})'s 'Booster Enabled State': {2} on controller".format(
                  self.sn,          # {0}
                  str(self.ad),     # {1}
                  str(self.bp)      # {2}
                  ))

    #################################
    def replace_single_valve_bicoder(self, _new_serial_number):
        """
        Replaces the bicoder with a new one with the specified new serial number. \n

        :param _new_serial_number:  The serial number that will be loaded onto the controller. \n
        :type _new_serial_number:   str \n
        """
        # Load the device on the controller, and in the process making the new bicoder object
        if _new_serial_number not in self.controller.valve_bicoders.keys():
            self.controller.load_dv(opcodes.single_valve_decoder, [_new_serial_number], device_type=opcodes.master_valve)
            self.controller.do_search_for_master_valves()

        # Assign the new bicoder and update the zones serial number
        self.bicoder = self.controller.valve_bicoders[_new_serial_number]
        self.sn = self.bicoder.sn
        self.identifiers[0][1] = _new_serial_number
        self.set_address()

        # Update all the objects attributes to stay consistent
        self.bicoder.self_test_and_update_object_attributes()

    #################################
    def replace_dual_valve_bicoder(self, _new_serial_number):
        """
        Replaces the bicoder with a new one with the specified new serial number. \n

        :param _new_serial_number:  The serial number that will be loaded onto the controller. \n
        :type _new_serial_number:   str \n
        """
        # Load the device on the controller, and in the process making the new bicoder object
        if _new_serial_number not in self.controller.valve_bicoders.keys():
            self.controller.load_dv(opcodes.two_valve_decoder, [_new_serial_number], device_type=opcodes.master_valve)
            self.controller.do_search_for_master_valves()

        # Assign the new bicoder and update the device serial number
        self.bicoder = self.controller.valve_bicoders[_new_serial_number]
        self.sn = self.bicoder.sn
        self.identifiers[0][1] = _new_serial_number
        self.set_address()

        # Update all the objects attributes to stay consistent
        self.bicoder.self_test_and_update_object_attributes()

    #################################
    def verify_who_i_am(self, _expected_status=None):
        """
        Verifier wrapper which verifies all attributes for this 'Flow Meter'. \n
        :return:
        """
        # Get all information about the device from the controller.
        self.get_data()

        # Verify base attributes
        self.verify_description()
        self.verify_serial_number()
        self.verify_latitude()
        self.verify_longitude()

        if _expected_status is not None:
            self.verify_status(_expected_status=_expected_status)

        # Verify Master Valve specific attributes
        self.verify_enabled_state()
        self.verify_normally_open_state()

        # 3200 controller specific attributes for Master Valves
        if self.controller.controller_type == "32":
            self.verify_booster_enable_state()

