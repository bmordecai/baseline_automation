# from common.imports import *
from common.imports import opcodes
from common.imports.types import ActionCommands,FlowMeterCommands
from decimal import Decimal
from common.objects.base_classes.devices import BaseDevices
# from common.objects.base_classes.messages import Messages
from common.objects.messages.devices.flow_meter_messages import FlowMeterMessages
from common.objects.statuses.devices.flow_meter_statuses import FlowMeterStatuses
import warnings
from common.objects.packets.devices.flow_meter_packets import FlowMeterPackets

__author__ = 'tige'


########################################################################################################################
# FLOW METER
########################################################################################################################
class FlowMeter(BaseDevices):
    """
    Flow Meter Object \n
    """
    starting_lat = Decimal(BaseDevices.controller_lat)
    starting_long = Decimal(BaseDevices.controller_long) + Decimal('.000400')

    #################################
    def __init__(self, _controller, _flow_bicoder, _address, _shared_with_sb=False):
        """
        Initializer for a Flow Meter object. \n

        :param _controller:     Controller this object is "attached" to \n
        :type _controller:      common.objects.controllers.bl_32.BaseStation3200 |
                                common.objects.controllers.bl_10.BaseStation1000 \n

        :param _flow_bicoder:   Bicoder of this device. \n
        :type _flow_bicoder:    common.objects.bicoders.flow_bicoder.FlowBicoder \n

        :param _address:        The address of this object on the controller \n
        :type _address:         int \n

        :param _shared_with_sb:
        :type _shared_with_sb:
        """
        # Give access to the controller
        self.controller = _controller
        # create message reference to object
        self.messages = FlowMeterMessages(_flow_meter_object=self)  # Flow Meter messages
        """:type: common.objects.messages.devices.flow_meter_messages.FlowMeterMessages"""

        self.packets = FlowMeterPackets(self)
        """:type: common.objects.packets.devices.flow_meter_packets.FlowMeterPackets"""

        # create a lat and long for the device that is offset to the controller
        lat = float((Decimal(str(self.starting_lat)) + (Decimal('0.000005') * _address)))
        lng = float((Decimal(str(self.starting_long)) + (Decimal('0.000005') * _address)))

        # Init parent class to inherit respective attributes
        BaseDevices.__init__(self, _ser=_controller.ser, _identifiers=[[opcodes.flow_meter, _flow_bicoder.sn]],
                             _la=lat, _lg=lng, _sn=_flow_bicoder.sn, _ad=_address, _ty=opcodes.flow_meter,
                             is_shared=_shared_with_sb)

        # Assign the bicoder to this device
        self.bicoder = _flow_bicoder
        # Give FM BiCoder a reference back to it's FlowMeter object so that we can use the 3200's serial connection to
        # set the "k-value" through the bicoder. K-Value can only be set through the 3200 and not substation.
        self.bicoder.fm_on_cn = self

        self.dv_type = opcodes.flow_meter

        # create status reference to object
        self.statuses = FlowMeterStatuses(_flow_meter_object=self)  # Flow Meter statuses
        """:type: common.objects.statuses.devices.flow_meter_statuses.FlowMeterStatuses"""

    #################################
    def set_default_values(self):
        """
        set the default values of the device on the controller
        :return:
        :rtype:
        """
        command = "{0},{1}={2},{3}={4},{5}={6},{7}={8},{9}={10},{11}={12}".format(
            ActionCommands.SET,                         # {0}
            FlowMeterCommands.Flow_Meter,               # {1}
            str(self.sn),                               # {2}
            FlowMeterCommands.Attributes.ENABLED,       # {3}
            str(self.en),                               # {4}
            FlowMeterCommands.Attributes.DESCRIPTION,   # {5}
            str(self.ds),                               # {6}
            FlowMeterCommands.Attributes.LATITUDE,      # {7}
            str(self.la),                               # {8}
            FlowMeterCommands.Attributes.LONGITUDE,     # {9}
            str(self.lg),                               # {10}
            FlowMeterCommands.Attributes.K_VALUE,       # {11}
            str(self.bicoder.kv),                       # {12}
        )

        try:
            # Attempt to set flow meter default values at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Flow Meter {0} ({1})'s 'Default values' to: '{2}'".format(
                    self.sn, str(self.ad), command)
            raise Exception(e_msg)
        else:
            print("Successfully set Flow Meter {0} ({1})'s 'Default values' to: {2}".format(
                  self.sn, str(self.ad), command))

    #################################
    def set_k_value(self, _value=None):
        """
        Set the k_value for the Flow Meter (Faux IO Only) \n
        :param _value:        Value to set the Flow Meter k value as a float \n
        :return:
        """
        warnings.warn('moved to flow bicoder Class', DeprecationWarning)
        pass
        # # if is None, use current value
        # if _value is not None:
        #
        #     # Verifies the k_value passed in is an float value
        #     if not isinstance(_value, float):
        #         e_msg = "Failed trying to set FM {0} ({1})'s k value. Invalid argument type, expected a float, " \
        #                 "received: {2}".format(self.sn, str(self.ad), type(_value))
        #         raise TypeError(e_msg)
        #     else:
        #         self.kv = _value
        #
        # # Command for sending
        # command = "{0},{1}={2},{3}={4}".format(
        #     ActionCommands.SET,  # {0}
        #     FlowMeterCommands.Flow_Meter,  # {1}
        #     self.sn,  # {2}
        #     FlowMeterCommands.Attributes.K_VALUE,  # {3}
        #     str(self.kv)  # {4}
        # )
        #
        # try:
        #     # Attempt to set k_value for Flow Meter at the controller in faux io
        #     self.ser.send_and_wait_for_reply(tosend=command)
        # except Exception:
        #     e_msg = "Exception occurred trying to set Flow Meter {0} ({1})'s 'K Value' to: '{2}'".format(
        #         self.sn, str(self.ad), str(self.kv))
        #     raise Exception(e_msg)
        # else:
        #     print("Successfully set Flow Meter {0} ({1})'s 'K Value' to: {2}".format(
        #         self.sn, str(self.ad), str(self.kv)))

    #################################
    def verify_k_value(self):
        """
        Verifies k value for this Flow Meter on the Controller. Expects the controller's value
        and this instance's value to be equal.
        :return:
        """
        warnings.warn('moved to flow bicoder Class', DeprecationWarning)
        pass
        # k_value = float(self.data.get_value_string_by_key(opcodes.k_value))
        #
        # # Compare status versus what is on the controller
        # if self.kv != k_value:
        #     e_msg = "Unable verify Flow Meter {0} ({1})'s k_value. Received: {2}, Expected: {3}".format(
        #         self.sn,  # {0}
        #         str(self.ad),  # {1}
        #         str(k_value),  # {2}
        #         str(self.kv)  # {3}
        #     )
        #     raise ValueError(e_msg)
        # else:
        #     print("Verified Flow Meter {0} ({1})'s k_value: '{2}' on controller".format(
        #         self.sn,  # {0}
        #         str(self.ad),  # {1}
        #         str(self.kv)  # {2}
        #     ))

    #################################
    def replace_flow_bicoder(self, _new_serial_number):
        """
        Replaces the bicoder with a new one with the specified new serial number. \n

        :param _new_serial_number:  The serial number that will be loaded onto the controller. \n
        :type _new_serial_number:   str \n
        """
        # Load the device on the controller, and in the process making the new bicoder object
        if _new_serial_number not in self.controller.flow_bicoders.keys():
            self.controller.load_dv(opcodes.flow_meter, [_new_serial_number])
            self.controller.do_search_for_flow_meters()

        # Assign the new bicoder and update the device serial number
        self.bicoder = self.controller.flow_bicoders[_new_serial_number]
        self.sn = self.bicoder.sn
        self.identifiers[0][1] = _new_serial_number
        self.set_address()

        # Update all the objects attributes to stay consistent
        self.bicoder.self_test_and_update_object_attributes()

    #################################
    def verify_who_i_am(self, _expected_status=None):
        """
        Verifier wrapper which verifies all attributes for this 'Flow Meter'. \n
        :return:
        """
        # Get all information about the device from the controller.
        self.get_data()

        # Verify base attributes
        self.verify_enabled_state()
        self.verify_description()
        self.verify_serial_number()
        self.verify_latitude()
        self.verify_longitude()

        if _expected_status is not None:
            self.verify_status(_expected_status=_expected_status)

        # Verify Flow Meter specific attributes
        self.verify_k_value()

