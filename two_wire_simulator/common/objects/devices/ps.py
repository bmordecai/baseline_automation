from decimal import Decimal
from common.imports import opcodes
from common.objects.base_classes.devices import BaseDevices
from common.objects.bicoders.analog_bicoder import AnalogBicoder
from common.objects.messages.devices.pressure_sensor_messages import PressureSensorMessages
from common.objects.statuses.devices.pressure_sensor_statuses import PressureSensorStatuses


__author__ = 'Tige'


########################################################################################################################
# Pressure Sensor
########################################################################################################################
class PressureSensor(BaseDevices, AnalogBicoder):
    """
    Pressure Sensor Object \n
    """

    starting_lat = Decimal(str(BaseDevices.controller_lat))
    starting_long = Decimal(str(BaseDevices.controller_long)) + Decimal('.000300')

    #################################
    def __init__(self, _controller, _analog_bicoder, _address, _shared_with_sb=False):
        """
        Initializer for a Pressure Sensor object. \n

        :param _controller:     Controller this object is "attached" to \n
        :type _controller:      common.objects.controllers.bl_32.BaseStation3200 |
                                common.objects.controllers.bl_10.BaseStation1000 \n

        :param _analog_bicoder: Bicoder of this device. \n
        :type _analog_bicoder:  common.objects.bicoders.analog_bicoder.AnalogBicoder \n

        :param _address:        The address of this object on the controller \n
        :type _address:         int \n

        :param _shared_with_sb:
        :type _shared_with_sb:
        """
        # Give access to the controller
        self.controller = _controller

        # create message reference to object
        self.messages = PressureSensorMessages(_pressure_sensor_object=self)  # Pressure Sensor messages

        # create a lat and long for the device that is offset to the controller
        lat = float((Decimal(str(self.starting_lat)) + (Decimal('0.000005') * _address)))
        lng = float((Decimal(str(self.starting_long)) + (Decimal('0.000005') * _address)))

        # Init parent class to inherit respective attributes
        BaseDevices.__init__(self, _ser=_controller.ser, _identifiers=[[opcodes.analog_decoder, _analog_bicoder.sn]],
                             _la=lat, _lg=lng, _sn=_analog_bicoder.sn, _ad=_address, _ty=opcodes.pressure_sensor,
                             is_shared=_shared_with_sb)

        # Assign the bicoder to this device
        self.bicoder = _analog_bicoder

        # Set device type to inheriting class type (aka, set it to "PS" for Pressure object)
        self.dv_type = opcodes.pressure_sensor

        # Pressure Sensor specific attributes for both controllers
        self.en = opcodes.true             # Enabled / Disabled (TR | FA)
        self.vr = 0.00  # {reading converted to units} read only
        self.vu = 'PSI'  # {sensor value user units:  PSI or GPM, default PSI}

        # self.cn_firmware_version = _cn_fm_version  # this is needed when a controller has version 16.0 or greater

        # create status reference to object
        self.statuses = PressureSensorStatuses(_pressure_sensor_object=self)  # Pressure Sensor Statuses
        """:type: common.objects.statuses.devices.pressure_sensor_statuses.PressureSensorStatuses"""

    #################################
    def set_default_values(self):
        """
        set the default values of the device on the controller
        :return:
        :rtype:
        """
        command = "SET,{0}={1},{2}={3},{4}={5},{6}={7},{8}={9},{10}={11},{12}={13},{14}={15},{16}={17}"\
            .format(
                    opcodes.analog_decoder,                 # {0}
                    str(self.sn),                           # {1}
                    opcodes.enabled,                        # {2}
                    str(self.en),                           # {3}
                    opcodes.description,                    # {4}
                    str(self.ds),                           # {5}
                    opcodes.latitude,                       # {6}
                    str(self.la),                           # {7}
                    opcodes.longitude,                      # {8}
                    str(self.lg),                           # {9}
                    opcodes.low_milli_amp_setting,          # {10}
                    str(self.bicoder.vl),                   # {11}
                    opcodes.high_milli_amp_setting,         # {12}
                    str(self.bicoder.vh),                   # {13}
                    opcodes.raw_analog_readings,            # {14}
                    str(self.bicoder.va),                   # {15}
                    opcodes.analog_units,                   # {16}
                    str(self.vu)                            # {17}
            )

        try:
            # Attempt to set Pressure Sensor default values at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Pressure Sensor {0}'s 'Default values' to: '{1}'".format(
                str(self.ad),   # {0}
                command         # {1}
            )
            raise Exception(e_msg)
        else:
            print("Successfully set Pressure Sensor {0}'s 'Default values' to: {1}".format(
                str(self.ad),   # {0}
                command)        # {1}
            )

    #################################
    def replace_analog_bicoder(self, _new_serial_number):
        """
        Replaces the bicoder with a new one with the specified new serial number. \n

        :param _new_serial_number:  The serial number that will be loaded onto the controller. \n
        :type _new_serial_number:   str \n
        """
        # Load the device on the controller, and in the process making the new bicoder object
        if _new_serial_number not in self.controller.analog_bicoders.keys():
            self.controller.load_dv(opcodes.analog_decoder, [_new_serial_number])
            self.controller.do_search_for_analog_bicoders()

        # Assign the new bicoder and update the device serial number
        self.bicoder = self.controller.analog_bicoders[_new_serial_number]
        self.sn = self.bicoder.sn
        self.identifiers[0][1] = _new_serial_number
        self.set_address()

        # Update all the objects attributes to stay consistent
        self.bicoder.self_test_and_update_object_attributes()

    #################################
    def verify_who_i_am(self, _expected_status=None, include_messages=False, _status_code=None):
        """

        TODO how do i verify status or where is it passed in from
        TODO need to add verify for moisture balance its a read only
        Verifier wrapper which verifies all attributes for this 'Analog Decoder'. \n
        :return:
        Get all information about the device from the controller.
        :param _expected_status
        :type _expected_status
        :param include_messages
        :type include_messages
        :param _status_code
        :type _status_code
        """

        self.get_data()

        # Verify base attributes
        self.verify_description()
        self.verify_latitude()
        self.verify_longitude()
        if _expected_status is not None:
            self.verify_status(_expected_status=_expected_status)
        # if the device is shared by a substation than do a self test and update object attributes

        self.verify_serial_number()
        self.verify_enabled_state()

        if include_messages:
            self.verify_message(_status_code)

        return True
