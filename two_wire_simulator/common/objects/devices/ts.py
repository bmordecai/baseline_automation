from decimal import Decimal
from common.imports import opcodes
from common.imports.types import ActionCommands, TemperatureSensorCommands
from common.objects.base_classes.devices import BaseDevices
from common.objects.messages.devices.temperature_sensor_messages import TemperatureSensorMessages
from common.objects.statuses.devices.temperature_sensor_statuses import TemperatureSensorStatuses
__author__ = 'tige'


########################################################################################################################
# TEMPERATURE SENSOR
########################################################################################################################
class TemperatureSensor(BaseDevices):
    """
    Temperature Sensor Object \n
    """
    starting_lat = Decimal(BaseDevices.controller_lat)
    starting_long = Decimal(BaseDevices.controller_long) + Decimal('.000800')

    #################################
    def __init__(self, _controller, _temp_bicoder, _address, _shared_with_sb=False):
        """
        Initializer for a Temperature Sensor object. \n

        :param _controller:     Controller this object is "attached" to \n
        :type _controller:      common.objects.controllers.bl_32.BaseStation3200 |
                                common.objects.controllers.bl_10.BaseStation1000 \n

        :param _temp_bicoder:   Bicoder of this device. \n
        :type _temp_bicoder:    common.objects.bicoders.temp_bicoder.TempBicoder \n

        :param _address:        The address of this object on the controller \n
        :type _address:         int \n

        :param _shared_with_sb:
        :type _shared_with_sb:
        """
        # Give access to the controller
        self.controller = _controller
        # create message reference to object
        self.messages = TemperatureSensorMessages(_temperature_sensor_object=self)  # Temperature Sensor messages
        """:type: common.objects.messages.devices.temperature_sensor_messages.TemperatureSensorMessages"""

        # create a lat and long for the device that is offset to the controller
        lat = float((Decimal(str(self.starting_lat)) + (Decimal('0.000005') * _address)))
        lng = float((Decimal(str(self.starting_long)) + (Decimal('0.000005') * _address)))

        # Init parent class to inherit respective attributes
        BaseDevices.__init__(self, _ser=_controller.ser, _identifiers=[[opcodes.temperature_sensor, _temp_bicoder.sn]],
                             _la=lat, _lg=lng, _sn=_temp_bicoder.sn, _ad=_address, _ty=opcodes.temperature_sensor,
                             is_shared=_shared_with_sb)

        # Assign the bicoder to this device
        self.bicoder = _temp_bicoder

        self.dv_type = opcodes.temperature_sensor
        # Temperature Sensor specific attributes
        self.vd = 93.1       # Temperature
        self.vt = 1.7       # Two Wire Drop

        # create status reference to object
        self.statuses = TemperatureSensorStatuses(_temperature_sensor_object=self)  # Temperature Sensor Statuses
        """:type: common.objects.statuses.devices.temperature_sensor_statuses.TemperatureSensorStatuses"""

    #################################
    def set_default_values(self):
        """
        set the default values of the device on the controller
        :return:
        :rtype:
        """
        command = "{0}{1},{2}={3},{4}={5},{6}={7}".format(
            ActionCommands.SET,                                 # {0}
            self.get_id(),                                      # {1}
            TemperatureSensorCommands.Attributes.DESCRIPTION,   # {2}
            self.ds,                                            # {3}
            TemperatureSensorCommands.Attributes.LATITUDE,      # {4}
            self.la,                                            # {5}
            TemperatureSensorCommands.Attributes.LONGITUDE,     # {6}
            self.lg                                             # {7}
        )

        try:
            # Attempt to set temperature default values at the controller in faux io
            self.ser.send_and_wait_for_reply(tosend=command)
        except Exception:
            e_msg = "Exception occurred trying to set Temperature Sensor {0} {1}'s 'Default values' to: '{2}'".format(
                    self.sn, str(self.ad), command)
            raise Exception(e_msg)
        else:
            print("Successfully set Temperature Sensor {0} ({1})'s 'Default values' to: {2}".format(
                  self.sn, str(self.ad), command))

    #################################
    def replace_temperature_bicoder(self, _new_serial_number):
        """
        Replaces the bicoder with a new one with the specified new serial number. \n

        :param _new_serial_number:  The serial number that will be loaded onto the controller. \n
        :type _new_serial_number:   str \n
        """
        # Load the device on the controller, and in the process making the new bicoder object
        if _new_serial_number not in self.controller.temperature_bicoders.keys():
            self.controller.load_dv(opcodes.temperature_sensor, [_new_serial_number])
            self.controller.do_search_for_temperature_sensor()

        # Assign the new bicoder and update the device serial number
        self.bicoder = self.controller.temperature_bicoders[_new_serial_number]
        self.sn = self.bicoder.sn
        self.identifiers[0][1] = _new_serial_number
        self.set_address()

        # Update all the objects attributes to stay consistent
        self.bicoder.self_test_and_update_object_attributes()

    #################################
    def verify_who_i_am(self, _expected_status=None):
        """
        Verifier wrapper which verifies all attributes for this 'Flow Meter'. \n
        :return:
        """
        # Get all information about the device from the controller.
        self.get_data()

        # Verify base attributes
        self.verify_description()
        self.verify_serial_number()
        self.verify_latitude()
        self.verify_longitude()

        if _expected_status is not None:
            self.verify_status(_expected_status=_expected_status)

