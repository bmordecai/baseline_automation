__author__ = 'Tige'

from common.epa_package.ir_asa_resources import dict_of_allowable_surface_accumulation, \
    dict_of_basic_soil_intake_rate, \
    dict_of_basic_soil_available_water



# Irrigation_System_Type = "IT"
# :param _it:             Irrigation System Types Type (ie = IrrigationSystemTypes.spray_heads from IrrigationSystemTypes class)
# :type  _it:             str \n
# Soil_Type = "SL"
# :param _sl:           Soil Type (ie = SoilTextureTypes.clay from SoilTextureTypes class)
# :type  _sl:           str \n
#
# Slope = "SP"
# :param _sp:            Slop (ie = 0.02)
# :type  _sp:           float \n comes from a percentage
#
# Soil_Intake_Rate = "IR"
# :param _ir:           Soil Intake Rate (this is based of soil texture) (ie =SoilTextureTypes.clay)
# :type  _ir:           float \n
#
# Reference_Crop_Evapotranspiration = "ETo"
# :param _eto:
# :type  _eto:          float \n
#
# Crop_Coefficient = "Kl"
# :param _kc:           Crop Coefficient
# :type  _kc:           float \n
#
# Species_Factor = "Ks"
# :param _ks:           Species Factor
# :type  _ks:           float \n
#
# Allowable_Depletion = "AD"
# :param _ad:           Allowable Depletion
# :type  _ad:           float \n
#
# Available_water = "AW"
# :param _aw:           Available water
# :type  _aw:           float \n
#
# Root_Depth = "RD"
# :param _rd:           Root Depth
# :type  _rd:           float \n
#
# Density_Factor = "Kd"
# :param _kd:           Density Factor
# :type  _kd:           float \n
#
# Microclimate_factor = "Kmc"
# :param _kmc:          Microclimate factor (shade index)
# :type  _kmc:          float \n
#
# Landscape_Coefficient = "Kl"
# :param _kl:           Landscape Coefficient (_ks x _kd x _kmc)
# :type  _kl:           float \n
#
# Turf_Evapotranspiration = "ETc"
# Landscape_Evapotranspiration = "ETc"
# :param _etc:          _eto x _kc \n or _eto x _kl \n both work
# :type  _etc:          float \n
#
# Precipitation_Rate = "PR"
# :param _pr:           Precipitation Rate \n
# :type  _pr:           float \n
#
# Allowable_surface_accumulation = "ASA"
# :param _asa:          Allowable_surface_accumulation
# :type  _asa:          float \n
#
# Distribution_uniformity = "DU", "E"
# :param _du:           Distribution uniformity or E = irrigation system application efficiency
# :type  _du:           float \n
#
# Runtime_Total = "RT"
# :param _rt:           Runtime Total((da(60))/PR) This is the total runtime in minutes
# :type  _rt:           float \n
#
# Soak_Time = "ST"
# :param _st:           Soak Time (_da/_ir)
# :type  _st:           float \n
#
# Max_Runtime_Before_Run_Off = "CT"
# Cycle_Time = "CT"
# :param _ct:           Max Runtime before run off or cycle time ((ASA)/(_pr - _ir)) == cycle time in minutes
# :type  _ct:           float \n
#
# Gross_Irrigation_Amount ="I"
# :param _i:            Gross Irrigation Amount ((_ct x _pr) / 60) in inches
# :type  _i:            float \n
#
# Net_Irrigation_Calculation = "DA"
# :param _da:           Net Irrigation Calculation (_i x _du) in inches
# :type  _da:           float \n
#
# Rain_Fall = "RA"
# :param _ra:           Rain Fall
# :type  _ra:           float \n
#
# Rain_Fall_Loss_of_20% = "RN"
# :param _rn:           Allows for an arbitrary loss of 20% of the rainfall to nonuniformity and runoff (RN = 0.8 (R)) in inches
# :type  _rn:           float \n
#
# Free_Water_Calculation = "FW"
# :param _fw:           Free water calculation FW = ((Rt/60)(PR - IR)) returns in inches
# :type  _fw:           float \n
#
# Root_Zone_Working_Water_Storage = "RZ"
# :param _rz:           Root Zone Working Water Storage
# :type  _rs:           float \n
#
# Yesterdays_Daily_Moisture_Balance = "MBo"
# :param _mbo:          Yesterdays Daily moisture Balance
# :type  _mbo:          float \n
#
# Daily_Moisture_Balance = "MB"
# :param _mb:           Daily moisture Balance (MB = MBo + I* (E) + 0.8 (R**) - ETc) return in inches.
# :type  _mb:           float \n
#
# Definition_of_Deficit = "D"
# :param _d:            Definition of Deficit (_d = sum of _mb < o) return in inches
# :type  _d:            float \n
#
# Definition_of_Surplus = "S"
# :param _s:            Definition of Surplus (_s  = Sum of MB > RZWWS) return in inches
# :type  _s:            float \n
#
# Gross Area = "GA"
# :param _ga:            Gross Area return in square feet
# :type  _ga:            float \n

# this is set in the PG3200 object
# set new description
# script
# define precipitation rate
# define soil type
# define slop

# TODO need to add a date to crop coefficient
# TODO need to Calculate RZWWS Uses an (Allowable depletion, root zone depth, available water ( asa Table)
# TODO need a setter for the RZWWS
# TODO need a setter for soak time from the soak time wrapper
# TODO need a setter for cycle time from the soak time wrapper
# TODO need a setter for enable ET for a program
# TODO use sun exposure and crop coefficient in a look up table


# -------------------------------------------------------------------------------------------------------------------- #
# Start of Wrappers                                                                                                    #
# -------------------------------------------------------------------------------------------------------------------- #
def return_calculated_soak_time(_sl, _pr, _sp, _du):
    """
    - Soak Time Calculation Wrapper
        -   Rt  = 60(_asa)/(_pr - _ir)
        -   I   = Rt(_pr)/60
        -   Da  = I(_e)
        -   St  = Da(60)/_ir

    - Notes:
        -   _asa and _ir are determined by soil types, these params can be replaced with just a single param for soil ty
        -   _pr comes from the sprinkler head type, so eventually this could be a sprinkler head type enum, but for now
            it will be a float value passed in from "test script"

    :param _sl:    Soil Type (ie = SoilTextureTypes.clay from SoilTextureTypes class)\n
    :type  _sl:    str

    :param _sp:    Slope (ie = 0.02) comes from a percentage
    :type  _sp:    float

    :param _pr:    Precipitation Rate \n
    :type  _pr:    float

    :param _du:    Irrigation System Application Efficiency
    :type _du:     float

    :return:        Calculated Soak Time
    :rtype:         float
    """
    _du_percent = _du * .01
    cal_ct = calculate_cycle_time(_pr=_pr, _sl=_sl, _sp=_sp)
    cal_i = calculate_gross_irrigation_amount(_ct=cal_ct, _pr=_pr)
    cal_da = calculate_net_irrigation(_i=cal_i, _du=_du_percent)
    _st = 60*(calculate_soak_time(_sl=_sl, _da=cal_da))
    return _st  # soak time


# TODO: This method might go away
def calculate_soak_time(_sl, _da):
    """
    - Soak Time
        - Required minimum time between the start of the consecutive irrigation cycles

    - Equation:
        - _st = (_da * 60.0) / _ir
    :param   _sl:     Soil Type (ie = SoilTextureTypes.clay from SoilTextureTypes class) \n
    :type    _sl:     str

    :param   _da:     Net Irrigation Calculation (_i x _du) in inches \n
    :type    _da:     float

    :return:          Soak Time AS A FLOAT NOT ROUNDED (_da/_ir) \n
    :rtype:           float
    """
    cal_ir = dict_of_basic_soil_intake_rate[_sl]
    _st = (_da * 60) / cal_ir
    return _st  # soak time


# -------------------------------------------------------------------------------------------------------------------- #
# Start of Equations                                                                                                   #
# -------------------------------------------------------------------------------------------------------------------- #
def calculate_landscape_coefficient(_ks, _kd, _kmc):
    """
    - Landscape Coefficient

    - Equation:
        - _kl = _ks x _kd x _kmc

    :param _ks:     Species Factor
    :type  _ks:     float

    :param _kd:     Density Factor
    :type  _kd:     float

    :param _kmc:    Microclimate factor (shade index)
    :type  _kmc:    float

    :return:        Landscape Coefficient (_ks x _kd x _kmc)
    :rtype:         float
    """
    _kl = _ks * _kd * _kmc
    return _kl  # Landscape Coefficient


# TODO:
#   This method currently isn't acting as a 'wrapper', it is just passing arguments straight to another method.
def return_calculated_cycle_time(_sl, _pr, _sp):
    """
    - Run Time Max (aka: cycle time)
        - maximum allowable run time to avoid runoff

    - Equation:
        - _ct = (60 * ASA) / (_pr - _ir)

    :param  _sl:    Soil Type (ie = SoilTextureTypes.clay from SoilTextureTypes class) \n
    :type   _sl:    str

    :param  _pr:    Precipitation Rate \n
    :type   _pr:    float

    :param  _sp:    Slope - Percentage (ie = 0.02) \n
    :type   _sp:    float

    :return:        Max Runtime before run off or cycle time ((ASA)/(_pr - _ir)) == cycle time in minutes \n
    :rtype:         float
    """
    _ct = 60*(calculate_cycle_time(_sl=_sl, _pr=_pr, _sp=_sp))
    return _ct  # cycle time


def calculate_cycle_time(_sl, _pr, _sp):
    """
    - Run Time Max (aka: cycle time)
        - maximum allowable run time to avoid runoff

    - Equation:
        - _ct = (60 * ASA) / (_pr - _ir)

    :param  _sl:    Soil Type (ie = SoilTextureTypes.clay from SoilTextureTypes class) \n
    :type   _sl:    str

    :param  _pr:    Precipitation Rate \n
    :type   _pr:    float

    :param  _sp:    Slope - Percentage (ie = 0.02) \n
    :type   _sp:    float

    :return:        Max Runtime before run off or cycle time ((ASA)/(_pr - _ir)) == cycle time in minutes \n
    :rtype:         float
    """
    cal_asa = dict_of_allowable_surface_accumulation[_sl][_sp]
    cal_ir = dict_of_basic_soil_intake_rate[_sl]

    # Below was added because of a divide by zero exception where _pr and cal_ir were both 0.2. I believe this could
    # have been why the script wasn't working.
    # What I don't know is if it is fair to assume that if there is no difference then to return a cycle time of 0..?

    # need watering?
    difference = _pr - cal_ir
    if difference != 0:
        _ct = (60.0 * cal_asa) / (_pr - cal_ir)
    else:
        _ct = 0
    return _ct  # cycle time


def calculate_gross_irrigation_amount(_ct, _pr):
    """
    - Gross irrigation water applied

    - Equation:
        - _i = (_ct * _pr) / 60

    :param  _ct:    Max Runtime before run off or cycle time ((ASA)/(_pr - _ir)) == cycle time in minutes \n
    :type   _ct:    float

    :param  _pr:    Precipitation Rate \n
    :type:  _pr:    float

    :return:        Gross Irrigation Amount ((_ct x _pr) / 60) in inches \n
    :rtype:         float
    """
    _i = (_ct * _pr) / 60.0
    return _i  # gross irrigation amount


def calculate_net_irrigation(_i, _du):
    """
    - Net Irrigation (da)

    - Equation:
        - _da = (_i * _du)

    :param _i:   Gross Irrigation Amount ((_ct x _pr) / 60) in inches \n
    :type _i:    float

    :param _du:  Distribution uniformity or E = irrigation system application efficiency \n
    :type _du:   float

    :return:     Net Irrigation Calculation (_i x _du) in inches \n
    :rtype:      float
    """
    _da = (_i * _du)
    return _da  # Net irrigation in inches


# Todo we need a wrapper in the devices class to set the etc for a zone  this zone.calculate_my_etc
def calculate_etc(_kc, _eto):
    """
    Solve Turf evapotranspiration _etc = (_kc*_eto)

    :param    _kc:   Crop Coefficient \n
    :type     _kc:   float

    :param   _eto:   Crop Evapotranspiration
    :type    _eto:   float

    :return:         Turf evapotranspiration(_kc*_eto)\n
    :rtype:          float
    """
    _etc = (_kc * _eto)
    return _etc     # turf evapotranspiration


def calculate_free_water_(_ct, _pr, _ir):
    """
    - Free Water:
        - Water applied that exceeds soil intake properties (inches)

    - EQUATION FW = (_ct / 60) * (_pr - _ir)

    :param  _ct:    Max Runtime before run off or cycle time ((ASA)/(_pr - _ir)) == cycle time in minutes \n
    :type   _ct:    float

    :param  _pr:    Precipitation Rate \n
    :type:  _pr:    float

    :param  _ir:    Soil Intake Rate (this is based of soil texture) (ie =SoilTextureTypes.clay)
    :type   _ir:    float

    :return:        Free water calculation FW = ((Rt/60)(PR - IR)) returns in inches
    :rtype:         float
    """
    _fw = ((_ct / 60) * _pr - _ir)
    return _fw  # free water (in inches)


def calculate_rain_fall(_ra):
    """
    - Rain Fall
        - net amount of daily rainfall to be used in moisture balance calculation

    - Equation:
        - _rn = 0.8 * _ra

    :param  _ra:    Gross amount of daily rain fall as reported (in inches) \n
    :type   _ra:    float

    :return:        Allows for an arbitrary loss of 20% of the rainfall to nonuniformity and runoff \n
    :rtype:         float
    """
    _rn = 0.8 *_ra
    return _rn  # net rain fall in inches as float


def calculate_definition_of_deficit(_mb):
    """
    - Deficit
        - Deficit crop consumptive use not satisfied by moisture from rainfall or storage (in inches)

    - Equation
        - _d = sum of _mb < 0 returns in inches

    :param _mb:     Moisture Balance Calculation (yesterdays MB value)
    :type  _mn:     float

    :return:        Deficit in inches \n
    :rtype:         float
    """
    # TODO: Need to figure out what values to return if _mb < 0 and if _mb > 0
    # TODO: Ben Skipped
    # _d = _mb < 0 #returned in inches
    # return _d
    pass


def return_calculate_root_zone_working_water_storage(_sl, _ad, _rd):
    """
    - Root Zone Working Water Storage
        - Maximum amount of moisture that can effectively be stored in the root zone (in inches)

    - Equation:
        - _rzwws = (_rd * _aw) *_ad

    :param _sl:    Soil Type (ie = SoilTextureTypes.clay from SoilTextureTypes class)
    :type  _sl:    str \n

    :param _ad:    Allowable Depletion
    :type  _ad:    float

    :param _aw:    Available water comes from the soil intake rate table
    :type  _aw:    float

    :param _rd:    Root Depth
    :type  _rd:    float

    :return:       Root Zone Working Water Storage
    :rtype:        float# return in inches
    """
    get_aw = dict_of_basic_soil_available_water[_sl]
    _rz = calculate_root_zone_working_water_storage(_ad=_ad, _aw=get_aw, _rd=_rd)
    return _rz   # root zone working water storage (in inches as float)


def calculate_root_zone_working_water_storage(_ad, _aw, _rd):
    """
    - Root Zone Working Water Storage
        - Maximum amount of moisture that can effectively be stored in the root zone (in inches)

    - Equation:
        - _rzwws = (_rd * _aw) *_ad

    :param _ad:    Allowable Depletion
    :type  _ad:    float

    :param _aw:    Available water comes from the soil intake rate table
    :type  _aw:    float

    :param _rd:    Root Depth
    :type  _rd:    float

    :return:       Root Zone Working Water Storage
    :rtype:        float
    """
    _rz = (_ad * _aw) * _rd
    return _rz   # root zone working water storage (in inches as float)


def calculate_definition_of_surplus(_mb, _rzwws):
    """
    Definition of Surplus _s  = Sum of MB > RZWWS returns in inches

    :param _mb:     Moisture Balance Calculation
    :type  _mn:     float \n

    :param _rzwws:  Root Zone Working Water Storage
    :type  _rzwws:  float \n

    :return:        Definition of Surplus (_s  = Sum of MB > RZWWS) return in inches
    :rtype:         float \n
    """
    # TODO: Need to figure out what values to return if sum < rzwws and if sum > rzwws
    # TODO: Ben Skipped
    # _s = Sum of MB > RZWWS #returned in inches
    # return _s
    pass


def return_todays_calculated_moisture_balance(_yesterdays_mb, _yesterdays_rt, _pr, _du, _ra, _etc):
    """
    - Daily Moisture Balance
        - Daily calculation of root zone moisture balance (in inches)

    - Equation:
        - _mb = _yesterdays_mb + ((_yesterdays_rt * _pr / 60 / 60 * _du) / 100) - yesterdays_rn - yesterdays_etc

    :param _yesterdays_mb:  Yesterday's Moisture Balance
    :type _yesterdays_mb:   float

    :param _yesterdays_rt:  Yesterday's Total Runtime (in minutes)
    :type _yesterdays_rt    float

    :param _pr:             Precipitation rate = inches per hour
    :type _pr:              float

    :param _du:             Distribution uniformity or E = irrigation system application efficiency
    :type _du:              float

    :param _yesterdays_rn:  Yesterdays Net Amount of daily rainfall to be used in moisture balance calculation. Allows
                            for an arbitrary loss of 20% of the rainfall to nonuniformity and runoff in inches
    :type _yesterdays_rn:   float

    :param _yesterdays_etc: yesterday's Turf/Landscape Moisture Requirements: _eto x _kc or _eto x _kl, both work
    :type _yesterdays_etc:  float

    :return:                Today's daily moisture balance (MB = y_MBo + ((y_rt * du)/100) - y_rn - y_ETc) in inches.
    :rtype:                 float
    """
    #TODO need to check comments and remove other calculate moisture balance
    # calculate the effective rainfall = 80%
    get_rn = calculate_rain_fall(_ra=_ra)
    # only use run times of four minutes or greater
    if _yesterdays_rt < 240:
        _yesterdays_rt = 0

    _mb = _yesterdays_mb + _yesterdays_rt * (_pr /60.0/60.0) *(_du /100.0) + get_rn - _etc

    if _mb > 0:
        _mb = 0
        print ("MB exceeded limit = " + str(_mb))

    return _mb  # today's moisture balance in inches as float
    #this is current moisture balance * _du / 100


def return_todays_calculated_runtime(_du, _pr, _mb,):
    """
    - Today's Runtime
        - Calculation of the total runtime for the current day (in )

    - Equation:
        - _rt = (get_mb *_du/100) * (60/_pr)

    :param _du:             Distribution uniformity or E = irrigation system application efficiency
    :type _du:              float

    :param _pr:             Precipitation Rate
    :type _pr:              float

    :param _yesterdays_mb:  Yesterday's Moisture Balance
    :type _yesterdays_mb:   float

    :param _yesterdays_rt:  Yesterday's Total Runtime (in minutes)
    :type _yesterdays_rt:   float

    :param _yesterdays_rn:  Yesterdays Net Amount of daily rainfall to be used in moisture balance calculation. Allows
                            for an arbitrary loss of 20% of the rainfall to nonuniformity and runoff in inches
    :type _yesterdays_rn:   float

    :param _yesterdays_etc: Yesterday's Turf/Landscape Moisture Requirements: _eto x _kc or _eto x _kl, both work
    :type _yesterdays_etc:  float

    :return:                Today's Runtime
    :rtype:                 float
    """
    #TODO need to check comments and remove other calculate runtime
    _rt = - ((_mb * (100.0/_du)*(60/_pr))*60) #converts to seconds
    _rt = max(0.0, _rt)
    return _rt  # today's total runtime as TODO min/sec? Also fix at top


def verify_irrigation_adequacy(_etc, _mb):
    """
    - Irrigation Adequacy
        - Ratio of crop ETc less deficit over the crop ETc as a percentage

    - Equation:
        - IA = ((_etc - _mb) / _etc) * 100

    - Note:
        - This needs to be 100% to pass the test

    :param _etc:    Turf/Landscape moisture requirements
    :type _etc:     float

    :param _mb:     Daily calculation of root zone moisture balance
    :type _mb:      float

    :return:        True if IA is equal to 100%, False otherwise
    :rtype:         bool
    """
    _ia = ((_etc - _mb) / _etc) * 100.0

    if _ia == 100.0:
        return True     # success
    else:
        return False    # failure
