__author__ = 'baseline'


class SoilTextureTypes(object):
    CLAY = "Clay"
    SILTY_CLAY = "Silty Clay"
    CLAY_LOAM = "Clay Loam"
    LOAM = "Loam"
    SANDY_LOAM = "Sandy Loam"
    LOAMY_SAND = "Loamy Sand"
    SAND = "Sand"


class VegetationTypes(object):
    FESCUE = "Fescue"
    BERMUDA = "Bermuda"
    GROUND_COVER = "Ground Cover"
    WOODY_SHRUBS = "Woody Shrubs"
    TREES_AND_GROUND_COVER = "Trees and Ground Cover"


class IrrigationSystemTypes(object):
    POPUP_SPRAY_HEADS = "Popup Spray Heads"
    POPUP_ROTORY_HEADS = "Popup Rotors Heads"
    BUBBLERS = "Bubblers"
    SURFACE_DRIP = "Surface Drip"
    IMPACT = "Impacts"
    ROTORS = "Rotors"


class WaterSenseCodes(object):
    Allowable_Depletion = "AD"
    Allowable_surface_accumulation = "ASA"
    Available_water = "AW"
    Crop_Coefficient = "KC"
    Cycle_Time = "CT"
    Daily_Moisture_Balance = "MB"
    Definition_of_Deficit = "D"
    Definition_of_Surplus = "S"
    Density_Factor = "Kd"
    # Distribution_uniformity = "DU", "E"
    Distribution_uniformity = "DU"
    Efficiency_Percentage = "EF"
    Enable_ET_Runtime = "EE"
    Evapotranspiration = "ET"
    Free_Water_Calculation = "FW"
    Gross_Area = "GA"
    Gross_Irrigation_Amount ="I"
    Initial_Evapotranspiration = "EI"
    Irrigation_System_Type = "IT"
    Landscape_Coefficient = "Kl"
    Landscape_Evapotranspiration = "ETc"
    Max_Runtime_Before_Run_Off = "CT"
    Microclimate_factor = "Kmc"
    Net_Irrigation_Calculation = "DA"
    Percentage_of_Shade = "PS"
    Plant_Type = "PT"
    Precipitation_Rate = "PR"
    Rain_Fall = "RF"
    Rain_Fall_Loss_of_20_Percent = "RN"
    Reference_Crop_Evapotranspiration = "ETo"
    Root_Depth = "RD"
    Root_Zone_Working_Water_Storage = "RZ"
    Runtime_Total = "RT"
    Slope = "SP"
    Soak_Time = "ST"
    Soil_Intake_Rate = "IR"
    Soil_Type = "SL"
    Species_Factor = "Ks"
    Sprinkler_Type = "SR"
    Sun_Exposure = "SE"
    Turf_Evapotranspiration = "ETc"
    Yesterdays_Daily_Moisture_Balance = "MBo"

