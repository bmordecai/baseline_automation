__author__ = 'baseline'
from wbw_imports import SoilTextureTypes

# Helper Function for creating a list of float values in range
def frange_percent(start, stop, step):
    """
    :param start: Starting index as integer
    :type start: int
    :param stop: Stopping index as integer (inclusive)
    :type stop: int
    :param step: Stepping value
    :type step: float
    :return: List of float in range from start to stop in step increments in percent form (i.e., 1 = 1%, 20 = 20%)
    :rtype: list[float]
    """
    frange_list = list()
    start_percent = float(start) / 100.0
    stop_percent = float(stop) / 100.0
    while start_percent <= stop_percent:
        frange_list.append(start_percent)
        start_percent = round(start_percent + step, 2)
    return frange_list

from multi_key_dict import multi_key_dict
clay = multi_key_dict()
clay[frange_percent(0, 3, 0.01)] = 0.2
clay[frange_percent(4, 6, 0.01)] = 0.15
clay[frange_percent(7, 12, 0.01)] = 0.1
clay[frange_percent(13, 100, 0.01)] = 0.1

silty_clay = multi_key_dict()
silty_clay[frange_percent(0, 3, 0.01)] = 0.23
silty_clay[frange_percent(4, 6, 0.01)] = 0.19
silty_clay[frange_percent(7, 12, 0.01)] = 0.16
silty_clay[frange_percent(13, 100, 0.01)] = 0.13

clay_loam = multi_key_dict()
clay_loam[frange_percent(0, 3, 0.01)] = 0.26
clay_loam[frange_percent(4, 6, 0.01)] = 0.22
clay_loam[frange_percent(7, 12, 0.01)] = 0.18
clay_loam[frange_percent(13, 100, 0.01)] = 0.15

loam = multi_key_dict()
loam[frange_percent(0, 3, 0.01)] = 0.3
loam[frange_percent(4, 6, 0.01)] = 0.25
loam[frange_percent(7, 12, 0.01)] = 0.21
loam[frange_percent(13, 100, 0.01)] = 0.17

sandy_loam = multi_key_dict()
sandy_loam[frange_percent(0, 3, 0.01)] = 0.33
sandy_loam[frange_percent(4, 6, 0.01)] = 0.29
sandy_loam[frange_percent(7, 12, 0.01)] = 0.24
sandy_loam[frange_percent(13, 100, 0.01)] = 0.2

loamy_sand = multi_key_dict()
loamy_sand[frange_percent(0, 3, 0.01)] = 0.36
loamy_sand[frange_percent(4, 6, 0.01)] = 0.3
loamy_sand[frange_percent(7, 12, 0.01)] = 0.26
loamy_sand[frange_percent(13, 100, 0.01)] = 0.22

sand = multi_key_dict()
sand[frange_percent(0, 3, 0.01)] = 0.4
sand[frange_percent(4, 6, 0.01)] = 0.35
sand[frange_percent(7, 12, 0.01)] = 0.3
sand[frange_percent(13, 100, 0.01)] = 0.25

"""
Motivation:
    The idea is to use soil type as keys to access information in order to encapsulate all data related to soil
    types. Thus, soil types are used as dictionary keys to access ASA and IR values respectively.
"""

# usage:
#   foo = dict_of_allowable_surface_accumulation[SoilTextureTypes.CLay][0.06]
#   => foo is now 0.15
dict_of_allowable_surface_accumulation = {
    SoilTextureTypes.CLAY: clay,
    SoilTextureTypes.SILTY_CLAY: silty_clay,
    SoilTextureTypes.CLAY_LOAM: clay_loam,
    SoilTextureTypes.LOAM: loam,
    SoilTextureTypes.SANDY_LOAM: sandy_loam,
    SoilTextureTypes.LOAMY_SAND: loamy_sand,
    SoilTextureTypes.SAND: sand
}

# usage:
#   foo = dict_of_basic_soil_intake_rate[SoilTextureTypes.CLay]
#   => foo is now 0.10
dict_of_basic_soil_intake_rate = {
    SoilTextureTypes.CLAY: 0.10,
    SoilTextureTypes.SILTY_CLAY: 0.15,
    SoilTextureTypes.CLAY_LOAM: 0.20,
    SoilTextureTypes.LOAM: 0.35,
    SoilTextureTypes.SANDY_LOAM: 0.40,
    SoilTextureTypes.LOAMY_SAND: 0.50,
    SoilTextureTypes.SAND: 0.60
}

# usage:
#   foo = dict_of_basic_soil_available_water[SoilTextureTypes.CLay]
#   => foo is now 0.17
dict_of_basic_soil_available_water = {
    SoilTextureTypes.CLAY: 0.17,
    SoilTextureTypes.SILTY_CLAY: 0.17,
    SoilTextureTypes.CLAY_LOAM: 0.18,
    SoilTextureTypes.LOAM: 0.17,
    SoilTextureTypes.SANDY_LOAM: 0.12,
    SoilTextureTypes.LOAMY_SAND: 0.08,
    SoilTextureTypes.SAND: 0.06
}

