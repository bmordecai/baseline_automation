import os
import json
import time
import warnings

# import basic methods for helpers
from common.user_configuration import UserConfiguration
from common.resource_handler import ResourceHandler
from common.object_factory import *
from common import helper_methods
import json_handler as JSON_Handler
from common import page_factory

# Import main objects for setting Connections
from common.objects.base_classes.ser import Ser
# from common.objects.base_classes.server_connections import ServerConnection

# Import main objects for setting Classes
from common.objects.bicoders.valve_bicoder import ValveBicoder
from common.objects.bicoders.flow_bicoder import FlowBicoder
from common.objects.bicoders.pump_bicoder import PumpBicoder
from common.objects.bicoders.switch_bicoder import SwitchBicoder
from common.objects.bicoders.moisture_bicoder import MoistureBicoder
from common.objects.bicoders.temp_bicoder import TempBicoder
from common.objects.bicoders.analog_bicoder import AnalogBicoder

# Pass controller lat/lng to all Device objects
from common.objects.base_classes.devices import BaseDevices
BaseDevices.controller_lat = 43.609768
BaseDevices.controller_long = -116.310569
import subprocess

# Imports main objects for setting Devices
from common.objects.devices.zn import Zone
from common.objects.devices.mv import MasterValve
from common.objects.devices.pm import Pump
from common.objects.devices.ms import MoistureSensor
from common.objects.devices.fm import FlowMeter
from common.objects.devices.sw import EventSwitch
from common.objects.devices.ts import TemperatureSensor
from common.objects.devices.ps import PressureSensor


# Import main objects for setting Controllers
from common.objects.controllers.bl_10 import BaseStation1000
from common.objects.controllers.bl_32 import BaseStation3200
from common.objects.controllers.bl_fs import FlowStation
from common.objects.controllers.bl_sb import Substation
from common.objects.controllers.tw_sim import TWSimulator

#flow items
from common.objects.programming.ml import Mainline
from common.objects.programming.ws import WaterSource
from common.objects.programming.point_of_control import PointOfControl

from common.objects.programming.pg_3200 import PG3200
from common.objects.programming.pg_1000 import PG1000

from common.objects.object_bucket import basestations_1000, basestations_3200, \
    substations, flowstations, server_connections,twsimualator

# server obects
# from common.objects.base_classes.server_connections import ServerConnection
from common.objects.base_classes.old_data_group_packets import OldDataGroupPackets
from common.objects.base_classes.data_group_packets import DataGroupPackets

from common.objects.appmanager.base_page import AppManager
from common.objects.basemanager.base_page import BaseManager
from common.objects.mobile_access.base_page import MobileAccess

# Get objects from bucket


__author__ = 'bens'


class Configuration(object):
    """
    :type resource_handler: common.resource_handler.ResourceHandler
    :type d1: list[str]
    :type sb_d1: dict[int, list[str]]
    :type sb_d2: dict[int, list[str]]
    :type sb_d4: dict[int, list[str]]
    :type sb_dd: dict[int, list[str]]
    :type sb_fm: dict[int, list[str]]
    :type sb_ms: dict[int, list[str]]
    :type sb_ts: dict[int, list[str]]
    :type sb_sw: dict[int, list[str]]
    :type sb_pu: dict[int, list[str]]
    """

    def __init__(self, test_name, user_conf_file, data_json_file=None, number_of_substations_to_use=0, configuration_dir=None, serial_port_time_out=None):
        """
        Configuration initializer.

        :param test_name:                       Name of test to set as the controller's description. \n
        :type test_name:                        str \n

        :param user_conf_file:                  User configuration instance. \n
        :type user_conf_file:                   UserConfiguration \n

        :param data_json_file:                  Name of json file containing all data for test. \n
        :type data_json_file:                   str or None\n
        
        :param number_of_substations_to_use:    Number of substations to initialize for Use case (if specified.)
        :type number_of_substations_to_use:     int \n
        """
        self.resource_handler = None
        self.cn_ty = ''

        self.number_of_substations_to_use = number_of_substations_to_use

        # Set the test name as the controller's description (to name the controller to the same thing as the test)
        self.test_name = test_name

        # User configuration file
        self.user_conf = user_conf_file

        self.serial_port_time_out = serial_port_time_out

        # directory of JSON configuration file, relative to ???
        if data_json_file:
            if configuration_dir is not None:
                self.configuration_file_path = os.path.join(configuration_dir, data_json_file)
            else:
                self.configuration_file_path = os.path.join('common' + os.sep + 'configuration_files', data_json_file)
        else:
            self.configuration_file_path = None

        # JSON configuration file
        # self.data_file = data_json_file

        # Files
        self.converted_file = ''

        # Server Objects Dictionaries
        self.ServerConnections = {}
        """:type: dict[int, common.objects.base_classes.server_connections.ServerConnection]"""

        # Controller object Dictionaries
        self.TWSimulator = {}
        """:type: dict[int, common.objects.controllers.tw_sim.TWSimulator]"""
        self.BaseStation1000 = {}
        """:type: dict[int, common.objects.controllers.bl_10.BaseStation1000]"""
        self.BaseStation3200 = {}
        """:type: dict[int, common.objects.controllers.bl_32.BaseStation3200]"""
        self.FlowStations = {}
        """:type: dict[int, common.objects.controllers.bl_fs.FlowStation]"""
        self.SubStations = {}
        """:type: dict[int, common.objects.controllers.bl_sb.Substation]"""
        self.AppManager = None
        """:type: common.objects.appmanager.base_page.AppManager"""
        self.BaseManager = None
        """:type: common.objects.basemanager.base_page.BaseManager"""
        self.MobileAccess = None
        """:type: common.objects.mobile_access.base_page.MobileAccess"""

        # Stores a list of all controllers
        self.all_controllers = []

        # Placeholders for Device Serial Numbers loaded onto the controller which are read from the json file.
        self.d1 = []           # Single Valve Decoders [list]
        self.mv_d1 = []        # Single Valve Decoder used as a master valve
        self.pm_d1 = []        # Single Valve Decoder used as a Pump
        self.d2 = []           # Dual Valve Decoders
        self.mv_d2 = []        # Dual Valve Decoder used as a master valve
        self.pm_d2 = []        # Dual Valve Decoder used as a Pump
        self.d4 = []           # Quad Valve Decoders
        self.dd = []           # Twelve Valve Decoders
        self.mv = []           # Master Valve Decoders
        self.pm = []           # Pump Decoders
        self.ms = []           # Moisture Sensor Serial Numbers
        self.ts = []           # Temperature Sensor Serial Numbers
        self.sw = []           # Event Switch Serial Numbers
        self.fm = []           # Flow Meter Serial Numbers
        self.ps = []           # Pressure Sensor Serial Numbers

        #####
        # Address ranges for each addressable object for configuration
        # This is read from the .json file with method load_data_from_json_file
        #####
        self.zn_ad_range = []    # Zone address range [list]
        self.ms_ad_range = []    # Moisture Sensor address range [list]
        self.mv_ad_range = []    # Master Valve address range [list]
        self.pm_ad_range = []    # Pump address range [list]
        self.fm_ad_range = []    # Flow Meter address range [list]
        self.sw_ad_range = []    # Event Switch address range [list]
        self.ts_ad_range = []    # Temperature address range [list]
        self.ps_ad_range = []    # Pressure Sensor address range [list]

        # This is read from the .json file with method load_data_from_json_file
        self.daily_eto_value = []
        self.daily_rainfall_value = []

        # Session ID of the BaseManager session after we log in
        self.session_id = ''

    #################################
    def reset_all(self):
        """
        Resets all object bucket dictionaries to clean state.
        """
        server_connections.clear()
        # controller classes
        basestations_1000.clear()
        basestations_3200.clear()
        flowstations.clear()
        substations.clear()
        twsimualator.clear()


    #################################
    def initialize_server_for_test(self, server=False):
        pass
        # TODO Finish this
        # if server:
        #     start server
        #     connect to server
        #     verify connection

    #################################
    def initialize_for_test(self, connect_to_basemanager=False, websocket=False, real_devices=False,
                            address_devices=True, web_tests= False):
        """
        Initializes objects and other things required for the test.
        
        1) Resets the object bucket to a clean starting state.

        2)
        """
        helper_methods.print_test_started(self.test_name)

        # 1 - Reset object bucket
        self.reset_all()

        # 2 - Base Class 'Controller Type' inheritance TODO this shouldn't need to be done anymore
        # BaseDevices.controller_type = self.cn_ty
        # Ser.controller_type = self.cn_ty

        # 3 - Initialize serial and browser instances for use.
        self.resource_handler = ResourceHandler(user_conf=self.user_conf, configuration=self)

        # Create all controller objects and assign them their devices from the JSON file
        self.create_cn_objects(real_devices=real_devices, address_devices=address_devices)

        # Connect to BaseManger using ip information from the user configuration
        #  - Verify that the controller connected
        current_controller = ''
        try:
            # The amount of times we want to attempt for our controller to connect to BaseManager without failing out
            retries_allowed = 2
            retry_amount = 0

            for current_controller in self.BaseStation3200.keys():
                while retry_amount <= retries_allowed:
                    try:
                        self.BaseStation3200[current_controller].get_data()
                        self.BaseStation3200[current_controller].vr = \
                            self.BaseStation3200[current_controller].data.get_value_string_by_key(opcodes.firmware_version)
                        self.BaseStation3200[current_controller]. \
                            add_basemanager_connection_to_controller(use_basemanager=connect_to_basemanager,
                                                                     _url=self.user_conf.url,
                                                                     _fixed_ip=self.user_conf.fixed_ip
                                                                     )
                        self.BaseStation3200[current_controller].basemanager_connection[1].verify_use_alternate_ip()
                        self.BaseStation3200[current_controller].basemanager_connection[1].verify_bm_ip_address_on_cn()
                        self.BaseStation3200[current_controller].basemanager_connection[1]. \
                            verify_controller_is_connecting_using_ethernet()
                        if connect_to_basemanager:
                            self.BaseStation3200[current_controller].basemanager_connection[1].wait_for_bm_connection()
                        break
                    except Exception as e:
                        retry_amount += 1
                        # If we are over our allotted amount of retries to connect, throw the error
                        if retry_amount > retries_allowed:
                            raise

            for current_controller in self.BaseStation1000.keys():
                while retry_amount <= retries_allowed:
                    try:
                        self.BaseStation1000[current_controller].get_data()
                        self.BaseStation1000[current_controller].vr = \
                            self.BaseStation1000[current_controller].data.get_value_string_by_key(opcodes.firmware_version)
                        self.BaseStation1000[current_controller].\
                            add_basemanager_connection_to_controller(use_basemanager=connect_to_basemanager,
                                                                     _url=self.user_conf.url,
                                                                     _fixed_ip=self.user_conf.fixed_ip
                                                                     )
                        self.BaseStation1000[current_controller].basemanager_connection[1].verify_use_alternate_ip()
                        self.BaseStation1000[current_controller].basemanager_connection[1].verify_bm_ip_address_on_cn()
                        if connect_to_basemanager:
                            self.BaseStation1000[current_controller].basemanager_connection[1].wait_for_bm_connection()
                        break
                    except Exception as e:
                        retry_amount += 1
                        # If we are over our allotted amount of retries to connect, throw the error
                        if retry_amount > retries_allowed:
                            raise

        except Exception as e:
            e_msg = "Exception occurred trying to connect Controller To BaseManager: Message '{0}'." \
                    .format(e.message)
            raise Exception(e_msg)

        if web_tests:
            # Init main page objects so that they have a web driver instance to
            # pass to child pages build through page factory
            self.create_web_objects(_browser=self.user_conf.browsers_for_testing)
            self.resource_handler.web_driver.open(_browser_name=self.user_conf.browsers_for_testing[0])

    #################################
    def get_controller_with_mac(self, mac_address):
        """
        Search the configuration for a controller object with the specified mac address.

        :param mac_address: Mac Address of the controller you want to find.
        :type mac_address:  str
        :return:            Controller object
        :rtype:             common.objects.base_classes.controller.BaseController
        """
        controllers_in_config = self.FlowStations.values() + self.BaseStation3200.values() + self.SubStations.values() + self.BaseStation1000.values()
        for controller in controllers_in_config:
            if mac_address == controller.mac:
                return controller

    #################################
    def create_cn_objects(self, real_devices=False, address_devices=True):
        """
        2) Create Serial connection Objects

        :param real_devices: Flag used by BaseStation use cases to manage whether or not real devices are being used.
                             This is mainly used for the manual BaseStation use cases. \n
        :type real_devices: bool
        :param address_devices: Flag used by BaseManager use cases to manage whether or not devices need to be addressed
                                prior to the use case running. \n
        :type address_devices: bool
        :return:
        :rtype:
        """
        controller_count = len(self.user_conf.controllers)
        substation_index = 1
        simulator_index = 1
        flowstation_index = 1
        bl3200_index = 1
        bl1000_index = 1

        for controller_obj_dict in self.user_conf.controllers:
            controller_type = controller_obj_dict['type']
            controller_serial_number = controller_obj_dict['serial_number']
            controller_firmware_version = controller_obj_dict['firmware_version']
            controller_mac_address = controller_obj_dict['mac_address']
            controller_comport = controller_obj_dict['port_address']
            controller_eth_port = controller_obj_dict['socket_port']
            controller_ip_address = controller_obj_dict['ip_address']
            try:
                logging_port = controller_obj_dict['logging_port']
            except KeyError:
                logging_port = None
            # controller_data_base_id = controller_obj_dict['fw_database_id']

            if controller_type == opcodes.flow_station:
                controller_ser = self.resource_handler.create_serial_connection(comport=controller_comport,
                                                                                ethernet_port=controller_eth_port)
                controller_ser.init_ser_connection()
                controller_ser.check_if_cn_connected()

                controller = FlowStation(_serial_port=controller_ser,
                                         _serial_number=controller_serial_number,
                                         _firmware_version=controller_firmware_version,
                                         _mac=controller_mac_address,
                                         _port_address=controller_comport,
                                         _socket_port=controller_eth_port,
                                         _ip_address=controller_ip_address,
                                         _description=self.test_name)

                self.FlowStations[flowstation_index] = controller
                controller.basestation_controllers = self.BaseStation3200
                if controller_ser is not None:
                    controller.initialize_for_test(real_devices=real_devices)
                if logging_port is not None:
                    logging_controller_ser = self.resource_handler.create_serial_connection('None', logging_port)
                    logging_controller_ser.init_ser_connection()
                    controller.ser2 = logging_controller_ser
                    controller.data_group_packets = DataGroupPackets(controller,
                                                                     self.user_conf.user_name,
                                                                     self.user_conf.user_password,
                                                                     self.session_id)
                    self.session_id = controller.data_group_packets.session_id
                    controller.old_data_group_packets = OldDataGroupPackets(controller,
                                                                            self.user_conf.user_name,
                                                                            self.user_conf.user_password,
                                                                            self.session_id)
                flowstation_index += 1

            elif controller_type == opcodes.basestation_3200:

                # ------------------------------------------------------------------------------------------------------
                # 0) Setup the serial port connection
                # 1) Read specified device serial numbers and address from use case JSON file
                # 2) Create 3200 instance
                # 3) Add 3200 instance to 3200 dictionary
                # 4) Clear programming and devices and configure controller for test
                # 5) Load devices onto the controller
                # 6) Search for devices on controller
                # 7) Create device objects
                # 8) Assign devices an address
                # ------------------------------------------------------------------------------------------------------

                # If we are processing a second 3200 and there was only a JSON config for a single 3200, ignore
                # initializing the second 3200.
                if bl3200_index == 2 and not self.is_configured_for_multiple_bl_3200s():
                    continue

                # 0
                controller_ser = self.resource_handler.create_serial_connection(comport=controller_comport,
                                                                                ethernet_port=controller_eth_port)
                controller_ser.init_ser_connection()
                controller_ser.check_if_cn_connected()
                # special use case for overwriting the serial port before the search happens
                if self.serial_port_time_out:
                    controller_ser.serial_conn.setTimeout(self.serial_port_time_out)
                # 1
                if self.configuration_file_path:
                    self.load_data_from_json_file(_controller_type=controller_type, _controller_number=bl3200_index)
                # 2
                controller = BaseStation3200(_serial_port=controller_ser,
                                             _address=bl3200_index,
                                             _serial_number=controller_serial_number,
                                             _firmware_version=controller_firmware_version,
                                             _mac=controller_mac_address,
                                             _port_address=controller_comport,
                                             _socket_port=controller_eth_port,
                                             _ip_address=controller_ip_address,
                                             _description=self.test_name)
                # 3
                self.BaseStation3200[bl3200_index] = controller
                # This is used in program_summary_reporter
                self.all_controllers.append(controller)
                # 4
                controller.initialize_for_test(real_devices=real_devices)
                # 5
                controller.load_all_dv(d1_list=self.d1, mv_d1_list=self.mv_d1,pm_d1_list=self.pm_d1, d2_list=self.d2,
                                       mv_d2_list=self.mv_d2, pm_d2_list=self.pm_d2, d4_list=self.d4, dd_list=self.dd,
                                       ms_list=self.ms,fm_list=self.fm, ts_list=self.ts, sw_list=self.sw,
                                       an_list=self.ps)
                _list = self.build_list_of_device_types_present_in_json()
                
                # 6 - Addition: only search devices if specified. Defaults to True, but BaseManager use cases might
                #               set this to false when trying to do assignments from BaseManager
                if address_devices:
                    controller.do_search_for_all_devices(_list_of_device_types=_list)
                
                time.sleep(5)  # add sleep so that the search does not over run
                # 7
                self.create_bl_3200_devices_objects(_controller=controller)

                # 8 - Addition: only address devices if specified. Defaults to True, but BaseManager use cases might
                #               set this to false when trying to do assignments from BaseManager
                if address_devices:
                    controller.assign_all_devices()
                    
                #  assign ser2 to the new logging controller serial object
                if logging_port is not None:
                    logging_controller_ser = self.resource_handler.create_serial_connection('None', logging_port)
                    logging_controller_ser.init_ser_connection()
                    controller.ser2 = logging_controller_ser
                    # Create a thread for listening in on a logging serial port. Log into BaseManager
                    controller.data_group_packets = DataGroupPackets(controller,
                                                                     self.user_conf.user_name,
                                                                     self.user_conf.user_password,
                                                                     self.session_id)
                    self.session_id = controller.data_group_packets.session_id
                    controller.old_data_group_packets = OldDataGroupPackets(controller,
                                                                            self.user_conf.user_name,
                                                                            self.user_conf.user_password,
                                                                            self.session_id)
                # Increase index for next 3200
                bl3200_index += 1

            elif controller_type == opcodes.basestation_1000:
                # 0 Setup the serial port connection
                controller_ser = self.resource_handler.create_serial_connection(comport=controller_comport,
                                                                                ethernet_port=controller_eth_port)
                controller_ser.init_ser_connection()
                controller_ser.check_if_cn_connected()
                # 1 Read specified device serial numbers and address from use case JSON file
                if self.configuration_file_path:
                    self.load_data_from_json_file(_controller_type=controller_type, _controller_number=bl3200_index)
                # 2 Create 1000 instance
                controller = BaseStation1000(_serial_port=controller_ser,
                                             _address=bl1000_index,
                                             _serial_number=controller_serial_number,
                                             _firmware_version=controller_firmware_version,
                                             _mac=controller_mac_address,
                                             _port_address=controller_comport,
                                             _socket_port=controller_eth_port,
                                             _ip_address=controller_ip_address,
                                             _description=self.test_name)
                # 3 Add 1000 instance to 1000 dictionary
                self.BaseStation1000[bl1000_index] = controller
                self.all_controllers.append(controller)
                # 4 Clear programming and devices and configure controller for test
                controller.initialize_for_test(real_devices=real_devices)
                # 5 Load devices onto the controller
                controller.load_all_dv(d1_list=self.d1, mv_d1_list=self.mv_d1, pm_d1_list=self.pm_d1, d2_list=self.d2,
                                       mv_d2_list=self.mv_d2, pm_d2_list=self.pm_d2, d4_list=self.d4, dd_list=self.dd,
                                       ms_list=self.ms, fm_list=self.fm, ts_list=self.ts, sw_list=self.sw,
                                       an_list=self.ps)
                _list = self.build_list_of_device_types_present_in_json()

                # 6 - Addition: only search devices if specified. Defaults to True, but BaseManager use cases might
                #               set this to false when trying to do assignments from BaseManager
                if address_devices:
                    controller.do_search_for_all_devices(_list_of_device_types=_list)

                time.sleep(5)  # add sleep so that the search does not over run
                # 7 Create device objects
                self.create_bl_1000_device_objects(_controller=controller)

                # 8 - Addition: only address devices if specified. Defaults to True, but BaseManager use cases might
                #               set this to false when trying to do assignments from BaseManager
                if address_devices:
                    controller.assign_all_devices()

                #  assign ser2 to the new logging controller serial object
                if logging_port is not None:
                    logging_controller_ser = self.resource_handler.create_serial_connection('None', logging_port)
                    logging_controller_ser.init_ser_connection()
                    controller.ser2 = logging_controller_ser
                    # Create a thread for listening in on a logging serial port. Log into BaseManager
                    controller.data_group_packets = DataGroupPackets(controller,
                                                                     self.user_conf.user_name,
                                                                     self.user_conf.user_password,
                                                                     self.session_id)
                    self.session_id = controller.data_group_packets.session_id
                    controller.old_data_group_packets = OldDataGroupPackets(controller,
                                                                            self.user_conf.user_name,
                                                                            self.user_conf.user_password,
                                                                            self.session_id)
                # Increase index for next 3200
                bl1000_index += 1

            elif controller_type == opcodes.substation:
                controller_ser = self.resource_handler.create_serial_connection(comport=controller_comport,
                                                                                ethernet_port=controller_eth_port)
                controller_ser.init_ser_connection()
                controller_ser.check_if_cn_connected()
                if self.configuration_file_path:
                    self.load_data_from_json_file(_controller_type=controller_type, _controller_number=substation_index)
                controller = Substation(_serial_port=controller_ser,
                                        _serial_number=controller_serial_number,
                                        _firmware_version=controller_firmware_version,
                                        _mac=controller_mac_address,
                                        _port_address=controller_comport,
                                        _socket_port=controller_eth_port,
                                        _ip_address=controller_ip_address,
                                        _description=self.test_name)
                self.SubStations[substation_index] = controller
                # This is used in program_summary_reporter
                self.all_controllers.append(controller)
                controller.initialize_for_test()
                # Loads all the devices from the JSON onto the substation and create those bicoder objects
                controller.load_all_dv(d1_list=self.d1, mv_d1_list=self.mv_d1,pm_d1_list=self.pm_d1, d2_list=self.d2,
                                       mv_d2_list=self.mv_d2, pm_d2_list=self.pm_d2, d4_list=self.d4, dd_list=self.dd,
                                       ms_list=self.ms,fm_list=self.fm, ts_list=self.ts, sw_list=self.sw)
                _list = self.build_list_of_device_types_present_in_json()
                # controller.do_search_for_all_devices(_list)
                time.sleep(5)  # add sleep so that the search does not over run

                substation_index += 1

            elif controller_type == opcodes.tw_simulator:
                # Execute the simulator from our local file system in a separate process
                two_wire_simulator_port = controller_eth_port.split(":")[2]
                curr_dir = os.path.dirname(__file__)
                path_to_executable = os.path.join(curr_dir, "..", "weathertrak_tests", opcodes.two_wire_simulator)
                args = [path_to_executable, "-com", controller_comport, "-port", two_wire_simulator_port]
                twsimulator_process = subprocess.Popen(args)

                controller_ser = self.resource_handler.create_serial_connection(comport='None',
                                                                                ethernet_port=controller_eth_port)
                controller_ser.init_ser_connection()
                controller_ser.check_if_cn_connected()
                if self.configuration_file_path:
                    self.load_data_from_json_file(_controller_type=controller_type, _controller_number=simulator_index)
                controller = TWSimulator(_serial_port=controller_ser,
                                         _serial_number=controller_serial_number,
                                         _firmware_version=controller_firmware_version,
                                         _mac=controller_mac_address,
                                         _port_address=controller_comport,
                                         _socket_port=controller_eth_port,
                                         _ip_address=controller_ip_address,
                                         _description=self.test_name)
                self.TWSimulator[simulator_index] = controller
                # This is used in program_summary_reporter
                self.all_controllers.append(controller)
                controller.initialize_for_test(real_devices=True)
                # Loads all simulated devices from the JSON onto the substation and create those bicoder objects
                controller.load_all_simulated_devices(d1_list=self.d1, mv_d1_list=self.mv_d1,pm_d1_list=self.pm_d1,
                                                      d2_list=self.d2, mv_d2_list=self.mv_d2, pm_d2_list=self.pm_d2,
                                                      d4_list=self.d4, dd_list=self.dd, ms_list=self.ms,fm_list=self.fm,
                                                      ts_list=self.ts, sw_list=self.sw)
                _list = self.build_list_of_device_types_present_in_json()

                time.sleep(5)  # add sleep so that the search does not over run

                simulator_index += 1

    #################################
    def create_web_objects(self, _browser):
        """
        Initializes all web-based clients main instance that houses references
        to each page object. Then for each client, creates each page object.
        """
        # Create/init AppManager main page
        self.AppManager = AppManager(web_driver=self.resource_handler.web_driver)
        # Create the rest of AppManager page objects
        self.create_appmanager_objects(_browser=_browser)

        # Create/init BaseManager main page
        self.BaseManager = BaseManager(web_driver=self.resource_handler.web_driver)
        # Create the rest of BaseManager page objects
        self.create_basemanager_objects(_controller=self.BaseStation3200[1])

        # Create/init MobileAccess main page
        self.MobileAccess = MobileAccess(web_driver=self.resource_handler.web_driver)
        # Create the rest of MobileAccess page objects
        self.create_mobile_access_objects(_browser=_browser)

    #################################
    def create_basemanager_objects(self, _controller):
        """
        Creates the BaseManager page objects.

        :param _controller: Controller reference for device views.
        :type _controller: common.objects.controllers.bl_32.BaseStation3200

        :return: Nothing.
        :rtype: None
        """

        # BM LOGIN
        page_factory.get_basemanager_login_page_object(base_page=self.BaseManager)

        # MAIN BM PAGES
        page_factory.get_basemanager_main_page_object(base_page=self.BaseManager)
        page_factory.get_basemanager_main_menu_object(base_page=self.BaseManager)
        page_factory.get_basemanager_controller_settings_dialog_object(base_page=self.BaseManager)

        # ADMIN TOOLS
        page_factory.get_basemanager_admin_tools_page_object(base_page=self.BaseManager)

        # DEVICES
        page_factory.get_basemanager_zones_page_object(base_page=self.BaseManager, _controller=_controller)
        page_factory.get_basemanager_moisture_sensors_page_object(base_page=self.BaseManager, _controller=_controller)
        page_factory.get_basemanager_flow_meter_page_object(base_page=self.BaseManager, _controller=_controller)
        page_factory.get_basemanager_master_valves_page_object(base_page=self.BaseManager, _controller=_controller)
        page_factory.get_basemanager_pumps_page_object(base_page=self.BaseManager, _controller=_controller)
        page_factory.get_basemanager_pressure_sensors_page_object(base_page=self.BaseManager, _controller=_controller)
        page_factory.get_basemanager_temperature_sensors_page_object(base_page=self.BaseManager, _controller=_controller)
        page_factory.get_basemanager_event_switch_page_object(base_page=self.BaseManager, _controller=_controller)

        # LIVE VIEW
        page_factory.get_basemanager_live_view_page_object(base_page=self.BaseManager)

        # MAPS
        page_factory.get_basemanager_maps_page_object(base_page=self.BaseManager)

        # PROGRAMS
        page_factory.get_basemanager_quick_view_page_object(base_page=self.BaseManager,  _controller=_controller)

        # QUICK VIEW
        page_factory.get_basemanager_subscription_page_object(base_page=self.BaseManager)

        # SUBSCRIPTIONS
        page_factory.get_basemanager_programs_page_object(base_page=self.BaseManager, _controller=_controller)

        # FLOW SETUP
        # page_factory.get_basemanager_1000_water_sources_tab(base_page=self.BaseManager)
        page_factory.get_basemanager_3200_water_sources_tab(base_page=self.BaseManager, _controller=_controller)
        page_factory.get_basemanager_3200_poc_tab(base_page=self.BaseManager, _controller=_controller)
        page_factory.get_basemanager_3200_mainlines_tab(base_page=self.BaseManager, _controller=_controller)

    #################################
    def create_mobile_access_objects(self, _browser):
        # Create page instances to use for Mobile Access Client use
        page_factory.get_mobile_login_page_object(base_page=self.MobileAccess)
        page_factory.get_mobile_main_page_object(base_page=self.MobileAccess)
        page_factory.get_mobile_test_device_page_object(base_page=self.MobileAccess)

    #################################
    def create_appmanager_objects(self,_browser):
        # Create page instances to use for AppMamanger Client use
        page_factory.get_appmanager_login_page_object(base_page=self.AppManager)
        page_factory.get_appmanager_main_page_object(base_page=self.AppManager)

    # #################################
    # def create_tw_simulated_device_objects(self, _controller):
    #     """
    #
    #     Uses object factory methods to create the device-specific objects specified by the ranges and serial numbers in
    #     the json. \n
    #
    #     :param _controller:     Controller to pass into all of your objects. \n
    #     :type _controller:      common.objects.controllers.tw_sim.TWSimulator \n
    #
    #     :return:
    #     """
    #     create_simulated_single_valve_bicoder_object(_controller=_controller,
    #                                                  _tw_address_range=self.zn_ad_range,
    #                                                  _serial_number='',_device_type='')
    #     create_simulated_dual_valve_bicoder_object(_controller=_controller,
    #                                                _tw_address_range=self.zn_ad_range,
    #                                                _serial_number='',_device_type='')
    #     create_simulated_quad_valve_bicoder_object(_controller=_controller,
    #                                                _tw_address_range=self.zn_ad_range,
    #                                                _serial_number='',_device_type='')
    #
    #     create_simulated_twelve_valve_bicoder_object(_controller=_controller,
    #                                                  _tw_address_range=self.zn_ad_range,
    #                                                  _serial_number='',_device_type='')
    #     create_simulated_master_valve_objects(_controller=_controller,
    #                                           _tw_address_range=self.mv,
    #                                           _serial_number=self.mv)
    #     create_simulated_pump_bicoder_objects(_controller=_controller,
    #                                           _tw_address_range=self.pm,
    #                                           _serial_number=self.pm)
    #     create_simulated_moisture_sensor_bicoder_objects(_controller=_controller,
    #                                                      _tw_address_range=self.ms_ad_range,
    #                                                      _serial_number=self.ms)
    #     create_simulated_flow_meter_bicoder_objects(_controller=_controller,
    #                                                 _tw_address_range=self.fm_ad_range,
    #                                                 _serial_number=self.fm)
    #     create_simulated_event_switch_bicoder_objects(_controller=_controller,
    #                                                   _tw_address_range=self.sw_ad_range,
    #                                                   _serial_number=self.sw)
    #     create_simulated_temperature_sensor_bicoder_objects(_controller=_controller,
    #                                                         _tw_address_range=self.ts_ad_range,
    #                                                         _serial_number=self.ts)
    #     create_simulated_pressure_bicoder_objects(_controller=_controller,
    #                                            _tw_address_range=self.ps_ad_range,
    #                                            _serial_number=self.ps)

    #################################
    def create_bl_1000_device_objects(self, _controller):
        """

        Uses object factory methods to create the device-specific objects specified by the ranges and serial numbers in
        the json. \n

        :param _controller:     Controller to pass into all of your objects. \n
        :type _controller:      common.objects.controllers.bl_10.BaseStation3200 \n

        :return:
        """
        create_zone_objects(controller=_controller, zn_ad_range=self.zn_ad_range, d1=self.d1, d2=self.d2, d4=self.d4,
                            dd=self.dd)
        create_master_valve_objects(controller=_controller, address_range=self.mv_ad_range, serial_numbers=self.mv)
        create_pump_objects(controller=_controller, address_range=self.pm_ad_range, serial_numbers=self.pm)
        create_moisture_sensor_objects(controller=_controller, address_range=self.ms_ad_range, serial_numbers=self.ms)
        create_flow_meter_objects(controller=_controller, address_range=self.fm_ad_range, serial_numbers=self.fm)
        create_event_switch_objects(controller=_controller, address_range=self.sw_ad_range, serial_numbers=self.sw)
        create_temperature_sensor_objects(controller=_controller, address_range=self.ts_ad_range,
                                          serial_numbers=self.ts)
        create_pressure_sensor_objects(controller=_controller, address_range=self.ps_ad_range, serial_numbers=self.ps)

    #################################
    def create_bl_3200_devices_objects(self, _controller):
        """
        Uses object factory methods to create the device-specific objects specified by the ranges and serial numbers in
        the json. \n

        :param _controller:     Controller to pass into all of your objects. \n
        :type _controller:      common.objects.controllers.bl_32.BaseStation3200 \n

        :return:
        """
        create_zone_objects(controller=_controller, zn_ad_range=self.zn_ad_range, d1=self.d1, d2=self.d2, d4=self.d4,
                            dd=self.dd)
        create_master_valve_objects(controller=_controller, address_range=self.mv_ad_range, serial_numbers=self.mv)
        create_pump_objects(controller=_controller, address_range=self.pm_ad_range, serial_numbers=self.pm)
        create_moisture_sensor_objects(controller=_controller, address_range=self.ms_ad_range, serial_numbers=self.ms)
        create_flow_meter_objects(controller=_controller, address_range=self.fm_ad_range, serial_numbers=self.fm)
        create_event_switch_objects(controller=_controller, address_range=self.sw_ad_range, serial_numbers=self.sw)
        create_temperature_sensor_objects(controller=_controller, address_range=self.ts_ad_range,
                                          serial_numbers=self.ts)
        create_pressure_sensor_objects(controller=_controller, address_range=self.ps_ad_range, serial_numbers=self.ps)

    #################################
    def load_data_from_json_file(self, _controller_type, _controller_number):
        """
        Gets all device information from a config file based on the type of controller ('10' | '32' | 'SB' | 'FS'), and
        the size of configuration requested. \n

        :param _controller_type:     The type of this controller ('10' | '32' | 'SB' | 'FS'). \n
        :type _controller_type:     str \n

        :param _controller_number:  The number of the controller in this configuration. (Example: If you have two 3200s,
                                    your controller_numbers will be 1 and 2 for the first and second 3200)\n
        :type _controller_number:   int \n

        :return:
        """
        # Open the .json file, at this point the .json file is un-readable by our python script.
        config_file = open(self.configuration_file_path)

        # Convert the .json file into a Python readable 'Object' so we can access the information stored.
        self.converted_file = json.load(config_file)
        # This next stuff is to clear out the dictionaries
        # Placeholders for Device Serial Numbers loaded onto the controller which are read from the json file.
        self.d1 = []           # Single Valve Decoders
        self.mv_d1 = []        # Single Valve Decoder used as a master valve
        self.pm_d1 = []        # Single Valve Decoder used as a Pump
        self.d2 = []           # Dual Valve Decoders
        self.mv_d2 = []        # Dual Valve Decoder used as a master valve
        self.pm_d2 = []        # Dual Valve Decoder used as a Pump
        self.d4 = []           # Quad Valve Decoders
        self.dd = []           # Twelve Valve Decoders
        self.mv = []           # Master Valve Decoders
        self.pm = []           # Pump Decoders
        self.ms = []           # Moisture Sensor Serial Numbers
        self.ts = []           # Temperature Sensor Serial Numbers
        self.sw = []           # Event Switch Serial Numbers
        self.fm = []           # Flow Meter Serial Numbers
        self.ps = []           # Pressure Sensor Serial Numbers

        #####
        # Address ranges for each addressable object for configuration
        # This is read from the .json file with method load_data_from_json_file
        #####
        self.zn_ad_range = []    # Zone address range [list]
        self.ms_ad_range = []    # Moisture Sensor address range [list]
        self.mv_ad_range = []    # Master Valve address range [list]
        self.pm_ad_range = []    # Pump address range [list]
        self.fm_ad_range = []    # Flow Meter address range [list]
        self.sw_ad_range = []    # Event Switch address range [list]
        self.ts_ad_range = []    # Temperature address range [list]
        self.ps_ad_range = []    # Pressure Sensor address range [list]


        try:
            self.converted_file[_controller_type]
        except KeyError as e:
            return

        # If the controller we are looking through is a substation
        if _controller_type == opcodes.substation:
            self.d1 = map(str, self.converted_file[_controller_type][_controller_number - 1]['D1'])
            self.d2 = map(str, self.converted_file[_controller_type][_controller_number - 1]['D2'])
            self.d4 = map(str, self.converted_file[_controller_type][_controller_number - 1]['D4'])
            self.dd = map(str, self.converted_file[_controller_type][_controller_number - 1]['DD'])
            self.fm = map(str, self.converted_file[_controller_type][_controller_number - 1]['FB'])
            self.sw = map(str, self.converted_file[_controller_type][_controller_number - 1]['SB'])
            self.ts = map(str, self.converted_file[_controller_type][_controller_number - 1]['TB'])
            self.ms = map(str, self.converted_file[_controller_type][_controller_number - 1]['MB'])
            self.pm = map(str, self.converted_file[_controller_type][_controller_number - 1]['PB'])
            self.pm = map(str, self.converted_file[_controller_type][_controller_number - 1]['AN'])
        else:
            # If the JSON has this controller type in a list format (meaning it has more than 1 of this controller type)
            if type(self.converted_file[_controller_type]) is list:
                # Get all device serial numbers
                self.d1 = map(str, self.converted_file[_controller_type][_controller_number - 1]['D1'])
                self.mv_d1 = map(str, self.converted_file[_controller_type][_controller_number - 1]['MV_D1'])
                self.d2 = map(str, self.converted_file[_controller_type][_controller_number - 1]['D2'])
                self.mv_d2 = map(str, self.converted_file[_controller_type][_controller_number - 1]['MV_D2'])
                self.d4 = map(str, self.converted_file[_controller_type][_controller_number - 1]['D4'])
                self.dd = map(str, self.converted_file[_controller_type][_controller_number - 1]['DD'])
                self.mv = map(str, self.converted_file[_controller_type][_controller_number - 1]['MV']['serial'])
                self.ms = map(str, self.converted_file[_controller_type][_controller_number - 1]['MS']['serial'])
                self.fm = map(str, self.converted_file[_controller_type][_controller_number - 1]['FM']['serial'])
                self.ts = map(str, self.converted_file[_controller_type][_controller_number - 1]['TS']['serial'])
                self.sw = map(str, self.converted_file[_controller_type][_controller_number - 1]['SW']['serial'])
                try:
                    self.pm = map(str, self.converted_file[_controller_type][_controller_number - 1]['PM']['serial'])
                    self.pm_d1 = map(str, self.converted_file[_controller_type][_controller_number - 1]['PM_D1'])
                    self.pm_d2 = map(str, self.converted_file[_controller_type][_controller_number - 1]['PM_D2'])
                except KeyError as e:
                    pass
                try:
                    self.ps = map(str, self.converted_file[_controller_type][_controller_number - 1]['PS']['serial'])
                except KeyError as e:
                    pass

                # Get all ranges for each device
                # in the json file this is looking at the
                # Example "ZN": {"range": [1, 150, 187, 200]},
                self.zn_ad_range = self.converted_file[_controller_type][_controller_number - 1]['ZN']['range']
                self.ms_ad_range = self.converted_file[_controller_type][_controller_number - 1]['MS']['range']
                self.fm_ad_range = self.converted_file[_controller_type][_controller_number - 1]['FM']['range']
                self.mv_ad_range = self.converted_file[_controller_type][_controller_number - 1]['MV']['range']
                self.ts_ad_range = self.converted_file[_controller_type][_controller_number - 1]['TS']['range']
                self.sw_ad_range = self.converted_file[_controller_type][_controller_number - 1]['SW']['range']
                try:
                    self.pm_ad_range = self.converted_file[_controller_type][_controller_number - 1]['PM']['range']
                except KeyError as e:
                    pass
                try:
                    self.ps_ad_range = self.converted_file[_controller_type][_controller_number - 1]['PS']['range']
                except KeyError as e:
                    pass
            else:
                # Get all device serial numbers
                self.d1 = map(str, self.converted_file[_controller_type]['D1'])
                self.mv_d1 = map(str, self.converted_file[_controller_type]['MV_D1'])
                self.d2 = map(str, self.converted_file[_controller_type]['D2'])
                self.mv_d2 = map(str, self.converted_file[_controller_type]['MV_D2'])
                self.d4 = map(str, self.converted_file[_controller_type]['D4'])
                self.dd = map(str, self.converted_file[_controller_type]['DD'])
                self.mv = map(str, self.converted_file[_controller_type]['MV']['serial'])
                try:    # Some configurations don't have pumps.  Just keep going on those
                    self.pm_d1 = map(str, self.converted_file[_controller_type]['PM_D1'])
                    self.pm = map(str, self.converted_file[_controller_type]['PM']['serial'])
                    self.pm_d2 = map(str, self.converted_file[_controller_type]['PM_D2'])
                except KeyError as e:
                    pass
                self.ms = map(str, self.converted_file[_controller_type]['MS']['serial'])
                self.fm = map(str, self.converted_file[_controller_type]['FM']['serial'])
                self.ts = map(str, self.converted_file[_controller_type]['TS']['serial'])
                self.sw = map(str, self.converted_file[_controller_type]['SW']['serial'])
                try:
                    self.ps = map(str, self.converted_file[_controller_type]['PS']['serial'])
                except KeyError as e:
                    pass

                # Get all ranges for each device
                # in the json file this is looking at the
                # Example "ZN": {"range": [1, 150, 187, 200]},
                self.zn_ad_range = self.converted_file[_controller_type]['ZN']['range']
                self.ms_ad_range = self.converted_file[_controller_type]['MS']['range']
                self.fm_ad_range = self.converted_file[_controller_type]['FM']['range']
                self.mv_ad_range = self.converted_file[_controller_type]['MV']['range']
                try:        # Some configurations don't have pumps.  Just keep going on those
                    self.pm_ad_range = self.converted_file[_controller_type]['PM']['range']
                except KeyError as e:
                    pass
                self.ts_ad_range = self.converted_file[_controller_type]['TS']['range']
                self.sw_ad_range = self.converted_file[_controller_type]['SW']['range']
                try:    # Some configurations don't have pumps.  Just keep going on those
                    self.ps_ad_range = self.converted_file[_controller_type]['PS']['range']
                except KeyError as e:
                    pass

    #################################
    def is_configured_for_multiple_bl_3200s(self):
        """
        :return: Returns whether or not the user config and json config is
                 configured for multiple 3200s.
        :rtype: bool
        """
        json_config_for_multiple = False
        user_config_for_multiple = False
        
        controllers = self.user_conf.controllers
        
        try:
            
            # -----------------------
            # Check USER config file:
            # -----------------------
            
            bl_3200s = [cn for cn in controllers if cn['type'] == '32']
            if len(bl_3200s) >= 2:
                user_config_for_multiple = True
            
            # -----------------------
            # Check JSON config file:
            # -----------------------
            
            # Get the JSON contents
            json_config = JSON_Handler.get_data(self.configuration_file_path)
            
            # Get the "32" JSON object (will be a list or an object)
            #   -> A list signifies multiple 3200s (will check for length)
            #   -> A object signifies single 3200
            bl_3200_json_obj = json_config[opcodes.basestation_3200]
            
            # If the BL32 JSON object is a list with multiple objects,
            # return True
            if type(bl_3200_json_obj) is list and len(bl_3200_json_obj) >= 2:
                json_config_for_multiple = True
    
        # Handle File I/O exception
        except IOError as e:
            e_msg = ("Unable to open JSON configuration file: {0}.\n"
                     "Exception: {1}.".format(
                         self.configuration_file_path,
                         e.message
                     ))
            raise IOError(e_msg)
        
        # Handle Key Error exception (key not existing in dict)
        except KeyError as e:
            e_msg = ("JSON configuration file is missing a BL 3200 JSON object "
                     "definition: {0}.\nException: {1}.".format(
                        self.configuration_file_path,
                        e.message
                     ))
            raise IOError(e_msg)
        
        else:
            return json_config_for_multiple and user_config_for_multiple

    #################################
    def build_list_of_device_types_present_in_json(self):
        list_of_device_types_present = []
        if not self.list_is_empty(self.zn_ad_range):
            list_of_device_types_present.append(opcodes.zone)
        if not self.list_is_empty(self.ms_ad_range):
            list_of_device_types_present.append(opcodes.moisture_sensor)
        if not self.list_is_empty(self.fm_ad_range):
            list_of_device_types_present.append(opcodes.flow_meter)
        if not self.list_is_empty(self.mv_ad_range):
            list_of_device_types_present.append(opcodes.master_valve)
        if not self.list_is_empty(self.pm_ad_range):
            list_of_device_types_present.append(opcodes.pump)
        if not self.list_is_empty(self.ts_ad_range):
            list_of_device_types_present.append(opcodes.temperature_sensor)
        if not self.list_is_empty(self.sw_ad_range):
            list_of_device_types_present.append(opcodes.event_switch)
        if not self.list_is_empty(self.ps_ad_range):
            list_of_device_types_present.append(opcodes.pressure_sensor)
        return list_of_device_types_present

    #################################
    def list_is_empty(self,_list):
        """
        this just checks to see if anything is present in the list
        :param _list:
        :type _list:  list\n
        :return:
        :rtype:
        """
        return len(_list) == 0

    #################################
    def load_eto_and_rain_fall_values(self):
        # Open the .json file, at this point the .json file is un-readable by our python script.
        config_file = open(self.configuration_file_path)
        # Convert the .json file into a Python readable 'Object' so we can access the information stored.
        converted_file = json.load(config_file)
        foo = converted_file["Daily_ETO_and_rain_fall_values"]
        r = range(0, len(foo))
        for index in r:
            self.daily_eto_value.append(foo[index]['eto'])
            self.daily_rainfall_value.append(foo[index]['rain_fall'])

    #################################
    def verify_full_configuration(self):
        """
        This goes through each object in the list and sorts them by address
        then it verifies all attributes of the object against what the controller thinks
        :return:
        :rtype:
        """
        list_of_object_pointers = []
        for controller3200 in self.BaseStation3200.values():
            controller3200.verify_full_configuration()

        for controller1000 in self.BaseStation1000.values():
            controller1000.verify_full_configuration()

        for flowstation in self.FlowStations.values():
            pass
        #     TODO add verify_full_configuration to bl_fs
        #     list_of_object_pointers = [
        #         flowstation,
        #         # flowstation.zones,
        #         # flowstation.moisture_sensors,
        #         # flowstation.master_valves,
        #         # flowstation.temperature_sensors,
        #         # # This should have been fixed in ZZ-522 bug
        #         # flowstation.flow_meters,
        #         # flowstation.event_switches,
        #         # flowstation.programs,
        #         # flowstation.zone_programs,
        #         # flowstation.poc,
        #         # flowstation.program_start_conditions,
        #         # flowstation.program_stop_conditions,
        #         # flowstation.program_pause_conditions,
        #         # flowstation.alert_relays
        #     ]
        # self.verify_objects_on_cn(list_of_obj_pointers=list_of_object_pointers)

        for substation in self.SubStations.values():
            substation.verify_full_configuration()

    #################################
    def check_use_of_alternate_ip_address(self):
        """
        Checks the passed in user configuration if the user specified that they would like to use an alternate IP
        address instead of connecting to BaseManager through DNS lookup. If so, then connect 3200 to the fixed IP
        address.
        """
        if str(self.user_conf.use_alternate_ip).upper() == opcodes.true:
            if str(self.user_conf.alternate_ip).upper() != "NONE":
                self.BaseStation3200[1].basemanager_connection[1].set_ai_for_cn(ip_address=self.user_conf.alternate_ip)
            else:
                self.BaseStation3200[1].basemanager_connection[1].set_ai_for_cn(ip_address=self.user_conf.url)
        elif str(self.user_conf.use_alternate_ip).upper() == opcodes.false:
            self.BaseStation3200[1].basemanager_connection[1].ua = opcodes.false
            self.BaseStation3200[1].basemanager_connection[1].set_default_values()

    #################################
    def share_sb_device_with_cn(self, _device_type, _device_serial, _address, _controller_3200_number=1, _substation_number=1):
        """
        Share a Substation device with the Controller being tested.
        
        Operations:
        1) Creates a new instance of the device and adds it to the respecitve object dictionary.
        2) Addresses and sets default values on the controller.
        
        :param _con: Type of device to share. (i.e., "FM" | "ZN" | "MV" | "SW" | "TS" | "MS" | "AN" | "PM")
        :type _device_type: str

        :param _device_type: Type of device to share. (i.e., "FM" | "ZN" | "MV" | "SW" | "TS" | "MS" | "AN" | "PM")
        :type device_type: str
        
        :param _device_serial: Serial number of device to share.
        :type _device_serial: str
        
        :param _address: Address to set for device.
        :type _address: int
        
        :param _controller_3200_number: Address of Substation to share device for.
        :type _controller_3200_number:  int

        :param _substation_number: Address of Substation to share device for.
        :type _substation_number: int
        """
        warnings.warn("1000 not yet finished.", PendingDeprecationWarning)
        pass
