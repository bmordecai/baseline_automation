__author__ = 'Brazil "Lucas Magnum" Guy & Sterling "Coding Lord" Tolley'

import logging as lg
import platform
from time import *

from selenium.webdriver.support.events import AbstractEventListener

# set up driver logger with file "log2.txt" in write mode
# logging.basicConfig(filename='log2.txt', filemode='w', level=logging.INFO

logging = lg.getLogger("LISTENER")


class MyListener(AbstractEventListener):
    """
    Implementation of the AbstractEventListener allowing event logging
    Events are logged in such a way as to display the best available identifier (ID->Name->Text)
    """

    FINISHED = 1

    def __init__(self, browser):
        self.element_clicked = ""
        self.bTime = 0
        self.ticker = 0
        self.browser = browser

    def before_navigate_to(self, url, driver):
        """
        Called by the event listener prior to opening a url
        """
        self.tickerTimer()
        mess = "Before opening the page %(url)s" % {"url": url}
        logging.debug(mess)
        MyListener.FINISHED = 0

    def after_navigate_to(self, url, driver):
        """
        Called by the event listener after opening a url
        """
        self.tickerTimer()
        mess = "%(url)s has been loaded!" % {"url": url}
        logging.debug(mess)
        MyListener.FINISHED = 1

    def before_click(self, element, driver):
        """
        Called before clicking an element, starts timing the length of the click
        """
        self.tickerTimer()
        mess = "Before clicking the element [%(el)s]" % {"el": self.getUniqueID(element)}
        logging.debug(mess)
        self.bTime = time()
        self.element_clicked = self.getUniqueID(element)
        MyListener.FINISHED = 0

    def after_click(self, element, driver):
        """
        Called after clicking an element, displays element clicked and the time it took
        """
        self.tickerTimer()
        mess = "After clicking the element [%(el)s] (%(tm)s)" % {"el": self.element_clicked,
                                                                 "tm": str(time() - self.bTime)+"s"}
        logging.debug(mess)
        MyListener.FINISHED = 1

    def before_change_value_of(self, element, driver):
        """
        Documents that an element's text value is about to change
        """
        self.tickerTimer()
        mess = "Before typing the text onto [%(el)s]" % {"el": self.getUniqueID(element)}
        logging.debug(mess)
        self.element_clicked = self.getUniqueID(element)
        MyListener.FINISHED = 0

    def after_change_value_of(self, element, driver):
        """
        Documents that an elements value has been changed and what it was changed to
        """
        self.tickerTimer()
        mess = "%(el)s: %(val)s" % {"el": self.getUniqueID(element), "val": element.get_attribute('value')}
        logging.debug(mess)
        MyListener.FINISHED = 1

    def before_close(self, driver):
        """
        Documents that the page is about to be closed
        """
        self.tickerTimer()
        logging.debug("before closing the page")
        MyListener.FINISHED = 0

    def after_close(self, driver):
        """
        Documents that the page has been closed
        """
        self.tickerTimer()
        logging.debug("After closing the page")
        MyListener.FINISHED = 1

    def on_exception(self, exception, driver):
        """
        Called when there is an error
        """
        logging.debug("Error")

    def tickerTimer(self):
        """
        Increments a variable that determines if the test information should be redisplayed
        :return:
        """
        if self.ticker % 50 == 0:
            logging.debug("Just a reminder you're running " + self.browser + " on " + str(platform.system()))
        self.ticker += 1

    def getUniqueID(self, element):
        """
        Returns an identifying attribute of an element
        :param element: Element being identified
        :return: str The best choice in identifying an element
        """
        if element.get_attribute("id") != "":
            return "#" + element.get_attribute("id")
        elif element.get_attribute("name") != "":
            return "name=" + element.get_attribute("name")
        else:
            return "'"+element.text+"'"