from common.objects.base_classes.ser import Ser
from common.objects.base_classes.web_driver import WebDriver
# from common.objects.base_classes.server_connections import ServerConnection

__author__ = 'baseline'


class ResourceHandler(object):
    """
    This class provides a location for all communication outlets to be initialized and/or closed.

    When a new 'base_class' is created and requires either a serial or web_driver communication resource:
    -   First, the new base class needs to be imported at the top.

    -   Second, if the base class requires controller type knowledge, the following needs to be added at #1 below:
        'Base_Class_Name.controller_type = controller_type' \n

    -   Lastly, if the base class requires serial and/or webdriver communication outlets, the following needs to be
        added at # 2 below: \n
        'Base_Class_Name.ser = self.ser' \n
        'Base_Class_Name.web_driver = self.web_driver' \n
    
    :type _user_conf: common.user_configuration.UserConfiguration
    :type web_driver: common.objects.base_classes.web_driver.WebDriver
    :type serial_connections: list[common.objects.base_classes.ser.Ser]
    """

    def __init__(self, user_conf, configuration=None):
        """
        Resource Handler initializer. Creates Serial and Browser instances for use across tests. \n

        :param user_conf:           User configuration instance created from 'product_assessments.py' \n
        :type user_conf:            common.user_configuration.UserConfiguration

        :param configuration:       Configuration instance created in the configuration.py. Needed for web tests. \n
        :type configuration:        common.configuration.Configuration \n
        """
        self._user_conf = user_conf
        self.web_driver = WebDriver(conf=self._user_conf, configuration=configuration)
        self.serial_connections = list()
        # self.server = ServerConnection(conf=self._user_conf)

    def create_serial_connection(self, comport, ethernet_port):
        """
        :param comport: Comport Number to Connect through.
        :type comport: str
        
        :param ethernet_port: Ethernet port to connect through.
        :type ethernet_port: str
        
        :return: 
        :rtype: common.objects.base_classes.ser.Ser
        """
        ser = Ser(comport=comport, eth_port=ethernet_port)
        self.serial_connections.append(ser)
        return ser

    def create_webdriver(self):
        """
        :return: 
        """
        self.web_driver = WebDriver(conf=self._user_conf)

    def release(self):
        try:
            # only close if an instance is present
            for serial_connection in self.serial_connections:
                if serial_connection.serial_conn is not None:
                    serial_connection.close_connection()

            # only close if an instance is present
            if self.web_driver:
                self.web_driver.teardown_driver()
        except Exception as e:
            print "Unable to close Serial and/or WebDriver connections"
            raise
        else:
            print "Successfully closed Serial and WebDriver connections"

    def restart_connections(self):
        try:
            self.release()

            # Re-open them
            self.create_webdriver()
        except:
            print "Failed trying to restart Serial and/or WebDriver connections"
            raise
        else:
            print "Successfully closed and reopened Serial and WebDriver connections."

    #
    # def create_server_connection(self,_controller):
    #     """
    #     """
    #     self.server = serve_handler(_controller=_controller)
