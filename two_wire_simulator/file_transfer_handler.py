import os
import zipfile
from email import base64mime
from common.imports import opcodes

__author__ = 'Tige'

"""
Using the with statement as seen below helps ensure the file is closed once we are at the end of the code
underneath the with statement.

updating firmware:

1. First: Reading the data from the file
    -   Open the file
    -   Load the data to a variable (for storage)
    -   Close the file

2. Second: Modify the read in data as needed

3. Third: Updating the file with new data
    -   Open the file
    -   Write the new data to the file from the variable it was stored in
    -   Close the file

Key File IO:
1. 'mode' - when we open a file, the mode tells it how to treat the file:
    a)  mode='a'    - Says open for writing, appending to the end of the current file
    b)  mode='w+'   - Open file, truncate the file (minimizes the file size temp) for updating (reading and writing)
    c)  mode='r'    - Open file for reading (default)
"""


def get_file_from_directory(_directory, _file_name, _file_type):
    """
    Get the data from the firmware update file with the file name passed in. \n
    :param _directory: Filename of firmware update file.  This should be relative to the root directory of the test \n
                       scripts.  Most likely, 'common/firmware_update_files' \n
    :type _directory: str \n
    :param _file_name: this is the name of the file inside the directory
    :type  _file_name: str \n
    :param _file_type: this the the extention to the file ".zip" ".txt"
    :type _file_type: str \n
    :return: the full path with the file name so the file can be opened \n
    :rtype: str \n
        in the configuration.py and the file location for the
        in know the file type of the firmware update is a .zip
        does it need to stay .zip
    :param data_zip_file=None:      Name of Update file containing firware for controller. \n
    :type data_zip_file=None:       zip

    directory = 'common/firmware_update_files'
    Added update_data_file to configuration.py
            Updates the json object with the specified file name with the new data passed in. The file name can also be a
    path to the location to store the file with the intended filename appended onto the end. \n

    Usage:
        let,
            _directory = "common/firmware_update_files_files/test.zip"
            data = {'lname': 'baseline'}

        the result,
            the file 'test.json' located in 'common/object_json_files' is opened, modified and then closed.
    """
    # This adds the extenstion to the file name
    file_name = _file_name + _file_type
    # this finds out what directory the file_transfer_handler is in on this specific computer
    dir_path = os.path.dirname(os.path.realpath(__file__))
    # This creates an absolute path by joining the absolute path to the testing root, the relative directory if the
    # firmware update files, and the firmware update file.  It should be OS-independent, so should work both in
    # linux and windows
    path_to_file = os.path.join(dir_path, _directory, file_name)
    # open the folder and verify the file is present
    # if not os.path.isdir(dir_path):
    #     print"bad"
    if not is_file_present(path_to_file):
        fail_e_msg = "File {0} is not present" .format(path_to_file)
        raise Exception(fail_e_msg)
    else:
        pass_e_msg = "File {0} was successfully found".format(path_to_file)
        print(pass_e_msg)
    return path_to_file


def open_file(_complete_path, _mode_type=None, _file_type=None, _file_in_zip=None,_file_type_in_zip=None):
    """
    pass in the complete file path with the name of the file and then read the contents  \n
    :param _complete_path: Complete file path with the file name a ttached \n
    :type _complete_path: str \n
    :param _mode_type :   reading or writing \n
        mode types for reading or writing
        'r'	Open a file for reading. (default) \n
        'w'	Open a file for writing. Creates a new file if it does not exist or \n
            truncates the file if it exists. \n
        'x'	Open a file for exclusive creation. If the file already exists, the operation fails. \n
    :type _mode_type: str \n
    :param _file_type: for file type \n
                        'a'	Open for appending at the end of the file without truncating it. \n
                            Creates a new file if it does not exist. \n
                        't'	Open in text mode. (default) \n
                        'b'	Open in binary mode. \n
                        '+'	Open a file for updating (reading and writing) \n
                        zip open zip file

    :type _file_type: str \n

    :return: full contents of file \n
    :rtype: bin \n
    """

    if _mode_type == 'reading':
        _mode_type = 'r'
    else:
        _mode_type = 'w'

    if _file_type == 'binary':
        _file_type ='b'
    else:
        _file_type = 't'

    file_mode_and_type = '{0}{1}'.format(_mode_type, _file_type)

    isinstance(file_mode_and_type, str)
    if file_mode_and_type is 'rb':
        print('file {0} opened for in {1} mode in {2} format'.format(_complete_path, _mode_type, _file_type))
    if _file_in_zip:
        opened_zip = zipfile.ZipFile(_complete_path)
        opened_file = opened_zip.open(_file_in_zip, mode=_mode_type)
    else:
        opened_file = open(_complete_path, file_mode_and_type)
    file_contents = opened_file.read()
    # close file after contents are saved to a variable
    opened_file.close()
    return file_contents


def is_file_present(_file_name):
    """
    Returns if the file is created or not
    :param _file_name: The file to check if it exists
    :type _file_name: str
    :return: True if the file exists, otherwise False
    :rtype: bool
    """
    if os.path.isfile(_file_name) and os.access(_file_name, os.R_OK):
        return True
    else:
        return False


def convert_data(_file_contents, _data_type, _maxlinelength):
    """
    pass in contents of a file
    check to see what the format is of the contents being passed in
    pass in a data type so you no what to convert the file too
    :param _file_contents:
    :type _file_contents:
    :param _data_type:
    :type _data_type:
    :param _maxlinelength:
    :type _maxlinelength:

    :return firmware_list_of_packets:
    :rtype: list:
    """
    # start with a blank list
    firmware_list_of_packets = []

    try:
        isinstance(_data_type, str)
    except TypeError:
        raise
    # this should verify that the file contents is binary
    try:
        isinstance(_file_contents, bytes)
    except TypeError:
        raise
    if _data_type is "base64":
        # append \ n to end of line length so it can be sent
        _firmware_image_base64_encode = base64mime.encode(_file_contents, maxlinelen=_maxlinelength, eol="\n")
        listofbase64lines = _firmware_image_base64_encode.splitlines()
        firmware_list_of_packets = listofbase64lines
    return firmware_list_of_packets


def convert_to_baseline_base64(_firmware_list_of_packets):
    """
    :param _firmware_list_of_packets:  the is a list of all the packets from the file
    :type _firmware_list_of_packets: \n
    :return _firmware_list_of_packets: \n
    :rtype: list \n
    """

    # Our base64 is a bit non-standard --
    #       make sure that "/" replaced by "-"
    #       and "="  replaced by "_"

    try:
        for index, packet in enumerate(_firmware_list_of_packets):
            _firmware_list_of_packets[index] = _firmware_list_of_packets[index]\
                .replace("/", "-")\
                .replace("=", "_")\
                .replace("\n", "_")
    except Exception, e:
        print("Failed to generate base64 list: Exception was: " + str(e))
    return _firmware_list_of_packets


def build_data_packet(_cn_type, _firmware_list_of_packets, _maxlinelen):
    _list_of_commands_to_send = []
    if _cn_type not in(opcodes.basestation_1000,opcodes.basestation_3200):
        e_msg = 'Controller type must be a {0} or a {1}'.format(
            opcodes.basestation_1000,
            opcodes.basestation_3200
        )
        raise Exception(e_msg)

    totalnumberinlist = len(_firmware_list_of_packets)-1
    # need to do a little difference with pack et 0 and last packet
    for index, packet in enumerate(_firmware_list_of_packets):
        num_bytes_in_packet = len(packet)
        _firmware_list_packet = _firmware_list_of_packets[index]

        command = '{0},{1}={2},{3}={4},'.format(
                    str(opcodes.set_action),  # {0}
                    str(opcodes.file_type),
                    str(opcodes.update_firmware),
                    str(opcodes.current_sequence_number),
                    int(index),  # This will be the index number from the list

        )
        # this would be the last packet need the last pack to have scence last = true
        if index == totalnumberinlist:
            command += '{0}={1},{2}={3},'.format(
                    str(opcodes.sequence_last),
                    str(opcodes.true),
                    str(opcodes.byte_length),
                    int(num_bytes_in_packet)
                    )
        else:
            command += '{0}={1},{2}={3},'.format(
                str(opcodes.sequence_last),
                str(opcodes.false),
                str(opcodes.byte_length),
                int(num_bytes_in_packet)
            )
        if _cn_type == str(opcodes.basestation_1000):
            command += '{0}={1}'.format(
                str(opcodes.bit_stream_1000),
                str(_firmware_list_packet)
                )
        elif _cn_type == str(opcodes.basestation_3200):
            command += '{0}={1}'.format(
                str(opcodes.bit_stream_3200),
                str(_firmware_list_packet)
                )
        _list_of_commands_to_send.append(command)

    return _list_of_commands_to_send


def packet_to_send_to_cn(_cn_type, _directory, _file_name, _file_type, _maxlinelen):
    """
    assemble the complete list of packets to send to the controller . \n

    :param _cn_type:        basestation 1000 = '10' basestation 3200 = '32'
    :type _cn_type:         str
    :param _directory:      Filename of firmware update file to open 'common/configuration_files' \n
    :type _directory:       str \n
    :param _file_name:      this is the name of the file inside the directory
    :type  _file_name:      str \n
    :param _file_type:      this the the extention to the file ".zip" ".txt"
    :type _file_type:       str \n
    :return command:
    :rtype: list:
    Packet:
        GET/ SET
        FL={type}
        File Type = {UP = update code, SET only}
        SN={sequence number, start at 0}
        SL={sequence last, TR or FA}
        LN={byte length of data field}
        BZ={3200 only, data byte stream, base64+ format of zip  same as BaseManager}
        BN={1000 only, data byte stream, base64+ format of binary  same as BaseManager)
        -----3200 Example-----
        Example:
        SET,FL=UP,SN=0,SL=FA,LN=1000,BZ={bytes}
        SET,FL=UP,SN=1,SL=FA,LN=1000,BZ={bytes}
            many packets
        SET,FL=UP,SN=2017,SL=TR,LN=228,BZ={bytes}
        Notes:
            - Each packet has an 'OK' if good, or an ER if out of sequence or length wrong
            - After last packet either an OK or ER
            - Controller reboots after receiving last packet if OK
    """

    complete_path = get_file_from_directory(_directory=_directory,
                                            _file_name=_file_name,
                                            _file_type=_file_type)
    actual_data = open_file(_complete_path=complete_path,
                            _mode_type='reading',
                            _file_type='binary')
    base_64_data = convert_data(_file_contents=actual_data,
                                _data_type='base64',
                                _maxlinelength=_maxlinelen)
    _firmware_list_of_packets = convert_to_baseline_base64(_firmware_list_of_packets=base_64_data)
    _list_of_commands_to_send = build_data_packet(_cn_type, _firmware_list_of_packets,
                                                  _maxlinelen)

    return _list_of_commands_to_send


def get_latest_firmware_version_available(_cn_type, _file_name=None):
    """
    This opens the main file in the directory and read the file called Versions

    :param _cn_type:        basestation 1000 = '10' basestation 3200 = '32'
    :type _cn_type:         str
    :param _directory:      Filename of firmware update file to open 'common/configuration_files' \n
    :type _directory:       str \n
    :param _file_name:      this is the name of the file inside the directory
    :type  _file_name:      str \n
    :param _file_type:      this the the extention to the file ".zip" ".txt"
    :type _file_type:       str \n

    :return _firmware_version:        16.0.547 \n
    :rtype: str:

    """
    if _cn_type is opcodes.basestation_3200:
        _directory = 'common' + os.sep + 'firmware_update_files'
        if _file_name:
            _file_name = _file_name
        else:
            _file_name = 'v16_latest'
        _file_type = ".zip"
        complete_path = get_file_from_directory(_directory=_directory,
                                                _file_name=_file_name,
                                                _file_type=_file_type)

        file_name = 'Update' + '/' + 'Version.txt'
        file_type = "t"
        actual_data = open_file(_complete_path=complete_path,
                                _mode_type='reading',
                                _file_type=file_type,
                                _file_in_zip=file_name)

        _firmware_version = str(actual_data).strip('\r\n')
        return _firmware_version
