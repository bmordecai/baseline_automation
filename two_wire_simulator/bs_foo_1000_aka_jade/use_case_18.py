import sys
from time import sleep
from datetime import datetime, timedelta
from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.logging_handler import log_handler
from common.imports import opcodes
from common import helper_methods

__author__ = 'Tige'


class ControllerUseCase18(object):
    """
    Test name:
        - CN UseCase 18 backup/restore
    purpose:
        - set up a full configuration on the controller
            - set up a full configuration on the controller
            - verify that no programming is lost when you:
                - do a backup to USB or BaseManager
            - verify that programming is loaded back when we do a restore.

    Coverage area: \n
        -setting up devices:
            - Loading \n
            - Searching \n
            - Addressing \n
                - Setting:
                    - descriptions
                    - locations \n

        - setting Programs: \n
            - start times
            - watering days
            - water windows
            - zone concurrency
            - assign zones to Programs \n
            - give each zone a run time of 1 hour and 30 minutes \n
            - assign moisture sensors to a primary zone 1 and set to lower limit watering

        - setting up poc's: \n
            - enable POC \n
            - assign:
                - master valve
                - flow meter \n
                - target flow\n
                - main line \n
                - priority
                    - low
                    - medium
                    - high\n
            - set high flow limit
            - high flow shut down \n
            - set unscheduled flow limit
            - enable unscheduled flow shut down \n

            - disable
                - all one of each device type \n
                - program
                - mainline
                - poc

        - Backup the programming to BaseManager, clear the programming from the controller, and then restore it.
            - Verify that the same programming that was cleared was then restored on the controller.
        - Do the same backup/restore but use a USB as the destination instead.
    Date References:
        - configuration for script is located common\configuration_files\back_restore_firmware_update.json
        - the devices and addresses range is read from the .json file

    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """
        time_it_should_take_test_to_run = timedelta(minutes=15)  # this is minutes
        time_test_started = datetime.now()
        try:
            number_of_retries = 1
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.test_elapsed_time(_time_started=time_test_started,
                                                     _time_expected_to_run=time_it_should_take_test_to_run)
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        ############################
        Setup Programs
        ############################
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            # this is set in the PG3200 object
            program_number_1_water_windows = ['011111100001111111111110']
            program_number_3_water_windows = ['011111100001111111111110']
            program_number_4_water_windows = ['011111100001111111111110']
            program_number_20_water_windows = ['011111100000111111111110']

            self.config.BaseStation1000[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation1000[1].programs[1].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation1000[1].programs[1].set_daily_water_window(_ww=program_number_1_water_windows)
            self.config.BaseStation1000[1].programs[1].set_seasonal_adjust(_percent=100)
            self.config.BaseStation1000[1].programs[1].add_master_valve(_mv_address=1)
            self.config.BaseStation1000[1].programs[1].set_soak_cycle_mode_disabled()

            self.config.BaseStation1000[1].add_program_to_controller(_program_address=3)
            self.config.BaseStation1000[1].programs[3].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation1000[1].programs[3].set_daily_water_window(_ww=program_number_3_water_windows)
            self.config.BaseStation1000[1].programs[3].set_seasonal_adjust(_percent=100)
            self.config.BaseStation1000[1].programs[3].add_master_valve(_mv_address=1)
            self.config.BaseStation1000[1].programs[3].set_soak_cycle_mode_disabled()

            self.config.BaseStation1000[1].add_program_to_controller(_program_address=4)
            self.config.BaseStation1000[1].programs[4].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation1000[1].programs[4].set_daily_water_window(_ww=program_number_4_water_windows)
            self.config.BaseStation1000[1].programs[4].set_seasonal_adjust(_percent=100)
            self.config.BaseStation1000[1].programs[4].add_master_valve(_mv_address=1)
            self.config.BaseStation1000[1].programs[4].set_soak_cycle_mode_disabled()

            self.config.BaseStation1000[1].add_program_to_controller(_program_address=20)
            self.config.BaseStation1000[1].programs[20].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation1000[1].programs[20].set_daily_water_window(_ww=program_number_20_water_windows)
            self.config.BaseStation1000[1].programs[20].set_seasonal_adjust(_percent=100)
            self.config.BaseStation1000[1].programs[20].add_master_valve(_mv_address=1)
            self.config.BaseStation1000[1].programs[20].set_soak_cycle_mode_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_4(self):
        """
        ############################
        Setup Zone Programs
        ############################
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation1000[1].programs[1].zone_programs[1].set_run_time(_minutes=15)

            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation1000[1].programs[1].zone_programs[2].set_run_time(_minutes=15)

            self.config.BaseStation1000[1].programs[3].add_zone_to_program(_zone_address=49)
            self.config.BaseStation1000[1].programs[3].zone_programs[49].set_run_time(_minutes=20)

            self.config.BaseStation1000[1].programs[3].add_zone_to_program(_zone_address=50)
            self.config.BaseStation1000[1].programs[3].zone_programs[50].set_run_time(_minutes=20)

            self.config.BaseStation1000[1].programs[4].add_zone_to_program(_zone_address=50)
            self.config.BaseStation1000[1].programs[4].zone_programs[50].set_run_time(_minutes=20)

            self.config.BaseStation1000[1].programs[20].add_zone_to_program(_zone_address=99)
            self.config.BaseStation1000[1].programs[20].zone_programs[99].set_run_time(_minutes=33)

            self.config.BaseStation1000[1].programs[20].add_zone_to_program(_zone_address=96)
            self.config.BaseStation1000[1].programs[20].zone_programs[96].set_run_time(_minutes=33)

            self.config.BaseStation1000[1].programs[20].add_zone_to_program(_zone_address=97)
            self.config.BaseStation1000[1].programs[20].zone_programs[97].set_run_time(_minutes=33)

            self.config.BaseStation1000[1].programs[20].add_zone_to_program(_zone_address=98)
            self.config.BaseStation1000[1].programs[20].zone_programs[98].set_run_time(_minutes=33)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_5(self):
        """
        ############################
        Setup Points of Connection
        ############################
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].add_poc(_poc_address=1)
            self.config.BaseStation1000[1].points_of_control[1].add_flow_meter_to_point_of_control(
                _flow_meter_address=1)
            self.config.BaseStation1000[1].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=1)
            self.config.BaseStation1000[1].points_of_control[1].set_target_flow(_gpm=500)
            self.config.BaseStation1000[1].points_of_control[1].set_high_flow_limit(_limit=550,
                                                                                    with_shutdown_enabled=True)
            self.config.BaseStation1000[1].points_of_control[1].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=True)

            self.config.BaseStation1000[1].add_poc(_poc_address=3)
            self.config.BaseStation1000[1].points_of_control[3].add_flow_meter_to_point_of_control(
                _flow_meter_address=1)
            self.config.BaseStation1000[1].points_of_control[3].add_master_valve_to_point_of_control(
                _master_valve_address=1)
            self.config.BaseStation1000[1].points_of_control[3].set_target_flow(_gpm=50)
            self.config.BaseStation1000[1].points_of_control[3].set_high_flow_limit(_limit=75,
                                                                                    with_shutdown_enabled=False)
            self.config.BaseStation1000[1].points_of_control[3].set_unscheduled_flow_limit(_gallons=5,
                                                                                           with_shutdown_enabled=False)

            self.config.BaseStation1000[1].programs[1].add_point_of_connection(_poc_address=1)
            self.config.BaseStation1000[1].programs[3].add_point_of_connection(_poc_address=1)
            self.config.BaseStation1000[1].programs[4].add_point_of_connection(_poc_address=3)
            self.config.BaseStation1000[1].programs[20].add_point_of_connection(_poc_address=3)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_6(self):
        """
        Disable devices
        This area covers not losing programing when a device is disabled during a firmware update or reboot
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            # disable Zone (97}
            self.config.BaseStation1000[1].zones[97].set_disabled()

            # disable flow meter {1}
            self.config.BaseStation1000[1].flow_meters[1].set_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_7(self):
        """
        Send backup to BaseManager. This takes all of our programming and creates a backup on BaseManager that we will
        later use in the test to do a restore.
        :return:
        :rtype:
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            # start clock
            self.config.BaseStation1000[1].start_clock()
            self.config.BaseStation1000[1].do_backup_programming(where_to=opcodes.basemanager)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_8(self):
        """
        Clears all the programming from the controller so we can do a restore from a clean state.
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            # clear all programming
            self.config.BaseStation1000[1].clear_all_devices()
            self.config.BaseStation1000[1].clear_all_programming()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_9(self):
        """
        We restore our programming from BaseManager. We give it a file number to request from BaseManager.
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].basemanager_connection[1].wait_for_bm_connection()
            sleep(10)
            self.config.BaseStation1000[1].do_restore_programming(opcodes.basemanager)

            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].bicoder.self_test_and_update_object_attributes()
            for moisture_sensors in self.config.ms_ad_range:
                self.config.BaseStation1000[1].moisture_sensors[moisture_sensors].bicoder.self_test_and_update_object_attributes()
            for master_valves in self.config.mv_ad_range:
                self.config.BaseStation1000[1].master_valves[master_valves].bicoder.self_test_and_update_object_attributes()
            for flow_meters in self.config.fm_ad_range:
                self.config.BaseStation1000[1].flow_meters[flow_meters].bicoder.self_test_and_update_object_attributes()
            for event_switches in self.config.sw_ad_range:
                self.config.BaseStation1000[1].event_switches[event_switches].bicoder.self_test_and_update_object_attributes()
            for temperature_sensors in self.config.ts_ad_range:
                self.config.BaseStation1000[1].temperature_sensors[temperature_sensors].bicoder.self_test_and_update_object_attributes()

            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_10(self):
        """
        The same thing as before but this time we do the backup to a thumb drive. We will use this backup to do a
        restore later on.
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            # start clock
            self.config.BaseStation1000[1].start_clock()
            self.config.BaseStation1000[1].do_backup_programming(where_to=opcodes.usb_flash_storage)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_11(self):
        """
        Clears all the programming from the controller so we can do a restore from a clean state.
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            # clear all programming
            self.config.BaseStation1000[1].initialize_for_test()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_12(self):
        """
        Clear the controller of all of its programming and devices.
        We then restore the programming using a backup from the thumb drive.
        Then verify that all the values that were left in our objects are now back on the controller.
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            # restore the programming to the controller
            self.config.BaseStation1000[1].do_restore_programming(opcodes.usb_flash_storage, file_number=1)

            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].bicoder.self_test_and_update_object_attributes()
            for moisture_sensors in self.config.ms_ad_range:
                self.config.BaseStation1000[1].moisture_sensors[moisture_sensors].bicoder.self_test_and_update_object_attributes()
            for master_valves in self.config.mv_ad_range:
                self.config.BaseStation1000[1].master_valves[master_valves].bicoder.self_test_and_update_object_attributes()
            for flow_meters in self.config.fm_ad_range:
                self.config.BaseStation1000[1].flow_meters[flow_meters].bicoder.self_test_and_update_object_attributes()
            for event_switches in self.config.sw_ad_range:
                self.config.BaseStation1000[1].event_switches[event_switches].bicoder.self_test_and_update_object_attributes()
            for temperature_sensors in self.config.ts_ad_range:
                self.config.BaseStation1000[1].temperature_sensors[temperature_sensors].bicoder.self_test_and_update_object_attributes()

            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]



