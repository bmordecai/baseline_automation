import sys
from time import sleep
from datetime import datetime, timedelta
from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.logging_handler import log_handler
from common import helper_methods

__author__ = "Eldin"


class ControllerUseCase9(object):
    """
    Test name: \n
        MV Pump Test \n
    Purpose: \n
        This test assigns a zone to one or more Programs and then sets start and run times. It then runs the controller
        and verifies that all the zones are running at the correct time and follow the concurrent zone limits. \n
    Coverage area: \n
        1. Set up the controller and limit its concurrent zones \n
        2. Searching and assigning: \n
            - Zones \n
            - Master Valves \n
        3. Set up Programs and give them a start times and limit their concurrent zones \n
        4. Assign zones to Programs and give them run times \n
        5. Set up the POC objects and assign master valves to them \n
        6. Verify the full configuration \n
        7. Set the time to 3:45 A.M., start the controller, and then verify that all zones are responding \n
        8. Increment the time so it is 4:00 A.M. and verify that 3 Programs were activated, but only two zones should
           be running \n
        9. Increment by 10 minutes and verify that the zones that were watering at first are done and that new zones
           are watering \n
        10. Increment by another 10 minutes and verify that Programs 1 and 2 are done watering along with their zones.
            Program 15 should now be running instead of waiting \n
        11. Increment by 5 minutes so that program 15's first zone is done and verify that the second zone starts \n
        12. Increment by 5 minutes again so program 15's second zone is done and verify that everything is done \n
        13. Increment the time to 12:00 P.M., this makes program 32 start and both of it's zones should also start \n
        14. Increment by 20 minutes and verify that program 32 and both of it's zones are done watering \n
        15. Increment the time to 9:00 P.M., this should start up both program 15 and 23. Both limit their zone
            concurrency to 1, so only the first zone from each program should be running \n
        16. Increment by 5 minutes and verify that both of the first zones on program 15 and 23 are done and that the
            second zones are running \n
        17. Increment by another 5 minutes and verify that everything is done watering \n
        18. Verify the full configuration again \n
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """
        time_it_should_take_test_to_run = timedelta(minutes=15)  # this is minutes
        time_test_started = datetime.now()
        try:
            number_of_retries = 1
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.test_elapsed_time(_time_started=time_test_started,
                                                     _time_expected_to_run=time_it_should_take_test_to_run)
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_3(self):
        """
        ############################
        Setup Programs
        ############################
        - Give Programs 1, 2, 15, and 23 a max concurrent zone value of 1 and program 28 a value of 2 \n
        - Give Programs start times \n
        - Give program 28 a master valve address of 4, which is serial number TSE0012 \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].set_max_concurrent_zones(_max_zones=2)

            self.config.BaseStation1000[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation1000[1].programs[1].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation1000[1].programs[1].set_daily_water_window(['011111100001111111111110'])

            self.config.BaseStation1000[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation1000[1].programs[2].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation1000[1].programs[2].set_weekly_water_window(['011111100000111111111111',
                                                                                '000011000000000000011110',
                                                                                '111111100000000000011110',
                                                                                '111111100000000000011110',
                                                                                '111111100000000000011110',
                                                                                '111111100000000000011110',
                                                                                '111111100000000000011110'])

            self.config.BaseStation1000[1].add_program_to_controller(_program_address=15)
            self.config.BaseStation1000[1].programs[15].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation1000[1].programs[15].set_daily_water_window(['011111100001111111111110'])

            self.config.BaseStation1000[1].add_program_to_controller(_program_address=23)
            self.config.BaseStation1000[1].programs[23].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation1000[1].programs[23].set_daily_water_window(['011111100001111111111110'])

            self.config.BaseStation1000[1].add_program_to_controller(_program_address=28)
            self.config.BaseStation1000[1].programs[28].set_max_concurrent_zones(_number_of_zones=2)
            self.config.BaseStation1000[1].programs[28].set_daily_water_window(['011111100001111111111110'])
            self.config.BaseStation1000[1].programs[28].add_master_valve(_mv_address=7)

            # Set the start time conditions
            self.config.BaseStation1000[1].programs[1].add_date_time_start_condition(_condition_number=1)
            self.config.BaseStation1000[1].programs[1].date_time_start_conditions[1].set_day_time_start(_st_list=[240])

            self.config.BaseStation1000[1].programs[2].add_date_time_start_condition(_condition_number=1)
            self.config.BaseStation1000[1].programs[2].date_time_start_conditions[1].set_day_time_start(_st_list=[240])

            self.config.BaseStation1000[1].programs[15].add_date_time_start_condition(_condition_number=1)
            self.config.BaseStation1000[1].programs[15].date_time_start_conditions[1].set_day_time_start(
                _st_list=[240, 1260])

            self.config.BaseStation1000[1].programs[23].add_date_time_start_condition(_condition_number=1)
            self.config.BaseStation1000[1].programs[23].date_time_start_conditions[1].set_day_time_start(
                _st_list=[1260])

            self.config.BaseStation1000[1].programs[28].add_date_time_start_condition(_condition_number=1)
            self.config.BaseStation1000[1].programs[28].date_time_start_conditions[1].set_day_time_start(_st_list=[720])

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_4(self):
        """
        ############################
        Setup Zone Programs
        ############################
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation1000[1].programs[1].zone_programs[1].set_run_time(_minutes=10)
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation1000[1].programs[1].zone_programs[2].set_run_time(_minutes=10)

            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=3)
            self.config.BaseStation1000[1].programs[2].zone_programs[3].set_run_time(_minutes=10)
            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=4)
            self.config.BaseStation1000[1].programs[2].zone_programs[4].set_run_time(_minutes=10)

            self.config.BaseStation1000[1].programs[15].add_zone_to_program(_zone_address=1)
            self.config.BaseStation1000[1].programs[15].zone_programs[1].set_run_time(_minutes=5)
            self.config.BaseStation1000[1].programs[15].add_zone_to_program(_zone_address=2)
            self.config.BaseStation1000[1].programs[15].zone_programs[2].set_run_time(_minutes=5)

            self.config.BaseStation1000[1].programs[23].add_zone_to_program(_zone_address=3)
            self.config.BaseStation1000[1].programs[23].zone_programs[3].set_run_time(_minutes=5)
            self.config.BaseStation1000[1].programs[23].add_zone_to_program(_zone_address=4)
            self.config.BaseStation1000[1].programs[23].zone_programs[4].set_run_time(_minutes=5)

            self.config.BaseStation1000[1].programs[28].add_zone_to_program(_zone_address=5)
            self.config.BaseStation1000[1].programs[28].zone_programs[5].set_run_time(_minutes=20)
            self.config.BaseStation1000[1].programs[28].add_zone_to_program(_zone_address=51)
            self.config.BaseStation1000[1].programs[28].zone_programs[51].set_run_time(_minutes=20)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_5(self):
        """
        Set up the water sources \n
        assign master valves to pocs and enable them \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].add_poc(_poc_address=1)
            self.config.BaseStation1000[1].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=4)

            self.config.BaseStation1000[1].add_poc(_poc_address=3)
            self.config.BaseStation1000[1].points_of_control[3].add_master_valve_to_point_of_control(
                _master_valve_address=7)

            self.config.BaseStation1000[1].programs[1].add_point_of_connection(_poc_address=1)
            self.config.BaseStation1000[1].programs[2].add_point_of_connection(_poc_address=3)
            self.config.BaseStation1000[1].programs[15].add_point_of_connection(_poc_address=3)
            self.config.BaseStation1000[1].programs[23].add_point_of_connection(_poc_address=1)
            self.config.BaseStation1000[1].programs[28].add_point_of_connection(_poc_address=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_6(self):
        """
        Verify full Configuration \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_7(self):
        """
        Set the date and time on the controller \n
        Start the controller and verify all the zones and Programs are working \n
        set the clock to 04/07/2014 3:45 am \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
        - Zone status: \n
            - zone 52 done watering \n
            - zone 53 done watering \n
            - zone 54 done watering \n
            - zone 55 done watering \n
            - zone 75 done watering \n
            - zone 76 done watering \n
            - zone 78 done watering \n
            - zone 79 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].set_date_and_time(_date="04/07/2014", _time="03:45:00")
            self.config.BaseStation1000[1].set_controller_to_run()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[15].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[23].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[28].statuses.verify_status_is_done()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_8(self):
        """
        Set the date and time on the controller \n
        Increment the clock by 15 minutes to put the time at 04/07/2014 4:00 A.M. \n
        At 4:00 A.M. Programs 1, 2, and 15 all start. However, each program only allows 1 concurrent zone, and the
         controller only allows 2 zones. So only the first zone of program 1 and 2 will be running. \n
        Start the controller and verify all the zones and Programs are working \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
            - program 2 running \n
            - program 15 waiting to water \n
            - program 23 done watering \n
            - program 28 done watering \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 done watering \n
            - zone 51 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=15)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[15].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].programs[23].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[28].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[51].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_9(self):
        """
        Set the date and time on the controller \n
        Increment the clock by 10 minutes to put the time at 04/07/2014  4:10 A.M. \n
        Programs 1 and 2 have a runtime of 600 seconds (10 minutes) so their first zones should be done watering \n
        At 4:10 A.M. Programs 1, 2, and 15 are running their second zones. There should still only be two zones running,
         but zone 1 should have finished running. However, since zone 1 is also attached to program 15, it should be
         waiting to water. \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
            - program 2 running \n
            - program 15 waiting to water \n
            - program 23 done watering \n
            - program 28 done watering \n
        - Zone status: \n
            - zone 1 waiting to water \n
            - zone 2 watering \n
            - zone 3 done watering \n
            - zone 4 watering \n
            - zone 5 done watering \n
            - zone 51 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[15].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].programs[23].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[28].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[51].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_10(self):
        """
        Set the date and time on the controller \n
        Increment the clock by 10 minutes to put the time at 04/07/2014 4:20 A.M. \n
        Programs 1 and 2 have a runtime of 600 seconds (10 minutes) so their second zones should be done watering \n
        At 4:20 A.M. Programs 1 and 2 should be done watering since they both had two zones attached and both should
         have run for 10 minutes. Program 15 is the only other program that started at 4:00 A.M. so it should be the
         only program running. It allows only 1 concurrent zone so only zone 1 should be running with a runtime of 300
         seconds (5 minutes). \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
            - program 15 watering \n
            - program 23 done watering \n
            - program 28 done watering \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
            - zone 51 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[15].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[23].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[28].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[51].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_11(self):
        """
        Set the date and time on the controller \n
        Increment the clock by 5 minutes to put the time at 04/07/2014  4:25 A.M. \n
        Program 15 has a runtime of 300 seconds (5 minutes), so now it's first zone should be done running. \n
        At 4:25 A.M. program 15 should be the only program left running. Since the clock was incremented 5 minutes,
         zone 1 should be done watering again, and zone 2 should start up again and run for 5 minutes. \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
            - program 15 watering \n
            - program 23 done watering \n
            - program 28 done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
            - zone 51 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[15].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[23].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[28].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[51].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_12(self):
        """
        Set the date and time on the controller \n
        Increment the clock by 5 minutes to put the time at 04/07/2014 4:30 A.M. \n
        Program 15 has a runtime of 300 seconds (5 minutes), so now it's second zone should be done running. \n
        At 4:30 A.M. program 15 should be done watering. Both of it's attached zones had a runtime of 5 minutes, and
         now both of them are done. \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
            - program 15 done watering \n
            - program 23 done watering \n
            - program 28 done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
            - zone 51 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[15].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[23].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[28].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[51].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_13(self):
        """
        Set the date and time on the controller \n
        Increment the clock by 7 hours and 30 minutes to put the time at 04/07/2014 12:00 P.M. \n
        Program 28 is the only program with a start time at 12:00 P.M. and it can run two concurrent zones. \n
        At 12:00 P.M. program 28 should be the only program running. Both of it's zones have a runtime of 20 minutes
         and both of them should be running since the controller and program 28 both have a two concurrent zone limit \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
            - program 15 done watering \n
            - program 23 done watering \n
            - program 28 running \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 watering \n
            - zone 51 watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=7, minutes=30)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[15].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[23].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[28].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[51].statuses.verify_status_is_watering()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_14(self):
        """
        Set the date and time on the controller \n
        Increment the clock by 20 minutes to put the time at 04/07/2014 12:20 P.M. \n
        Program 28's zones have a runtime of 20 minutes, so they should both be done watering \n
        At 12:20 P.M. program 28 should be done watering. Both of it's zones should also be done after 20 minutes \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
            - program 15 done watering \n
            - program 23 done watering \n
            - program 28 done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
            - zone 51 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=20)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[15].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[23].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[28].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[51].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_15(self):
        """
        Set the date and time on the controller \n
        Increment the clock by 8 hours and 40 minutes to put the time at 04/07/2014 9:00 P.M. \n
        Program 15 had two run times and one of them was 9:00 P.M., so zones 1 and 2 should be watering soon \n
        Program 23 has a run time of 9:00, so zones , so zones 3 and 4 should be watering soon \n
        At 9:00 P.M. Programs 15 and 28 both have a concurrent zone limit of 1. This means that only the first zones
         attached to each program should be running \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
            - program 15 running \n
            - program 23 running \n
            - program 28 done watering \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 done watering \n
            - zone 51 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=8, minutes=40)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[15].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[23].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[28].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[51].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_16(self):
        """
        Set the date and time on the controller \n
        Increment the clock by 5 minutes to put the time at 04/07/2014 9:05 P.M. \n
        At 9:05 P.M. both program 15's and 23's zones have a run time of 5 minutes. After the increment of 5 minutes,
         the first zones should be done and the second zones on each program should be watering \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
            - program 15 running \n
            - program 23 running \n
            - program 28 done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 watering \n
            - zone 3 done watering \n
            - zone 4 watering \n
            - zone 5 done watering \n
            - zone 51 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[15].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[23].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[28].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[51].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_17(self):
        """
        Set the date and time on the controller \n
        Increment the clock by 5 minutes to put the time at 04/07/2014 9:10 P.M. \n
        At 9:10 P.M. both program 15's and 23's zones have run for 5 minutes, which means that everything should be
         done \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
            - program 15 done watering \n
            - program 23 done watering \n
            - program 28 done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
            - zone 51 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()


            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[15].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[23].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[28].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[51].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_18(self):
        """
        Verify full Configuration \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
