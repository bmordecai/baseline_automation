import sys
from time import sleep
import time

# import log_handler for logging functionality
from common.logging_handler import log_handler

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr

from common.configuration import Configuration
from common.imports import opcodes

from common import helper_methods

__author__ = 'Eldin'


class ControllerUseCase4(object):
    """
    Test name: \n
        Concurrent Zones Test \n
    Purpose: \n
        This test covers zone concurrency. It verifies that max controller concurrency and program concurrency dictate
        the number of zones running at a given time \n
    Coverage area: \n
        1. Setting up the controller and limiting the concurrent zones to 1 \n
        2. Searching and assigning: \n
            - Zones \n
        3. Set up the program and assign it's concurrent zones \n
        4. Assign all zones to a program \n
        5. Verify the full configuration \n
        6. Verify that all zones and Programs have a done watering status before Programs are started \n
        7. Verify that only the first zone is running after the program starts due to the controller having a 1 zone
           concurrent limit \n
        8. Change the Controller max zone concurrency to 7 and make sure that the zone's running are only restricted by
           the program's concurrency \n
        9. the program's concurrency \n
        10.Verify the full configuration again \n
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.run_use_case()

    def run_use_case(self):
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]

                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise

        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        ############################
        Setup Programs
        ############################
        - Set the controller max concurrency for 1
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set the controller zone concurrency to 1
            self.config.BaseStation1000[1].set_max_concurrent_zones(_max_zones=1)

            # Program 1
            self.config.BaseStation1000[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation1000[1].programs[1].set_max_concurrent_zones(_number_of_zones=3)

            # Program 2
            self.config.BaseStation1000[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation1000[1].programs[2].set_max_concurrent_zones(_number_of_zones=3)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_2(self):
        """
        ############################
        Setup Zone Programs
        ############################
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation1000[1].programs[1].zone_programs[1].set_run_time(_minutes=60)

            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation1000[1].programs[1].zone_programs[2].set_run_time(_minutes=60)

            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation1000[1].programs[1].zone_programs[3].set_run_time(_minutes=60)

            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=4)
            self.config.BaseStation1000[1].programs[1].zone_programs[4].set_run_time(_minutes=60)

            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=5)
            self.config.BaseStation1000[1].programs[2].zone_programs[5].set_run_time(_minutes=60)

            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=51)
            self.config.BaseStation1000[1].programs[2].zone_programs[51].set_run_time(_minutes=60)

            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=53)
            self.config.BaseStation1000[1].programs[2].zone_programs[53].set_run_time(_minutes=60)

            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=78)
            self.config.BaseStation1000[1].programs[2].zone_programs[78].set_run_time(_minutes=60)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_3(self):
        """
        Verify full Configuration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Assign a design flow value to each zone so that they have a default setting
            self.config.verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_4(self):
        """
        Set the date of the controller so that there is a known days of the week and days of the month \n
        Verify that all zones are working properly by verifying the zones before starting the Programs \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
            - zone 51 done watering \n
            - zone 53 done watering \n
            - zone 78 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].set_date_and_time(_date="04/08/2014", _time="08:00:00")
            self.config.BaseStation1000[1].set_controller_to_run()

            # Verify nothing starts until we increment the clock
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_5(self):
        """
        Start Both Programs \n
        Only one zone should be running since the controller's zone concurrency is 1 \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
            - program 2 waiting to water \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 waiting to water \n
            - zone 51 waiting to water \n
            - zone 53 waiting to water \n
            - zone 78 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].set_program_to_start()
            self.config.BaseStation1000[1].programs[2].set_program_to_start()

            # Allows the controller to update the Zone status
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            # Verify the status of all zones and programs
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[51].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[53].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[78].statuses.verify_status_is_waiting_to_water()

        except Exception, e:
            raise Exception(method + " " + self.config.BaseStation1000[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_6(self):
        """
        Stop Both Programs \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 Done \n
            - program 2 Done \n
        - Zone status: \n
            - zone 1 Done \n
            - zone 2 Done \n
            - zone 3 Done \n
            - zone 4 Done  \n
            - zone 5 Done \n
            - zone 51 Done  \n
            - zone 53 Done  \n
            - zone 78 Done  \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            # Stop both programs from running
            self.config.BaseStation1000[1].programs[1].set_program_to_stop()
            self.config.BaseStation1000[1].programs[2].set_program_to_stop()
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            # Verify that the programs and all of their zones have stopped
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_done()
        except Exception, e:
            raise Exception(method + " " + self.config.BaseStation1000[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_7(self):
        """
        Change the max zone concurrency for the controller to 7 \n
        Because the controller can run 7 zones but each program can only run 3 zones the total zones running should be
        6 zones
        Start Programs 1 and 2 \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
            - program 2 running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 watering \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 watering \n
            - zone 51 watering \n
            - zone 53 watering \n
            - zone 78 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            # Update the max concurrent zones and start both programs
            self.config.BaseStation1000[1].set_max_concurrent_zones(_max_zones=7)
            self.config.BaseStation1000[1].programs[1].set_program_to_start()
            self.config.BaseStation1000[1].programs[2].set_program_to_start()

            # Give the controller time to update statuses
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            # This is necessary for our data in zone and program to update
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[51].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[53].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[78].statuses.verify_status_is_waiting_to_water()

        except Exception, e:
            raise Exception(method + " " + self.config.BaseStation1000[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_8(self):
        """
        Verify full Configuration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Assign a design flow value to each zone so that they have a default setting
            self.config.verify_full_configuration()

        except Exception, e:
            raise Exception(method + " " + self.config.BaseStation1000[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)