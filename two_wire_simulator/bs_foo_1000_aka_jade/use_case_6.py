import sys
from time import sleep
from datetime import datetime, timedelta
from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.logging_handler import log_handler
from common import helper_methods

__author__ = "Eldin"


class ControllerUseCase6(object):
    """
    Test name: \n
        Moisture Decoder Test \n
    Purpose: \n
        This test covers using start/stop/pause programming with moisture sensors. \n
    Coverage area: \n
    1. Set up the controller and limit its concurrent zones \n
    2. Searching and assigning: \n
        - Zones \n
        - Moisture sensors \n
    3. Set up Programs and give them a run time using commands \n
    4. Assign zones to Programs and give them run times \n
    5. Set up pause conditions for Programs 1 and 2 to pause when the respective moisture sensor reading is below 10 \n
    6. Verify the full configuration \n
    7-8. Run the controller to verify the zone concurrency is working as expected \n
    9-10. Verify that a moisture sensor can pause a program by setting the moisture percent on SB07270 to be 9, which \n
          is below program 1's lower limit \n
    11-13. Assign both Programs to moisture sensor SB07270 and setting a pause condition when the moisture percent is \n
           above 10 and 20 for Programs 1 and 2. Then set SB07270 to 21 to pause both Programs \n
    14-17. Assign both Programs to separate moisture sensors and set a condition to start both Programs when their \n
           respective moisture sensor reads below 10. Change the moisture percent to be under 10 one at a time and \n
           verify that each program starts individually \n
    18-20. Create a condition for program 2 to start when SB07270's moisture percent is below 10. Then set SB07270's \n
           moisture percent to be 9, therefore triggering program 1 and 2 to start \n
    21-24. Set conditions for program 1 and 2 to start when SB07270's and SB07263's moisture percents are below 10. \n
           Then set a condition to stop when the moisture percents are above 15. Run and verify the zones/Programs \n
    25-28. Set a condition for program 2 to start when SB07270's moisture percent is below 10 and to stop when it is \n
           above 15. Run the controller to verify the Programs and zones are running when the moisture percentage
    29. Verify full configuration again \n
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """
        time_it_should_take_test_to_run = timedelta(minutes=15)  # this is minutes
        time_test_started = datetime.now()
        try:
            number_of_retries = 1
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.test_elapsed_time(_time_started=time_test_started,
                                                     _time_expected_to_run=time_it_should_take_test_to_run)
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_3(self):
        """
        ############################
        Setup Programs
        ############################
        Setup a start condition at 8:00 AM
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].set_max_concurrent_zones(_max_zones=4)

            self.config.BaseStation1000[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation1000[1].programs[1].set_max_concurrent_zones(_number_of_zones=2)

            self.config.BaseStation1000[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation1000[1].programs[2].set_max_concurrent_zones(_number_of_zones=2)

            self.config.BaseStation1000[1].programs[1].add_date_time_start_condition(_condition_number=1)
            self.config.BaseStation1000[1].programs[1].date_time_start_conditions[1].set_day_time_start(_st_list=[480])

            self.config.BaseStation1000[1].programs[2].add_date_time_start_condition(_condition_number=1)
            self.config.BaseStation1000[1].programs[2].date_time_start_conditions[1].set_day_time_start(_st_list=[480])
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_4(self):
        """
        ############################
        Setup Zone Programs
        ############################
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation1000[1].programs[1].zone_programs[1].set_run_time(_minutes=45)
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation1000[1].programs[1].zone_programs[2].set_run_time(_minutes=45)
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation1000[1].programs[1].zone_programs[3].set_run_time(_minutes=45)
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=4)
            self.config.BaseStation1000[1].programs[1].zone_programs[4].set_run_time(_minutes=45)

            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=5)
            self.config.BaseStation1000[1].programs[2].zone_programs[5].set_run_time(_minutes=45)
            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=6)
            self.config.BaseStation1000[1].programs[2].zone_programs[6].set_run_time(_minutes=45)
            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=7)
            self.config.BaseStation1000[1].programs[2].zone_programs[7].set_run_time(_minutes=45)
            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=8)
            self.config.BaseStation1000[1].programs[2].zone_programs[8].set_run_time(_minutes=45)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_5(self):
        """
        Setup pause Programs to pause when the moisture percent goes below 10% for both program 1 and 2 \n
        Set the value read by each moisture sensor \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].add_moisture_pause_condition(_moisture_sensor_address=1)
            self.config.BaseStation1000[1].programs[1].moisture_pause_conditions[1].set_moisture_mode_to_lower_limit()
            self.config.BaseStation1000[1].programs[1].moisture_pause_conditions[1].set_moisture_threshold(_percent=10)

            self.config.BaseStation1000[1].programs[2].add_moisture_pause_condition(_moisture_sensor_address=2)
            self.config.BaseStation1000[1].programs[2].moisture_pause_conditions[2].set_moisture_mode_to_lower_limit()
            self.config.BaseStation1000[1].programs[2].moisture_pause_conditions[2].set_moisture_threshold(_percent=10)

            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=11)
            self.config.BaseStation1000[1].moisture_sensors[2].bicoder.set_moisture_percent(_percent=14)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_6(self):
        """
        Verify full Configuration \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_7(self):
        """
        set the clock 2 minutes to make the time 04/08/2014 8:00 am \n
        Verify that all zones are set to done before the test starts
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
            - zone 6 done watering \n
            - zone 7 done watering \n
            - zone 8 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].set_date_and_time(_date="04/08/2014", _time="07:59:00")
            self.config.BaseStation1000[1].verify_date_and_time()
            self.config.BaseStation1000[1].set_controller_to_run()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_8(self):
        """
        Start program 1 & program 2
        advance the clock 1 minutes to make the time 04/08/2014 8:01 this should start the zones watering \n
        Start the Programs and verify that only two zones are running on each program due to concurrent limit \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
            - program 2 running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 watering \n
            - zone 6 watering \n
            - zone 7 waiting to water \n
            - zone 8 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].set_program_to_start()
            self.config.BaseStation1000[1].programs[2].set_program_to_start()
            self.config.BaseStation1000[1].do_increment_clock(minutes=2)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[7].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[8].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_9(self):
        """
        Test that a single moisture sensor can pause a program \n
        Set SB07270 and SB07263 to be 9, which is lower than the 'lower limit', and verify that both Programs pause \n
        advance the clock 9 minutes to make the time 04/08/2014 8:10 \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 paused \n
            - program 2 paused \n
        - Zone status: \n
            - zone 1 paused \n
            - zone 2 paused \n
            - zone 3 paused \n
            - zone 4 paused \n
            - zone 5 paused \n
            - zone 6 paused \n
            - zone 7 paused \n
            - zone 8 paused \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=9)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            self.config.BaseStation1000[1].moisture_sensors[2].bicoder.set_moisture_percent(_percent=9)
            self.config.BaseStation1000[1].moisture_sensors[2].bicoder.do_self_test()
            self.config.BaseStation1000[1].set_controller_to_run()
            self.config.BaseStation1000[1].do_increment_clock(minutes=9)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_paused()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_paused()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_paused()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_10(self):
        """
        Turn off both Programs
        advance the clock 1 minutes to make the time 04/08/2014 8:11 to let the statuses update \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
            - zone 6 done watering \n
            - zone 7 done watering \n
            - zone 8 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].set_program_to_stop()
            self.config.BaseStation1000[1].programs[2].set_program_to_stop()
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_11(self):
        """
        Sensor SB07263 is disabled \n
        Sensor SB07270 is set up so that it will pause:
            - program 1 if it is above 10 \n
            - program 2 if it is above 20 \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[2].moisture_pause_conditions[2].set_moisture_mode_off()

            self.config.BaseStation1000[1].programs[1].moisture_pause_conditions[1].set_moisture_mode_to_upper_limit()
            self.config.BaseStation1000[1].programs[1].moisture_pause_conditions[1].set_moisture_threshold(_percent=10)

            self.config.BaseStation1000[1].programs[2].add_moisture_pause_condition(_moisture_sensor_address=1)
            self.config.BaseStation1000[1].programs[2].moisture_pause_conditions[1].set_moisture_mode_to_upper_limit()
            self.config.BaseStation1000[1].programs[2].moisture_pause_conditions[1].set_moisture_threshold(_percent=20)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_12(self):
        """
        Set SB07270's moisture percent to be 21 so it triggers both program's upper limit threshold \n
        Set both Programs to run \n
        advance the clock 11 minutes to make the time 04/08/2014 8:22 \n\n
        Since the the moisture percent is above both upper limits that we set, all Programs should be paused \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 paused \n
            - program 2 paused \n
        - Zone status: \n
            - zone 1 paused \n
            - zone 2 paused \n
            - zone 3 paused \n
            - zone 4 paused \n
            - zone 5 paused \n
            - zone 6 paused \n
            - zone 7 paused \n
            - zone 8 paused \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].set_controller_to_run()
            self.config.BaseStation1000[1].programs[1].set_program_to_start()
            self.config.BaseStation1000[1].programs[2].set_program_to_start()
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=21.0)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            self.config.BaseStation1000[1].do_increment_clock(minutes=11)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_paused()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_paused()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_paused()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_13(self):
        """
        Turn off both Programs \n
        advance the clock 1 minutes to make the time 04/08/2014 8:23 to let the statuses update \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
            - zone 6 done watering \n
            - zone 7 done watering \n
            - zone 8 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].set_program_to_stop()
            self.config.BaseStation1000[1].programs[2].set_program_to_stop()
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_14(self):
        """
        Set the moisture percentages of both moisture sensors to be above 10 \n
        Clear the previous pause commands \n
        Create conditions for both Programs: \n
            - Program 1 starts when moisture sensor SB07270 reads below 10 \n
            - Program 2 starts when moisture sensor SB07263 reads below 10 \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=20)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            self.config.BaseStation1000[1].moisture_sensors[2].bicoder.set_moisture_percent(_percent=12)
            self.config.BaseStation1000[1].moisture_sensors[2].bicoder.do_self_test()
            self.config.BaseStation1000[1].set_controller_to_run()

            # Clearing previous pause conditions
            self.config.BaseStation1000[1].programs[1].moisture_pause_conditions[1].set_moisture_mode_off()
            self.config.BaseStation1000[1].programs[2].moisture_pause_conditions[2].set_moisture_mode_off()

            # Set new start conditions
            self.config.BaseStation1000[1].programs[1].add_moisture_start_condition(_moisture_sensor_address=1)
            self.config.BaseStation1000[1].programs[1].moisture_start_conditions[1].set_moisture_mode_to_lower_limit()
            self.config.BaseStation1000[1].programs[1].moisture_start_conditions[1].set_moisture_threshold(_percent=10)

            self.config.BaseStation1000[1].programs[2].add_moisture_start_condition(_moisture_sensor_address=2)
            self.config.BaseStation1000[1].programs[2].moisture_start_conditions[2].set_moisture_mode_to_lower_limit()
            self.config.BaseStation1000[1].programs[2].moisture_start_conditions[2].set_moisture_threshold(_percent=10)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_15(self):
        """
        Set the moisture percent on moisture sensor SB07270 to 9, which should start program 1 since it has a lower
        limit threshold of 10. Program 2 should still be in the default state because it's moisture sensor SB07263 has
        a value above 10. \n
        advance the clock 7 minutes to make the time 04/08/2014 8:30 \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
            - program 2 done watering \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 paused \n
            - zone 6 paused \n
            - zone 7 paused \n
            - zone 8 paused \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=9)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            self.config.BaseStation1000[1].set_controller_to_run()
            self.config.BaseStation1000[1].do_increment_clock(minutes=7)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[8].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_16(self):
        """
        Set the moisture percent on moisture sensor SB07263 to 9, which should start program 2 since it has a lower
        limit threshold of 10. Now both program 1 and 2 should be running as both of the moisture percents on their
        respective moisture sensors are below their lower limit. \n
        advance the clock 10 minutes to make the time 04/08/2014 8:40 \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
            - program 2 running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 watering \n
            - zone 6 watering \n
            - zone 7 waiting to water \n
            - zone 8 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].moisture_sensors[2].bicoder.set_moisture_percent(_percent=8)
            self.config.BaseStation1000[1].moisture_sensors[2].bicoder.do_self_test()
            self.config.BaseStation1000[1].set_controller_to_run()
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[7].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[8].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_17(self):
        """
        Turn off both Programs \n
        advance the clock 1 minutes to make the time 04/08/2014 8:41 to let the statuses update \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
            - zone 6 done watering \n
            - zone 7 done watering \n
            - zone 8 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].set_program_to_stop()
            self.config.BaseStation1000[1].programs[2].set_program_to_stop()
            self.config.BaseStation1000[1].set_controller_to_off()
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            self.config.BaseStation1000[1].verify_date_and_time()
            # This is necessary for our data in zone to update
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[2].get_data()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_18(self):
        """
        Set the moisture percentages of both moisture sensors to be above 10 \n
        Create a start condition for program 2 when moisture sensor SB07270 has a moisture percent below 10 \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=11)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()           
            self.config.BaseStation1000[1].moisture_sensors[2].bicoder.set_moisture_percent(_percent=11)
            self.config.BaseStation1000[1].moisture_sensors[2].bicoder.do_self_test()
            self.config.BaseStation1000[1].set_controller_to_run()

            self.config.BaseStation1000[1].programs[2].add_moisture_start_condition(_moisture_sensor_address=1)
            self.config.BaseStation1000[1].programs[2].moisture_start_conditions[1].set_moisture_mode_to_lower_limit()
            self.config.BaseStation1000[1].programs[2].moisture_start_conditions[1].set_moisture_threshold(_percent=10)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_19(self):
        """
        Set the moisture percent on moisture sensor SB07263 to 9, which should start program 2 since it has a lower
        limit threshold of 10. Now both program 1 and 2 should be running as both of the moisture percents on their
        respective moisture sensors are below their lower limit. \n
        advance the clock 9 minutes to make the time 04/08/2014 8:40 \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
            - program 2 running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 watering \n
            - zone 6 watering \n
            - zone 7 waiting to water \n
            - zone 8 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=9)
            # self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            # self.config.BaseStation1000[1].set_controller_to_run()
            self.config.BaseStation1000[1].do_increment_clock(minutes=9)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[7].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[8].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_20(self):
        """
        Turn off both Programs \n
        advance the clock 1 minutes to make the time 04/08/2014 8:51 to let the statuses update \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
            - zone 6 done watering \n
            - zone 7 done watering \n
            - zone 8 done watering \n
        Clear all start conditions from both program 1 and 2 \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].set_program_to_stop()
            self.config.BaseStation1000[1].programs[2].set_program_to_stop()
            self.config.BaseStation1000[1].set_controller_to_off()
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_done()

            # Clearing previous start conditions
            self.config.BaseStation1000[1].programs[1].moisture_start_conditions[1].set_moisture_mode_off()
            self.config.BaseStation1000[1].programs[2].moisture_start_conditions[1].set_moisture_mode_off()
            self.config.BaseStation1000[1].programs[2].moisture_start_conditions[2].set_moisture_mode_off()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_21(self):
        """
        Set the moisture percent on both the SB07270 and SB07263 moisture sensors to be 11 \n
        Set program 1 so that it starts when moisture sensor SB07270 reads below 10 \n
        Set program 2 so that it starts when moisture sensor SB07263 reads below 10 \n
        Set program 1 so that it stops when moisture sensor SB07270 reads above 15 and set SI to stop immediately \n
        Set program 2 so that it stops when moisture sensor SB07263 reads above 15 and set SI to stop immediately \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=11)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()           
            self.config.BaseStation1000[1].moisture_sensors[2].bicoder.set_moisture_percent(_percent=11)
            self.config.BaseStation1000[1].moisture_sensors[2].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation1000[1].set_controller_to_run()

            # Set new start conditions
            self.config.BaseStation1000[1].programs[1].moisture_start_conditions[1].set_moisture_mode_to_lower_limit()
            self.config.BaseStation1000[1].programs[1].moisture_start_conditions[1].set_moisture_threshold(_percent=10)
            self.config.BaseStation1000[1].programs[2].moisture_start_conditions[2].set_moisture_mode_to_lower_limit()
            self.config.BaseStation1000[1].programs[2].moisture_start_conditions[2].set_moisture_threshold(_percent=10)

            # Set new stop conditions for both Programs
            self.config.BaseStation1000[1].programs[1].add_moisture_stop_condition(_moisture_sensor_address=1)
            self.config.BaseStation1000[1].programs[1].moisture_stop_conditions[1].set_moisture_mode_to_upper_limit()
            self.config.BaseStation1000[1].programs[1].moisture_stop_conditions[1].set_moisture_threshold(_percent=15)
            self.config.BaseStation1000[1].programs[1].moisture_stop_conditions[1].set_stop_immediately_true()

            self.config.BaseStation1000[1].programs[2].add_moisture_stop_condition(_moisture_sensor_address=2)
            self.config.BaseStation1000[1].programs[2].moisture_stop_conditions[2].set_moisture_mode_to_upper_limit()
            self.config.BaseStation1000[1].programs[2].moisture_stop_conditions[2].set_moisture_threshold(_percent=15)
            self.config.BaseStation1000[1].programs[2].moisture_stop_conditions[2].set_stop_immediately_true()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_22(self):
        """
        Set the moisture percent on both the SB07270 and SB07263 moisture sensors to be 9, which is below both program's
        lower limit. This should cause both Programs to start. \n
        advance the clock 9 minutes to make the time 04/08/2014 9:00 \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
            - program 2 running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 watering \n
            - zone 6 watering \n
            - zone 7 waiting to water \n
            - zone 8 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=9)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()           
            self.config.BaseStation1000[1].moisture_sensors[2].bicoder.set_moisture_percent(_percent=9)
            self.config.BaseStation1000[1].moisture_sensors[2].bicoder.do_self_test()
            self.config.BaseStation1000[1].set_controller_to_run()
            self.config.BaseStation1000[1].do_increment_clock(minutes=9)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[7].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[8].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_23(self):
        """
        Set the moisture percent on both the SB07270 and SB07263 moisture sensors to be 16, which is above both
        program's upper limit. This should cause both Programs to stop. \n
        advance the clock 10 minutes to make the time 04/08/2014 9:10 \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
            - zone 6 done watering \n
            - zone 7 done watering \n
            - zone 8 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=16)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()           
            self.config.BaseStation1000[1].moisture_sensors[2].bicoder.set_moisture_percent(_percent=16)
            self.config.BaseStation1000[1].moisture_sensors[2].bicoder.do_self_test()
            self.config.BaseStation1000[1].set_controller_to_run()
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_24(self):
        """
        Turn off both Programs \n
        advance the clock 1 minutes to make the time 04/08/2014 9:11 to let the statuses update \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
            - zone 6 done watering \n
            - zone 7 done watering \n
            - zone 8 done watering \n
        Clear all start conditions from both program 1 and 2 \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            # TODO this method is a little redundant because we already stop the Programs in the previous step
            self.config.BaseStation1000[1].programs[1].set_program_to_stop()
            self.config.BaseStation1000[1].programs[2].set_program_to_stop()
            self.config.BaseStation1000[1].set_controller_to_off()
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_25(self):
        """
        Set the moisture percent on moisture sensor SB07270 to 11 \n
        Set program 2 so that it starts when moisture sensor SB07270 reads below 10 \n
        Set program 2 so that it stops when moisture sensor SB07270 reads above 15 and set SI to stop immediately \n
        These assignments make it so that program 1 and 2 are controlled by the same moisture sensor \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=11)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            self.config.BaseStation1000[1].set_controller_to_run()

            self.config.BaseStation1000[1].programs[2].moisture_start_conditions[1].set_moisture_mode_to_lower_limit()
            self.config.BaseStation1000[1].programs[2].moisture_start_conditions[1].set_moisture_threshold(_percent=10)

            self.config.BaseStation1000[1].programs[2].add_moisture_stop_condition(_moisture_sensor_address=1)
            self.config.BaseStation1000[1].programs[2].moisture_stop_conditions[1].set_moisture_mode_to_upper_limit()
            self.config.BaseStation1000[1].programs[2].moisture_stop_conditions[1].set_moisture_threshold(_percent=15)
            self.config.BaseStation1000[1].programs[2].moisture_stop_conditions[1].set_stop_immediately_true()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_26(self):
        """
        Set the moisture percent on the moisture sensor SB07270 to be 9, which is below both program's lower limit,
        this should cause both Programs to start. \n
        advance the clock 9 minutes to make the time 04/08/2014 9:20 \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
            - program 2 running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 watering \n
            - zone 6 watering \n
            - zone 7 waiting to water \n
            - zone 8 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=9)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            self.config.BaseStation1000[1].set_controller_to_run()
            self.config.BaseStation1000[1].do_increment_clock(minutes=9)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[7].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[8].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_27(self):
        """
        Set the moisture percent on moisture sensor SB07270 to 16, which is higher than both program's upper limit. This
        should cause both Programs to stop running. \n
        advance the clock 10 minutes to make the time 04/08/2014 9:30 \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
            - zone 6 done watering \n
            - zone 7 done watering \n
            - zone 8 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=16)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            self.config.BaseStation1000[1].set_controller_to_run()
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_28(self):
        """
        Turn off both Programs \n
        advance the clock 1 minutes to make the time 04/08/2014 9:31 to let the statuses update \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
            - zone 6 done watering \n
            - zone 7 done watering \n
            - zone 8 done watering \n
        Clear all start conditions from both program 1 and 2 \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            # TODO this method is a little redundant because we already stop the Programs in the previous step
            self.config.BaseStation1000[1].programs[1].set_program_to_stop()
            self.config.BaseStation1000[1].programs[2].set_program_to_stop()
            self.config.BaseStation1000[1].set_controller_to_off()
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_29(self):
        """
        Verify full Configuration \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]