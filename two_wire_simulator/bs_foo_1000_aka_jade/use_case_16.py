import sys
from time import sleep
from datetime import datetime, timedelta
from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.logging_handler import log_handler
from common.imports import opcodes
from common import helper_methods

__author__ = "Baseline"


class ControllerUseCase16(object):
    """
    Test name: \n
        Timed Zones With Soak Cycles \n
    Purpose: \n
        This test covers using regular soak cycles and intelligent soak cycles. \n
    Coverage area: \n
        1. Set up the controller and limit its concurrent zones \n
        2. Searching and assigning: \n
            - Zones \n
            - Moisture sensors \n
            - Flow meters \n
        3. Set up Programs and give them a run time using commands \n
        4. Assign zones to Programs and give them run times \n
        5. Verify the full configuration \n
        6-26. This code goes through regular watering, and each step increments by one minute and then verifies that
              the correct zones are running \n
        27. Set both Programs to use intelligent soak cycles, and set cycle counts, soak times, and run-times \n
        28-48. This code runs through the intelligent soak and each step increments one minute and then verifies that
               the correct zones are running/soaking/waiting \n
        49. Set the soak cycle mode on program 2 to be 'program cycles' \n
        50-70. This code runs through regular soak cycles and each step increments one minute and then verifies that
               the correct zones are running/soaking/waiting
        71. Re-verify the full configuration \n
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """
        time_it_should_take_test_to_run = timedelta(minutes=15)  # this is minutes
        time_test_started = datetime.now()
        try:
            number_of_retries = 1
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.test_elapsed_time(_time_started=time_test_started,
                                                     _time_expected_to_run=time_it_should_take_test_to_run)
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        ############################
        Setup Programs
        ############################
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].set_max_concurrent_zones(_max_zones=1)

            self.config.BaseStation1000[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation1000[1].add_program_to_controller(_program_address=2)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_2(self):
        """
        ############################
        Setup Zone Programs
        ############################
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation1000[1].programs[1].zone_programs[1].set_run_time(_minutes=3)
            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=2)
            self.config.BaseStation1000[1].programs[2].zone_programs[2].set_run_time(_minutes=5)
            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=3)
            self.config.BaseStation1000[1].programs[2].zone_programs[3].set_run_time(_minutes=4)
            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=4)
            self.config.BaseStation1000[1].programs[2].zone_programs[4].set_run_time(_minutes=3)
            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=5)
            self.config.BaseStation1000[1].programs[2].zone_programs[5].set_run_time(_minutes=3)

            self.config.BaseStation1000[1].zones[5].set_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_3(self):
        """
        Verify the entire configuration \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_4(self):
        """
        Set the controller to off and then turn it back on \n
        Stop both Programs \n
        Increment the clock by 1 second \n
        Set the time to be '04/08/2014 02:37:58' \n
        Increment the clock by 1 second again \n
        Verify that all zones are responding and have a 'done watering' status \n
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].set_controller_to_off()
            self.config.BaseStation1000[1].set_controller_to_run()
            self.config.BaseStation1000[1].programs[1].set_program_to_stop()
            self.config.BaseStation1000[1].programs[2].set_program_to_stop()
            self.config.BaseStation1000[1].do_increment_clock(seconds=1)
            self.config.BaseStation1000[1].set_date_and_time(_date="04/08/2014", _time="02:37:58")
            self.config.BaseStation1000[1].do_increment_clock(seconds=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_5(self):
        """
        Start both Programs \n
        Increment the clock by 1 second again so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].set_program_to_start()
            self.config.BaseStation1000[1].programs[2].set_program_to_start()
            self.config.BaseStation1000[1].do_increment_clock(seconds=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_8(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:39:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_9(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:40:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_10(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:41:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_11(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:42:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_12(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:43:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_13(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:44:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_14(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:45:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #
    def step_15(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:46:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_16(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:47:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_17(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:48:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_18(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:49:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_19(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:50:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 watering \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_20(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:51:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 watering \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_21(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:52:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 watering \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_22(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:53:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_23(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:54:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_24(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:55:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_25(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:56:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_26(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:57:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_27(self):
        """
        Set both Programs to use intelligent soak cycles \n
        Set cycle counts on both Programs \n
        Set soak times for both Programs \n
        Change the run-times on each zone program \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].set_soak_cycle_mode(_mode=opcodes.intelligent_soak)
            self.config.BaseStation1000[1].programs[1].set_cycle_count(_new_count=20)
            self.config.BaseStation1000[1].programs[1].set_soak_time_in_seconds(_seconds=480)
            self.config.BaseStation1000[1].programs[2].set_soak_cycle_mode(_mode=opcodes.intelligent_soak)
            self.config.BaseStation1000[1].programs[2].set_cycle_count(_new_count=6)
            self.config.BaseStation1000[1].programs[2].set_soak_time_in_seconds(_seconds=300)

            self.config.BaseStation1000[1].programs[1].zone_programs[1].set_run_time(_minutes=60)
            self.config.BaseStation1000[1].programs[2].zone_programs[2].set_run_time(_minutes=12)
            self.config.BaseStation1000[1].programs[2].zone_programs[3].set_run_time(_minutes=18)
            self.config.BaseStation1000[1].programs[2].zone_programs[4].set_run_time(_minutes=5)
            self.config.BaseStation1000[1].programs[2].zone_programs[5].set_run_time(_minutes=12)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_28(self):
        """
        Set the controller to off and then turn it back on \n
        Stop both Programs \n
        Increment the clock by 1 second \n
        Set the time to be '04/08/2014 02:37:58' \n
        Increment the clock by 1 second again \n
        Verify that all zones are responding and have a 'done watering' status \n
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].set_controller_to_off()
            self.config.BaseStation1000[1].set_controller_to_run()
            self.config.BaseStation1000[1].programs[1].set_program_to_stop()
            self.config.BaseStation1000[1].programs[2].set_program_to_stop()
            self.config.BaseStation1000[1].do_increment_clock(seconds=1)
            self.config.BaseStation1000[1].set_date_and_time(_date="04/08/2014", _time="02:37:58")
            self.config.BaseStation1000[1].do_increment_clock(seconds=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_29(self):
        """
        Start both Programs \n
        Increment the clock by 1 second again so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].set_program_to_start()
            self.config.BaseStation1000[1].programs[2].set_program_to_start()
            self.config.BaseStation1000[1].do_increment_clock(seconds=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_30(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:39:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_31(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:40:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_32(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:41:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_33(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:42:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_34(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:43:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_35(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:44:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_36(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:45:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_37(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:46:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 soaking \n
            - zone 4 watering \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_38(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:47:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 soaking \n
            - zone 4 soaking \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_39(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:48:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 watering \n
            - zone 3 soaking \n
            - zone 4 soaking \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_40(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:49:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 waiting to water \n
            - zone 2 watering \n
            - zone 3 soaking \n
            - zone 4 soaking \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_41(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:50:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 soaking \n
            - zone 3 soaking \n
            - zone 4 soaking \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_42(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:51:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 soaking \n
            - zone 3 waiting to water \n
            - zone 4 soaking \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_43(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:52:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 soaking \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_44(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:53:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_45(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:54:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_46(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:55:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 waiting to water \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_47(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:56:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 watering \n
            - zone 3 soaking \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_48(self):
        """
        Increment the clock by 1 minute again so that the time is '04/08/2014 02:57:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 watering \n
            - zone 3 soaking \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_49(self):
        """
        Set the soak cycle mode of program 2 to program cycles \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[2].set_soak_cycle_mode(_mode=opcodes.program_cycles)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_50(self):
        """
        Set the controller to off and then turn it back on \n
        Stop both Programs \n
        Increment the clock by 1 second \n
        Set the time to be '04/08/2014 02:37:58' \n
        Increment the clock by 1 second again \n
        Verify that all zones are responding and have a 'done watering' status \n
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].set_controller_to_off()
            self.config.BaseStation1000[1].set_controller_to_run()
            self.config.BaseStation1000[1].programs[1].set_program_to_stop()
            self.config.BaseStation1000[1].programs[2].set_program_to_stop()
            self.config.BaseStation1000[1].do_increment_clock(seconds=1)
            self.config.BaseStation1000[1].set_date_and_time(_date="04/08/2014", _time="02:37:58")
            self.config.BaseStation1000[1].do_increment_clock(seconds=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_51(self):
        """
        Start both Programs \n
        Increment the clock by 1 second again so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].set_program_to_start()
            self.config.BaseStation1000[1].programs[2].set_program_to_start()
            self.config.BaseStation1000[1].do_increment_clock(seconds=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_52(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_53(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_54(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_55(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_56(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_57(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_58(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_59(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 soaking \n
            - zone 4 watering \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_60(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 soaking \n
            - zone 4 soaking \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_61(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 watering \n
            - zone 3 soaking \n
            - zone 4 soaking \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_62(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 waiting to water \n
            - zone 2 watering \n
            - zone 3 soaking \n
            - zone 4 soaking \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_63(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 soaking \n
            - zone 3 soaking \n
            - zone 4 soaking \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_64(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 soaking \n
            - zone 3 waiting to water \n
            - zone 4 soaking \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_65(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 soaking \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]


    def step_66(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_67(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_68(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 waiting to water \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_69(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 waiting to water \n
            - zone 3 soaking \n
            - zone 4 watering \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_70(self):
        """
        Increment the clock by 1 minute so that the time is '04/08/2014 02:38:00'
        Verify the status for each zone on the controller \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 watering \n
            - zone 3 soaking \n
            - zone 4 soaking \n
            - zone 5 disabled \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_71(self):
        """
        Re-verify the full configuration \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
