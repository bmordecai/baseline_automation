import sys
from time import sleep
from datetime import datetime, timedelta
from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.logging_handler import log_handler
from common import helper_methods
from common.imports import opcodes

__author__ = 'Tige'


class ControllerUseCase3(object):
    """
    Test name: \n
        Basic Programming Test \n
    Purpose: \n
        To test that each command and verify that they work \n
    Covers: \n
        1. Load devices \n
        2. Search for devices \n
        3. Set descriptions and locations \n
        4. Can assign devices to addresses \n
        5. Can assign zones to Programs \n
        6. Can set up Programs with; start times, watering days, water windows, zone concurrency, and assign Programs to
    water sources \n
        7. Can set up moisture sensors and give them values \n
        8. Can set up temperature sensors and give them values \n
        9. Can set up event switches and give them values \n
        10. Can set up master valves \n
        11. Can set up flow meters and give them values \n
        12. Can set up water sources \n
        13. Can set up booster pumps \n
        14. Can set up water sources \n
        15. Can test all devices \n

    added new verify flow values section do to a bug that was found by support in the 1000 version .123 if the
    controller was reboot the 1000 would read incorrect flow values the decimal point would be changed to be 1/10th
    of the current value \n
    a reboot of the the controller was add and then all values abre verified \n
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    ###############################
    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """
        time_it_should_take_test_to_run = timedelta(minutes=15)  # this is minutes
        time_test_started = datetime.now()
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.test_elapsed_time(_time_started=time_test_started,
                                                     _time_expected_to_run=time_it_should_take_test_to_run)
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        Over write zone values: \n
        set up the zones \n
            - Change the design flow for each zone \n
            - disable zone 98
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].set_max_concurrent_zones(_max_zones=4)
            self.config.BaseStation1000[1].zones[1].set_design_flow(_gallons_per_minute=10)
            self.config.BaseStation1000[1].zones[2].set_design_flow(_gallons_per_minute=10)
            self.config.BaseStation1000[1].zones[49].set_design_flow(_gallons_per_minute=30)
            self.config.BaseStation1000[1].zones[50].set_design_flow(_gallons_per_minute=30)
            self.config.BaseStation1000[1].zones[97].set_design_flow(_gallons_per_minute=80)
            self.config.BaseStation1000[1].zones[98].set_disabled()
            self.config.BaseStation1000[1].zones[99].set_design_flow(_gallons_per_minute=80)
            self.config.BaseStation1000[1].zones[100].set_design_flow(_gallons_per_minute=100)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_2(self):
        """
        Over write Moisture sensor values: \n
        Set up Moisture Sensors \n
            - set new moisture values for each moisture sensor \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=40.0)
            self.config.BaseStation1000[1].moisture_sensors[2].bicoder.set_moisture_percent(_percent=10.0)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_3(self):
        """
        Over write Temperature Sensors values \n
        - set new temperature value for temperature sensor to 82 degrees fahrenheit \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].temperature_sensors[1].bicoder.set_temperature_reading(_degrees=82.0)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_4(self):
        """
        Over write event switch values:  \n
        Set up Event Switches \n
        - set event switch to have a closed value \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].event_switches[1].bicoder.set_contact_closed()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_5(self):
        """
        Over write flow meter values: \n
        Set up flow meters \n
            - set new k values for each flow meter \n
            - set flow values for each flow meter \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].flow_meters[1].bicoder.set_k_value(_value=6.99)
            self.config.BaseStation1000[1].flow_meters[2].bicoder.set_k_value(_value=.86)
            self.config.BaseStation1000[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=125.0)
            self.config.BaseStation1000[1].flow_meters[2].bicoder.set_flow_rate(_gallons_per_minute=550)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_6(self):
        """
        ############################
        Setup Programs
        ############################
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        daily_water_window = ['011111100001111111111110']
        fully_open_water_window = ['111111111111111111111111']
        weekly_water_windows = ['011111100001111111111110', '011111100000111111111110', '111111100000000000011110',
                                '111111100000000000011110', '111111100000000000011110', '111111100000000000011110',
                                '111111100000000000011110']
        try:
            self.config.BaseStation1000[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation1000[1].programs[1].set_daily_water_window(_ww=daily_water_window)
            self.config.BaseStation1000[1].programs[1].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation1000[1].programs[1].set_seasonal_adjust(_percent=100)
            self.config.BaseStation1000[1].programs[1].set_soak_cycle_mode(_mode=opcodes.program_cycles)

            self.config.BaseStation1000[1].programs[1].set_cycle_count(_new_count=4)
            self.config.BaseStation1000[1].programs[1].set_soak_time_in_seconds(_seconds=300)

            self.config.BaseStation1000[1].add_program_to_controller(_program_address=3)
            self.config.BaseStation1000[1].programs[3].set_weekly_water_window(_ww=weekly_water_windows)
            self.config.BaseStation1000[1].programs[3].set_max_concurrent_zones(_number_of_zones=5)
            self.config.BaseStation1000[1].programs[3].set_seasonal_adjust(_percent=50)
            self.config.BaseStation1000[1].programs[3].set_soak_cycle_mode(_mode=opcodes.intelligent_soak)
            self.config.BaseStation1000[1].programs[3].set_cycle_count(_new_count=6)
            self.config.BaseStation1000[1].programs[3].set_soak_time_in_seconds(_seconds=600)

            self.config.BaseStation1000[1].add_program_to_controller(_program_address=30)
            self.config.BaseStation1000[1].programs[30].set_weekly_water_window(_ww=weekly_water_windows)
            self.config.BaseStation1000[1].programs[30].set_max_concurrent_zones(_number_of_zones=5)
            self.config.BaseStation1000[1].programs[30].set_seasonal_adjust(_percent=150)
            self.config.BaseStation1000[1].programs[30].set_soak_cycle_mode(_mode=opcodes.zone_soak_cycles)
            self.config.BaseStation1000[1].programs[30].set_cycle_count(_new_count=8)
            self.config.BaseStation1000[1].programs[30].set_soak_time_in_seconds(_seconds=1200)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_7(self):
        """
        ############################
        Setup Zone Programs
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation1000[1].programs[1].zone_programs[1].set_run_time(_minutes=15)

            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation1000[1].programs[1].zone_programs[2].set_run_time(_minutes=15)

            self.config.BaseStation1000[1].programs[3].add_zone_to_program(_zone_address=49)
            self.config.BaseStation1000[1].programs[3].zone_programs[49].set_run_time(_minutes=20)

            self.config.BaseStation1000[1].programs[3].add_zone_to_program(_zone_address=50)
            self.config.BaseStation1000[1].programs[3].zone_programs[50].set_run_time(_minutes=20)

            self.config.BaseStation1000[1].programs[30].add_zone_to_program(_zone_address=97)
            self.config.BaseStation1000[1].programs[30].zone_programs[97].set_run_time(_minutes=33)

            self.config.BaseStation1000[1].programs[30].add_zone_to_program(_zone_address=98)
            self.config.BaseStation1000[1].programs[30].zone_programs[98].set_run_time(_minutes=33)

            self.config.BaseStation1000[1].programs[30].add_zone_to_program(_zone_address=99)
            self.config.BaseStation1000[1].programs[30].zone_programs[99].set_run_time(_minutes=33)

            self.config.BaseStation1000[1].programs[30].add_zone_to_program(_zone_address=100)
            self.config.BaseStation1000[1].programs[30].zone_programs[100].set_run_time(_minutes=33)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_8(self):
        """
        ############################
        Setup Points of Connection
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].add_poc(_poc_address=1)
            self.config.BaseStation1000[1].points_of_control[1].add_flow_meter_to_point_of_control(
                _flow_meter_address=1)
            self.config.BaseStation1000[1].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=1)
            self.config.BaseStation1000[1].points_of_control[1].set_target_flow(_gpm=500)
            self.config.BaseStation1000[1].points_of_control[1].set_high_flow_limit(_limit=550,
                                                                                    with_shutdown_enabled=True)
            self.config.BaseStation1000[1].points_of_control[1].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=True)
            self.config.BaseStation1000[1].points_of_control[1].set_limit_concurrent_to_target_state_enabled()
            self.config.BaseStation1000[1].points_of_control[1].set_fill_time(_minutes=2)
            self.config.BaseStation1000[1].points_of_control[1].set_flow_variance(_percent=10)
            self.config.BaseStation1000[1].points_of_control[1].set_flow_variance_enabled()
            self.config.BaseStation1000[1].points_of_control[1].set_flow_fault_state_disabled()

            self.config.BaseStation1000[1].add_poc(_poc_address=2)
            self.config.BaseStation1000[1].points_of_control[2].set_target_flow(_gpm=500)
            self.config.BaseStation1000[1].points_of_control[2].set_high_flow_limit(_limit=550,
                                                                                    with_shutdown_enabled=True)
            self.config.BaseStation1000[1].points_of_control[2].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=True)
            self.config.BaseStation1000[1].points_of_control[2].set_limit_concurrent_to_target_state_enabled()
            self.config.BaseStation1000[1].points_of_control[2].set_fill_time(_minutes=2)
            self.config.BaseStation1000[1].points_of_control[2].set_flow_variance(_percent=10)
            self.config.BaseStation1000[1].points_of_control[2].set_flow_variance_enabled()
            self.config.BaseStation1000[1].points_of_control[2].set_flow_fault_state_disabled()

            self.config.BaseStation1000[1].add_poc(_poc_address=3)
            self.config.BaseStation1000[1].points_of_control[3].add_flow_meter_to_point_of_control(
                _flow_meter_address=2)
            self.config.BaseStation1000[1].points_of_control[3].add_master_valve_to_point_of_control(
                _master_valve_address=7)
            self.config.BaseStation1000[1].points_of_control[3].set_target_flow(_gpm=50)
            self.config.BaseStation1000[1].points_of_control[3].set_high_flow_limit(_limit=75,
                                                                                    with_shutdown_enabled=False)
            self.config.BaseStation1000[1].points_of_control[3].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=False)
            self.config.BaseStation1000[1].points_of_control[3].set_limit_concurrent_to_target_state_enabled()
            self.config.BaseStation1000[1].points_of_control[3].set_fill_time(_minutes=4)
            self.config.BaseStation1000[1].points_of_control[3].set_flow_variance(_percent=25)
            self.config.BaseStation1000[1].points_of_control[3].set_flow_variance_enabled()
            self.config.BaseStation1000[1].points_of_control[3].set_flow_fault_state_disabled()

            # Add points of connection to programs
            self.config.BaseStation1000[1].programs[1].add_point_of_connection(_poc_address=[1])
            self.config.BaseStation1000[1].programs[3].add_point_of_connection(_poc_address=[1, 2])
            self.config.BaseStation1000[1].programs[30].add_point_of_connection(_poc_address=[3])
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_9(self):
        """
        set up start stop pause conditions
            Program 1:
                - start times 8:00 am, 9:00 am, 10:00 am, 11:00 am \n
                - watering days are set to weekly \n
                - Watering days enabled are Monday, Wednesday, Friday \n
                - assign moisture sensor SB05308 to start program 30 and give it a threshold of 38 \n

            Program 3:
                - start time start times 10:00 am, 11:00 am, 12:00 am, 1:00 pm \n
                - watering days are set calendar interval's on odd days \n
                - set up a temperature stop when the sensor reads below 32 degrees \n

            Program 30:
                - start times 10:00 am, 11:00 am, 12:00 am, 1:00 pm \n
                - watering days are set to semi monthly  \n
                - set the event switch to stop program 30 when the contacts open \n
        """

        start_times_800_900_1000_1100 = [480, 540, 600, 660]
        start_times_1000_1100_1200_1300 = [600, 660, 720, 780]
        mon_wed_fri_watering_days = [0, 1, 0, 1, 0, 1, 0]  # runs monday, wednesday, friday
        semi_monthly = [0, 0, 0, 0, 0, 0, 8, 8, 6, 5, 5, 4, 3, 3, 3, 3, 4, 5, 6, 7, 0, 0, 0, 0]
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].add_date_time_start_condition(_condition_number=1)
            self.config.BaseStation1000[1].programs[1].date_time_start_conditions[1].set_day_time_start(
                _dt=opcodes.true,
                _st_list=start_times_800_900_1000_1100,
                _interval_type=opcodes.week_days,
                _interval_args=mon_wed_fri_watering_days)

            self.config.BaseStation1000[1].programs[3].add_date_time_start_condition(_condition_number=1)
            self.config.BaseStation1000[1].programs[3].date_time_start_conditions[1].set_day_time_start(
                _dt=opcodes.true,
                _st_list=start_times_1000_1100_1200_1300,
                _interval_type=opcodes.calendar_interval,
                _interval_args=opcodes.odd_day)

            self.config.BaseStation1000[1].programs[30].add_date_time_start_condition(_condition_number=1)
            self.config.BaseStation1000[1].programs[30].date_time_start_conditions[1].set_day_time_start(
                _dt=opcodes.true,
                _st_list=start_times_1000_1100_1200_1300,
                _interval_type=opcodes.semi_month_interval,
                _interval_args=semi_monthly)

            self.config.BaseStation1000[1].programs[1].add_moisture_start_condition(_moisture_sensor_address=1)
            self.config.BaseStation1000[1].programs[1].moisture_start_conditions[1].set_moisture_mode_to_lower_limit()
            self.config.BaseStation1000[1].programs[1].moisture_start_conditions[1].set_moisture_threshold(
                _percent=24.0)

            self.config.BaseStation1000[1].programs[3].add_temperature_stop_condition(_temperature_sensor_address=1)
            self.config.BaseStation1000[1].programs[3].temperature_stop_conditions[1].set_temperature_mode_to_lower_limit()
            self.config.BaseStation1000[1].programs[3].temperature_stop_conditions[1].set_temperature_threshold(
                _degrees=32.0)

            self.config.BaseStation1000[1].programs[30].add_switch_pause_condition(_event_switch_address=1)
            self.config.BaseStation1000[1].programs[30].event_switch_pause_conditions[1].set_switch_mode_to_open()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_10(self):
        """
        perform a test on all devices to verify that they are functioning properly \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].bicoder.do_self_test()
            for moisture_sensors in self.config.ms_ad_range:
                self.config.BaseStation1000[1].moisture_sensors[moisture_sensors].bicoder.do_self_test()
            for master_valves in self.config.mv_ad_range:
                self.config.BaseStation1000[1].master_valves[master_valves].bicoder.do_self_test()
            for flow_meters in self.config.fm_ad_range:
                self.config.BaseStation1000[1].flow_meters[flow_meters].bicoder.do_self_test()
            for event_switches in self.config.sw_ad_range:
                self.config.BaseStation1000[1].event_switches[event_switches].bicoder.do_self_test()
            for temperature_sensors in self.config.ts_ad_range:
                self.config.BaseStation1000[1].temperature_sensors[temperature_sensors].bicoder.do_self_test()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_11(self):
        """
        this goes out and verifies the status on each zone and program all should be set to done
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].set_controller_to_run()
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[49].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[50].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[97].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[98].statuses.verify_status_is_disabled()
            self.config.BaseStation1000[1].zones[99].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[100].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[30].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_12(self):
        """
        verify the full configuration of the controller
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_13(self):
        """
        this reboot the controller
        waits 10 seconds after controller reconnects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # reboot controller
            self.config.BaseStation1000[1].do_reboot_controller()
            sleep(10)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_14(self):
        """
        after the reboot all of the devices get set to default values in the controller so you have to do a self test
        and than do an update to get each object to match what the controller has
        perform a test on all zones to verify that they are functioning properly \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].bicoder.self_test_and_update_object_attributes()
            for moisture_sensors in self.config.ms_ad_range:
                self.config.BaseStation1000[1].moisture_sensors[moisture_sensors]\
                    .bicoder.self_test_and_update_object_attributes()
            for master_valves in self.config.mv_ad_range:
                self.config.BaseStation1000[1].master_valves[master_valves]\
                    .bicoder.self_test_and_update_object_attributes()
            for flow_meters in self.config.fm_ad_range:
                self.config.BaseStation1000[1].flow_meters[flow_meters]\
                    .bicoder.self_test_and_update_object_attributes()
            for event_switches in self.config.sw_ad_range:
                self.config.BaseStation1000[1].event_switches[event_switches]\
                    .bicoder.self_test_and_update_object_attributes()
            for temperature_sensors in self.config.ts_ad_range:
                self.config.BaseStation1000[1].temperature_sensors[temperature_sensors]\
                    .bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_15(self):
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Assign a design flow value to each zone so that they have a default setting
            self.config.BaseStation1000[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
