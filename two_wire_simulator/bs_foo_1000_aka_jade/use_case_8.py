import sys
from time import sleep
from datetime import datetime, timedelta
from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.logging_handler import log_handler
from common.imports import opcodes
from common import helper_methods

__author__ = 'Tige'


class ControllerUseCase8(object):
    """
    Test name: \n
        -  Stat Times Test \n
    Coverage Area:
        1. test sever start times turning on at one time \n
        2. test the controller hitting a second start time while zones are watering \n
        3. verify that odd zones run for 1 minute and even zones run for 2 minutes \n
        4. tests that only 5 Programs can start at one do to memory in the 1000 even when 20 program are requested to start \n
        5. able to set zone concurrency for the controller and each program. controller has 2 max zones and each program
           has 1 max zone \n
        6. verify that 2 zones are always running \n
        7. Test the when new start times are reach the controller starts over
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    ###################################################################################################################
    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """
        time_it_should_take_test_to_run = timedelta(minutes=15)  # this is minutes
        time_test_started = datetime.now()
        try:
            number_of_retries = 1
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.test_elapsed_time(_time_started=time_test_started,
                                                     _time_expected_to_run=time_it_should_take_test_to_run)
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_3(self):
        """
        ############################
        Setup Programs
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].set_max_concurrent_zones(_max_zones=2)
            for program in range(1, 21):
                self.config.BaseStation1000[1].add_program_to_controller(_program_address=program)
                self.config.BaseStation1000[1].programs[program].set_max_concurrent_zones(_number_of_zones=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_4(self):
        """
        set up start stop pause conditions
            Program 1 - 20:
                - start times 12:00 pm and a 11:45 pm\n
                - watering days are set to weekly \n
                - Watering days enabled are every day\n
                - assign moisture sensor SB05308 to start program 1 and give it a threshold of 25 \n
        """
        start_times = [0, 1425]
        every_day_watering = [1, 1, 1, 1, 1, 1, 1]  # runs every day
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for program in range(1, 21):
                self.config.BaseStation1000[1].programs[program].add_date_time_start_condition(_condition_number=1)
                self.config.BaseStation1000[1].programs[program].date_time_start_conditions[1].set_day_time_start(
                    _st_list=start_times,
                    _interval_type=opcodes.week_days,
                    _interval_args=every_day_watering
                )

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_5(self):
        """
        assign each zone to their own program and a run time either 1 minute or 2 minutes \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation1000[1].programs[1].zone_programs[1].set_run_time(_minutes=1)

            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=2)
            self.config.BaseStation1000[1].programs[2].zone_programs[2].set_run_time(_minutes=2)

            self.config.BaseStation1000[1].programs[3].add_zone_to_program(_zone_address=3)
            self.config.BaseStation1000[1].programs[3].zone_programs[3].set_run_time(_minutes=1)

            self.config.BaseStation1000[1].programs[4].add_zone_to_program(_zone_address=4)
            self.config.BaseStation1000[1].programs[4].zone_programs[4].set_run_time(_minutes=2)

            self.config.BaseStation1000[1].programs[5].add_zone_to_program(_zone_address=5)
            self.config.BaseStation1000[1].programs[5].zone_programs[5].set_run_time(_minutes=1)

            self.config.BaseStation1000[1].programs[6].add_zone_to_program(_zone_address=6)
            self.config.BaseStation1000[1].programs[6].zone_programs[6].set_run_time(_minutes=2)

            self.config.BaseStation1000[1].programs[7].add_zone_to_program(_zone_address=7)
            self.config.BaseStation1000[1].programs[7].zone_programs[7].set_run_time(_minutes=1)

            self.config.BaseStation1000[1].programs[8].add_zone_to_program(_zone_address=8)
            self.config.BaseStation1000[1].programs[8].zone_programs[8].set_run_time(_minutes=2)

            self.config.BaseStation1000[1].programs[9].add_zone_to_program(_zone_address=9)
            self.config.BaseStation1000[1].programs[9].zone_programs[9].set_run_time(_minutes=1)

            self.config.BaseStation1000[1].programs[10].add_zone_to_program(_zone_address=10)
            self.config.BaseStation1000[1].programs[10].zone_programs[10].set_run_time(_minutes=2)

            self.config.BaseStation1000[1].programs[11].add_zone_to_program(_zone_address=11)
            self.config.BaseStation1000[1].programs[11].zone_programs[11].set_run_time(_minutes=1)

            self.config.BaseStation1000[1].programs[12].add_zone_to_program(_zone_address=12)
            self.config.BaseStation1000[1].programs[12].zone_programs[12].set_run_time(_minutes=2)

            self.config.BaseStation1000[1].programs[13].add_zone_to_program(_zone_address=17)
            self.config.BaseStation1000[1].programs[13].zone_programs[17].set_run_time(_minutes=1)

            self.config.BaseStation1000[1].programs[14].add_zone_to_program(_zone_address=18)
            self.config.BaseStation1000[1].programs[14].zone_programs[18].set_run_time(_minutes=2)

            self.config.BaseStation1000[1].programs[15].add_zone_to_program(_zone_address=19)
            self.config.BaseStation1000[1].programs[15].zone_programs[19].set_run_time(_minutes=1)

            self.config.BaseStation1000[1].programs[16].add_zone_to_program(_zone_address=96)
            self.config.BaseStation1000[1].programs[16].zone_programs[96].set_run_time(_minutes=2)

            self.config.BaseStation1000[1].programs[17].add_zone_to_program(_zone_address=97)
            self.config.BaseStation1000[1].programs[17].zone_programs[97].set_run_time(_minutes=1)

            self.config.BaseStation1000[1].programs[18].add_zone_to_program(_zone_address=98)
            self.config.BaseStation1000[1].programs[18].zone_programs[98].set_run_time(_minutes=2)

            self.config.BaseStation1000[1].programs[19].add_zone_to_program(_zone_address=99)
            self.config.BaseStation1000[1].programs[19].zone_programs[99].set_run_time(_minutes=1)

            self.config.BaseStation1000[1].programs[20].add_zone_to_program(_zone_address=100)
            self.config.BaseStation1000[1].programs[20].zone_programs[100].set_run_time(_minutes=2)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_6(self):
        """
        turn on sim mode in the controller \n
        stop the clock so that it can be incremented manually \n
        set the date and time of the controller so that there is a known days of the week and days of the month \n
        because of how the 1000 is set up you either have to wait a few minutes or change the dial position
        before you can start a program, after doing a search \n
        verify that none of the zones start early by checking 1 minute before the first start time \n
        zone concurrency varies between Programs odd numbered Programs have a zone concurrency of 1 meaning those zones
        should run 1 time before stopping \n
        even numbered Programs have a zone concurrency of 2 meaning those zones should run twice back to back before
        stopping \n
        verify odd Programs run 1 time before stopping and even Programs run 2 times before stopping \n
        due to a lack of memory on the 1000 only 10 Programs can run at one time \n
        set clock to 06/18/2012 12:44 this should start the zones watering \n
        \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 - 20 done watering \n
            - Zone status: \n
                - zone 1 done watering \n
                - zone 2 done watering \n
                - zone 3 done watering  \n
                - zone 4 done watering  \n
                - zone 5 done watering  \n
                - zone 6 done watering  \n
                - zone 7 done watering \n
                - zone 8 done watering  \n
                - zone 9 done watering \n
                - zone 10 done watering  \n
                - zone 11 done watering  \n
                - zone 12 done watering  \n
                - zone 17 done watering  \n
                - zone 18 done watering  \n
                - zone 19 done watering  \n
                - zone 96 done watering  \n
                - zone 97 done watering  \n
                - zone 98 done watering  \n
                - zone 99 done watering  \n
                - zone 100 done watering  \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for program in range(1, 21):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].set_date_and_time(_date='06/18/2012', _time='23:44:00')
            self.config.BaseStation1000[1].verify_date_and_time()
            self.config.BaseStation1000[1].set_controller_to_run()

            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_7(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 11:45 this should start the zones watering \n
        get data from Programs so that it can be verified against the object
        get data from zones that are addressed from the json file so that it can be verified against the object
        Verify the status for each program and zone and on the controller: \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 3):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_running()
            for program in range(3, 21):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[8].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[9].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[10].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[12].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[17].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[18].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[19].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[96].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[97].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[98].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[99].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[100].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_8(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 12:46 \n
        Verify the status for each program and zone and on the controller: \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_running()
            for program in range(4, 21):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[8].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[9].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[10].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[12].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[17].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[18].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[19].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[96].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[97].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[98].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[99].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[100].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_9(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 11:48 \n
        Verify the status for each program and zone and on the controller: \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_running()
            for program in range(6, 21):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[7].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[8].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[9].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[10].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[12].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[17].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[18].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[19].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[96].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[97].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[98].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[99].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[100].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_10(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 11:49 \n
        Verify the status for each program and zone and on the controller: \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 4):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_running()
            for program in range(7, 21):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[7].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[8].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[9].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[10].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[12].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[17].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[18].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[19].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[96].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[97].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[98].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[99].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[100].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_11(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 11:50 \n
        Verify the status for each program and zone and on the controller: \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 6):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_done()
            for program in range(6, 8):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_running()
            for program in range(8, 21):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[7].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[8].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[9].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[10].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[12].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[17].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[18].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[19].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[96].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[97].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[98].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[99].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[100].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_12(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 11:51 \n
        Verify the status for each program and zone and on the controller: \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 8):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[8].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[9].statuses.verify_status_is_running()
            for program in range(10, 21):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[8].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[9].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[10].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[11].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[12].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[17].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[18].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[19].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[96].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[97].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[98].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[99].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[100].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_13(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 11:52 \n
        Verify the status for each program and zone and on the controller: \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 8):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[8].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[9].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[10].statuses.verify_status_is_running()
            for program in range(11, 21):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[8].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[9].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[10].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[11].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[12].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[17].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[18].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[19].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[96].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[97].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[98].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[99].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[100].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_14(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 11:53 \n
        Verify the status for each program and zone and on the controller: \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 10):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[10].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[11].statuses.verify_status_is_running()
            for program in range(12, 21):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[8].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[9].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[10].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[11].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[12].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[17].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[18].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[19].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[96].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[97].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[98].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[99].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[100].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_15(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 11:54 \n
        Verify the status for each program and zone and on the controller: \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 11):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[12].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[13].statuses.verify_status_is_running()
            for program in range(14, 21):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[8].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[9].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[10].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[12].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[17].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[18].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[19].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[96].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[97].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[98].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[99].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[100].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_16(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 11:55 \n
        Verify the status for each program and zone and on the controller: \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 12):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[12].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[13].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[14].statuses.verify_status_is_running()
            for program in range(15, 21):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[8].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[9].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[10].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[12].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[17].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[18].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[19].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[96].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[97].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[98].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[99].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[100].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_17(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 11:56 \n
        Verify the status for each program and zone and on the controller: \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 14):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_done()
            for program in range(14, 16):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_running()
            for program in range(18, 21):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[8].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[9].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[10].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[12].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[17].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[18].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[19].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[96].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[97].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[98].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[99].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[100].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_18(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 11:57 \n
        Verify the status for each program and zone and on the controller: \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 16):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[16].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[17].statuses.verify_status_is_running()
            for program in range(18, 21):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[8].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[9].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[10].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[12].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[17].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[18].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[19].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[96].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[97].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[98].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[99].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[100].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_19(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 11:58 \n
        Verify the status for each program and zone and on the controller: \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 16):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[16].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[17].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[18].statuses.verify_status_is_running()
            for program in range(19, 20):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[8].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[9].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[10].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[12].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[17].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[18].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[19].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[96].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[97].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[98].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[99].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[100].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_20(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 11:59 \n
        Verify the status for each program and zone and on the controller: \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 18):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[18].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[19].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[20].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[8].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[9].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[10].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[12].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[17].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[18].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[19].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[96].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[97].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[98].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[99].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[100].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_21(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 12:00 \n
        Verify the status for each program and zone and on the controller: \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=2)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            for program in range(2, 20):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].programs[20].statuses.verify_status_is_running()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[8].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[9].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[10].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[12].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[17].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[18].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[19].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[96].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[97].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[98].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[99].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[100].statuses.verify_status_is_watering()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_22(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 12:01 this should start the zones watering \n
        get data from Programs so that it can be verified against the object
        get data from zones that are addressed from the json file so that it can be verified against the object
        Verify the status for each program and zone and on the controller: \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_running()
            for program in range(4, 21):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[8].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[9].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[10].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[12].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[17].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[18].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[19].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[96].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[97].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[98].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[99].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[100].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_23(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 12:02 \n
        Verify the status for each program and zone and on the controller: \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_running()
            for program in range(5, 21):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[7].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[8].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[9].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[10].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[12].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[17].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[18].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[19].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[96].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[97].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[98].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[99].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[100].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_24(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 12:03 \n
        Verify the status for each program and zone and on the controller: \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_running()
            for program in range(6, 21):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[7].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[8].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[9].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[10].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[12].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[17].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[18].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[19].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[96].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[97].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[98].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[99].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[100].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_25(self):
        """
        advance the clock 1 minutes to make the time 06/18/2012 12:04 \n
        Verify the status for each program and zone and on the controller: \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 6):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[7].statuses.verify_status_is_running()
            for program in range(8, 21):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[7].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[8].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[9].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[10].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[12].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[17].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[18].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[19].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[96].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[97].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[98].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[99].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[100].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_26(self):
        """
        advance the clock 12 minutes to make the time 06/18/2012 12:16 \n
        Verify the status for each program and zone and on the controller: \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=12)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[8].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[9].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[10].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[12].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[17].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[18].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[19].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[96].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[97].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[98].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[99].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[100].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]