import sys
from time import sleep
from datetime import datetime, timedelta
from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.logging_handler import log_handler
from common import helper_methods
from common.imports import opcodes

__author__ = "baseline"


class ControllerUseCase11(object):
    """
    Test name: \n
        Replacing Devices Test \n
    Purpose: \n
        The purpose of this test is to make sure that we can replace devices in a specific address and everything will
        still function as expected \n
    Coverage area: \n
        1. Set up the controller \n
        2. Search and assign: \n
            - Zones \n
            - Master Valves \n
            - Moisture Sensors \n
            - Temperature Sensors \n
            - Event Switch \n
            - Flow Meters \n
        3. Give the zones a design flow \n
        4. Set up Programs and give them a start times \n
        5. Assign zones to Programs and give them run times \n
        6. Set a start condition for each moisture sensor and then set their moisture percent to hit the lower limit \n
        7. Set a stop/pause condition for each temp sensor and then set their temperature to hit the thresholds \n
        8. Set a pause condition for an event switch to pause program 30 when contacts are open \n
        9. Assign master valves to Programs 1 and 3, and POC 1 and 3, and set the 'normally open state' \n
        10. Set flow meter values \n
        11. Set up the POC objects and assign master valves to them \n
        12. Reboot the controller and then set it to sim mode again, and turn on the faux io \n
        13. Reset the moisture/temperature sensor and flow meter values \n
        14. Perform a self test on all the devices to make sure the default values from the controller are stored
            in the objects \n
        15. Verify the full configuration \n
        16. Load the devices that are going to be used as replacements \n
        17. Initialize the new zones and replace the zones at address' 1, 2, 99, and 100 \n
        18. Replace soil moisture sensor at address 2 with sensor SB07263 \n
        19. Replace the master valve at address 7 with TSQ0084 \n
        20. Perform a test on all devices to make sure the new devices are working properly \n
        21. Verify the full configuration with the new devices \n
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """
        time_it_should_take_test_to_run = timedelta(minutes=15)  # this is minutes
        time_test_started = datetime.now()
        try:
            number_of_retries = 1
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.test_elapsed_time(_time_started=time_test_started,
                                                     _time_expected_to_run=time_it_should_take_test_to_run)
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        Over write zone values: \n
        set up the zones \n
            - Change the design flow for each zone \n
            - disable zone 98
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].set_max_concurrent_zones(_max_zones=4)
            self.config.BaseStation1000[1].zones[1].set_design_flow(_gallons_per_minute=10)
            self.config.BaseStation1000[1].zones[2].set_design_flow(_gallons_per_minute=10)
            self.config.BaseStation1000[1].zones[49].set_design_flow(_gallons_per_minute=30)
            self.config.BaseStation1000[1].zones[50].set_design_flow(_gallons_per_minute=30)
            self.config.BaseStation1000[1].zones[97].set_design_flow(_gallons_per_minute=80)
            self.config.BaseStation1000[1].zones[98].set_disabled()
            self.config.BaseStation1000[1].zones[99].set_design_flow(_gallons_per_minute=80)
            self.config.BaseStation1000[1].zones[100].set_design_flow(_gallons_per_minute=100)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_2(self):
        """
        ############################
        Setup Programs
        ############################
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        daily_water_window = ['011111100001111111111110']
        fully_open_water_window = ['111111111111111111111111']
        weekly_water_windows = ['011111100001111111111110', '011111100000111111111110', '111111100000000000011110',
                                '111111100000000000011110', '111111100000000000011110', '111111100000000000011110',
                                '111111100000000000011110']
        try:
            self.config.BaseStation1000[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation1000[1].programs[1].set_daily_water_window(_ww=daily_water_window)
            self.config.BaseStation1000[1].programs[1].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation1000[1].programs[1].set_seasonal_adjust(_percent=100)
            self.config.BaseStation1000[1].programs[1].set_soak_cycle_mode(_mode=opcodes.program_cycles)

            self.config.BaseStation1000[1].programs[1].set_cycle_count(_new_count=4)
            self.config.BaseStation1000[1].programs[1].set_soak_time_in_seconds(_seconds=300)

            self.config.BaseStation1000[1].add_program_to_controller(_program_address=3)
            self.config.BaseStation1000[1].programs[3].set_weekly_water_window(_ww=weekly_water_windows)
            self.config.BaseStation1000[1].programs[3].set_max_concurrent_zones(_number_of_zones=5)
            self.config.BaseStation1000[1].programs[3].set_seasonal_adjust(_percent=50)
            self.config.BaseStation1000[1].programs[3].set_soak_cycle_mode(_mode=opcodes.intelligent_soak)
            self.config.BaseStation1000[1].programs[3].set_cycle_count(_new_count=6)
            self.config.BaseStation1000[1].programs[3].set_soak_time_in_seconds(_seconds=600)

            self.config.BaseStation1000[1].add_program_to_controller(_program_address=30)
            self.config.BaseStation1000[1].programs[30].set_weekly_water_window(_ww=weekly_water_windows)
            self.config.BaseStation1000[1].programs[30].set_max_concurrent_zones(_number_of_zones=5)
            self.config.BaseStation1000[1].programs[30].set_seasonal_adjust(_percent=150)
            self.config.BaseStation1000[1].programs[30].set_soak_cycle_mode(_mode=opcodes.zone_soak_cycles)
            self.config.BaseStation1000[1].programs[30].set_cycle_count(_new_count=8)
            self.config.BaseStation1000[1].programs[30].set_soak_time_in_seconds(_seconds=1200)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_3(self):

        """
        ############################

        ############################
        """

        daily_water_window = ['011111100001111111111110']
        weekly_water_windows = ['011111100001111111111110', '011111100001111111111110', '011111100001111111111110',
                                '011111100001111111111110', '011111100001111111111110', '011111100001111111111110',
                                '011111100001111111111110']
        mon_wed_fri_watering_days = [0, 1, 0, 1, 0, 1, 0]  # runs monday, wednesday, friday
        semi_monthly = [0, 0, 0, 0, 0, 0, 8, 8, 6, 5, 5, 4, 3, 3, 3, 3, 4, 5, 6, 7, 0, 0, 0, 0]

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].add_date_time_start_condition(_condition_number=1)
            self.config.BaseStation1000[1].programs[3].add_date_time_start_condition(_condition_number=3)
            self.config.BaseStation1000[1].programs[30].add_date_time_start_condition(_condition_number=30)

            self.config.BaseStation1000[1].programs[1].date_time_start_conditions[1].set_day_time_start(
                _dt='TR',
                _st_list=[480, 540, 600, 660],
                _interval_type=opcodes.week_days,
                _interval_args=mon_wed_fri_watering_days)

            self.config.BaseStation1000[1].programs[3].date_time_start_conditions[3].set_day_time_start(
                _dt='TR',
                _st_list=[480, 540, 600, 660],
                _interval_type=opcodes.calendar_interval,
                _interval_args=opcodes.odd_day)

            self.config.BaseStation1000[1].programs[30].date_time_start_conditions[30].set_day_time_start(
                _dt='TR',
                _st_list=[480, 540, 600, 660],
                _interval_type=opcodes.semi_month_interval,
                _interval_args=semi_monthly)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_4(self):
        """
        ############################
        Setup Zone Programs
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation1000[1].programs[1].zone_programs[1].set_run_time(_minutes=15)

            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation1000[1].programs[1].zone_programs[2].set_run_time(_minutes=15)

            self.config.BaseStation1000[1].programs[3].add_zone_to_program(_zone_address=49)
            self.config.BaseStation1000[1].programs[3].zone_programs[49].set_run_time(_minutes=20)

            self.config.BaseStation1000[1].programs[3].add_zone_to_program(_zone_address=50)
            self.config.BaseStation1000[1].programs[3].zone_programs[50].set_run_time(_minutes=20)

            self.config.BaseStation1000[1].programs[30].add_zone_to_program(_zone_address=97)
            self.config.BaseStation1000[1].programs[30].zone_programs[97].set_run_time(_minutes=33)

            self.config.BaseStation1000[1].programs[30].add_zone_to_program(_zone_address=98)
            self.config.BaseStation1000[1].programs[30].zone_programs[98].set_run_time(_minutes=33)

            self.config.BaseStation1000[1].programs[30].add_zone_to_program(_zone_address=99)
            self.config.BaseStation1000[1].programs[30].zone_programs[99].set_run_time(_minutes=33)

            self.config.BaseStation1000[1].programs[30].add_zone_to_program(_zone_address=100)
            self.config.BaseStation1000[1].programs[30].zone_programs[100].set_run_time(_minutes=33)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_5(self):
        """
        Set up two separate moisture conditions \n
            - Set the moisture percents on both moisture sensors to be below their Programs lower limit \n
            - Self test the moisture sensor to update their values \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # self.config.BaseStation1000[1].programs[1].add_date_time_start_condition(_condition_number=4)
            self.config.BaseStation1000[1].programs[1].add_moisture_start_condition(_moisture_sensor_address=2)
            self.config.BaseStation1000[1].programs[1].moisture_start_conditions[2].set_moisture_mode_to_lower_limit()
            self.config.BaseStation1000[1].programs[1].moisture_start_conditions[2].set_moisture_threshold(_percent=24)

            # self.config.BaseStation1000[1].programs[30].add_date_time_start_condition(_condition_number=5)
            self.config.BaseStation1000[1].programs[30].add_moisture_start_condition(_moisture_sensor_address=1)
            self.config.BaseStation1000[1].programs[30].moisture_start_conditions[1].set_moisture_mode_to_lower_limit()
            self.config.BaseStation1000[1].programs[30].moisture_start_conditions[1].set_moisture_threshold(_percent=38)

            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=26.0)
            self.config.BaseStation1000[1].moisture_sensors[2].bicoder.set_moisture_percent(_percent=10.0)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_two_wire_drop_value(1.8)
            self.config.BaseStation1000[1].moisture_sensors[2].bicoder.set_two_wire_drop_value(1.6)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            self.config.BaseStation1000[1].moisture_sensors[2].bicoder.do_self_test()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_6(self):
        """
        Set up temperature conditions \n
            - Set temperature sensor TAT0001 to pause program 1 when the sensor reads below 32 degrees \n
            - Set temperature sensor TAT0002 to stop program 3 when the sensor reads below 60 degrees \n
            - Set the temperature reading of both sensors to 26 \n
            - Set the two wire drop value of TAT0001 and TAT0002 to 1.7 and 1.8, respectively \n
        Do a self test to update the values on both temperature sensors \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].add_temperature_pause_condition(_temperature_sensor_address=1)
            self.config.BaseStation1000[1].programs[1].temperature_pause_conditions[1]\
                .set_temperature_mode_to_lower_limit()
            self.config.BaseStation1000[1].programs[1].temperature_pause_conditions[1]\
                .set_temperature_threshold(_degrees=32)

            self.config.BaseStation1000[1].programs[3].add_temperature_stop_condition(_temperature_sensor_address=2)
            self.config.BaseStation1000[1].programs[3].temperature_stop_conditions[2]\
                .set_temperature_mode_to_lower_limit()
            self.config.BaseStation1000[1].programs[3].temperature_stop_conditions[2]\
                .set_temperature_threshold(_degrees=60)

            self.config.BaseStation1000[1].temperature_sensors[1].bicoder.set_temperature_reading(_degrees=26.0)
            self.config.BaseStation1000[1].temperature_sensors[2].bicoder.set_temperature_reading(_degrees=26.0)
            self.config.BaseStation1000[1].temperature_sensors[1].bicoder.set_two_wire_drop_value(_value=1.7)
            self.config.BaseStation1000[1].temperature_sensors[2].bicoder.set_two_wire_drop_value(_value=1.8)
            self.config.BaseStation1000[1].temperature_sensors[1].bicoder.do_self_test()
            self.config.BaseStation1000[1].temperature_sensors[2].bicoder.do_self_test()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_7(self):
        """
        Set up event switch conditions \n
            - Set event switch TPD0001 to pause program 30 when the contracts open \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[30].add_switch_pause_condition(_event_switch_address=1)
            self.config.BaseStation1000[1].programs[30].event_switch_pause_conditions[1].set_switch_mode_to_open()
            # self.config.BaseStation1000[1].programs[30].temperature_pause_conditions[1]\
            #     .set_temperature_threshold(_degrees=32)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_8(self):
        """
        set up master valve assignments \n
            - Assign master valves to Programs 1 and 3 \n
            - Assign master valves to POCs 1 and 3 \n
            - Set the 'normally open state' on each master valve \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].add_master_valve(_mv_address=1)
            self.config.BaseStation1000[1].programs[3].add_master_valve(_mv_address=2)

            self.config.BaseStation1000[1].master_valves[1].set_normally_open_state(_normally_open=opcodes.false)
            self.config.BaseStation1000[1].master_valves[2].set_normally_open_state(_normally_open=opcodes.false)
            self.config.BaseStation1000[1].master_valves[7].set_normally_open_state(_normally_open=opcodes.true)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_9(self):
        """
        Set the enable state on both flow meters to be true \n
            - Set the k values for both flow meters \n
            - Set the flow rate for both flow meters \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].flow_meters[1].set_enabled()
            self.config.BaseStation1000[1].flow_meters[2].set_enabled()
            self.config.BaseStation1000[1].flow_meters[1].bicoder.set_k_value(_value=3.10)
            self.config.BaseStation1000[1].flow_meters[2].bicoder.set_k_value(_value=5.01)
            self.config.BaseStation1000[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=25)
            self.config.BaseStation1000[1].flow_meters[2].bicoder.set_flow_rate(_gallons_per_minute=50)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_10(self):
        """
        ############################
        Setup Points of Connection
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].add_poc(_poc_address=1)
            self.config.BaseStation1000[1].points_of_control[1].add_flow_meter_to_point_of_control(
                _flow_meter_address=1)
            self.config.BaseStation1000[1].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=2)
            self.config.BaseStation1000[1].points_of_control[1].set_target_flow(_gpm=500)
            self.config.BaseStation1000[1].points_of_control[1].set_high_flow_limit(_limit=550,
                                                                                    with_shutdown_enabled=True)
            self.config.BaseStation1000[1].points_of_control[1].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=True)
            self.config.BaseStation1000[1].points_of_control[1].set_limit_concurrent_to_target_state_enabled()
            self.config.BaseStation1000[1].points_of_control[1].set_fill_time(_minutes=2)
            self.config.BaseStation1000[1].points_of_control[1].set_flow_variance(_percent=10)
            self.config.BaseStation1000[1].points_of_control[1].set_flow_variance_enabled()
            self.config.BaseStation1000[1].points_of_control[1].set_flow_fault_state_disabled()

            self.config.BaseStation1000[1].add_poc(_poc_address=3)
            self.config.BaseStation1000[1].points_of_control[3].add_flow_meter_to_point_of_control(
                _flow_meter_address=2)
            self.config.BaseStation1000[1].points_of_control[3].add_master_valve_to_point_of_control(
                _master_valve_address=7)
            self.config.BaseStation1000[1].points_of_control[3].set_target_flow(_gpm=50)
            self.config.BaseStation1000[1].points_of_control[3].set_high_flow_limit(_limit=75,
                                                                                    with_shutdown_enabled=False)
            self.config.BaseStation1000[1].points_of_control[3].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=False)
            self.config.BaseStation1000[1].points_of_control[3].set_limit_concurrent_to_target_state_enabled()
            self.config.BaseStation1000[1].points_of_control[3].set_fill_time(_minutes=4)
            self.config.BaseStation1000[1].points_of_control[3].set_flow_variance(_percent=25)
            self.config.BaseStation1000[1].points_of_control[3].set_flow_variance_enabled()
            self.config.BaseStation1000[1].points_of_control[3].set_flow_fault_state_disabled()

            # Add points of connection to programs
            self.config.BaseStation1000[1].programs[1].add_point_of_connection(_poc_address=[1])
            self.config.BaseStation1000[1].programs[3].add_point_of_connection(_poc_address=[1])
            self.config.BaseStation1000[1].programs[3].add_point_of_connection(_poc_address=[3])
            self.config.BaseStation1000[1].programs[30].add_point_of_connection(_poc_address=[3])
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_11(self):
        """
        Increment the clock by two minutes \n
        Reboot the controller. Set 10 seconds of sleep to allow it to reboot \n
        Turn on sim mode, faux io, stop the clock, and reset the date time \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=2)
            self.config.BaseStation1000[1].do_reboot_controller()
            sleep(10)
            self.config.BaseStation1000[1].set_date_and_time(_date='01/01/2014', _time='01:00:00')
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_12(self):
        """
        Set both moisture sensor values so the Programs can start watering and not be effected \n
        Set both temperature sensor values so that the Programs can start watering and not be effected \n
        Set flow meter readings \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=26.0)
            self.config.BaseStation1000[1].moisture_sensors[2].bicoder.set_moisture_percent(_percent=10.0)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_two_wire_drop_value(_value=1.8)
            self.config.BaseStation1000[1].moisture_sensors[2].bicoder.set_two_wire_drop_value(_value=1.6)
            self.config.BaseStation1000[1].temperature_sensors[1].bicoder.set_temperature_reading(_degrees=26.0)
            self.config.BaseStation1000[1].temperature_sensors[2].bicoder.set_temperature_reading(_degrees=26.0)
            self.config.BaseStation1000[1].temperature_sensors[1].bicoder.set_two_wire_drop_value(_value=1.7)
            self.config.BaseStation1000[1].temperature_sensors[2].bicoder.set_two_wire_drop_value(_value=1.8)
            self.config.BaseStation1000[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=25)
            self.config.BaseStation1000[1].flow_meters[2].bicoder.set_flow_rate(_gallons_per_minute=50)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_13(self):
        """
        Perform a test on all devices to verify that they are functioning properly \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].bicoder.self_test_and_update_object_attributes()
            for moisture_sensors in self.config.ms_ad_range:
                self.config.BaseStation1000[1].moisture_sensors[moisture_sensors] \
                    .bicoder.self_test_and_update_object_attributes()
            for master_valves in self.config.mv_ad_range:
                self.config.BaseStation1000[1].master_valves[master_valves] \
                    .bicoder.self_test_and_update_object_attributes()
            for flow_meters in self.config.fm_ad_range:
                self.config.BaseStation1000[1].flow_meters[flow_meters] \
                    .bicoder.self_test_and_update_object_attributes()
            for event_switches in self.config.sw_ad_range:
                self.config.BaseStation1000[1].event_switches[event_switches] \
                    .bicoder.self_test_and_update_object_attributes()
            for temperature_sensors in self.config.ts_ad_range:
                self.config.BaseStation1000[1].temperature_sensors[temperature_sensors] \
                    .bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_14(self):
        """
        verify the full configuration of the controller
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].set_controller_to_run()
            self.config.BaseStation1000[1].verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_15(self):
        """
        Load replacement devices these are not located in the json file so they must be a str \n
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].load_dv_to_cn(dv_type=opcodes.four_valve_decoder,
                                                         list_of_decoder_serial_nums=["TSQ0031"])
            self.config.BaseStation1000[1].load_dv_to_cn(dv_type=opcodes.single_valve_decoder,
                                                         list_of_decoder_serial_nums=["TSQ0084"])
            self.config.BaseStation1000[1].load_dv_to_cn(dv_type=opcodes.moisture_sensor,
                                                         list_of_decoder_serial_nums=["SB07263"])
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_16(self):
        """
        Search for zones \n
        Initialize the new zones and replace the zones at address' 1, 2, 99, and 100 \n
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_search_for_dv(dv_type=opcodes.zone)
            self.config.BaseStation1000[1].zones[1].replace_quad_valve_bicoder('TSQ0031')
            self.config.BaseStation1000[1].zones[2].replace_quad_valve_bicoder('TSQ0032')
            self.config.BaseStation1000[1].zones[99].replace_quad_valve_bicoder('TSQ0033')
            self.config.BaseStation1000[1].zones[100].replace_quad_valve_bicoder('TSQ0034')
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_17(self):
        """
        Search for moisture sensors \n
        Replace soil moisture sensor at address 2 with sensor SB07263 \n
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
            self.config.BaseStation1000[1].moisture_sensors[1].replace_moisture_bicoder('SB07263')

            # this changes the serial number in the object to match the controller
            self.config.BaseStation1000[1].programs[30].moisture_start_conditions[1].device_serial = \
                self.config.BaseStation1000[1].moisture_sensors[1].sn
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_18(self):
        """
        Search for master valves \n
        Replace the master valve at address 7 with TSQ0084 \n
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_search_for_dv(dv_type=opcodes.master_valve)
            self.config.BaseStation1000[1].master_valves[1].replace_dual_valve_bicoder(_new_serial_number='TSQ0084')
        except Exception, e:
            raise Exception(method + " " + self.config.BaseStation1000[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_20(self):
        """
        Perform a test on all devices to verify that they are functioning properly and verify the new devices work \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].bicoder.self_test_and_update_object_attributes()
            for moisture_sensors in self.config.ms_ad_range:
                self.config.BaseStation1000[1].moisture_sensors[moisture_sensors] \
                    .bicoder.self_test_and_update_object_attributes()
            for master_valves in self.config.mv_ad_range:
                self.config.BaseStation1000[1].master_valves[master_valves] \
                    .bicoder.self_test_and_update_object_attributes()
            for flow_meters in self.config.fm_ad_range:
                self.config.BaseStation1000[1].flow_meters[flow_meters] \
                    .bicoder.self_test_and_update_object_attributes()
            for event_switches in self.config.sw_ad_range:
                self.config.BaseStation1000[1].event_switches[event_switches] \
                    .bicoder.self_test_and_update_object_attributes()
            for temperature_sensors in self.config.ts_ad_range:
                self.config.BaseStation1000[1].temperature_sensors[temperature_sensors] \
                    .bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_21(self):
        """
        Verify the full configuration of the controller with the new devices \n
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].set_controller_to_run()
            self.config.BaseStation1000[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]


