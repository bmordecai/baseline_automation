import sys
from time import sleep
from datetime import datetime, timedelta
from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.logging_handler import log_handler
from common import helper_methods
from common.imports import opcodes

__author__ = 'Dillon'


class ControllerUseCase17(object):
    """
    Test name:
        - CN UseCase 17 firmware update
        - This is a manual test until Jirabug YY-261 has been resolved!!

    User Story:
        - As a user I want my controller to be able perform a firmware update without losing its configuration
        - As a user I want my controller to be able to perform a firmware downgrade without losing
          losing it's configuration

    Purpose:
        - set up a full configuration on the controller
            - verify that no programming is lost when you:
                - update the firmware of the controller
            - verify that firmware update take effects:
                - verify all programing, even with certain devices or attributes are disabled

    Coverage area:
        - setting Programs:
            - start times
            - watering days
            - water windows
            - zone concurrency
            - assign zones to Programs \n
            - give each zone a run time of 1 hour and 30 minutes
            - assign moisture sensors to a primary zone 1 and set to lower limit watering

        - setting up poc's:
            - enable POC
            - assign:
                - master valve
                - flow meter
                - target flow
                - main line
                - priority
                    - low
                    - medium
                    - high\n
            - set high flow limit
            - high flow shut down
            - firmware update from both BaseManager and usb:
            - verify message
            - verify configuration all stayed

    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    ###########################
    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """
        time_it_should_take_test_to_run = timedelta(minutes=15)  # this is minutes
        time_test_started = datetime.now()
        try:
            number_of_retries = 1
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                     # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.test_elapsed_time(_time_started=time_test_started,
                                                     _time_expected_to_run=time_it_should_take_test_to_run)
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #########################
    def step_1(self):
        """
        - sets up the controller \n
        - verify basemanager connection
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # only need this for BaseManager
            # Here we don't want to set sim mode to off because it won't allow us to increment the controller's clock for
            self.config.BaseStation1000[1].basemanager_connection[1].verify_ip_address_state()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #########################
    def step_2(self):
        """
        Setup Programs on controller
            - Setup program 1,3,4,20
            - All need water windows
            - Max concurrent zones to 1 for program 1, 3, & 20
            - Max concurrent zones to 4 for program 4
            - Each program needs seasonal adjust of 100%
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # this is set in the PG3200 object
            program_number_1_water_windows = ['011111100001111111111110']
            program_number_3_water_windows = ['011111100001111111111110']
            program_number_4_water_windows = ['011111100001111111111110']
            program_number_20_water_windows = ['011111100000111111111110']

            # Setup program 1
            self.config.BaseStation1000[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation1000[1].programs[1].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation1000[1].programs[1].set_water_window(_ww=program_number_1_water_windows)

            # Setup program 3
            self.config.BaseStation1000[1].add_program_to_controller(_program_address=3)
            self.config.BaseStation1000[1].programs[3].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation1000[1].programs[3].set_water_window(_ww=program_number_3_water_windows)

            # Setup program 4
            self.config.BaseStation1000[1].add_program_to_controller(_program_address=4)
            self.config.BaseStation1000[1].programs[4].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation1000[1].programs[4].set_water_window(_ww=program_number_4_water_windows)

            # Setup program 20
            self.config.BaseStation1000[1].add_program_to_controller(_program_address=20)
            self.config.BaseStation1000[1].programs[20].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation1000[1].programs[20].set_water_window(_ww=program_number_20_water_windows)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ##########################
    def step_3(self):
        """
        Setup zones and add to programs
            - Each zone will have a run time of 60 minutes

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation1000[1].programs[1].zone_programs[1].set_run_time(_minutes=60)

            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation1000[1].programs[1].zone_programs[2].set_run_time(_minutes=60)

            self.config.BaseStation1000[1].programs[3].add_zone_to_program(_zone_address=49)
            self.config.BaseStation1000[1].programs[3].zone_programs[49].set_run_time(_minutes=60)

            self.config.BaseStation1000[1].programs[3].add_zone_to_program(_zone_address=50)
            self.config.BaseStation1000[1].programs[3].zone_programs[50].set_run_time(_minutes=60)

            self.config.BaseStation1000[1].programs[20].add_zone_to_program(_zone_address=96)
            self.config.BaseStation1000[1].programs[20].zone_programs[96].set_run_time(_minutes=60)

            self.config.BaseStation1000[1].programs[20].add_zone_to_program(_zone_address=99)
            self.config.BaseStation1000[1].programs[20].zone_programs[99].set_run_time(_minutes=60)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ############################
    def step_4(self):
        """
        set_poc_1000
            Set up POC 1
                - flow meter address of 1
                - master valve address of 1
                - enable state set to true
                - target flow is 500 gallons per minute
                - high flow limit is 550 gallons per minute
                - shutdown on high flow is enabled
                - unscheduled flow limit is set to 10 gallons per minute
                - shutdown on unscheduled flow is enabled
            Set up POC 2
                - flow meter address of 1
                - master valve address of 1
                - enabled state set to true
                - target flow is set to 50 gallons per minute
                - high flow limit is set to 75 gallons per minute
                - shutdown on high flow is disabled
                - unscheduled flow limit is 5 gallons per minute
                - shutdown on unscheduled flow is disabled
        With the way these two POCs are set up, we have a fairly good coverage on the high and low, true or false
        variables that we want to stay consistent in a controller after an update.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Setup POC 1
            self.config.BaseStation1000[1].add_poc(_poc_address=1)
            self.config.BaseStation1000[1].points_of_control[1].set_enabled()
            self.config.BaseStation1000[1].points_of_control[1].set_target_flow(_gpm=500)
            self.config.BaseStation1000[1].points_of_control[1].add_flow_meter_to_point_of_control(_flow_meter_address=1)
            self.config.BaseStation1000[1].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=1)
            self.config.BaseStation1000[1].points_of_control[1].set_high_flow_limit(_limit=550, with_shutdown_enabled=True)
            self.config.BaseStation1000[1].points_of_control[1].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=True)

            # Setup POC 2
            self.config.BaseStation1000[1].add_poc(_poc_address=3)
            self.config.BaseStation1000[1].points_of_control[3].set_enabled()
            self.config.BaseStation1000[1].points_of_control[3].set_target_flow(_gpm=50)
            self.config.BaseStation1000[1].points_of_control[3].\
                add_master_valve_to_point_of_control(_master_valve_address=2)
            self.config.BaseStation1000[1].points_of_control[3].set_high_flow_limit(_limit=75, with_shutdown_enabled=False)
            self.config.BaseStation1000[1].points_of_control[3].set_unscheduled_flow_limit(_gallons=5,
                                                                                           with_shutdown_enabled=False)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ################################
    def step_5(self):
        """
        Disable devices
        This area covers not losing programing when a device is disabled during a firmware update or reboot
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # disable Zone (97}
            self.config.BaseStation1000[1].zones[97].set_disabled()

            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ################################
    def step_6(self):
        """
        Do a firmware update from a USB that will have an older version of firmware.
            - After the downgrade to v1.19 the normal test engine commands do not work
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Breakpoint below and perform the fw update
            print "Perform USB firmware update to v1.19 for the 1000!"
            print "After the 1000 has been updated proceed, and enable com 3 in Test Settings."
            self.config.BaseStation1000[1].stop_clock()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ################################
    def step_7(self):
        """
        Verify that all the values on our objects are still matching the values in the controller. This includes
        the firmware version.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # this also verifies firmware version
            self.config.verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ################################
    def step_8(self):
        """
        We do a firmware update from BaseManager to v1.20.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_firmware_update(were_from=opcodes.basemanager, bm_id_number=179)
            self.config.BaseStation1000[1].stop_clock()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ################################
    def step_9(self):
        """
        Verifies all of our object's variables are matching their controller counterparts after fw upgrade
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # this also verifies firmware version
            self.config.verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]




