import sys
from time import sleep
from datetime import datetime, timedelta
from common.configuration import Configuration

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr

# Objects
from common.logging_handler import log_handler
from common.imports import opcodes
from common import helper_methods
__author__ = 'Tige'


class ControllerUseCase13(object):
    """
        Test name: \n
            Watering Day Schedule 1000 Test \n
        Purpose: \n
            This test sets up different watering schedules and verifies that Programs run correctly \n
        Coverage area: \n
            1.	Set up a Programs to water for the different watering scheduled modes: \n
            2.  Set up a week days watering schedule and verify that the program only runs on week days set in the watering
                schedule \n
            3.  Set up an interval watering schedule for 3 days and verify that the program runs then skips 3 days and runs on
                the 4th \n
            4.  Set up an even days watering schedule and verify that the program only runs on even days of the week \n
            5.  Set up an odd days watering schedule and verify that the program only runs on odd days of the week \n
            6.  Set up an odd days skip 31st watering schedule and verify that the program only runs on odd days of the week
                and skips the 31st \n
            7.  Set up a historical ET calendar and verify that the set intervals work and that the interval changes every half
                a month \n
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """
        time_it_should_take_test_to_run = timedelta(minutes=40)  # this is minutes
        time_test_started = datetime.now()
        try:
            number_of_retries = 1
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.test_elapsed_time(_time_started=time_test_started,
                                                     _time_expected_to_run=time_it_should_take_test_to_run)
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        ############################
        Setup Programs
        ############################
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for programs in range(1, 7):
                self.config.BaseStation1000[1].add_program_to_controller(_program_address=programs)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_2(self):
        """
        ############################
        Setup Zone Programs
        ############################
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for zone_programs in range(1, 7):
                self.config.BaseStation1000[1].programs[zone_programs].add_zone_to_program(_zone_address=zone_programs)
                self.config.BaseStation1000[1].programs[zone_programs].zone_programs[zone_programs].set_run_time(
                    _minutes=60)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_3(self):
        """
        give each program a start time \n
        this time is given in minutes past midnight \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for programs in range(1, 7):
                self.config.BaseStation1000[1].programs[programs].add_date_time_start_condition(_condition_number=1)
                self.config.BaseStation1000[1].programs[programs].date_time_start_conditions[1].set_day_time_start(
                    _st_list=[60]
                )
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_4(self):
        """
        assign each program to use a different watering days strategy so we can test each one \n
        assign program 1 to use days of the week watering \n
        assign program 2 to use event days \n
        assign program 3 to use odd days \n
        assign program 4 to use odd days skip 31st day \n
        assign program 5 to use historical day calendar \n
        assign program 6 to use day intervals \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            mon_fri_list = [0,  # Sunday
                            1,  # Monday
                            0,  # Tuesday
                            1,  # Wednesday
                            0,  # Thursday
                            1,  # Friday
                            0   # Saturday
                            ]

            semi_month_list = [7, 6,       # January 1-15, January 16-31
                               6, 6,       # February 1-15, February 16-End of month
                               4, 3,       # March 1-15, March 16-31
                               3, 3,       # April 1-15, April 16-30
                               3, 2,       # May 1-15, May 16-31
                               2, 2,       # June 1-15, June 16-30
                               2, 2,       # July 1-15, July 16-31
                               2, 2,       # August 1-15, August 16-31
                               3, 3,       # September 1-15, September 16-30
                               3, 4,       # October 1-15, October 16-31
                               6, 6,       # November 1-15, November 16-30
                               6, 7        # December 1-15, December 16-31
                               ]

            self.config.BaseStation1000[1].programs[1].date_time_start_conditions[1].set_day_time_start(
                _interval_type=opcodes.week_days,
                _interval_args=mon_fri_list
            )
            self.config.BaseStation1000[1].programs[2].date_time_start_conditions[1].set_day_time_start(
                _interval_type=opcodes.calendar_interval,
                _interval_args=opcodes.even_day
            )
            self.config.BaseStation1000[1].programs[3].date_time_start_conditions[1].set_day_time_start(
                _interval_type=opcodes.calendar_interval,
                _interval_args=opcodes.odd_day
            )
            self.config.BaseStation1000[1].programs[4].date_time_start_conditions[1].set_day_time_start(
                _interval_type=opcodes.calendar_interval,
                _interval_args=opcodes.odd_days_skip_31
            )
            self.config.BaseStation1000[1].programs[5].date_time_start_conditions[1].set_day_time_start(
                _interval_type=opcodes.semi_month_interval,
                _interval_args=semi_month_list
            )
            self.config.BaseStation1000[1].programs[6].date_time_start_conditions[1].set_day_time_start(
                _interval_type=opcodes.day_interval,
                _interval_args=3
            )
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_6(self):
        """
        Verify Full Configuration
        time \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_7(self):
        """
        start of first day:
        1 turn off sim mode in the controller \n
        2 stop the clock so that it can be manually incremented \n
        3 set the date and time of the controller so that there is a known days of the week and days of the month \n
        4 increment time by one second so that the controller is at the top of the hour this will get the correct zone
        status set \n
        5 set dial to run so that the controller will work in sim mode \n
        6 increment the time on the controller long enough to cross midnight the controller uses number of time pass
        midnight to determine days since last watering \n
        7 set date and time to 1 second before the start time after each date change increment the time by one second
        to allow status to change and get to the ball rolling \n
        8 after each date a time get set we increment the time one hour at a time in order to see the watering patterns
         \n
        9 at the end of each hour get zone status and verify the watering behavior is correct: \n
        10 on first day only Programs 1 and 2 should water program 1 because its a monday and program 2 because it is an
        even day \n
        11 second day program 3 and 4 should run because they are odd days \n
        12 zone five is set to smart watering and will water ever third day \n
        13 zone six is set to day intervals and will water ever second day \n
            turn on sim mode in the controller \n
            stop the clock so that it can be incremented manually \n
            set the date and time of the controller so that there is a known days of the week and days of the
            month \n
            because of how the 1000 is set up you either have to wait a few minutes or change the dial position
            before you can start a program, after doing a search \n
            verify that none of the zones start early by checking 1 minute before the first start time \n
            zone concurrency varies between Programs odd numbered Programs have a zone concurrency of 1 meaning
            those zones should run 1 time before stopping \n
        set clock to 06/18/2012 12:59:59  which should start the zones watering \n
        increment clock 1 second to 6/18/2012 1:00 pm \n
        Verify the status for each program and zone and on the controller: \n
        get data from controller for each program \n
        get data from controller for each zone \n
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
                - Program 2 waiting
                - Program 3 done watering
                - Program 4 done watering
                - Program 5 done watering
                - Program 6 done watering
            - Zone Status
                - Zone 1 watering
                - Zone 2 waiting to water
                - Zone 3 done watering
                - Zone 4 done watering
                - Zone 5 done watering
                - Zone 6 done watering
            """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].set_date_and_time(_date='06/18/2012', _time='00:59:59')
            self.config.BaseStation1000[1].set_controller_to_run()
            # self.config.BaseStation1000[1].do_increment_clock(seconds=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_8(self):
        """
        First Day Second Hour \n
        increment clock 1 hour to 6/18/2012, which will make the time 2:00 A.M.\n
        Verify the status for each program and zone and on the controller: \n
        - Program Status
            - Program 1 done watering
            - Program 2 running
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 done watering
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 watering
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 done watering
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_9(self):
        """
        First Day Third Hour \n
        set clock to 06/18/2012 and increment by one hour, which will make a time of 3:00 A.M. \n
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 - 6 done watering
            - Zone Status
                - Zone 1 - 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_done()
            for zones in range(1, 7):
                self.config.BaseStation1000[1].zones[zones].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_10(self):
        """
        Second Day First Hour \n
        increment clock 21 hours and 15 minutes to 6/19/2012 \n
        set clock to 06/19/2012 12:59:59 one second before the zones begin watering \n
        Increment clock 1 second to 6/19/2012 1:00 pm \n
        Verify the status for each program and zone and on the controller: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 running
            - Program 4 waiting to water
            - Program 5 done watering
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 watering
            - Zone 4 waiting to water
            - Zone 5 done watering
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # self.config.BaseStation1000[1].set_date_and_time(_date='06/18/2012', _time='23:55:00')
            # self.config.BaseStation1000[1].do_increment_clock(minutes=6)
            self.config.BaseStation1000[1].do_increment_clock(hours=21, minutes=15)
            self.config.BaseStation1000[1].set_date_and_time(_date='06/19/2012', _time='00:59:59')
            self.config.BaseStation1000[1].do_increment_clock(seconds=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                         self.config.test_name,
                         sys._getframe().f_code.co_name,
                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                         str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_11(self):
        """
        Second Day Second Hour \n
        Increment the time by one hour, which will make a time of 2:00 pm. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 done watering
            - Program 4 running
            - Program 5 done watering
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 done watering
            - Zone 4 watering
            - Zone 5 done watering
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_12(self):
        """
        Second Day Third Hour \n
        Increment the time by one hour, which will make a time of 3:00 pm. \n
        Then verify the statuses of the following: \n
            - Program Status
                - Program 1 - 6 done watering
            - Zone Status
                - Zone 1 - 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_done()
            for zones in range(1, 7):
                self.config.BaseStation1000[1].zones[zones].statuses.verify_status_is_done()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.BaseStation1000[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + str(ae.message))

    def step_13(self):
        """
        Second Day Fourth Hour \n
        Increment the time by one hour, which will make a time of 4:00 pm. \n
        Then verify the statuses of the following: \n
           - Program Status
                - Program 1 - 6 done watering
           - Zone Status
                - Zone 1 - 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_done()
            for zones in range(1, 7):
                self.config.BaseStation1000[1].zones[zones].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_14(self):
        """
        Third Day First Hour
        set time twenty one hours and fifteen minutes later \n
        set clock to 06/20/2012 12:59 one second before the zones begin watering \n
        Increment the clock by one second to 1:00 pm. \n
        Verify the status for each program and zone and on the controller: \n
        - Program Status
            - Program 1 running
            - Program 2 waiting to water
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 waiting to water
            - Program 6 done watering
        - Zone Status
            - Zone 1 watering
            - Zone 2 waiting to water
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 waiting to water
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # self.config.BaseStation1000[1].set_date_and_time(_date='06/19/2012', _time='23:55:00')
            # self.config.BaseStation1000[1].do_increment_clock(minutes=6)
            self.config.BaseStation1000[1].do_increment_clock(hours=20, minutes=15)
            self.config.BaseStation1000[1].set_date_and_time(_date='06/20/2012', _time='00:59:59')
            self.config.BaseStation1000[1].do_increment_clock(seconds=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_15(self):
        """
        Third Day Second Hour \n
        Increment the time by one hour, which will make a time of 2:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 running
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 waiting to water
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 watering
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 waiting to water
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_16(self):
        """
        Third Day Third Hour \n
        Increment the time by one hour, which will make a time of 3:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 running
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 waiting to water
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_17(self):
        """
        Third Day Fourth Hour \n
        Increment the time by one hour, which will make a time of 4:00 A.M . \n
        Then verify the statuses of the following: \n
           - Program Status
                - Program 1 - 6 done watering
           - Zone Status
                - Zone 1 - 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_done()
            for zones in range(1, 7):
                self.config.BaseStation1000[1].zones[zones].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_18(self):
        """
        Fourth Day First Hour
        set time twenty one hours and fifteen minutes later \n
        set clock to 06/21/2012 12:59 one second before the zones begin watering \n
        Increment the clock by one second which will make a time of 1:00 A.M \n
        Verify the status for each program and zone and on the controller: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 running
            - Program 4 waiting to water
            - Program 5 done watering
            - Program 6 waiting to water
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 watering
            - Zone 4 waiting to water
            - Zone 5 done watering
            - Zone 6 waiting to water
                """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # self.config.BaseStation1000[1].set_date_and_time(_date='06/20/2012', _time='23:55:00')
            # self.config.BaseStation1000[1].do_increment_clock(minutes=6)
            self.config.BaseStation1000[1].do_increment_clock(hours=20, minutes=15)
            self.config.BaseStation1000[1].set_date_and_time(_date='06/21/2012', _time='00:59:59')
            self.config.BaseStation1000[1].do_increment_clock(seconds=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_19(self):
        """
        Fourth Day Second Hour \n
        Increment the time by one hour, which will make a time of 2:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 done watering
            - Program 4 running
            - Program 5 done watering
            - Program 6 waiting to water
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 done watering
            - Zone 4 watering
            - Zone 5 done watering
            - Zone 6 waiting to water
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_20(self):
        """
        Fourth Day Third Hour \n
        Increment the time by one hour, which will make a time of 3:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 done watering
            - Program 6 running
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 done watering
            - Zone 6 watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_running()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_watering()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_21(self):
        """
        Fifth Day First Hour
        set time twenty one hours and fifteen minutes later \n
        set clock to 06/22/2012 12:59 one second before the zones begin watering \n
        Increment the clock by one second, which will make a time of 1:00 A.M. \n
        Verify the status for each program and zone and on the controller: \n
        - Program Status
            - Program 1 running
            - Program 2 waiting to water
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 waiting to water
            - Program 6 done watering
        - Zone Status
            - Zone 1 watering
            - Zone 2 waiting to water
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 waiting to water
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # self.config.BaseStation1000[1].set_date_and_time(_date='06/21/2012', _time='23:55:00')
            # self.config.BaseStation1000[1].do_increment_clock(minutes=6)
            self.config.BaseStation1000[1].do_increment_clock(hours=21, minutes=15)
            self.config.BaseStation1000[1].set_date_and_time(_date='06/22/2012', _time='00:59:59')
            self.config.BaseStation1000[1].do_increment_clock(seconds=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_22(self):
        """
        Fifth Day Second Hour \n
        Increment the time by one hour, which will make a time of 2:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 running
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 waiting to water
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 watering
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 waiting to water
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_23(self):
        """
        Fifth Day Third Hour \n
        Increment the time by one hour, which will make a time of 3:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 running
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 watering
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_24(self):
        """
        Fifth Day Fourth Hour \n
        Increment the time by one hour, which will make a time of 4:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 - 6 done watering
        - Zone Status
            - Zone 1 - 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_done()
            for zones in range(1, 7):
                self.config.BaseStation1000[1].zones[zones].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_25(self):
        """
        Sixth Day First Hour \n
        set time twenty one hours and fifteen minutes later \n
        set clock to 06/23/2012 12:59 one second before the zones begin watering \n
        Increment the clock by one second, which will make a time of 1:00 A.M. \n
        Verify the status for each program and zone and on the controller: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 running
            - Program 4 waiting to water
            - Program 5 done watering
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 watering
            - Zone 4 waiting to water
            - Zone 5 done watering
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # self.config.BaseStation1000[1].set_date_and_time(_date='06/22/2012', _time='23:55:00')
            # self.config.BaseStation1000[1].do_increment_clock(minutes=6)
            self.config.BaseStation1000[1].do_increment_clock(hours=20, minutes=15)
            self.config.BaseStation1000[1].set_date_and_time(_date='06/23/2012', _time='00:59:59')
            self.config.BaseStation1000[1].do_increment_clock(seconds=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_26(self):
        """
        Sixth Day Second Hour \n
        Increment the time by one hour, which will make a time of 2:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 done watering
            - Program 4 running
            - Program 5 done watering
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 done watering
            - Zone 4 watering
            - Zone 5 done watering
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
    def step_27(self):
        """
        Sixth Day Third Hour \n
        Increment the time by one hour, which will make a time of 3:00 A.M. \n
        Then verify the statuses of the following: \n
            - Program Status
                - Program 1 - 6 done watering
            - Zone Status
                - Zone 1 - 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_done()
            for zones in range(1, 7):
                self.config.BaseStation1000[1].zones[zones].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_28(self):
        """
        Sixth Day Fourth Hour \n
        Increment the time by one hour, which will make a time of 4:00 A.M. \n
        Then verify the statuses of the following: \n
            - Program Status
                - Program 1 - 6 done watering
            - Zone Status
                - Zone 1 - 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_done()
            for zones in range(1, 7):
                self.config.BaseStation1000[1].zones[zones].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_29(self):
        """
        Seventh Day First Hour \n
        set time twenty one hours and fifteen minutes later \n
        set clock to 06/24/2012 12:59 one second before the zones begin watering \n
        Increment the clock by one second, which will make a time of 1:00 A.M. \n
        Verify the status for each program and zone and on the controller: \n
        - Program Status
            - Program 1 done watering
            - Program 2 running
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 waiting to water
            - Program 6 waiting to water
        - Zone Status
            - Zone 1 done watering
            - Zone 2 watering
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 waiting to water
            - Zone 6 waiting to water
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # self.config.BaseStation1000[1].set_date_and_time(_date='06/23/2012', _time='23:55:00')
            # self.config.BaseStation1000[1].do_increment_clock(minutes=6)
            self.config.BaseStation1000[1].do_increment_clock(hours=20, minutes=15)
            self.config.BaseStation1000[1].set_date_and_time(_date='06/24/2012', _time='00:59:59')
            self.config.BaseStation1000[1].do_increment_clock(seconds=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_30(self):
        """
        Seventh Day Second Hour \n
        Increment the time by one hour, which will make a time of 2:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 running
            - Program 6 waiting to water
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 watering
            - Zone 6 waiting to water
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_31(self):
        """
        Seventh Day Third Hour \n
        Increment the time by one hour, which will make a time of 3:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 done watering
            - Program 6 running
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 done watering
            - Zone 6 watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_running()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_watering()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_32(self):
        """
        Seventh Day Fourth Hour \n
        Increment the time by one hour, which will make a time of 4:00 A.M. \n
        Then verify the statuses of the following: \n
            - Program Status
                - Program 1 - 6 done watering
            - Zone Status
                - Zone 1 - 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_done()
            for zones in range(1, 7):
                self.config.BaseStation1000[1].zones[zones].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_33(self):
        """
        Eighth Day First Hour \n
        set time twenty one hours and fifteen minutes later \n
        set clock to 06/25/2012 12:59 one second before the zones begin watering \n
        Increment the clock by one second, which will make a time of 1:00 A.M. \n
        Verify the status for each program and zone and on the controller: \n
        - Program Status
            - Program 1 running
            - Program 2 done watering
            - Program 3 waiting to water
            - Program 4 waiting to water
            - Program 5 done watering
            - Program 6 done watering
        - Zone Status
            - Zone 1 watering
            - Zone 2 done watering
            - Zone 3 waiting to water
            - Zone 4 waiting to water
            - Zone 5 done watering
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=20, minutes=15)
            self.config.BaseStation1000[1].set_date_and_time(_date='06/25/2012', _time='00:59:59')
            self.config.BaseStation1000[1].do_increment_clock(seconds=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_34(self):
        """
        Eighth Day Second Hour \n
        Increment the time by one hour, which will make a time of 2:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 running
            - Program 4 waiting to water
            - Program 5 done watering
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 watering
            - Zone 4 waiting to water
            - Zone 5 done watering
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_35(self):
        """
        Eighth Day Third Hour \n
        Increment the time by one hour, which will make a time of 3:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 done watering
            - Program 4 running
            - Program 5 done watering
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 done watering
            - Zone 4 watering
            - Zone 5 done watering
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_36(self):
        """
        Eighth Day Fourth Hour \n
        Increment the time by one hour, which will make a time of 4:00 A.M. \n
        Then verify the statuses of the following: \n
            - Program Status
                - Program 1 - 6 done watering
            - Zone Status
                - Zone 1 - 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_done()
            for zones in range(1, 7):
                self.config.BaseStation1000[1].zones[zones].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_37(self):
        """
        Ninth Day First Hour \n
        set time twenty one hours and fifteen minutes later \n
        set clock to 06/26/2012 12:59 one second before the zones begin watering \n
        Increment the clock by one second, which will make a time of 1:00 A.M. \n
        Verify the status for each program and zone and on the controller: \n
        - Program Status
            - Program 1 done watering
            - Program 2 running
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 waiting to water
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 watering
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 waiting to water
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=20, minutes=15)
            self.config.BaseStation1000[1].set_date_and_time(_date='06/26/2012', _time='00:59:59')
            self.config.BaseStation1000[1].do_increment_clock(seconds=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_38(self):
        """
        Ninth Day Second Hour \n
        Increment the time by one hour, which will make a time of 2:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 running
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 watering
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_39(self):
        """
        Ninth Day Third Hour \n
        Increment the time by one hour, which will make a time of 3:00 A.M. \n
        Then verify the statuses of the following: \n
            - Program Status
                - Program 1 - 6 done watering
            - Zone Status
                - Zone 1 - 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_done()
            for zones in range(1, 7):
                self.config.BaseStation1000[1].zones[zones].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_40(self):
        """
        Tenth Day First Hour \n
        set time twenty one hours and fifteen minutes later \n
        set clock to 06/27/2012 12:59 one second before the zones begin watering \n
        Increment the clock by one second, which will make a time of 1:00 A.M. \n
        Verify the status for each program and zone and on the controller: \n
        - Program Status
            - Program 1 running
            - Program 2 done watering
            - Program 3 waiting to water
            - Program 4 waiting to water
            - Program 5 done watering
            - Program 6 waiting to water
        - Zone Status
            - Zone 1 watering
            - Zone 2 done watering
            - Zone 3 waiting to water
            - Zone 4 waiting to water
            - Zone 5 done watering
            - Zone 6 waiting to water
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=21, minutes=15)
            self.config.BaseStation1000[1].set_date_and_time(_date='06/27/2012', _time='00:59:59')
            self.config.BaseStation1000[1].do_increment_clock(seconds=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_41(self):
        """
        Tenth Day Second Hour \n
        Increment the time by one hour, which will make a time of 2:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 running
            - Program 4 waiting to water
            - Program 5 done watering
            - Program 6 waiting to water
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 watering
            - Zone 4 waiting to water
            - Zone 5 done watering
            - Zone 6 waiting to water
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_42(self):
        """
        Tenth Day Third Hour \n
        Increment the time by one hour, which will make a time of 3:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 done watering
            - Program 4 running
            - Program 5 done watering
            - Program 6 waiting to water
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 done watering
            - Zone 4 watering
            - Zone 5 done watering
            - Zone 6 waiting to water
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_43(self):
        """
        Tenth Day Fourth Hour \n
        Increment the time by one hour, which will make a time of 4:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 done watering
            - Program 6 running
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 done watering
            - Zone 6 watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_running()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_watering()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_44(self):
        """
        Tenth Day Fifth Hour \n
        Increment the time by one hour, which will make a time of 5:00 A.M. \n
        Then verify the statuses of the following: \n
            - Program Status
                - Program 1 - 6 done watering
            - Zone Status
                - Zone 1 - 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_done()
            for zones in range(1, 7):
                self.config.BaseStation1000[1].zones[zones].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_45(self):
        """
        Eleventh Day First Hour \n
        set time twenty one hours and fifteen minutes later \n
        set clock to 06/28/2012 12:59 one second before the zones begin watering \n
        Increment the clock by one second, which will make a time of 1:00 A.M. \n
        Verify the status for each program and zone and on the controller: \n
        - Program Status
            - Program 1 done watering
            - Program 2 running
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 waiting to water
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 watering
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 waiting to water
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=19, minutes=15)
            self.config.BaseStation1000[1].set_date_and_time(_date='06/28/2012', _time='00:59:59')
            self.config.BaseStation1000[1].do_increment_clock(seconds=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_46(self):
        """
        Eleventh Day Second Hour \n
        Increment the time by one hour, which will make a time of 2:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 running
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 watering
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_47(self):
        """
        Eleventh Day Third Hour \n
        Increment the time by one hour, which will make a time of 3:00 A.M. \n
        Then verify the statuses of the following: \n
            - Program Status
                - Program 1 - 6 done watering
            - Zone Status
                - Zone 1 - 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_done()
            for zones in range(1, 7):
                self.config.BaseStation1000[1].zones[zones].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_48(self):
        """
        Twelfth Day First Hour \n
        set time twenty one hours and fifteen minutes later \n
        set clock to 06/29/2012 12:59 one second before the zones begin watering \n
        Increment the clock by one second, which will make a time of 1:00 A.M. \n
        Verify the status for each program and zone and on the controller: \n
        - Program Status
            - Program 1 running
            - Program 2 done watering
            - Program 3 waiting to water
            - Program 4 waiting to water
            - Program 5 done watering
            - Program 6 done watering
        - Zone Status
            - Zone 1 watering
            - Zone 2 done watering
            - Zone 3 waiting to water
            - Zone 4 waiting to water
            - Zone 5 done watering
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=21, minutes=15)
            self.config.BaseStation1000[1].set_date_and_time(_date='06/29/2012', _time='00:59:59')
            self.config.BaseStation1000[1].do_increment_clock(seconds=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_49(self):
        """
        Twelfth Day Second Hour \n
        Increment the time by one hour, which will make a time of 2:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 running
            - Program 4 waiting to water
            - Program 5 done watering
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 watering
            - Zone 4 waiting to water
            - Zone 5 done watering
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_50(self):
        """
        Twelfth Day Third Hour \n
        Increment the time by one hour, which will make a time of 3:00 A.M. \n
        Then verify the statuses of the following: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 done watering
            - Program 4 running
            - Program 5 done watering
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 done watering
            - Zone 4 watering
            - Zone 5 done watering
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_51(self):
        """
        Twelfth Day Fourth Hour \n
        Increment the time by one hour, which will make a time of 4:00 A.M. \n
        Then verify the statuses of the following: \n
            - Program Status
                - Program 1 - 6 done watering
            - Zone Status
                - Zone 1 - 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_done()
            for zones in range(1, 7):
                self.config.BaseStation1000[1].zones[zones].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_52(self):
        """
        Thirteenth Day First Hour \n
        set time twenty one hours and fifteen minutes later \n
        set clock to 06/30/2012 12:59 one second before the zones begin watering \n
        Increment the clock by one second, which will make a time of 1:00 A.M. \n
        Verify the status for each program and zone and on the controller: \n
        - Program Status
            - Program 1 done watering
            - Program 2 running
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 waiting to water
            - Program 6 waiting to water
        - Zone Status
            - Zone 1 done watering
            - Zone 2 watering
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 waiting to water
            - Zone 6 waiting to water
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=20, minutes=15)
            self.config.BaseStation1000[1].set_date_and_time(_date='06/30/2012', _time='00:59:59')
            self.config.BaseStation1000[1].do_increment_clock(seconds=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_53(self):
        """
        Thirteenth Day Second Hour \n
        Increment time to one hour later, which will make a time of 2:00 A.M. \n
        Verify the status for each program and zone and on the controller: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 running
            - Program 6 waiting to water
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 watering
            - Zone 6 waiting to water
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_54(self):
        """
        Thirteenth Day Third Hour \n
        Increment time to one hour later, which will make a time of 3:00 A.M. \n
        Verify the status for each program and zone and on the controller: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 done watering
            - Program 4 done watering
            - Program 5 done watering
            - Program 6 running
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 done watering
            - Zone 4 done watering
            - Zone 5 done watering
            - Zone 6 watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_running()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_watering()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_55(self):
        """
        Thirteenth Day Fourth Hour \n
         Increment the time by one hour, which will make a time of 4:00 A.M. \n
         Then verify the statuses of the following: \n
             - Program Status
                 - Program 1 - 6 done watering
             - Zone Status
                 - Zone 1 - 6 done watering
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_done()
            for zones in range(1, 7):
                self.config.BaseStation1000[1].zones[zones].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_56(self):
        """
        Fourteenth Day First Hour \n
        set time twenty one hours and fifteen minutes later \n
        set clock to 07/01/2012 12:59 one second before the zones begin watering \n
        Increment the clock by one second, which will make a time of 1:00 A.M. \n
        Verify the status for each program and zone and on the controller: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 running
            - Program 4 waiting to water
            - Program 5 done watering
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 watering
            - Zone 4 waiting to water
            - Zone 5 done watering
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=20, minutes=15)
            self.config.BaseStation1000[1].set_date_and_time(_date='07/01/2012', _time='00:59:59')
            self.config.BaseStation1000[1].do_increment_clock(seconds=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_57(self):
        """
        Fourteenth Day Second Hour \n
        set clock to 06/30/2012 12:59 one second before the zones begin watering \n
        Increment the clock by one hour, which will make a time of 2:00 A.M. \n
        Verify the status for each program and zone and on the controller: \n
        - Program Status
            - Program 1 done watering
            - Program 2 done watering
            - Program 3 done watering
            - Program 4 running
            - Program 5 done watering
            - Program 6 done watering
        - Zone Status
            - Zone 1 done watering
            - Zone 2 done watering
            - Zone 3 done watering
            - Zone 4 watering
            - Zone 5 done watering
            - Zone 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[4].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[6].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_58(self):
        """
        Fourteenth Day Third Hour \n
        Increment the time by one hour, which will make a time of 3:00 A.M. \n
        Then verify the statuses of the following: \n
            - Program Status
                - Program 1 - 6 done watering
            - Zone Status
                - Zone 1 - 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 7):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_done()
            for zones in range(1, 7):
                self.config.BaseStation1000[1].zones[zones].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_59(self):
        """
        Fourteenth Day Fourth Hour \n
        Increment the time by one hour, which will make a time of 4:00 A.M. \n
        Then verify the statuses of the following: \n
            - Program Status
                - Program 1 - 6 done watering
            - Zone Status
                - Zone 1 - 6 done watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1)
            self.config.BaseStation1000[1].verify_date_and_time()
            
            for program in range(1, 7):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_done()
            for zones in range(1, 7):
                self.config.BaseStation1000[1].zones[zones].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

