import sys
from time import sleep
from datetime import datetime, timedelta
from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.logging_handler import log_handler
from common import helper_methods

__author__ = "Eldin"


class ControllerUseCase5(object):
    """
    Test name: \n
        Event Switch Test \n
    Purpose: \n
        The purpose of this test is to verify that an event switch causes Programs to act in a certain way when opened
        and closed. This includes the event switch starting, stopping, and pausing Programs. \n
    Coverage Area: \n
        This test covers using the start/stop/pause functions with event decoders. \n
            1. Initialize the controller \n
            2. Searching and assigning: \n
                - Zones \n
                - Flow Meters \n
                - Event Switches \n
            3. Set up Programs \n
            4. Assign each zone to a program \n
            5. Set max zone concurrency for the controller and Programs \n
            6. Verify the full configuration \n
            7. Set program 2 stop condition when the event switch is open \n
            8. Disable day/time start for both program 2 and 3 \n
            9. Set the event switch to closed and verify that all zones and Programs are 'DN' before the Programs start \n
            10. Start both Programs and verify that all zones and Programs are in line with concurrency settings \n
            11. Open the event switch and verify that program 2 stops while program 3 keeps running \n
            12. Close the event switch and verify that program 2 is still stopped and program 3 is still running \n
            13. Stop both Programs and verify that all zones and Programs are 'done watering'
            14. Set program 3 to start when the event switch is open and then close the event switch. Verify that all zones
                and Programs have the expected statuses. \n
            15. Open the event switch and verify that program 3 starts \n
            16. Close the event switch and again verify that program 2 is still stopped and program 3 is still running
            17. Stop both Programs and verify that all zones and Programs are 'done watering'
            18. Set program 2 to pause for 60 seconds when the event switch is opened. Turn off any start or stop commands
                from previously. Close the event switch and start the Programs then verify the zones and Programs \n
            19. Open the switch and verify that program 2 and it's attached zones are paused while program 3 is running \n
            20. Close the event switch and verify that program 2 is still paused and program 3 is continuing to run \n
            21. Verify the full configuration before ending the use case \n
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """
        time_it_should_take_test_to_run = timedelta(minutes=15)  # this is minutes
        time_test_started = datetime.now()
        try:
            number_of_retries = 1
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.test_elapsed_time(_time_started=time_test_started,
                                                     _time_expected_to_run=time_it_should_take_test_to_run)
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        Setup Programs
        ############################
        Set the max concurrent zone of the controller
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].set_max_concurrent_zones(_max_zones=1)

            self.config.BaseStation1000[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation1000[1].programs[2].set_max_concurrent_zones(_number_of_zones=3)

            self.config.BaseStation1000[1].add_program_to_controller(_program_address=3)
            self.config.BaseStation1000[1].programs[3].set_max_concurrent_zones(_number_of_zones=3)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_2(self):
        """
        ############################
        Setup Zone Programs
        ############################
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=52)
            self.config.BaseStation1000[1].programs[2].zone_programs[52].set_run_time(_minutes=45)

            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=53)
            self.config.BaseStation1000[1].programs[2].zone_programs[53].set_run_time(_minutes=45)

            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=54)
            self.config.BaseStation1000[1].programs[2].zone_programs[54].set_run_time(_minutes=45)

            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=55)
            self.config.BaseStation1000[1].programs[2].zone_programs[55].set_run_time(_minutes=45)

            self.config.BaseStation1000[1].programs[3].add_zone_to_program(_zone_address=75)
            self.config.BaseStation1000[1].programs[3].zone_programs[75].set_run_time(_minutes=5)

            self.config.BaseStation1000[1].programs[3].add_zone_to_program(_zone_address=76)
            self.config.BaseStation1000[1].programs[3].zone_programs[76].set_run_time(_minutes=5)

            self.config.BaseStation1000[1].programs[3].add_zone_to_program(_zone_address=78)
            self.config.BaseStation1000[1].programs[3].zone_programs[78].set_run_time(_minutes=5)

            self.config.BaseStation1000[1].programs[3].add_zone_to_program(_zone_address=79)
            self.config.BaseStation1000[1].programs[3].zone_programs[79].set_run_time(_minutes=5)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_3(self):
        """
        Verify full Configuration \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            # Assign a design flow value to each zone so that they have a default setting
            self.config.BaseStation1000[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_4(self):
        """
        set the original event switch sensor settings \n
        set event switch TPD0001 to stop program 2 when contacts open \n
        This sets up the condition for the event switch to trigger on
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[2].add_switch_stop_condition(_event_switch_address=1)
            self.config.BaseStation1000[1].programs[2].event_switch_stop_conditions[1].set_switch_mode_to_open()
            self.config.BaseStation1000[1].programs[2].event_switch_stop_conditions[1].set_stop_immediately_true()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_6(self):
        """
        Set event switch TPD0001 to closed \n
        By setting the initial state of the switch to closed you are telling the program not to run
        Because of how the 1000 is set up you either have to wait a few minutes or change the dial position
        Before you can start a program, after doing a search \n
        Set the date and time of the controller so that there is a known days of the week and days of the month \n
        Increment time by one second so that the controller is at the top of the hour this will get the correct zone
        Status set \n
        Verify that all zones are functioning \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
        - Zone status: \n
            - zone 52 done watering \n
            - zone 53 done watering \n
            - zone 54 done watering \n
            - zone 55 done watering \n
            - zone 75 done watering \n
            - zone 76 done watering \n
            - zone 78 done watering \n
            - zone 79 done watering \n
        start both Programs \n
        verify that both Programs are still functioning properly \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].event_switches[1].bicoder.set_contact_closed()
            self.config.BaseStation1000[1].set_controller_to_run()
            self.config.BaseStation1000[1].set_date_and_time(_date="04/08/2014", _time="06:59:59")
            self.config.BaseStation1000[1].do_increment_clock(seconds=1)

            # Verify that no zones are running. This is known by checking the status for 'DN'
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_done()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_7(self):
        """
        Start both Programs \n
        Verify that both Programs are still functioning properly \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
            - program 2 waiting to water \n
        - Zone status: \n
            - zone 52 watering \n
            - zone 53 waiting to water \n
            - zone 54 waiting to water \n
            - zone 55 waiting to water \n
            - zone 75 waiting to water \n
            - zone 76 waiting to water \n
            - zone 78 waiting to water \n
            - zone 79 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[2].set_program_to_start()
            self.config.BaseStation1000[1].programs[3].set_program_to_start()
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            # This is necessary for our data in zone to update
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[52].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[53].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[54].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[55].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].zones[75].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[76].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[78].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[79].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_8(self):
        """
        Set event switch TPD0001 to open \n
        This section runs off the time from the section above \n
        Advance clock 2 minutes \n
        Because the the event switch gets opened program 2 will stop running
        Verify that program 2 stops and that program 3 continues to run \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 running \n
        - Zone status: \n
            - zone 52 done watering \n
            - zone 53 done watering \n
            - zone 54 done watering \n
            - zone 55 done watering \n
            - zone 75 watering \n
            - zone 76 waiting to water \n
            - zone 78 waiting to water \n
            - zone 79 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].event_switches[1].bicoder.set_contact_open()
            self.config.BaseStation1000[1].event_switches[1].bicoder.set_two_wire_drop_value(_value=1.8)
            self.config.BaseStation1000[1].do_increment_clock(minutes=2)

            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[52].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[53].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[54].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[55].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[75].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[76].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[78].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[79].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_9(self):
        """
        Set event switch TPD0001 to closed \n
        This section runs off the same time settings as the first verification \n
        Run the clock for 2 more minutes \n
        verify that closing the switch didn't cause program two to start running
        Verify that program 2 is still stopped and that program 3 is still running \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 running \n
        - Zone status: \n
            - zone 52 done watering \n
            - zone 53 done watering \n
            - zone 54 done watering \n
            - zone 55 done watering \n
            - zone 75 watering \n
            - zone 76 waiting to water \n
            - zone 78 waiting to water \n
            - zone 79 waiting to water \n
        Stop both Programs \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].event_switches[1].bicoder.set_contact_closed()
            self.config.BaseStation1000[1].event_switches[1].bicoder.set_two_wire_drop_value(_value=1.8)
            self.config.BaseStation1000[1].do_increment_clock(minutes=2)

            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[52].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[53].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[54].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[55].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[75].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[76].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[78].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[79].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_10(self):
        """
        Stop both Programs and verify that everything is 'done watering' \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
        - Zone status: \n
            - zone 52 done watering \n
            - zone 53 done watering \n
            - zone 54 done watering \n
            - zone 55 done watering \n
            - zone 75 done watering \n
            - zone 76 done watering \n
            - zone 78 done watering \n
            - zone 79 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[2].set_program_to_stop()
            self.config.BaseStation1000[1].programs[3].set_program_to_stop()
            self.config.BaseStation1000[1].do_increment_clock(seconds=1)

            # Verify that no zones are running. This is known by checking the status for 'DN'
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_done()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_11(self):
        """
        Change the original event switch sensor settings \n
        assign event switch to program 3 so that it is on program 2 and 3 \n
                Set event switch TPD0001 to start program 3 when contacts open \n
        Now the event switch is set to stop program 2 and start program 3 when switch opens \n
        Start both Programs \n
        Advance the clock 2 minutes \n
        Verify that program 2 starts watering while program 3 is waiting \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
            - program 2 waiting to water \n
        - Zone status: \n
            - zone 52 watering \n
            - zone 53 waiting to water \n
            - zone 54 waiting to water \n
            - zone 55 waiting to water \n
            - zone 75 waiting to water \n
            - zone 76 waiting to water \n
            - zone 78 waiting to water \n
            - zone 79 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[3].add_switch_start_condition(_event_switch_address=1)
            self.config.BaseStation1000[1].programs[3].event_switch_start_conditions[1].set_switch_mode_to_open()

            self.config.BaseStation1000[1].programs[2].set_program_to_start()
            self.config.BaseStation1000[1].programs[3].set_program_to_start()
            self.config.BaseStation1000[1].do_increment_clock(minutes=2)

            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[52].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[53].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[54].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[55].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].zones[75].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[76].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[78].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[79].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_12(self):
        """
        Set event switch TPD0001 to open \n
        Advance time 2 minutes \n
        Verify that program 2 stops when the switch is opened and program 3 continues to water  when the switch opens \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 running \n
        - Zone status: \n
            - zone 52 done watering \n
            - zone 53 done watering \n
            - zone 54 done watering \n
            - zone 55 done watering \n
            - zone 75 watering \n
            - zone 76 waiting to water \n
            - zone 78 waiting to water \n
            - zone 79 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].event_switches[1].bicoder.set_contact_open()
            self.config.BaseStation1000[1].do_increment_clock(minutes=2)

            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[52].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[53].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[54].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[55].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[75].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[76].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[78].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[79].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_13(self):
        """
        Set event switch TPD0001 to closed \n
        Advance the clock 2 minutes \n
        Verify that program 2 stays stopped and program 3 continues to water even though the switch is set to closed\n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 running \n
        - Zone status: \n
            - zone 52 done watering \n
            - zone 53 done watering \n
            - zone 54 done watering \n
            - zone 55 done watering \n
            - zone 75 watering \n
            - zone 76 waiting to water \n
            - zone 78 waiting to water \n
            - zone 79 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].event_switches[1].bicoder.set_contact_closed()
            self.config.BaseStation1000[1].do_increment_clock(minutes=2)

            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[52].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[53].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[54].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[55].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[75].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[76].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[78].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[79].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_14(self):
        """
        Stop both Programs and verify that everything is 'done watering' \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
        - Zone status: \n
            - zone 52 done watering \n
            - zone 53 done watering \n
            - zone 54 done watering \n
            - zone 55 done watering \n
            - zone 75 done watering \n
            - zone 76 done watering \n
            - zone 78 done watering \n
            - zone 79 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[2].set_program_to_stop()
            self.config.BaseStation1000[1].programs[3].set_program_to_stop()
            self.config.BaseStation1000[1].do_increment_clock(seconds=1)

            # Verify that no zones are running. This is known by checking the status for 'DN'
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_done()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_15(self):
        """
        Change the event switch sensor settings again \n
        Set event switch TPD0001 to pause program 2 for 60 minutes when contacts open \n
        disable event switch TPD0001 from stopping program 2 \n
        disable event switch TPD0001 from starting program 3 \n
        Set Event switch to closed \n
        Start both Programs \n
        Advance the clock 5 minutes \n
        Verify that program 2 starts off running and program 3 starts off waiting \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
            - program 2 waiting to water \n
        - Zone status: \n
            - zone 52 watering \n
            - zone 53 waiting to water \n
            - zone 54 waiting to water \n
            - zone 55 waiting to water \n
            - zone 75 waiting to water \n
            - zone 76 waiting to water \n
            - zone 78 waiting to water \n
            - zone 79 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[2].add_switch_pause_condition(_event_switch_address=1)
            self.config.BaseStation1000[1].programs[2].event_switch_pause_conditions[1].set_switch_mode_to_open()
            self.config.BaseStation1000[1].programs[2].event_switch_pause_conditions[1].set_switch_pause_time(
                _minutes=60)
            self.config.BaseStation1000[1].programs[2].event_switch_stop_conditions[1].set_switch_mode_to_off()
            self.config.BaseStation1000[1].programs[3].event_switch_start_conditions[1].set_switch_mode_to_off()

            self.config.BaseStation1000[1].event_switches[1].bicoder.set_contact_closed()
            self.config.BaseStation1000[1].programs[2].set_program_to_start()
            self.config.BaseStation1000[1].programs[3].set_program_to_start()
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)

            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[52].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[53].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[54].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[55].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].zones[75].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[76].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[78].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[79].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_16(self):
        """
        Set event switch TPD0001 to open \n
        Advance the clock 1 minute \n
        Verify that program 2 pauses when the switch opens and that program 3 starts running \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 paused \n
            - program 2 running \n
        - Zone status: \n
            - zone 52 paused \n
            - zone 53 paused \n
            - zone 54 paused \n
            - zone 55 paused \n
            - zone 75 watering \n
            - zone 76 waiting to water \n
            - zone 78 waiting to water \n
            - zone 79 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].event_switches[1].bicoder.set_contact_open()
            self.config.BaseStation1000[1].event_switches[1].bicoder.set_two_wire_drop_value(_value=1.8)
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_paused()
            self.config.BaseStation1000[1].zones[52].statuses.verify_status_is_paused()
            self.config.BaseStation1000[1].zones[53].statuses.verify_status_is_paused()
            self.config.BaseStation1000[1].zones[54].statuses.verify_status_is_paused()
            self.config.BaseStation1000[1].zones[55].statuses.verify_status_is_paused()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[75].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[76].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[78].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[79].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_17(self):
        """
        Set event switch TPD0001 to open \n
        Advance the clock 8 minutes \n
        Verify that program 2 remains paused and that program 3 continues to run \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 paused \n
            - program 2 running \n
        - Zone status: \n
            - zone 52 paused \n
            - zone 53 paused \n
            - zone 54 paused \n
            - zone 55 paused \n
            - zone 75 done \n
            - zone 76 watering \n
            - zone 78 waiting to water \n
            - zone 79 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].event_switches[1].bicoder.set_contact_open()
            self.config.BaseStation1000[1].do_increment_clock(minutes=8)

            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_paused()
            self.config.BaseStation1000[1].zones[52].statuses.verify_status_is_paused()
            self.config.BaseStation1000[1].zones[53].statuses.verify_status_is_paused()
            self.config.BaseStation1000[1].zones[54].statuses.verify_status_is_paused()
            self.config.BaseStation1000[1].zones[55].statuses.verify_status_is_paused()
            self.config.BaseStation1000[1].programs[3].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[75].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[76].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[78].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[79].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_19(self):
        """
        Verify full Configuration \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            # Assign a design flow value to each zone so that they have a default setting
            self.config.BaseStation1000[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
