import sys
from time import sleep
from datetime import datetime, timedelta
from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.logging_handler import log_handler
from common import helper_methods

__author__ = 'Baseline'


class ControllerUseCase2(object):
    """
    Test name: \n
        Basic Design Flow Test \n
    Purpose: \n
        This test covers different flow operations: \n
            - When the flow limit only allows certain zones to be run. The zones that run should utilize as much of the
             flow limit as they can depending on their design flow. \n
            - When there is no flow limit and we want to run a certain number of concurrent zones. \n
            - Running multiple Programs with a flow limit from a single water source (POC) \n
            - Running multiple Programs with separate water sources \n
    Coverage area: \n
        1. Setting up the controller \n
        2. Searching and assigning: \n
            - Zones \n
            - Master Valves \n
            - Flow Meters \n
        3. Assign each zone a design flow \n
        4. Setting up water sources \n
        5. Set up the Programs \n
        6. Assign each zone to a program \n
        7. Set up the zone concurrency for the controller and Programs \n
        8. Verify the full configuration
        9. Run Programs with a design flow restriction on program 1 with Limit Concurrent active \n
        10. Run Programs with two design flow one on each program, one with Limit Concurrent active and one without \n
        11. Run Programs with two design flow one on each program, both with Limit Concurrent active \n
        12. Verify the full configuration again
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    ###############################
    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """
        time_it_should_take_test_to_run = timedelta(minutes=15)  # this is minutes
        time_test_started = datetime.now()
        try:
            number_of_retries = 1
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.test_elapsed_time(_time_started=time_test_started,
                                                     _time_expected_to_run=time_it_should_take_test_to_run)
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        Give each zone a design flow
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].set_max_concurrent_zones(_max_zones=4)
            # Assign a design flow value to each zone so that they have a default setting
            self.config.BaseStation1000[1].zones[51].set_design_flow(_gallons_per_minute=20)
            self.config.BaseStation1000[1].zones[52].set_design_flow(_gallons_per_minute=20)
            self.config.BaseStation1000[1].zones[53].set_design_flow(_gallons_per_minute=20)
            self.config.BaseStation1000[1].zones[54].set_design_flow(_gallons_per_minute=12)
            self.config.BaseStation1000[1].zones[55].set_design_flow(_gallons_per_minute=7.5)
            self.config.BaseStation1000[1].zones[56].set_design_flow(_gallons_per_minute=15)
            self.config.BaseStation1000[1].zones[75].set_design_flow(_gallons_per_minute=15)
            self.config.BaseStation1000[1].zones[76].set_design_flow(_gallons_per_minute=7.5)
            self.config.BaseStation1000[1].zones[78].set_design_flow(_gallons_per_minute=7.5)
            self.config.BaseStation1000[1].zones[79].set_design_flow(_gallons_per_minute=7.5)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_2(self):
        """
        ############################
        Setup Points of Connection
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].add_poc(_poc_address=1)
            self.config.BaseStation1000[1].points_of_control[1].add_flow_meter_to_point_of_control(
                _flow_meter_address=1)
            self.config.BaseStation1000[1].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=1)
            self.config.BaseStation1000[1].points_of_control[1].set_target_flow(_gpm=50)
            self.config.BaseStation1000[1].points_of_control[1].set_limit_concurrent_to_target_state_enabled()

            self.config.BaseStation1000[1].add_poc(_poc_address=2)
            self.config.BaseStation1000[1].points_of_control[2].add_flow_meter_to_point_of_control(
                _flow_meter_address=1)
            self.config.BaseStation1000[1].points_of_control[2].add_master_valve_to_point_of_control(
                _master_valve_address=1)
            self.config.BaseStation1000[1].points_of_control[2].set_target_flow(_gpm=25)
            self.config.BaseStation1000[1].points_of_control[2].set_limit_concurrent_to_target_state_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_3(self):
        """
        ############################
        Setup Programs
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation1000[1].programs[1].set_cycle_count(_new_count=2)
            self.config.BaseStation1000[1].programs[1].set_soak_time_in_seconds(_seconds=240)
            self.config.BaseStation1000[1].programs[1].add_point_of_connection(_poc_address=1)

            self.config.BaseStation1000[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation1000[1].programs[2].set_cycle_count(_new_count=2)
            self.config.BaseStation1000[1].programs[2].set_soak_time_in_seconds(_seconds=240)
            self.config.BaseStation1000[1].programs[2].add_point_of_connection(_poc_address=1)
            self.config.BaseStation1000[1].programs[1].set_soak_time_in_seconds(_seconds=240)
            self.config.BaseStation1000[1].programs[1].add_point_of_connection(_poc_address=1)

            self.config.BaseStation1000[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation1000[1].programs[2].set_cycle_count(_new_count=2)
            self.config.BaseStation1000[1].programs[2].set_soak_time_in_seconds(_seconds=240)
            self.config.BaseStation1000[1].programs[2].add_point_of_connection(_poc_address=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_4(self):
        """
        ############################
        Setup Zone Programs
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=51)
            self.config.BaseStation1000[1].programs[1].zone_programs[51].set_run_time(_minutes=60)

            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=52)
            self.config.BaseStation1000[1].programs[1].zone_programs[52].set_run_time(_minutes=60)

            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=53)
            self.config.BaseStation1000[1].programs[1].zone_programs[53].set_run_time(_minutes=60)

            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=54)
            self.config.BaseStation1000[1].programs[1].zone_programs[54].set_run_time(_minutes=60)

            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=55)
            self.config.BaseStation1000[1].programs[1].zone_programs[55].set_run_time(_minutes=60)

            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=56)
            self.config.BaseStation1000[1].programs[2].zone_programs[56].set_run_time(_minutes=60)

            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=75)
            self.config.BaseStation1000[1].programs[2].zone_programs[75].set_run_time(_minutes=60)

            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=76)
            self.config.BaseStation1000[1].programs[2].zone_programs[76].set_run_time(_minutes=60)

            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=78)
            self.config.BaseStation1000[1].programs[2].zone_programs[78].set_run_time(_minutes=60)

            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=79)
            self.config.BaseStation1000[1].programs[2].zone_programs[79].set_run_time(_minutes=60)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_5(self):
        """
        Set up max zone concurrency for the controller and each program \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].set_max_concurrent_zones(_max_zones=10)
            self.config.BaseStation1000[1].programs[1].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation1000[1].programs[2].set_max_concurrent_zones(_number_of_zones=3)

            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_6(self):
        """
        Verify full Configuration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Assign a design flow value to each zone so that they have a default setting
            self.config.BaseStation1000[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_7(self):
        """
        turn on sim mode in the controller \n
        stop the clock so that it can be incremented manually \n
        set the date and time of the controller so that there is a known days of the week and days of the month \n
        verify that no zones are currently running \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program done watering \n
        - Zone status: \n
            - zone 51 done watering \n
            - zone 52 done watering \n
            - zone 53 done watering \n
            - zone 54 done watering \n
            - zone 55 done watering \n
            - zone 56 done watering \n
            - zone 75 done watering \n
            - zone 76 done watering \n
            - zone 78 done watering \n
            - zone 79 done watering \n
        increment time by one minute so that the controller will get the correct zone status set \n
        verify that only zones 51, 52, and 55 are watering because water source 1 is limited to 50 GPM while zones 51
        and 52 run at 20 GPM each and zone 55 runs at 7.5 bring it up to a grand total of 47.5 GPM leaving
        nothing for the rest of the water source \n
        verify that program 2 does not run because it runs off the flow from water source 1 and there is not enough flow
        left to run any zones on program 2 \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program done watering \n
        - Zone status: \n
            - zone 51 watering \n
            - zone 52 watering \n
            - zone 53 waiting to water \n
            - zone 54 waiting to water \n
            - zone 55 watering \n
            - zone 56 waiting to water \n
            - zone 75 waiting to water \n
            - zone 76 waiting to water \n
            - zone 78 waiting to water \n
            - zone 79 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].set_date_and_time(_date="06/18/2012", _time="08:00:00")
            self.config.BaseStation1000[1].set_controller_to_run()
            # This is necessary for our data in zone to update
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()

            # Verify that no zones are running. This is known by checking the status for 'DN'
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_done()

            # This starts the Programs
            self.config.BaseStation1000[1].programs[1].set_program_to_start()
            self.config.BaseStation1000[1].programs[2].set_program_to_start()

            # Allows the controller to update the Zone status
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            # Verify that only zones 51, 52, and 55 are watering and the rest are waiting
            for zone in [51, 52, 55]:
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_watering()
            # Verify that the rest of the zones are waiting to water
            for zone in [53, 54, 56, 75, 76, 78, 79]:
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_8(self):
        """
        re-assign program 2 to water source 2 \n
        turn dial to off to ensure that nothing is left running \n
        increment the clock 1 second so that the Programs stop running \n
        Because of how the 1000 is set up you either have to wait a few minutes or change the dial position to run
        Before you can start a program, after doing a search \n
        start Programs 1 & 2 \n
        set the date and time of the controller so that there is a known days of the week and days of the month \n
        increment clock by 1 minute to get the Programs running \n
        verify that zones 51, 52, and 55 are the only zones running on program 1 because water source 1 is limited to
        50 GPM, and those 3 zones take up 47.5 GPM \n
        verify that zones 56, 75, and 76 are the only zones running on program 2 because water source 2 is not limited
        by flow so the program runs off concurrency \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program watering \n
        - Zone status: \n
            - zone 51 watering \n
            - zone 52 watering \n
            - zone 53 waiting to water \n
            - zone 54 waiting to water \n
            - zone 55 watering \n
            - zone 56 watering \n
            - zone 75 watering \n
            - zone 76 watering \n
            - zone 78 waiting to water \n
            - zone 79 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[2].remove_point_of_connection(_poc_address=1)
            self.config.BaseStation1000[1].programs[2].add_point_of_connection(_poc_address=2)
            self.config.BaseStation1000[1].set_controller_to_off()
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            self.config.BaseStation1000[1].set_controller_to_run()
            self.config.BaseStation1000[1].programs[1].set_program_to_start()
            self.config.BaseStation1000[1].programs[2].set_program_to_start()
            self.config.BaseStation1000[1].set_date_and_time(_date="06/18/2012", _time="08:00:00")
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            # Verify that only zones 51, 52, and 55 are watering and the rest are waiting
            for zone in [51, 52, 55, 56, 75, 76]:
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_watering()
            # Verify that the rest of the zones are waiting to water
            for zone in [53, 54, 78, 79]:
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_9(self):
        """
        enable limit concurrent to target on water source 2 \n
        turn dial to off to ensure that nothing is left running \n
        advance the clock 1 second so that the Programs stop running \n
        Because of how the 1000 is set up you either have to wait a few minutes or change the dial position
        Before you can start a program, after doing a search \n
        set the date and time of the controller so that there is a known days of the week and days of the month \n
        increment clock so that the Programs can start \n
        verify that zones 51, 52, and 55 are the only zones running on program 1 because water source 1 is limited to
        50 GPM and those 3 zones take up 47.5 GPM \n
        verify that zones 56 and 76 are the only zones running on program 2 because water source 2 is limited to 25 GPM
        because it is now limited by flow and both zones take up 22.5 GPM \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program done watering \n
        - Zone status: \n
            - zone 51 watering \n
            - zone 52 watering \n
            - zone 53 waiting to water \n
            - zone 54 waiting to water \n
            - zone 55 watering \n
            - zone 56 watering \n
            - zone 75 waiting to water \n
            - zone 76 watering \n
            - zone 78 waiting to water \n
            - zone 79 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].points_of_control[2].set_limit_concurrent_to_target_state_enabled()
            self.config.BaseStation1000[1].set_controller_to_off()
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            self.config.BaseStation1000[1].set_controller_to_run()
            self.config.BaseStation1000[1].programs[1].set_program_to_start()
            self.config.BaseStation1000[1].programs[2].set_program_to_start()
            self.config.BaseStation1000[1].set_date_and_time(_date="06/18/2012", _time="08:00:00")
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)

            for zone in [51, 52, 55, 56, 76]:
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_watering()
            # Verify that the rest of the zones are waiting to water
            for zone in [53, 54, 75, 78, 79]:
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_10(self):
        """
        Verify full Configuration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Assign a design flow value to each zone so that they have a default setting
            self.config.BaseStation1000[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]