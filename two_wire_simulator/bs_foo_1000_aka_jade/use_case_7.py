import sys
from time import sleep
from datetime import datetime, timedelta
from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.logging_handler import log_handler
from common import helper_methods

__author__ = "Eldin"


class ControllerUseCase7(object):
    """
    Test name: \n
        Temperature Decoder Test \n
    Purpose: \n
        This test covers using start/stop/pause using temperature sensors \n
    Coverage area: \n
        1. Set up the controller and limit its concurrent zones \n
        2. Searching and assigning: \n
            - Zones \n
            - Temperature sensors \n
        3. Set up Programs and give them a run time using commands \n
        4. Assign zones to Programs and give them run times \n
        5. Set up pause conditions for Programs 1 and 2 to pause when the respective temperature sensor reading is below
           32 degrees \n
        6. Verify the full configuration \n
        7-8. Run the Programs without any conditions to ensure that everything runs as expected \n
        9. Set temperature to be lower than both Programs lower limits and verify that all Programs/zones are paused \n
        10-12. Set the temperature to exceed both Programs upper limit and verify that all Programs/zones are paused \n
        13-14. Set program 1 to start when the temperature on TAT0001 is below 90. Verify that program 1 runs \n
        15-17. Set program 2 to also start when the temperature is below 90. Verify that both Programs are running \n
        18-21. Set program 1 and 2 to have a start condition, but only make a stop condition for program 1. Verify that
               both start correctly, and then verify that only program 1 and its zones stop \n
        22-24. Keep the conditions from the previous steps and give program 2 a stop condition that is the same as
               program 1. Verify that both Programs start and stop when their conditions are triggered. \n
        25. Verify the full configuration again \n
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    ##################################################################################################################
    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """
        time_it_should_take_test_to_run = timedelta(minutes=15)  # this is minutes
        time_test_started = datetime.now()
        try:
            number_of_retries = 1
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.test_elapsed_time(_time_started=time_test_started,
                                                     _time_expected_to_run=time_it_should_take_test_to_run)
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        Setup Programs
        ############################
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].set_max_concurrent_zones(_max_zones=4)

            self.config.BaseStation1000[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation1000[1].programs[1].set_max_concurrent_zones(_number_of_zones=2)

            self.config.BaseStation1000[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation1000[1].programs[2].set_max_concurrent_zones(_number_of_zones=2)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_2(self):
        """
        ############################
        Setup Zone Programs
        ############################
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation1000[1].programs[1].zone_programs[1].set_run_time(_minutes=45)
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation1000[1].programs[1].zone_programs[2].set_run_time(_minutes=45)
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation1000[1].programs[1].zone_programs[3].set_run_time(_minutes=45)
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=4)
            self.config.BaseStation1000[1].programs[1].zone_programs[4].set_run_time(_minutes=45)

            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=5)
            self.config.BaseStation1000[1].programs[2].zone_programs[5].set_run_time(_minutes=45)
            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=6)
            self.config.BaseStation1000[1].programs[2].zone_programs[6].set_run_time(_minutes=45)
            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=7)
            self.config.BaseStation1000[1].programs[2].zone_programs[7].set_run_time(_minutes=45)
            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=8)
            self.config.BaseStation1000[1].programs[2].zone_programs[8].set_run_time(_minutes=45)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_3(self):
        """
        set up start stop pause conditions
            Program 1:
                - start times 8:00 am \n
                - Setup pause Programs to pause when the temperature reading goes below 32 for both program 1 and 2 \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            # Start conditions to start both programs at 8:00AAM
            self.config.BaseStation1000[1].programs[1].add_date_time_start_condition(_condition_number=1)
            self.config.BaseStation1000[1].programs[1].date_time_start_conditions[1].set_day_time_start(_st_list=[480])

            self.config.BaseStation1000[1].programs[2].add_date_time_start_condition(_condition_number=1)
            self.config.BaseStation1000[1].programs[2].date_time_start_conditions[1].set_day_time_start(_st_list=[480])

            self.config.BaseStation1000[1].programs[1].add_temperature_pause_condition(_temperature_sensor_address=1)
            self.config.BaseStation1000[1].programs[1].temperature_pause_conditions[1].\
                set_temperature_mode_to_lower_limit()
            self.config.BaseStation1000[1].programs[1].temperature_pause_conditions[1].set_temperature_threshold(
                _degrees=32)

            self.config.BaseStation1000[1].programs[2].add_temperature_pause_condition(_temperature_sensor_address=1)
            self.config.BaseStation1000[1].programs[2].temperature_pause_conditions[1].\
                set_temperature_mode_to_lower_limit()
            self.config.BaseStation1000[1].programs[2].temperature_pause_conditions[1].set_temperature_threshold(
                _degrees=32)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_4(self):
        """
        Verify full Configuration \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_5(self):
        """
        set the clock 2 minutes to make the time 04/08/2014 8:00 am \n
        Verify that all zones are set to done before the test starts
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
            - zone 6 done watering \n
            - zone 7 done watering \n
            - zone 8 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].set_date_and_time(_date="04/08/2014", _time="08:00:00")
            self.config.BaseStation1000[1].set_controller_to_run()
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_6(self):
        """
        Start program 1 & program 2
        Start the Programs and verify that only two zones are running on each program due to concurrent limit \n
        advance the clock 5 minutes to make the time 04/08/2014 8:05
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
            - program 2 running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 watering \n
            - zone 6 watering \n
            - zone 7 waiting to water \n
            - zone 8 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].set_program_to_start()
            self.config.BaseStation1000[1].programs[2].set_program_to_start()
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[7].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[8].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_7(self):
        """
        Test that a single temperature sensor can pause both Programs \n
        Set TAT0001 to be 31, which is lower than the 'lower limit', and verify that both Programs pause \n
        advance the clock 5 minutes to make the time 04/08/2014 8:10 \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 paused \n
            - program 2 paused \n
        - Zone status: \n
            - zone 1 paused \n
            - zone 2 paused \n
            - zone 3 paused \n
            - zone 4 paused \n
            - zone 5 paused \n
            - zone 6 paused \n
            - zone 7 paused \n
            - zone 8 paused \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].temperature_sensors[1].bicoder.set_temperature_reading(_degrees=31)
            self.config.BaseStation1000[1].temperature_sensors[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation1000[1].set_controller_to_run()
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_paused()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_paused()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_paused()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_8(self):
        """
        Setup pause Programs to pause when the temperature reading is above 90 for both program 1 and 2 \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].temperature_pause_conditions[1].\
                set_temperature_mode_to_upper_limit()
            self.config.BaseStation1000[1].programs[1].temperature_pause_conditions[1].set_temperature_threshold(
                _degrees=90)

            self.config.BaseStation1000[1].programs[2].temperature_pause_conditions[1].\
                set_temperature_mode_to_upper_limit()
            self.config.BaseStation1000[1].programs[2].temperature_pause_conditions[1].set_temperature_threshold(
                _degrees=90)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_9(self):
        """
        Test that a single temperature sensor can pause both Programs \n
        Set TAT0001 to be 95, which is higher than the 'upper limit', verify that both Programs and all zones pause \n
        advance the clock 5 minutes to make the time 04/08/2014 8:15 \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 paused \n
            - program 2 paused \n
        - Zone status: \n
            - zone 1 paused \n
            - zone 2 paused \n
            - zone 3 paused \n
            - zone 4 paused \n
            - zone 5 paused \n
            - zone 6 paused \n
            - zone 7 paused \n
            - zone 8 paused \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].temperature_sensors[1].bicoder.set_temperature_reading(_degrees=95)
            self.config.BaseStation1000[1].temperature_sensors[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation1000[1].set_controller_to_run()
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_paused()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_paused()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_paused()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_10(self):
        """
        Verify that all zones are set to done before the next test starts
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
            - zone 6 done watering \n
            - zone 7 done watering \n
            - zone 8 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].set_program_to_stop()
            self.config.BaseStation1000[1].programs[2].set_program_to_stop()
            self.config.BaseStation1000[1].set_controller_to_run()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_11(self):
        """
        Turn off the pause conditions that were previously set \n
        Setup a start condition when the temperature reading is above 90 for program 1 only \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].temperature_pause_conditions[1].set_temperature_mode_off()
            self.config.BaseStation1000[1].programs[2].temperature_pause_conditions[1].set_temperature_mode_off()

            self.config.BaseStation1000[1].programs[1].add_temperature_start_condition(_temperature_sensor_address=1)
            self.config.BaseStation1000[1].programs[1].temperature_start_conditions[1].\
                set_temperature_mode_to_lower_limit()
            self.config.BaseStation1000[1].programs[1].temperature_start_conditions[1].\
                set_temperature_threshold(_degrees=90)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_12(self):
        """
        Set the temperature to 65 on temp sensor TAT0001, which is below the lower limit for temperature \n
        This should cause program 1 to start as it has a lower limit condition set in the previous step, but only the
         first two zones should be running since there is a max zone concurrency of 2 \n
         advance the clock 5 minutes to make the time 04/08/2014 8:20
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
            - program 2 done watering \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 done watering \n
            - zone 6 done watering \n
            - zone 7 done watering \n
            - zone 8 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].temperature_sensors[1].bicoder.set_temperature_reading(_degrees=65)
            self.config.BaseStation1000[1].temperature_sensors[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation1000[1].set_controller_to_run()
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[8].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_13(self):
        """
        Setup a start condition when the temperature reading is above 90 for program 2 as well \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].temperature_sensors[1].bicoder.set_temperature_reading(_degrees=95)
            self.config.BaseStation1000[1].temperature_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation1000[1].programs[2].add_temperature_start_condition(_temperature_sensor_address=1)
            self.config.BaseStation1000[1].programs[2].temperature_start_conditions[1].\
                set_temperature_mode_to_lower_limit()
            self.config.BaseStation1000[1].programs[2].temperature_start_conditions[1].\
                set_temperature_threshold(_degrees=90)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_14(self):
        """
        Set the temperature to 75 on temp sensor TAT0001, which is below the lower limit for temperature \n
        This should cause program 1 and 2 to start running because they have a lower limit of 90. However, only the
         first two zones should be running in each program since there is a max zone concurrency of 2 \n
         advance the clock 5 minutes to make the time 04/08/2014 8:25
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
            - program 2 running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 watering \n
            - zone 6 watering \n
            - zone 7 waiting to water \n
            - zone 8 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].temperature_sensors[1].bicoder.set_temperature_reading(_degrees=75)
            self.config.BaseStation1000[1].temperature_sensors[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation1000[1].set_controller_to_run()
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[7].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[8].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_15(self):
        """
        Turn off both Programs \n
        Turn off the start conditions that were previously set \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].set_program_to_stop()
            self.config.BaseStation1000[1].programs[2].set_program_to_stop()

            self.config.BaseStation1000[1].programs[1].temperature_start_conditions[1].set_temperature_mode_off()
            self.config.BaseStation1000[1].programs[2].temperature_start_conditions[1].set_temperature_mode_off()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_16(self):
        """
        Set up two new start conditions that start both Programs 1 and 2 when the temperature drops below 70 \n
        Set up a stop condition for program 1 when the temperature is higher than 90 \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].temperature_start_conditions[1]. \
                set_temperature_mode_to_lower_limit()
            self.config.BaseStation1000[1].programs[1].temperature_start_conditions[1]. \
                set_temperature_threshold(_degrees=70)

            self.config.BaseStation1000[1].programs[2].temperature_start_conditions[1]. \
                set_temperature_mode_to_lower_limit()
            self.config.BaseStation1000[1].programs[2].temperature_start_conditions[1]. \
                set_temperature_threshold(_degrees=70)

            self.config.BaseStation1000[1].programs[1].add_temperature_stop_condition(_temperature_sensor_address=1)
            self.config.BaseStation1000[1].programs[1].temperature_stop_conditions[1].\
                set_temperature_mode_to_upper_limit()
            self.config.BaseStation1000[1].programs[1].temperature_stop_conditions[1].\
                set_temperature_threshold(_degrees=90)
            self.config.BaseStation1000[1].programs[1].temperature_stop_conditions[1].set_stop_immediately_true()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_17(self):
        """
        Set the temperature to 65 on temp sensor TAT0001, which is below the lower limit for temperature \n
        This should cause program 1 and 2 to start running because they have a lower limit of 70. However, only the
         first two zones should be running in each program since there is a max zone concurrency of 2 \n
        advance the clock 5 minutes to make the time 04/08/2014 8:30
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
            - program 2 running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 watering \n
            - zone 6 watering \n
            - zone 7 waiting to water \n
            - zone 8 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].temperature_sensors[1].bicoder.set_temperature_reading(_degrees=65)
            self.config.BaseStation1000[1].temperature_sensors[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation1000[1].set_controller_to_run()
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[7].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[8].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_18(self):
        """
        Set the temperature to 95 on temp sensor TAT0001, which is above the upper limit for temperature \n
        This should cause program 1 to stop running because it has an upper limit of 90. This should cause program 1
         and all of it's zones to be done watering \n
        advance the clock 5 minutes to make the time 04/08/2014 8:35
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 running \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 watering \n
            - zone 6 watering \n
            - zone 7 waiting to water \n
            - zone 8 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].temperature_sensors[1].bicoder.set_temperature_reading(_degrees=95)
            self.config.BaseStation1000[1].temperature_sensors[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation1000[1].set_controller_to_run()
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[7].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[8].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_19(self):
        """
        Turn off both Programs \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].set_program_to_stop()
            self.config.BaseStation1000[1].programs[2].set_program_to_stop()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_20(self):
        """
        Set up a the temperature so it neither starts or stops the Programs \n
        Set up a stop condition for program 2 when the temperature is higher than 90 \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].temperature_sensors[1].bicoder.set_temperature_reading(_degrees=80)
            self.config.BaseStation1000[1].temperature_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation1000[1].programs[2].add_temperature_stop_condition(_temperature_sensor_address=1)
            self.config.BaseStation1000[1].programs[2].temperature_stop_conditions[1]. \
                set_temperature_mode_to_upper_limit()
            self.config.BaseStation1000[1].programs[2].temperature_stop_conditions[1]. \
                set_temperature_threshold(_degrees=90)
            self.config.BaseStation1000[1].programs[2].temperature_stop_conditions[1].set_stop_immediately_true()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_21(self):
        """
        Set the temperature to 65 on temp sensor TAT0001, which is below the lower limit for temperature \n
        This should cause program 1 and 2 to start running because they have a lower limit of 70. However, only the
         first two zones should be running in each program since there is a max zone concurrency of 2 \n
        advance the clock 5 minutes to make the time 04/08/2014 8:40
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 running \n
            - program 2 running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 watering \n
            - zone 6 watering \n
            - zone 7 waiting to water \n
            - zone 8 waiting to water \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].temperature_sensors[1].bicoder.set_temperature_reading(_degrees=65)
            self.config.BaseStation1000[1].temperature_sensors[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation1000[1].set_controller_to_run()
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[6].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[7].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[8].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_22(self):
        """
        Raise the value of the temperature on the temperature sensor to 95 which is above both Programs upper limit \n
        This should cause both Programs and their zones to stop \n
        advance the clock 1 minutes to make the time 04/08/2014 8:41
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1 done watering \n
            - program 2 done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
            - zone 6 done watering \n
            - zone 7 done watering \n
            - zone 8 done watering \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].temperature_sensors[1].bicoder.set_temperature_reading(_degrees=95)
            self.config.BaseStation1000[1].temperature_sensors[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation1000[1].set_controller_to_run()
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_23(self):
        """
        Verify full Configuration \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
