import sys
from time import sleep
from datetime import datetime, timedelta
from common.configuration import Configuration
from common.objects.devices.ar import AlertRelay
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.logging_handler import log_handler
from common.imports import opcodes
from common import helper_methods

__author__ = "Baseline"


class ControllerUseCase15(object):
    """
    Test name: \n
        Alert Relay Test \n
    Purpose: \n
        Test to verify that the alert relay's contact state opens and closes with its flags.
    Coverage area: \n
        1. Set up the controller and limit its concurrent zones \n
        2. Searching and assigning: \n
            - Zones \n
            - Moisture sensors \n
            - Flow meters \n
        3. Set up Programs and give them a run time using commands \n
        4. Assign zones to Programs and give them run times \n
        5. Set up pause conditions for both Programs \n
        6. Assign values to the flow meter \n
        7. Build a POC object and instantiate its variables \n
        8. Load the alert relay to the controller and instantiate all of its variables \n
        9. Verify the full configuration \n
        10. Set alert relay flag to 1 and verify that its contact is open when there is an unexpected high flow message \n
        11. Set alert relay flag to 2 and verify that its contact is open when there is a high flow message \n
        12. Set alert relay flag to 4 and verify that its contact is open when there is a zone flow variance message \n
        13. Set alert relay flag to 8 and verify that its contact is open when there is a device no reply message \n
        14. Set alert relay flag to 16 and verify that its contact is open when there is a checksum message \n
        15. Set alert relay flag to 32 and verify that its contact is open when there is a solenoid error message \n
        16. Set alert relay flag to 64 and verify that its contact is open when there is a program overrun message \n
        17. Verify the full configuration
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """
        time_it_should_take_test_to_run = timedelta(minutes=15)  # this is minutes
        time_test_started = datetime.now()
        try:
            number_of_retries = 1
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.test_elapsed_time(_time_started=time_test_started,
                                                     _time_expected_to_run=time_it_should_take_test_to_run)
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        ############################
        Setup Programs
        ############################
        Give both programs an 8AM start time
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            program_8am_start_time = [480]

            self.config.BaseStation1000[1].set_max_concurrent_zones(_max_zones=4)

            self.config.BaseStation1000[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation1000[1].programs[1].set_max_concurrent_zones(_number_of_zones=2)

            self.config.BaseStation1000[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation1000[1].programs[2].set_max_concurrent_zones(_number_of_zones=2)

            self.config.BaseStation1000[1].programs[1].add_date_time_start_condition(_condition_number=1)
            self.config.BaseStation1000[1].programs[1].date_time_start_conditions[1].set_day_time_start(
                _st_list=program_8am_start_time)

            self.config.BaseStation1000[1].programs[2].add_date_time_start_condition(_condition_number=1)
            self.config.BaseStation1000[1].programs[2].date_time_start_conditions[1].set_day_time_start(
                _st_list=program_8am_start_time)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_2(self):
        """
        ############################
        Setup Zone Programs
        ############################
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation1000[1].programs[1].zone_programs[1].set_run_time(_minutes=45)
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation1000[1].programs[1].zone_programs[2].set_run_time(_minutes=45)
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation1000[1].programs[1].zone_programs[3].set_run_time(_minutes=45)
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=4)
            self.config.BaseStation1000[1].programs[1].zone_programs[4].set_run_time(_minutes=45)

            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=5)
            self.config.BaseStation1000[1].programs[2].zone_programs[5].set_run_time(_minutes=45)
            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=6)
            self.config.BaseStation1000[1].programs[2].zone_programs[6].set_run_time(_minutes=45)
            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=7)
            self.config.BaseStation1000[1].programs[2].zone_programs[7].set_run_time(_minutes=45)
            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=8)
            self.config.BaseStation1000[1].programs[2].zone_programs[8].set_run_time(_minutes=45)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_3(self):
        """
        Set up pause conditions for both Programs to pause when the moisture percent hits the lower limit of 10% \n
        Set the moisture percent on SB07270 and SB07263 to 11 and 14, respectively \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].add_moisture_pause_condition(_moisture_sensor_address=1)
            self.config.BaseStation1000[1].programs[1].moisture_pause_conditions[1].set_moisture_mode_to_lower_limit()
            self.config.BaseStation1000[1].programs[1].moisture_pause_conditions[1].set_moisture_threshold(_percent=10)

            self.config.BaseStation1000[1].programs[2].add_moisture_pause_condition(_moisture_sensor_address=2)
            self.config.BaseStation1000[1].programs[2].moisture_pause_conditions[2].set_moisture_mode_to_lower_limit()
            self.config.BaseStation1000[1].programs[2].moisture_pause_conditions[2].set_moisture_threshold(_percent=10)

            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=11)
            self.config.BaseStation1000[1].moisture_sensors[2].bicoder.set_moisture_percent(_percent=14)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_4(self):
        """
        Set the flow meters k-value to 2.01 and it's flow rate to 25 gpm \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].flow_meters[1].bicoder.set_k_value(_value=2.01)
            self.config.BaseStation1000[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=25)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_5(self):
        """
        ############################
        Setup Points of Connection
        ############################
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].add_poc(_poc_address=1)
            self.config.BaseStation1000[1].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=1)
            self.config.BaseStation1000[1].points_of_control[1].add_flow_meter_to_point_of_control(
                _flow_meter_address=1)
            self.config.BaseStation1000[1].points_of_control[1].set_target_flow(_gpm=500)
            self.config.BaseStation1000[1].points_of_control[1].set_high_flow_limit(_limit=550,
                                                                                    with_shutdown_enabled=True)
            self.config.BaseStation1000[1].points_of_control[1].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=True)
            self.config.BaseStation1000[1].points_of_control[1].set_limit_concurrent_to_target_state_enabled()
            self.config.BaseStation1000[1].points_of_control[1].set_fill_time(_minutes=2)
            self.config.BaseStation1000[1].points_of_control[1].set_flow_variance(_percent=10)
            self.config.BaseStation1000[1].points_of_control[1].set_flow_variance_enabled()
            self.config.BaseStation1000[1].points_of_control[1].set_flow_fault_state_enabled()

            self.config.BaseStation1000[1].programs[1].add_point_of_connection(_poc_address=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_6(self):
        """
        Load the alert relay to controller \n
        Give the alert relay a serial number and address \n
        Initialize all of it's values \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].add_alert_relay_to_controller(_address=1, _serial_number='AR00001')
            # set relay to closed so that we have a known state
            self.config.BaseStation1000[1].alert_relays[1].bicoder.vc = opcodes.closed
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_9(self):
        """
        Increment the clock by 10 seconds to allow the controller to react to commands \n
        Verify the full configuration \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(seconds=10)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_10(self):
        """
        Set the flag on the alert relay to 1 (unexpected high flow) \n
        Set an unexpected high flow message on poc 1 \n
        Increment clock by 10 seconds \n
        Verify the contact state is open and the flags is set to 1 \n
        Clear message from poc 1 \n
        Increment the clock by 10 seconds \n
        Now verify the flag is the same but the contact state is closed \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].alert_relays[1].set_flags(_flag=1)
            self.config.BaseStation1000[1].points_of_control[1].messages.set_unscheduled_flow_shutdown_message()
            self.config.BaseStation1000[1].do_increment_clock(seconds=10)
            self.config.BaseStation1000[1].alert_relays[1].bicoder.get_data()
            self.config.BaseStation1000[1].alert_relays[1].get_data()
            # Must do this manually because the controller does it without updating our objects
            self.config.BaseStation1000[1].alert_relays[1].bicoder.vc = opcodes.open
            self.config.BaseStation1000[1].alert_relays[1].bicoder.verify_contact_state()
            self.config.BaseStation1000[1].alert_relays[1].verify_flag()
            self.config.BaseStation1000[1].points_of_control[1].messages.clear_unscheduled_flow_shutdown_message()
            self.config.BaseStation1000[1].alert_relays[1].bicoder.get_data()
            self.config.BaseStation1000[1].alert_relays[1].get_data()
            # Must do this manually because the controller does it without updating our objects
            self.config.BaseStation1000[1].alert_relays[1].bicoder.vc = opcodes.closed
            self.config.BaseStation1000[1].do_increment_clock(seconds=10)
            self.config.BaseStation1000[1].alert_relays[1].bicoder.verify_contact_state()
            self.config.BaseStation1000[1].alert_relays[1].verify_flag()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_11(self):
        """
        Set the flag on the alert relay to 2 (high flow) \n
        Set an high flow message on poc 1 \n
        Increment clock by 10 seconds \n
        Verify the contact state is open and the flags is set to 2 \n
        Clear message from poc 1 \n
        Increment the clock by 10 seconds \n
        Now verify the flag is the same but the contact state is closed \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].alert_relays[1].set_flags(_flag=2)
            self.config.BaseStation1000[1].points_of_control[1].messages.set_high_flow_shutdown_message()
            self.config.BaseStation1000[1].do_increment_clock(seconds=10)
            self.config.BaseStation1000[1].alert_relays[1].bicoder.get_data()
            self.config.BaseStation1000[1].alert_relays[1].get_data()
            # Must do this manually because the controller does it without updating our objects
            self.config.BaseStation1000[1].alert_relays[1].bicoder.vc = opcodes.open
            self.config.BaseStation1000[1].alert_relays[1].bicoder.verify_contact_state()
            self.config.BaseStation1000[1].alert_relays[1].verify_flag()
            self.config.BaseStation1000[1].points_of_control[1].messages.clear_high_flow_shutdown_message()
            self.config.BaseStation1000[1].alert_relays[1].bicoder.get_data()
            self.config.BaseStation1000[1].alert_relays[1].get_data()
            # Must do this manually because the controller does it without updating our objects
            self.config.BaseStation1000[1].alert_relays[1].bicoder.vc = opcodes.closed
            self.config.BaseStation1000[1].do_increment_clock(seconds=10)
            self.config.BaseStation1000[1].alert_relays[1].bicoder.verify_contact_state()
            self.config.BaseStation1000[1].alert_relays[1].verify_flag()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_12(self):
        """
        Set the flag on the alert relay to 4 (zone flow variance) \n
        Set a high flow variance message on zone 4 \n
        Increment clock by 10 seconds \n
        Verify the contact state is open and the flags is set to 4 \n
        Clear message from zone 4 \n
        Increment the clock by 10 seconds \n
        Now verify the flag is the same but the contact state is closed \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].alert_relays[1].set_flags(_flag=4)
            self.config.BaseStation1000[1].zones[1].messages.set_exceeds_design_flow_message()
            self.config.BaseStation1000[1].do_increment_clock(seconds=10)
            self.config.BaseStation1000[1].alert_relays[1].bicoder.get_data()
            self.config.BaseStation1000[1].alert_relays[1].get_data()
            # Must do this manually because the controller does it without updating our objects
            self.config.BaseStation1000[1].alert_relays[1].bicoder.vc = opcodes.open
            self.config.BaseStation1000[1].alert_relays[1].bicoder.verify_contact_state()
            self.config.BaseStation1000[1].alert_relays[1].verify_flag()
            self.config.BaseStation1000[1].zones[1].messages.clear_exceeds_design_flow_message()
            self.config.BaseStation1000[1].alert_relays[1].bicoder.get_data()
            self.config.BaseStation1000[1].alert_relays[1].get_data()
            # Must do this manually because the controller does it without updating our objects
            self.config.BaseStation1000[1].alert_relays[1].bicoder.vc = opcodes.closed
            self.config.BaseStation1000[1].do_increment_clock(seconds=10)
            self.config.BaseStation1000[1].alert_relays[1].bicoder.verify_contact_state()
            self.config.BaseStation1000[1].alert_relays[1].verify_flag()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_13(self):
        """
        Set the flag on the alert relay to 8 (device no reply) \n
        Set a device no reply message on zone 1 \n
        Increment clock by 10 seconds \n
        Verify the contact state is open and the flags is set to 8 \n
        Clear message from zone 1 \n
        Increment the clock by 10 seconds \n
        Now verify the flag is the same but the contact state is closed \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].alert_relays[1].set_flags(_flag=8)
            self.config.BaseStation1000[1].zones[1].messages.set_no_response_message()
            self.config.BaseStation1000[1].do_increment_clock(seconds=10)
            self.config.BaseStation1000[1].alert_relays[1].bicoder.get_data()
            self.config.BaseStation1000[1].alert_relays[1].get_data()
            # Must do this manually because the controller does it without updating our objects
            self.config.BaseStation1000[1].alert_relays[1].bicoder.vc = opcodes.open
            self.config.BaseStation1000[1].alert_relays[1].bicoder.verify_contact_state()
            self.config.BaseStation1000[1].alert_relays[1].verify_flag()
            self.config.BaseStation1000[1].zones[1].messages.clear_no_response_message()
            self.config.BaseStation1000[1].alert_relays[1].bicoder.get_data()
            self.config.BaseStation1000[1].alert_relays[1].get_data()
            # Must do this manually because the controller does it without updating our objects
            self.config.BaseStation1000[1].alert_relays[1].bicoder.vc = opcodes.closed
            self.config.BaseStation1000[1].do_increment_clock(seconds=10)
            self.config.BaseStation1000[1].alert_relays[1].bicoder.verify_contact_state()
            self.config.BaseStation1000[1].alert_relays[1].verify_flag()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_14(self):
        """
        Set the flag on the alert relay to 16 (device checksum) \n
        Set a device checksum message on moisture sensor 1 \n
        Increment clock by 10 seconds \n
        Verify the contact state is open and the flags is set to 16 \n
        Clear message from moisture sensor 1 \n
        Increment the clock by 10 seconds \n
        Now verify the flag is the same but the contact state is closed \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].alert_relays[1].set_flags(_flag=16)
            self.config.BaseStation1000[1].moisture_sensors[1].messages.set_checksum_message()
            self.config.BaseStation1000[1].do_increment_clock(seconds=10)
            self.config.BaseStation1000[1].alert_relays[1].bicoder.get_data()
            self.config.BaseStation1000[1].alert_relays[1].get_data()
            # Must do this manually because the controller does it without updating our objects
            self.config.BaseStation1000[1].alert_relays[1].bicoder.vc = opcodes.open
            self.config.BaseStation1000[1].alert_relays[1].bicoder.verify_contact_state()
            self.config.BaseStation1000[1].alert_relays[1].verify_flag()
            self.config.BaseStation1000[1].moisture_sensors[1].messages.clear_checksum_message()
            self.config.BaseStation1000[1].alert_relays[1].bicoder.get_data()
            self.config.BaseStation1000[1].alert_relays[1].get_data()
            # Must do this manually because the controller does it without updating our objects
            self.config.BaseStation1000[1].alert_relays[1].bicoder.vc = opcodes.closed
            self.config.BaseStation1000[1].do_increment_clock(seconds=10)
            self.config.BaseStation1000[1].alert_relays[1].bicoder.verify_contact_state()
            self.config.BaseStation1000[1].alert_relays[1].verify_flag()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_15(self):
        """
        Set the flag on the alert relay to 32 (solenoid error) \n
        Set a device checksum message on zone 2 \n
        Increment clock by 10 seconds \n
        Verify the contact state is open and the flags is set to 32 \n
        Clear message from zone 2 \n
        Increment the clock by 10 seconds \n
        Now verify the flag is the same but the contact state is closed \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].alert_relays[1].set_flags(_flag=32)
            self.config.BaseStation1000[1].zones[2].messages.set_open_circuit_message()
            self.config.BaseStation1000[1].do_increment_clock(seconds=10)
            self.config.BaseStation1000[1].alert_relays[1].bicoder.get_data()
            self.config.BaseStation1000[1].alert_relays[1].get_data()
            # Must do this manually because the controller does it without updating our objects
            self.config.BaseStation1000[1].alert_relays[1].bicoder.vc = opcodes.open
            self.config.BaseStation1000[1].alert_relays[1].bicoder.verify_contact_state()
            self.config.BaseStation1000[1].alert_relays[1].verify_flag()
            self.config.BaseStation1000[1].zones[2].messages.clear_open_circuit_message()
            self.config.BaseStation1000[1].alert_relays[1].bicoder.get_data()
            self.config.BaseStation1000[1].alert_relays[1].get_data()
            # Must do this manually because the controller does it without updating our objects
            self.config.BaseStation1000[1].alert_relays[1].bicoder.vc = opcodes.closed
            self.config.BaseStation1000[1].do_increment_clock(seconds=10)
            self.config.BaseStation1000[1].alert_relays[1].bicoder.verify_contact_state()
            self.config.BaseStation1000[1].alert_relays[1].verify_flag()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_16(self):
        """
        Set the flag on the alert relay to 64 (program overrun) \n
        Set a device checksum message on program 1 \n
        Increment clock by 10 seconds \n
        Verify the contact state is open and the flags is set to 64 \n
        Clear message from program 1 \n
        Increment the clock by 10 seconds \n
        Now verify the flag is the same but the contact state is closed \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.BaseStation1000[1].alert_relays[1].set_flags(_flag=64)
            self.config.BaseStation1000[1].programs[1].messages.set_overrun_message()
            self.config.BaseStation1000[1].do_increment_clock(seconds=10)
            self.config.BaseStation1000[1].alert_relays[1].bicoder.get_data()
            self.config.BaseStation1000[1].alert_relays[1].get_data()
            # Must do this manually because the controller does it without updating our objects
            self.config.BaseStation1000[1].alert_relays[1].bicoder.vc = opcodes.open
            self.config.BaseStation1000[1].alert_relays[1].bicoder.verify_contact_state()
            self.config.BaseStation1000[1].alert_relays[1].verify_flag()
            self.config.BaseStation1000[1].programs[1].messages.clear_overrun_message()
            self.config.BaseStation1000[1].alert_relays[1].bicoder.get_data()
            self.config.BaseStation1000[1].alert_relays[1].get_data()
            # Must do this manually because the controller does it without updating our objects
            self.config.BaseStation1000[1].alert_relays[1].bicoder.vc = opcodes.closed
            self.config.BaseStation1000[1].do_increment_clock(seconds=10)
            self.config.BaseStation1000[1].alert_relays[1].bicoder.verify_contact_state()
            self.config.BaseStation1000[1].alert_relays[1].verify_flag()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_17(self):
        """
        Verify the full configuration \n
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        try:
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
