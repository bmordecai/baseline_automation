import sys
from time import sleep
from datetime import datetime, timedelta
from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.logging_handler import log_handler
from common import helper_methods
from common.imports import opcodes

__author__ = 'Eldin'


class ControllerUseCase10(object):
    """
    Test name:
        - Lower Limit One Time Calibration

    Purpose:
        - Run a program using a moisture Sensor and have it do a lower limit Calibration cycles
        - verify a calibration cycle will fail because the saturation level wasn't met
        - verify a calibration cycle will pass and set the correct values
        TODO Things I've noticed while trying to understand this test
            - When we start a calibration cycle, it doesn't wait for a start time, it will start in the next time
              increment of 10, so if we are at 9:45, we need to increment 5 minutes for the run to start.
            - The run does not do 150% of the zones normal runtime, it looks closer to 75%.
            - After the 24 hour time, the doc is right that it reads the moisture sensor and then sets the lower limit
              to be the moisture reading * .875 (or basically subtracting 12.5%), if you are doing a calibration on
              lower limit.
            - The doc is also right in that the program will not start for 24 hours after the calibration cycle.
            - After that 24 hours then the lower limit is adjusted.
    User Story:
        -

    Coverage area: \n
        This test covers smart watering with sensors. It covers the one time calibration for moisture sensor water for
        both
            upper and lower limits. \n
        -setting up devices:
            - Loading \n
            - Searching \n
            - Addressing \n
                - Setting:
                    - descriptions
                    - locations \n

            - do a single lower limit calibration \n
            - Normal calibration
            - calibration that moisture doesnt hit saturation

    Date References:
        - configuration for script is located common\configuration_files\One_time_calibration.json
        - the devices and addresses range is read from the .json file

    """
    # TODO Things to add to the test or to a new test \n
    # TODO calibration that moisture level doesnt move
    # TODO upper limit calibration
    # TODO do a single upper limit calibration \n (this not covered)
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """
        time_it_should_take_test_to_run = timedelta(minutes=15)  # this is minutes
        time_test_started = datetime.now()
        try:
            number_of_retries = 1
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.test_elapsed_time(_time_started=time_test_started,
                                                     _time_expected_to_run=time_it_should_take_test_to_run)
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        Setup Programs
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation1000[1].programs[1].set_enabled()
            self.config.BaseStation1000[1].programs[1].set_daily_water_window(['111111111111111111111111'])
            self.config.BaseStation1000[1].programs[1].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation1000[1].programs[1].set_seasonal_adjust(_percent=100)
            self.config.BaseStation1000[1].programs[1].set_soak_cycle_mode(_mode=opcodes.intelligent_soak)
            self.config.BaseStation1000[1].programs[1].set_cycle_count(_new_count=2)
            self.config.BaseStation1000[1].programs[1].set_soak_time_in_seconds(_seconds=1200)

            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(25.5)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_2(self):
        """
        ############################
        Setup Zone Programs
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation1000[1].programs[1].zone_programs[1].set_run_time(_minutes=10)
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation1000[1].programs[1].zone_programs[2].set_run_time(_minutes=20)
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation1000[1].programs[1].zone_programs[3].set_run_time(_minutes=10)
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=4)
            self.config.BaseStation1000[1].programs[1].zone_programs[4].set_run_time(_minutes=10)
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=5)
            self.config.BaseStation1000[1].programs[1].zone_programs[5].set_run_time(_minutes=10)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_3(self):
        """
        ############################
        Setup Points of Connection
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            self.config.BaseStation1000[1].add_poc(_poc_address=1)
            self.config.BaseStation1000[1].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=1)
            self.config.BaseStation1000[1].points_of_control[1].set_enabled()
            self.config.BaseStation1000[1].points_of_control[1].set_target_flow(_gpm=500)

            self.config.BaseStation1000[1].programs[1].add_point_of_connection(_poc_address=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_4(self):
        """
        set up start stop pause conditions
            Program 1:
                - start times 8:00 am \n
                - watering days are set to weekly \n
                - Watering days enabled are every day\n
                - assign moisture sensor SB05308 to start program 1 and give it a threshold of 25 \n
        """
        start_times_800am = [480]
        every_day_watering = [1, 1, 1, 1, 1, 1, 1]  # runs every day
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            self.config.BaseStation1000[1].programs[1].add_date_time_start_condition(_condition_number=1)
            self.config.BaseStation1000[1].programs[1].date_time_start_conditions[1].set_day_time_start(
                _st_list=start_times_800am,
                _interval_type=opcodes.week_days,
                _interval_args=every_day_watering
            )
            self.config.BaseStation1000[1].programs[1].add_moisture_start_condition(_moisture_sensor_address=1)
            self.config.BaseStation1000[1].programs[1].moisture_start_conditions[1].set_moisture_mode_to_lower_limit()
            self.config.BaseStation1000[1].programs[1].moisture_start_conditions[1].set_moisture_threshold(_percent=25.0)

        except Exception as e:
                e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                        "\tThe Controller Date and time was currently set to {2}\n" \
                        "\tThe Exception thrown was {3}".format(
                         self.config.test_name,
                         sys._getframe().f_code.co_name,
                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                         str(e.message))
                raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_5(self):
        """
        verify the full configuration of the controller
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_6(self):
        """
        - how it works run cycle time than soak time than take reading
        - set the controller date and time so that there is a known days of the week and days of the month \n
        - verify that all zones are working properly by verifying them before starting the test \n
        - start the moisture level at 20% and raise it to 25.5%
        - verify that the primary zones shut off
        - Verify that all link zone shut off
        - each zone has a 30 run time and a 20 minute cycle and a 30 minute soak
        - zone 3 is 50% watering time set to 15 minutes and 10 minute cycle time and a 30 minute soak
        - zone 4 is 150% watering time set to 45 minutes and 30 minute cycle time and a 30 minute soak
        when you first install a sensor you must wait 24 hours in order to do a calibration cycle \n
        set the moisture sensor to be below the upper limit \n
        the upper limit is set to 25 \n
        program 1 has a start time of 8:00 am \n
        set the clock to 08/27/2014 12:45 pm  and then increment time to verify the program starts \n
        do a self test on the moisture sensor so that the 3200 will read the sensors and update there current values \n
        verify that none of the zones have started watering \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].set_date_and_time(_date='08/27/2014', _time='23:45:00')
            self.config.BaseStation1000[1].verify_date_and_time()
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(23.5)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            self.config.BaseStation1000[1].programs[1].moisture_start_conditions[1].set_calibration_cycle_start()
            # We increment by 5 minutes so that we hit a '10 minute' barrier
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_waiting_to_water()
            # TODO STOPPED HERE, still need to revamp comments up top and in this step
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_7(self):
        """
        set moisture sensor values so the Programs can start watering and not be effected \n
        set moisture sensor value to 24% \n
        Do a self test to update the value in the controller \n
        advance the clock 2 minutes to make the time 08/28/2014 8:01 this should start the zones watering \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(24)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            self.config.BaseStation1000[1].set_controller_to_run()
            self.config.BaseStation1000[1].do_increment_clock(minutes=2)
            self.config.BaseStation1000[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_8(self):
        """
        advance the clock 10 minutes to make the time 08/28/2014 8:11 \n
        set the moisture sensor to 24.3% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:

            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(24.3)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            self.config.BaseStation1000[1].set_controller_to_run()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_9(self):
        """
        advance the clock 10 minutes to make the time 08/28/2014 8:21 \n
        set the moisture sensor to 24.5% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(24.5)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            self.config.BaseStation1000[1].set_controller_to_run()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_10(self):
        """
        advance the clock 10 minutes to make the time 08/28/2014 8:31 \n
        set the moisture sensor to 25.0% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 soaking \n
            - zone 4 watering \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(25)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            self.config.BaseStation1000[1].set_controller_to_run()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_11(self):
        """
        advance the clock 10 minutes to make the time 08/28/2014 8:41 \n
        set the moisture sensor to 25.5% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 soaking \n
            - zone 3 soaking \n
            - zone 4 soaking \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(25.5)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            self.config.BaseStation1000[1].set_controller_to_run()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_12(self):
        """
        advance the clock 10 minutes to make the time 08/28/2014 8:51 \n
        set the moisture sensor to 25.6% \n
        Do a self test to update the value in the controller \n
        beacause saturation was not hit zone 1 is set to soaking \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 watering \n
            - zone 3 soaking \n
            - zone 4 soaking \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(25.6)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            self.config.BaseStation1000[1].set_controller_to_run()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_13(self):
        """
        advance the clock 10 minutes to make the time 08/28/2014 9:01 \n
        set the moisture sensor to 26.0% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 soaking \n
            - zone 5 watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(26.0)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            sleep(10)
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_14(self):
        """
        advance the clock 10 minutes to make the time 08/28/2014 9:11 \n
        set the moisture sensor to 26.1% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 watering \n
            - zone 5 soaking \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(26.1)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_15(self):
        """
        advance the clock 10 minutes to make the time 08/28/2014 9:21 \n
        set the moisture sensor to 26.3% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 soaking \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(26.3)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_16(self):
        """
        advance the clock 10 minutes to make the time 08/28/2014 9:31 \n
        set the moisture sensor to 26.4% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_17(self):
        """
        advance the clock 10 minutes to make the time 08/28/2014 9:41 \n
        set the moisture sensor to 26.4% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_watering()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_18(self):
        """
        advance the clock 10 minutes to make the time 08/28/2014 9:51 \n
        set the moisture sensor to 26.4% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_soaking()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_19(self):
        """
        advance the clock 10 minutes to make the time 08/28/2014 10:01 \n
        set the moisture sensor to 26.4% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_soaking()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_20(self):
        """
        advance the clock 10 minutes to make the time 08/28/2014 10:11 \n
        set the moisture sensor to 26.4% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_watering()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_21(self):
        """
        advance the clock 10 minutes to make the time 08/28/2014 10:21 \n
        set the moisture sensor to 26.4% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_22(self):
        """
        - The controller waits 24 hours to display the failed message
        - Set clock to 08/29/2016 10:18 am
        - increment clock to 08/29/2016 10:19 am which is 24 hours after the zones that were calibrating finished watering
        - Check the message on the controller
        - Check the message on the controller
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation1000[1].set_date_and_time(_date='08/29/2014', _time='10:18:00')
            self.config.BaseStation1000[1].do_increment_clock(minutes=2)
            self.config.BaseStation1000[1].programs[1].verify_message(opcodes.calibrate_failure_no_change)
            # this clears the message and then verifies that it was cleared
            self.config.BaseStation1000[1].programs[1].clear_message(opcodes.calibrate_failure_no_change)
            print ('all is good')
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_23(self):
        """
        set the moisture sensor to 26.4% \n
        Do a self test to update the value in the controller \n
        advance the clock 10 minutes to make the time 08/29/2016 9:41 \n
        set the moisture sensor to 26.5% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(26.4)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            self.config.BaseStation1000[1].do_increment_clock(minutes=14)
            self.config.BaseStation1000[1].verify_date_and_time()
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(26.5)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_24(self):
        """
        set up start stop pause conditions
            Program 1:
                - reset lower limit threshold to 25%
                - assign moisture sensor SB05308 to start program 1 and give it a threshold of 25 \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].add_moisture_start_condition(_moisture_sensor_address=1)
            self.config.BaseStation1000[1].programs[1].moisture_start_conditions[2].set_moisture_mode_to_lower_limit()
            self.config.BaseStation1000[1].programs[1].moisture_start_conditions[2].set_moisture_threshold(_percent=25)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_25(self):
        """
        - how it works run cycle time than soak time than take reading
        - set the controller date and time so that there is a known days of the week and days of the month \n
        - verify that all zones are working properly by verifying them before starting the test \n
        - start the moisture level at 20% and raise it to 25.5%
        - verify that the primary zones shut off
        - Verify that all link zone shut off
        - each zone has a 30 run time and a 20 minute cycle and a 30 minute soak
        - zone 3 is 50% watering time set to 15 minutes and 10 minute cycle time and a 30 minute soak
        - zone 4 is 150% watering time set to 45 minutes and 30 minute cycle time and a 30 minute soak
        when you first install a sensor you must wait 24 hours in order to do a calibration cycle \n
        set the moisture sensor to be below the upper limit \n
        the upper limit is set to 25 \n
        program 1 has a start time of 8:00 am \n
        set the clock to 7:45 and then increment time to verify the program starts \n
        do a self test on the moisture sensor so that the 3200 will read the sensors and update there current values \n
        verify that none of the zones have started watering \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(24.0)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            self.config.BaseStation1000[1].set_date_and_time(_date='08/29/2014', _time='23:45:00')
            self.config.BaseStation1000[1].verify_date_and_time()
            self.config.BaseStation1000[1].do_increment_clock(minutes=20)
            self.config.BaseStation1000[1].verify_date_and_time()
        except AssertionError, ae:
            raise Exception(method + " " + self.config.BaseStation1000[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)
        try:
            self.config.BaseStation1000[1].set_date_and_time(_date='08/30/2014', _time='07:53:00')
            self.config.BaseStation1000[1].do_increment_clock(minutes=6)
            self.config.BaseStation1000[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_26(self):
        """
        set moisture sensor values so the Programs can start watering and not be effected \n
        set moisture sensor value to 24% \n
        Do a self test to update the value in the controller \n
        advance the clock 2 minutes to make the time 08/30/2016 8:01 this should start the zones watering \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(24.0)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            self.config.BaseStation1000[1].set_controller_to_run()
            self.config.BaseStation1000[1].do_increment_clock(minutes=2)
            self.config.BaseStation1000[1].verify_date_and_time()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_27(self):
        """
        advance the clock 10 minutes to make the time 08/30/2016 8:11 \n
        set the moisture sensor to 24.3% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:

            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(24.3)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            self.config.BaseStation1000[1].set_controller_to_run()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_28(self):
        """
        advance the clock 10 minutes to make the time 08/30/2016 8:21 \n
        set the moisture sensor to 24.5% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(24.5)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            self.config.BaseStation1000[1].set_controller_to_run()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_29(self):
        """
        advance the clock 10 minutes to make the time 08/30/2016 8:31 \n
        set the moisture sensor to 25.0% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(25.0)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            self.config.BaseStation1000[1].set_controller_to_run()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_30(self):
        """
        advance the clock 10 minutes to make the time 08/30/2016 8:41 \n
        set the moisture sensor to 25.5% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 soaking \n
            - zone 3 soaking \n
            - zone 4 waiting to water \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(25.5)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            self.config.BaseStation1000[1].set_controller_to_run()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_31(self):
        """
        advance the clock 10 minutes to make the time 08/30/2016 8:51 \n
        set the moisture sensor to 25.6% \n
        Do a self test to update the value in the controller \n
        beacause saturation was not hit zone 1 is set to soaking \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 done watering\n
            - zone 2 watering \n
            - zone 3 soaking \n
            - zone 4 waiting to water \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(25.6)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            self.config.BaseStation1000[1].set_controller_to_run()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_32(self):
        """
        advance the clock 10 minutes to make the time 08/30/2016 9:01 \n
        set the moisture sensor to 26.0% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(26.0)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            sleep(10)
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_33(self):
        """
        advance the clock 10 minutes to make the time 08/30/2016 9:11 \n
        set the moisture sensor to 26.1% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(26.1)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_34(self):
        """
        advance the clock 10 minutes to make the time 08/30/2016 9:21 \n
        set the moisture sensor to 26.3% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 watering \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(26.3)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_35(self):
        """
        advance the clock 10 minutes to make the time 08/30/2016 9:31 \n
        set the moisture sensor to 26.4% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 watering  \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(26.4)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_36(self):
        """
        advance the clock 10 minutes to make the time 08/30/2016 9:41 \n
        set the moisture sensor to 26.4% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 soaking \n
            - zone 5 watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(26.4)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_watering()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_37(self):
        """
        advance the clock 10 minutes to make the time 08/30/2016 9:51 \n
        set the moisture sensor to 26.4% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 soaking \n
            - zone 5 watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(26.4)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_watering()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_38(self):
        """
        advance the clock 10 minutes to make the time 08/30/2016 10:01 \n
        set the moisture sensor to 26.4% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 waiting to water\n
            - zone 5 watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(26.4)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_watering()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_39(self):
        """
        advance the clock 10 minutes to make the time 08/30/2016 10:11 \n
        set the moisture sensor to 26.4% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 watering \n
            - zone 5 soaking \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(26.4)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_soaking()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_40(self):
        """
        advance the clock 10 minutes to make the time 08/30/2016 10:21 \n
        set the moisture sensor to 26.4% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 watering \n
            - zone 5 soaking \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(26.4)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_soaking()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_41(self):
        """
        advance the clock 10 minutes to make the time 08/30/2016 10:31 \n
        set the moisture sensor to 26.4% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(26.4)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_watering()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_42(self):
        """
        advance the clock 10 minutes to make the time 08/30/2016 10:41 \n
        set the moisture sensor to 26.4% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running\n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 watering\n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(26.4)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_watering()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_43(self):
        """
        advance the clock 10 minutes to make the time 08/30/2016 10:51 \n
        set the moisture sensor to 26.4% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(26.4)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_watering()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_44(self):
        """
        advance the clock 10 minutes to make the time 08/30/2016 11:01 \n
        set the moisture sensor to 26.4% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=10)
            self.config.BaseStation1000[1].verify_date_and_time()
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.set_moisture_percent(26.4)
            self.config.BaseStation1000[1].moisture_sensors[1].bicoder.do_self_test()
            for zone in self.config.zn_ad_range:
                self.config.BaseStation1000[1].zones[zone].get_data()
            self.config.BaseStation1000[1].programs[1].get_data()
            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_45(self):
        """
        The controller waits 24 hours to to complete the calibration cycle
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation1000[1].set_date_and_time(_date='08/31/2014', _time='11:00:00')
            self.config.BaseStation1000[1].do_increment_clock(minutes=2)
            print ('all is good')
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_46(self):
        """
        Need to set the object sor that we can verify against the controller:
            - lower limit threshold of 23.1%
            - calibration cycle should now be stopped because it completed the calibration cycle
        """
        # TODO need to verify moisture threshold that got set the value should be 26.4% but Why???
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[2].moisture_start_conditions[2].threshold = 23.1
            # self.config.BaseStation1000[1].program_start_conditions[2]._cc = opcodes.stop
            # self.config.BaseStation1000[1].programs[2].moisture_start_conditions[2].

            print ('all is good')
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_47(self):
        """
        compare each zones information with the expected information to verify that everything is correct \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]