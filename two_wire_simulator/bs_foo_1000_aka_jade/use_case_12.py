import sys
from time import sleep
from datetime import datetime, timedelta
from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.logging_handler import log_handler
from common.imports import opcodes
from common import helper_methods


__author__ = 'Tige'


class ControllerUseCase12(object):
    """
    Test name:
        - Soak cycles
    purpose:
        -
      Coverage area: \n
    1. Able to disable zones \n
    2. Able to set zone concurrency \n
    3. Able to set up soak cycles and verify that they run properly \n

    Date References:
        - configuration for script is located common\configuration_files\soak_cycles.json
        - the devices and addresses range is read from the .json file

    """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """
        time_it_should_take_test_to_run = timedelta(minutes=15)  # this is minutes
        time_test_started = datetime.now()
        try:
            number_of_retries = 1
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                     # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.test_elapsed_time(_time_started=time_test_started,
                                                     _time_expected_to_run=time_it_should_take_test_to_run)
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        ############################
        Setup Programs
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        program_full_open_water_windows = ['111111111111111111111111']
        try:
            self.config.BaseStation1000[1].set_max_concurrent_zones(_max_zones=4)

            self.config.BaseStation1000[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation1000[1].programs[1].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation1000[1].programs[1].set_daily_water_window(_ww=program_full_open_water_windows)
            self.config.BaseStation1000[1].programs[1].set_seasonal_adjust(_percent=100)
            self.config.BaseStation1000[1].programs[1].set_soak_cycle_mode(_mode=opcodes.intelligent_soak)
            self.config.BaseStation1000[1].programs[1].set_cycle_count(_new_count=6)
            self.config.BaseStation1000[1].programs[1].set_soak_time_in_seconds(_seconds=1500)

            self.config.BaseStation1000[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation1000[1].programs[2].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation1000[1].programs[2].set_daily_water_window(_ww=program_full_open_water_windows)
            self.config.BaseStation1000[1].programs[2].set_seasonal_adjust(_percent=100)
            self.config.BaseStation1000[1].programs[2].set_soak_cycle_mode(_mode=opcodes.intelligent_soak)
            self.config.BaseStation1000[1].programs[2].set_cycle_count(_new_count=4)
            self.config.BaseStation1000[1].programs[2].set_soak_time_in_seconds(_seconds=900)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_2(self):
        """
        ############################
        Setup Zone Programs
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation1000[1].programs[1].zone_programs[1].set_run_time(_minutes=60)
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation1000[1].programs[1].zone_programs[2].set_run_time(_minutes=120)
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation1000[1].programs[1].zone_programs[3].set_run_time(_minutes=30)

            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=4)
            self.config.BaseStation1000[1].programs[2].zone_programs[4].set_run_time(_minutes=60)
            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=5)
            self.config.BaseStation1000[1].programs[2].zone_programs[5].set_run_time(_minutes=60)
            self.config.BaseStation1000[1].zones[5].set_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_3(self):
        """
        set up start stop pause conditions
            Program 1:
                - start times 8:00 am \n
                - watering days are set to weekly \n
                - Watering days enabled are every day\n
        """
        start_times_800am = [480]
        every_day_watering = [1, 1, 1, 1, 1, 1, 1]  # runs every day
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].programs[1].add_date_time_start_condition(_condition_number=1)
            self.config.BaseStation1000[1].programs[1].date_time_start_conditions[1].set_day_time_start(
                _st_list=start_times_800am,
                _interval_type=opcodes.week_days,
                _interval_args=every_day_watering
            )
            self.config.BaseStation1000[1].programs[2].add_date_time_start_condition(_condition_number=1)
            self.config.BaseStation1000[1].programs[2].date_time_start_conditions[1].set_day_time_start(
                _st_list=start_times_800am,
                _interval_type=opcodes.week_days,
                _interval_args=every_day_watering
            )
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_4(self):
        """
        verify the full configuration of the controller
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        try:
            self.config.verify_full_configuration()

        except Exception, e:
            raise Exception(method + " " + self.config.BaseStation1000[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)

    def step_6(self):
        """
        zone concurrency varies between Programs odd numbered Programs have a zone concurrency of 1 meaning those zones
        should run 1 time before stopping \n
        even numbered Programs have a zone concurrency of 2 meaning those zones should run twice back to back before
        stopping \n
        verify odd Programs run 1 time before stopping and even Programs run 2 times before stopping \n
        due to a lack of memory on the 1000 only 10 Programs can run at one time \n
        set clock to 04/08/2014 7:55 am this should start the zones watering \n
        \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 - 2 done watering \n
            - Zone status: \n
                - zone 1 done watering \n
                - zone 2 done watering \n
                - zone 3 done watering  \n
                - zone 4 done watering  \n
                - zone 5 done watering  \n

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for program in range(1, 3):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].set_controller_to_run()
            self.config.BaseStation1000[1].set_date_and_time(_date='04/08/2014', _time='07:55:00')
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_7(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 8:00 this should start the zones watering \n
        get data from Programs so that it can be verified against the object
        get data from zones that are addressed from the json file so that it can be verified against the object
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running \n
                - program 2 running \n
            - Zone status: \n
                - zone 1 watering \n
                - zone 2 watering \n
                - zone 3 waiting to water   \n
                - zone 4 watering   \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 3):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_running()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_8(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 8:05 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running \n
                - program 2 running \n

            - Zone status: \n
                - zone 1 watering \n
                - zone 2 waiting to water\n
                - zone 3 waiting to water  \n
                - zone 4 watering   \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 3):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_running()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_9(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 8:10 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running \n
                - program 2 running \n

            - Zone status: \n
                - zone 1 soaking \n
                - zone 2 watering\n
                - zone 3 waiting to water  \n
                - zone 4 watering  \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 3):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_running()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_10(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 8:15 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running \n
                - program 2 waiting to water \n

            - Zone status: \n
                - zone 1 soaking \n
                - zone 2 watering\n
                - zone 3 waiting to water  \n
                - zone 4 soaking  \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_11(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 8:20 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running \n
                - program 2 waiting to water \n

            - Zone status: \n
                - zone 1 soaking \n
                - zone 2 watering\n
                - zone 3 waiting to water  \n
                - zone 4 soaking  \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_12(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 8:25 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running \n
                - program 2 waiting to water \n

            - Zone status: \n
                - zone 1 soaking \n
                - zone 2 watering\n
                - zone 3 waiting to water  \n
                - zone 4 soaking  \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_13(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 8:30 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running \n
                - program 2 running \n

            - Zone status: \n
                - zone 1 soaking \n
                - zone 2 soaking\n
                - zone 3 watering  \n
                - zone 4 watering \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 3):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_running()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_14(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 8:35 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running \n
                - program 2 running \n

            - Zone status: \n
                - zone 1 watering\n
                - zone 2 soaking\n
                - zone 3 soaking  \n
                - zone 4 watering \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 3):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_running()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_15(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 8:40 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running \n
                - program 2 running \n

            - Zone status: \n
                - zone 1 watering \n
                - zone 2 soaking\n
                - zone 3 soaking  \n
                - zone 4 watering  \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 3):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_running()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_16(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 8:45 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 waiting to water \n
                - program 2 waiting to water\n

            - Zone status: \n
                - zone 1 soaking\n
                - zone 2 soaking\n
                - zone 3 soaking  \n
                - zone 4 soaking  \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_17(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 8:50 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 waiting to water \n
                - program 2 waiting to water\n

            - Zone status: \n
                - zone 1 soaking\n
                - zone 2 soaking\n
                - zone 3 soaking  \n
                - zone 4 soaking  \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_18(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 8:55 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running \n
                - program 2 waiting to water\n

            - Zone status: \n
                - zone 1 soaking\n
                - zone 2 watering\n
                - zone 3 soaking  \n
                - zone 4 soaking  \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_19(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 9:00 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running \n
                - program 2 running\n

            - Zone status: \n
                - zone 1 soaking\n
                - zone 2 watering\n
                - zone 3 waiting to water  \n
                - zone 4 watering  \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 3):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_running()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_20(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 9:05 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running \n
                - program 2 running \n

            - Zone status: \n
                - zone 1 soaking\n
                - zone 2 watering\n
                - zone 3 waiting to water  \n
                - zone 4 watering  \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 3):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_running()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_21(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 9:10 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running \n
                - program 2 running \n

            - Zone status: \n
                - zone 1 waiting to water\n
                - zone 2 watering\n
                - zone 3 waiting to water  \n
                - zone 4 watering  \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 3):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_running()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_22(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 9:15 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running \n
                - program 2 waiting to water \n

            - Zone status: \n
                - zone 1 watering\n
                - zone 2 soaking\n
                - zone 3 waiting to water  \n
                - zone 4 soaking  \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_23(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 9:20 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running \n
                - program 2 waiting to water \n

            - Zone status: \n
                - zone 1 watering\n
                - zone 2 soaking\n
                - zone 3 waiting to water  \n
                - zone 4 soaking  \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_24(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 9:25 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running \n
                - program 2 waiting to water \n

            - Zone status: \n
                - zone 1 watering\n
                - zone 2 soaking\n
                - zone 3 waiting to water  \n
                - zone 4 soaking  \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_25(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 9:30 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 waiting to water\n
                - program 2 running \n

            - Zone status: \n
                - zone 1 soaking\n
                - zone 2 soaking\n
                - zone 3 soaking  \n
                - zone 4 watering  \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_running()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_26(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 9:35 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 waiting to water\n
                - program 2 running \n

            - Zone status: \n
                - zone 1 soaking\n
                - zone 2 soaking\n
                - zone 3 soaking  \n
                - zone 4 watering  \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_running()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_27(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 9:40 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running\n
                - program 2 running \n

            - Zone status: \n
                - zone 1 soaking\n
                - zone 2 watering\n
                - zone 3 soaking  \n
                - zone 4 watering  \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            for program in range(1, 3):
                self.config.BaseStation1000[1].programs[program].statuses.verify_status_is_running()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_28(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 9:45 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running\n
                - program 2 done watering\n

            - Zone status: \n
                - zone 1 soaking\n
                - zone 2 watering\n
                - zone 3 soaking  \n
                - zone 4 done watering \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_29(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 9:50 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running\n
                - program 2 done watering\n

            - Zone status: \n
                - zone 1 waiting to water\n
                - zone 2 watering\n
                - zone 3 soaking  \n
                - zone 4 done watering \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_30(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 9:55 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running\n
                - program 2 done watering\n

            - Zone status: \n
                - zone 1 waiting to water \n
                - zone 2 watering\n
                - zone 3 waiting to water  \n
                - zone 4 done watering \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_31(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 10:00 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running\n
                - program 2 done watering\n

            - Zone status: \n
                - zone 1 watering\n
                - zone 2 soaking\n
                - zone 3 waiting to water  \n
                - zone 4 done watering \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_32(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 10:05 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running\n
                - program 2 done watering\n

            - Zone status: \n
                - zone 1 watering\n
                - zone 2 soaking\n
                - zone 3 waiting to water  \n
                - zone 4 done watering \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_33(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 10:10 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running\n
                - program 2 done watering\n

            - Zone status: \n
                - zone 1 soaking\n
                - zone 2 soaking\n
                - zone 3 watering \n
                - zone 4 done watering \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_34(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 10:15 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 waiting to water\n
                - program 2 done watering\n

            - Zone status: \n
                - zone 1 soaking\n
                - zone 2 soaking\n
                - zone 3 soaking \n
                - zone 4 done watering \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_35(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 10:20 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 waiting to water\n
                - program 2 done watering\n

            - Zone status: \n
                - zone 1 soaking\n
                - zone 2 soaking\n
                - zone 3 soaking \n
                - zone 4 done watering \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_36(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 10:25 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running\n
                - program 2 done watering\n

            - Zone status: \n
                - zone 1 soaking\n
                - zone 2 watering\n
                - zone 3 soaking \n
                - zone 4 done watering \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_37(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 10:30 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running\n
                - program 2 done watering\n

            - Zone status: \n
                - zone 1 soaking\n
                - zone 2 watering\n
                - zone 3 soaking \n
                - zone 4 done watering \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_38(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 10:35 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running\n
                - program 2 done watering\n

            - Zone status: \n
                - zone 1 waiting to water\n
                - zone 2 watering\n
                - zone 3 soaking \n
                - zone 4 done watering \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_39(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 10:40 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running\n
                - program 2 done watering\n

            - Zone status: \n
                - zone 1 waiting to water\n
                - zone 2 watering\n
                - zone 3 waiting to water \n
                - zone 4 done watering \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_40(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 10:45 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running\n
                - program 2 done watering\n

            - Zone status: \n
                - zone 1 watering\n
                - zone 2 soaking\n
                - zone 3 waiting to water \n
                - zone 4 done watering \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_41(self):
        """
        advance the clock 5 minutes to make the time 04/08/2014 10:50 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 running\n
                - program 2 done watering\n

            - Zone status: \n
                - zone 1 watering\n
                - zone 2 soaking\n
                - zone 3 waiting to water \n
                - zone 4 done watering \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_42(self):
        """
        advance the clock 1 hour and 35 minutes to make the time 04/08/2014 12:25 \n
        Verify the status for each program and zone and on the controller: \n
            - Program status: \n
                - program 1 done watering\n
                - program 2 done watering\n

            - Zone status: \n
                - zone 1 done watering\n
                - zone 2 done watering\n
                - zone 3 done watering \n
                - zone 4 done watering \n
                - zone 5 disabled   \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].do_increment_clock(hours=1, minutes=35)
            self.config.BaseStation1000[1].verify_date_and_time()

            self.config.BaseStation1000[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].programs[2].statuses.verify_status_is_done()

            self.config.BaseStation1000[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation1000[1].zones[5].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_43(self):
        """
        verify the full configuration of the controller
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        try:
            self.config.verify_full_configuration()

        except Exception, e:
            raise Exception(method + " " + self.config.BaseStation1000[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + e.message)