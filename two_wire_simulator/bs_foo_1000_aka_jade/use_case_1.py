import sys
from time import sleep
from datetime import datetime, timedelta
from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.logging_handler import log_handler
from common import helper_methods
from common.imports import opcodes

__author__ = 'Eldin'


class ControllerUseCase1(object):
    """
    Test name: \n
        Basic Learn Flow Test \n
    Purpose: \n
        This test covers basic Learn flow operations: \n
            First it clears the controller so we have a base point to start from, then it loads some device so that we
            can configure them into the controller (4 zones 1 POC 1 mainline, 1 program) \n
    Coverage area: \n
        1. Setting up the controller \n
        2. Searching and assigning: \n
            - Zones
            - Master Valves
            - Flow Meters
        3. Giving zones an address, description, serial number, latitude, and longitude \n
        4. Assigning zones to a program, and set their runtime to 1 hour \n
        5. Setting the default design flow values for the zones \n
        6. Setting a POC \n
        7. Change flow without changing the learn flow \n
        8. Change the design flow of multiple zones by enabling learn flow of a program \n
        9.  Learn flow of multiple zones one at a time \n
        10. Learn flow of multiple zones one at a time \n
        11. Learn flow of multiple zones one at a time \n
        12. Design flow of a zone being over written by a learn flow after a learn flow is complete \n
    """
    # TODO Things to add to the test or to a new test \n
    # TODO Changing flow rate while learning flow of a zone causing learn flow failures \n
    # TODO Changing flow rate during pipe fill time verify learn flow of a zone is still successful \n
    # TODO Changing flow rate during pipe fill time verify learn flow of a program is still successful \n
    # TODO check pipe file time \n
    # (The pipe fill time is a variable in the POC1000 object instead of mainline)
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """
        time_it_should_take_test_to_run = timedelta(minutes=15)  # this is minutes
        time_test_started = datetime.now()
        try:
            number_of_retries = 1
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.test_elapsed_time(_time_started=time_test_started,
                                                     _time_expected_to_run=time_it_should_take_test_to_run)
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #############################
    def step_1(self):
        """
        ############################
        Setup Programs
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].set_max_concurrent_zones(_max_zones=4)
            # Set the start time for each program to 8:00 am
            self.config.BaseStation1000[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation1000[1].programs[1].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation1000[1].programs[1].add_date_time_start_condition(_condition_number=1)
            self.config.BaseStation1000[1].programs[1].date_time_start_conditions[1].set_day_time_start(_st_list=[480])

            self.config.BaseStation1000[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation1000[1].programs[2].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation1000[1].programs[2].add_date_time_start_condition(_condition_number=1)
            self.config.BaseStation1000[1].programs[2].date_time_start_conditions[1].set_day_time_start(_st_list=[480])
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #############################
    def step_2(self):
        """
        ############################
        Setup Zone Programs
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set the number of soak cycles to 15 and the soak time to 300 seconds on each program
            # Assign zones to a program and give each zone a run time of 1 hour
            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=51)
            self.config.BaseStation1000[1].programs[1].zone_programs[51].set_run_time(_minutes=60)

            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=52)
            self.config.BaseStation1000[1].programs[1].zone_programs[52].set_run_time(_minutes=60)

            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=53)
            self.config.BaseStation1000[1].programs[1].zone_programs[53].set_run_time(_minutes=60)

            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=54)
            self.config.BaseStation1000[1].programs[1].zone_programs[54].set_run_time(_minutes=60)

            self.config.BaseStation1000[1].programs[1].add_zone_to_program(_zone_address=55)
            self.config.BaseStation1000[1].programs[1].zone_programs[55].set_run_time(_minutes=60)

            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=56)
            self.config.BaseStation1000[1].programs[2].zone_programs[56].set_run_time(_minutes=60)

            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=75)
            self.config.BaseStation1000[1].programs[2].zone_programs[75].set_run_time(_minutes=60)

            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=76)
            self.config.BaseStation1000[1].programs[2].zone_programs[76].set_run_time(_minutes=60)

            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=78)
            self.config.BaseStation1000[1].programs[2].zone_programs[78].set_run_time(_minutes=60)

            self.config.BaseStation1000[1].programs[2].add_zone_to_program(_zone_address=79)
            self.config.BaseStation1000[1].programs[2].zone_programs[79].set_run_time(_minutes=60)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #############################
    def step_3(self):
        """
        assign a design flow value to each zone so that they have a default setting \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Assign a design flow value to each zone so that they have a default setting
            self.config.BaseStation1000[1].zones[51].set_design_flow(_gallons_per_minute=20)
            self.config.BaseStation1000[1].zones[52].set_design_flow(_gallons_per_minute=20)
            self.config.BaseStation1000[1].zones[53].set_design_flow(_gallons_per_minute=20)
            self.config.BaseStation1000[1].zones[54].set_design_flow(_gallons_per_minute=12)
            self.config.BaseStation1000[1].zones[55].set_design_flow(_gallons_per_minute=7.5)
            self.config.BaseStation1000[1].zones[56].set_design_flow(_gallons_per_minute=15)
            self.config.BaseStation1000[1].zones[75].set_design_flow(_gallons_per_minute=15)
            self.config.BaseStation1000[1].zones[76].set_design_flow(_gallons_per_minute=7.5)
            self.config.BaseStation1000[1].zones[78].set_design_flow(_gallons_per_minute=7.5)
            self.config.BaseStation1000[1].zones[79].set_design_flow(_gallons_per_minute=7.5)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                         self.config.test_name,
                         sys._getframe().f_code.co_name,
                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                         str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #############################
    def step_4(self):
        """
        ############################
        Setup Points of Connection
        ############################
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].add_poc(_poc_address=1)
            self.config.BaseStation1000[1].points_of_control[1].add_flow_meter_to_point_of_control(
                _flow_meter_address=1)
            self.config.BaseStation1000[1].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=1)
            self.config.BaseStation1000[1].points_of_control[1].set_target_flow(_gpm=40)
            self.config.BaseStation1000[1].points_of_control[1].set_high_flow_limit(_limit=50,
                                                                                    with_shutdown_enabled=False)
            self.config.BaseStation1000[1].points_of_control[1].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=False)

            self.config.BaseStation1000[1].add_poc(_poc_address=2)
            self.config.BaseStation1000[1].points_of_control[2].set_disabled()

            self.config.BaseStation1000[1].programs[1].add_point_of_connection(_poc_address=1)
            self.config.BaseStation1000[1].programs[2].add_point_of_connection(_poc_address=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #############################
    def step_5(self):
        """
        Verify full Configuration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Assign a design flow value to each zone so that they have a default setting
            self.config.BaseStation1000[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #############################
    def step_6(self):
        """
        set the date and time of the controller so that there is a known days of the week and days of the month \n
        set flow sensor TWF0003 to 15 GPM \n
        verify the original zone flow settings \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].set_date_and_time(_date="04/08/2014", _time="08:00:00")  # Set Date/Time
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)  # Set the flow sensor TWF0003 to 15 GPM

            # Set flow sensor TWF0003 to 15 GPM
            self.config.BaseStation1000[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=15)

            # Verify the original zone flow settings
            self.config.BaseStation1000[1].zones[51].verify_design_flow()
            self.config.BaseStation1000[1].zones[52].verify_design_flow()
            self.config.BaseStation1000[1].zones[53].verify_design_flow()
            self.config.BaseStation1000[1].zones[54].verify_design_flow()
            self.config.BaseStation1000[1].zones[55].verify_design_flow()
            self.config.BaseStation1000[1].zones[56].verify_design_flow()
            self.config.BaseStation1000[1].zones[75].verify_design_flow()
            self.config.BaseStation1000[1].zones[76].verify_design_flow()
            self.config.BaseStation1000[1].zones[78].verify_design_flow()
            self.config.BaseStation1000[1].zones[79].verify_design_flow()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #############################
    def step_7(self):
        """
        turn on sim mode in the controller \n
        set the date and time of the controller so that there is a known days of the week and days of the month \n
        set flow sensor TWF0003 to 15 GPM \n
        perform a learn flow on program 1 \n
        increment the clock by 25 minutes so that the learn flow has time to run all the way through \n
        Verify that zones 1 through 5 have successfully learned flow, they should be at 15 GPM \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].set_date_and_time(_date="04/08/2014", _time="08:00:00")

            # Set flow sensor TWF0003 to 15 GPM
            self.config.BaseStation1000[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=15)
            self.config.BaseStation1000[1].set_learn_flow_enabled(_pg_ad=self.config.BaseStation1000[1].programs[1].ad)
            self.config.BaseStation1000[1].do_increment_clock(minutes=25)

            # These are the zones we expect are going to have their design flow changed by the learn flow
            for zone in range(51, 56):
                self.config.BaseStation1000[1].zones[zone].df = 15
            # Get the zone values from the controller so we can verify it against our objects
            for zone in sorted(self.config.BaseStation1000[1].zones.keys()):
                self.config.BaseStation1000[1].zones[zone].get_data()

            # Verify the original zone flow settings
            self.config.BaseStation1000[1].zones[51].verify_design_flow()
            self.config.BaseStation1000[1].zones[52].verify_design_flow()
            self.config.BaseStation1000[1].zones[53].verify_design_flow()
            self.config.BaseStation1000[1].zones[54].verify_design_flow()
            self.config.BaseStation1000[1].zones[55].verify_design_flow()
            self.config.BaseStation1000[1].zones[56].verify_design_flow()
            self.config.BaseStation1000[1].zones[75].verify_design_flow()
            self.config.BaseStation1000[1].zones[76].verify_design_flow()
            self.config.BaseStation1000[1].zones[78].verify_design_flow()
            self.config.BaseStation1000[1].zones[79].verify_design_flow()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                         self.config.test_name,
                         sys._getframe().f_code.co_name,
                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                         str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #############################
    def step_8(self):
        """
        set flow sensor TWF0003 to 25 GPM \n
        set the pipe fill time to 1 minute \n
        perform a learn flow on zone 51 \n
        increment clock by 5 minutes so that the learn flow has time to run all the way through \n
        verify that all zones remained the same except for zone 1 which should be 25 GPM now \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=25)
            self.config.BaseStation1000[1].points_of_control[1].set_fill_time(_minutes=1)
            self.config.BaseStation1000[1].set_learn_flow_enabled(_pg_ad=self.config.BaseStation1000[1].programs[1].ad,
                                                                  _zn_ad=self.config.BaseStation1000[1].zones[51].ad)
            self.config.BaseStation1000[1].do_increment_clock(minutes=5)

            # This is the only zone we expect to be changed by the learn flow
            self.config.BaseStation1000[1].zones[51].df = 25
            # Get the zone values from the controller so we can verify it against our objects
            for zone in sorted(self.config.BaseStation1000[1].zones.keys()):
                self.config.BaseStation1000[1].zones[zone].get_data()

            # Verify the original zone flow settings
            self.config.BaseStation1000[1].zones[51].verify_design_flow()
            self.config.BaseStation1000[1].zones[52].verify_design_flow()
            self.config.BaseStation1000[1].zones[53].verify_design_flow()
            self.config.BaseStation1000[1].zones[54].verify_design_flow()
            self.config.BaseStation1000[1].zones[55].verify_design_flow()
            self.config.BaseStation1000[1].zones[56].verify_design_flow()
            self.config.BaseStation1000[1].zones[75].verify_design_flow()
            self.config.BaseStation1000[1].zones[76].verify_design_flow()
            self.config.BaseStation1000[1].zones[78].verify_design_flow()
            self.config.BaseStation1000[1].zones[79].verify_design_flow()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                         self.config.test_name,
                         sys._getframe().f_code.co_name,
                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                         str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #############################
    def step_9(self):
        """
        change to number of concurrent zones for the controller and each program \n
        turn on sim mode in the controller \n
        stop  the clock so that it can be incremented manually \n
        set the date and time of the controller so that there is a known days of the week and days of the month \n
        Set flow sensor TWF0003 to 50 GPM
        Perform a learn flow on zone 2
        increment clock by 12 minutes so that the learn flow has time to run all the way through \n
        verify that the only zones affected by the learn flow were 52 and 53 \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].set_max_concurrent_zones(_max_zones=10)
            self.config.BaseStation1000[1].programs[1].set_max_concurrent_zones(_number_of_zones=2)
            self.config.BaseStation1000[1].programs[2].set_max_concurrent_zones(_number_of_zones=3)
            self.config.BaseStation1000[1].set_sim_mode_to_on()
            self.config.BaseStation1000[1].stop_clock()
            self.config.BaseStation1000[1].set_date_and_time(_date="04/08/2014", _time="08:00:00")
            self.config.BaseStation1000[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=50)
            self.config.BaseStation1000[1].set_learn_flow_enabled(_pg_ad=self.config.BaseStation1000[1].programs[1].ad,
                                                                  _zn_ad=self.config.BaseStation1000[1].zones[52].ad)
            self.config.BaseStation1000[1].do_increment_clock(minutes=12)

            # This is the only zone we expect to be changed by the learn flow
            self.config.BaseStation1000[1].zones[52].df = 50
            # Get the zone values from the controller so we can verify it against our objects
            for zone in sorted(self.config.BaseStation1000[1].zones.keys()):
                self.config.BaseStation1000[1].zones[zone].get_data()

            # Verify the original zone flow settings
            self.config.BaseStation1000[1].zones[51].verify_design_flow()
            self.config.BaseStation1000[1].zones[52].verify_design_flow()
            self.config.BaseStation1000[1].zones[53].verify_design_flow()
            self.config.BaseStation1000[1].zones[54].verify_design_flow()
            self.config.BaseStation1000[1].zones[55].verify_design_flow()
            self.config.BaseStation1000[1].zones[56].verify_design_flow()
            self.config.BaseStation1000[1].zones[75].verify_design_flow()
            self.config.BaseStation1000[1].zones[76].verify_design_flow()
            self.config.BaseStation1000[1].zones[78].verify_design_flow()
            self.config.BaseStation1000[1].zones[79].verify_design_flow()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                         self.config.test_name,
                         sys._getframe().f_code.co_name,
                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                         str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #############################
    def step_10(self):
        """
        change to number of concurrent zones for the controller and each program \n
        turn on sim mode in the controller \n
        stop the clock so that it can be incremented manually \n
        set the date and time of the controller so that there is a known days of the week and days of the month \n
        Set flow sensor TWF0003 to 50 GPM \n
        Perform a learn flow on zone 53 \n
        increment clock by 12 minutes \n
        verify that all zones remained the same except zones 52 and 53, they should be 50 GPM \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].set_max_concurrent_zones(_max_zones=10)
            self.config.BaseStation1000[1].programs[1].set_max_concurrent_zones(_number_of_zones=2)
            self.config.BaseStation1000[1].programs[2].set_max_concurrent_zones(_number_of_zones=3)
            self.config.BaseStation1000[1].set_sim_mode_to_on()
            self.config.BaseStation1000[1].stop_clock()
            self.config.BaseStation1000[1].set_date_and_time(_date="04/08/2014", _time="08:00:00")
            self.config.BaseStation1000[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=50)
            self.config.BaseStation1000[1].set_learn_flow_enabled(_pg_ad=self.config.BaseStation1000[1].programs[1].ad,
                                                                  _zn_ad=self.config.BaseStation1000[1].zones[53].ad)
            self.config.BaseStation1000[1].do_increment_clock(minutes=12)

            # This is the only zone we expect to be changed by the learn flow
            self.config.BaseStation1000[1].zones[53].df = 50
            # Get the zone values from the controller so we can verify it against our objects
            for zone in sorted(self.config.BaseStation1000[1].zones.keys()):
                self.config.BaseStation1000[1].zones[zone].get_data()

            # Verify the original zone flow settings
            self.config.BaseStation1000[1].zones[51].verify_design_flow()
            self.config.BaseStation1000[1].zones[52].verify_design_flow()
            self.config.BaseStation1000[1].zones[53].verify_design_flow()
            self.config.BaseStation1000[1].zones[54].verify_design_flow()
            self.config.BaseStation1000[1].zones[55].verify_design_flow()
            self.config.BaseStation1000[1].zones[56].verify_design_flow()
            self.config.BaseStation1000[1].zones[75].verify_design_flow()
            self.config.BaseStation1000[1].zones[76].verify_design_flow()
            self.config.BaseStation1000[1].zones[78].verify_design_flow()
            self.config.BaseStation1000[1].zones[79].verify_design_flow()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                         self.config.test_name,
                         sys._getframe().f_code.co_name,
                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                         str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #############################
    def step_11(self):
        """
        change the pipe fill time to 3 minutes \n
        turn on sim mode in the controller \n
        stop the clock so that it can be incremented manually \n
        set the date and time of the controller so that there is a known days of the week and days of the month \n
        set flow sensor TWF0003 to 15 GPM \n
        perform a learn flow on program 1 \n
        increment the clock by 15 minutes \n
        perform a learn flow on zone 2 \n
        change the flow sensor reading from 15 for a minute to 20 for a minute to 5 for a minute \n
        verify that program 1 learned flow and that zone 2 successful learned flow again \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].points_of_control[1].set_fill_time(_minutes=3)
            self.config.BaseStation1000[1].set_sim_mode_to_on()
            self.config.BaseStation1000[1].stop_clock()
            self.config.BaseStation1000[1].set_date_and_time(_date="04/08/2014", _time="08:00:00")
            self.config.BaseStation1000[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=15)
            self.config.BaseStation1000[1].set_learn_flow_enabled(self.config.BaseStation1000[1].programs[1].ad)
            self.config.BaseStation1000[1].do_increment_clock(minutes=25)
            self.config.BaseStation1000[1].set_learn_flow_enabled(_pg_ad=self.config.BaseStation1000[1].programs[1].ad,
                                                                  _zn_ad=self.config.BaseStation1000[1].zones[52].ad)
            self.config.BaseStation1000[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=20)
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            self.config.BaseStation1000[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=5)
            self.config.BaseStation1000[1].do_increment_clock(minutes=1)
            self.config.BaseStation1000[1].do_increment_clock(minutes=15)

            # These are the zones expected to be changed by learn flow
            self.config.BaseStation1000[1].zones[51].df = 15
            self.config.BaseStation1000[1].zones[52].df = 5
            self.config.BaseStation1000[1].zones[53].df = 15
            # Get the zone values from the controller so we can verify it against our objects
            for zone in sorted(self.config.BaseStation1000[1].zones.keys()):
                self.config.BaseStation1000[1].zones[zone].get_data()

            # Verify the original zone flow settings
            self.config.BaseStation1000[1].zones[51].verify_design_flow()
            self.config.BaseStation1000[1].zones[52].verify_design_flow()
            self.config.BaseStation1000[1].zones[53].verify_design_flow()
            self.config.BaseStation1000[1].zones[54].verify_design_flow()
            self.config.BaseStation1000[1].zones[55].verify_design_flow()
            self.config.BaseStation1000[1].zones[56].verify_design_flow()
            self.config.BaseStation1000[1].zones[75].verify_design_flow()
            self.config.BaseStation1000[1].zones[76].verify_design_flow()
            self.config.BaseStation1000[1].zones[78].verify_design_flow()
            self.config.BaseStation1000[1].zones[79].verify_design_flow()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                         self.config.test_name,
                         sys._getframe().f_code.co_name,
                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                         str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #############################
    def step_12(self):
        """
        Verify full Configuration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Assign a design flow value to each zone so that they have a default setting
            self.config.BaseStation1000[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                         self.config.test_name,
                         sys._getframe().f_code.co_name,
                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                         str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
