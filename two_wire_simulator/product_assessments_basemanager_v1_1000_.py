from datetime import datetime, timedelta
import sys
import os

import old_32_10_sb_objects_dec_29_2017.common.user_configuration as user_conf_module

# bmw use cases
import old_32_10_sb_objects_dec_29_2017.bmw_bs1000_v1_scripts.use_case_1 as uc1_module
import old_32_10_sb_objects_dec_29_2017.bmw_bs1000_v1_scripts.use_case_2 as uc2_module
import old_32_10_sb_objects_dec_29_2017.bmw_bs1000_v1_scripts.use_case_5 as uc5_module
import old_32_10_sb_objects_dec_29_2017.bmw_bs1000_v1_scripts.use_case_6 as uc6_module

_author__ = 'ben'


def run_use_cases_basemanager_v1_1000(user_configuration_file_name, passing_tests, failing_tests,
                                      manual_tests, specific_tests):
    """
    This is where we will run all of our SubStation + 3200 use cases. \n

    :param user_configuration_file_name:    The name of the configuration file we will be using for the tests. \n
    :type user_configuration_file_name:     str
    
    :param auto_update_fw:    If you want to automatically update firmware versions of controllers to "latest" version.
    :type auto_update_fw:     bool

    :param passing_tests:   If you want to run passing tests. True for yes, False for no. \n
    :type passing_tests:    bool

    :param failing_tests:   If you want to run failing tests. True for yes, False for no. \n
    :type failing_tests:    bool

    :param manual_tests:    If you want to run manual tests. True for yes, False for no. \n
    :type manual_tests:     bool

    :param specific_tests:  Pick which tests you want to run individually. Will run all if the list is empty. \n
    :type specific_tests:   list[int]
    """

    # Load in user configured items from text file (necessary for serial connection)
    # user_conf = user_conf_module.UserConfiguration(os.path.join('common/user_credentials', user_configuration_file_name), _auto_update=auto_update_fw)

    # ---------------------------------------------------------------------------------------------------------------- #
    #                                                                                                                  #
    #                                             BaseManager + 1000 Tests                                             #
    #                                                                                                                  #
    # ---------------------------------------------------------------------------------------------------------------- #
    # ---------------------------------------------------------------------------------------------------------------- #
    # ##########################################------Passing Tests------############################################# #
    # ---------------------------------------------------------------------------------------------------------------- #
    if passing_tests:
        if 1 in specific_tests or len(specific_tests) == 0:
            uc1 = uc1_module.BaseManagerUseCase1(controller_type="10",
                                                 test_name="BM-UseCase1-NewETProgTest-1000",
                                                 user_configuration_instance=user_conf,
                                                 json_configuration_file='update_cn.json')
            uc1.run_use_case()
        if 2 in specific_tests or len(specific_tests) == 0:
            uc2 = uc2_module.BaseManagerUseCase2(controller_type="10",
                                                 test_name="BM-UseCase2-MenuTabTest-1000",
                                                 user_configuration_instance=user_conf,
                                                 json_configuration_file='update_cn.json')
            uc2.run_use_case()
        if 5 in specific_tests or len(specific_tests) == 0:

            uc5 = uc5_module.BaseManagerUseCase5(controller_type="10",
                                                 test_name="BM-UseCase5-AddressDevTest-1000",
                                                 user_configuration_instance=user_conf,
                                                 json_configuration_file='update_cn.json')
            uc5.run_use_case()
        if 6 in specific_tests or len(specific_tests) == 0:

            uc6 = uc6_module.BaseManagerUseCase6(controller_type="10",
                                                 test_name="BM-UseCase6-SubscriptioTest-1000",
                                                 user_configuration_instance=user_conf,
                                                 json_configuration_file='update_cn.json')
            uc6.run_use_case()

        print("##################### ----FINISHED RUNNING PASSING BASEMANAGER + 1000 TESTS---- #######################")
    
    # ---------------------------------------------------------------------------------------------------------------- #
    # ##########################################------Manual Tests------############################################## #
    # ---------------------------------------------------------------------------------------------------------------- #
    # ---------------------------------------------------------------------------------------------------------------- #
    #                                                                                                                  #
    #      (Requires Manual Device/Use-Case Modification)                                                              #
    #                                                                                                                  #
    #    - THESE USE CASES ARE NOT APART OF NIGHTLY BUILD                                                              #
    #    - These use cases require setting breakpoints at specific locations in the test in order to configure         #
    #      a real-device to be in an expected state for the test.                                                      #
    #    - Above each use case is a list of break points to set inside the use case. The breakpoints should point      #
    #      to a "To Do" item which explains what the user needs to do.                                                 #
    #                                                                                                                  #
    # ---------------------------------------------------------------------------------------------------------------- #
    if manual_tests:

        print("##################### ----FINISHED RUNNING MANUAL BASEMANAGER + 1000 TESTS---- ########################")

    # ---------------------------------------------------------------------------------------------------------------- #
    # ##########################################------Failing Tests------############################################# #
    # ---------------------------------------------------------------------------------------------------------------- #
    if failing_tests:

        print("##################### ----FINISHED RUNNING FAILING BASEMANAGER + 1000 TESTS---- #######################")

    # ---------------------------------------------------------------------------------------------------------------- #
    # #######################################------Future Needed Tests------########################################## #
    # ---------------------------------------------------------------------------------------------------------------- #

    print("####################### ---- YOU ARE A WINNER WINNER CHICKEN DINNER ---- #######################")


if __name__ == "__main__":
    run_use_cases_basemanager_v1_1000(user_configuration_file_name="user_credentials_eldin.json",
                                      passing_tests=True,
                                      failing_tests=False,
                                      manual_tests=False,
                                      specific_tests=[])
    exit()
