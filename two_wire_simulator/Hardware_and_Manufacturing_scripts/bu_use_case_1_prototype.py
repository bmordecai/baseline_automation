__author__ = 'baseline'

from common.imports import *
import common.user_configuration as user_conf_mod
import common.configuration as test_conf_mod
import time


from multiprocessing import Process, Pipe, Queue
from common.objects.baseunit import bu_imports, bu_helpers, bu_mfg_factory, bu
from common.objects.programming import zn
from common.objects.base_classes import ser
from tcp_handler import TcpHandler


class BUUseCase1ProtoType:

    def __init__(self, _conf, _test_mode, _verbose, _company, _type, _tcp):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n
        :type _conf: user_conf_mod.UserConfiguration

        :param _verbose: Boolean for printing out additional information received from controller
        :type _verbose: bool

        :param _company: Company name selected by user in the UI
        :type _company: str

        :param _type: Decoder Category selected by user in the UI
        :type _type: str

        :param _tcp: tcp connection
        :type _tcp: TcpHandler
        """
        self.helper = bu_mfg_factory.MFGFactory(_conf)
        self.test_bu = self.helper.get_base_unit_object()
        self.test_bu.ui_company_name = _company
        self.test_bu.ui_decoder_category = _type
        self.test_zone = self.helper.get_remote_zone()
        self.tcp_conn = _tcp

        self.test_mode = _test_mode
        self.test_limits = None
        self.verbose = _verbose

        self.step1_message = ""
        self.step2_message = ""
        self.step3_message = ""
        self.step4_message = ""
        self.step5_message = ""
        self.step6_message = ""
        self.step7_message = ""
        self.step8_message = ""
        self.step9_message = ""

    def run_use_case(self):
        """
        Step 1:
        - Saves BaseUnit Version, current, and voltage values
        - Reset BaseUnit
        - IF ReadBaseUnitVersionType != OK  -> error
        - IF BaseUnitType != BaseUnitOEM    -> error
        - IF PowerUpTwoWire != OK           -> error

        Step 2:
        - Read SN of Device being tested and check type and version
        - IF ReadSerialNumber(global) != OK -> error
        - IF ReadOEMTypeVer(SerNum) != OK   -> error
        - IF Version != OK or Type != OK    -> error

        Step 3:
        - Write and verify address(s) to Decoder
        - IF 12Valve --
        -       Write/Verify Addresses 1-26
        - ELSE --
        -       FOR i=1 to # of valves --
        -           Write/Verify Address(1)

        Step 4:
        - Measure two-wire voltage drop and test range

        Step 5:
        - Check BitSkews

        Step 6:
        - Activate valves, read and check current and voltage
        - FOR i=1 to number of valves --
        -       Prompt user to touch test fixture to correct post
        -       Test valve(i)
        -       User clicks 'OK' to continue test
        -       IF user clicks 'Cancel'     -> error

        Step 7:
        - Blink each LED
        - Ask user if LEDs blink correctly
        - IF user clicks 'NO'               -> error

        Step 8:
        - Write Null Address(s)
        - Null all addresses written in Step 7

        Step 9:
        - Verify new SN with user then write and verify SN to Decoder
        - IF SerialNumber == Default        -> NextSerialNumber
        - Verify SN with user or Exit
        - IF WriteSerialNumber != OK        -> error

        """
        self.test_limits = bu_helpers.get_device_limits(_company=self.test_bu.ui_company_name,
                                                        _valve=self.test_bu.ui_decoder_category)

        self.tcp_conn.start_connection()

        reply = self.tcp_conn.receive_msg()

        self.tcp_conn.send_msg("Test Started ----")
        self.set_test_limits(reply)

        # Run Tests
        while True:
            step_res = self.step1(_verbose=self.verbose)
            self.tcp_send(_message=self.step1_message,
                            _packet_type=bu_imports.PacketTypes.STEP_OUTPUT,
                            _step_num=1,
                            _results=step_res)
            if step_res == bu_imports.TCP.TEST_RESULTS.FAILED:
                break

            step_res = self.step2(_verbose=self.verbose)
            self.tcp_send(_message=self.step2_message,
                            _packet_type=bu_imports.PacketTypes.STEP_OUTPUT,
                            _step_num=2,
                            _results=step_res)
            if step_res == bu_imports.TCP.TEST_RESULTS.FAILED:
                break

            step_res = self.step3(_verbose=self.verbose)
            self.tcp_send(_message=self.step3_message,
                            _packet_type=bu_imports.PacketTypes.STEP_OUTPUT,
                            _step_num=3,
                            _results=step_res)
            if step_res == bu_imports.TCP.TEST_RESULTS.FAILED:
                break

            step_res = self.step4(_verbose=self.verbose)
            self.tcp_send(_message=self.step4_message,
                            _packet_type=bu_imports.PacketTypes.STEP_OUTPUT,
                            _step_num=4,
                            _results=step_res)
            if step_res == bu_imports.TCP.TEST_RESULTS.FAILED:
                break

            step_res = self.step5(_verbose=self.verbose)
            self.tcp_send(_message=self.step5_message,
                            _packet_type=bu_imports.PacketTypes.STEP_OUTPUT,
                            _step_num=5,
                            _results=step_res)
            if step_res == bu_imports.TCP.TEST_RESULTS.FAILED:
                break

            step_res = self.step6(_verbose=self.verbose)
            self.tcp_send(_message=self.step6_message,
                            _packet_type=bu_imports.PacketTypes.STEP_OUTPUT,
                            _step_num=6,
                            _results=step_res)
            if step_res == bu_imports.TCP.TEST_RESULTS.FAILED:
                break

            step_res = self.step7(_verbose=self.verbose)
            self.tcp_send(_message=self.step7_message,
                            _packet_type=bu_imports.PacketTypes.STEP_OUTPUT,
                            _step_num=7,
                            _results=step_res)
            if step_res == bu_imports.TCP.TEST_RESULTS.FAILED:
                break

            step_res = self.step8(_verbose=self.verbose)
            self.tcp_send(_message=self.step8_message,
                            _packet_type=bu_imports.PacketTypes.STEP_OUTPUT,
                            _step_num=8,
                            _results=step_res)
            if step_res == bu_imports.TCP.TEST_RESULTS.FAILED:
                break

            step_res = self.step9(_verbose=self.verbose)
            self.tcp_send(_message=self.step9_message,
                            _packet_type=bu_imports.PacketTypes.STEP_OUTPUT,
                            _step_num=9,
                            _results=step_res)
            if step_res == bu_imports.TCP.TEST_RESULTS.FAILED:
                break

            break

        self.tcp_send(_message="Test Complete!",
                        _packet_type=bu_imports.PacketTypes.TEST_RESULTS,
                        _step_num=0,
                        _results=bu_imports.TCP.TEST_RESULTS.NA)




    # ########################################################
    def step1(self, _verbose=False):
        """
        Step 1:
        - Saves BaseUnit Version, current, and voltage values
        - Reset BaseUnit
        - IF ReadBaseUnitVersionType != OK  -> error
        - IF BaseUnitType != BaseUnitOEM    -> error
        - IF PowerUpTwoWire != OK           -> error

        :param _verbose: Boolean for printing out additional information received from controller
        :type _verbose: bool
        """
        self.test_bu.reboot()
        time.sleep(4)

        # Command sent to BU
        self.test_bu.read_version()

        # Error message if error returned from BU
        if self.test_bu.ss != bu_imports.BaseErrors.STATUS_OK:
            self.step1_message = "Step1 Error: ReadBaseUnitVersion - {0}\n{1}"\
                .format(bu_helpers.get_status_text(self.test_bu.ss),                                        # 0
                        bu_helpers.get_help_text(bu_imports.ErrorHelpText.BASE_NO_REPLY, self.test_bu.ss))  # 1
            if _verbose:
                self.step1_message += "\n{0}".format(self.test_bu.data_string)
            return bu_imports.TCP.TEST_RESULTS.FAILED

        # Error message if type returned does not match test limits
        if self.test_bu.ty != self.test_limits['Base']['Type']:
            self.step1_message = "Step1 Error: BaseUnit Type/Ver - Incorrect Type or Version\n" \
                                 "Returned Type/Ver = {0} / {1}\n" \
                                 "Expected Type = {2}\n" \
                                 "{3}"\
                .format(self.test_bu.ty,                                                                      # 0
                        self.test_bu.vr,                                                                      # 1
                        self.test_limits['Base']['Type'],                                                     # 2
                        bu_helpers.get_help_text(bu_imports.ErrorHelpText.BASE_WRONG_TYPE, self.test_bu.ss))  # 3
            if _verbose:
                self.step1_message += "\n{0}".format(self.test_bu.data_string)
            return bu_imports.TCP.TEST_RESULTS.FAILED

        # Command sent to BU
        self.test_bu.power_up_2_wire()

        # Error message if error returned from BU
        if self.test_bu.ss != bu_imports.BaseErrors.STATUS_OK:
            self.step1_message = "Step1 Error: PowerUp2Wire - {0}\n{1}"\
                .format(bu_helpers.get_status_text(self.test_bu.ss),                                           # 0
                        bu_helpers.get_help_text(bu_imports.ErrorHelpText.BASE_POWER_UP, self.test_bu.ss))     # 1
            if _verbose:
                self.step1_message += "\n{0}".format(self.test_bu.data_string)
            return bu_imports.TCP.TEST_RESULTS.FAILED

        time.sleep(1.5)

        # Command sent to BU
        self.test_bu.read_power()

        # Error message if error returned from BU
        if self.test_bu.ss != bu_imports.BaseErrors.STATUS_OK:
            self.step1_message = "Step1 Error: ReadPowerBaseUnit - {0}\n{1}"\
                .format(bu_helpers.get_status_text(self.test_bu.ss),                                           # 0
                        bu_helpers.get_help_text(bu_imports.ErrorHelpText.BASE_POWER_UP, self.test_bu.ss))     # 1
            if _verbose:
                self.step1_message += "\n{0}".format(self.test_bu.data_string)
            return bu_imports.TCP.TEST_RESULTS.FAILED

        # Error message if current_a (negative) is less than base currentmax test limits or
        #               if current_b (positive) is less than base_currentmax test limits or
        #               if voltage_a is less than base voltagemin test limits
        if self.test_bu.current_a < self.test_limits["Base"]["CurrentMax"] or \
                self.test_bu.current_b < self.test_limits["Base"]["CurrentMax"] or \
                self.test_bu.voltage_a < self.test_limits["Base"]["VoltageMin"]:
            self.step1_message = "Step1 Error: ReadPowerBaseUnit - Current or Voltage out of range\n" \
                                 "Returned Current A/B = {0}/{1}, Voltage A = {2}\n" \
                                 "Expected Currents < {3}, Expected Voltage > {4}\n" \
                                 "{5}"\
                .format(self.test_bu.current_a,                                                                     # 0
                        self.test_bu.current_b,                                                                     # 1
                        self.test_bu.voltage_a,                                                                     # 2
                        self.test_limits['Base']['CurrentMax'],                                                     # 3
                        self.test_limits['Base']['VoltageMin'],                                                     # 4
                        bu_helpers.get_help_text(bu_imports.ErrorHelpText.BASE_CURRENT_VOLTAGE, self.test_bu.ss))   # 5
            if _verbose:
                self.step1_message += "\n{0}".format(self.test_bu.data_string)
            return bu_imports.TCP.TEST_RESULTS.FAILED

        # Success message if all tests pass
        self.step1_message = "Step1 Complete: ReadPowerBaseUnit - Current or Voltage OK\n" \
                             "Returned Current A/B = {0}/{1}, Voltage A = {2}\n" \
                             "Expected Currents < {3}, Expected Voltage > {4}\n" \
                             "{5}"\
            .format(self.test_bu.current_a,                     # 0
                    self.test_bu.current_b,                     # 1
                    self.test_bu.voltage_a,                     # 4
                    self.test_limits['Base']['CurrentMax'],     # 6
                    self.test_limits['Base']['VoltageMin'],     # 7
                    self.test_bu.data_string)                   # 8

        return bu_imports.TCP.TEST_RESULTS.PASSED

    # ########################################################
    def step2(self, _verbose=False):
        """
        Step 2:
        - Read SN of Device being tested and check type and version
        - IF ReadSerialNumber(global) != OK -> error
        - IF ReadOEMTypeVer(SerNum) != OK   -> error
        - IF Version != OK or Type != OK    -> error
        pass

        :param _verbose: Boolean for printing out additional information received from controller
        :type _verbose: bool
        """
        self.test_bu.r_reset_remote_device(_remote_device=self.test_zone)

        time.sleep(0.25)

        # Command sent to BU which is sent to remote dev
        self.test_bu.r_read_serial_number(_remote_device=self.test_zone)

        # Error message if error returned from BU
        if self.test_bu.ss != bu_imports.BaseErrors.STATUS_OK:
            self.step2_message = "{0}\nStep2 Error: ReadSerialNumber - {1}\n{2}"\
                .format(self.test_zone.final_sn[0],                                                           # 0
                        bu_helpers.get_status_text(self.test_bu.ss),                                          # 1
                        bu_helpers.get_help_text(bu_imports.ErrorHelpText.REMOTE_READ_SN, self.test_bu.ss))   # 2
            if _verbose:
                self.step2_message += "\n{0}".format(self.test_bu.data_string)
            return bu_imports.TCP.TEST_RESULTS.FAILED

        # If manufacturer name from UI is Baseline
        if self.test_bu.ui_company_name is bu_imports.CompanyNames.BASELINE:

            # Command send to BU which is sent to remote dev
            self.test_bu.r_unlock_serial_number(_remote_device=self.test_zone)

            # Error message if error returned from BU
            if self.test_bu.ss != bu_imports.BaseErrors.STATUS_OK:
                self.step2_message = "{0}\nStep2 Error: UnlockSerialNumber - {0}\n{1}"\
                    .format(self.test_zone.final_sn[0],                                                             # 0
                            bu_helpers.get_status_text(self.test_bu.ss),                                            # 1
                            bu_helpers.get_help_text(bu_imports.ErrorHelpText.REMOTE_UNLOCK_SN, self.test_bu.ss))   # 2
                if _verbose:
                    self.step2_message += "\n{0}".format(self.test_bu.data_string)
                return bu_imports.TCP.TEST_RESULTS.FAILED

        # If manufacturer name from UI != Baseline
        else:

            # Command sent to BU which is sent to remote dev
            self.test_bu.r_read_remote_version_oem(_remote_device=self.test_zone)

            # Error message if error returned from BU
            if self.test_bu.ss != bu_imports.BaseErrors.STATUS_OK:
                self.step2_message = "{0}\nStep2 Error: ReadRemoteVersionOEM - {0}\n{1}"\
                    .format(self.test_zone.final_sn[0],                                                              # 0
                            bu_helpers.get_status_text(self.test_bu.ss),                                             # 1
                            bu_helpers.get_help_text(bu_imports.ErrorHelpText.REMOTE_VERSION_OEM, self.test_bu.ss))  # 2
                if _verbose:
                    self.step2_message += "\n{0}".format(self.test_bu.data_string)
                return bu_imports.TCP.TEST_RESULTS.FAILED

        # Error message if decoder category (1 Valve, 2 Valve, etc) from UI does not match type returned from BU
        if not self.type_ok():
            self.step2_message = "{0}\n" \
                                 "Step2 Error: Decoder Type/Ver - Incorrect Type \n" \
                                 "Returned Type/Ver = {1} / {2}\n" \
                                 "Expected Type/Ver = {3} / {4}\n" \
                                 "{5}"\
                .format(self.test_zone.final_sn[0],                                         # 0
                        self.test_zone.ty,                                                  # 1
                        self.test_zone.vr,                                                  # 2
                        self.expected_type_text(),                                          # 3
                        self.expected_ver_text(),                                           # 4
                        bu_helpers.get_help_text(bu_imports.ErrorHelpText.REMOTE_VERSION_BAD, self.test_bu.ss)    # 5
                        )
            if _verbose:
                self.step2_message += "\n{0}".format(self.test_bu.data_string)
            return bu_imports.TCP.TEST_RESULTS.FAILED

        # Error message if version returned from BU does not match expected version based on expected type (decoder cat)
        if not self.ver_ok():
            self.step2_message = "{0}\n" \
                                 "Step2 Error: Decoder Type/Ver - Incorrect Version \n" \
                                 "Returned Type/Ver = {1} / {2}\n" \
                                 "Expected Type/Ver = {3} / {4}\n" \
                                 "{5}"\
                .format(self.test_zone.final_sn[0],                                         # 0
                        self.test_zone.ty,                                                  # 1
                        self.test_zone.vr,                                                  # 2
                        self.expected_type_text(),                                          # 3
                        self.expected_ver_text(),                                           # 4
                        bu_helpers.get_help_text(bu_imports.ErrorHelpText.REMOTE_VERSION_BAD, self.test_bu.ss)    # 5
                        )
            if _verbose:
                self.step2_message += "\n{0}".format(self.test_bu.data_string)
            return bu_imports.TCP.TEST_RESULTS.FAILED

        # Success message if all tests pass
        self.step2_message = "{0}\n" \
                             "Step2 Success: Decoder Type/Ver OK\n" \
                             "Returned Type/Ver = {1} / {2}\n" \
                             "Expected Type/Ver = {3} / {4}\n" \
                             "{5}"\
            .format(self.test_zone.final_sn[0],     # 0
                    self.test_zone.ty,              # 1
                    self.test_zone.vr,              # 2
                    self.expected_type_text(),      # 3
                    self.expected_ver_text(),       # 4
                    self.test_bu.data_string)       # 5

        self.test_zone.r_temp_sn = self.test_zone.final_sn[0]
        return bu_imports.TCP.TEST_RESULTS.PASSED

    # ########################################################
    def step3(self, _verbose=False):
        """
        Step 3:
        - Write and verify address(s) to Decoder
        - IF 12Valve --
        -       Write/Verify Addresses 1-26
        - ELSE --
        -       FOR i=1 to # of valves --
        -           Write/Verify Address(1)

        :param _verbose: Boolean for printing out additional information received from controller
        :type _verbose: bool
        """

        # Reset remote device if self.test_mode boolean is set to True
        if self.test_mode:
            self.test_bu.r_reset_remote_device(_remote_device=self.test_zone)
            time.sleep(.25)

        # If the remote device is either a 12 valve or 24 valve...
        if self.test_bu.ui_decoder_category is bu_imports.DeviceTypes.TWELVE_VALVE_STR or \
           self.test_bu.ui_decoder_category is bu_imports.DeviceTypes.TWENTY_FOUR_VALVE_STR:

            self.test_zone.final_sn[1] = "VB{0}".format(self.test_zone.final_sn[0][2:])
            self.test_zone.final_sn[2] = "VE{0}".format(self.test_zone.final_sn[0][2:])

            # Cycles through each valve 1 - 12
            for i in range(1, 13):

                # Calls write_addr_chk_sn on first bank of twelve valves and fails step3() if returned False
                if not self.write_addr_chk_sn(_zone_num=i, _bank12R=0, _verbose=_verbose):
                    return bu_imports.TCP.TEST_RESULTS.FAILED

                # Calls write_addr_chk_sn on second bank of twelve valves on 24 V decoders and fails step3() if
                # returned false
                if self.test_bu.ui_decoder_category is bu_imports.DeviceTypes.TWENTY_FOUR_VALVE_STR:
                    if not self.write_addr_chk_sn(_zone_num=i + 12, _bank12R=1, _verbose=_verbose):
                        return bu_imports.TCP.TEST_RESULTS.FAILED

                    # Calls write_addr_chk_sn on last 2 valves and fails step3() if returned false
                    if i < 3:
                        if not self.write_addr_chk_sn(_zone_num=i + 24, _bank12R=2, _verbose=_verbose):
                            return bu_imports.TCP.TEST_RESULTS.FAILED

        # If the remote device is 1, 2, or 4 valve...
        else:

            # Cycles through all valves in decoder depending on the type of decoder
            for i in range(0, self.expected_type_min()):

                # Calls write_addr_chk_sn on all valves and fails step3() if returned false
                if not self.write_addr_chk_sn(_zone_num=i + 1, _bank12R=0, _verbose=_verbose):
                    return bu_imports.TCP.TEST_RESULTS.FAILED

        # Success message if all tests pass
        self.step3_message = "{0}\n" \
                             "Step3 Success: Address(es) Written - Verified\n" \
                             "Serial Number = {1}\n" \
                             "{2}"\
            .format(self.test_zone.final_sn[0],     # 0
                    self.test_zone.r_temp_sn,       # 1
                    self.test_bu.data_string)       # 2

        return bu_imports.TCP.TEST_RESULTS.PASSED

    # ########################################################
    def step4(self, _verbose=False):
        """
        Step 4:
            - Measure two-wire voltage drop and test range

        :param _verbose: Boolean for printing out additional information received from controller
        :type _verbose: bool
        """

        # Reset remote device if self.test_mode boolean is set to True
        if self.test_mode:
            self.test_bu.r_reset_remote_device(_remote_device=self.test_zone)
            time.sleep(.25)

        # Command sent to BU
        self.test_bu.test_two_wire_drop(_remote_device=self.test_zone)

        # Error message if error returned from BU
        if self.test_bu.ss != bu_imports.BaseErrors.STATUS_OK:
            self.step4_message = "{0}\nStep4 Error: TestTwoWireDrop - {1}\n{2}"\
                .format(self.test_zone.final_sn[0],                                                                 # 0
                        bu_helpers.get_status_text(self.test_bu.ss),                                                # 1
                        bu_helpers.get_help_text(bu_imports.ErrorHelpText.REMOTE_TWO_WIRE_DROP, self.test_bu.ss))   # 2
            if _verbose:
                self.step4_message += "\n{0}".format(self.test_bu.data_string)
            return bu_imports.TCP.TEST_RESULTS.FAILED

        # Error message if two wire drop value returned from BU is not within test_limits
        if self.test_zone.vt < self.test_limits["TwoWireDrop"]["Min"] or \
           self.test_zone.vt > self.test_limits["TwoWireDrop"]["Max"]:
            self.step4_message = "{0}\n" \
                                 "Step4 Error: TestTwoWireDrop - Out of Range\n" \
                                 "Returned = {1}\n" \
                                 "Expected = {2} to {3}\n" \
                                 "{4}"\
                .format(self.test_zone.final_sn[0],                                                             # 0
                        self.test_zone.vt,                                                                      # 1
                        self.test_limits["TwoWireDrop"]["Min"],                                                 # 2
                        self.test_limits["TwoWireDrop"]["Max"],                                                 # 3
                        bu_helpers.get_help_text(bu_imports.ErrorHelpText.REMOTE_UNLOCK_SN, self.test_bu.ss))   # 4
            if _verbose:
                self.step4_message += "\n{0}".format(self.test_bu.data_string)
            return bu_imports.TCP.TEST_RESULTS.FAILED

        # Success message if all tests pass
        self.step4_message = "{0}\n" \
                             "Step4 Success: TestTwoWireDrop OK\n" \
                             "Returned = {1}\n" \
                             "Expected = {2} to {3}\n" \
                             "{4}"\
            .format(self.test_zone.final_sn[0],                 # 0
                    self.test_zone.vt,                          # 1
                    self.test_limits["TwoWireDrop"]["Min"],     # 2
                    self.test_limits["TwoWireDrop"]["Max"],     # 3
                    self.test_bu.data_string)                   # 4

        return bu_imports.TCP.TEST_RESULTS.PASSED

    # ########################################################
    def step5(self, _verbose=False):
        """
        Step 5:
        - Check BitSkews

        :param _verbose: Boolean for printing out additional information received from controller
        :type _verbose: bool
        """

        # Reset remote device if self.test_mode boolean is set to True
        if self.test_mode:
            self.test_bu.r_reset_remote_device(_remote_device=self.test_zone)
            time.sleep(.25)

        # Command sent to BU
        self.test_bu.test_base_channel(_remote_device=self.test_zone)

        # Error message if error returned from BU
        if self.test_bu.ss != bu_imports.BaseErrors.STATUS_OK:
            self.step5_message = "{0}\nStep5 Error: TestChannel - {1}\n{2}"\
                .format(self.test_zone.final_sn[0],                                                                 # 0
                        bu_helpers.get_status_text(self.test_bu.ss),                                                # 1
                        bu_helpers.get_help_text(bu_imports.ErrorHelpText.REMOTE_BIT_TIMES, self.test_bu.ss))       # 2
            if _verbose:
                self.step5_message += "\n{0}".format(self.test_bu.data_string)
            return bu_imports.TCP.TEST_RESULTS.FAILED

        # Set base bit times using returned values from BU
        self.test_bu.bit_time_base_long_hi = self.test_bu.bit1
        self.test_bu.bit_time_base_long_lo = self.test_bu.bit2
        self.test_bu.bit_time_base_short_hi = self.test_bu.bit4
        self.test_bu.bit_time_base_short_lo = self.test_bu.bit5

        # Check each of the bit times ^^^ to make sure they are within limits, error if not within limits
        if abs(self.test_limits["BitTime"]["BaseLong"] - self.test_bu.bit_time_base_long_hi) > \
                self.test_bu.bit_time_margin * self.test_limits["BitTime"]["BaseLong"] or \
                abs(self.test_limits["BitTime"]["BaseLong"] - self.test_bu.bit_time_base_long_lo) > \
                self.test_bu.bit_time_margin * self.test_limits["BitTime"]["BaseLong"] or \
                abs(self.test_limits["BitTime"]["BaseShort"] - self.test_bu.bit_time_base_short_hi) > \
                self.test_bu.bit_time_margin * self.test_limits["BitTime"]["BaseShort"] or \
                abs(self.test_limits["BitTime"]["BaseShort"] - self.test_bu.bit_time_base_short_lo) > \
                self.test_bu.bit_time_margin * self.test_limits["BitTime"]["BaseShort"]:

            self.step5_message = "{0}\n" \
                                 "Step5 Error: TestChannel - Bit Times BaseUnit Receive\n" \
                                 "Returned = {1}\n" \
                                 "Expected = {2}\n" \
                                 "{3}"\
                .format(self.test_zone.final_sn[0],                                                             # 0
                        self.test_bu.get_bit_time_base(),                                                       # 1
                        self.test_bu.get_bit_time_base(self.test_limits),                                       # 2
                        bu_helpers.get_help_text(bu_imports.ErrorHelpText.REMOTE_BIT_TIMES, self.test_bu.ss))   # 3
            if _verbose:
                self.step5_message += "\n{0}".format(self.test_bu.data_string)

        # Command sent to BU
        self.test_bu.r_test_remote_channel(_remote_device=self.test_zone)

        # Error message if error returned from BU
        if self.test_bu.ss != bu_imports.BaseErrors.STATUS_OK:
            self.step5_message = "{0}\nStep5 Error: TestChannel - {1}\n{2}"\
                .format(self.test_zone.final_sn[0],                                                                 # 0
                        bu_helpers.get_status_text(self.test_bu.ss),                                                # 1
                        bu_helpers.get_help_text(bu_imports.ErrorHelpText.REMOTE_BIT_TIMES, self.test_bu.ss))       # 2
            if _verbose:
                self.step5_message += "\n{0}".format(self.test_bu.data_string)
            return bu_imports.TCP.TEST_RESULTS.FAILED

        # TODO: These values may not be being set correctly because they are returning pretty much the same values
        # TODO:   for both long and short
        # Set remote bit times using returned values from BU
        self.test_bu.bit_time_remote_long_hi = self.test_bu.bit1
        self.test_bu.bit_time_remote_long_lo = self.test_bu.bit2
        self.test_bu.bit_time_remote_short_hi = self.test_bu.bit4
        self.test_bu.bit_time_remote_short_lo = self.test_bu.bit5

        # Check each of the bit times ^^^ to make sure they are within limits, error if not within limits
        if abs(self.test_limits["BitTime"]["RemoteLong"] - self.test_bu.bit_time_remote_long_hi) > \
                self.test_bu.bit_time_margin * self.test_limits["BitTime"]["RemoteLong"] or \
                abs(self.test_limits["BitTime"]["RemoteLong"] - self.test_bu.bit_time_remote_long_lo) > \
                self.test_bu.bit_time_margin * self.test_limits["BitTime"]["RemoteLong"] or \
                abs(self.test_limits["BitTime"]["RemoteShort"] - self.test_bu.bit_time_remote_short_hi) > \
                self.test_bu.bit_time_margin * self.test_limits["BitTime"]["RemoteShort"] or \
                abs(self.test_limits["BitTime"]["RemoteShort"] - self.test_bu.bit_time_remote_short_lo) > \
                self.test_bu.bit_time_margin * self.test_limits["BitTime"]["RemoteShort"]:

            self.step5_message = "{0}\n" \
                                 "Step5 Error: TestChannel - Bit Times Remote Receive\n" \
                                 "Returned = {1} - Variance: Long - {2}\{3} Short - {4}/{5}\n" \
                                 "Expected = {6} - Variance: Long - {7} Short - {8}\n" \
                                 "{9}"\
                .format(self.test_zone.final_sn[0],                                                             # 0
                        self.test_bu.get_bit_time_remote(),                                                     # 1
                        abs(self.test_limits["BitTime"]["RemoteLong"] - self.test_bu.bit_time_remote_long_hi),
                        abs(self.test_limits["BitTime"]["RemoteLong"] - self.test_bu.bit_time_remote_long_lo),
                        abs(self.test_limits["BitTime"]["RemoteShort"] - self.test_bu.bit_time_remote_short_hi),
                        abs(self.test_limits["BitTime"]["RemoteShort"] - self.test_bu.bit_time_remote_short_lo),
                        self.test_bu.get_bit_time_remote(self.test_limits),                                     # 2
                        self.test_bu.bit_time_margin * self.test_limits["BitTime"]["RemoteLong"],
                        self.test_bu.bit_time_margin * self.test_limits["BitTime"]["RemoteShort"],
                        bu_helpers.get_help_text(bu_imports.ErrorHelpText.REMOTE_BIT_TIMES, self.test_bu.ss))   # 3
            if _verbose:
                self.step5_message += "\n{0}".format(self.test_bu.data_string)
            return bu_imports.TCP.TEST_RESULTS.FAILED

        # Success message if all tests pass
        self.step5_message = "{0}\n" \
                             "Step5 Success: TestChannel - Bit Times OK\n" \
                             "Returned = {1}\n" \
                             "Expected = {2}\n" \
                             "{3}"\
            .format(self.test_zone.final_sn[0],                     # 0
                    self.test_bu.get_bit_times(),                   # 1
                    self.test_bu.get_bit_times(self.test_limits),   # 2
                    self.test_bu.data_string)                       # 3

        return bu_imports.TCP.TEST_RESULTS.PASSED

    # ########################################################
    def step6(self, _verbose=False):
        """
        Step 6:
        - Activate valves, read and check current and voltage
        - FOR i=1 to number of valves --
        -       Prompt user to touch test fixture to correct post
        -       Test valve(i)
        -       User clicks 'OK' to continue test
        -       IF user clicks 'Cancel'     -> error

        :param _verbose: Boolean for printing out additional information received from controller
        :type _verbose: bool
        """

        # Reset remote device if self.test_mode boolean is set to True
        if self.test_mode:
            self.test_bu.r_reset_remote_device(_remote_device=self.test_zone)
            time.sleep(.25)

        # Does the following if the decoder is a 12 or 24 valve
        if self.test_bu.ui_decoder_category is bu_imports.DeviceTypes.TWELVE_VALVE_STR or \
                self.test_bu.ui_decoder_category is bu_imports.DeviceTypes.TWENTY_FOUR_VALVE_STR:

            # Temp variables for the loop
            valve_num = 1
            attempt = 0

            # Loops through 26 valves
            while valve_num <= 26:

                # If the remote_dev is a 12 valve and the loop is on 13, the count is advanced to 25
                if self.test_bu.ui_decoder_category is bu_imports.DeviceTypes.TWELVE_VALVE_STR and \
                        valve_num is 13:
                    valve_num = 25

                # special use case for valves 1, 2, 13, 14 where the user must perform an action
                if valve_num in [1, 2, 13, 14]:
                    attempt = 1

                    # Starts loop to attempt the following operations on a valve, produces error if the attempt fails
                    #       more than one time
                    while True:

                        # Test name based on decoder type
                        str_temp = ""
                        if self.test_bu.ui_decoder_category is bu_imports.DeviceTypes.TWELVE_VALVE_STR:
                            str_temp = "12R PCB biSensor Test ({0})".format(attempt)
                        else:
                            str_temp = "24R PCB biSensor Test ({0})".format(attempt)

                        # Sends packet to UI that creates a message box that user can click either ok or cancel.
                        #       Test name above and prompt_message() message are sent to UI for the message box
                        reply = self.tcp_send(_test_name=str_temp, _message=self.prompt_message(valve_num),
                                                _packet_type=bu_imports.PacketTypes.PROMPT_UI, _step_num=6,
                                                _type=bu_imports.PromptTypes.OK_CANCEL)

                        # If user clicks 'cancel', the test fails
                        if reply is "Cancel":
                            self.step6_message = "Step6B Error: biSensor Test FAILED\n" \
                                                 "Cancelled by User"
                            return bu_imports.TCP.TEST_RESULTS.FAILED

                        self.test_bu.r_sleep_serial_number(_remote_device=self.test_zone, _ser_num_index=0)
                        self.test_bu.r_sleep_serial_number(_remote_device=self.test_zone, _ser_num_index=1)
                        self.test_bu.r_sleep_serial_number(_remote_device=self.test_zone, _ser_num_index=2)

                        # Uses a temp address of 0 and calls read_serial_number on that address
                        temp_addr = 0
                        self.test_bu.r_read_serial_number(_remote_device=self.test_zone, _temp_addr=temp_addr)

                        # Calls unlock_serial_number if the above operation is successful
                        if self.test_bu.ss is bu_imports.BaseErrors.STATUS_OK:
                            self.test_bu.r_unlock_serial_number(_remote_device=self.test_zone,
                                                                _temp_sn=self.test_zone.r_temp_sn)

                            # If the version returned from the above call is 3.4, break from the while loop and continue
                            #       step6 operation
                            if self.test_zone.vr is "3.4":
                                break

                        # Error message if operations in this while loop have been performed at least twice
                        if attempt > 1:
                            self.step6_message = "{0}\nStep6 Error: biSensor IO Test {1}\n{2}\n{3}"\
                                .format(self.test_zone.final_sn[0],                                             # 0
                                        self.test_bu.ss,                                                        # 1
                                        bu_helpers.get_help_text(bu_imports.ErrorHelpText.REMOTE_SENSOR_IO),    # 2
                                        self.test_bu.data_string)                                               # 3
                            return bu_imports.TCP.TEST_RESULTS.FAILED

                        # Increment attempt number for the first pass of this while loop if we haven't broken from loop
                        #       yet
                        attempt += 1

                # Resets attempt number for next operations within the test on the specific valve_num the outside loop
                #   is currently on
                attempt = 1

                # Runs test_valve on all valves after previous tests have passed.
                while True:
                    self.test_bu.r_reset_remote_device()

                    # If loop has run through more than once or valve is not special case, send info to UI to create
                    #       message box with prompt_message for the specific valve_num
                    if attempt > 1 or valve_num not in [1, 2, 13, 14]:
                        str_temp = ""
                        if self.test_bu.ui_decoder_category is bu_imports.DeviceTypes.TWELVE_VALVE_STR:
                            str_temp = "12R PCB biSensor Test ({0})".format(attempt)
                        else:
                            str_temp = "24R PCB biSensor Test ({0})".format(attempt)

                        # Todo: Finalize packet structure include sending and returning format
                        reply = self.tcp_send(_test_name=str_temp, _message=self.prompt_message(valve_num),
                                                _step_num=6, _packet_type=bu_imports.PacketTypes.PROMPT_UI,
                                                _type=bu_imports.PromptTypes.OK_CANCEL)

                        # Error message if user clicks 'cancel' in UI message box
                        if reply == bu_imports.ReplyMessages.CANCEL:
                            self.step6_message = "Step6B Error: biSensor Test FAILED\n" \
                                                 "Cancelled by User"
                            return bu_imports.TCP.TEST_RESULTS.FAILED

                    # If test_valve() for this specific valve passes, break from this loop
                    if self.test_valve(_valve_num=valve_num, _verbose=_verbose):
                        break

                    # If loop has run at least twice, test fails
                    if attempt > 1:
                        return bu_imports.TCP.TEST_RESULTS.FAILED

                    # Increment attempt variable if loop has only run once
                    attempt += 1

                # Increment valve_num to test next valve
                valve_num += 1

        # Remote_dev is a 1, 2, or 4 valve decoder
        else:
            # Loops through number of valves for the indicated decoder type
            for valve_num in range(0, self.expected_type_min()):

                # If test_valve() for this specific valve fails, step6 fails
                if not self.test_valve(_valve_num=valve_num + 1, _verbose=_verbose):
                    return bu_imports.TCP.TEST_RESULTS.FAILED

        return bu_imports.TCP.TEST_RESULTS.PASSED

    # ########################################################
    def step7(self, _verbose=False):
        """
        Step 7:
        - Blink each LED
        - Ask user if LEDs blink correctly
        - IF user clicks 'NO'               -> error

        :param _verbose: Boolean for printing out additional information received from controller
        :type _verbose: bool
        """

        if self.test_mode:
            self.test_bu.r_reset_remote_device(_remote_device=self.test_zone)
            time.sleep(.25)

        if self.test_bu.ui_company_name != bu_imports.CompanyNames.WEATHERMATIC and \
                self.test_bu.ui_decoder_category != bu_imports.DeviceTypes.TWELVE_VALVE_STR and \
                self.test_bu.ui_decoder_category != bu_imports.DeviceTypes.TWENTY_FOUR_VALVE_STR:

            # message to be sent to UI
            message = "Do all LEDs blink correctly?"

            # Create the input and output ends of the pipe used for different processes to communicate
            # output_p, input_p = Pipe()

            # Create a queue used for different processes to communicate
            the_queue = Queue(1)

            # Create and start process to run blink_leds() to the side of the tcp_listener()/main process
            #  Pipe obj = (output_p, input_p)
            process_blink = Process(target=blink_leds, args=(the_queue,
                                                             self.expected_type_min(),
                                                             self.test_bu.ser.c_port,
                                                             self.test_bu.ad,
                                                             self.test_zone.ad,
                                                             self.test_zone.final_sn[0],))

            # A new instance of the BU must be made in the new process in order to maintain thread safety so we must
            #    close the serial connection here of self.test_bu so we can open one for the temporary instance of a
            #    BU in the other process
            self.test_bu.ser.close_connection()

            # Process is now running
            process_blink.start()

            time.sleep(10)

            # Send yes/no message box to UI and listen for reply while blink_leds() is running in the other process
            result = self.tcp_send(_message=message, _step_num=7, _queue=the_queue,
                                     _packet_type=bu_imports.PacketTypes.PROMPT_UI,
                                     _type=bu_imports.PromptTypes.YES_NO)

            # Close queue
            the_queue.close()

            # Join the process back with the main process after the method has completed
            process_blink.join()

            self.test_bu.ser.open_connection()

            self.test_bu.r_turn_all_valves_off(_remote_device=self.test_zone)

            if result == bu_imports.ReplyMessages.YES:
                self.step7_message = "{0}\n" \
                                     "Step7 Success: Blink LEDs - {1}\n" \
                                     "Serial Number = {2}\n" \
                                     "{3}"\
                    .format(self.test_zone.final_sn[0],                     # 0
                            bu_helpers.get_status_text(self.test_bu.ss),    # 1
                            self.test_zone.r_temp_sn,                       # 2
                            self.test_bu.data_string)                       # 3
                return bu_imports.TCP.TEST_RESULTS.PASSED

            else:
                self.step7_message = "{0}\n" \
                                     "Step7 Failed: Blink LEDs - {1}\n" \
                                     "Serial Number = {2}\n" \
                                     "{3}"\
                    .format(self.test_zone.final_sn[0],                     # 0
                            bu_helpers.get_status_text(self.test_bu.ss),    # 1
                            self.test_zone.r_temp_sn,                       # 2
                            self.test_bu.data_string)                       # 3
                return bu_imports.TCP.TEST_RESULTS.FAILED

        # Weathermatic controller
        else:

            self.step7_message = "{0}\n" \
                                 "Step7 Skipped: Blink LEDs - {1}\n" \
                                 "Serial Number = {2}\n" \
                                 "{3}"\
                .format(self.test_zone.final_sn[0],                     # 0
                        bu_helpers.get_status_text(self.test_bu.ss),    # 1
                        self.test_zone.r_temp_sn,                       # 2
                        self.test_bu.data_string)                       # 3
            return bu_imports.TCP.TEST_RESULTS.PASSED

    # ########################################################
    def step8(self, _verbose=False):
        """
        Step 8:
        - Write Null Address(s)
        - Null all addresses written in Step 7

        :param _verbose: Boolean for printing out additional information received from controller
        :type _verbose: bool
        """

        if not self.test_mode:

            # Check if remote_dev is either a 12 or 24 valve
            if self.test_bu.ui_decoder_category is bu_imports.DeviceTypes.TWELVE_VALVE_STR or \
                    self.test_bu.ui_decoder_category is bu_imports.DeviceTypes.TWENTY_FOUR_VALVE_STR:

                # Begins loop that iterates from 1 to 12
                for valve_num in range(1, 12):

                    # Calls write_null_address() on the current valve_num (first bank of valves and
                    # fails if returns false
                    if not self.write_null_address(_valve_number=valve_num,
                                                   _bank_number=0,
                                                   _verbose=_verbose):
                        return bu_imports.TCP.TEST_RESULTS.FAILED

                    # if the remote_dev is a 24 valve
                    if self.test_bu.ui_decoder_category is bu_imports.DeviceTypes.TWENTY_FOUR_VALVE_STR:

                        # Calls write_null_address() on the current valve_num plus 12 (second bank of valves)
                        # and fails if returns false
                        if not self.write_null_address(_valve_number=valve_num + 12,
                                                       _bank_number=1,
                                                       _verbose=_verbose):
                            return bu_imports.TCP.TEST_RESULTS.FAILED

                        # Calls write_null_address() on last two valves (third bank of valves) and fails if returns
                        # false
                        if valve_num in [1, 2]:

                            if not self.write_null_address(_valve_number=valve_num + 24,
                                                           _bank_number=2,
                                                           _verbose=_verbose):
                                return bu_imports.TCP.TEST_RESULTS.FAILED

            # If remote_dev is a 1, 2, or 4 valve
            else:

                # Calls write_null_address on all valves individually and fails if returns false
                for valve_num in range(0, self.expected_type_min()):
                    if not self.write_null_address(_valve_number=valve_num + 1,
                                                   _bank_number=0,
                                                   _verbose=_verbose):
                        return bu_imports.TCP.TEST_RESULTS.FAILED

        # test_mode is true and test is skipped
        else:
            self.step8_message = "{0}\n" \
                                 "Step8 SKIPPED: WriteTempAddress FAIL \n" \
                                 "Serial Number = {1}\n"\
                .format(self.test_zone.final_sn[0],     # 0
                        self.test_zone.r_temp_sn)       # 1
            return bu_imports.TCP.TEST_RESULTS.PASSED

        # Success message if all tests pass
        self.step8_message = "{0}\n" \
                             "Step8 Success: WriteTempAddress - {1}\n" \
                             "Serial Number = {2}\n" \
                             "{3}"\
            .format(self.test_zone.final_sn[0],                     # 0
                    bu_helpers.get_status_text(self.test_bu.ss),    # 1
                    self.test_zone.r_temp_sn,                       # 2
                    self.test_bu.data_string)                       # 3

        return bu_imports.TCP.TEST_RESULTS.PASSED

    # ########################################################
    def step9(self, _verbose=False):
        """
        Step 9:
        - Verify new SN with user then write and verify SN to Decoder
        - IF SerialNumber == Default        -> NextSerialNumber
        - Verify SN with user or Exit
        - IF WriteSerialNumber != OK        -> error

        :param _verbose: Boolean for printing out additional information received from controller
        :type _verbose: bool
        """
        # declaring temporary variable that will be used in the method
        sn_from_file = ""

        self.test_zone.r_temp_sn = self.test_zone.final_sn[0]

        if not self.test_mode:

            # If the number stored in final_sn[0] is a default serial number retrieves a new serial number from json
            if self.test_zone.final_sn[0] == bu_helpers.get_default_sn(_type=self.test_bu.ui_decoder_category):

                sn_from_file = bu_helpers.get_next_sn(_type=self.test_bu.ui_decoder_category,
                                                      _company=self.test_bu.ui_company_name)
                self.test_zone.final_sn[0] = sn_from_file

            # outer loop
            while True:

                # inner loop - asking for new serial number, breaks out when user clicks yes
                while True:

                    message = "Is this the correct Serial Number: {0}?".format(self.test_zone.final_sn[0])

                    # Sends yes/no message box to UI
                    result = self.tcp_send(_message=message, _step_num=9, _test_name="Decoder Serial Number",
                                             _packet_type=bu_imports.PacketTypes.PROMPT_UI,
                                             _type=bu_imports.PromptTypes.YES_NO)

                    # If user clicks 'no', a new input box is sent to UI for user to enter another serial number,
                    # final_sn[0] is set to the new number and the inner loop tries the process again
                    if result == bu_imports.ReplyMessages.NO:

                        input_message = "Enter the correct Serial Number"
                        new_sn = self.tcp_send(_message=input_message, _step_num=9, _test_name="Decoder Serial #",
                                                 _packet_type=bu_imports.PacketTypes.PROMPT_UI,
                                                 _type=bu_imports.PromptTypes.INPUT)
                        self.test_zone.final_sn[0] = new_sn

                        # if user doesn't input anything, final_sn[0] is set to its initial value and inner loop tries
                        # again
                        if self.test_zone.final_sn[0] is None:
                            self.test_zone.final_sn[0] = self.test_zone.r_temp_sn

                    # If user clicks 'yes', breaks from inner loop
                    elif result == bu_imports.ReplyMessages.YES:
                        break

                    # If user clicks anything else, Test9 fails
                    else:
                        return bu_imports.TCP.TEST_RESULTS.FAILED

                # Todo: Structure and/or process for saving Serial Numbers, decide to use duplicate or force unique
                # Checks if serial number chosen has already been used
                if bu_helpers.is_sn_used(_sn=self.test_zone.final_sn[0]):
                    message = "{0} has been used before.  Use Again?".format(self.test_zone.final_sn[0])

                    # Sends yes/no message to UI to see if user wants to use serial number again
                    result = self.tcp_send(_message=message, _step_num=9, _test_name="Serial Number Reuse",
                                             _packet_type=bu_imports.PacketTypes.PROMPT_UI,
                                             _type=bu_imports.PromptTypes.YES_NO)

                    # If user clicks 'no', a new input message is sent to UI to input a new serial number.
                    # final_sn[0] is set to the new value and the outer loop runs again
                    if result == bu_imports.ReplyMessages.NO:
                        input_message = "Enter the correct Serial Number"
                        new_sn = self.tcp_send(_message=input_message, _step_num=9,
                                                 _test_name="Decoder Serial Number",
                                                 _packet_type=bu_imports.PacketTypes.PROMPT_UI,
                                                 _type=bu_imports.PromptTypes.INPUT)

                        if new_sn.upper() != bu_imports.ReplyMessages.CANCEL:
                            self.test_zone.final_sn[0] = new_sn
                        else:
                            return bu_imports.TCP.TEST_RESULTS.FAILED

                    # If user clicks 'yes', break from outer loop
                    elif result == bu_imports.ReplyMessages.YES:
                        break

                    # Fails step9 if user clicks anything else
                    else:
                        return bu_imports.TCP.TEST_RESULTS.FAILED

                # If serial number chosen has not been used, break from outer loop
                else:
                    break

            # If remote_dev is a 12 or 24 valve
            if self.test_bu.ui_decoder_category is bu_imports.DeviceTypes.TWELVE_VALVE_STR or \
                    self.test_bu.ui_decoder_category is bu_imports.DeviceTypes.TWENTY_FOUR_VALVE_STR:

                # Sets the serial number for the 2nd and 3rd bank of valves
                self.test_zone.final_sn[1] = "VB{0}".format(self.test_zone.final_sn[0][2:])
                self.test_zone.final_sn[2] = "VE{0}".format(self.test_zone.final_sn[0][2:])

                # Error message if write_remote_sn_12to24_valve() returns false
                if not self.write_remote_sn_12to24_valve():
                    self.step9_message = "{0}\nStep9 Error: WriteVerifyRemoteSN - {1}\n{2}"\
                        .format(self.test_zone.final_sn[0],                                             # 0
                                bu_helpers.get_status_text(self.test_bu.ss),                            # 1
                                bu_helpers.get_help_text(bu_imports.ErrorHelpText.REMOTE_WRITE_SN))     # 2
                    if _verbose:
                        self.step9_message += "\n{0}".format(self.test_bu.data_string)
                    return bu_imports.TCP.TEST_RESULTS.FAILED

            # If remote_dev is 1, 2, or 4 valve
            else:
                # Error message if write_verify_remote_sn_1to4() returns false
                if not self.write_verify_remote_sn_1to4():
                    self.step9_message = "{0}\nStep9 Error: WriteVerifyRemoteSN - {1}\n{2}"\
                        .format(self.test_zone.final_sn[0],                                             # 0
                                bu_helpers.get_status_text(self.test_bu.ss),                            # 1
                                bu_helpers.get_help_text(bu_imports.ErrorHelpText.REMOTE_WRITE_SN))     # 2
                    if _verbose:
                        self.step9_message += "\n{0}".format(self.test_bu.data_string)
                    return bu_imports.TCP.TEST_RESULTS.FAILED

            # If final_sn[0] matches the temporary sn from earlier in the test, write that sn to json
            if self.test_zone.final_sn[0] is sn_from_file:
                bu_helpers.put_sn_to_file(_sn=sn_from_file,
                                          _type=self.test_bu.ui_decoder_category,
                                          _company=self.test_bu.ui_company_name)

            # Success message if all tests pass
            self.step9_message = "{0}\nStep9 Success: WriteVerifyRemoteSN OK - {1}\n{2}"\
                .format(self.test_zone.final_sn[0],                     # 0
                        bu_helpers.get_status_text(self.test_bu.ss),    # 1
                        self.test_bu.data_string)                       # 2

        # If test_mode is false, skip step9
        else:
            self.step9_message = "{0}\nStep9 SKIPPED: Write Serial Number\n{1}"\
                .format(self.test_zone.final_sn[0],     # 0
                        self.test_bu.data_string)       # 1
            return bu_imports.TCP.TEST_RESULTS.PASSED

        return bu_imports.TCP.TEST_RESULTS.PASSED

    # #########################################################################################
    # ##################### Helper Methods ####################################################
    # #########################################################################################

    # ########################################################
    def base_current_max(self):
        """
        Step2 Helper
        Determines if test_bu's remote dev's type matches expected type based on decoder category

        :return: bool
        """

        res = self.test_limits["Base"]["CurrentMax"] * bu_imports.CurrentVoltageScalers.BASE_UNIT_CURRENT_SCALE + \
              bu_imports.CurrentVoltageScalers.BASE_UNIT_CURRENT_OFFSET
        return res

    # ########################################################
    def base_voltage_min(self):
        """
        Step2 Helper
        Determines if test_bu's remote dev's type matches expected type based on decoder category

        :return: bool
        """

        res = (self.test_limits["Base"]["VoltageMin"] - bu_imports.CurrentVoltageScalers.BASE_UNIT_VOLTAGE_OFFSET) * \
              bu_imports.CurrentVoltageScalers.BASE_UNIT_VOLTAGE_SCALE
        return res

    # ########################################################
    def type_ok(self):
        """
        Step2 Helper
        Determines if test_bu's remote dev's type matches expected type based on decoder category

        :return: bool
        """
        return self.test_zone.ty == self.expected_type_text()

    # ########################################################
    def expected_type_min(self):
        """
        Step2 Helper
        Returns the expected type_min based on decoder category from UI

        :rtype: int
        """
        if self.test_bu.ui_decoder_category is bu_imports.DeviceTypes.SINGLE_VALVE_STR:
            return 1
        elif self.test_bu.ui_decoder_category is bu_imports.DeviceTypes.DUAL_VALVE_STR:
            return 2
        elif self.test_bu.ui_decoder_category is bu_imports.DeviceTypes.QUAD_VALVE_STR:
            return 4
        elif self.test_bu.ui_decoder_category is bu_imports.DeviceTypes.TWELVE_VALVE_STR or \
                self.test_bu.ui_decoder_category is bu_imports.DeviceTypes.TWENTY_FOUR_VALVE_STR:
            return 12

    # ########################################################
    def expected_type_maj(self):
        """
        Step2 Helper
        Returns the expected type_max based on decoder category from UI

        :rtype: int
        """
        if self.test_bu.ui_decoder_category is bu_imports.DeviceTypes.TWELVE_VALVE_STR or \
           self.test_bu.ui_decoder_category is bu_imports.DeviceTypes.TWENTY_FOUR_VALVE_STR:
            return 12
        else:
            return 2

    # ########################################################
    def expected_type_text(self):
        """
        Step2 Helper
        Returns the expected type in str format

        :rtype: str
        """

        return "{0}.{1}".format(self.expected_type_maj(), self.expected_type_min())

    # ########################################################
    def ver_ok(self):
        """
        Step2 Helper
        Determines if test_bu's remote dev's version matches expected version based on decoder category (type maj/min)

        :rtype bool
        """

        return self.test_zone.vr == self.expected_ver_text()

    # ########################################################
    def expected_ver_text(self):
        """
        Step2 Helper
        Returns the expected version based on decoder category (type maj/min) is str format

        :return: str
        """

        min_ver = self.test_limits["Version"]["Min"]
        maj_ver = self.test_limits["Version"]["Maj"]

        return "{0}.{1}".format(maj_ver, min_ver)

    # ########################################################
    def write_addr_chk_sn(self, _zone_num, _bank12R, _verbose):
        """
        Step3 Helper
        Writes the address of the valve specified in arguments.
        Calls read_serial_number to remote device and saves the returned sn in a local variable rather than
            the self.test_bu object variable and compares the local_sn to

        :param _zone_num: The valve to which the address will be written
        :type _zone_num: int

        :param _bank12R: The specific bank of the zone being passed in
        :type _bank12R: int

        :param _verbose: Boolean for printing out additional information received from controller
        :type _verbose: bool

        :rtype: bool
        """
        sn_int = bu_helpers.convert_sn_string_to_int(self.test_zone.final_sn[_bank12R])

        time.sleep(.05)

        # Sets the temp_sn for remote_dev to the valve bank's ser num + the specific valve
        temp_sn_new_suffix = _zone_num - 12 * _bank12R - 1
        temp_sn_old_suffix = self.test_zone.final_sn[_bank12R][len(self.test_zone.r_temp_sn) - 3:]
        temp_sn_old_suffix = int(temp_sn_old_suffix)
        temp_sn_new_suffix += temp_sn_old_suffix
        self.test_zone.r_temp_sn = "{0}{1}"\
            .format(self.test_zone.r_temp_sn[:len(self.test_zone.r_temp_sn) - 3], temp_sn_new_suffix)

        # Command sent to BU
        self.test_bu.r_unlock_serial_number(_remote_device=self.test_zone, _temp_sn=self.test_zone.r_temp_sn)

        # Error message if error returned from BU
        if self.test_bu.ss != bu_imports.BaseErrors.STATUS_OK:
            self.step3_message = "{0}\nStep3 Error: UnlockSerialNumber - {1}\n{2}"\
                .format(self.test_zone.final_sn[_bank12R],                                                      # 0
                        bu_helpers.get_status_text(self.test_bu.ss),                                            # 1
                        bu_helpers.get_help_text(bu_imports.ErrorHelpText.REMOTE_UNLOCK_SN, self.test_bu.ss))   # 2
            if _verbose:
                self.step3_message += "\n{0}".format(self.test_bu.data_string)
            return False

        # time.sleep(.05)

        # Command sent to BU
        self.test_bu.r_write_address(_remote_device=self.test_zone, _new_address=_zone_num)

        # Error message if error returned from BU
        if self.test_bu.ss != bu_imports.BaseErrors.STATUS_OK:
            self.step3_message = "{0}\n" \
                                 "Step3 Error: WriteAddress - {1}\n" \
                                 "Serial Number = {2}, Address = {3}\n" \
                                 "{4}"\
                .format(self.test_zone.final_sn[_bank12R],                                                          # 0
                        bu_helpers.get_status_text(self.test_bu.ss),                                                # 1
                        self.test_zone.r_temp_sn,                                                                   # 2
                        _zone_num,                                                                                  # 3
                        bu_helpers.get_help_text(bu_imports.ErrorHelpText.REMOTE_WRITE_ADDRESS, self.test_bu.ss))   # 4
            if _verbose:
                self.step3_message += "\n{0}".format(self.test_bu.data_string)
            return False

        time.sleep(.05)

        # Command sent to BU
        local_sn = self.test_bu.r_read_serial_number(_remote_device=self.test_zone, _test_sn=True)

        # Error message if error returned from BU
        if self.test_bu.ss != bu_imports.BaseErrors.STATUS_OK:
            self.step3_message = "{0}\n" \
                                 "Step3 Error: ReadSerialNumber - {1}\n" \
                                 "Address = {2}\n" \
                                 "{3}"\
                .format(self.test_zone.final_sn[_bank12R],                                                      # 0
                        bu_helpers.get_status_text(self.test_bu.ss),                                            # 1
                        _zone_num,                                                                              # 2
                        bu_helpers.get_help_text(bu_imports.ErrorHelpText.REMOTE_UNLOCK_SN, self.test_bu.ss))   # 3
            if _verbose:
                self.step3_message += "\n{0}".format(self.test_bu.data_string)
            return False

        # Error message if the test_zone.r_temp_sn value does not match the serial number returned from remote dev
        if bu_helpers.convert_sn_string_to_int(local_sn) != \
                bu_helpers.convert_sn_string_to_int(self.test_zone.r_temp_sn):
            self.step3_message = "{0}\n" \
                                 "Step3 Error: VerifyAddressSerialNumber - Serial Number Wrong\n" \
                                 "Returned = {1}\n" \
                                 "Expected = {2}\n" \
                                 "{3}"\
                .format(self.test_zone.final_sn[_bank12R],                                                      # 0
                        local_sn,                                                                               # 1
                        self.test_zone.r_temp_sn,                                                               # 2
                        bu_helpers.get_help_text(bu_imports.ErrorHelpText.REMOTE_UNLOCK_SN, self.test_bu.ss))   # 3
            if _verbose:
                self.step3_message += "\n{0}".format(self.test_bu.data_string)
            return False
        return True

    # ########################################################
    def calculate_two_wire_drop(self, _temp=0):
        """
        Calculate two wire drop value from raw data.

        :param _temp: A Temporary raw value to calculate

        :return:
        """
        if _temp == 0:
            _temp = self.test_zone.vt

        res = (_temp - bu_imports.CurrentVoltageScalers.BASE_UNIT_VOLTAGE_OFFSET) *   bu_imports.CurrentVoltageScalers.BASE_UNIT_VOLTAGE_SCALE

        return res

    # ########################################################
    def prompt_message(self, _valve_num):
        """
        Step6 Helper
        Generates correct message for prompt in UI depending on valve_num passed in

        :param _valve_num: The specific valve_num to base the message on
        :type _valve_num: int

        :return:
        """

        if _valve_num in [1, 2]:
            return "Touch test fixture to post A{0}.\n\n" \
                   "Press biSensor Update button on PCB.\n\n" \
                   "Click OK if LED remains ON.".format(_valve_num)
        elif _valve_num in range(3, 12):
            return "Touch test fixture to post A{0}.\n\n" \
                   "Click OK to continue test.".format(_valve_num)
        elif _valve_num in [13, 14]:
            return "Touch test fixture to post B{0}.\n\n" \
                   "Press biSensor Update button on PCB.\n\n" \
                   "Click OK if LED remains ON.".format(_valve_num - 12)
        elif _valve_num in range(15, 24):
            return "Touch test fixture to post B{0}.\n\n" \
                   "Click OK to continue test.".format(_valve_num - 12)
        else:
            return "Touch test fixture to post E{0}.\n\n" \
                   "Click OK to continue test.".format(_valve_num - 24)

    # ########################################################
    def test_valve(self, _valve_num, _verbose):
        """
        Step6 Helper
        Turns valve number that is passed as argument on and off to test the valve

        :param _valve_num: The specific valve_num to base the message on
        :type _valve_num: int

        :param _verbose: Boolean for printing out additional information received from controller
        :type _verbose: bool

        :rtype: bool
        """

        # Command sent to BU to turn on specific valve
        self.test_bu.r_turn_valve_on(_remote_device=self.test_zone, _valve_number=_valve_num)

        # Error message if error returned from BU
        if self.test_bu.ss != bu_imports.BaseErrors.STATUS_OK:
            self.step6_message = "{0}\nStep6 Error: TurnValveOn {1}\n{2}\n{3}\n{4}"\
                .format(self.test_zone.final_sn[0],                                                 # 0
                        self.port_wire_color(_valve_num),                                           # 1
                        bu_helpers.get_status_text(self.test_bu.ss),                                # 2
                        bu_helpers.get_help_text(bu_imports.ErrorHelpText.REMOTE_TURN_ON_VALVE),    # 3
                        self.test_bu.ss)                                                            # 4
            time.sleep(.1)

            # Turns all valves off again before returning from method
            self.test_bu.r_turn_all_valves_off(self.test_zone)
            return False

        time.sleep(1)

        # Reads power from the valve that was turned on
        self.test_bu.r_read_remote_power(_remote_device=self.test_zone, _valve_number=_valve_num)

        # Error message if error returned from BU
        if self.test_bu.ss != bu_imports.BaseErrors.STATUS_OK:
            self.step6_message = "{0}\nStep6 Error: ReadRemotePower {0}{1}\n{2}"\
                .format(self.test_zone.final_sn[0],                                                 # 0
                        self.port_wire_color(_valve_num),                                           # 1
                        bu_helpers.get_status_text(self.test_bu.ss),                                # 2
                        bu_helpers.get_help_text(bu_imports.ErrorHelpText.REMOTE_TURN_ON_VALVE))    # 3
            if _verbose:
                self.step6_message += "\n{0}".format(self.test_bu.data_string)
            time.sleep(.1)

            # Turns all valves off again before returning from method
            self.test_bu.r_turn_all_valves_off(_remote_device=self.test_zone)
            return False

        # Error message if current reading from valve is not within limits
        if abs(self.test_zone.r_va) < self.test_limits["Current"]["Max"] or \
                abs(self.test_zone.r_va) > self.test_limits["Current"]["Min"]:
            self.step6_message = "{0}\n" \
                                 "Step6 Error: ReadRemotePower {1}Current Out of Range\n" \
                                 "Returned = {2}\n" \
                                 "Expected = {3} to {4}\n" \
                                 "{5}"\
                .format(self.test_zone.final_sn[0],                                                 # 0
                        self.port_wire_color(_valve_num),                                           # 1
                        self.test_zone.r_va,                                                        # 2
                        self.test_limits["Current"]["Max"],                                         # 3
                        self.test_limits["Current"]["Min"],                                         # 4
                        bu_helpers.get_help_text(bu_imports.ErrorHelpText.REMOTE_TURN_ON_VALVE))    # 5
            if _verbose:
                self.step6_message += "\n{0}".format(self.test_bu.data_string)
            time.sleep(.1)

            # Turns all valves off again before returning from method
            self.test_bu.r_turn_all_valves_off(_remote_device=self.test_zone)
            return False

        # Success message if valve passes all tests
        self.step6_message = "{0}\n" \
                             "Step6 Success: ReadRemotePower - Valve Current/Voltage OK\n" \
                             "Returned = {1}\n" \
                             "Expected = {2} to {3}\n" \
                             "Returned = {4}\n" \
                             "Expected = {5} to {6}\n" \
                             "{7}"\
            .format(self.test_zone.final_sn[0],             # 0
                    self.test_zone.r_va,                    # 1
                    self.test_limits["Current"]["Max"],     # 2
                    self.test_limits["Current"]["Min"],     # 3
                    self.test_zone.r_vv,                    # 4
                    self.test_limits["Voltage"]["Max"],     # 5
                    self.test_limits["Voltage"]["Min"],     # 6
                    self.test_bu.data_string)               # 7

        # Error message if voltage reading from valve is not within limits
        if self.test_zone.r_vv < self.test_limits["Voltage"]["Min"] or \
                self.test_zone.r_vv > self.test_limits["Voltage"]["Max"]:
            self.step6_message = "{0}\n" \
                                 "Step6 Error: ReadRemotePower {1}Voltage Out of Range\n" \
                                 "Returned = {2}\n" \
                                 "Expected = {3} to {4}\n" \
                                 "{5}"\
                .format(self.test_zone.final_sn[0],                                                 # 0
                        self.port_wire_color(_valve_num),                                           # 1
                        self.test_zone.r_vv,                                                        # 2
                        self.test_limits["Voltage"]["Max"],                                         # 3
                        self.test_limits["Voltage"]["Min"],                                         # 4
                        bu_helpers.get_help_text(bu_imports.ErrorHelpText.REMOTE_TURN_ON_VALVE))    # 5
            if _verbose:
                self.step6_message += "\n{0}".format(self.test_bu.data_string)
            time.sleep(.1)

            # Turns all valves off again before returning from method
            self.test_bu.r_turn_all_valves_off(_remote_device=self.test_zone)
            return False

        # Command sent to BU to turn specific valve off
        self.test_bu.r_turn_valve_off(_remote_device=self.test_zone, _valve_number=_valve_num)

        # Error message if error returned from BU
        if self.test_bu.ss != bu_imports.BaseErrors.STATUS_OK:
            self.step6_message = "{0}\nStep6 Error: TurnValveOff {1}{2}\n{3}"\
                .format(self.test_zone.final_sn[0],                                                 # 0
                        self.port_wire_color(_valve_num),                                           # 1
                        bu_helpers.get_status_text(self.test_bu.ss),                                # 2
                        bu_helpers.get_help_text(bu_imports.ErrorHelpText.REMOTE_TURN_ON_VALVE))    # 3
            if _verbose:
                self.step6_message += "\n{0}".format(self.test_bu.data_string)
            time.sleep(.1)

            # Turns all valves off again before returning from method
            self.test_bu.r_turn_all_valves_off(_remote_device=self.test_zone)
            return False

        time.sleep(.1)

        # Turns all valves off again before returning from method
        self.test_bu.r_turn_all_valves_off(_remote_device=self.test_zone)
        return True

    # ########################################################
    def port_wire_color(self, _valve_num):
        """
        Step6 Helper
        Turns valve number that is passed as argument on and off to test the valve

        :param _valve_num: The specific valve_num to base the message on
        :type _valve_num: int

        :rtype: str
        """

        if self.test_bu.ui_decoder_category is bu_imports.DeviceTypes.TWELVE_VALVE_STR:
            if _valve_num < 13:
                return "Post A{0} - ".format(_valve_num)
            else:
                return "Post E{0} - ".format(_valve_num - 12)

        elif self.test_bu.ui_decoder_category is bu_imports.DeviceTypes.TWENTY_FOUR_VALVE_STR:
            if _valve_num < 13:
                return "Post A{0} - ".format(_valve_num)
            elif _valve_num < 25:
                return "Post B{0} - ".format(_valve_num - 12)
            else:
                return "Post E{0} - ".format(_valve_num - 24)

        else:
            if _valve_num is 1:
                return "(Orange Wire) - "
            elif _valve_num is 2:
                return "(Yellow Wire) - "
            elif _valve_num is 3:
                return "(Green Wire) - "
            elif _valve_num is 4:
                return "(Blue Wire) - "
            else:
                return "(Post {0}) - "

    # ########################################################
    def write_null_address(self, _valve_number, _bank_number, _verbose=False):
        """
        * Step 8 helper method *

        :param _valve_number: Valve number to write to (1 - 26 available)
        :type _valve_number: int

        :param _bank_number: Decoder bank for 12 & 24-valve decoders (0: 1-12 valves, 1: 13-24, 2: 24-26)
        :type _bank_number: int

        :param _verbose: Flag - True to add additional packet info to error message
        :type _verbose: bool
        """
        # Get initial serial number as integer
        sn_as_int = bu_helpers.convert_sn_string_to_int(self.test_zone.final_sn[_bank_number])

        # calc valve offset
        # valve_offset = sn_as_int + (_valve_number - (12 * _bank_number) - 1)

        # Sets the temp_sn for remote_dev to the valve bank's ser num + the specific valve (valve offset)
        temp_sn_new_suffix = _valve_number - 12 * _bank_number - 1
        temp_sn_old_suffix = self.test_zone.final_sn[_bank_number][len(self.test_zone.r_temp_sn) - 3:]
        temp_sn_old_suffix = int(temp_sn_old_suffix)
        temp_sn_new_suffix += temp_sn_old_suffix
        self.test_zone.r_temp_sn = "{0}{1}"\
            .format(self.test_zone.r_temp_sn[:len(self.test_zone.r_temp_sn) - 3], temp_sn_new_suffix)

        # Add valve offset to serial number to allow us to address correct valve
        # self.test_zone.r_temp_sn = sn_as_int + valve_offset

        # Command sent to BU
        time.sleep(0.5)
        self.test_bu.r_unlock_serial_number(_remote_device=self.test_zone, _temp_sn=self.test_zone.r_temp_sn)

        # Error message if error returned from BU
        if self.test_bu.ss != bu_imports.BaseErrors.STATUS_OK:
            self.step8_message = "{0}\nStep8 Error: UnlockSerialNumber - {1}\n{2}" \
                .format(self.test_zone.final_sn[_bank_number],                                                    # 0
                        bu_helpers.get_status_text(self.test_bu.ss),                                              # 1
                        bu_helpers.get_help_text(bu_imports.ErrorHelpText.REMOTE_UNLOCK_SN, self.test_bu.ss))     # 2
            if _verbose:
                self.step8_message += "\n{0}".format(self.test_bu.data_string)
            return False

        # Command sent to BU
        time.sleep(0.5)
        self.test_bu.r_write_temp_address(_remote_device=self.test_zone)

        # Error message if error returned from BU
        if self.test_bu.ss != bu_imports.BaseErrors.STATUS_OK:
            self.step8_message = "{0}\nStep8 Error: WriteTempAddress - {1} - Serial Number = {2}\n{3}" \
                .format(self.test_zone.final_sn[_bank_number],                                                      # 0
                        bu_helpers.get_status_text(self.test_bu.ss),                                                # 1
                        self.test_zone.r_temp_sn,                                                                   # 2
                        bu_helpers.get_help_text(bu_imports.ErrorHelpText.REMOTE_WRITE_ADDRESS, self.test_bu.ss))   # 3
            if _verbose:
                self.step8_message += "\n{0}".format(self.test_bu.data_string)
            return False

        return True

    #########################################################
    def write_verify_remote_sn_1to4(self):
        """
        Step9 Helper
        """
        self.test_bu.r_unlock_remote_global(_remote_device=self.test_zone)
        if self.test_bu.ss != bu_imports.BaseErrors.STATUS_OK or self.test_zone.ss != bu_imports.BaseErrors.STATUS_OK:
            return False

        self.test_bu.r_write_serial_number(_remote_device=self.test_zone,_temp_sn=self.test_zone.r_temp_sn)
        if self.test_bu.ss != bu_imports.BaseErrors.STATUS_OK or self.test_zone.ss != bu_imports.BaseErrors.STATUS_OK:
            return False

        self.test_bu.r_reset_remote_device(_remote_device=self.test_zone)
        time.sleep(10)

        self.test_bu.r_read_serial_number(_remote_device=self.test_zone)
        if self.test_bu.ss != bu_imports.BaseErrors.STATUS_OK or self.test_zone.ss != bu_imports.BaseErrors.STATUS_OK:
            return False

        if self.test_zone.r_temp_sn != self.test_zone.final_sn[0]:
            return False

        return True

    #########################################################
    def write_remote_sn_12to24_valve(self):
        """
        Step9 Helper

        :return:
        :rtype: bool
        """
        temp_sn = ""

        # unlock using remote temp sn
        self.test_bu.r_unlock_serial_number(_remote_device=self.test_zone, _temp_sn=self.test_zone.r_temp_sn)

        # errors?
        if self.test_bu.ss != bu_imports.BaseErrors.STATUS_OK or self.test_zone.ss != bu_imports.BaseErrors.STATUS_OK:
            return False

        # unlock using first bank final sn
        self.test_bu.r_write_serial_number(_remote_device=self.test_zone, _temp_sn=self.test_zone.final_sn[0])

        # errors?
        if self.test_bu.ss != bu_imports.BaseErrors.STATUS_OK or self.test_zone.ss != bu_imports.BaseErrors.STATUS_OK:
            return False

        # 24-valve decoder being tested?
        if self.test_zone.r_decoder_type == bu_imports.DeviceTypes.TWENTY_FOUR_VALVE_STR:

            # convert temp sn to 24-valve sn equivalent
            self.test_bu.r_unlock_serial_number(_remote_device=self.test_zone,
                                                _temp_sn="VB" + self.test_zone.r_temp_sn[2:])

            # errors?
            if self.test_bu.ss != bu_imports.BaseErrors.STATUS_OK or \
                    self.test_zone.ss != bu_imports.BaseErrors.STATUS_OK:
                return False

            # unlock using second bank final sn
            self.test_bu.r_write_serial_number(_remote_device=self.test_zone, _temp_sn=self.test_zone.final_sn[1])

            # errors?
            if self.test_bu.ss != bu_imports.BaseErrors.STATUS_OK or \
                    self.test_zone.ss != bu_imports.BaseErrors.STATUS_OK:
                return False

        # unlock using remote temp sn
        self.test_bu.r_unlock_serial_number(_remote_device=self.test_zone, _temp_sn="VE" + self.test_zone.r_temp_sn[2:])

        # errors?
        if self.test_bu.ss != bu_imports.BaseErrors.STATUS_OK or self.test_zone.ss != bu_imports.BaseErrors.STATUS_OK:
            return False

        # unlock using first bank final sn
        self.test_bu.r_write_serial_number(_remote_device=self.test_zone, _temp_sn=self.test_zone.final_sn[2])

        # errors?
        if self.test_bu.ss != bu_imports.BaseErrors.STATUS_OK or self.test_zone.ss != bu_imports.BaseErrors.STATUS_OK:
            return False

        # Upon success, return true
        return True

    #########################################################
    def set_test_limits(self, ui_limits):
        """
        Sets the test limit values to that the values set by the user in the UI

        :param ui_limits:  Limits sent from the UI
        :type ui_limits: dict
        """
        self.test_limits["Current"]["Max"] = ui_limits["TEST_LIMITS"]["Current"]["Max"]
        self.test_limits["Current"]["Min"] = ui_limits["TEST_LIMITS"]["Current"]["Min"]
        self.test_limits["Voltage"]["Max"] = ui_limits["TEST_LIMITS"]["Voltage"]["Max"]
        self.test_limits["Voltage"]["Min"] = ui_limits["TEST_LIMITS"]["Voltage"]["Min"]
        self.test_limits["TwoWireDrop"]["Max"] = ui_limits["TEST_LIMITS"]["TwoWireDrop"]["Max"]
        self.test_limits["TwoWireDrop"]["Min"] = ui_limits["TEST_LIMITS"]["TwoWireDrop"]["Min"]
        self.test_limits["Version"]["Maj"] = ui_limits["TEST_LIMITS"]["Version"]["Maj"]
        self.test_limits["Version"]["Min"] = ui_limits["TEST_LIMITS"]["Version"]["Min"]



    # #########################################################################################
    # ##################### TCP Listener Methods ##############################################
    # #########################################################################################

    # ########################################################
    def tcp_send(self, _message, _step_num, _packet_type, _type=None, _results="2", _test_name=None, _queue=None):
        """
        A method that will send a packet through tcp to the UI then listen for a reply

        :param _message: The message to be displayed in the UI
        :type _message: str

        :param _step_num: The current step number
        :type _step_num: int

        :param _packet_type:  The type of packet being sent

        :param _type: The type of prompt the UI needs to create if _packet_type == PROMPT_UI
        :type _type: str

        :param _results: Results of test, defaults to "2" which means test is not complete yet
        :type _results: str

        :param _test_name: The name of the test being sent to UI or name to go in header of message box
        :type _test_name: str

        :param _queue: The Queue where the return value will be sent if _multiprocess is True
        :type _queue: Queue

        :return:  Returns whatever the reply is from the UI
        """

        # for i in range(1, 15):
        #     time.sleep(1)
        #     # print str(i * 10)

        if _packet_type == bu_imports.PacketTypes.PROMPT_UI:
            sendpacket = self.tcp_conn.build_tcp_prompt_packet(_step=_step_num, _msg=_message, _p_type=_type,
                                                               _test_name=_test_name)
            reply = self.tcp_conn.send_msg_and_listen_for_reply(sendpacket)
            reply_message = reply["MESSAGE"]

            if _queue is not None:
                _queue.put(0)

            return reply_message

        elif _packet_type == bu_imports.PacketTypes.STEP_OUTPUT:
            sendpacket = self.tcp_conn.build_step_results_packet(step=_step_num, msg=_message, result=_results)
            self.tcp_conn.send_msg(_msg=sendpacket)

        elif _packet_type == bu_imports.PacketTypes.TEST_RESULTS:
            sendpacket = self.tcp_conn.build_test_results_packet(msg=_message, result=_results)
            self.tcp_conn.send_msg(_msg=sendpacket)


# ########################################################
# ##########  Inner Function #############################
# ########################################################
def blink_leds(_queue, _max_valves, _comport, _base_ad, _remote_ad, _remote_sn):
    """
    Step7 Helper
    Runs a loop that turns each valve on for a set time and then turns it off

    :param _queue: The Queue object used to communicate with another process during MultiProcessing
    :type _queue: Queue

    :param _max_valves: Number of valves
    """

    this_ser = ser.Ser(comport=_comport)
    temp_bu = bu.BaseUnit(_ser=this_ser)
    temp_bu.ad = _base_ad
    temp_zone = zn.Zone(_serial=_remote_sn, _address=_remote_ad)

    this_ser.init_ser_connection(_baud_rate=9600)

    temp_bu.read_version()
    temp_bu.read_power()
    temp_bu.power_up_2_wire()
    temp_bu.r_read_serial_number(_remote_device=temp_zone)
    temp_bu.r_unlock_serial_number(_remote_device=temp_zone)
    temp_bu.r_read_remote_power(_remote_device=temp_zone)

    # set pointer to the input and output of the multiprocess pipe
    the_queue = _queue

    # keep_looping = 1
    valve_num = 1

    # while keep_looping == 1:
    while True:
        temp_bu.r_turn_valve_on(_remote_device=temp_zone, _valve_number=valve_num)
        # time.sleep(.5)
        temp_bu.r_turn_valve_off(_remote_device=temp_zone, _valve_number=valve_num)
        # time.sleep(.5)

        # time.sleep(.2)
        valve_num += 1

        if valve_num > _max_valves:
            valve_num = 1

        try:
            if the_queue.full():
                # keep_looping = the_queue.get()
                break
        except Exception as e:
            print "Blink_leds: " + e.message
            break

    temp_bu.r_turn_all_valves_off(_remote_device=temp_zone)

    this_ser.close_connection()

    return True

if __name__ == '__main__':
    import os
    # data = get_device_info()
    # print data
