import sys
import csv
import logging
from time import sleep
import datetime

from common.configuration import Configuration
from common import object_factory
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from csv_handler import CSVWriter

from common.imports import opcodes, csv

from common import helper_methods

__author__ = "Tige"


class ControllerUseCase2(object):
    """
    Test name: \n
        Test Moisture Sensors \n
        
    Purpose: \n
        Purpose of this use case is to test if the moisture sensors are getting waterincursionn into the Blade. \n

    
    Coverage area: \n
        1. For each Moisture Sensor:
            a. Search for MS's \n
            b. Verify device found on two-wire search \n
            c. Can assign device an address \n
            d. Verify device assigned to address \n
            e. for each Sensor run a self test \n
            f. Verify that the moisture reading has not gone outside a 20% boundary \n

        

    Real-Devices to be configured by default for test: \n

        - Moisture Sensors \n
            - SB01250 (1) \n
            - SB06300 (25) \n

    """
    # Configuration used in the test

    mini_ms_1_serial_number = 'ST00824'
    mini_ms_2_serial_number = 'ST00825'
    mini_ms_3_serial_number = 'ST00828'
    lg_ms_1_serial_number = 'SB18328'
    lg_ms_2_serial_number = 'SB18348'
    lg_ms_3_serial_number = 'SB18433'
    lg_ms_4_serial_number = 'SB18587'
    lg_ms_5_serial_number = 'SB10000'

    list_of_moisture_sensors = [
        mini_ms_1_serial_number,
        mini_ms_2_serial_number,
        mini_ms_3_serial_number,
        lg_ms_1_serial_number,
        lg_ms_2_serial_number,
        lg_ms_3_serial_number,
        lg_ms_4_serial_number
    ]

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        controller_type = 'BaseStation3200'
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        # initializes the csv writer object so it can be used in the remainder of the use_case
        self.csv_writer = CSVWriter(file_name=test_name + controller_type,
                                    relative_path='Hardware_and_Manufacturing_scripts',
                                    delimiter=',',
                                    line_terminator='\n')
        # Configures how the logging will appear in the rest of the use case
        # logging.basicConfig(filename=str(os.path.dirname(os.path.abspath(__file__))) + '/' + test_name + ".txt",
        #                     level=logging.INFO, filemode='w', format='%(levelname)s:%(message)s')
        try:
            self.csv_writer.open()
        except IOError:
            e_msg = self.csv_writer.fname + ' file is in use'
            raise IOError(e_msg)

        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True, real_devices=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)
            
    #################################
    def step_1(self):
        """
        ############################
        Configure Controller
        ############################
        
        Configure the controller to use real devices in real-time mode.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].turn_off_faux_io()
            self.config.BaseStation3200[1].set_sim_mode_to_off()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        Configure Moisture Sensors
        ############################

        1. Create Moisture Sensors
            - For each Moisture Sensor:
                1) Create bicoder
                2) Create Moisture Sensor
                3) Update bicoder attributes (with real values returned by controller) for verifying against
        2. Search and address Moisture Sensors

        -------------------------------------------------------------------
        | Address | Serial  | 2-Wire | Moisture % | Temperature (degrees) |
        -------------------------------------------------------------------
        |    1    | SB01250 | 0.9    | 0.0        | 75.2                  |
        |   25    | SB06300 | 1.2    | 2.4        | 75.2                  |
        -------------------------------------------------------------------
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # ----------------
            # BL_5311_Mini_MOISTURE SENSOR 1
            # ----------------
            self.config.BaseStation3200[1].\
                load_dv(dv_type=opcodes.moisture_sensor,
                        list_of_decoder_serial_nums=self.list_of_moisture_sensors, real_devices=True)

            # Search for Moisture Sensors so the controller can see them
            # self.config.BaseStation3200[1].do_search_for_moisture_sensor()
            for index in range(len(self.list_of_moisture_sensors)):

                self.config.BaseStation3200[1].\
                    add_moisture_sensor_to_controller(_address=index+1,
                                                      _serial_number=self.list_of_moisture_sensors[index],
                                                      real_devices=True)
                # Verify address assigned to Moisture Sensor 1
                #self.config.BaseStation3200[1].moisture_sensors[index+1].bicoder.verify_device_address()
                # starting writing csv file
                # self.csv_writer.writerow(["Set all of the Moisture Sensor Addresses"])
                # self.csv_writer.writerow([sys._getframe().f_code.co_name + " complete!"])
        except Exception as e:
                e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                        "\tThe Controller Date and time was currently set to {2}\n" \
                        "\tThe Exception thrown was {3}".format(
                    self.config.test_name,
                    sys._getframe().f_code.co_name,
                    date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                    str(e.message))
                raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        Collect readings from moisture sensors and check if their tolerances are outside of acceptable limits.

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            starting_moisture = []
            starting_temperature = []
            current_time = []
            start_of_csv_msg = ['-------Starting Data Capture-------']

            for index in range(len(self.list_of_moisture_sensors)):

                self.config.BaseStation3200[1].moisture_sensors[index+1].bicoder.self_test_and_update_object_attributes()

                starting_moisture.append(self.config.BaseStation3200[1].moisture_sensors[index+1].bicoder.vp)
                starting_temperature.append(self.config.BaseStation3200[1].moisture_sensors[index+1].bicoder.vd)

            current_time.append(datetime.now())

            # log these values
            self.csv_writer.writerow(data_to_write=start_of_csv_msg)
            self.csv_writer.writerow(data_to_write=current_time)
            self.csv_writer.writerow(data_to_write=self.list_of_moisture_sensors)
            self.csv_writer.writerow(data_to_write=starting_moisture)
            self.csv_writer.writerow(data_to_write=starting_temperature)

            minute = 1
            while True:
                current_sensors = []
                current_moisture = []
                current_temperature = []
                current_time = []
                for index in sorted(self.config.BaseStation3200[1].moisture_sensors.keys()):

                    old_moisture = self.config.BaseStation3200[1].moisture_sensors[index].bicoder.vp
                    old_temp = self.config.BaseStation3200[1].moisture_sensors[index].bicoder.vd

                    # Get the updated values for this minute
                    self.config.BaseStation3200[1].moisture_sensors[index].bicoder.\
                        self_test_and_update_object_attributes()

                    new_moisture = self.config.BaseStation3200[1].moisture_sensors[index].bicoder.vp
                    new_temp = self.config.BaseStation3200[1].moisture_sensors[index].bicoder.vd

                    current_time.append(datetime.now())
                    current_sensors.append(index)
                    current_moisture.append(new_moisture)
                    current_temperature.append(new_temp)

                    if old_moisture is None:
                        old_moisture = 0

                    if new_moisture == 0:
                        new_moisture = self.config.BaseStation3200[1].moisture_sensors[index].bicoder. \
                            self_test_and_update_object_attributes()
                        if new_moisture is None:
                            new_moisture = 0

                        if new_moisture == 0:
                            e_msg = "Exception occurred on Moisture Senor {0}." \
                                    " Moisture Percent reading: '{1}'," \
                                    " Temperature reading: '{2}'," \
                                    " Controller Date and time were set to:  {3}, "\
                                    .format(
                                        str(self.config.BaseStation3200[1].moisture_sensors[index].bicoder.sn),  # {0}
                                        str(new_moisture),      # {1}
                                        str(new_temp),          # {2}
                                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                                     )
                            log_handler.error(e_msg + '\n')

                    if new_temp == 0:
                        new_temp = self.config.BaseStation3200[1].moisture_sensors[index].bicoder. \
                            self_test_and_update_object_attributes()
                        if new_temp == 0:
                            e_msg = "Exception occurred on Temperature Senor {0}." \
                                    " Temperature Sensor reading: '{1}'," \
                                    " Moisture Sensor reading: '{2}'," \
                                    " Controller Date and time were set to:  {3}, "\
                                    .format(
                                        str(self.config.BaseStation3200[1].moisture_sensors[index].bicoder.sn),  # {0}
                                        str(new_temp),              # {1}
                                        str(new_moisture),          # {2}
                                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                                     )
                            log_handler.error(e_msg + '\n')

                    if old_moisture != 0:
                        if abs((new_moisture - old_moisture) / old_moisture) >= 10.0:
                            e_msg = "Exception occurred on Moisture Senor {0}." \
                                    " Moisture Percent reading: '{1}'," \
                                    " Temperature reading: '{2}'," \
                                    " Controller Date and time were set to:  {3}, " \
                                .format(
                                    str(self.config.BaseStation3200[1].moisture_sensors[index].bicoder.sn),  # {0}
                                    str(new_moisture),  # {1}
                                    str(new_temp),  # {2}
                                    date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                                )
                            log_handler.error(e_msg + '\n')

                    if old_temp != 0:
                        if abs((new_temp - old_temp) / old_temp) >= 10.0:
                            e_msg = "Exception occurred on Temperature Senor {0}." \
                                    " Temperature Sensor reading: '{1}'," \
                                    " Moisture Sensor reading: '{2}'," \
                                    " Controller Date and time were set to:  {3}, " \
                                .format(
                                    str(self.config.BaseStation3200[1].moisture_sensors[index].bicoder.sn),  # {0}
                                    str(new_temp),  # {1}
                                    str(new_moisture),  # {2}
                                    date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                                )
                            log_handler.error(e_msg + '\n')

                    if (new_moisture - starting_moisture[index-1]) != 0:
                        if abs((new_moisture - starting_moisture[index-1]) / starting_moisture[index-1]) >= 10.0:
                            e_msg = "Exception occurred on Moisture Senor {0}." \
                                    " Moisture Percent reading: '{1}'," \
                                    " Temperature reading: '{2}'," \
                                    " Controller Date and time were set to:  {3}, " \
                                .format(
                                    str(self.config.BaseStation3200[1].moisture_sensors[index].bicoder.sn),  # {0}
                                    str(new_moisture),  # {1}
                                    str(new_temp),  # {2}
                                    date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                                )
                            log_handler.error(e_msg + '\n')

                    if (new_temp - starting_temperature[index-1]) != 0:
                        if abs((new_temp - starting_temperature[index - 1]) / starting_temperature[index - 1]) >= 10.0:
                            e_msg = "Exception occurred on Temperature Senor {0}." \
                                    " Temperature Sensor reading: '{1}'," \
                                    " Moisture Sensor reading: '{2}'," \
                                    " Controller Date and time were set to:  {3}, " \
                                .format(
                                    str(self.config.BaseStation3200[1].moisture_sensors[index].bicoder.sn),  # {0}
                                    str(new_temp),  # {1}
                                    str(new_moisture),  # {2}
                                    date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                                )
                            log_handler.error(e_msg + '\n')

                time.sleep(60)
                minute += 1

                self.csv_writer.writerow(data_to_write=self.list_of_moisture_sensors)
                self.csv_writer.writerow(data_to_write=current_time)
                self.csv_writer.writerow(data_to_write=current_moisture)
                self.csv_writer.writerow(data_to_write=current_temperature)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
