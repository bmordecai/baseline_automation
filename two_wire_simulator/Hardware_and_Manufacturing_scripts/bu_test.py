__author__ = 'Tige'
from common.imports import *
import common.user_configuration as user_conf_mod
import common.configuration as test_conf_mod
import common.objects.base_classes.ser as ser_mod

import common.objects.baseunit.bu as bu_mod


class BUUseCase1():

    def __init__(self, conf):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n
        :type conf: user_conf_mod.UserConfiguration
        """
        self.ser = ser_mod.Ser(comport=conf.bu_comport)
        self.ser.init_ser_connection(_baud_rate=9600)

    def run_use_case(self):
        dut = bu_mod.BaseUnit(_ser=self.ser)
        dut.read_version(_base_address=1)
        print "'Read Version' results:"
        print " - Version:  " + dut.version_text()
        print " - Type:     " + dut.type_text()

        dut.read_power(_base_address=1)
        print "BEFORE 2WIRE POWER UP:"
        print " - Current A:  " + dut.current_a_text()
        print " - Voltage A:  " + dut.voltage_a_text()
        print " - Current B:  " + dut.current_b_text()
        print " - Voltage B:  " + dut.voltage_b_text()

        dut.power_up_2_wire(_base_address=1)
        print "AFTER 2WIRE POWER UP:"
        print " - Current A:  " + dut.current_a_text()
        print " - Voltage A:  " + dut.voltage_a_text()
        print " - Current B:  " + dut.current_b_text()
        print " - Voltage B:  " + dut.voltage_b_text()

        self.ser.close_connection()
