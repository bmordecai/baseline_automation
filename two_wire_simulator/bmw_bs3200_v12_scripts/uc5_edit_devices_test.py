import sys
import old_32_10_sb_objects_dec_29_2017.common.configuration as test_conf
import old_32_10_sb_objects_dec_29_2017.common.user_configuration as user_conf
import old_32_10_sb_objects_dec_29_2017.page_factory as factory
from common import helper_methods
from old_32_10_sb_objects_dec_29_2017.common import helper_methods as legacy_helper_methods
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

__author__ = 'baseline'


class BaseManagerUseCase5(object):
    """
    Purpose:
        The purpose of this test is to verify that all device types can be
        edited on BaseManager and that the changes show up in the UI.
        
    Coverage area:
        - For each device:
            - Edit the device description
            - Save
            - Verify new description displays
            - Go to QuickView and verify updated description displays
    """
    
    # ~~~~ Local Variables ~~~~ #
    
    def __init__(self, controller_type, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str
        """
        self.config = test_conf.Configuration(cn_type=controller_type,
                                              test_name=test_name,
                                              user_conf_file=user_configuration_instance,
                                              data_json_file=json_configuration_file)
        
        # Get web driver instance from config
        self.web_driver = None
        
        # Create page instances to use for Client use
        self.main_menu = None
        """:type: old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.main_page.MainPageMenu"""
        
        # Main Tab Page Objects
        self.login_page = None
        """:type: old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.login_page.LoginPage"""
        
        self.main_page = None
        """:type: old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.main_page.MainPage"""
        
        self.quick_view_tab = None
        """:type: old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.qv_tab.QuickViewTab"""
        
        # Device page Objects
        self.zones_tab = None
        """:type: old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.dv_tab.ZonesTab"""
        
        self.moisture_sensor_tab = None
        """:type: old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.dv_tab.MoistureSensorsTab"""
        
        self.master_valve_tab = None
        """:type: old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.dv_tab.MasterValvesTab"""
        
        self.flow_meter_tab = None
        """:type: old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.dv_tab.FlowMetersTab"""
        
        self.temperature_sensor_tab = None
        """:type: old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.dv_tab.TemperatureSensorsTab"""
        
        self.event_switch_tab = None
        """:type: old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.dv_tab.EventSwitchTab"""
        
        # run UC
        self.run_use_case()
    
    def run_use_case(self):
        """
        """
        try:
            # browsers_for_test = ["chrome", "firefox"]
            browsers_for_test = ['chrome']
            for run_number, browser in enumerate(browsers_for_test):
                # Resets objects to a known state, creates serial connections, creates all objects.
                self.config.initialize_for_test()
                self.step_1()
                self.step_2()
                self.step_3(_browser=browser)
                self.step_4()
                self.step_5()
                self.step_6()
                self.step_7()
                self.step_8()
                self.step_9()
                self.step_10()
                self.step_11()

                # Close the first webdriver to start the second web driver
                if run_number == 0 and len(browsers_for_test) > 1:
                    self.config.resource_handler.web_driver.web_driver.quit()
        
        except Exception as e:
            legacy_helper_methods.print_test_failed(test_name=self.config.test_name)
            # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
            # to the next use case in the list
            if log_handler.is_enabled():
                log_handler.exception(message=e.message)
            else:
                raise
        
        else:
            legacy_helper_methods.print_test_passed(self.config.test_name)
        
        finally:
            # We close the first webdriver and not the second because of this line, which closes the webdriver
            legacy_helper_methods.end_controller_test(self.config)

    #################################
    def step_1(self):
        """
        Initialize browser objects
        """
        helper_methods.print_method_name()
        try:
            
            # Get web driver instance from config
            self.web_driver = self.config.resource_handler.web_driver
            
            # Create page instances to use for Client use
            self.main_menu = factory.get_main_menu_object(_webdriver=self.web_driver)
            
            # Main Tab Page Objects
            self.login_page = factory.get_login_page_object(_webdriver=self.web_driver)
            self.main_page = factory.get_main_page_object(_webdriver=self.web_driver)
            self.quick_view_tab = factory.get_quick_view_page_object(_webdriver=self.web_driver)
            
            # Device page Objects
            self.zones_tab = factory.get_zones_page_object(_webdriver=self.web_driver)
            self.moisture_sensor_tab = factory.get_moisture_sensors_page_object(_webdriver=self.web_driver)
            self.master_valve_tab = factory.get_master_valves_page_object(_webdriver=self.web_driver)
            self.flow_meter_tab = factory.get_flow_meter_page_object(_webdriver=self.web_driver)
            self.temperature_sensor_tab = factory.get_temperature_sensors_page_object(_webdriver=self.web_driver)
            self.event_switch_tab = factory.get_event_switch_page_object(_webdriver=self.web_driver)
        
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Created BaseManager page objects."
            )

    #################################
    def step_2(self):
        """
        Initialize the controller:
        1. Clear programming
        2. Load devices
        3. Do Search for all devices
        """
        helper_methods.print_method_name()
        try:
            # Clear programming
            self.config.controllers[1].init_cn()
            
            # Verify controller is connected to BaseManger
            self.config.basemanager_connection[1].verify_ip_address_state()
            
            # Load all devices
            self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                         mv_d1_list=self.config.mv_d1,
                                                         d2_list=self.config.d2,
                                                         mv_d2_list=self.config.mv_d2,
                                                         d4_list=self.config.d4,
                                                         dd_list=self.config.dd,
                                                         ms_list=self.config.ms,
                                                         fm_list=self.config.fm,
                                                         ts_list=self.config.ts,
                                                         sw_list=self.config.sw)
            
            # Address Zones
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
            self.config.controllers[1].set_address_for_objects(object_dict=self.config.zones,
                                                               ad_range=self.config.zn_ad_range)
            
            # Address Master Valves
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
            self.config.controllers[1].set_address_for_objects(object_dict=self.config.master_valves,
                                                               ad_range=self.config.mv_ad_range)
            
            # Address Moisture Sensors
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
            # self.config.controllers[1].set_address_for_objects(object_dict=self.config.moisture_sensors,
            #                                                    ad_range=self.config.ms_ad_range)
            
            # Address Temperature Sensors
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.temperature_sensor)
            # self.config.controllers[1].set_address_for_objects(object_dict=self.config.temperature_sensors,
            #                                                    ad_range=self.config.ts_ad_range)
            
            # Address Event Switches
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.event_switch)
            # self.config.controllers[1].set_address_for_objects(object_dict=self.config.event_switches,
            #                                                    ad_range=self.config.sw_ad_range)
            
            # Address Flow Meters
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.flow_meter)
            # self.config.controllers[1].set_address_for_objects(object_dict=self.config.flow_meters,
            #                                                    ad_range=self.config.fm_ad_range)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Searched and assigned devices on the controller through test engine."
            )

    #################################
    def step_3(self, _browser):
        """
        Initializes the web driver to the correct browser executor
        """
        helper_methods.print_method_name()
        try:
            # Open browser
            self.config.resource_handler.web_driver.open(_browser)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Opened the web browser."
            )

    #################################
    def step_4(self):
        """
        Log in to client.
        """
        helper_methods.print_method_name()
        try:
            # enter the user name and password selects login button
            self.login_page.enter_login_info()
            self.login_page.click_login_button()
            self.main_page.verify_open()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Logged in as: {user}.".format(user=self.config.user_conf.user_name)
            )

    #################################
    def step_5(self):
        """
        - Selects a site and controller
        - Verifies QuickView Tab is opened
        :return:
        :rtype:
        """
        helper_methods.print_method_name()
        try:
            self.main_page.select_main_menu()
            self.main_menu.select_site()
            self.main_menu.select_a_controller(mac_address=self.config.mac)
            self.main_menu.wait_for_main_menu_close()
            
            # selecting a controller returns you to the quick view tab
            self.quick_view_tab.verify_open()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Selected site and controller and verified user taken to QuickView."
            )

    #################################
    def step_6(self):
        """
        - Navigate to zones tab
        - Select a zone and edit its description and save
        - Verify zones description updates in devices - zones tab
        - Verify zones description updates in quick view
        """
        helper_methods.print_method_name()
        try:
            zone_to_edit = 1
            new_zone_desc = "New Zone {address} Description".format(address=zone_to_edit)
            
            self.main_page.select_devices_tab(device_type=opcodes.zone)
            self.zones_tab.verify_is_open()
            self.zones_tab.select_zone(address=zone_to_edit)
            self.zones_tab.verify_zone_detail_view_is_visible()
            self.zones_tab.select_zone_edit_button()
            self.zones_tab.set_description_in_edit_view(address=zone_to_edit,
                                                        description=new_zone_desc)
            self.zones_tab.select_save_button()
            self.zones_tab.verify_description_updates(address=zone_to_edit)
            
            self.main_page.select_quick_view_tab()
            self.quick_view_tab.verify_open()
            self.quick_view_tab.select_zone_status_box(address=zone_to_edit)
            self.quick_view_tab.verify_description(dv_type=opcodes.zone,
                                                   id=zone_to_edit,
                                                   description=new_zone_desc)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Edited zone {address}'s description and verified changes "
                    "appeared in Zones tab and QuickView".format(address=zone_to_edit)
            )

    #################################
    def step_7(self):
        """
        - Navigate to master valves tab
        - Select a mv and edit its description and save
        - Verify mv's description updates in devices - master valve tab
        - Verify mv's description updates in quick view
        """
        helper_methods.print_method_name()
        try:
            mv_to_edit = 1
            mv_obj = self.config.master_valves[mv_to_edit]
            new_mv_desc = "New MV {address} {serial} Description".format(
                address=mv_to_edit,
                serial=mv_obj.sn
            )
        
            # edit description in devices - MV
            self.main_page.select_devices_tab(device_type=opcodes.master_valve)
            self.master_valve_tab.verify_is_open()
            self.master_valve_tab.select_non_zone_device_edit_button(serial=mv_obj.sn)
            self.master_valve_tab.verify_non_zone_edit_dialog_is_open()
            self.master_valve_tab.set_description_in_edit_view(address=mv_to_edit,
                                                               description=new_mv_desc)
            self.master_valve_tab.select_save_button()
            
            # verify description updates in Devices > MV tab
            self.master_valve_tab.verify_description_updates(address=mv_to_edit)
            
            # verify description updates in QuickView
            self.main_page.select_quick_view_tab()
            self.quick_view_tab.verify_open()
            self.quick_view_tab.select_master_valve_status_box(address=mv_to_edit, serial=mv_obj.sn)
            self.quick_view_tab.verify_description(dv_type=opcodes.master_valve,
                                                   id=mv_obj.sn,
                                                   description=new_mv_desc)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Edited master valves {address} {serial}'s description and verified changes "
                    "appeared in Master Valves tab and QuickView".format(
                        address=mv_to_edit,
                        serial=mv_obj.sn
                )
            )

    #################################
    def step_8(self):
        """
        - Navigate to Flow Meters tab
        - Select a fm and edit its description and save
        - Verify fm's description updates in devices - Flow Meters tab
        - Verify fm's description updates in quick view
        """
        helper_methods.print_method_name()
        try:
            fm_to_edit = 1
            fm_obj = self.config.flow_meters[fm_to_edit]
            new_fm_desc = "New FM {address} {serial} Description".format(
                address=fm_to_edit,
                serial=fm_obj.sn
            )
        
            # edit description in devices - FM
            self.main_page.select_devices_tab(device_type=opcodes.flow_meter)
            self.flow_meter_tab.verify_is_open()
            self.flow_meter_tab.select_non_zone_device_edit_button(serial=fm_obj.sn)
            self.flow_meter_tab.verify_non_zone_edit_dialog_is_open()
            self.flow_meter_tab.set_description_in_edit_view(address=fm_to_edit,
                                                             description=new_fm_desc)
            self.flow_meter_tab.select_save_button()
        
            # verify description updates in Devices > FM tab
            self.flow_meter_tab.verify_description_updates(address=fm_to_edit)
        
            # verify description updates in QuickView
            self.main_page.select_quick_view_tab()
            self.quick_view_tab.verify_open()
            self.quick_view_tab.verify_description(dv_type=opcodes.flow_meter,
                                                   id=fm_obj.sn,
                                                   description=new_fm_desc)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Edited Flow Meter {address} {serial}'s description and verified changes "
                    "appeared in Flow Meters tab and QuickView".format(
                        address=fm_to_edit,
                        serial=fm_obj.sn
                    )
            )

    #################################
    def step_9(self):
        """
        - Navigate to Moisture Sensors tab
        - Select a ms and edit its description and save
        - Verify ms's description updates in devices - Moisture Sensors tab
        - Verify ms's description updates in quick view
        """
        helper_methods.print_method_name()
        try:
            ms_to_edit = 1
            ms_obj = self.config.moisture_sensors[ms_to_edit]
            new_ms_desc = "New MS {address} {serial} Description".format(
                address=ms_to_edit,
                serial=ms_obj.sn
            )
        
            # edit description in devices - MS
            self.main_page.select_devices_tab(device_type=opcodes.moisture_sensor)
            self.moisture_sensor_tab.verify_is_open()
            self.moisture_sensor_tab.select_non_zone_device_edit_button(serial=ms_obj.sn)
            self.moisture_sensor_tab.verify_non_zone_edit_dialog_is_open()
            self.moisture_sensor_tab.set_description_in_edit_view(address=ms_to_edit,
                                                                  description=new_ms_desc)
            self.moisture_sensor_tab.select_save_button()
        
            # verify description updates in Devices > MS tab
            self.moisture_sensor_tab.verify_description_updates(address=ms_to_edit)
        
            # verify description updates in QuickView
            self.main_page.select_quick_view_tab()
            self.quick_view_tab.verify_open()
            self.quick_view_tab.verify_description(dv_type=opcodes.moisture_sensor,
                                                   id=ms_obj.sn,
                                                   description=new_ms_desc)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Edited Moisture Sensor {address} {serial}'s description and verified changes "
                    "appeared in Moisture Sensors tab and QuickView".format(
                        address=ms_to_edit,
                        serial=ms_obj.sn
                    )
            )
            
    #################################
    def step_10(self):
        """
        - Navigate to Event Switches tab
        - Select a sw and edit its description and save
        - Verify sw's description updates in devices - Event Switchs tab
        - Verify sw's description updates in quick view
        """
        helper_methods.print_method_name()
        try:
            sw_to_edit = 1
            sw_obj = self.config.event_switches[sw_to_edit]
            new_sw_desc = "New SW {address} {serial} Description".format(
                address=sw_to_edit,
                serial=sw_obj.sn
            )
        
            # edit description in devices - SW
            self.main_page.select_devices_tab(device_type=opcodes.event_switch)
            self.event_switch_tab.verify_is_open()
            self.event_switch_tab.select_non_zone_device_edit_button(serial=sw_obj.sn)
            self.event_switch_tab.verify_non_zone_edit_dialog_is_open()
            self.event_switch_tab.set_description_in_edit_view(address=sw_to_edit,
                                                               description=new_sw_desc)
            self.event_switch_tab.select_save_button()
        
            # verify description updates in Devices > SW tab
            self.event_switch_tab.verify_description_updates(address=sw_to_edit)
        
            # verify description updates in QuickView
            self.main_page.select_quick_view_tab()
            self.quick_view_tab.verify_open()
            self.quick_view_tab.verify_description(dv_type=opcodes.event_switch,
                                                   id=sw_obj.sn,
                                                   description=new_sw_desc)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Edited Event Switch {address} {serial}'s description and verified changes "
                    "appeared in Event Switches tab and QuickView".format(
                        address=sw_to_edit,
                        serial=sw_obj.sn
                    )
            )
    
    #################################
    def step_11(self):
        """
        - Navigate to Temperature Sensors tab
        - Select a ts and edit its description and save
        - Verify ts's description updates in devices - Temperature Sensors tab
        - Verify ts's description updates in quick view
        """
        helper_methods.print_method_name()
        try:
            ts_to_edit = 1
            ts_obj = self.config.temperature_sensors[ts_to_edit]
            new_ts_desc = "New TS {address} {serial} Description".format(
                address=ts_to_edit,
                serial=ts_obj.sn
            )
        
            # edit description in devices - TS
            self.main_page.select_devices_tab(device_type=opcodes.temperature_sensor)
            self.temperature_sensor_tab.verify_is_open()
            self.temperature_sensor_tab.select_non_zone_device_edit_button(serial=ts_obj.sn)
            self.temperature_sensor_tab.verify_non_zone_edit_dialog_is_open()
            self.temperature_sensor_tab.set_description_in_edit_view(address=ts_to_edit,
                                                                     description=new_ts_desc)
            self.temperature_sensor_tab.select_save_button()
        
            # verify description updates in Devices > TS tab
            self.temperature_sensor_tab.verify_description_updates(address=ts_to_edit)
        
            # verify description updates in QuickView
            self.main_page.select_quick_view_tab()
            self.quick_view_tab.verify_open()
            self.quick_view_tab.verify_description(dv_type=opcodes.temperature_sensor,
                                                   id=ts_obj.sn,
                                                   description=new_ts_desc)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Edited Temperature Sensor {address} {serial}'s description and verified changes "
                    "appeared in Temperature Sensors tab and QuickView".format(
                        address=ts_to_edit,
                        serial=ts_obj.sn
                    )
            )
