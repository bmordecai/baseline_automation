__author__ = 'baseline'
import datetime
import sys
from datetime import datetime, timedelta

import old_32_10_sb_objects_dec_29_2017.common.helper_methods

from old_32_10_sb_objects_dec_29_2017.common.imports import *
import old_32_10_sb_objects_dec_29_2017.common.configuration as test_conf
import old_32_10_sb_objects_dec_29_2017.common.user_configuration as user_conf
import old_32_10_sb_objects_dec_29_2017.page_factory as factory
from old_32_10_sb_objects_dec_29_2017.common import helper_methods
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_3200 import POC3200


class BaseManagerUseCase6(object):
    """
    Purpose:
        This test verifies all Point of Connection attributes displaying on Base Manager for v12 3200 controller. \n
            - verify each tab opens successfully
                - Water Sources
                    - Point of Connection
            -
            
    Coverage:
        - The following POC attributes are verified to:
            - Be visible for v12 3200
            - Have correct text

        POC Attributes verified:
            - Switch Empty Condition Limit
            - Switch Empty Condition Wait Time

        This test verifies fix of JIRA Bugs:
            - BM-1925 (https://baseline.atlassian.net/browse/BM-1925)
            - BM-1926 (https://baseline.atlassian.net/browse/BM-1926)
            - BM-1927 (https://baseline.atlassian.net/browse/BM-1927)

    
    TODO: Need to add verifying the rest of POC attributes being displayed in the UI
    TODO: Need to add verifying Moisture Empty condition being displayed correctly (similar to event switch)

    Bugs covered by test:
        (1) BM-1925
        (2) BM-1926
        (3) BM-1927

    :type poc_tab: old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.water_sources_tab_3200.PointOfConnectionPage3200
    """
    
    # ~~~~ Local Variables ~~~~ #
    
    def __init__(self, controller_type, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = test_conf.Configuration(cn_type=controller_type,
                                              test_name=test_name,
                                              user_conf_file=user_configuration_instance,
                                              data_json_file=json_configuration_file)
        
        # Get web driver instance from config
        self.web_driver = None
        
        # Create page instances to use for Client use
        self.main_menu = None
        
        # Main Tab Page Objects
        self.login_page = None
        self.main_page = None
        
        # 3200 specific tabs
        self.poc_tab = None  # :type
        
        # run UC
        self.run_use_case()
    
    def run_use_case(self):
        """
        """
        time_test_started = datetime.now()
        time_it_should_take_test_to_run = timedelta(minutes=3, seconds=30)  # this is minutes
        try:
            # browsers_for_test = ["chrome", "firefox"]
            browsers_for_test = ["chrome"]
            for run_number, browser in enumerate(browsers_for_test):
                
                # Resets objects to a known state, creates serial connections, creates all objects.
                self.config.initialize_for_test()
                
                self.step_1()
                self.step_2()
                self.step_3(_browser=browser)
                self.step_4()
                self.step_5()
                self.step_6()
                self.step_7()
                self.step_8()
                self.step_9()
                self.step_10()
                self.step_11()
                self.step_12()
                
                # Close the first webdriver to start the second web driver
                if run_number == 0 and len(browsers_for_test) > 1:
                    self.config.resource_handler.web_driver.web_driver.quit()

            helper_methods.test_elapsed_time(_time_started=time_test_started, _time_expected_to_run=time_it_should_take_test_to_run)
        except Exception as e:
            helper_methods.print_test_failed(test_name=self.config.test_name)
            # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
            # to the next use case in the list
            if log_handler.is_enabled():
                log_handler.exception(message=e.message)
            else:
                raise
        
        else:
            helper_methods.print_test_passed(self.config.test_name)
        
        finally:
            # We close the first webdriver and not the second because of this line, which closes the webdriver
            helper_methods.end_controller_test(self.config)

    #################################
    def step_1(self):
        """
        Initialize POC on controller
        
        MV: TPR0011
        FM: TWF0001
        SW: TPD0001
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        # Init controller and connect to BM
        self.config.controllers[1].init_cn()
        self.config.basemanager_connection[1].verify_ip_address_state()
        
        # ----------------------
        # Load devices onto controller
        # ----------------------
        
        self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                     mv_d1_list=self.config.mv_d1,
                                                     d2_list=self.config.d2,
                                                     mv_d2_list=self.config.mv_d2,
                                                     d4_list=self.config.d4,
                                                     dd_list=self.config.dd,
                                                     ms_list=self.config.ms,
                                                     fm_list=self.config.fm,
                                                     ts_list=self.config.ts,
                                                     sw_list=self.config.sw)

        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
        self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                         zn_ad_range=self.config.zn_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
        self.config.controllers[1].set_address_and_default_values_for_mv(mv_object_dict=self.config.master_valves,
                                                                         mv_ad_range=self.config.mv_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
        self.config.controllers[1].set_address_and_default_values_for_ms(ms_object_dict=self.config.moisture_sensors,
                                                                         ms_ad_range=self.config.ms_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.temperature_sensor)
        self.config.controllers[1].set_address_and_default_values_for_ts(ts_object_dict=self.config.temperature_sensors,
                                                                         ts_ad_range=self.config.ts_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.event_switch)
        self.config.controllers[1].set_address_and_default_values_for_sw(sw_object_dict=self.config.event_switches,
                                                                         sw_ad_range=self.config.sw_ad_range)
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.flow_meter)
        self.config.controllers[1].set_address_and_default_values_for_fm(fm_object_dict=self.config.flow_meters,
                                                                         fm_ad_range=self.config.fm_ad_range)
        
        # ----------------------
        # Set up POCs for test
        # ----------------------
        
        self.config.poc[1] = POC3200(_ad=1,
                                     _mv=self.config.master_valves[1].ad,
                                     _fm=self.config.flow_meters[1].ad,
                                     _en=opcodes.true,
                                     _ml=0,
                                     _fl=50,
                                     _pr=3,
                                     _uf=5,
                                     _us=opcodes.false,
                                     _hf=75,
                                     _hs=opcodes.false,
                                     _wb=1000,
                                     _ws=opcodes.false,
                                     _wr=opcodes.false,
                                     _sw=1,
                                     _se=opcodes.open,
                                     _sn=opcodes.true,
                                     _ew=9)
        print "----"
        print "Successfully finished Step 1 in BaseManager POC Attribute Test"
        print "-> Initialized POC 1 with an event switch empty condition."
        print "----"

    #################################
    def step_2(self):
        """
        Initializes all pages needed for test
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        # Get web driver instance from config
        self.web_driver = self.config.resource_handler.web_driver
        
        # Create page instances to use for Client use
        self.main_menu = factory.get_main_menu_object(_webdriver=self.web_driver)
        
        # Main Tab Page Objects
        self.login_page = factory.get_login_page_object(_webdriver=self.web_driver)
        self.main_page = factory.get_main_page_object(_webdriver=self.web_driver)
        
        # 3200 specific tabs
        self.poc_tab = factory.get_3200_poc_tab(_webdriver=self.web_driver)
        print "----"
        print "Successfully finished Step 2 in BaseManager POC Attribute Test"
        print "-> Initialized web pages"
        print "----"

    #################################
    def step_3(self, _browser):
        """
        Initializes the web driver to the correct browser executor
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        # Open browser
        self.config.resource_handler.web_driver.open(_browser)
        print "----"
        print "Successfully finished Step 3 in BaseManager POC Attribute Test"
        print "-> Opened Web Browser"
        print "----"

    #################################
    def step_4(self):
        """
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        # enter the user name and password selects login button
        self.login_page.enter_login_info()
        self.login_page.click_login_button()
        self.main_page.verify_open()
        print "----"
        print "Successfully finished Step 4 in BaseManager POC Attribute Test"
        print "-> Logged In"
        print "----"

    #################################
    def step_5(self):
        """
        - Selects a site and controller
        - Verifies QuickView Tab is opened
        :return:
        :rtype:
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        self.main_page.select_main_menu()
        self.main_menu.select_site()
        self.main_menu.select_a_controller(mac_address=self.config.mac)
        self.main_menu.wait_for_main_menu_close()
        print "----"
        print "Successfully finished Step 5 in BaseManager POC Attribute Test"
        print "-> Selected Site and Controller"
        print "----"

    #################################
    def step_6(self):
        """
        Selects the POC tab
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        self.main_page.select_water_sources_tab(water_source_type=opcodes.point_of_connection)
        self.poc_tab.verify_open()
        print "----"
        print "Successfully finished Step 6 in BaseManager POC Attribute Test"
        print "-> Selected Points of Connection Tab and Verified it opened"
        print "----"

    #################################
    def step_7(self):
        """
        Verify POC view header and table headers
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        self.poc_tab.verify_header_text()
        self.poc_tab.verify_list_view_table_id_column_displayed()
        self.poc_tab.verify_list_view_table_description_column_displayed()
        self.poc_tab.verify_list_view_table_mainline_column_displayed()
        print "----"
        print "Successfully finished Step 7 in BaseManager POC Attribute Test"
        print "-> Verified POC List View UI"
        print "----"

    #################################
    def step_8(self):
        """
        Verify JIRA Bugs in POC list view
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        
        self.poc_tab.verify_flowstation_mainline_assignment_is_visible_in_list_view_jira_bm_1932(poc_number=1)
        
        print "----"
        print "Successfully finished Step 8 in BaseManager POC Attribute Test"
        print "-> Verified JIRA Bugs (BM-1932) for POC List View."
        print "----"
        
    #################################
    def step_9(self):
        """
        Select POC 1
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        self.poc_tab.select_poc(poc_number=1)
        self.poc_tab.verify_detail_view_displayed(poc_number=1)
        
        print "----"
        print "Successfully finished Step 9 in BaseManager POC Attribute Test"
        print "-> Verified POC Detail View Displayed for POC 1."
        print "----"

    #################################
    def step_10(self):
        """
        Verify JIRA Bugs in POC detail view
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        self.poc_tab.verify_share_this_poc_option_is_hidden_jira_bm_1925()
        self.poc_tab.verify_master_valve_enable_is_hidden_jira_bm_1926()
        self.poc_tab.verify_switch_empty_condition_components_are_visible_jira_bm_1927()
        self.poc_tab.verify_flowstation_mainline_assignment_is_visible_in_detail_view_jira_bm_1932()
        
        print "----"
        print "Successfully finished Step 10 in BaseManager POC Attribute Test"
        print "-> Verified JIRA Bugs (BM-1925, BM-1926, BM-1927, BM-1932) for POC Detail View."
        print "----"

    #################################
    def step_11(self):
        """
        Verify JIRA Bugs in POC detail view (edit mode)
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        
        self.poc_tab.select_edit_button()
        self.poc_tab.verify_flowstation_option_visible_in_mainline_assignment_jira_bm_1932()
    
        print "----"
        print "Successfully finished Step 11 in BaseManager POC Attribute Test"
        print "-> Verified JIRA Bugs (BM-1932) for POC Detail View in edit mode."
        print "----"

    #################################
    def step_12(self):
        """
        Verify Editing a POC description propogates to other portions of the app.
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method
        
        poc_to_edit = 1
        new_poc_description = "New POC Description {address}".format(address=poc_to_edit)

        self.main_page.select_water_sources_tab(water_source_type=opcodes.point_of_connection)
        self.poc_tab.verify_open()
        self.poc_tab.select_poc(poc_number=poc_to_edit)
        self.poc_tab.verify_detail_view_displayed(poc_number=poc_to_edit)
        self.poc_tab.select_edit_button()
        self.poc_tab.set_description(number=poc_to_edit, description=new_poc_description)
        self.poc_tab.select_save_button()
        self.poc_tab.verify_description_updates(number=poc_to_edit)
    
        print "----"
        print "Successfully finished Step 12 in BaseManager POC Attribute Test"
        print "-> Verified editing a POC description and displaying new description in list view."
        print "----"
