from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram

__author__ = 'baseline'
import datetime
import sys
from datetime import datetime, timedelta

from old_32_10_sb_objects_dec_29_2017.common.imports import *
import old_32_10_sb_objects_dec_29_2017.common.configuration as test_conf
import old_32_10_sb_objects_dec_29_2017.common.user_configuration as user_conf
import old_32_10_sb_objects_dec_29_2017.page_factory as factory
from old_32_10_sb_objects_dec_29_2017.common import helper_methods as legacy_helper_methods
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
from common import helper_methods

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.ml import Mainline
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_3200 import PG3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_3200 import POC3200


class BaseManagerUseCase8(object):
    """
    Purpose:
        This test verifies all Mainline attributes displaying on Base Manager for v12 3200 controller. \n
            - verify each tab opens successfully
                - Water Sources
                    - Mainline
            -

    Coverage:
        - The following ML attributes are verified to:
            - Be visible for v12 3200
            - Have correct text

        ML Attributes verified:
            - Description

        This test verifies fix of JIRA Bugs:
            - None


    TODO: Need to add verifying the rest of ML attributes being displayed in the UI

    :type login_page: old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.login_page.LoginPage
    :type main_page: old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.main_page.MainPage
    :type main_menu: old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.main_page.MainPageMenu
    :type mainline_tab: old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.water_sources_tab_3200.MainlinePage3200
    :type poc_tab: old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.water_sources_tab_3200.PointOfConnectionPage3200
    :type programs_tab: old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.pg_tab.ProgramsTab
    """
    
    # ~~~~ Local Variables ~~~~ #
    
    def __init__(self, controller_type, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = test_conf.Configuration(cn_type=controller_type,
                                              test_name=test_name,
                                              user_conf_file=user_configuration_instance,
                                              data_json_file=json_configuration_file)
        
        # Get web driver instance from config
        self.web_driver = None
        
        # Create page instances to use for Client use
        self.main_menu = None
        
        # Main Tab Page Objects
        self.login_page = None
        self.main_page = None
        
        # 3200 specific tabs
        self.mainline_tab = None
        self.poc_tab = None
        self.programs_tab = None
        
        # set local reference to the objects to be used throughout test
        self.ml_to_use = 1
        self.poc_to_use = 1
        self.pg_to_use = 1
        self.zn_to_use = 1
        
        # run UC
        self.run_use_case()

    #################################
    def run_use_case(self):
        """
        """
        time_test_started = datetime.now()
        time_it_should_take_test_to_run = timedelta(minutes=2, seconds=45)  # this is minutes
        try:
            # browsers_for_test = ["chrome", "firefox"]
            browsers_for_test = ["chrome"]
            for run_number, browser in enumerate(browsers_for_test):
                
                # Resets objects to a known state, creates serial connections, creates all objects.
                self.config.initialize_for_test()
                
                self.step_1()
                self.step_2()
                self.step_3(_browser=browser)
                self.step_4()
                self.step_5()
                self.step_6()
                self.step_7()
                self.step_8()
                
                # Close the first webdriver to start the second web driver
                if run_number == 0 and len(browsers_for_test) > 1:
                    self.config.resource_handler.web_driver.web_driver.quit()
            
            legacy_helper_methods.test_elapsed_time(_time_started=time_test_started,
                                                    _time_expected_to_run=time_it_should_take_test_to_run)
        except Exception as e:
            legacy_helper_methods.print_test_failed(test_name=self.config.test_name)
            # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
            # to the next use case in the list
            if log_handler.is_enabled():
                log_handler.exception(message=e.message)
            else:
                raise
        
        else:
            legacy_helper_methods.print_test_passed(self.config.test_name)
        
        finally:
            # We close the first webdriver and not the second because of this line, which closes the webdriver
            legacy_helper_methods.end_controller_test(self.config)
    
    #################################
    def step_1(self):
        """
        Initialize ML on controller

        MV: TPR0011
        FM: TWF0001
        SW: TPD0001
        """
        helper_methods.print_method_name()
        try:
            # Init controller and connect to BM
            self.config.controllers[1].init_cn()
            self.config.basemanager_connection[1].verify_ip_address_state()
            
            # ----------------------
            # Load devices onto controller
            # ----------------------
            
            self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                         mv_d1_list=self.config.mv_d1,
                                                         d2_list=self.config.d2,
                                                         mv_d2_list=self.config.mv_d2,
                                                         d4_list=self.config.d4,
                                                         dd_list=self.config.dd,
                                                         ms_list=self.config.ms,
                                                         fm_list=self.config.fm,
                                                         ts_list=self.config.ts,
                                                         sw_list=self.config.sw)
            
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
            self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                             zn_ad_range=self.config.zn_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
            self.config.controllers[1].set_address_and_default_values_for_mv(mv_object_dict=self.config.master_valves,
                                                                             mv_ad_range=self.config.mv_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ms(ms_object_dict=self.config.moisture_sensors,
                                                                             ms_ad_range=self.config.ms_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.temperature_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ts(ts_object_dict=self.config.temperature_sensors,
                                                                             ts_ad_range=self.config.ts_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.event_switch)
            self.config.controllers[1].set_address_and_default_values_for_sw(sw_object_dict=self.config.event_switches,
                                                                             sw_ad_range=self.config.sw_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.flow_meter)
            self.config.controllers[1].set_address_and_default_values_for_fm(fm_object_dict=self.config.flow_meters,
                                                                             fm_ad_range=self.config.fm_ad_range)
            # ----------------------
            # Set up MLs for test
            # ----------------------

            self.config.mainlines[self.ml_to_use] = Mainline(_ad=self.ml_to_use)
            self.config.poc[self.poc_to_use] = POC3200(_ad=self.poc_to_use, _ml=self.ml_to_use)
            
            # create a program object so that we can verify it's assigned mainline in programs tab without having to
            # add a program.
            self.config.programs[self.pg_to_use] = PG3200(_ad=self.pg_to_use, _ml=self.ml_to_use)
            
            # create a zone program so that the PG shows up in BM
            self.config.zone_programs[self.zn_to_use] = ZoneProgram(
                zone_obj=self.config.zones[self.zn_to_use],
                prog_obj=self.config.programs[self.pg_to_use],
                _rt=60,
                _pz=0   # pz=0 => timed
            )
            
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Initialized controller and created initial mainline, poc and program for test."
            )
    
    #################################
    def step_2(self):
        """
        Initializes all pages needed for test
        """
        helper_methods.print_method_name()
        try:
            # Get web driver instance from config
            self.web_driver = self.config.resource_handler.web_driver
            
            # Create page instances to use for Client use
            self.main_menu = factory.get_main_menu_object(_webdriver=self.web_driver)
            
            # Main Tab Page Objects
            self.login_page = factory.get_login_page_object(_webdriver=self.web_driver)
            self.main_page = factory.get_main_page_object(_webdriver=self.web_driver)
            
            # 3200 specific tabs
            self.mainline_tab = factory.get_3200_mainlines_tab(_webdriver=self.web_driver)
            self.poc_tab = factory.get_3200_poc_tab(_webdriver=self.web_driver)
            self.programs_tab = factory.get_programs_page_object(_webdriver=self.web_driver)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Initialized BaseManager page objects"
            )
    
    #################################
    def step_3(self, _browser):
        """
        Initializes the web driver to the correct browser executor
        """
        helper_methods.print_method_name()
        try:
            # Open browser
            self.config.resource_handler.web_driver.open(_browser)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Opened a browser instance for test."
            )
    
    #################################
    def step_4(self):
        """
        Login to BaseManager.
        """
        helper_methods.print_method_name()
        try:
            # enter the user name and password selects login button
            self.login_page.enter_login_info()
            self.login_page.click_login_button()
            self.main_page.verify_open()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Successfully logged in to BaseManager."
            )
    
    #################################
    def step_5(self):
        """
        - Selects a site and controller
        - Verifies QuickView Tab is opened
        :return:
        :rtype:
        """
        helper_methods.print_method_name()
        try:
            self.main_page.select_main_menu()
            self.main_menu.select_site()
            self.main_menu.select_a_controller(mac_address=self.config.mac)
            self.main_menu.wait_for_main_menu_close()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Selected site and controller."
            )
    
    #################################
    def step_6(self):
        """
        Selects the ML tab
        """
        helper_methods.print_method_name()
        try:
            self.main_page.select_water_sources_tab(water_source_type=opcodes.mainline)
            self.mainline_tab.verify_open()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Selected the Mainlines Tab and verified the mainline list displayed."
            )
    
    #################################
    def step_7(self):
        """
        Verify Editing a ML description propagates to other portions of the app.
        """
        helper_methods.print_method_name()
        try:
            new_ml_description = "New ML Description {ml_number}".format(ml_number=self.ml_to_use)
            self.mainline_tab.select_mainline(ml_number=self.ml_to_use)
            self.mainline_tab.verify_detail_view_displayed(ml_number=self.ml_to_use)
            self.mainline_tab.select_edit_button()
            self.mainline_tab.set_description(number=self.ml_to_use, description=new_ml_description)
            self.mainline_tab.select_save_button()
            self.mainline_tab.verify_description_updates(number=self.ml_to_use)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Verified editing a mainlines description updates in the mainline list view."
            )

    #################################
    def step_8(self):
        """
        Verify Editing a ML description propagates to other tabs in BaseManager
        - Point of connection
        - Programs
        """
        helper_methods.print_method_name()
        try:
            # Verify ML description in POC UI (list view)
            self.main_page.select_water_sources_tab(water_source_type=opcodes.point_of_connection)
            self.poc_tab.verify_open()
            self.poc_tab.verify_list_view_assigned_mainline_description(poc_number=self.poc_to_use)
            
            # Verify ML description in PG UI (detail view)
            self.main_page.select_programs_tab()
            self.programs_tab.verify_open()
            self.programs_tab.select_program(pg_number=self.pg_to_use)
            self.programs_tab.verify_assigned_mainline_in_detail_view(pg_number=self.pg_to_use)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Verified editing a mainlines description updates in the mainline list view."
            )
