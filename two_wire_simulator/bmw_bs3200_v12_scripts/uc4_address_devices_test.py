import sys
import old_32_10_sb_objects_dec_29_2017.common.configuration as test_conf
import old_32_10_sb_objects_dec_29_2017.common.user_configuration as user_conf
import old_32_10_sb_objects_dec_29_2017.page_factory as factory
from old_32_10_sb_objects_dec_29_2017.common import helper_methods
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler

__author__ = 'baseline'


class BaseManagerUseCase4(object):
    """
    Purpose:
        The purpose of this test is to verify that all device types can be
        configured into each controller type from Basemanager.
    Coverage area:
        - Search for each device
        - Assign each device to an address/serial number and give descriptions
        - Verify that the search and address holds after save

    """
    # ~~~~ Local Variables ~~~~ #

    def __init__(self, controller_type, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = test_conf.Configuration(cn_type=controller_type,
                                              test_name=test_name,
                                              user_conf_file=user_configuration_instance,
                                              data_json_file=json_configuration_file)
        
        # Get web driver instance from config
        self.web_driver = None

        # Create page instances to use for Client use
        self.main_menu = None

        # Main Tab Page Objects
        self.login_page = None
        self.main_page = None
        self.maps_tab = None
        self.quick_view_tab = None
        self.live_view_tab = None
        self.programs_tab = None

        # Device page Objects
        self.zones_tab = None
        self.moisture_sensor_tab = None
        self.master_valve_tab = None
        self.flow_meter_tab = None
        self.temperature_sensor_tab = None
        self.event_switch_tab = None

        # 3200 specific tabs
        self.poc_tab = None
        self.mainlines_tab = None
        
        # run UC
        self.run_use_case()

    def run_use_case(self):
        """
        The following steps are done on all browser specified in the user configuration: \n

        Step 1:
            - Initialize the controller
                1. Clear all programming
                2. Load all necessary devices from json file
                3. Do search for all devices

        Step 2:
            - initialize & open browser

        step 3:
            - login & verify successful logged in

        Step 4:
            - select site
            - select controller
            - verify QuickView tab opens

        Step 5:
            - Select Zones Tab
            - Search, address and give a description to all loaded Zones
            - Save and verify all changes held

        Step 6:
            - Select Temperature Sensors Tab
            - Search, address and give a description to all loaded Moisture Sensors
            - Save and verify all changes held

        Step 7:
            - Select Flow Meter Tab
            - Search, address and give a description to all loaded Flow Meter
            - Save and verify all changes held

        step 8:
            - Select Master Valve Tab
            - Search, address and give a description to all loaded Master Valve
            - Save and verify all changes held

        step 9:
            - Select Temperature Sensors Tab
            - Search, address and give a description to all loaded Temperature Sensors
            - Save and verify all changes held

        step 10:
            - Select Event Switches Tab
            - Search, address and give a description to all loaded Event Switches
            - Save and verify all changes held
        """
        try:
            # browsers_for_test = ["chrome", "firefox"]
            browsers_for_test = ['chrome']
            for run_number, browser in enumerate(browsers_for_test):
                # Resets objects to a known state, creates serial connections, creates all objects.
                self.config.initialize_for_test()
                self.step_1()
                self.step_2()
                self.step_3(_browser=browser)
                self.step_4()
                self.step_5()
                self.step_6()
                self.step_7()
                self.step_8()
                self.step_9()
                self.step_10()
                self.step_11()

                # Close the first webdriver to start the second web driver
                if run_number == 0 and len(browsers_for_test) > 1:
                    self.config.resource_handler.web_driver.web_driver.quit()

        except Exception as e:
            helper_methods.print_test_failed(test_name=self.config.test_name)
            # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
            # to the next use case in the list
            if log_handler.is_enabled():
                log_handler.exception(message=e.message)
            else:
                raise

        else:
            helper_methods.print_test_passed(self.config.test_name)

        finally:
            # We close the first webdriver and not the second because of this line, which closes the webdriver
            helper_methods.end_controller_test(self.config)
            
    def step_1(self):
        """
        Initialize browser objects
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        # Get web driver instance from config
        self.web_driver = self.config.resource_handler.web_driver

        # Create page instances to use for Client use
        self.main_menu = factory.get_main_menu_object(_webdriver=self.web_driver)

        # Main Tab Page Objects
        self.login_page = factory.get_login_page_object(_webdriver=self.web_driver)
        self.main_page = factory.get_main_page_object(_webdriver=self.web_driver)
        self.maps_tab = factory.get_maps_page_object(_webdriver=self.web_driver)
        self.quick_view_tab = factory.get_quick_view_page_object(_webdriver=self.web_driver)
        self.live_view_tab = factory.get_live_view_page_object(_webdriver=self.web_driver)
        self.programs_tab = factory.get_programs_page_object(_webdriver=self.web_driver)

        # Device page Objects
        self.zones_tab = factory.get_zones_page_object(_webdriver=self.web_driver)
        self.moisture_sensor_tab = factory.get_moisture_sensors_page_object(_webdriver=self.web_driver)
        self.master_valve_tab = factory.get_master_valves_page_object(_webdriver=self.web_driver)
        self.flow_meter_tab = factory.get_flow_meter_page_object(_webdriver=self.web_driver)
        self.temperature_sensor_tab = factory.get_temperature_sensors_page_object(_webdriver=self.web_driver)
        self.event_switch_tab = factory.get_event_switch_page_object(_webdriver=self.web_driver)

        # 3200 specific tabs
        self.poc_tab = factory.get_3200_poc_tab(_webdriver=self.web_driver)
        self.mainlines_tab = factory.get_3200_mainlines_tab(_webdriver=self.web_driver)
        print "----"
        print "Successfully finished Step 1 in BaseManager 3200 Setup Devices Test"
        print "-> Loaded devices onto controller"
        print "----"

    def step_2(self):
        """
        Initialize the controller:
        1. Clear programming
        2. Load devices
        3. Do Search for all devices
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        # Clear programming
        self.config.controllers[1].init_cn()

        # Verify controller is connected to BaseManger
        self.config.basemanager_connection[1].verify_ip_address_state()

        # Load all devices
        self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                     mv_d1_list=self.config.mv_d1,
                                                     d2_list=self.config.d2,
                                                     mv_d2_list=self.config.mv_d2,
                                                     d4_list=self.config.d4,
                                                     dd_list=self.config.dd,
                                                     ms_list=self.config.ms,
                                                     fm_list=self.config.fm,
                                                     ts_list=self.config.ts,
                                                     sw_list=self.config.sw)

        # Address Zones
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
        self.config.controllers[1].set_address_for_objects(object_dict=self.config.zones,
                                                           ad_range=self.config.zn_ad_range)

        # Address Master Valves
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
        self.config.controllers[1].set_address_for_objects(object_dict=self.config.master_valves,
                                                           ad_range=self.config.mv_ad_range)

        # Address Moisture Sensors
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
        # self.config.controllers[1].set_address_for_objects(object_dict=self.config.moisture_sensors,
        #                                                    ad_range=self.config.ms_ad_range)

        # Address Temperature Sensors
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.temperature_sensor)
        # self.config.controllers[1].set_address_for_objects(object_dict=self.config.temperature_sensors,
                                                           # ad_range=self.config.ts_ad_range)

        # Address Event Switches
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.event_switch)
        # self.config.controllers[1].set_address_for_objects(object_dict=self.config.event_switches,
        #                                                    ad_range=self.config.sw_ad_range)

        # Address Flow Meters
        self.config.controllers[1].do_search_for_dv(dv_type=opcodes.flow_meter)
        # self.config.controllers[1].set_address_for_objects(object_dict=self.config.flow_meters,
        #                                                    ad_range=self.config.fm_ad_range)
        print "----"
        print "Successfully finished Step 2 in BaseManager 3200 Setup Devices Test"
        print "-> Loaded devices onto controller"
        print "----"

    def step_3(self, _browser):
        """
        Initializes the web driver to the correct browser executor
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        # Open browser
        self.config.resource_handler.web_driver.open(_browser)
        print "----"
        print "Successfully finished Step 3 in BaseManager 3200 Setup Devices Test"
        print "-> Opened Web Browser"
        print "----"

    def step_4(self):
        """
        """
        # enter the user name and password selects login button
        self.login_page.enter_login_info()
        self.login_page.click_login_button()
        self.main_page.verify_open()
        print "----"
        print "Successfully finished Step 4 in BaseManager 3200 Setup Devices Test"
        print "-> Logged In"
        print "----"

    def step_5(self):
        """
        - Selects a site and controller
        - Verifies QuickView Tab is opened
        :return:
        :rtype:
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        self.main_page.select_main_menu()
        self.main_menu.select_site()
        self.main_menu.select_a_controller(mac_address=self.config.mac)
        self.main_menu.wait_for_main_menu_close()

        # selecting a controller returns you to the quick view tab
        self.quick_view_tab.verify_open()
        print "----"
        print "Successfully finished Step 5 in BaseManager 3200 Setup Devices Test"
        print "-> Selected Site and Controller"
        print "----"

    def step_6(self):
        """
        - Selects Zones Tab
        - Do search for zones
        - Assign each zone to a serial number and give description
        - Save and verify changes are saved
        :return:
        :rtype:
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        self.main_page.select_devices_tab(device_type=opcodes.zone)
        self.zones_tab.verify_is_open()
        self.zones_tab.select_assign_bicoders()

        for each_zone_address in sorted(self.config.zones.keys()):
            # self.zones_tab.set_assigned_serial_number(address=each_zone_address)
            self.zones_tab.set_description(address=each_zone_address)

        self.zones_tab.save_device_assignment()

        for each_zone_address in sorted(self.config.zones.keys()):
            self.zones_tab.verify_address(address=each_zone_address)
            self.zones_tab.verify_description(address=each_zone_address)
        print "----"
        print "Successfully finished Step 6 in BaseManager 3200 Setup Devices Test"
        print "-> Searched, Addressed and Described All Zones and Verified changes"
        print "----"

    def step_7(self):
        """
        - Selects Moisture Sensor Tab
        - Do search for Moisture Sensor
        - Assign each Moisture Sensor to a serial number and give description
        - Save and verify changes are saved
        :return:
        :rtype:
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        self.main_page.select_devices_tab(device_type=opcodes.moisture_sensor)
        self.moisture_sensor_tab.verify_is_open()
        self.moisture_sensor_tab.select_assign_bicoders()

        for each_ms_address in sorted(self.config.moisture_sensors.keys()):
            self.moisture_sensor_tab.set_description(address=each_ms_address)

        self.moisture_sensor_tab.save_device_assignment()

        for each_ms_address in sorted(self.config.moisture_sensors.keys()):
            self.moisture_sensor_tab.verify_address(address=each_ms_address)
            self.moisture_sensor_tab.verify_description(address=each_ms_address)
        print "----"
        print "Successfully finished Step 7 in BaseManager 3200 Setup Devices Test"
        print "-> Searched, Addressed and Described All Moisture Sensors and Verified changes"
        print "----"

    def step_8(self):
        """
        - Selects Flow Meter Tab
        - Do search for Flow Meter
        - Assign each Flow Meter to a serial number and give description
        - Save and verify changes are saved
        :return:
        :rtype:
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        self.main_page.select_devices_tab(device_type=opcodes.flow_meter)
        self.flow_meter_tab.verify_is_open()
        self.flow_meter_tab.select_assign_bicoders()

        for each_fm_address in sorted(self.config.flow_meters.keys()):
            self.flow_meter_tab.set_description(address=each_fm_address)

        self.flow_meter_tab.save_device_assignment()

        for each_fm_address in sorted(self.config.flow_meters.keys()):
            self.flow_meter_tab.verify_address(address=each_fm_address)
            self.flow_meter_tab.verify_description(address=each_fm_address)
        print "----"
        print "Successfully finished Step 8 in BaseManager 3200 Setup Devices Test"
        print "-> Searched, Addressed and Described All Flow Meters and Verified changes"
        print "----"

    def step_9(self):
        """
        - Selects Master Valve Tab
        - Do search for Master Valve
        - Assign each Master Valve to a serial number and give description
        - Save and verify changes are saved
        :return:
        :rtype:
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        self.main_page.select_devices_tab(device_type=opcodes.master_valve)
        self.master_valve_tab.verify_is_open()
        self.master_valve_tab.select_assign_bicoders()

        for each_mv_address in sorted(self.config.master_valves.keys()):
            # self.master_valve_tab.set_assigned_serial_number(address=each_mv_address)
            self.master_valve_tab.set_description(address=each_mv_address)

        self.master_valve_tab.save_device_assignment()

        for each_mv_address in sorted(self.config.master_valves.keys()):
            self.master_valve_tab.verify_address(address=each_mv_address)
            self.master_valve_tab.verify_description(address=each_mv_address)
        print "----"
        print "Successfully finished Step 9 in BaseManager 3200 Setup Devices Test"
        print "-> Searched, Addressed and Described All Master Valves and Verified changes"
        print "----"

    def step_10(self):
        """
        - Selects Temperature Sensor Tab
        - Do search for Temperature Sensor
        - Assign each Temperature Sensor to a serial number and give description
        - Save and verify changes are saved
        :return:
        :rtype:
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        self.main_page.select_devices_tab(device_type=opcodes.temperature_sensor)
        self.temperature_sensor_tab.verify_is_open()
        self.temperature_sensor_tab.select_assign_bicoders()

        for each_ts_address in sorted(self.config.temperature_sensors.keys()):
            self.temperature_sensor_tab.set_description(address=each_ts_address)

        self.temperature_sensor_tab.save_device_assignment()

        for each_ts_address in sorted(self.config.temperature_sensors.keys()):
            self.temperature_sensor_tab.verify_address(address=each_ts_address)
            self.temperature_sensor_tab.verify_description(address=each_ts_address)
        print "----"
        print "Successfully finished Step 10 in BaseManager 3200 Setup Devices Test"
        print "-> Searched, Addressed and Described All Temperature Sensors and Verified changes"
        print "----"

    def step_11(self):
        """
        - Selects Event Switch Tab
        - Do search for Event Switch
        - Assign each Event Switch to a serial number and give description
        - Save and verify changes are saved
        :return:
        :rtype:
        """
        method = "\n ########################   Running " + sys._getframe().f_code.co_name + "   ######################"
        print method

        self.main_page.select_devices_tab(device_type=opcodes.event_switch)
        self.event_switch_tab.verify_is_open()
        self.event_switch_tab.select_assign_bicoders()

        for each_sw_address in sorted(self.config.event_switches.keys()):
            self.event_switch_tab.set_description(address=each_sw_address)

        self.event_switch_tab.save_device_assignment()

        for each_sw_address in sorted(self.config.event_switches.keys()):
            self.event_switch_tab.verify_address(address=each_sw_address)
            self.event_switch_tab.verify_description(address=each_sw_address)
        print "----"
        print "Successfully finished Step 11 in BaseManager 3200 Setup Devices Test"
        print "-> Searched, Addressed and Described All Event Switches and Verified changes"
        print "----"