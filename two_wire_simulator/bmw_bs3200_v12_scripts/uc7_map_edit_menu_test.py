__author__ = 'ben'

import sys
import datetime

from datetime import datetime, timedelta
import old_32_10_sb_objects_dec_29_2017.common.configuration as test_conf
import old_32_10_sb_objects_dec_29_2017.page_factory as factory
from old_32_10_sb_objects_dec_29_2017.common import helper_methods

# import log_handler for logging functionality
from old_32_10_sb_objects_dec_29_2017.common.logging_handler import log_handler


class BaseManagerUseCase7(object):
    """
    Purpose:
        This test verifies Maps Site View Edit Menu options displayed.

    Coverage:
        - This test verifies fix of JIRA Bug: BM-1924 (https://baseline.atlassian.net/browse/BM-1924)

    Bugs covered by test:
        (1) BM-1924

    :type maps_tab: old_32_10_sb_objects_dec_29_2017.common.objects.basemanager.maps_tab.MapsTab
    """
    
    # ~~~~ Local Variables ~~~~ #
    
    def __init__(self, controller_type, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param controller_type:                 Type of controller, "32"=3200, "10"=1000 \n
        :type controller_type:                  str \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = test_conf.Configuration(cn_type=controller_type,
                                              test_name=test_name,
                                              user_conf_file=user_configuration_instance,
                                              data_json_file=json_configuration_file)
        
        # Get web driver instance from config
        self.web_driver = None
        
        # Create page instances to use for Client use
        self.main_menu = None
        
        # Main Tab Page Objects
        self.login_page = None
        self.main_page = None
        
        # 3200 specific tabs
        self.maps_tab = None

        # run UC
        self.run_use_case()
    
    def run_use_case(self):
        """
        The following steps are done on all browser specified in the user configuration: \n

        Step 1:
            - initialize & open browser
        step 2:
            - login & verify successful logged in
        Step 3:
            - select site
            - select controller
            - verify QuickView tab opens
        Step 4:
            - select Maps
            - verify it opened
        Step 5:
            - Select Quick View tab
            - Verify it opened
        Step 6:
            - Select Programs Tab
            - Verify it opened
        step 7:
            - Verify all device tabs:
                - select Zones & verify it opens
                - select Moisture Sensors & verify it opens
                - select Flow Meters & verify it opens
                - select Temperature Sensors & verify it opens
                - select Master Valves & verify it opens
                - select Event Switch & verify it opens
        step 8:
            - Verify all Water Sources sub tabs:
                - select Point of Connection tab
                    - verify it opened
                - select Mainlines
                    - verify it opened
        step 9:
            - select LiveView
            - verify it opens

        Not Covered:
            - Selecting the different map views and verifying each view opens
            - Selecting dialog boxes in each tab to verify they work/open to the correct destination
        """
        time_test_started = datetime.now()
        time_it_should_take_test_to_run = timedelta(minutes=2)  # this is minutes
        try:
            # browsers_for_test = ["chrome", "firefox"]
            browsers_for_test = ["chrome"]
            for run_number, browser in enumerate(browsers_for_test):
                
                # Resets objects to a known state, creates serial connections, creates all objects.
                self.config.initialize_for_test()
                
                self.step_1()
                self.step_2()
                self.step_3(_browser=browser)
                self.step_4()
                self.step_5()
                self.step_6()
                
                # Close the first webdriver to start the second web driver
                if run_number == 0 and len(browsers_for_test) > 1:
                    self.config.resource_handler.web_driver.web_driver.quit()
            
            helper_methods.test_elapsed_time(_time_started=time_test_started,
                                             _time_expected_to_run=time_it_should_take_test_to_run)
        except Exception as e:
            helper_methods.print_test_failed(test_name=self.config.test_name)
            # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
            # to the next use case in the list
            if log_handler.is_enabled():
                log_handler.exception(message=e.message)
            else:
                raise
        
        else:
            helper_methods.print_test_passed(self.config.test_name)
        
        finally:
            # We close the first webdriver and not the second because of this line, which closes the webdriver
            helper_methods.end_controller_test(self.config)
    
    #################################
    def step_1(self):
        """
        Initialize controller
        """
        helper_methods.print_method_name()
        try:
            # Init controller and connect to BM
            self.config.controllers[1].init_cn()
            self.config.basemanager_connection[1].verify_ip_address_state()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     e.msg if e.msg and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Initialized controller and connected to BaseManager"
            )
    
    #################################
    def step_2(self):
        """
        Initializes all pages needed for test
        """
        helper_methods.print_method_name()
        try:
            # Get web driver instance from config
            self.web_driver = self.config.resource_handler.web_driver
            
            # Create page instances to use for Client use
            self.main_menu = factory.get_main_menu_object(_webdriver=self.web_driver)
            
            # Main Tab Page Objects
            self.login_page = factory.get_login_page_object(_webdriver=self.web_driver)
            self.main_page = factory.get_main_page_object(_webdriver=self.web_driver)
            
            # 3200 specific tabs
            self.maps_tab = factory.get_maps_page_object(_webdriver=self.web_driver)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     e.msg if e.msg and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Initialized web pages"
            )
    
    #################################
    def step_3(self, _browser):
        """
        Initializes the web driver to the correct browser executor
        """
        helper_methods.print_method_name()
        try:
            # Open browser
            self.config.resource_handler.web_driver.open(_browser)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     e.msg if e.msg and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Opened Web Browser"
            )
    
    #################################
    def step_4(self):
        """
        """
        helper_methods.print_method_name()
        try:
            # enter the user name and password selects login button
            self.login_page.enter_login_info()
            self.login_page.click_login_button()
            self.main_page.verify_open()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     e.msg if e.msg and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Logged in to client"
            )
    
    #################################
    def step_5(self):
        """
        - Selects a site and controller
        - Verifies QuickView Tab is opened
        :return:
        :rtype:
        """
        helper_methods.print_method_name()
        try:
            self.main_page.select_main_menu()
            self.main_menu.select_site()
            self.main_menu.select_a_controller(mac_address=self.config.mac)
            self.main_menu.wait_for_main_menu_close()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     e.msg if e.msg and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Selected Site and Controller"
            )
    
    #################################
    def step_6(self):
        """
        Verify JIRA Bugs
        """
        helper_methods.print_method_name()
        try:
            self.main_page.select_maps_tab(map_view="site")
            self.maps_tab.verify_open()
            self.maps_tab.open_edit_menu()
            self.maps_tab.select_markers_menu_option()
            self.maps_tab.verify_flowstation_markers_are_hidden_jira_bm_1924()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     e.msg if e.msg and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Verified JIRA Bugs (BM-1924) for Maps > Site View"
            )
