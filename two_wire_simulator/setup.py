from distutils.core import setup

setup(
    name='automated_testing_2015',
    version='1.0',
    packages=['common', 'common.imports', 'common.objects', 'common.objects.baseunit', 'common.objects.controller',
              'common.objects.basemanager', 'common.objects.base_classes', 'common.objects.mobile_access',
              'common.variables', 'common.unit_tests', 'common.unit_tests.objects', 'common.user_credentials',
              'bmw_scripts', 'MFG_scripts', 'bs1000_scripts', 'bs3200_scripts'],
    url='',
    license='',
    author='Baseline',
    author_email='',
    description=''
)
