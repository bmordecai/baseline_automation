import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration

# Objects
from common.objects.base_classes.web_driver import *

# import log_handler for logging functionality
from common.logging_handler import log_handler

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr

from common import helper_methods

__author__ = 'Kent'


class ControllerUseCase164(object):
    """
    Test name:
        - FS UseCase164 POC dynamic flow allocation with a FlowStation

    Problem statement
        -  Only the POC's needed to supply water to a running zone should be turned on. All other POC's connected
        to a running mainline should be turned off.
        - A Flow Station should optimize and turn on shared mainlines to provide water to running zones. Only those
        POC's needed to provide water to a running mainline should turned on.

    User Story
        - As a user, I want a 3200 to only turn on a Point of Control when it is needed. If a Point of
        Control is connected to a watering Mainline, but it is not needed to water, it should not turn on.
        My 3200 should be able to determine if a Point of Control is needed based on the flow requirements
        of the zones running in the program.(3200 alone is covered in 3200 Use Case 60 at a basic level)
        - As a user, I want my Flow Station to dynamically turn on a shared POC based on the water needs of
        the zones currently running in the irrigation system. If there are multiple zones running, and the water need
        exceeds the capacity of the POC of a single controller, the Flow Station should turn on
        other shared POC's to meet the water needs of the running zones.(Tested in this use case, but only with two
        shared POC's, and one shared mainline on one 3200)
        - As a user, I want my FlowStation to provide water based on the priority of a Water Source. A water source of
        a lower priority will not be used up until water sources of a higher priority are empty. (This functionality
        will be tested in a later use case)
        - As a user, I want the flow provided by the POC's supplying a mainline to not exceed the target flow
        of the watering mainlines.
        - As a user, I want the controller to optimize the water capacity of the available POC's supplying water to the
         running zones by turning on the least number of POC's needed to meet the watering demand of the running zones.

    Purpose:
        - Test Dynamic flow using shared water sources, poc's and one mainline.

    Coverage area:
        - When shared with a Flow Station, all POCs run when dynamic flow allocation is off on a mainline
        - When shared with a Flow Station, and dynamic flow allocation is on, POCs are turned on only when needed
         as flow demand increases on a mainline.


    Areas not covered:
        - Different priorities on shared water sources and then use Dynamic Flow
        - Sharing of multiple poc's on multiple controllers and using Dynamic Flow
        - Sharing a max number (20) of POC's with a Flow Station and using Dynamic Flow


    Test Configuration setup: \n
        1. controller:
            - concurrent zones = 10

        2. poc 1:
            - 20gpm

        2. poc 2:
            - 100gpm

        2. mainline 1:
            - 200gpm
            - concurrent zones = yes

        2. program 1 & 2:
            - 8 am start time
            - concurrent zones = 1

        1. zones 1 on program 1
            - 10 min runtime
            - 15pgm design flow
            - assigned to mainline 1

        1. zones 2 on program 2
            - 10 min runtime
            - 95pgm design flow
            - assigned to mainline 1

        - Controller and Flow Station Setup:

            Controller              FlowStation
            - WS1                   - WS1
            - POC1                  - POC1
            - ML1                   - ML1

            - WS2                   - WS2
            - POC2                  - POC2

            - Flow Station Connectivity
            - WS1 -> POC1 -> ML1
            - WS2 -> POC2 -> ML1

    Test Steps - one case to test:
        1. Test 1 without dynamic flow
            - start program 1
                - verify only zone 1 is running on the 3200 and Flow Station
                - verify POC 1 and 2 are running on the 3200 and Flow Station
            - stop program 1
            - start program 2
                - verify only zone 2 is running on the 3200 and Flow Station
                - verify POC 1 and 2 are running on the 3200 and Flow Station
            - stop program 2
            - start program 1 and 2
                - verify zone 1 and 2 are running on the 3200 and Flow Station
                - verify POC 1 and 2 are running on the 3200 and Flow Station
            - stop both programs
        2. Test 2 with dynamic flow
            - start program 1
                - verify only zone 1 is running on the 3200 and Flow Station
                - verify only POC 1 is running on the 3200 and Flow Station
            - stop program 1
            - start program 2
                - verify only zone 2 is running on the 3200 and Flow Station
                - verify only POC 2 is running on the 3200 and Flow Station
            - start program 1
                - verify zone 1 and 2 are running on the 3200 and Flow Station
                - verify POC 1 and 2 are running on the 3200 and Flow Station
            - stop both programs

    """

    ###############################
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase44' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/fs_json_config_files')
        self.run_use_case()

    ###############################
    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=False)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break

                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        setup controller
        ############################
        1. controller:
            - concurrent zones = 10
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=10)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_2(self):
        """
        ############################
        setup programs
        ############################

        1. water sources 1 and 2:
            - assigned to POC 1 and 2 respectively

        2. poc 1:
            - Target Flow: 20gpm

        3. poc 2:
            - Target Flow: 100gpm

        4. mainline 1:
            - 200gpm
            - concurrent zones = yes

        5. program 1 & 2:
            - 8 am start time
            - concurrent zones = 1
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Program 1
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=1)

            # Program 2
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation3200[1].programs[2].set_enabled()
            self.config.BaseStation3200[1].programs[2].set_max_concurrent_zones(_number_of_zones=1)

            # Mainline 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].get_mainline(1).set_target_flow(_gpm=200)
            self.config.BaseStation3200[1].get_mainline(1).set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].get_mainline(1).add_zone_to_mainline(_zone_address=1)
            self.config.BaseStation3200[1].get_mainline(1).add_zone_to_mainline(_zone_address=2)

            # POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].get_point_of_control(1).set_target_flow(_gpm=20)
            self.config.BaseStation3200[1].get_point_of_control(1).add_mainline_to_point_of_control(_mainline_address=1)

            # POC 2
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=2)
            self.config.BaseStation3200[1].get_point_of_control(2).set_target_flow(_gpm=100)
            self.config.BaseStation3200[1].get_point_of_control(2).add_mainline_to_point_of_control(_mainline_address=1)

            # Water Sources
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=2)
            self.config.BaseStation3200[1].get_water_source(1).add_point_of_control_to_water_source(_point_of_control_address=1)
            self.config.BaseStation3200[1].get_water_source(2).add_point_of_control_to_water_source(_point_of_control_address=2)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_3(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Setup zone 1
            self.config.BaseStation3200[1].zones[1].set_design_flow(15)
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=10)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=10)

            # Setup zone 2
            self.config.BaseStation3200[1].zones[2].set_design_flow(95)
            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[2].zone_programs[2].set_run_time(_minutes=10)
            self.config.BaseStation3200[1].programs[2].zone_programs[2].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[2].zone_programs[2].set_soak_time(_minutes=10)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ###############################
        verify the entire configuration
        ###############################

        - Get information for each object from controller
        - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.BaseStation3200[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ###############################
        Share controller, water source, point of control, mainline with Flow Station
        ###############################
            - Share controller 1 with FlowStation 1

            - Share Water Source 1 on controller 1 with FlowStation 1
            - Share Point of Control 1 on controller 1 with FlowStation 1
            - Share Mainline 1 on controller 1 with FlowStation 1

            - Share Water Source 2 on controller 1 with FlowStation 1
            - Share Point of Control 2 on controller 1 with FlowStation 1

            - Assign WS, POC, and ML on the FlowStation
            - WS1 -> PC1 -> ML1
            - WS2 -> PC2 -> ML1
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            #################################
            # Add controller to FlowStation #
            #################################

            self.config.FlowStations[1].add_controller_to_flow_station(
                _controller_address=1,
                _flow_station_slot_number=1)

            ####################################
            # Add Water Sources to FlowStation #
            ####################################

            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=1,
                _controller_water_source_address=1,
                _flow_station_water_source_slot_number=1)

            # Tell the 3200 that the FlowStation is in control of Water Source 1.
            self.config.BaseStation3200[1].get_water_source(1).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=1,
                _controller_water_source_address=2,
                _flow_station_water_source_slot_number=2)

            # Tell the 3200 that the FlowStation is in control of Water Source 1.
            self.config.BaseStation3200[1].get_water_source(2).set_manage_by_flowstation()

            ########################################
            # Add Points of Control to FlowStation #
            ########################################

            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=1,
                _controller_point_of_control_address=1,
                _flow_station_point_of_control_slot_number=1)

            # Tell the 3200 that the FlowStation is in control of Point of Control 1.
            self.config.BaseStation3200[1].get_point_of_control(1).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=1,
                _controller_point_of_control_address=2,
                _flow_station_point_of_control_slot_number=2)

            # Tell the 3200 that the FlowStation is in control of Point of Control 2.
            self.config.BaseStation3200[1].get_point_of_control(2).set_manage_by_flowstation()

            ################################
            # Add Mainlines to FlowStation #
            ################################

            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=1,
                _controller_mainline_address=1,
                _flow_station_mainline_slot_number=1)

            # Tell the 3200 that the FlowStation is in control of Mainline 1.
            self.config.BaseStation3200[1].get_mainline(1).set_manage_by_flowstation()

            # Assign WS, POC, and ML on FlowStation
            # WS1 -> PC1 -> ML1
            self.config.FlowStations[1].get_point_of_control(pc_number=1).add_mainline_to_point_of_control(_mainline_address=1)
            self.config.FlowStations[1].get_water_source(ws_number=1).add_point_of_control_to_water_source(_point_of_control_address=1)

            # WS2 -> PC2 -> ML1
            self.config.FlowStations[1].get_point_of_control(pc_number=2).add_mainline_to_point_of_control(_mainline_address=1)
            self.config.FlowStations[1].get_water_source(ws_number=2).add_point_of_control_to_water_source(_point_of_control_address=2)

            # Explicitly disable dynamic flow on the FlowStation
            self.config.FlowStations[1].set_dynamic_flow_allocation_to_false()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ###############################
        verify the entire configuration
        ###############################

        - Get information for each object from controller
        - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.FlowStations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ###############################
        Test 1 without dynamic flow
        ###############################
        All POC's connected to a mainline with running zones should be turned on to meet the watering needs of the
        running zones
        1. Test 1 without dynamic flow
            - start program 1
                - verify only zone 1 is running on the 3200 and Flow Station
                - verify POC 1 and 2 are running on the 3200 and Flow Station
            - stop program 1
            - start program 2
                - verify only zone 2 is running on the 3200 and Flow Station
                - verify POC 1 and 2 are running on the 3200 and Flow Station
            - stop program 2
            - start program 1 and 2
                - verify zone 1 and 2 are running on the 3200 and Flow Station
                - verify POC 1 and 2 are running on the 3200 and Flow Station
            - stop both programs
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set time to 1:00 am
            self.config.BaseStation3200[1].set_date_and_time(_date='1/31/2018', _time='01:00:00')
            self.config.FlowStations[1].set_date_and_time(_date='01/31/2018', _time='01:00:00')

            # =====================================================================
            # start program 1
            self.config.BaseStation3200[1].programs[1].set_program_to_start()
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=2)

            # verify status
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            # Both POC's should be running since Dynamic Flow is turned off even though
            # the watering need of the running zones doesn't exceed that of POC 1
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()

            # stop program 1
            self.config.BaseStation3200[1].programs[1].set_program_to_stop()

            # =====================================================================
            # start program 2
            self.config.BaseStation3200[1].programs[2].set_program_to_start()
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=2)

            # verify status
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            # Both POC's should be running since Dynamic Flow is turned off even though
            # the watering need of the running zones don't exceed that of POC 2
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()

            # =====================================================================
            # start program 1
            self.config.BaseStation3200[1].programs[1].set_program_to_start()
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=2)

            # verify status
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            # Both POC's should be running. The Flow needed doesn't exceed the GPM needed by 1 and 2
            # together
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()

            # clean up
            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=2)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        Test 2 -  Test 2 with dynamic flow
        ###############################
        Only POC's needed to meet watering needs of running zones should be turned on
        2. Test 2 with dynamic flow
            - start program 1
                - verify only zone 1 is running on the 3200 and Flow Station
                - verify only POC 1 is running on the 3200 and Flow Station
            - stop program 1
            - start program 2
                - verify only zone 2 is running on the 3200 and Flow Station
                - verify only POC 2 is running on the 3200 and Flow Station
            - start program 1
                - verify zone 1 and 2 are running on the 3200 and Flow Station
                - verify POC 1 and 2 are running on the 3200 and Flow Station
            - stop both programs
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set time to 1:00 am
            self.config.BaseStation3200[1].set_date_and_time(_date='1/31/2018', _time='01:00:00')
            self.config.FlowStations[1].set_date_and_time(_date='01/31/2018', _time='01:00:00')

            # Set dynamic flow allocation to true on mainline 1
            self.config.FlowStations[1].set_dynamic_flow_allocation_to_true()
            # self.config.FlowStations[1].get_mainline(1).set_dynamic_flow_allocation_to_true()

            # =====================================================================
            # start program 1
            self.config.BaseStation3200[1].programs[1].set_program_to_start()
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=2)

            # verify status
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            # Only POC 1 should be running since Dynamic Flow is turned on and the watering need of the running zones do
            # not exceed the Target Flow of POC 1
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_off()

            # stop program 1
            self.config.BaseStation3200[1].programs[1].set_program_to_stop()

            # =====================================================================
            # start program 2
            self.config.BaseStation3200[1].programs[2].set_program_to_start()
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=2)

            # verify status
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            # Both POC 1 and POC 2 should be running since Dynamic Flow is turned on and the watering need of the
            # running zones exceeds the Target Flow of POC 1 so the additional POC is turned on
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()

            # =====================================================================
            # start program 1
            self.config.BaseStation3200[1].programs[1].set_program_to_start()
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=2)

            # verify status
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            # Only POC 1 and POC 2 should be running since the watering need of zones 1 and 2 exceed the Target Flow
            # of POC 1
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()

            # clean up
            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        ###############################
        verify the entire configuration
        ###############################

        - Get information for each object from controller
        - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.FlowStations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]