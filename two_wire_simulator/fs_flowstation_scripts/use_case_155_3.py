import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration

# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr

__author__ = 'Ben'


class ControllerUseCase155_3(object):
    """
    [CONVERTED FROM v16 BS-3200 UC_5_3]
    
    Test name:
        - Using multiple water source budgets to a single point mainline with priorities

    User Story: \n

        1)  As a user I want to use my least expensive water first. If they run out of that water they want to be
            able to have the system switch the water source for them. In some cases they want the water source with
            the least expensive water to shut off and in other cases they want that water source to shut off and the
            next water source to turn on. They also want to be able to decide when the cheep water runs out based on a
            known used amount or water tier pricing is met that the system will notify them and switch over the
            more expensive water \n

    Coverage area of feature: \n
        1.	When a mainline is receiving water from multiple water sources with different priorities:
            a.	Highest prioritized water source(s) (that have available water) will be used first until all available
                water is used, i.e., reached monthly budget.
            b.	All other water sources that don't have the highest priority will not be used for water allocation
                and will not run until the higher prioritized water sources run out of available water.
            c.	When highest prioritized water source(s) run out of water, system will continue watering by allocating
                water from the other available water sources.

   Use Case explanation:
        - Two controllers managed by a single FlowStation
        - Two water source, two POCs, and single mainline with 3 zones.

        First Scenario:
            Controller 1:
                - WS1, no budget (don't care)
                    - Priority 2
                - POC1, DF 100 GPM
                - ML1 (default priority), DF 100 GPM
                - 3 zones on ML1, all DF 30GPM
                - Zone 1 on program 1 with a runtime of 5 minutes (primary)
                - Zones 2,3 on program 1 with a runtime of 5 minutes (linked to 1)
                - Program 1 start time 8:00am
                - Zones are attached to ML1
            Controller 2:
                - WS2, budget 270 Gallons, shutdown enabled
                    - Priority 1
                - POC2, DF 100 GPM
            FlowStation:
                - WS/PC/ML Assignments:
                    C1:W1       C2:W2
                      |           |
                    C1:P1       C2:P2
                        \       /
                          C1:M1

    Not Covered:
        1.	Dynamic Flow Allocation Enabled
        2.	Single 3200 managed by FlowStation
        3.	High flows & shutdowns
        4.	Unscheduled flows & shutdowns
        5.	Variances & shutdowns
        6.	Empty conditions

    Date References:
        - configuration for script is located common\configuration_files\using_real_time_flow.json
        - the devices and addresses range is read from the .json file


    """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/fs_json_config_files'
                                    )
        self.run_use_case()

    #################################
    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=False)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        ############################
        setup controller concurrency
        ############################
        We want the concurrency to not be a limiting factor in the test.

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=15)
            self.config.BaseStation3200[2].set_max_concurrent_zones(_max_zones=15)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        Set concurrency so it is not a limiting factor in the test
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=15)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=[480])

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        The primary zone is used to verify the controller requests the correct minimum amount of water to run the primary
        zone first.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add zone programs to Program 1
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_as_linked_zone(_primary_zone=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
        No budgets are used for the first water source because it has a lower priority and will be used once the second
        water source is used up
        Budget is set on the second water source, with a higher priority so that it is used first and so that we can
        make it run out of water.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Water Source 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].get_water_source(1).set_enabled()
            self.config.BaseStation3200[1].get_water_source(1).set_priority(_priority_for_water_source=2)

            # Water Source 2
            self.config.BaseStation3200[2].add_water_source_to_controller(_water_source_address=2)
            self.config.BaseStation3200[2].get_water_source(2).set_enabled()
            self.config.BaseStation3200[2].get_water_source(2).set_priority(_priority_for_water_source=1)
            self.config.BaseStation3200[2].get_water_source(2).set_monthly_watering_budget(_budget=270, _with_shutdown_enabled=True)


        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n

        We are not setting any limiting factors (shutdowns, priorities, etc.).
        The design flow also should not be a limiting factor for water allocation so we set it to a value greater than
        its mainline
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].get_point_of_control(1).set_enabled()
            self.config.BaseStation3200[1].get_point_of_control(1).set_target_flow(_gpm=100)

            # POC 2
            self.config.BaseStation3200[2].add_point_of_control_to_controller(_point_of_control_address=2)
            self.config.BaseStation3200[2].get_point_of_control(2).set_enabled()
            self.config.BaseStation3200[2].get_point_of_control(2).set_target_flow(_gpm=100)
            self.config.BaseStation3200[2].get_point_of_control(2).add_flow_meter_to_point_of_control(_flow_meter_address=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control

        Set mainline design flows so it will not be the limiting factor.
        We assume that if the mainline is managed by the FlowStation it will automatically limit concurrency by flow.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Mainline 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].get_mainline(1).set_enabled()
            self.config.BaseStation3200[1].get_mainline(1).set_target_flow(_gpm=100)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone
        Set design flow such that when they are all running they do not hit the limit of the upstream POC's and mainlines
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add Zones 1-3 to Mainline 1
            for zone_address in range(1, 4):
                self.config.BaseStation3200[1].get_mainline(1).add_zone_to_mainline(_zone_address=zone_address)

            self.config.BaseStation3200[1].zones[1].set_design_flow(_gallons_per_minute=30)
            self.config.BaseStation3200[1].zones[2].set_design_flow(_gallons_per_minute=30)
            self.config.BaseStation3200[1].zones[3].set_design_flow(_gallons_per_minute=30)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        Share controller, water source, point of control, mainline with Flow Station
        ###############################
            - Share controller1 with the flow station
            - Add and share a Water Source between controller1 and the FlowStation
            - Add and share Point of Control between controller1 and the FlowStation
            - Add and share Mainline between controller1 and the FlowStation

            - Share controller2 with the flow station
            - Add and share a Water Source between controller2 and the FlowStation
            - Add and share Point of Control between controller2 and the FlowStation
            - Add and share Mainline between controller2 and the FlowStation

            - Assign WS, POC, and ML on the FlowStation
            - WS/PC/ML Assignments:
                    C1:W1       C2:W2
                      |           |
                    C1:P1       C2:P2
                        \       /
                          C1:M1
            -By assigning the controller WS, POC, and MLs to the FlowStation we automatically setting the managed by
            FlowStation setting for each.
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add controller 1 to FlowStation
            self.config.FlowStations[1].add_controller_to_flow_station(
                _controller_address=1,
                _flow_station_slot_number=1)

            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=1,
                _controller_water_source_address=1,
                _flow_station_water_source_slot_number=1)

            self.config.BaseStation3200[1].water_sources[1].set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=1,
                _controller_point_of_control_address=1,
                _flow_station_point_of_control_slot_number=1)

            self.config.BaseStation3200[1].points_of_control[1].set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=1,
                _controller_mainline_address=1,
                _flow_station_mainline_slot_number=1)

            self.config.BaseStation3200[1].mainlines[1].set_manage_by_flowstation()

            # Add controller 2 to FlowStation
            self.config.FlowStations[1].add_controller_to_flow_station(
                _controller_address=2,
                _flow_station_slot_number=2)

            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=2,
                _controller_water_source_address=2,
                _flow_station_water_source_slot_number=2)

            self.config.BaseStation3200[2].water_sources[2].set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=2,
                _controller_point_of_control_address=2,
                _flow_station_point_of_control_slot_number=2)

            self.config.BaseStation3200[2].points_of_control[2].set_manage_by_flowstation()

            # Assign WS, POC, and ML on FlowStation
            # WS1 -> PC1 -> ML1
            self.config.FlowStations[1].get_water_source(ws_number=1).add_point_of_control_to_water_source(_point_of_control_address=1)
            self.config.FlowStations[1].get_point_of_control(pc_number=1).add_mainline_to_point_of_control(_mainline_address=1)

            # WS2 -> PC2 -> ML1
            self.config.FlowStations[1].get_water_source(ws_number=2).add_point_of_control_to_water_source(_point_of_control_address=2)
            self.config.FlowStations[1].get_point_of_control(pc_number=2).add_mainline_to_point_of_control(_mainline_address=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.BaseStation3200[2].verify_full_configuration()
            self.config.FlowStations[1].verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        Set the flow meter to zero usage this gives us a starting point \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            
            self.config.BaseStation3200[2].flow_meters[1].bicoder.set_water_usage(_water_usage=0)
            self.config.BaseStation3200[2].flow_meters[1].bicoder.self_test_and_update_object_attributes()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
          Set the time to be 7:59, which is 1 minute before both of our program's start times \n
        Verify that nothing is started: \n
            - Verify that no Programs are running \n
            - Verify that no zones are watering \n
        Increment the clock two minutes which should trigger both of our Programs to start \n
        Verify that specific zones have started for each program below: \n
            Controller 1 Program 1:
                - Program starts at 8:01am
                    - Zones will be set to waiting because water needs to be requested from the FlowStation next
                - 8:02am
                    - All Zones will be running because they require less flow than what the upstream ML/POC have available
                    - Since C2W2 has highest priority, it and its downstream POC C2P2 will be supplying downstream water.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_date_and_time(_date='02/12/2017', _time='7:59:00')
            self.config.BaseStation3200[1].verify_date_and_time()

            self.config.BaseStation3200[2].set_date_and_time(_date='02/12/2017', _time='7:59:00')
            self.config.BaseStation3200[2].verify_date_and_time()

            self.config.FlowStations[1].set_date_and_time(_date='02/12/2017', _time='07:59:00')
            self.config.FlowStations[1].verify_date_and_time()

            # all zone have a status of done
            for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_done()

            # programs have a status of done
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()

            # verify water source status
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_ok()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_ok()

            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_ok()
            self.config.FlowStations[1].get_water_source(2).statuses.verify_status_is_ok()
            # verify POC status
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_off()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_off()

            # verify mainline status
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_off()

            # This should trigger the Programs to start at 8:01am
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=2)

            # setting the flow rate on the flow meter 1 to match the flow of the running zones.
            self.config.BaseStation3200[2].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=90)

            # all zone have a status of waiting
            for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_waiting_to_water()

            # programs have a status of waiting
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            # verify water source status
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_ok()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_ok()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(2).statuses.verify_status_is_running()

            # verify POC status
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_off()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()

            # verify mainline status
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Increment to 8:02am
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify zone status
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()

            # Program status
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # verify water source status
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_ok()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_ok()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(2).statuses.verify_status_is_running()

            # verify POC status
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_off()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()

            # verify mainline status
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            fm_rate = self.config.BaseStation3200[2].flow_meters[1].bicoder.vr
            ws2_budget = self.config.BaseStation3200[2].get_water_source(2).wb
            minutes_until_budget_met = ws2_budget / fm_rate

            # Increment to 8:05am
            # This is the point where C2W2 has met its budget, but that condition won't be acted upon until the
            # next minute, so verify system is still watering through the same WS/POC/ML path
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=minutes_until_budget_met)

            # Verify zone status
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()

            # Program status
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # verify water source status
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_over_budget()
            self.config.FlowStations[1].get_water_source(2).statuses.verify_status_is_over_budget()

            # verify POC status
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_off()

            # verify mainline status
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        Incrementing forward to 8:06.  At this point, the C2W2 is over budget, and water is now allocated from
        C1W1 and through C1P1

        Verify that:
            Controller 1 Program 1:
                - Program, zones, and mainline should remain watering
                - C2W2 will be over budget with a message, and C2P2 will be off
                - C1W1 will be supplying water through C1P1

        This verifies that the functionality of the FlowStation switching to a lower-priority water source when the
        budget of the higher priority water source is met and shutdown
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment to 8:06am

            # This is the point where C2W2 has met its budget, but that condition won't be acted upon until the
            # next minute, so verify system is still watering through the same WS/POC/ML path
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify zone status
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()

            # Program status
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # verify water source status
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_over_budget()
            self.config.FlowStations[1].get_water_source(2).statuses.verify_status_is_over_budget()

            # verify POC status
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_off()

            # verify mainline status
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
