import sys
from time import sleep
from datetime import datetime, timedelta
from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

__author__ = 'Eldin'


class ControllerUseCase154(object):
    """
    Test name:
        - CN UseCase154 Basic Design Flow with Flowstation

    User Story:
       The user wants to use a flowstation to manage flow on a 3200 that doesn't have a flowmeter.  The user wants
       to be able to set the design flow on each zone, set the overall flow limit for the upstream mainline(s) and
       POC(s), have the flowstation allocate water as requested by the 3200, and limit the water allocated as necessary
       to keep the allocation under the mainline(s) / POC(s) flow limit(s)

    Purpose:
        - This test is to verify that, using a single flowstation attached to a single 3200, the flowstation can
          allocate flow based on zone design flows and POC / Mainline target flows
        - Set up a zones on the controller and attach them to program
        - Make Zones 1 and 11 primary zones, and attach the rest of the zones to these two zones
        - Set up a design flow for all zones
        - Set up a POC and Mainline to manipulate the zones in a certain way described in comments in each step \n

    Coverage Area: \n
        1. Flowstation is doing the water management
        2. Manage flow using design flows on the zones instead of using flow meter \n
        3. Able to assign flow to zones \n
        4. Able to set target flows on mainlines \n
        5. Run Programs with mainline 1 design flow restriction \n
        5. Run Programs with mainline 1 and mainline 2 design flow restrictions \n

    Use Case explanation:
        Each water source will be assigned a single poc, mainline, and program. \n

        First Scenario:
            10 zones will be assigned to mainline 1 with two being a primary and the other eight being linked \n
            The programs will start at 8:00am
            - The primary zones will be given water first with the linked zones getting the rest

        Second Scenario:
            5 zones will be assigned to each mainline with one being a primary and the other four being linked \n
            The programs will be told to start manually
            - Each set of zones will be limited by the design flow of the assigned mainlines

    Date References:
        - configuration for script is located common\configuration_files\fs_json_config_files\FS_basic_design_flow.json
        - the devices and addresses range is read from the .json file
    """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/fs_json_config_files'
                                    )
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                     # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        ######################
        setup zones on mainlines
        ######################
        - set up zone  Attributes associated with main lines \n
            - set design flow on zone
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].zones[1].set_design_flow(_gallons_per_minute=20)
            self.config.BaseStation3200[1].zones[2].set_design_flow(_gallons_per_minute=20)
            self.config.BaseStation3200[1].zones[3].set_design_flow(_gallons_per_minute=20)
            self.config.BaseStation3200[1].zones[4].set_design_flow(_gallons_per_minute=12)
            self.config.BaseStation3200[1].zones[5].set_design_flow(_gallons_per_minute=7.5)
            
            self.config.BaseStation3200[1].zones[11].set_design_flow(_gallons_per_minute=15)
            self.config.BaseStation3200[1].zones[12].set_design_flow(_gallons_per_minute=15)
            self.config.BaseStation3200[1].zones[13].set_design_flow(_gallons_per_minute=7.5)
            self.config.BaseStation3200[1].zones[14].set_design_flow(_gallons_per_minute=7.5)
            self.config.BaseStation3200[1].zones[15].set_design_flow(_gallons_per_minute=7.5)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=10)

            # Add & Configure Program 1
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=[480])

            # Add & Configure Program 2
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation3200[1].programs[2].set_enabled()
            self.config.BaseStation3200[1].programs[2].set_max_concurrent_zones(_number_of_zones=3)
            self.config.BaseStation3200[1].programs[2].set_start_times(_st_list=[480])
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # -------------------------------------------------------------------------------------------------------- #
            # Program 1 Zone Programs
            # -------------------------------------------------------------------------------------------------------- #
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=60)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=4)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[1].zone_programs[4].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[5].set_as_linked_zone(_primary_zone=1)

            # -------------------------------------------------------------------------------------------------------- #
            # Program 2 Zone Programs
            # -------------------------------------------------------------------------------------------------------- #
            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=11)
            self.config.BaseStation3200[1].programs[2].zone_programs[11].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[2].zone_programs[11].set_run_time(_minutes=60)
            self.config.BaseStation3200[1].programs[2].zone_programs[11].set_cycle_time(_minutes=2)
            self.config.BaseStation3200[1].programs[2].zone_programs[11].set_soak_time(_minutes=4)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=12)
            self.config.BaseStation3200[1].programs[2].zone_programs[12].set_as_linked_zone(_primary_zone=11)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=13)
            self.config.BaseStation3200[1].programs[2].zone_programs[13].set_as_linked_zone(_primary_zone=11)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=14)
            self.config.BaseStation3200[1].programs[2].zone_programs[14].set_as_linked_zone(_primary_zone=11)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=15)
            self.config.BaseStation3200[1].programs[2].zone_programs[15].set_as_linked_zone(_primary_zone=11)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Water Source 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()

            # Water Source 2
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=2)
            self.config.BaseStation3200[1].water_sources[2].set_enabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=50)

            # Add POC 1 to Water Source 1
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=1)
            # Add & Configure POC 2
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=2)
            self.config.BaseStation3200[1].points_of_control[2].set_enabled()
            self.config.BaseStation3200[1].points_of_control[2].set_target_flow(_gpm=50)

            # Add POC 2 to Water Source 2
            self.config.BaseStation3200[1].water_sources[2].add_point_of_control_to_water_source(
                _point_of_control_address=2)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
            - Add Mainline ---> to point of control
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure ML 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_true()
            # Add ML 1 to POC 1
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)

            # Add & Configure ML 2
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=2)
            self.config.BaseStation3200[1].mainlines[2].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].mainlines[2].set_limit_zones_by_flow_to_true()
            # Add ML 2 to POC 2
            self.config.BaseStation3200[1].points_of_control[2].add_mainline_to_point_of_control(_mainline_address=2)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone

        Program | Mainline  | Zones
        ---------------------------------------------
        1       | 1         | 1,2,3,4,5,11,12,13,14,15
        2       | 1         |
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Add all zones to mainline 1
            for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].get_mainline(1).add_zone_to_mainline(_zone_address=zone_address)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_8(self):
        """
        ###############################
        Share controller, water source, point of control, mainline with Flow Station
        ###############################
            - Share controller 1 with the flow station
            - Add and share two Water Sources between controller 1 and the FlowStation
            - Add and share two Point of Controls between controller 1 and the FlowStation
            - Add and share two Mainlines between controller 1 and the FlowStation

            - Assign WS, POC, and ML on the FlowStation
            - WS1 -> PC8 -> ML1
            - WS8 -> PC1 -> ML8
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add controller 1 to FlowStation
            self.config.FlowStations[1].add_controller_to_flow_station(
                _controller_address=1,
                _flow_station_slot_number=1)

            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=1,
                _controller_water_source_address=1,
                _flow_station_water_source_slot_number=1)

            # Tell the 3200 that the FlowStation is in control of Water Source 1.
            self.config.BaseStation3200[1].get_water_source(1).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=1,
                _controller_point_of_control_address=1,
                _flow_station_point_of_control_slot_number=1)

            # Tell the 3200 that the FlowStation is in control of Point of Control 1.
            self.config.BaseStation3200[1].get_point_of_control(1).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=1,
                _controller_mainline_address=1,
                _flow_station_mainline_slot_number=1)

            # Tell the 3200 that the FlowStation is in control of Mainline 1.
            self.config.BaseStation3200[1].get_mainline(1).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=1,
                _controller_water_source_address=2,
                _flow_station_water_source_slot_number=8)

            # Tell the 3200 that the FlowStation is in control of Water Source 2.
            self.config.BaseStation3200[1].get_water_source(2).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=1,
                _controller_point_of_control_address=2,
                _flow_station_point_of_control_slot_number=8)

            # Tell the 3200 to have the FlowStation manage Point of Control 2.
            self.config.BaseStation3200[1].get_point_of_control(2).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=1,
                _controller_mainline_address=2,
                _flow_station_mainline_slot_number=8)

            # Tell the 3200 to have the FlowStation manage Mainline 2.
            self.config.BaseStation3200[1].get_mainline(2).set_manage_by_flowstation()

            # Assign WS, POC, and ML on FlowStation
            # WS1 -> PC8 -> ML1
            self.config.FlowStations[1].get_point_of_control(pc_number=8).add_mainline_to_point_of_control(_mainline_address=1)
            self.config.FlowStations[1].get_water_source(ws_number=1).add_point_of_control_to_water_source(_point_of_control_address=8)

            # WS8 -> PC1 -> ML8
            self.config.FlowStations[1].get_point_of_control(pc_number=1).add_mainline_to_point_of_control(_mainline_address=8)
            self.config.FlowStations[1].get_water_source(ws_number=8).add_point_of_control_to_water_source(_point_of_control_address=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Use case is dying with "connection reset by peer" when incrementing the clock for the flowstation.
            # Maybe doesn't like the configuration set in step_8?
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.FlowStations[1].save_programming_to_flow_station()   # Basically, hit the "run" button on the flowstation
            self.config.FlowStations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        Set the time to be 7:59, which is 1 minute before both of our program's start times \n
        Verify that nothing is started: \n
            - Verify that no Programs are running \n
            - Verify that no zones are watering \n
        Increment the clock two minutes which should trigger both of our Programs to start \n
        Verify that specific zones have started: \n
            Program 1: \n
                - Verify that only zones 1, 2, and 5 are watering because water source 1 is limited to 50 GPM while zones 1
                  and 2 run at 20 GPM each and zone 5 runs at 7.5 bring it up to a grand total of 47.5 GPM leaving
                  nothing for the rest of the water source \n
            Program 2: \n
                - Verify that program 2 does not run because it runs off the flow from water source 1 and there is not
                  enough flow left to run any zones on the controller \n
            Stop the Programs after we verify they started correctly \n
                - This test only covers that the correct zones are running when there are flow restrictions, for a test
                  that verifies every minute ran, look at use_case_17 and use_case 16 \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_date_and_time(_date='02/12/2017', _time='7:59:00')
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.FlowStations[1].set_date_and_time(_date='02/12/2017', _time='07:59:00')
            self.config.FlowStations[1].verify_date_and_time()

            for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_done()

            for program in range(1, 3):
                self.config.BaseStation3200[1].programs[program].statuses.verify_status_is_done()         # For program 2, getting WA instead of DN running against 3200 16.0.592 and Flowstation 2.0.86

            # This should trigger the Programs to start watering
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                       controllers=self.config.BaseStation3200,
                                                                       minutes=3)

            # primary zones run first than linked when the controller is deciding which zones to turn on
            # Verify that zones 1, 11, are both primary zones so they should be on
            for program in range(1, 3):
                self.config.BaseStation3200[1].programs[program].statuses.verify_status_is_running()
            # program 1 can only run a max of 4 zone and mainline 1 can supply 50 gpm
            # program 2 can only run a max of 3 zone and mainline 1 can supply 50 gpm
            # these are the two primary zones
            for zone_address in [1, 11]:  # this gives you 35 gpm
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_watering()
                # linked zones on program 1 that can be watering
            for zone_address in [4]:  # this give  us 12 gpm
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_watering()
                # linked zones on program 1 that don't have enough water to run
            for zone_address in [2, 3, 5]:  # this give  us 12 gpm
                    self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_waiting_to_water()
                # linked zones on program 2
            for zone_address in [12, 13, 14, 15]:
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_waiting_to_water()

            # Stop the Programs after we verify everything ran as expected
            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        Change flow settings: \n
            - Mainline 2 (8 on flowstation): Flow to 25 GPM \n
            - Move zone 11-15 to mainline 2
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.FlowStations[1].get_mainline(8).set_target_flow(_gpm=25)

            # Add zones 11,12,13,14,15 to mainline 2
            for zone_address in [11, 12, 13, 14, 15]:
                self.config.BaseStation3200[1].get_mainline(2).add_zone_to_mainline(_zone_address=zone_address)

            # self.config.FlowStations[1].get_mainline(8).set_limit_zones_by_flow_to_true()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        Verify that nothing is started: \n
            - Verify that no Programs are running \n
            - Verify that no zones are watering \n
        Start both Programs using commands, and then increment the clock to update the status of the zones \n
        Verify that specific zones have started: \n
        Program 1: \n
            - Verify that only zones 1, 2, and 5 are watering because mainline 1 is still limited to 50 GPM while
              zones 1 and 2 run at 20 GPM each and zone 5 runs at 7.5 bring it up to a grand total of 47.5 GPM leaving
              nothing for the rest of the water source \n
        Program 2: \n
            - Verify that zones 11 and 13 are the only zones running on program 2 because mainline 2 (8 on
              flowstation) is not limited by flow (25gpm) so the program runs off the controller and program
              zone concurrency \n
        Stop the Programs after we verify they started correctly \n
            - Verify that zones 11 and 13 are the only zones running on program 2 because mainline 2 (8 on flowstation)
              is limited to 25 GPM because it is now limited by flow and both zones take up 22.5 GPM \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_done()

            for program in range(1, 3):
                self.config.BaseStation3200[1].programs[program].statuses.verify_status_is_done()

            # This should trigger the Programs to start watering
            self.config.BaseStation3200[1].programs[1].set_program_to_start()
            self.config.BaseStation3200[1].programs[2].set_program_to_start()
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=2)

            # Verify that programs 1 and 2 are running
            for program in [1, 2]:
                self.config.BaseStation3200[1].programs[program].statuses.verify_status_is_running()

            # Verify mainines 1 and 2 are running
            for mainline in [1, 2]:
                self.config.BaseStation3200[1].get_mainline(mainline).statuses.verify_status_is_running()

            # program 1 can only run a max of 4 zone and mainline 1 can supply 50 gpm
            for zone_address in [1, 2, 5]:  # this gives you a total of 47.5 gpm
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_watering()
            # program 1 waiting zones
            for zone_address in [3, 4]:  # this gives us 3 zones running
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_waiting_to_water()

            # program 2 can only run a max of 3 zone and mainline 1 can supply 25 gpm
            for zone_address in [11, 13]:  # this gives you a total of 22.5 gpm
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_watering()
            # program 2 waiting zones
            for zone_address in [12, 14, 15]:
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_waiting_to_water()

            # Stop the Programs after we verify everything ran as expected
            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
