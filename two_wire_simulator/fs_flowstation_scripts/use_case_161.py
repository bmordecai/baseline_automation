import sys
from time import sleep
from datetime import datetime, timedelta
from distutils.version import LooseVersion

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

__author__ = 'Kent'


class ControllerUseCase161(object):
    """
    Test name: \n
        Use Case 161: Flowstation Master valve and Pumps 

    User Story: \n
        1) As a user, I would like my normally closed master valve to be opened and have a "watering" status when the
           downstream zones are watering. I would like my normally closed master valve to be closed when downstream
           zones are done watering. I would also like my normally closed master valve to not be opened when manually
           starting a disabled program.
           
        2) As a user, I would like my normally open master valve to remain opened and have a "watering" status when the
           downstream zones are watering. I would like my normally open master valve to remain open when downstream
           zones are done watering. I would also like my normally open master valve to remain opened when manually
           starting a disabled program.

        3) As a user, I would like my pump to turn on when respective programming is running and to turn off when
           programming (irrigation) completes. I would also like my pump to remain off when when manually
           starting a disabled program.

    Coverage and Objectives:
        1.	Master Valve/Pump behavior when manually starting programs from a connected 3200
            a.	normally closed master valves display an on status (watering) when associated zones are running
            b.	normally open master valves display an on status (watering) when its zones are running
            c.  pumps that are off will display a watering status when associated zones are running
            d.	WS status is "RN", POC status is "RN", ML status is "RN", MV status is "WT", PM status is "WT"
        2.	Master Valve/Pump behavior when manually stopping programs from a connected 3200
            a.	normally closed master valves display an off status when associated zones are done
            b.	normally open master valves display an on status (watering) when its zones are done
            c.  pumps that are on (watering) will display a off status when associated zones are done
            d.	WS status is "OK", POC status is "OF", ML status is "OF", MV status is "OF" (if normally closed) or "WT"
               (if normally open), PM status is "OF"
        3.	Master Valve/Pump behavior when manually starting a disabled program from a connected 3200
            a.	normally closed master valves display an off status when associated zones are done
            b.	normally open master valves display an on status (watering) when its zones are done
            c.  pumps that are off will display a off status when associated zones are done 
            d.	WS status is "OK", POC status is "OF", ML status is "OF", MV status is "OF" (if normally closed) or 
               "WT" (if normally open), PM status is "OF"

    Not Covered:
        1.	Multiple 3200s managed by FlowStation
        2.	Concurrent Zones
        3.	Pressure flow stabilization
        4.	Mainline zone delays
        5.	POC grouping

    Test Overview:

        - FlowStation assignments
        
                     C1:W1
                       |
                /-------------\
                |      |      |
              C1:P1  C1:P2  C1:P3
                |      |      |
              C1:M1  C1:M2  C1:M3
                          
            - WS1 -> POC1 -> ML1
            - WS1 -> POC2 -> ML2
            - WS1 -> POC3 -> ML3
            
        - Initial Configuration:
            - WS 1 ---> PC 1 ---> ML 1 \n
                - PC 1
                    - MV 1 (normally closed)
                - ML 1
                    - ZN 1,2
                - PG 1
                    - Enabled
                    - ZN 1,2 (timed, 45 min RT)
            - WS 1 ---> PC 2 ---> ML 2 \n
                - PC 2
                    - MV 2 (normally open)
                - ML 2
                    - ZN 3,4
                - PG 2
                    - Enabled
                    - ZN 3,4 (timed, 45 min RT)
            - WS 1 ---> PC 3 ---> ML 3 \n
                - PC 3
                    - PM 3
                - ML 3
                    - ZN 5,6
                - PG 3
                    - ZN 5,6 (timed, 45 min RT)

    Scenario 1:
        - Start program 1
        - Verify correct statuses on everything attached to program 1.
    Scenario 2:
        - Start program 2, Stop program 1
        - Verify correct statuses on everything attached to program 2.
    Scenario 3:
        - Start program 3, Stop program 2
        - Verify correct statuses on everything attached to program 3.
    Scenario 4:
        - Disabled program 1
        - Start program 1 (It is disabled), Stop program 3
        - Verify correct statuses on everything attached to program 1.
    Scenario 5:
        - Disabled program 2
        - Start program 2 (It is disabled)
        - Verify correct statuses on everything attached to program 2.
    Scenario 6:
        - Disabled program 3
        - Start program 3 (It is disabled)
        - Verify correct statuses on everything attached to program 3.
    Scenario 7:
        - Enable programs 1, 2 and 3
        - Start programs 1, 2, 3 in parallel
        - Verify correct statuses on everything attached to program 1, 2, 3.

    """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/fs_json_config_files')
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=False)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)
            
    #################################
    def step_1(self):
        """
        ############################
        setup controller concurrency
        ############################
        - Set controller concurrency so that it isn't a limiting factor of test.
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=45)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        
    #################################
    def step_2(self):
        """
        ############################
        setup programs
        ############################

        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
            
        - Programs don't need start times because we are testing using manual starts
        - Set program concurrency so it isn't a limiting factor of test.
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=15)

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation3200[1].programs[2].set_enabled()
            self.config.BaseStation3200[1].programs[2].set_max_concurrent_zones(_number_of_zones=15)

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=3)
            self.config.BaseStation3200[1].programs[3].set_enabled()
            self.config.BaseStation3200[1].programs[3].set_max_concurrent_zones(_number_of_zones=15)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        Setup Zones on programs
        ############################

        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ratio \n
            
        - Set run times to be high because we are not going to run programming through until the end of run time, we are
          just testing statuses when starting/stopping programs.
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=45)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_run_time(_minutes=45)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[2].zone_programs[3].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[2].zone_programs[3].set_run_time(_minutes=45)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[2].zone_programs[4].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[2].zone_programs[4].set_run_time(_minutes=45)

            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=5)
            self.config.BaseStation3200[1].programs[3].zone_programs[5].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[3].zone_programs[5].set_run_time(_minutes=45)

            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=6)
            self.config.BaseStation3200[1].programs[3].zone_programs[6].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[3].zone_programs[6].set_run_time(_minutes=45)
            
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        Setup water sources
        ############################

        Add water sources -----> to controlLer
            - set up water source  Attributes \n
                - set enable state \n
                - set priority \n
                - set water budget \n
                - set water rationing state \n
                - set high pressure limit with shut down state \n
                - set low pressure limit with shut down state \n
                
        - Set no budget with shutdown disabled because we don't want water availability to affect the test.
        """
        helper_methods.print_method_name()
        try:

            # Water Source 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].get_water_source(1).set_enabled()
            self.config.BaseStation3200[1].get_water_source(1).set_monthly_watering_budget(_budget=0, _with_shutdown_enabled=False)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        Setup points of control
        ############################

        Add Points of Control -----> to controlLer
            - set up points of control Attributes \n
                - set enable state \n
                - set target flow \n
                - set high flow limit with shut down state \n
                - set unscheduled flow limit with shut down state \n
                - set high pressure limit with shut down state \n
                - set low pressure limit with shut down state \n
        Add flow meters ---> to point of control \n
        Add pump ---> to point of control \n
        Add master valve  ---> to point of control \n
        Add pressure sensor  ---> to point of control \n
        Add Points of Control -----> To Water Source
        
        - Not setting flow limits because they are irrelevant for the test
        - Don't need to assign PC to a WS on the 3200 here because we are going to assign them in the FlowStation
        - Set expected flow (target flows) to a large value (not relevant to test) but needs to be non-zero for
          FlowStation to allocate water. We are also setting to a large enough value to run all zones concurrently for
          each mainline.
        """
        helper_methods.print_method_name()
        try:
            # Add & Configure POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].get_point_of_control(1).set_enabled()
            self.config.BaseStation3200[1].get_point_of_control(1).set_target_flow(_gpm=500)
            # Add MV 1 to POC 1
            self.config.BaseStation3200[1].get_point_of_control(1).add_master_valve_to_point_of_control(_master_valve_address=1)
            # Configure MV 1 to be normally closed
            self.config.BaseStation3200[1].master_valves[1].set_normally_open_state(_normally_open=opcodes.false)

            # Add & Configure POC 2
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=2)
            self.config.BaseStation3200[1].get_point_of_control(2).set_enabled()
            self.config.BaseStation3200[1].get_point_of_control(2).set_target_flow(_gpm=500)
            # Add MV 2 to POC 2
            self.config.BaseStation3200[1].get_point_of_control(2).add_master_valve_to_point_of_control(_master_valve_address=2)
            # Configure MV 2 to be normally open
            self.config.BaseStation3200[1].master_valves[2].set_normally_open_state(_normally_open=opcodes.true)

            # Add & Configure POC 3
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=3)
            self.config.BaseStation3200[1].get_point_of_control(3).set_enabled()
            self.config.BaseStation3200[1].get_point_of_control(3).set_target_flow(_gpm=500)
            # Add PM 3 to POC 3
            self.config.BaseStation3200[1].get_point_of_control(3).add_pump_to_point_of_control(_pump_address=3)
            
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ############################
        Setup mainlines
        ############################

        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
        Add Mainline ---> to point of control
        
        - Not setting pipe stabilization times. Not apart of test coverage, will let controller use default (2min) 
          pipe fill time.
        - Not setting any flow variance or shutdowns because we don't want them to affect the test (not needed)
        - Not setting delays because we don't want them to affect the test (not needed)
        - Set expected flow (target flows) to a large value (not relevant to test) but needs to be non-zero for
          FlowStation to allocate water. We are also setting to a large enough value to run all zones concurrently for
          each mainline.
        """
        helper_methods.print_method_name()
        try:
            # Add & Configure ML 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].get_mainline(1).set_enabled()
            self.config.BaseStation3200[1].get_mainline(1).set_target_flow(_gpm=500)
            
            # Add & Configure ML 2
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=2)
            self.config.BaseStation3200[1].get_mainline(2).set_enabled()
            self.config.BaseStation3200[1].get_mainline(2).set_target_flow(_gpm=500)

            # Add & Configure ML 3
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=3)
            self.config.BaseStation3200[1].get_mainline(3).set_enabled()
            self.config.BaseStation3200[1].get_mainline(3).set_target_flow(_gpm=500)
            
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ############################
        Add zones to mainlines
        ############################

        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone (must be non-zero because otherwise the FlowStation won't allocate us any
                  water)
                  
        """
        helper_methods.print_method_name()
        try:
            # Mainline 1
            self.config.BaseStation3200[1].get_mainline(1).add_zone_to_mainline(_zone_address=1)
            self.config.BaseStation3200[1].get_mainline(1).add_zone_to_mainline(_zone_address=2)
            # Mainline 2
            self.config.BaseStation3200[1].get_mainline(2).add_zone_to_mainline(_zone_address=3)
            self.config.BaseStation3200[1].get_mainline(2).add_zone_to_mainline(_zone_address=4)
            # Mainline 3
            self.config.BaseStation3200[1].get_mainline(3).add_zone_to_mainline(_zone_address=5)
            self.config.BaseStation3200[1].get_mainline(3).add_zone_to_mainline(_zone_address=6)

            # Add Design Flow to Zones
            self.config.BaseStation3200[1].zones[1].set_design_flow(_gallons_per_minute=1)
            self.config.BaseStation3200[1].zones[2].set_design_flow(_gallons_per_minute=1)
            self.config.BaseStation3200[1].zones[3].set_design_flow(_gallons_per_minute=1)
            self.config.BaseStation3200[1].zones[4].set_design_flow(_gallons_per_minute=1)
            self.config.BaseStation3200[1].zones[5].set_design_flow(_gallons_per_minute=1)
            self.config.BaseStation3200[1].zones[6].set_design_flow(_gallons_per_minute=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        Share controller, water source, point of control, mainline with Flow Station
        ###############################

        - FlowStation assignments

                     C1:W1
                       |
                /-------------\
                |      |      |
              C1:P1  C1:P2  C1:P3
                |      |      |
              C1:M1  C1:M2  C1:M3

            - WS1 -> POC1 -> ML1
            - WS1 -> POC2 -> ML2
            - WS1 -> POC3 -> ML3
        """
        helper_methods.print_method_name()
        try:
            #################################
            # Add controller to FlowStation #
            #################################

            self.config.FlowStations[1].add_controller_to_flow_station(
                _controller_address=1,
                _flow_station_slot_number=1)

            ####################################
            # Add Water Sources to FlowStation #
            ####################################

            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=1,
                _controller_water_source_address=1,
                _flow_station_water_source_slot_number=1)

            # Tell the 3200 that the FlowStation is in control of Water Source 1.
            self.config.BaseStation3200[1].get_water_source(1).set_manage_by_flowstation()

            ########################################
            # Add Points of Control to FlowStation #
            ########################################

            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=1,
                _controller_point_of_control_address=1,
                _flow_station_point_of_control_slot_number=1)

            # Tell the 3200 that the FlowStation is in control of Point of Control 1.
            self.config.BaseStation3200[1].get_point_of_control(1).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=1,
                _controller_point_of_control_address=2,
                _flow_station_point_of_control_slot_number=2)

            # Tell the 3200 that the FlowStation is in control of Point of Control 2.
            self.config.BaseStation3200[1].get_point_of_control(2).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=1,
                _controller_point_of_control_address=3,
                _flow_station_point_of_control_slot_number=3)

            # Tell the 3200 that the FlowStation is in control of Point of Control 3.
            self.config.BaseStation3200[1].get_point_of_control(3).set_manage_by_flowstation()

            ################################
            # Add Mainlines to FlowStation #
            ################################

            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=1,
                _controller_mainline_address=1,
                _flow_station_mainline_slot_number=1)

            # Tell the 3200 that the FlowStation is in control of Mainline 1.
            self.config.BaseStation3200[1].get_mainline(1).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=1,
                _controller_mainline_address=2,
                _flow_station_mainline_slot_number=2)

            # Tell the 3200 that the FlowStation is in control of Mainline 1.
            self.config.BaseStation3200[1].get_mainline(2).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=1,
                _controller_mainline_address=3,
                _flow_station_mainline_slot_number=3)

            # Tell the 3200 that the FlowStation is in control of Mainline 1.
            self.config.BaseStation3200[1].get_mainline(3).set_manage_by_flowstation()

            ####################################
            # FLOWSTATION WS/PC/ML ASSIGNMENTS #
            ####################################

            # Assign WS, POC, and ML on FlowStation
            # WS1 -> PC1 -> ML1
            self.config.FlowStations[1].get_water_source(1).add_point_of_control_to_water_source(_point_of_control_address=1)
            self.config.FlowStations[1].get_point_of_control(1).add_mainline_to_point_of_control(_mainline_address=1)

            # WS1 -> PC2 -> ML2
            self.config.FlowStations[1].get_water_source(1).add_point_of_control_to_water_source(_point_of_control_address=2)
            self.config.FlowStations[1].get_point_of_control(2).add_mainline_to_point_of_control(_mainline_address=2)

            # WS1 -> PC3 -> ML3
            self.config.FlowStations[1].get_water_source(1).add_point_of_control_to_water_source(_point_of_control_address=3)
            self.config.FlowStations[1].get_point_of_control(3).add_mainline_to_point_of_control(_mainline_address=3)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        ###############################
        verify the entire configuration
        ###############################

        - Increment clock to save configuration
        - Get information for each object from controller
        - verify information returned from controller against information stored in the objects
        """
        helper_methods.print_method_name()
        try:
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.FlowStations[1].save_programming_to_flow_station()
            self.config.FlowStations[1].verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        ###############################
        verify initial statuses
        ###############################

        set the date and time of the controller so that there is a known days of the week and days of the month \n
        - verify all devices are working by doing a verification before starting the test \n
        """
        helper_methods.print_method_name()
        try:

            self.config.BaseStation3200[1].set_date_and_time(_date='04/08/2018', _time='07:00:00')
            self.config.FlowStations[1].set_date_and_time(_date='04/08/2018', _time='07:00:00')

            # verify water sources status
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_ok()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_ok()

            # --------------------------------------------
            # User Story 1 Components (MV Normally Closed)
            # --------------------------------------------

            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_off()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_off()

            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()

            # ------------------------------------------
            # User Story 2 Components (MV Normally Open)
            # ------------------------------------------

            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(2).statuses.verify_status_is_off()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_off()

            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()

            # ------------------------------
            # User Story 3 Components (Pump)
            # ------------------------------

            self.config.BaseStation3200[1].get_point_of_control(3).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_off()

            self.config.FlowStations[1].get_point_of_control(3).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_off()

            self.config.BaseStation3200[1].pumps[3].statuses.verify_status_is_off()

            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        ###############################
        verify scenario 1:
        ###############################

        Scenario 1:
            - Manually Start program 1
            - Verify correct statuses on everything attached to program 1 for first minute as 3200 waits for water
              allocation from FlowStation
            - Increment clock second minute to see watering statuses.

        """
        helper_methods.print_method_name()
        try:
            self.config.BaseStation3200[1].programs[1].set_program_to_start()

            # increment clock 1 minute so that the program will start and status will be updated \n
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # verify water sources status
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            # --------------------------------------------
            # User Story 1 Components (MV Normally Closed)
            # --------------------------------------------

            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()

            # increment clock 1 minute so that status will be updated to show watering \n
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # verify water sources status
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            # --------------------------------------------
            # User Story 1 Components (MV Normally Closed)
            # --------------------------------------------

            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        ###############################
        verify scenario 2:
        ###############################

        Scenario 2:
            - Start program 2, Stop program 1
            - Verify correct statuses on everything attached to program 2 for first minute as 3200 waits for water
              allocation from FlowStation
            - Increment clock second minute to see watering statuses.

        """
        helper_methods.print_method_name()
        try:
            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[2].set_program_to_start()

            # increment clock 1 minute so that the program will start and status will be updated \n
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # verify water sources status
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            # --------------------------------------------
            # User Story 1 Components (MV Normally Closed)
            # --------------------------------------------

            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_off()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_off()

            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()

            # ------------------------------------------
            # User Story 2 Components (MV Normally Open)
            # ------------------------------------------

            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()

            # increment clock 1 minute so that status will be updated to show watering \n
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # verify water sources status
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            # ------------------------------------------
            # User Story 2 Components (MV Normally Open)
            # ------------------------------------------

            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()

            # increment clock 1 minute so that status will be updated to show watering \n
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # verify water sources status
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            # ------------------------------------------
            # User Story 2 Components (MV Normally Open)
            # ------------------------------------------

            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        ###############################
        verify scenario 3:
        ###############################

        Scenario 3:
            - Start program 3, Stop program 2
            - Verify program 2 and related components go back to "IDLE" state
            - Verify correct statuses on everything attached to program 3 for first minute as 3200 waits for water
              allocation from FlowStation
            - Increment clock second minute to see watering statuses.

        """
        helper_methods.print_method_name()
        try:
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()
            self.config.BaseStation3200[1].programs[3].set_program_to_start()

            # increment clock 1 minute so that the program will start and status will be updated \n
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # verify water sources status
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            # ------------------------------------------
            # User Story 2 Components (MV Normally Open)
            # ------------------------------------------

            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(2).statuses.verify_status_is_off()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_off()

            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()

            # ------------------------------
            # User Story 3 Components (Pump)
            # ------------------------------

            self.config.BaseStation3200[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].pumps[3].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_waiting_to_water()

            # increment clock 1 minute so that status will be updated to show watering \n
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # verify water sources status
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].pumps[3].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_watering()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_14(self):
        """
        ###############################
        verify scenario 4:
        ###############################

        Scenario 4:
            - Disabled program 1
            - Start program 1 (It is disabled), Stop program 3
            - verify program 3 and related components are shut off
            - Verify correct statuses on everything attached to program 1  (program should have disabled status)
                - program 1 should be disabled
                - WS1 should be ok
                - PC1/ML1 should be off
                - MV1 should be still watering because it's normally open
                - zones 1,2 should be done

        """
        helper_methods.print_method_name()
        try:
            self.config.BaseStation3200[1].programs[3].set_program_to_stop()

            self.config.BaseStation3200[1].programs[1].set_disabled()
            self.config.BaseStation3200[1].programs[1].set_program_to_start()

            # increment clock 1 minute so that the program will start and status will be updated \n
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # verify water sources status
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_ok()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_ok()

            # --------------------------------------------
            # User Story 1 Components (MV Normally Closed)
            # --------------------------------------------

            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_off()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_off()

            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_disabled()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()

            # ------------------------------
            # User Story 3 Components (Pump)
            # ------------------------------

            self.config.BaseStation3200[1].get_point_of_control(3).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_off()

            self.config.FlowStations[1].get_point_of_control(3).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_off()

            self.config.BaseStation3200[1].pumps[3].statuses.verify_status_is_off()

            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_15(self):
        """
        ###############################
        verify scenario 5:
        ###############################

        Scenario 5:
            - Disabled program 2
            - Start program 2 (It is disabled)
            - Verify correct statuses on everything attached to program 2 (program should have disabled status).
                - program 2 should be disabled
                - WS1 should be ok
                - PC2/ML2 should be off
                - MV2 should be still watering because it's normally open
                - zones 3,4 should be done

        """
        helper_methods.print_method_name()
        try:
            self.config.BaseStation3200[1].programs[2].set_disabled()
            self.config.BaseStation3200[1].programs[2].set_program_to_start()

            # increment clock 1 minute so that the program will start and status will be updated \n
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # verify water sources status
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_ok()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_ok()

            # ------------------------------------------
            # User Story 2 Components (MV Normally Open)
            # ------------------------------------------

            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(2).statuses.verify_status_is_off()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_off()

            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_disabled()

            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_16(self):
        """
        ###############################
        verify scenario 6:
        ###############################

        Scenario 6:
            - Disabled program 3
            - Start program 3 (It is disabled)
            - Verify correct statuses on everything attached to program 3 (program should have disabled status).
                - program 3 should be disabled
                - WS1 should be ok
                - PC3/ML3 should be off
                - pump 3 should be off
                - zones 5,6 should be done

        """
        helper_methods.print_method_name()
        try:
            self.config.BaseStation3200[1].programs[3].set_disabled()
            self.config.BaseStation3200[1].programs[3].set_program_to_start()

            # increment clock 1 minute so that the program will start and status will be updated \n
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # verify water sources status
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_ok()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_ok()

            # ------------------------------
            # User Story 3 Components (Pump)
            # ------------------------------

            self.config.BaseStation3200[1].get_point_of_control(3).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_off()

            self.config.FlowStations[1].get_point_of_control(3).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_off()

            self.config.BaseStation3200[1].pumps[3].statuses.verify_status_is_off()

            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_disabled()

            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_17(self):
        """
        ###############################
        verify scenario 7:
        ###############################

        Scenario 7:
            - Enable programs 1, 2 and 3
            - Start programs 1, 2, 3 in parallel
            - Verify correct statuses on everything attached to program 1, 2, 3 (system should be waiting for water
              allocation granted by FlowStation)
                - WS 1 turned on
                - ALL POCs turned on
                - All mainlines remain off
                - Normally closed MV opened (MV1)
                - Pump turned on (PM3)
                - Programs 1,2,3 go to waiting
                - All zones go to waiting
            - Increment clock an additional minute to see watering statuses
                - All mainlines start watering
                - All programs go to watering
                - All zones go to watering
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_program_to_start()

            self.config.BaseStation3200[1].programs[2].set_enabled()
            self.config.BaseStation3200[1].programs[2].set_program_to_start()

            self.config.BaseStation3200[1].programs[3].set_enabled()
            self.config.BaseStation3200[1].programs[3].set_program_to_start()

            # increment clock 1 minute so that the program will start and status will be updated \n
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # verify water sources status
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            # --------------------------------------------
            # User Story 1 Components (MV Normally Closed)
            # --------------------------------------------

            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()

            # ------------------------------------------
            # User Story 2 Components (MV Normally Open)
            # ------------------------------------------

            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()

            # ------------------------------
            # User Story 3 Components (Pump)
            # ------------------------------

            self.config.BaseStation3200[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].pumps[3].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_waiting_to_water()

            # ----------------------------------------------------------------------------------------------------
            # Increment clock forward 1 minute to have 3200 react to water allocation from FlowStation and to turn
            # stuff on
            # ----------------------------------------------------------------------------------------------------

            # increment clock 1 minute so that status will be updated to show watering \n
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # verify water sources status
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            # --------------------------------------------
            # User Story 1 Components (MV Normally Closed)
            # --------------------------------------------

            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()

            # ------------------------------------------
            # User Story 2 Components (MV Normally Open)
            # ------------------------------------------

            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()

            # ------------------------------
            # User Story 3 Components (Pump)
            # ------------------------------

            self.config.BaseStation3200[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].pumps[3].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()
            self.config.BaseStation3200[1].programs[3].set_program_to_stop()

            # increment clock 1 minute so that status will be updated to show watering \n
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
