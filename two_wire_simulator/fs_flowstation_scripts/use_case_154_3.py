import sys
from time import sleep
from datetime import datetime, timedelta
from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

__author__ = 'Eldin'


class ControllerUseCase154_3(object):
    """
    Test name:
        - CN UseCase154_3 Basic Design Flow with Flowstation

    User Story:
       The user wants to use a FlowStation to manage flow on a 3200 that doesn't have a flowmeter.  The user wants
       a zone with a design flow set larger than the total allowable flow configured at the mainline and POC to irrigate
       using all available flow. This is because the user mis-configured the zone design flow or sources of
       water have become unavailable or the zone learn flow value was larger than the design flow of the mainlines or POCs.

    Purpose:
        - This test is to verify that, using a single FlowStation attached to multiple 3200s, the FlowStation can
          allocate flow based on zone design flows and POC / Mainline target flows
        - Set up a zones on the controller and attach them to program
        - Make Zones 1 and 11 primary zones, and attach the rest of the zones to these two zones
        - Set up a design flow for all zones. One zone will have a design flow larger than the mainline design flow
        - Set up a POC and Mainline to manipulate the zones in a certain way described in comments in each step \n

    Coverage Area: \n
        1.  When a zone has a design flow value greater that the design flow value of the mainline and/or mainline's
            point of control:
            a.  zone should still run as expected
            b.  zone's mainline and/or point of control should never exceed its design flow, with the exception of
                trying to run a single zone
            c.  only a single zone is allowed to run when a mainline and/or point of control is exceeding its set
                design flow
            d.  with multiple 3200's managed by FlowStation, first controller to request water wins

    Use Case explanation:
        - Two controllers managed by a single FlowStation
        - Two water sources, two POCs, and two mainlines will be setup to verify the behavior
            + One per 3200 respectively
        -

        First Scenario:
            Controller 1:
                - Zone 1 design flow = 200GPM
                - Zones 2, 3 design flow = 25GPM
                - Zone 1 on program 1 with a runtime of 5 minutes (primary)
                - Zones on program 1 with a runtime of 5 minutes (linked to 1)
                - Program 1 start time 8:00am
            Controller 2:
                - Zones 1, 2, 3 design flow = 50GPM
                - All zones on program 1 with a runtime of 5 minutes (timed)
                - Program 1 start time 8:05am
            FlowStation:
                - WS/PC/ML Assignments:
                    C1:W1       C2:W2
                      |           |
                    C1:P1       C2:P2
                        \       /
                          C1:M1
                            |
                          C2:M2

    Date References:
        - configuration for script is located common\configuration_files\fs_json_config_files\FS_basic_design_flow_3.json
        - the devices and addresses range is read from the .json file
    """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/fs_json_config_files'
                                    )
        self.run_use_case()

    #################################
    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                     # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        ############################
        setup controller concurrency
        ############################
        We want the concurrency to not be a limiting factor in the test.

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=15)
            self.config.BaseStation3200[2].set_max_concurrent_zones(_max_zones=15)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        Set concurrency so it is not a limiting factor in the test
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=15)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=[480])

            self.config.BaseStation3200[2].add_program_to_controller(_program_address=2)
            self.config.BaseStation3200[2].programs[2].set_enabled()
            self.config.BaseStation3200[2].programs[2].set_max_concurrent_zones(_number_of_zones=15)
            self.config.BaseStation3200[2].programs[2].set_start_times(_st_list=[485])

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        The primary zone is used to verify the controller requests the correct minimum amount of water to run the primary
        zone first.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add zone programs to Program 1
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_as_linked_zone(_primary_zone=1)

            # Add zone programs to Program 2
            self.config.BaseStation3200[2].programs[2].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[2].programs[2].zone_programs[1].set_run_time(_minutes=5)
            self.config.BaseStation3200[2].programs[2].zone_programs[1].set_as_timed_zone()

            self.config.BaseStation3200[2].programs[2].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[2].programs[2].zone_programs[2].set_run_time(_minutes=5)
            self.config.BaseStation3200[2].programs[2].zone_programs[2].set_as_timed_zone()

            self.config.BaseStation3200[2].programs[2].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[2].programs[2].zone_programs[3].set_run_time(_minutes=5)
            self.config.BaseStation3200[2].programs[2].zone_programs[3].set_as_timed_zone()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
        No budgets are used so it will not disable our water sources
        Priorities are the same so they are both part of the water allocation
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Water Source 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].get_water_source(1).set_enabled()
            self.config.BaseStation3200[1].get_water_source(1).set_priority(_priority_for_water_source=1)

            # Water Source 2
            self.config.BaseStation3200[2].add_water_source_to_controller(_water_source_address=2)
            self.config.BaseStation3200[2].get_water_source(2).set_enabled()
            self.config.BaseStation3200[2].get_water_source(2).set_priority(_priority_for_water_source=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n

        We are not setting any limiting factors (shutdowns, priorities, etc.).
        The design flow also should not be a limiting factor for water allocation so we set it to a value greater than
        its mainline
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].get_point_of_control(1).set_enabled()
            self.config.BaseStation3200[1].get_point_of_control(1).set_target_flow(_gpm=150)

            # POC 2
            self.config.BaseStation3200[2].add_point_of_control_to_controller(_point_of_control_address=2)
            self.config.BaseStation3200[2].get_point_of_control(2).set_enabled()
            self.config.BaseStation3200[2].get_point_of_control(2).set_target_flow(_gpm=150)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control

        Set both mainline design flows so they will limit the flow through the mainlines. Mainline 1s design flow is set
        to be a limiting factor for water allocation in the FlowStation. Mainline 2s design flow is arbitrary as it does
        not affect the test.
        We assume that if the mainline is managed by the FlowStation it will automatically limit concurrency by flow.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Mainline 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].get_mainline(1).set_enabled()
            self.config.BaseStation3200[1].get_mainline(1).set_target_flow(_gpm=100)

            # Mainline 2
            self.config.BaseStation3200[2].add_mainline_to_controller(_mainline_address=2)
            self.config.BaseStation3200[2].get_mainline(2).set_enabled()
            self.config.BaseStation3200[2].get_mainline(2).set_target_flow(_gpm=100)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone

        Setting controller 1 zone 1 to be higher than the design flow of the mainline so we can verify it will run alone.

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add Zones 1-3 to Mainline 1
            for zone_address in range(1, 4):
                self.config.BaseStation3200[1].get_mainline(1).add_zone_to_mainline(_zone_address=zone_address)

            self.config.BaseStation3200[1].zones[1].set_design_flow(_gallons_per_minute=200)
            self.config.BaseStation3200[1].zones[2].set_design_flow(_gallons_per_minute=25)
            self.config.BaseStation3200[1].zones[3].set_design_flow(_gallons_per_minute=25)

            # Add Zones 1-3 to Mainline 2
            for zone_address in range(1, 4):
                self.config.BaseStation3200[2].get_mainline(2).add_zone_to_mainline(_zone_address=zone_address)

            self.config.BaseStation3200[2].zones[1].set_design_flow(_gallons_per_minute=50)
            self.config.BaseStation3200[2].zones[2].set_design_flow(_gallons_per_minute=50)
            self.config.BaseStation3200[2].zones[3].set_design_flow(_gallons_per_minute=50)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        Share controller, water source, point of control, mainline with Flow Station
        ###############################
            - Share controller1 with the flow station
            - Add and share a Water Source between controller1 and the FlowStation
            - Add and share Point of Control between controller1 and the FlowStation
            - Add and share Mainline between controller1 and the FlowStation

            - Share controller2 with the flow station
            - Add and share a Water Source between controller2 and the FlowStation
            - Add and share Point of Control between controller2 and the FlowStation
            - Add and share Mainline between controller2 and the FlowStation

            - Assign WS, POC, and ML on the FlowStation
            - WS/PC/ML Assignments:
                    C1:W1       C2:W2
                      |           |
                    C1:P1       C2:P2
                        \       /
                          C1:M1
                            |
                          C2:M2
            -By assigning the controller WS, POC, and MLs to the FlowStation we automatically setting the managed by
            FlowStation setting for each.
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add controller 1 to FlowStation
            self.config.FlowStations[1].add_controller_to_flow_station(
                _controller_address=1,
                _flow_station_slot_number=1)

            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=1,
                _controller_water_source_address=1,
                _flow_station_water_source_slot_number=1)

            self.config.BaseStation3200[1].get_water_source(1).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=1,
                _controller_point_of_control_address=1,
                _flow_station_point_of_control_slot_number=1)

            self.config.BaseStation3200[1].get_point_of_control(1).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=1,
                _controller_mainline_address=1,
                _flow_station_mainline_slot_number=1)

            self.config.BaseStation3200[1].get_mainline(1).set_manage_by_flowstation()

            # Add controller 2 to FlowStation
            self.config.FlowStations[1].add_controller_to_flow_station(
                _controller_address=2,
                _flow_station_slot_number=2)

            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=2,
                _controller_water_source_address=2,
                _flow_station_water_source_slot_number=2)

            self.config.BaseStation3200[2].get_water_source(2).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=2,
                _controller_point_of_control_address=2,
                _flow_station_point_of_control_slot_number=2)

            self.config.BaseStation3200[2].get_point_of_control(2).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=2,
                _controller_mainline_address=2,
                _flow_station_mainline_slot_number=2)

            self.config.BaseStation3200[2].get_mainline(2).set_manage_by_flowstation()

            # Assign WS, POC, and ML on FlowStation
            # WS1 -> PC1 -> ML1
            self.config.FlowStations[1].get_water_source(ws_number=1).add_point_of_control_to_water_source(_point_of_control_address=1)
            self.config.FlowStations[1].get_point_of_control(pc_number=1).add_mainline_to_point_of_control(_mainline_address=1)

            # WS2 -> PC2 -> ML1
            self.config.FlowStations[1].get_water_source(ws_number=2).add_point_of_control_to_water_source(_point_of_control_address=2)
            self.config.FlowStations[1].get_point_of_control(pc_number=2).add_mainline_to_point_of_control(_mainline_address=1)

            # ML1 -> ML2
            self.config.FlowStations[1].get_mainline(ml_number=1).add_mainline_to_mainline(_mainline_address=2)

            # Explicitly disable dynamic flow on the FlowStation
            self.config.FlowStations[1].set_dynamic_flow_allocation_to_false()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.BaseStation3200[2].verify_full_configuration()
            self.config.FlowStations[1].verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        Set the time to be 7:59, which is 1 minute before both of our program's start times \n
        Verify that nothing is started: \n
            - Verify that no Programs are running \n
            - Verify that no zones are watering \n
        Increment the clock two minutes which should trigger both of our Programs to start \n
        Verify that specific zones have started for each program below: \n
            Controller 1 Program 1:
                - Program starts at 8:01am
                    - Zones will be set to waiting because water needs to be requested from the FlowStation next
                - 8:02am
                    - Zone 1 is running. All other zones are waiting. Zone one will only run alone because its design
                    flow is set higher than the total available flow and is the primary zone and must run first.
            Controller 2 Program 2:
                - Program starts at 8:01am
                    - Zones will be set to waiting because water needs to be requested from the FlowStation next
                - 8:02am
                    - All zones are waiting because all available water is being used by controller 1 program 1.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_date_and_time(_date='02/12/2017', _time='7:59:00')
            self.config.BaseStation3200[1].verify_date_and_time()

            self.config.BaseStation3200[2].set_date_and_time(_date='02/12/2017', _time='7:59:00')
            self.config.BaseStation3200[2].verify_date_and_time()

            self.config.FlowStations[1].set_date_and_time(_date='02/12/2017', _time='07:59:00')
            self.config.FlowStations[1].verify_date_and_time()

            # all zone have a status of done
            for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_done()
            for zone_address in sorted(self.config.BaseStation3200[2].zones.keys()):
                self.config.BaseStation3200[2].zones[zone_address].statuses.verify_status_is_done()

            # programs have a status of done
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_done()

            # verify water source status
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_ok()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_ok()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_ok()
            self.config.FlowStations[1].get_water_source(2).statuses.verify_status_is_ok()

            # verify POC status
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_off()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_off()

            # verify mainline status
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_off()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_off()

            # This should trigger the Programs to start at 8:01am
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=2)

            # all zones on controller 1 have a status of waiting
            for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_waiting_to_water()
            # all zones on controller 2 are done
            for zone_address in sorted(self.config.BaseStation3200[2].zones.keys()):
                self.config.BaseStation3200[2].zones[zone_address].statuses.verify_status_is_done()

            # programs have a status of waiting
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_done()

            # verify water source status
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(2).statuses.verify_status_is_running()

            # verify POC status
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()

            # verify mainline status
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_off()

            # Increment to 8:02am
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify zone status
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()

            # all zones on controller 2 are done
            for zone_address in sorted(self.config.BaseStation3200[2].zones.keys()):
                self.config.BaseStation3200[2].zones[zone_address].statuses.verify_status_is_done()

            # Program status
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_done()

            # verify water source status
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(2).statuses.verify_status_is_running()

            # verify POC status
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()

            # verify mainline status
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_off()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        Set the time to be 8:08, which is 5 minute after the start of the programs and the runtime of controller 1
        zone 1.

        Increment the clock 6 minutes which should finish the runtime of controller 1 zone 1 \n
        Verify that specific zones status: \n
            Controller 1 Program 1:
                - 8:08am
                    - Zone 1 has completed it runtime and is set to done. Zones 2 and 3 start water because they only
                     require 50GPM and we have 100GPM available through our mainline to water sources.
            Controller 2 Program 2:
                - 8:08am
                    - Zone 1 start watering because there is 50GPM leftover from what was allocated to controller 1
                    mainline 1
                    - Zones 2 and 3 are still waiting because there is no more flow available for them
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment to 8:08am
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=6)

            # Verify zone status
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()

            self.config.BaseStation3200[2].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[3].statuses.verify_status_is_waiting_to_water()

            # Program status
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_running()

            # verify water source status
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(2).statuses.verify_status_is_running()

            # verify POC status
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()

            # verify mainline status
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]