import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration

# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr

__author__ = 'Ben'


class ControllerUseCase155_2(object):
    """
    Test name:
        - Using multiple water source budgets to a single point mainline with dynamic flow allocation disabled

    User Story: \n

        1)  The user wants to have to different water supply delivering water to their single mainline on the property
            with separate amounts of allocated water for each supply. If either source reaches its allocated amount
            of water they want to be alerted, but they want the water source to keep supplying water.
            If starting a new month, the user would like the monthly usage for his water source to be reset

    Coverage and Objectives:
        1.	When a mainline is receiving water from multiple water sources and dynamic flow allocation is disabled
            at the flow station:
            a.	multiple water sources with different monthly budgets without shutdown
            b.	all available water sources will be ran until they are out of water, including water sources that had
                no water allocation provided
                i.	If budget is met,
                    1.	a water source exceeds budget message will be generated at the 3200
                    2.	water source should not be "available" until after the start of a new month
                    3.	water source's budget should be reset at the start of the month
            c.	irrigation should continue until programming completes.

    Use Case explanation:
        - Two controllers managed by a single FlowStation
        - Two water source, two POCs, and single mainline with 4 zones.

        First Scenario:
            Controller 1:
                - WS1, budget: 900
                - POC1, DF 150 GPM
                - ML1 (default priority), DF 200 GPM
                - 4 zones on ML1, all DF 50 GPM
                - Zones 1 on program 1 with a runtime of 5 minutes (primary)
                - Zones 2,3,4 on program 1 with a runtime of 5 minutes (linked to 1)
                - Program 1 start time 8:00am
                - Zones are attached to ML1
            Controller 2:
                - WS2, budget: 300
                - POC2, DF 150 GPM

            FlowStation:
                - WS/PC/ML Assignments:

                    C1:W1         C2:W2
                      |             |
                    C2:P2         C1:P1
                        \         /
                           C1:M1

    Not Covered:
        1.	Dynamic Flow Allocation Enabled
        2.	Single 3200 managed by FlowStation
        3.	High flows & shutdowns
        4.	Unscheduled flows & shutdowns
        5.	Variances & shutdowns
        6.	Empty conditions

    Date References:
        - configuration for script is located common\configuration_files\using_real_time_flow.json
        - the devices and addresses range is read from the .json file
    """
    
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/fs_json_config_files'
                                    )
        self.run_use_case()

    #################################
    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)
                    
                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1
                    
                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        ############################
        setup controller concurrency
        ############################
        We want the concurrency to not be a limiting factor in the test.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=15)
            self.config.BaseStation3200[2].set_max_concurrent_zones(_max_zones=15)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        Set concurrency so it is not a limiting factor in the test
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=15)
            # We're going to manually run the program, so not setting start times

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ratio \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add zone programs to Program 1
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[1].zone_programs[4].set_as_linked_zone(_primary_zone=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n

        Budget is set lower on WS2 so that we can test the FlowStation running out of budget on WS2 (and so setting
        an out-of-budget message), but still continuing to run

        Budget on WS1 is set high enough so that it doesn't become a limiting factor

        Not setting water source priorities to allow the FlowStation to default all water sources to the same priority.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Water Source 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].get_water_source(1).set_enabled()
            self.config.BaseStation3200[1].get_water_source(1).set_monthly_watering_budget(_budget=900, _with_shutdown_enabled=False)

            # Water Source 2
            self.config.BaseStation3200[2].add_water_source_to_controller(_water_source_address=2)
            self.config.BaseStation3200[2].get_water_source(2).set_enabled()
            self.config.BaseStation3200[2].get_water_source(2).set_monthly_watering_budget(_budget=300, _with_shutdown_enabled=False)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n

        We are not setting any limiting factors (shutdowns, priorities, etc.).

        C1P1 & C2P2 have small enough design flows (smaller than the downstream ML) so that they both are necessary to
        supply water.

        Both POCs have flowmeters so that we can track budgets on both water sources
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].get_point_of_control(1).set_enabled()
            self.config.BaseStation3200[1].get_point_of_control(1).set_target_flow(_gpm=150)
            self.config.BaseStation3200[1].get_point_of_control(1).add_flow_meter_to_point_of_control(_flow_meter_address=1)

            # POC 2
            self.config.BaseStation3200[2].add_point_of_control_to_controller(_point_of_control_address=2)
            self.config.BaseStation3200[2].get_point_of_control(2).set_enabled()
            self.config.BaseStation3200[2].get_point_of_control(2).set_target_flow(_gpm=150)
            self.config.BaseStation3200[2].get_point_of_control(2).add_flow_meter_to_point_of_control(_flow_meter_address=2)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control

        Set mainline design flow so that it will allow all four attached zones to run at the same time

        We assume that if the mainline is managed by the FlowStation it will automatically limit concurrency by flow.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Mainline 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].get_mainline(1).set_enabled()
            self.config.BaseStation3200[1].get_mainline(1).set_target_flow(_gpm=200)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone

        Set design flow such that when they are all running they do not hit the limit of the upstream POC's and mainlines
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add Zones 1-4 to Mainline 1
            for zone_address in range(1, 5):
                self.config.BaseStation3200[1].get_mainline(1).add_zone_to_mainline(_zone_address=zone_address)

            self.config.BaseStation3200[1].zones[1].set_design_flow(_gallons_per_minute=50)
            self.config.BaseStation3200[1].zones[2].set_design_flow(_gallons_per_minute=50)
            self.config.BaseStation3200[1].zones[3].set_design_flow(_gallons_per_minute=50)
            self.config.BaseStation3200[1].zones[4].set_design_flow(_gallons_per_minute=50)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        Share controller, water source, point of control, mainline with Flow Station
        ###############################
            - Share controller1 with the flow station
            - Add and share Water Sources between controller 1 and the FlowStation
            - Add and share Points of Control between controller 1 and the FlowStation
            - Add and share Mainlines between controller 1 and the FlowStation

            - Share controller2 with the flow station
            - Add and share a Water Source between controller 2 and the FlowStation
            - Add and share Point of Control between controller 2 and the FlowStation

            - Assign WS, POC, and ML on the FlowStation
            - WS/PC/ML Assignments:

                    C1:W1         C2:W2
                      |             |
                    C2:P2         C1:P1
                        \         /
                           C1:M1

            - By assigning the controller WS, POC, and MLs to the FlowStation we automatically setting the managed by
              FlowStation setting for each.
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add controller 1 to FlowStation
            self.config.FlowStations[1].add_controller_to_flow_station(
                _controller_address=1,
                _flow_station_slot_number=1)

            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=1,
                _controller_water_source_address=1,
                _flow_station_water_source_slot_number=1)

            self.config.BaseStation3200[1].water_sources[1].set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=1,
                _controller_point_of_control_address=1,
                _flow_station_point_of_control_slot_number=1)

            self.config.BaseStation3200[1].points_of_control[1].set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=1,
                _controller_mainline_address=1,
                _flow_station_mainline_slot_number=1)

            self.config.BaseStation3200[1].mainlines[1].set_manage_by_flowstation()

            # Add controller 2 to FlowStation
            self.config.FlowStations[1].add_controller_to_flow_station(
                _controller_address=2,
                _flow_station_slot_number=2)

            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=2,
                _controller_water_source_address=2,
                _flow_station_water_source_slot_number=2)

            self.config.BaseStation3200[2].water_sources[2].set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=2,
                _controller_point_of_control_address=2,
                _flow_station_point_of_control_slot_number=2)

            self.config.BaseStation3200[2].points_of_control[2].set_manage_by_flowstation()

            # Assign WS, POC, and ML on FlowStation
            # WS1 -> PC2 -> ML1
            self.config.FlowStations[1].get_water_source(1).add_point_of_control_to_water_source(_point_of_control_address=2)
            self.config.FlowStations[1].get_point_of_control(2).add_mainline_to_point_of_control(_mainline_address=1)

            # WS2 -> PC1 -> ML1
            self.config.FlowStations[1].get_water_source(2).add_point_of_control_to_water_source(_point_of_control_address=1)
            self.config.FlowStations[1].get_point_of_control(1).add_mainline_to_point_of_control(_mainline_address=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.BaseStation3200[2].verify_full_configuration()
            self.config.FlowStations[1].verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        Set the flow meters to zero usage this gives us a starting point \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_water_usage(_water_usage=0)
            self.config.BaseStation3200[2].flow_meters[2].bicoder.set_water_usage(_water_usage=0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[2].flow_meters[2].bicoder.self_test_and_update_object_attributes()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        Set the time to be 23:52 on the last day of the month.  This gives us time to run the program to completion,
        and run out the budget for C2W2 before the end of the month.
        Manually start the program at this time
        Verify that nothing is started: \n
            - Verify that no Programs are running \n
            - Verify that no zones are watering \n
        Increment the clock two minutes which will give time for the controller to request and be allocated water
        from the flowstation and to run for a minute
        Verify that specific zones have started for Program 1: \n
            Controller 1 Program 1:
                - Program will be started manually at 23:52
                    - Zones will be set to waiting because water needs to be requested from the FlowStation next
                - 23:53
                    - Watering starts (although statuses don't show it for another minute).  Flowmeters start reading

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # set an initial time on the controller
            self.config.BaseStation3200[1].set_date_and_time(_date='02/28/2014', _time='23:52:00')
            self.config.BaseStation3200[1].verify_date_and_time()

            self.config.BaseStation3200[2].set_date_and_time(_date='02/28/2014', _time='23:52:00')
            self.config.BaseStation3200[2].verify_date_and_time()

            self.config.FlowStations[1].set_date_and_time(_date='02/28/2014', _time='23:52:00')
            self.config.FlowStations[1].verify_date_and_time()

            # all zone have a status of done
            for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_done()

            # programs have a status of done
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()

            # verify water source status
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_ok()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_ok()

            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_ok()
            self.config.FlowStations[1].get_water_source(2).statuses.verify_status_is_ok()

            # verify POC status
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_off()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_off()

            # verify mainline status
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_off()

            # Manually start program 1
            self.config.BaseStation3200[1].programs[1].set_program_to_start()

            # 23:53
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # setting the flow rate on the flow meters.  Zones are pulling 200gpm, so just divide up equally
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=100)
            self.config.BaseStation3200[2].flow_meters[2].bicoder.set_flow_rate(_gallons_per_minute=100)

            # all zone have a status of waiting
            for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_waiting_to_water()

            # programs have a status of waiting
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            # verify water source status
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(2).statuses.verify_status_is_running()

            # verify POC status
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()

            # verify mainline status
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Increment to 23:54 (statuses will now show water is flowing)
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify zone status
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()

            # Program status
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # verify water source status
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(2).statuses.verify_status_is_running()

            # verify POC status
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()

            # verify mainline status
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        Incrementing forward to 23:57.  At this point, the zones and water paths are running

        Verify that:
            Controller 1 Program 1:
                - 23:57
                    - WS2 over budget message should be displayed, everything else still running as before
                - 23:58
                    - Finishing up last minute of runtime (system still running as before)

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            fm_rate = self.config.BaseStation3200[1].flow_meters[1].bicoder.vr
            ws2_budget = self.config.BaseStation3200[2].get_water_source(2).wb
            minutes_until_budget_met = ws2_budget / fm_rate

            # Increment to 23:57
            # This is the point where C2W2 has met its budget, and the 3200 has had time to act on that fact, so
            # we now have the overbudget message available
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=minutes_until_budget_met)

            # Update the flow meter object's flow usage to be 3 minutes worth so that our message verifier
            # uses correct usage value when generating the message.
            self.config.BaseStation3200[1].flow_meters[1].bicoder.vg = minutes_until_budget_met * fm_rate

            # Verify zone status
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()

            # Program status
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # verify water source status
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(2).statuses.verify_status_is_running()

            # verify POC status
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()

            # verify mainline status
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Verify C2W2 overbudget message
            self.config.BaseStation3200[2].get_water_source(2).messages.verify_exceed_monthly_budget_message()

            # Increment to 23:58
            # Program is still running
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify zone status
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()

            # Program status
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # verify water source status
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(2).statuses.verify_status_is_running()

            # verify POC status
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()

            # verify mainline status
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        Incrementing forward to 23:59.  At this point, the zones and water paths are idle, C2W2 is overbudget

        Verify that:
            Controller 1 Program 1:
                - 23:59
                    - System idle, C2W2 now showing OB state
                - 00:01
                    - System idle, C2W2 changes to OK status (due to month rollover)

        This verifies the water source status resets to OK from Over Budget when passing threshold of a new month. This
        indirectly verifies the monthly usage for the water source is reset.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment to 23:59
            # System is now idle, C2W2 is overbudget
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Program no longer running, flow goes to zero
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=0)
            self.config.BaseStation3200[2].flow_meters[2].bicoder.set_flow_rate(_gallons_per_minute=0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[2].flow_meters[2].bicoder.self_test_and_update_object_attributes()

            # all zone have a status of done
            for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_done()

            # programs have a status of done
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()

            # verify water source status
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_ok()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_over_budget()

            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_ok()
            self.config.FlowStations[1].get_water_source(2).statuses.verify_status_is_over_budget()

            # verify POC status
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_off()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_off()

            # verify mainline status
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_off()

            # Increment to 00:01
            # System is still idle, C2W2 is now status OK due to month rollover
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=2)

            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_ok()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_over_budget()

            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_ok()
            self.config.FlowStations[1].get_water_source(2).statuses.verify_status_is_over_budget()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
