import sys
from time import sleep
from datetime import datetime, timedelta
from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

__author__ = 'A Team'


class ControllerUseCase154_4(object):
    """
    Test name:
        - CN UseCase154_4 Flowstation Run Zone With Design Flow of Zero

    User Story:
       1. The user wants to use a FlowStation to manage flow on a 3200 that doesn't have a flowmeter.  The user might
       have not set design flows on his zones yet, but wants to manually run the zone while at the 3200.  User
       would expect that the FlowStation will still allocate water to the 3200 so that he can manually run the zone.
       2. The user wants to use a FlowStation to manage flow on a 3200 thaz doesn't have a flowmeter.  The user might
       have not set design flows on his zones yet, but wants to run a program containing those zones.  User would expect
       that the FlowStation will still allocate water to the 3200 so that he can have the zone water while the program
       is running.

    Purpose:
        - This test is to verify that, using a single FlowStation attached to a single 3200, the FlowStation can
          allocate flow to the 3200 so that a zone can run even though there is no design flow set on one
          of the 3200s zones.
        - Set up zones on the controller and attach them to a program
        - Make both zones timed zones.
        - Set up a non-zero design flow on one of the zones. The other zone should have a zero design flow.
        - Set up a POC and Mainline to supply water to the zones. \n

    Coverage Area: \n
        1.	Able to manual run a zone with a design flow of zero
        2.	Water is allocated and provided for a zone manual run
        3.	Zone successfully completes manual run runtime
        4.	Able to run a program that contains a zone with a design flow of zero, and the zone runs
        5.	Water is allocated and provided for the zone running under the program
        6.  Zone successfully completes program runtime

    Use Case explanation:
        - Single controller managed by a single FlowStation
        - Single water source, single POC, and a single mainline with two attached zones will be setup to verify
         the behavior

        First and second Scenario:
            Controller 1:
                - Zone 1 design flow = 50 GPM
                - Zone 2 design flow = 0 GPM
                - Zone 1 on program 1 with a runtime of 5 minutes (timed)
                - Zone 2 on program 1 with a runtime of 10 minutes (timed)
                - Program 1 start time 8:00am
            FlowStation:
                - WS/PC/ML Assignments:
                    C1:W1
                      |
                    C1:P1
                      |
                    C1:M1

    Expected behavior:
       Scenario 1:
       - Zone 2 will be manually run.  It should water, and upstream ML and POC should activate.  Zone 2 will then
       be manually stopped
        - The purpose of this test is to verify that a zone with a zero design flow will run when manually started

       Scenario 2:
       - Clock will be advanced to the start of program 1.
       - Zones 1 and 2 should run for the first 5 minutes. After zone 1 is complete, zone 2 will continue until it has
         reached the end of its runtime.
       - The purpose of this test is to verify that a zone with
         non-zero design flow will run in parallel with the zero-design-flow zone
       - The second purpose is to verify the zero-design-flow zone will continue to run alone even when the other zone
         has completed and turned off. The POC and water source will also continue to run.

    Date References:
        - configuration for script is located common\configuration_files\fs_json_config_files\FS_basic_design_flow_3.json
        - the devices and addresses range is read from the .json file
    """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/fs_json_config_files'
                                    )
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=False)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                     # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        ############################
        setup controller concurrency
        ############################
        We want the concurrency to not be a limiting factor in the test.

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=8)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        Set concurrency so it is not a limiting factor in the test
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=3)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=[1320])
            self.config.BaseStation3200[1].programs[1].set_priority_level(_pr_level=2)
            self.config.BaseStation3200[1].programs[1].set_seasonal_adjust(_percent=50)
            self.config.BaseStation3200[1].programs[1].set_water_window(_ww=['111111100000000000000111'])
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(_sun=False,
                                                                                                           _mon=True,
                                                                                                           _tues=False,
                                                                                                           _wed=True,
                                                                                                           _thurs=False,
                                                                                                           _fri=False,
                                                                                                           _sat=True)

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation3200[1].programs[2].set_enabled()
            self.config.BaseStation3200[1].programs[2].set_max_concurrent_zones(_number_of_zones=2)
            self.config.BaseStation3200[1].programs[2].set_start_times(_st_list=[1320])
            self.config.BaseStation3200[1].programs[2].set_priority_level(_pr_level=2)
            self.config.BaseStation3200[1].programs[2].set_seasonal_adjust(_percent=50)
            self.config.BaseStation3200[1].programs[2].set_water_window(_ww=['111111100000000000011111'])
            self.config.BaseStation3200[1].programs[2].set_watering_intervals_to_selected_days_of_the_week(_sun=False,
                                                                                                           _mon=True,
                                                                                                           _tues=False,
                                                                                                           _wed=True,
                                                                                                           _thurs=False,
                                                                                                           _fri=False,
                                                                                                           _sat=False)

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=3)
            self.config.BaseStation3200[1].programs[3].set_enabled()
            self.config.BaseStation3200[1].programs[3].set_max_concurrent_zones(_number_of_zones=2)
            self.config.BaseStation3200[1].programs[3].set_start_times(_st_list=[60])
            self.config.BaseStation3200[1].programs[3].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[3].set_seasonal_adjust(_percent=100)


        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        We are only testing that the zones will run in parallel with each other, and the second zone will continue
        running after the first zone completes
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add zone programs to Program 1

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=10)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].add_moisture_sensor_to_primary_zone(_moisture_sensor_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_water_strategy_to_lower_limit()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_lower_limit_threshold(_percent=27.3)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=26.6)


            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=67)
            self.config.BaseStation3200[1].programs[1].zone_programs[67].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=73)
            self.config.BaseStation3200[1].programs[1].zone_programs[73].set_as_linked_zone(_primary_zone=1)


            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[2].zone_programs[4].set_run_time(_minutes=30)
            self.config.BaseStation3200[1].programs[2].zone_programs[4].set_cycle_time(_minutes=15)
            self.config.BaseStation3200[1].programs[2].zone_programs[4].set_soak_time(_minutes=10)

            self.config.BaseStation3200[1].programs[2].zone_programs[4].set_as_timed_zone()

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=5)
            self.config.BaseStation3200[1].programs[2].zone_programs[5].set_run_time(_minutes=10)
            self.config.BaseStation3200[1].programs[2].zone_programs[5].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[2].zone_programs[5].set_soak_time(_minutes=10)

            self.config.BaseStation3200[1].programs[2].zone_programs[5].set_as_timed_zone()

            for zone_address in range(10, 17):
                self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=zone_address)
                self.config.BaseStation3200[1].programs[2].zone_programs[zone_address].set_run_time(_minutes=90)
                self.config.BaseStation3200[1].programs[2].zone_programs[zone_address].set_as_timed_zone()

            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=8)
            self.config.BaseStation3200[1].programs[3].zone_programs[8].set_run_time(_minutes=10)
            self.config.BaseStation3200[1].programs[3].zone_programs[8].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[3].zone_programs[8].set_soak_time(_minutes=10)

            self.config.BaseStation3200[1].programs[3].zone_programs[8].set_as_timed_zone()

            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=9)
            self.config.BaseStation3200[1].programs[3].zone_programs[9].set_run_time(_minutes=10)
            self.config.BaseStation3200[1].programs[3].zone_programs[9].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[3].zone_programs[9].set_soak_time(_minutes=10)

            self.config.BaseStation3200[1].programs[3].zone_programs[9].set_as_timed_zone()



        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
        No budgets are used so it will not disable our water sources
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Water Source 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].get_water_source(1).set_enabled()

            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=2)
            self.config.BaseStation3200[1].get_water_source(2).set_enabled()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n

        We are not setting any limiting factors (shutdowns, priorities, etc.).
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].get_point_of_control(1).set_enabled()
            self.config.BaseStation3200[1].get_point_of_control(1).set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].get_water_source(1).add_point_of_control_to_water_source(1)

            # POC 2
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=2)
            self.config.BaseStation3200[1].get_point_of_control(2).set_enabled()
            self.config.BaseStation3200[1].get_point_of_control(2).set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].get_water_source(2).add_point_of_control_to_water_source(2)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control

        Set the mainline design flows so it will not limit the flow.
        We assume that if the mainline is managed by the FlowStation it will automatically limit concurrency by flow.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Mainline 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].get_mainline(1).set_enabled()
            self.config.BaseStation3200[1].get_mainline(1).set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].get_point_of_control(1).add_mainline_to_point_of_control(_mainline_address=1)
            self.config.BaseStation3200[1].get_point_of_control(2).add_mainline_to_point_of_control(_mainline_address=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone

        Setting controller 1 zone 1 to be higher than the design flow of the mainline so we can verify it will run alone.

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add Zone 1 to Mainline 1
            # for zone_address in range(1, 2):
            #     self.config.BaseStation3200[1].get_mainline(1).add_zone_to_mainline(_zone_address=zone_address)
            #
            # self.config.BaseStation3200[1].ser.send_and_wait_for_reply("set,zn=2,ml=0")
            # self.config.BaseStation3200[1].ser.send_and_wait_for_reply("set,zn=4,ml=0")

            self.config.BaseStation3200[1].get_mainline(ml_number=1).remove_zone_from_mainline(_zone_address=2)
            self.config.BaseStation3200[1].get_mainline(ml_number=1).remove_zone_from_mainline(_zone_address=4)

            self.config.BaseStation3200[1].zones[1].set_design_flow(_gallons_per_minute=0)
            self.config.BaseStation3200[1].zones[2].set_design_flow(_gallons_per_minute=0)
            self.config.BaseStation3200[1].zones[3].set_design_flow(_gallons_per_minute=0)
            self.config.BaseStation3200[1].zones[4].set_design_flow(_gallons_per_minute=0)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def x_step_8(self):
        """
        ###############################
        Share controller, water source, point of control, mainline with Flow Station
        ###############################
            - Share controller1 with the flow station
            - Add and share a Water Source between controller1 and the FlowStation
            - Add and share Point of Control between controller1 and the FlowStation
            - Add and share Mainline between controller1 and the FlowStation

            - Assign WS, POC, and ML on the FlowStation
            - WS/PC/ML Assignments:
                    C1:W1
                      |
                    C1:P1
                      |
                    C1:M1
            -By assigning the controller WS, POC, and MLs to the FlowStation we automatically setting the managed by
            FlowStation setting for each.
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add controller 1 to FlowStation
            self.config.FlowStations[1].add_controller_to_flow_station(
                _controller_address=1,
                _flow_station_slot_number=1)

            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=1,
                _controller_water_source_address=1,
                _flow_station_water_source_slot_number=1)
            self.config.FlowStations[1].get_water_source(1).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=1,
                _controller_point_of_control_address=1,
                _flow_station_point_of_control_slot_number=1)
            self.config.FlowStations[1].get_point_of_control(1).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=1,
                _controller_mainline_address=1,
                _flow_station_mainline_slot_number=1)
            self.config.FlowStations[1].get_mainline(1).set_manage_by_flowstation()

        # Assign WS, POC, and ML on FlowStation
            # WS1 -> PC1 -> ML1
            self.config.FlowStations[1].get_water_source(ws_number=1).add_point_of_control_to_water_source(_point_of_control_address=1)
            self.config.FlowStations[1].get_point_of_control(pc_number=1).add_mainline_to_point_of_control(_mainline_address=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
            #                                                        controllers=self.config.BaseStation3200,
            #                                                        minutes=1)
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_full_configuration()
            # self.config.FlowStations[1].verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        Set the time to be 7:59, which is 1 minute before both of our program's start times \n
        Verify that nothing is started: \n
            - Verify that no Programs are running \n
            - Verify that no zones are watering \n
        Increment the clock two minutes which should trigger our Program to start \n
        Verify that specific zones have started for each program below: \n
            Controller 1 Program 1:
                - Program starts at 8:01am
                    - Zones will be set to waiting because water needs to be requested from the FlowStation next
                - 8:02am
                    - Both Zone 1 and zone 2 are running, since we have enough water allocated for both
                    
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_date_and_time(_date='05/07/2018', _time='21:55:00')
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()
            self.config.BaseStation3200[1].programs[3].set_program_to_stop()

            # self.config.FlowStations[1].set_date_and_time(_date='02/12/2017', _time='07:59:00')
            # self.config.FlowStations[1].verify_date_and_time()

            # all zone have a status of done
            # for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
            #     self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_done()

            # programs have a status of done
            # self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()

            # verify water source status
            # self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_ok()
            # # self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_ok()
            #
            # # verify POC status
            # self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_off()
            # # self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_off()

            # verify mainline status
            # self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_off()
            # self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_off()

            # # This should trigger the Programs to start at 8:01am and ZN 2 turn on
            # helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
            #                                                        controllers=self.config.BaseStation3200,
            #                                                        minutes=2)
            count = 0
            upper_limit = 240
            while count < upper_limit:
                self.config.BaseStation3200[1].do_increment_clock(minutes=1)
                if count < upper_limit:
                    count += 1
                else:
                    break
                if count % 60 == 0:
                    pass
                if count == 19:
                    pass
                if count == 115:
                    pass
                for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                    self.config.BaseStation3200[1].zones[zone_address].statuses.get_status()
                    test_status = self.config.BaseStation3200[1].zones[zone_address].ss
                    print test_status
                for program_address in sorted(self.config.BaseStation3200[1].programs.keys()):
                    self.config.BaseStation3200[1].programs[program_address].statuses.get_status()
                    program_status = self.config.BaseStation3200[1].programs[program_address].ss
                    print program_status
                print count
                print self.config.BaseStation3200[1].get_date_and_time()




            self.config.BaseStation3200[1].do_increment_clock(minutes=3)


            # TODO: 5/2/18 READ BELOW COMMENTS:
            #   Verify ZN2 turns on because it has a 0 design flow. This isn't expected behavior, but until the 3200
            #   changes behavior for zones with 0 design flow, we are verifying against the current behavior.

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()

            # programs have a status of waiting
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # verify water source status
            # self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            # self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_ok()

            # verify POC status
            # self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            # self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_off()

            # verify mainline status
            # self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            # self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # increment clock forward 2 minutes (to 8:03) to allow FlowStation and 3200 to finish water allocation
            # so that PG can turn on ZN1 with a non-zero design flow.
            # helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
            #                                                        controllers=self.config.BaseStation3200,
            #                                                        minutes=2)
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)


            # Verify zone status
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()

            # Program status
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # verify water source status
            # self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            # self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            # verify POC status
            # self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            # self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()

            # verify mainline status
            # self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            # self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        BELOW IS EXPECTED BEHAVIOR:

        Set the time to be 8:08, which is 5 minute after the start of the programs and the runtime of controller 1
        zone 1.

        Increment the clock 6 minutes which should finish the runtime of controller 1 zone 1 \n
        Verify that specific zones status: \n
            Controller 1 Program 1:
                - 8:08am
                    - Zone 1 has completed it runtime and is set to done. Zone 2 is still watering since it has a 10
                      minute runtime and 0GPM design flow

        UPDATE 5/2/2018:
            Adjusted time incremented forward from 6 minutes to 7 minutes to allow for ZN1 to finish its run cycle.
            The expected behavior is for ZN1 to finish at 8:08. But current behavior has ZN1 finishing at 8:10.

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment to 8:08am (check step comments, incrementing forward to 8:10 for now)
            # helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
            #                                                        controllers=self.config.BaseStation3200,
            #                                                        minutes=7)

            self.config.BaseStation3200[1].do_increment_clock(minutes=20)


            # Verify zone status
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()

            # Program status
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # verify water source status
            # self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_ok()
            # self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_ok()

            # verify POC status
            # self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_off()
            # self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_off()

            # verify mainline status
            # self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            # self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
            #                                                        controllers=self.config.BaseStation3200,
            #                                                        minutes=10)
            self.config.BaseStation3200[1].do_increment_clock(minutes=10)

            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

