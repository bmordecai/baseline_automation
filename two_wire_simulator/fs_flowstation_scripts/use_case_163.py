import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration

# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr

__author__ = 'Ben'


class ControllerUseCase163(object):
    """
    Test name:
        - POC Grouping with multiple 3200's managed by a FlowStation

    User Story: \n

        1)  As a user, I would like to limit my flow when irrigating to the lowest required flow rate. This would allow
            me to view more accurate flow data and help better manage my system.

    Coverage and Objectives:

        1.	When a mainline has multiple upstream POCs that are in the same poc group:
            a.	The mainline will turn on the POC (only one at a time) with the least amount of GPM that it takes to
                fulfill the mainlines requested GPM
                i.	eg: If POC 1, 2, 3 have a design flow of 50, 500, 1000 respectively, and are in the same POC group.
                    If mainline that is attached to all 3 POCs requests 50 GPM, then only POC 1 will be turned on. If
                    the mainline requests 550 GPM, only POC 3 is turned on
            b.	The master valve of the POC that is turned on should have an "on" status
            c.	Remaining POCs that have normally open master valves will have their master valves closed, and the
                status of the master valve will reflect that

        2.	When a mainline has multiple upstream pocs that are not in the same poc group:
            a.	The mainline will turn on all the upstream POCs, even if only one is used
            b.	The master valve of all POCs that are turned on should have an "on" status


    Use Case explanation:
        - Two controllers managed by a single FlowStation
        - Two water sources, two POCs, and single mainline with 3 zones.

        First Scenario:
            - Multiple upstream points of control assigned to the same downstream mainline with master valves normally
              open.
                - Controller 1:
                    - WS1 budget 0 (non-limiting) without shutdown and rationing disabled
                    - PC1 design flow 50 GPM - FlowStation POC Group 1
                        - MV1 normally open
                        - FM1
                    - ML1 design flow 50 GPM to limit zones by flow (FS managed)
                        - ZN 1 DF 25 GPM
                        - ZN 2 DF 25 GPM
                        - ZN 3 DF 100 GPM
                    - PG1
                        - 8:00am start time
                        - ZN1 timed 5 min runtime (no cycle/soak)
                        - ZN2 timed 5 min runtime (no cycle/soak)
                        - ZN3 timed 5 min runtime (no cycle/soak)
                - Controller 2:
                    - WS8 budget 0 (non-limiting) without shutdown and rationing disabled
                    - PC8 design flow 150 GPM - FlowStation POC Group 1
                        - MV8 normally closed
                        - FM8

            FlowStation:
                - WS/PC/ML Assignments:

                         C1:W1
                        /    \
                     C1:P1   C2:P8
                        \     |
                         C1:M1
                           |
                      ZN1<-|
                           |->ZN2
                      ZN3<-|

    Not Covered:
        1.	Single 3200 managed by FlowStation
        2.	Dynamic Flow Allocation
        3.	Running a single zone that exceeds the design flow of any POC in the POC group
        4.	Pump on the POC
        5.	Concurrent Zones
        6.	Pressure flow stabilization
        7.	Mainline zone delays
        8.	Parallel POCs to a single mainline

    Date References:
        - configuration for script is located common\configuration_files\using_real_time_flow.json
        - the devices and addresses range is read from the .json file
    """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/fs_json_config_files'
                                    )
        self.run_use_case()

    #################################
    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=False)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]

                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))

                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        ############################
        setup controller concurrency
        ############################
        We want the concurrency to not be a limiting factor in the test.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=15)
            self.config.BaseStation3200[2].set_max_concurrent_zones(_max_zones=15)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
            
        Set concurrency to 2 so that 2 zones run at a time.
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=2)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=[480])

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        helper_methods.print_method_name()
        try:
            # Add zones programs to Program 1
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_timed_zone()
            
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_run_time(_minutes=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_timed_zone()

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_run_time(_minutes=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_as_timed_zone()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n

        Not setting budgets or shutdowns because we don't want it to be a limiting factor for the test.

        Not setting water source priorities to allow the FlowStation to default all water sources to the same priority.
        """
        helper_methods.print_method_name()
        try:
            # C1 Water Source 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].get_water_source(1).set_enabled()
            self.config.BaseStation3200[1].get_water_source(1).set_monthly_watering_budget(_budget=0)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n

        We are not setting any limiting factors (shutdowns, priorities, etc.).

        C1P1 has DF of 50 to force first two zones to use it as the flow provider.

        Setting MV1 to normally open to test FlowStation shutting down normally open MV correctly when its POC is not
        being used to provide water.

        C2P8 has DF of 100 to force the third zone to use it as the flow provider.

        Setting MV8 to normally closed to test FlowStation opening a normally closed MV correctly when its POC is being
        used to provide water.
        """
        helper_methods.print_method_name()
        try:
            # C1 POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].get_point_of_control(1).set_enabled()
            self.config.BaseStation3200[1].get_point_of_control(1).set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].get_point_of_control(1).add_flow_meter_to_point_of_control(_flow_meter_address=1)
            self.config.BaseStation3200[1].get_point_of_control(1).add_master_valve_to_point_of_control(_master_valve_address=1)
            self.config.BaseStation3200[1].master_valves[1].set_normally_open_state(_normally_open=opcodes.true)

            # C2 POC 8
            self.config.BaseStation3200[2].add_point_of_control_to_controller(_point_of_control_address=8)
            self.config.BaseStation3200[2].get_point_of_control(8).set_enabled()
            self.config.BaseStation3200[2].get_point_of_control(8).set_target_flow(_gpm=150)
            self.config.BaseStation3200[2].get_point_of_control(8).add_master_valve_to_point_of_control(_master_valve_address=8)
            self.config.BaseStation3200[2].get_point_of_control(8).add_flow_meter_to_point_of_control(_flow_meter_address=8)
            self.config.BaseStation3200[2].master_valves[8].set_normally_open_state(_normally_open=opcodes.false)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control

        Set mainline design flows so it will limit the number of zones it can run to 2 concurrently.

        We assume that if the mainline is managed by the FlowStation it will automatically limit concurrency by flow.
        """
        helper_methods.print_method_name()
        try:
            # Mainline 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].get_mainline(1).set_enabled()
            self.config.BaseStation3200[1].get_mainline(1).set_target_flow(_gpm=100)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone
        Set design flow such that when they are all running they do not hit the limit of the upstream POC's and mainlines
        """
        helper_methods.print_method_name()
        try:
            # Add Zones 1-4 to Mainline 1
            for zone_address in range(1, 4):
                self.config.BaseStation3200[1].get_mainline(1).add_zone_to_mainline(_zone_address=zone_address)

            self.config.BaseStation3200[1].zones[1].set_design_flow(_gallons_per_minute=25)
            self.config.BaseStation3200[1].zones[2].set_design_flow(_gallons_per_minute=25)
            self.config.BaseStation3200[1].zones[3].set_design_flow(_gallons_per_minute=100)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        Share controller, water source, point of control, mainline with Flow Station
        ###############################
            - Share controller1 with the flow station
            - Add and share Water Sources between controller 1 and the FlowStation
            - Add and share Points of Control between controller 1 and the FlowStation
            - Add and share Mainlines between controller 1 and the FlowStation

            - Share controller2 with the flow station
            - Add and share a Water Source between controller 2 and the FlowStation
            - Add and share Point of Control between controller 2 and the FlowStation

            - Assign WS, POC, and ML on the FlowStation

            FlowStation:
                - WS/PC/ML Assignments:

                         C1:W1
                        /    \
                     C1:P1   C2:P8
                        \     |
                         C1:M1

            - By assigning the controller WS, POC, and MLs to the FlowStation we automatically setting the managed by
              FlowStation setting for each.

            - Grouped POCs for the purpose of the test.
        """
        helper_methods.print_method_name()
        try:

            # Add controller 1 to FlowStation
            self.config.FlowStations[1].add_controller_to_flow_station(
                _controller_address=1,
                _flow_station_slot_number=1)

            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=1,
                _controller_water_source_address=1,
                _flow_station_water_source_slot_number=1)
            self.config.BaseStation3200[1].get_water_source(1).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=1,
                _controller_point_of_control_address=1,
                _flow_station_point_of_control_slot_number=1)
            self.config.BaseStation3200[1].get_point_of_control(1).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=1,
                _controller_mainline_address=1,
                _flow_station_mainline_slot_number=1)
            self.config.BaseStation3200[1].get_mainline(1).set_manage_by_flowstation()

            # Add controller 2 to FlowStation
            self.config.FlowStations[1].add_controller_to_flow_station(
                _controller_address=2,
                _flow_station_slot_number=2)

            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=2,
                _controller_point_of_control_address=8,
                _flow_station_point_of_control_slot_number=8)
            self.config.BaseStation3200[2].get_point_of_control(8).set_manage_by_flowstation()

            # Assign WS, POC, and ML on FlowStation
            # WS1 -> PC1 -> ML1
            self.config.FlowStations[1].get_water_source(1).add_point_of_control_to_water_source(_point_of_control_address=1)
            self.config.FlowStations[1].get_point_of_control(1).add_mainline_to_point_of_control(_mainline_address=1)

            # WS1 -> PC8 -> ML1
            self.config.FlowStations[1].get_water_source(1).add_point_of_control_to_water_source(_point_of_control_address=8)
            self.config.FlowStations[1].get_point_of_control(8).add_mainline_to_point_of_control(_mainline_address=1)

            # Group POCs
            self.config.FlowStations[1].get_point_of_control(1).set_group(_group_number=1)
            self.config.FlowStations[1].get_point_of_control(8).set_group(_group_number=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        helper_methods.print_method_name()
        try:

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.BaseStation3200[2].verify_full_configuration()
            self.config.FlowStations[1].verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        Set the time to be 7:59, which is 1 minute before Program 1's start time of 8:00 a.m. \n

        Verify that nothing is started: \n
            - Verify that no Programs are running \n
            - Verify that no zones are watering \n

        Increment the clock two minutes which should trigger Program 1 to start and status' to visibly change \n

        Verify: \n
            - PG 1 goes to "Waiting" while FS/3200's finish water allocation requests
            - ZN 1-3 go to "Waiting"
            - Controller 1:
                - WS1 will go to "Running" because it's downstream POC has the lowest available flow rate to provide
                  for the first group of zones turning on.
                - PC1 will go to "Running" because it has the lowest available flow rate to provide for the first group
                  of zones turning on. (Grouped with C2P8)
                    - MV1 is "Watering" (normally open)
                - ML1 will go to "Running" because its zones are turning on.
            - Controller 2:
                - WS8 and PC8 will remain "OK" and "OFF" respectively because it's available flow is not the smallest
                  for the current group of zones turning on. (Grouped with C1P1)
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseStation3200[1].set_date_and_time(_date='02/12/2018', _time='7:59:00')
            self.config.BaseStation3200[1].verify_date_and_time()

            self.config.BaseStation3200[2].set_date_and_time(_date='02/12/2018', _time='7:59:00')
            self.config.BaseStation3200[2].verify_date_and_time()

            self.config.FlowStations[1].set_date_and_time(_date='02/12/2018', _time='07:59:00')
            self.config.FlowStations[1].verify_date_and_time()

            # Increment to 8:01am to trigger C1 PG1 start time (8:00am) and start water allocation requests
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=2)

            # verify water source status
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            # verify POC status
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()

            self.config.BaseStation3200[2].get_point_of_control(8).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_point_of_control(8).statuses.verify_status_is_off()

            # MV status
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].master_valves[8].statuses.verify_status_is_off()

            # FM status
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_ok()
            self.config.BaseStation3200[2].flow_meters[8].statuses.verify_status_is_ok()

            # verify mainline status
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Program status
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            # Verify zone status
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        Set C1:FM1 rate to be the expected flow rate of ZN1, ZN2.

        Increment the clock forward 1 minute to see "running" status' for the system.

        Verify: \n
            - PG1 goes to "Running" while FS/3200's finish water allocation requests
            - ZN1 and ZN2 goes to "Watering" b/c ML1 is being managed by the FlowStation which automatically enables
              managing by flow on the mainline.
            - ZN3 goes to "Waiting" since ML1 is managing by flow.
            - Controller 1:
                - WS1/PC1/ML1 remain "Running"
                - MV1 is "Watering"
            - Controller 2:
                - WS8 and PC8 will remain "OK" and "OFF" respectively because it's available flow is not the smallest
                  for the current group of zones turning on. (Grouped with C1P1)
        """
        helper_methods.print_method_name()
        try:

            # Set rate equal to ZN1 and ZN2's expected flow
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=50.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()

            # Increment to 8:02am to show "running" statuses
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # verify water source status
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            # verify POC status
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()

            self.config.BaseStation3200[2].get_point_of_control(8).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_point_of_control(8).statuses.verify_status_is_off()

            # MV status
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].master_valves[8].statuses.verify_status_is_off()

            # FM status
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[2].flow_meters[8].statuses.verify_status_is_ok()

            # verify mainline status
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Program status
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Verify zone status
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        Increment clock forward 4 minutes to simulate through:
            (1) ZN1 and ZN2 finishing their 5 minute runtime's
            (2) 3200 requesting more water for ZN3's larger expected flow (100 GPM)

        Set C1:FM1 rate to 0 because its POC will be turned off because it doesn't have enough flow for the next group
        of zones.

        Set C2:FM8 rate to 100 GPM (the expected flow rate of ZN3 when it is turned on) because its POC will be turned
        on and used to provide flow.

        Increment the clock forward 1 minute to see next group of Zones "running" status' and "running" status for the
        system.

        Verify: \n
            - PG1 is "Running" while FS/3200's finish water allocation requests
            - ZN1 and ZN2 goes to "Done" b/c they finished their run times.
            - ZN3 goes to "Watering"
            - Controller 1:
                - WS1/PC1 are turned off b/c PC1 can't provide enough water to the downstream requests.
                - ML1 remain "Running"
                - MV1 (normally open) should be turned of by the FS b/c POCs are grouped.
            - Controller 2:
                - WS8 and PC8 should get turned on b/c the POCs are grouped and C2P8 has enough flow to provide for ZN3.

        This verifies the user story and basic use case with POC grouping:
            - Grouped POCs are run one at a time based on requested flow from downstream ML (by themselves)
            - Normally open MVs are closed
        """
        helper_methods.print_method_name()
        try:
            # Increment to 8:06am
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=4)

            # Set C1FM1 rate to 0 b/c POC is off
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=0.0)

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # JIRA BUG: FLOWSTN-315 (https://baseline.atlassian.net/browse/FLOWSTN-315) fixed 5/12/18.
            # 3200 now returns running status for this water source
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            # verify POC status
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_off()

            self.config.BaseStation3200[2].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(8).statuses.verify_status_is_running()

            # MV status
            # Fixed: v16.1.609 JIRA BUG: ZZ-1766, ZZ-1767
            # We are expecting MV1 which is NO master valve to be closed
            # 3200 is not shutting down MV1 when given a shutdown message from FlowStation
            # We are expecting MV8 which is NC master valve to be open
            # 3200 is not updating the MV8 till the next minute
            # Swap the two lines below for the other two once fixed
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[2].master_valves[8].statuses.verify_status_is_watering()

            # FM status
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_ok()
            self.config.BaseStation3200[2].flow_meters[8].statuses.verify_status_is_ok()

            # verify mainline status
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Program status
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            # Verify zone status
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()

            # Set C2FM8 rate to 100 b/c POC8 is being turned on
            self.config.BaseStation3200[2].flow_meters[8].bicoder.set_flow_rate(_gallons_per_minute=100.0)

            # Increment to 8:02am to show "running" statuses
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # JIRA BUG: FLOWSTN-315 (https://baseline.atlassian.net/browse/FLOWSTN-315) is fixed.
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            # verify POC status
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_off()

            self.config.BaseStation3200[2].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(8).statuses.verify_status_is_running()

            # MV status
            # Fixed: v16.1.609 JIRA BUG: ZZ-1766, ZZ-1767 See above notes on these issues
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[2].master_valves[8].statuses.verify_status_is_watering()

            # FM status
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_ok()
            self.config.BaseStation3200[2].flow_meters[8].statuses.verify_status_is_running()

            # verify mainline status
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Program status
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Verify zone status
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
