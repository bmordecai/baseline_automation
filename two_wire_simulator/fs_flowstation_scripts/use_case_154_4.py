import sys
from common.imports import opcodes
from time import sleep
from datetime import datetime, timedelta
from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *
from common.objects.bicoders.valve_bicoder import ValveBicoder
from common.objects.devices.zn import Zone

from csv_handler import CSVWriter


# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

__author__ = 'A Team'


class ControllerUseCase154_4(object):
    """
    Test name:
        - CN UseCase154_4 Flowstation Run Zone With Design Flow of Zero

    User Story:
       1. The user wants to use a FlowStation to manage flow on a 3200 that doesn't have a flowmeter.  The user might
       have not set design flows on his zones yet, but wants to manually run the zone while at the 3200.  User
       would expect that the FlowStation will still allocate water to the 3200 so that he can manually run the zone.
       2. The user wants to use a FlowStation to manage flow on a 3200 thaz doesn't have a flowmeter.  The user might
       have not set design flows on his zones yet, but wants to run a program containing those zones.  User would expect
       that the FlowStation will still allocate water to the 3200 so that he can have the zone water while the program
       is running.

    Purpose:
        - This test is to verify that, using a single FlowStation attached to a single 3200, the FlowStation can
          allocate flow to the 3200 so that a zone can run even though there is no design flow set on one
          of the 3200s zones.
        - Set up zones on the controller and attach them to a program
        - Make both zones timed zones.
        - Set up a non-zero design flow on one of the zones. The other zone should have a zero design flow.
        - Set up a POC and Mainline to supply water to the zones. \n

    Coverage Area: \n
        1.	Able to manual run a zone with a design flow of zero
        2.	Water is allocated and provided for a zone manual run
        3.	Zone successfully completes manual run runtime
        4.	Able to run a program that contains a zone with a design flow of zero, and the zone runs
        5.	Water is allocated and provided for the zone running under the program
        6.  Zone successfully completes program runtime

    Use Case explanation:
        - Single controller managed by a single FlowStation
        - Single water source, single POC, and a single mainline with two attached zones will be setup to verify
         the behavior

        First and second Scenario:
            Controller 1:
                - Zone 1 design flow = 50 GPM
                - Zone 2 design flow = 0 GPM
                - Zone 1 on program 1 with a runtime of 5 minutes (timed)
                - Zone 2 on program 1 with a runtime of 10 minutes (timed)
                - Program 1 start time 8:00am
            FlowStation:
                - WS/PC/ML Assignments:
                    C1:W1
                      |
                    C1:P1
                      |
                    C1:M1

    Expected behavior:
       Scenario 1:
       - Zone 2 will be manually run.  It should water, and upstream ML and POC should activate.  Zone 2 will then
       be manually stopped
        - The purpose of this test is to verify that a zone with a zero design flow will run when manually started

       Scenario 2:
       - Clock will be advanced to the start of program 1.
       - Zones 1 and 2 should run for the first 5 minutes. After zone 1 is complete, zone 2 will continue until it has
         reached the end of its runtime.
       - The purpose of this test is to verify that a zone with
         non-zero design flow will run in parallel with the zero-design-flow zone
       - The second purpose is to verify the zero-design-flow zone will continue to run alone even when the other zone
         has completed and turned off. The POC and water source will also continue to run.

    Date References:
        - configuration for script is located common\configuration_files\fs_json_config_files\FS_basic_design_flow_3.json
        - the devices and addresses range is read from the .json file
    """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/fs_json_config_files'
                                    )

        # initializes the csv writer object so it can be used in the remainder of the use_case
        self.csv_writer = CSVWriter(file_name=test_name,
                                    relative_path='fs_flowstation_scripts',
                                    delimiter=',',
                                    line_terminator='\n')
        # Configures how the logging will appear in the rest of the use case
        # logging.basicConfig(filename=str(os.path.dirname(os.path.abspath(__file__))) + '/' + test_name + ".txt",
        #                     level=logging.INFO, filemode='w', format='%(levelname)s:%(message)s')
        try:
            self.csv_writer.open()
        except IOError:
            e_msg = self.csv_writer.fname + ' file is in use'
            raise IOError(e_msg)

        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=False)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]

                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))

                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()


                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        ############################
        setup controller concurrency
        ############################
        We want the concurrency to not be a limiting factor in the test.

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=8)

            # Load 12-valve boards
            self.config.BaseStation3200[1].send_command_with_reply("DEV,DD=VA00001")
            self.config.BaseStation3200[1].send_command_with_reply("DEV,DD=VB00101")
            self.config.BaseStation3200[1].send_command_with_reply("DEV,DD=VA75601")
            self.config.BaseStation3200[1].send_command_with_reply("DEV,DD=VB75601")
            self.config.BaseStation3200[1].send_command_with_reply("DEV,DD=VAA0601")
            self.config.BaseStation3200[1].send_command_with_reply("DEV,DD=VBA0601")

            # Search for zones
            self.config.BaseStation3200[1].do_search_for_zones()

            serial_numbers = {
                12: "VA00012",
                27: "VB00110",
                36: "VB00107",
                52: "VA75611",
                55: "VB75602",
                # 70: "VAA0612",
                71: "VBA0601",
                76: "VBA0606",
                79: "VBA0609",
                80: "VBA0610"
            }

            for zone_ad in sorted(serial_numbers.keys()):

                # Create bicoder for Zone
                valve_bicoder = ValveBicoder(_sn=serial_numbers[zone_ad],
                                             _controller=self.config.BaseStation3200[1],
                                             _id=opcodes.zone,
                                             _address=zone_ad,
                                             _decoder_type=opcodes.twelve_valve_decoder)

                # Create Zone with bicoder
                zone = Zone(_controller=self.config.BaseStation3200[1],
                            _address=zone_ad,
                            _valve_bicoder=valve_bicoder)

                # Add zone to controller dict
                self.config.BaseStation3200[1].zones[zone_ad] = zone

                # Address and set default values for Zone
                zone.set_address()
                zone.set_default_values()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        Set concurrency so it is not a limiting factor in the test
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=12)
            self.config.BaseStation3200[1].programs[12].set_enabled()
            self.config.BaseStation3200[1].programs[12].set_max_concurrent_zones(_number_of_zones=3)
            self.config.BaseStation3200[1].programs[12].set_start_times(_st_list=[960])
            self.config.BaseStation3200[1].programs[12].set_priority_level(_pr_level=2)
            self.config.BaseStation3200[1].programs[12].set_seasonal_adjust(_percent=50)
            self.config.BaseStation3200[1].programs[12].set_water_window(_ww=['111111111111111111111111'])
            self.config.BaseStation3200[1].programs[12].set_watering_intervals_to_selected_days_of_the_week(_sun=True,
                                                                                                            _mon=False,
                                                                                                            _tues=True,
                                                                                                            _wed=False,
                                                                                                            _thurs=False,
                                                                                                            _fri=True,
                                                                                                            _sat=False)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        We are only testing that the zones will run in parallel with each other, and the second zone will continue
        running after the first zone completes
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add zone programs to Program 1
            self.config.BaseStation3200[1].programs[12].add_zone_to_program(_zone_address=12)
            self.config.BaseStation3200[1].programs[12].zone_programs[12].set_run_time(_minutes=45)
            self.config.BaseStation3200[1].programs[12].zone_programs[12].set_cycle_time(_minutes=15)
            self.config.BaseStation3200[1].programs[12].zone_programs[12].set_soak_time(_minutes=10)
            self.config.BaseStation3200[1].programs[12].zone_programs[12].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[12].zone_programs[12].set_water_strategy_to_timed()

            self.config.BaseStation3200[1].programs[12].add_zone_to_program(_zone_address=27)
            self.config.BaseStation3200[1].programs[12].zone_programs[27].set_as_linked_zone(_primary_zone=12)

            self.config.BaseStation3200[1].programs[12].add_zone_to_program(_zone_address=36)
            self.config.BaseStation3200[1].programs[12].zone_programs[36].set_as_linked_zone(_primary_zone=12)

            self.config.BaseStation3200[1].programs[12].add_zone_to_program(_zone_address=52)
            self.config.BaseStation3200[1].programs[12].zone_programs[52].set_as_linked_zone(_primary_zone=12)

            self.config.BaseStation3200[1].programs[12].add_zone_to_program(_zone_address=55)
            self.config.BaseStation3200[1].programs[12].zone_programs[55].set_as_linked_zone(_primary_zone=12)

            self.config.BaseStation3200[1].programs[12].add_zone_to_program(_zone_address=71)
            self.config.BaseStation3200[1].programs[12].zone_programs[71].set_as_linked_zone(_primary_zone=12)

            self.config.BaseStation3200[1].programs[12].add_zone_to_program(_zone_address=76)
            self.config.BaseStation3200[1].programs[12].zone_programs[76].set_as_linked_zone(_primary_zone=12)

            self.config.BaseStation3200[1].programs[12].add_zone_to_program(_zone_address=79)
            self.config.BaseStation3200[1].programs[12].zone_programs[79].set_as_linked_zone(_primary_zone=12)

            self.config.BaseStation3200[1].programs[12].add_zone_to_program(_zone_address=80)
            self.config.BaseStation3200[1].programs[12].zone_programs[80].set_as_linked_zone(_primary_zone=12)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
        No budgets are used so it will not disable our water sources
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Water Source 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].get_water_source(1).set_enabled()
            self.config.BaseStation3200[1].get_water_source(1).set_priority(_priority_for_water_source=2)

            # Water Source 2
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=2)
            self.config.BaseStation3200[1].get_water_source(2).set_enabled()
            self.config.BaseStation3200[1].get_water_source(2).set_priority(_priority_for_water_source=2)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n

        We are not setting any limiting factors (shutdowns, priorities, etc.).
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].get_point_of_control(1).set_enabled()
            self.config.BaseStation3200[1].get_point_of_control(1).set_target_flow(_gpm=260)
            self.config.BaseStation3200[1].get_point_of_control(1).set_high_flow_limit(_limit=300)
            self.config.BaseStation3200[1].get_point_of_control(1).set_unscheduled_flow_limit(_gallons=60)
            self.config.BaseStation3200[1].get_water_source(1).add_point_of_control_to_water_source(1)
            self.config.BaseStation3200[1].get_point_of_control(1).add_flow_meter_to_point_of_control(_flow_meter_address=1)
            self.config.BaseStation3200[1].get_point_of_control(1).add_master_valve_to_point_of_control(_master_valve_address=1)

            # POC 2
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=2)
            self.config.BaseStation3200[1].get_point_of_control(2).set_enabled()
            self.config.BaseStation3200[1].get_point_of_control(2).set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].get_point_of_control(2).set_high_flow_limit(_limit=65)
            self.config.BaseStation3200[1].get_water_source(2).add_point_of_control_to_water_source(2)
            self.config.BaseStation3200[1].get_point_of_control(2).add_flow_meter_to_point_of_control(_flow_meter_address=2)

            # Need to assign PC 2-8 to ML 1 to tickle bug
            self.config.BaseStation3200[1].send_command_with_reply("set,pc=2,ml=1")
            self.config.BaseStation3200[1].send_command_with_reply("set,pc=3,ml=1")
            self.config.BaseStation3200[1].send_command_with_reply("set,pc=4,ml=1")
            self.config.BaseStation3200[1].send_command_with_reply("set,pc=5,ml=1")
            self.config.BaseStation3200[1].send_command_with_reply("set,pc=6,ml=1")
            self.config.BaseStation3200[1].send_command_with_reply("set,pc=7,ml=1")
            self.config.BaseStation3200[1].send_command_with_reply("set,pc=8,ml=1")
        except Exception as e:

            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control

        Set the mainline design flows so it will not limit the flow.
        We assume that if the mainline is managed by the FlowStation it will automatically limit concurrency by flow.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Mainline 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].get_mainline(1).set_enabled()
            self.config.BaseStation3200[1].get_mainline(1).set_target_flow(_gpm=280)
            self.config.BaseStation3200[1].get_mainline(1).set_pipe_fill_units_to_time()
            self.config.BaseStation3200[1].get_mainline(1).set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].get_point_of_control(1).add_mainline_to_point_of_control(_mainline_address=1)
            self.config.BaseStation3200[1].get_point_of_control(2).add_mainline_to_point_of_control(_mainline_address=1)
            self.config.BaseStation3200[1].get_mainline(1).set_high_flow_variance_tier_one(_percent=20.0,_with_shutdown_enabled=False)
            self.config.BaseStation3200[1].get_mainline(1).set_high_flow_variance_tier_two(_percent=20.0,_with_shutdown_enabled=False)
            self.config.BaseStation3200[1].get_mainline(1).set_high_flow_variance_tier_three(_percent=20.0,_with_shutdown_enabled=False)
            self.config.BaseStation3200[1].get_mainline(1).set_high_flow_variance_tier_four(_percent=20.0,_with_shutdown_enabled=False)

            self.config.BaseStation3200[1].get_mainline(1).set_low_flow_variance_tier_one(_percent=30.0,_with_shutdown_enabled=False)
            self.config.BaseStation3200[1].get_mainline(1).set_low_flow_variance_tier_two(_percent=30.0,_with_shutdown_enabled=False)
            self.config.BaseStation3200[1].get_mainline(1).set_low_flow_variance_tier_three(_percent=30.0,_with_shutdown_enabled=False)
            self.config.BaseStation3200[1].get_mainline(1).set_low_flow_variance_tier_four(_percent=30.0,_with_shutdown_enabled=False)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone

        Setting controller 1 zone 1 to be higher than the design flow of the mainline so we can verify it will run alone.

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=12)
            self.config.BaseStation3200[1].zones[12].set_design_flow(_gallons_per_minute=7.5)

            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=27)
            self.config.BaseStation3200[1].zones[27].set_design_flow(_gallons_per_minute=0.0)

            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=36)
            self.config.BaseStation3200[1].zones[36].set_design_flow(_gallons_per_minute=0.0)

            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=52)
            self.config.BaseStation3200[1].zones[52].set_design_flow(_gallons_per_minute=23.1)

            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=55)
            self.config.BaseStation3200[1].zones[55].set_design_flow(_gallons_per_minute=0.0)

            # self.config.BaseStation3200[1].get_mainline(ml_number=1).remove_zone_from_mainline(_zone_address=70)
            self.config.BaseStation3200[1].get_mainline(ml_number=1).remove_zone_from_mainline(_zone_address=71)
            self.config.BaseStation3200[1].get_mainline(ml_number=1).remove_zone_from_mainline(_zone_address=76)

            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=79)
            self.config.BaseStation3200[1].zones[79].set_design_flow(_gallons_per_minute=0.0)

            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=80)
            self.config.BaseStation3200[1].zones[80].set_design_flow(_gallons_per_minute=0.0)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        Set the time to be 3:59 PM, which is 1 minute before PG 12's start times \n

        Set FM1 and FM2 rates to 0.0 GPM to copy the swoosh config. \n

        Increment clock to 4:00 PM to trigger PG12 start time. "Watering" status' will show the next minute.

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_date_and_time(_date='05/08/2018', _time='15:59:00')
            self.config.BaseStation3200[1].verify_date_and_time()

            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=0.0)
            self.config.BaseStation3200[1].flow_meters[2].bicoder.set_flow_rate(_gallons_per_minute=0.0)

            # Increment time to 4:00 pm to trigger pg start
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in self.config.BaseStation3200[1].zones.values():
                zone.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        Incrementing clock to 4:01PM to trigger PG 12 to start and to see the water statuses.
        Expect zones 12,27,36 to start watering. The rest should be waiting.
        Expected Output: 05/08/2018 04:01:00 PM,"WT=12,27,36,",SO=,"WA=52,55,71,76,79,80,",DN=

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # 4:01PM
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for watering_zones in [12, 27, 36]:
                self.config.BaseStation3200[1].zones[watering_zones].statuses.verify_status_is_watering()

            for waiting_zones in [52, 55, 71, 76, 79, 80]:
                self.config.BaseStation3200[1].zones[waiting_zones].statuses.verify_status_is_waiting_to_water()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        Incrementing clock to 4:16PM.
        Checking the next group of zones are watering.

        Due to a v16 bug,
            Expected output for v16.1.606 is:
                05/08/2018 04:16:00 PM,"WT=52,71,76,","SO=12,27,36,","WA=55,79,80,",DN=

            Expected output for v16.1.608 (and beyond):
                05/08/2018 04:16:00 PM,"WT=52,71,76,","SO=12,27,36,","WA=55,79,80,",DN=

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # 4:16PM
            self.config.BaseStation3200[1].do_increment_clock(minutes=15)

            for watering_zones in [52, 71, 76]:
                self.config.BaseStation3200[1].zones[watering_zones].statuses.verify_status_is_watering()

            for soaking_zones in [12, 27, 36]:
                self.config.BaseStation3200[1].zones[soaking_zones].statuses.verify_status_is_soaking()

            for waiting_zones in [55, 79, 80]:
                self.config.BaseStation3200[1].zones[waiting_zones].statuses.verify_status_is_waiting_to_water()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        Incrementing clock to 4:31PM.
        Checking the next group of zones are watering.

        Due to a v16 bug,
            Expected output for v16.1.606 is:
                05/08/2018 04:31:00 PM,"WT=12,71,76,","SO=27,36,52,","WA=55,79,80,",DN=

            Expected output for v16.1.608 (and beyond):
                05/08/2018 04:31:00 PM,"WT=12,27,36,","SO=52,71,76,","WA=55,79,80,",DN=


        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # 4:31PM
            self.config.BaseStation3200[1].do_increment_clock(minutes=15)

            # for watering_zones in [12, 71,76]:
            #     self.config.BaseStation3200[1].zones[watering_zones].statuses.verify_status_is_watering()

            for watering_zones in [12, 27, 36]:
                self.config.BaseStation3200[1].zones[watering_zones].statuses.verify_status_is_watering()

            # for soaking_zones in [27, 36, 52]:
            #     self.config.BaseStation3200[1].zones[soaking_zones].statuses.verify_status_is_soaking()

            for soaking_zones in [52, 71, 76]:
                self.config.BaseStation3200[1].zones[soaking_zones].statuses.verify_status_is_soaking()

            # for waiting_zones in [55, 79, 80]:
            #     self.config.BaseStation3200[1].zones[waiting_zones].statuses.verify_status_is_waiting_to_water()

            for waiting_zones in [55, 79, 80]:
                self.config.BaseStation3200[1].zones[waiting_zones].statuses.verify_status_is_waiting_to_water()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_14(self):
        """
        Incrementing clock to 4:39PM.
        Checking the next group of zones are watering.

        Due to a v16 bug,
            Expected output for v16.1.606 is:
                05/08/2018 04:39:00 PM,"WT=27,71,76,","SO=36,52,","WA=55,79,80,","DN=12,"

            Expected output for v16.1.608 (and beyond):
                05/08/2018 04:39:00 PM,"WT=55,79,80,","SO=52,71,76,",WA=,"DN=12,27,36,"


        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # 4:39PM
            self.config.BaseStation3200[1].do_increment_clock(minutes=8)

            # for watering_zones in [27, 71, 76]:
            #     self.config.BaseStation3200[1].zones[watering_zones].statuses.verify_status_is_watering()

            for watering_zones in [55, 79, 80]:
                self.config.BaseStation3200[1].zones[watering_zones].statuses.verify_status_is_watering()

            # for soaking_zones in [36, 52]:
            #     self.config.BaseStation3200[1].zones[soaking_zones].statuses.verify_status_is_soaking()

            for soaking_zones in [52, 71, 76]:
                self.config.BaseStation3200[1].zones[soaking_zones].statuses.verify_status_is_soaking()

            # for waiting_zones in [55, 79, 80]:
            #     self.config.BaseStation3200[1].zones[waiting_zones].statuses.verify_status_is_waiting_to_water()

            for waiting_zones in [12, 27, 36]:
                self.config.BaseStation3200[1].zones[waiting_zones].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_15(self):
        """
        Incrementing clock to 4:47PM.
        Checking the next group of zones are watering.

        Due to a v16 bug,
            Expected output for v16.1.606 is:
                05/08/2018 04:47:00 PM,"WT=36,71,76,","SO=52,","WA=55,79,80,","DN=12,27,"

            Expected output for v16.1.608 (and beyond):
                05/08/2018 04:47:00 PM,"WT=55,79,80,","SO=52,71,76,",WA=,"DN=12,27,36,"

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # 4:47PM
            self.config.BaseStation3200[1].do_increment_clock(minutes=8)

            # for watering_zones in [36, 71, 76]:
            #     self.config.BaseStation3200[1].zones[watering_zones].statuses.verify_status_is_watering()

            for watering_zones in [55, 79, 80]:
                self.config.BaseStation3200[1].zones[watering_zones].statuses.verify_status_is_watering()

            # for soaking_zones in [52]:
            #     self.config.BaseStation3200[1].zones[soaking_zones].statuses.verify_status_is_soaking()

            for soaking_zones in [52, 71, 76]:
                self.config.BaseStation3200[1].zones[soaking_zones].statuses.verify_status_is_soaking()

            # for waiting_zones in [55, 79, 80]:
            #     self.config.BaseStation3200[1].zones[waiting_zones].statuses.verify_status_is_waiting_to_water()

            for waiting_zones in [12, 27, 36]:
                self.config.BaseStation3200[1].zones[waiting_zones].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_16(self):
        """
        Incrementing clock to 4:55PM.
        Checking the next group of zones are watering.

        Due to a v16 bug,
            Expected output for v16.1.606 is:
                05/08/2018 04:55:00 PM,"WT=52,71,76,",SO=,"WA=55,79,80,","DN=12,27,36,"

            Expected output for v16.1.608 (and beyond):
                05/08/2018 04:55:00 PM,"WT=52,71,76,","SO=55,79,80,",WA=,"DN=12,27,36,"

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # 4:55PM
            self.config.BaseStation3200[1].do_increment_clock(minutes=8)

            # for watering_zones in [52, 71, 76]:
            #     self.config.BaseStation3200[1].zones[watering_zones].statuses.verify_status_is_watering()

            for watering_zones in [52, 71, 76]:
                self.config.BaseStation3200[1].zones[watering_zones].statuses.verify_status_is_watering()

            # for soaking_zones in []:
            #     self.config.BaseStation3200[1].zones[soaking_zones].statuses.verify_status_is_soaking()

            for soaking_zones in [55, 79, 80]:
                self.config.BaseStation3200[1].zones[soaking_zones].statuses.verify_status_is_soaking()

            # for waiting_zones in [55, 79, 80]:
            #     self.config.BaseStation3200[1].zones[waiting_zones].statuses.verify_status_is_waiting_to_water()

            for waiting_zones in []:
                self.config.BaseStation3200[1].zones[waiting_zones].statuses.verify_status_is_waiting_to_water()

            # for done_zones in [12, 27, 36]:
            #     self.config.BaseStation3200[1].zones[done_zones].statuses.verify_status_is_done()

            for done_zones in [12, 27, 36]:
                self.config.BaseStation3200[1].zones[done_zones].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_17(self):
        """
        Incrementing clock to 5:03PM.
        Checking the next group of zones are watering.

        Due to a v16 bug,
            Expected output for v16.1.606 is:
                05/08/2018 05:03:00 PM,"WT=55,71,76,",SO=,"WA=79,80,","DN=12,27,36,52,"

            Expected output for v16.1.608 (and beyond):
                05/08/2018 05:03:00 PM,WT=,"SO=55,79,80,",WA=,"DN=12,27,36,52,71,76,"

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # 5:03PM
            self.config.BaseStation3200[1].do_increment_clock(minutes=8)

            # for watering_zones in [55, 71, 76]:
            #     self.config.BaseStation3200[1].zones[watering_zones].statuses.verify_status_is_watering()

            for watering_zones in []:
                self.config.BaseStation3200[1].zones[watering_zones].statuses.verify_status_is_watering()

            # for soaking_zones in []:
            #     self.config.BaseStation3200[1].zones[soaking_zones].statuses.verify_status_is_soaking()

            for soaking_zones in [55, 79, 80]:
                self.config.BaseStation3200[1].zones[soaking_zones].statuses.verify_status_is_soaking()

            # for waiting_zones in [79, 80]:
            #     self.config.BaseStation3200[1].zones[waiting_zones].statuses.verify_status_is_waiting_to_water()

            for waiting_zones in []:
                self.config.BaseStation3200[1].zones[waiting_zones].statuses.verify_status_is_waiting_to_water()

            # for done_zones in [12, 27, 36, 52]:
            #     self.config.BaseStation3200[1].zones[done_zones].statuses.verify_status_is_done()

            for done_zones in [12, 27, 36, 52, 71, 76]:
                self.config.BaseStation3200[1].zones[done_zones].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_18(self):
        """
        Incrementing clock to 5:18PM.
        Checking the next group of zones are watering.

        Due to a v16 bug,
            Expected output for v16.1.606 is:
                05/08/2018 05:18:00 PM,"WT=71,76,","SO=55,","WA=79,80,","DN=12,27,36,52,"

            Expected output for v16.1.608 (and beyond):
                05/08/2018 05:18:00 PM,WT=,SO=,WA=,"DN=12,27,36,52,55,71,76,79,80,"

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # 5:18PM
            self.config.BaseStation3200[1].do_increment_clock(minutes=15)

            # for watering_zones in [71, 76]:
            #     self.config.BaseStation3200[1].zones[watering_zones].statuses.verify_status_is_watering()

            for watering_zones in []:
                self.config.BaseStation3200[1].zones[watering_zones].statuses.verify_status_is_watering()
            #
            # for soaking_zones in [55]:
            #     self.config.BaseStation3200[1].zones[soaking_zones].statuses.verify_status_is_soaking()

            for soaking_zones in []:
                self.config.BaseStation3200[1].zones[soaking_zones].statuses.verify_status_is_soaking()

            # for waiting_zones in [79, 80]:
            #     self.config.BaseStation3200[1].zones[waiting_zones].statuses.verify_status_is_waiting_to_water()

            for waiting_zones in []:
                self.config.BaseStation3200[1].zones[waiting_zones].statuses.verify_status_is_waiting_to_water()

            # for done_zones in [12, 27, 36, 52]:
            #     self.config.BaseStation3200[1].zones[done_zones].statuses.verify_status_is_done()

            for done_zones in [12, 27, 36, 52, 71, 76, 79, 80]:
                self.config.BaseStation3200[1].zones[done_zones].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]