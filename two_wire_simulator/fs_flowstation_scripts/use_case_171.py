import sys
from time import sleep

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
from datetime import timedelta
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from csv_handler import CSVWriter

from common import helper_methods

# Browser pages used
# import page_factory

__author__ = 'Dillon'


class ControllerUseCase171(object):
    """
    Test name:
        - CN UseCase171 High/Unscheduled Flow with POC alone

    Purpose:
        - This test will determine if a POC with no mainline will effect a program that experiences a high flow
          shutdown or unscheduled flow. We will be inducing the high flow on the POC that does not have anything
          attached to it. I'm expecting the flow fault to not effect devices upstream.

    User Story:
        1) As a user if I have a lone POC experiencing a high flow or unscheduled flow, I do not want it to effect
           any devices upstream.

    Coverage Area:
        1. Configure program 1

        2. Setup zones
            - Timed zones, one zones on controller 1. Run time of 5 minutes

        3. Setup Water Sources
            - Add one water source to controller 1, C1WS1

        4. Setup Points of Control
            - Add a POC on controller 1 with a Master Valve and Flow Meter. C1P1
            - Add a POC on controller 1 with a Master Valve and Flow Meter. C2P2

        5. Setup Mainlines
            - Add a Mainline on controller 1. Needs GPM of 100. C1M1 --> C1P1

        6. Share Controllers and Flow Objects with the FlowStation
            - Share controller 1 with the FlowStation
                - Share a Water Source on controller 1 with the FlowStation
                - Share a Point of Control on controller 1 with the FlowStation
                - Share a Mainline on controller 1 with the FlowStation
            - Share controller 2 with the FlowStation
                - Share a Water Source on controller 2 with the FlowStation
                - Share a Point of Control on controller 2 with the FlowStation
                - Share a Mainline on controller 2 with the FlowStation

        7. Setup zone one mainlines
            - add zones to a mainline on controller 1. C1M1 --> Zn1

        8. Set design flow on all added zones on controller 1
            - zones on controller 1 have a design flow of 25 gpm

        9. System Layout:
               C1WS1
                 |
               C1P1
                 |
               C1M1---Zn1
                 |
               C2P2

    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase170' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/fs_json_config_files'
                                    )
        # initializes the csv writer object so it can be used in the remainder of the use_case
        self.csv_writer = CSVWriter(file_name=test_name,
                                    relative_path='fs_flowstation_scripts',
                                    delimiter=',',
                                    line_terminator='\n')

        try:
            self.csv_writer.open()
        except IOError:
            e_msg = self.csv_writer.fname + ' file is in use'
            raise IOError(e_msg)

        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=False)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        This step is setting up the basic program settings needed for this use case. Since there will be only one zone
        we don't need to set max concurrent zones.
        """
        program_water_windows = ['111111111111111111111111']
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=3)

            # Add and configure Program 1 to controller 1
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=3)
            self.config.BaseStation3200[1].programs[1].set_priority_level(_pr_level=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        There will only be one zone, so it's been set to a timed zone with 5 minute run time.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # add zones to programs on controller 1
            # Add & Configure Program Zone 1
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=5)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        setup WaterSources
        ############################
        This step we add our one water source to our controller.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Water Source 1 controller 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].set_priority(_priority_for_water_source=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        setup Point of Controls
        ############################
        This step adds our two POCs to our controller. The first POC will connect to the water source and then to a
        mainline. From that mainline we will have our second POC. The first POC will have the same gpm as our mainline
        will. The second POC needs a target flow of 50gpm.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Adding POC 1 to controller 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_flow_meter_to_point_of_control(
                _flow_meter_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=100)

            # This will create the connection from our water source 1 to POC 1 on controller 1
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=1)

            # Adding POC 2 to controller 1
            self.config.BaseStation3200[2].add_point_of_control_to_controller(_point_of_control_address=2)
            self.config.BaseStation3200[2].points_of_control[2].set_enabled()
            self.config.BaseStation3200[2].points_of_control[2].add_master_valve_to_point_of_control(
                _master_valve_address=1)
            self.config.BaseStation3200[2].points_of_control[2].add_flow_meter_to_point_of_control(
                _flow_meter_address=1)
            self.config.BaseStation3200[2].points_of_control[2].set_target_flow(_gpm=50)
            self.config.BaseStation3200[2].points_of_control[2].set_unscheduled_flow_limit(
                _gallons=10, with_shutdown_enabled=True)
            self.config.BaseStation3200[2].points_of_control[2].set_high_flow_limit(_limit=80.0,
                                                                                    with_shutdown_enabled=True)

            self.config.BaseStation3200[2].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=50)
            self.config.BaseStation3200[2].flow_meters[1].bicoder.self_test_and_update_object_attributes()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ##################
        setup mainlines
        ##################
        This step sets up our one mainline. The mainline will have the same target flow as the POC above it.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure ML 1 on controller 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=100)

            # This makes the connection from mainline 1 to POC 1 on controller
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ######################
        setup zones on mainlines
        ######################
        In this step we only need one zone off our mainline.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Add ZN 1 to ML 1 on controller 1
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ############################
        Give each zone a design flow
        ############################
        This step is setting our design flow for our only zone.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # set design flow of zones  on controller 1
            self.config.BaseStation3200[1].zones[1].set_design_flow(_gallons_per_minute=25)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        verify the entire configuration
        ###############################
        This step verifies our full configuration on both controllers. Refers to all the objects we created and
        verifies the controller can see all of them.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.BaseStation3200[2].verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        ###############################
        Share controllers, water sources, point of controls, and mainlines with Flow Station
        ###############################
        This step is setting up our FlowStation objects, so the FlowStation can properly help the system learn flow.
        Every object that was created above needs to be shared with the FlowStation.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add controller 1 & 2 to FlowStation
            self.config.FlowStations[1].add_controller_to_flow_station(
                _controller_address=1,
                _flow_station_slot_number=1)
            self.config.FlowStations[1].add_controller_to_flow_station(
                _controller_address=2,
                _flow_station_slot_number=2)

            # Add our three water sources to the FlowStation
            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=1,
                _controller_water_source_address=1,
                _flow_station_water_source_slot_number=1)

            # Tell the 3200 that the FlowStation is in control of Water Sources.
            self.config.BaseStation3200[1].water_sources[1].set_manage_by_flowstation()

            # Add controller 1 & 2 POCs to FlowStation
            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=1,
                _controller_point_of_control_address=1,
                _flow_station_point_of_control_slot_number=1)

            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=2,
                _controller_point_of_control_address=2,
                _flow_station_point_of_control_slot_number=2)

            self.config.BaseStation3200[1].points_of_control[1].set_manage_by_flowstation()
            self.config.BaseStation3200[2].points_of_control[2].set_manage_by_flowstation()

            # Adding ML's to FS
            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=1,
                _controller_mainline_address=1,
                _flow_station_mainline_slot_number=1)

            self.config.BaseStation3200[1].mainlines[1].set_manage_by_flowstation()

            # Connect water paths in the FlowStation to form our full system configuration
            self.config.FlowStations[1].get_water_source(1).add_point_of_control_to_water_source(
                _point_of_control_address=1)

            # Needed to use the get method here so I could reach the correct POC on controller 2
            self.config.FlowStations[1].get_point_of_control(1).add_mainline_to_point_of_control(_mainline_address=1)

            # Connecting POC with no other down stream connections to mainline 1
            self.config.FlowStations[1].get_mainline(ml_number=1).add_poc_to_mainline(_poc_address=2)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        ###############################
        verify the FlowStation configuration
        ###############################
        This step will verify the system configuration we have set up on the FlowStation, and save all the programming.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            self.config.FlowStations[1].save_programming_to_flow_station()
            self.config.FlowStations[1].verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]


    ###############################
    def step_11(self):
        """
        ###############################
        Start our scenario
        ###############################
        This step is setting our date to match the computer. Then we set the time and start our program.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            date_mngr.set_current_date_to_match_computer()
            self.config.BaseStation3200[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                             _time='10:00:00')
            self.config.BaseStation3200[2].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                             _time='10:00:00')
            self.config.FlowStations[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                          _time='10:00:00')

            self.config.BaseStation3200[1].programs[1].set_program_to_start()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_12(self):
        """
        ###############################
        Verify device statuses
        ###############################
        This step is verifying our statuses are at their defaults before we increment the clock at all.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Time is 10:00AM
            # Verify the statuses
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_ok()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_off()

            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_off()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_off()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_off()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_waiting_to_water()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_13(self):
        """
        ###############################
        Verify device statuses
        ###############################
        This step is verifying that the zone is waiting to water because it is waiting for flow allocation. The CP that
        outputs to nothing should be off because water should not be flowing through it.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Time is 10:01AM
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify the statuses
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_off()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_off()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_waiting_to_water()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_14(self):
        """
        ###############################
        Verify device statuses
        ###############################
        This step is verifying that our zone is now watering because it has water allocated to it from the FS
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Time is 10:02AM
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify the statuses
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_off()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_off()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_15(self):
        """
        #####################
        Set flow meter values
        #####################
        This step is setting the flow meter reading to induce a high flow fault on our lone POC.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[2].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=100)
            self.config.BaseStation3200[2].flow_meters[1].bicoder.self_test_and_update_object_attributes()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_16(self):
        """
        ###############################
        Verify device statuses
        ###############################
        This step is verifying that a flow fault shows up because we set the flow rate on C2P2 to be above the high flow
        limit, and that the zone running is unaffected because that CP isn't upstream of the zone.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Time is 10:04AM
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=2)

            # Verify the statuses
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_flow_fault()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_flow_fault()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()

            self.config.BaseStation3200[2].get_point_of_control(pc_number=2).messages.check_for_high_flow_shutdown_message_not_present()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_17(self):
        """
        ###############################
        Verify device statuses
        ###############################
        This step is incrementing another minutes and verifying/clearing our flow fault, and checking that our zone is
        still watering. Need to do an increment clock after clearing the fault, so the controller sees the fault is
        cleared.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # Time is 10:06AM
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=2)

            self.config.BaseStation3200[2].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=50)
            self.config.BaseStation3200[2].flow_meters[1].bicoder.self_test_and_update_object_attributes()

            # Verify the statuses
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_flow_fault()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_flow_fault()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_18(self):
        """
        ###############################
        Verify device statuses
        ###############################
        This step is verifying that each device gets to a "finished" state. This shows that our high flow shutdown
        didn't effect our upstream devices.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Time is 10:10AM
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=4)

            # Verify the statuses
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_ok()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_off()

            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_off()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_flow_fault()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_flow_fault()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_19(self):
        """
        ###############################
        verify the entire configuration
        ###############################
        This step is used to make sure nothing in our system has gone offline, or been added since the last config check.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.FlowStations[1].save_programming_to_flow_station()
            helper_methods.increment_controller_flowstation_clocks(self.config.FlowStations[1], self.config.BaseStation3200, minutes=1)
            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.BaseStation3200[2].verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
