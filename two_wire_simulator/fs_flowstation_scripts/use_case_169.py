import sys

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

# import log_handler for logging functionality
from common.logging_handler import log_handler

from csv_handler import CSVWriter

from common import helper_methods

# Browser pages used
# import page_factory

__author__ = 'Dillon'


class ControllerUseCase169(object):
    """
    Test name:
        - CN UseCase169 Flow Variance Status and Clear

    Purpose:
        - Verifies our shared control point behavior, with one mainline experiencing a shutdown on variance, and
          the another mainline continuing to run.

    User Story:
        1) As a user I need to have one mainline shutdown on variance, and another mainline continue to run that
           shares a common control point.

        2) When the variance condition is cleared I expect that the mainline will water when started over.

    Coverage Area:
        1) Configure Programs
            - Program 1, zone 1 with 30 minute run time, and 5 concurrent zones
            - Program 2, zone 2 with 30 minute run time, and 5 concurrent zones

        2) Setup Zones
            - Zone 1, design flow = 50 GPM
            - Zone 2, design flow = 50 GPM

        3) Setup Water Sources
            - One water source, managed by FlowStation

        4) Setup Points of Control
            - POC 1, managed by FlowStation with design flow = 200 GPM
                - Need flow meter on POC

        5) Setup Mainlines
            - Mainline 1
                - 1 minute pipe fill
                - 50 gpm design flow
                - advanced variance off
                - Flow variance = 50%
                - Flow variance shutdown = true
            - Mainline 2
                - 1 minute pipe fill
                - 50 gpm design flow
                - advanced variance off
                - Flow variance = 0%
                - Flow variance shutdown = false

        6) Share objects with FlowStation
            - Share all object made with FlowStation

        7) Add Zones to mainlines

    Scenario:
        1) Start program 1 and 2
        2) Set flow meter = 175 gpm
        3) Advance clock 2 minutes
        4) Verify zones watering, mainlines running, control points running, programs running
        5) Advacne clock 10 minutes
        6) Verify zone 1 = done, mainline 1 flow variance shutdown message, zone 2 = watering, mainline 2 = running,
            control point running, mainline 1 = flow fault, program 1 = done, program 2 = running
        7) Clear mainline 1 message
        8) Advance 1 minute
        9) Mainline 1 status = off, zone 1 = off
        10) Start program 1
        11) Advance clock 2 minutes
        12) Verify zones watering, mainlines running, control points running, programs = running

    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase169' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/fs_json_config_files'
                                    )
        # initializes the csv writer object so it can be used in the remainder of the use_case
        self.csv_writer = CSVWriter(file_name=test_name,
                                    relative_path='fs_flowstation_scripts',
                                    delimiter=',',
                                    line_terminator='\n')

        try:
            self.csv_writer.open()
        except IOError:
            e_msg = self.csv_writer.fname + ' file is in use'
            raise IOError(e_msg)

        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=False)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        This step is setting up program 1 and 2 for this use case.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set max zones on the controller to 6 so that it runs all zones
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=5)

            # Add and configure Program 1 to controller 1
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=[600])

            # Add and configure Program 2 to controller 1
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation3200[1].programs[2].set_enabled()
            self.config.BaseStation3200[1].programs[2].set_start_times(_st_list=[600])

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        This step is setting up our two zones. Zone 1 is on program 1 and Zone 2 is on program 2.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure Program Zone 1
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=30)

            # Add & Configure Program Zone 2
            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[2].zone_programs[2].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[2].zone_programs[2].set_run_time(_minutes=30)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        setup WaterSources
        ############################
        This step is setting up our one water source that will be used in our test.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Water Source 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].set_priority(_priority_for_water_source=2)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        setup Point of Controls
        ############################
        This step is setting up our shared control point for our test.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Adding POC 1 to controller 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].add_flow_meter_to_point_of_control(
                _flow_meter_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=200)

            # Add POC to our water source
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ##################
        setup mainlines
        ##################
        This step is setting up our mainlines for our system. There will be two mainlines that are both on controller 1.
        Our two mainlines will be sharing a control point.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure ML 1 on controller 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].mainlines[1].set_standard_variance_limit_with_shutdown(_percentage=50)
            self.config.BaseStation3200[1].mainlines[1].set_use_advanced_flow_to_false()
            # self.config.BaseStation3200[1].mainlines[1].set_high_flow_variance_tier_one(_percent=50,
            #                                                                             _with_shutdown_enabled=True)

            # Add & Configure ML 2 on controller 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=2)
            self.config.BaseStation3200[1].mainlines[2].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[2].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].mainlines[2].set_use_advanced_flow_to_false()
            self.config.BaseStation3200[1].mainlines[2].set_standard_variance_limit_without_shutdown(_percentage=50)
            # self.config.BaseStation3200[1].mainlines[1].set_high_flow_variance_tier_one(_percent=0,
            #                                                                             _with_shutdown_enabled=False)

            # Add & Configure ML 3 on controller 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=3)
            self.config.BaseStation3200[1].mainlines[3].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[3].set_target_flow(_gpm=200)
            self.config.BaseStation3200[1].mainlines[3].set_use_advanced_flow_to_false()
            # self.config.BaseStation3200[1].mainlines[3].set_high_flow_variance_tier_one(_percent=0,
            #                                                                             _with_shutdown_enabled=False)

            # This makes the connection from mainline 3 to POC 1
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=3)

            # # This creates our ML to ML connections
            # self.config.BaseStation3200[1].mainlines[1].add_mainline_to_mainline(_mainline_address=3)
            # self.config.BaseStation3200[1].mainlines[2].add_mainline_to_mainline(_mainline_address=3)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ######################
        setup zones on mainlines
        ######################
        This step is adding one zone to each mainline to cover a simple test setup.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Add ZN 1 to ML 1 on controller 1
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=1)

            # Add ZN 2 to ML 2 on controller 1
            self.config.BaseStation3200[1].mainlines[2].add_zone_to_mainline(_zone_address=2)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ############################
        Give each zone a design flow
        ############################
        This step is setting our design flow for each zone.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # set design flow of zones  on controller 1
            self.config.BaseStation3200[1].zones[1].set_design_flow(_gallons_per_minute=50)
            self.config.BaseStation3200[1].zones[2].set_design_flow(_gallons_per_minute=50)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        verify the entire configuration
        ###############################
        This step verifies our full configuration on both controllers. Refers to all the objects we created and
        verifies the controller can see all of them.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            self.config.BaseStation3200[1].verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        ###############################
        Share controllers, water sources, point of controls, and mainlines with Flow Station
        ###############################
        This step is setting up our FlowStation objects, so the FlowStation can properly help manage the system.
        Every object that was created above needs to be shared with the FlowStation.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add controller 1 to FlowStation
            self.config.FlowStations[1].add_controller_to_flow_station(
                _controller_address=1,
                _flow_station_slot_number=1)

            # Add our one water sources to the FlowStation
            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=1,
                _controller_water_source_address=1,
                _flow_station_water_source_slot_number=1)

            # Tell the 3200 that the FlowStation is in control of Water Sources.
            self.config.BaseStation3200[1].water_sources[1].set_manage_by_flowstation()

            # Add controller 1 POCs to FlowStation
            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=1,
                _controller_point_of_control_address=1,
                _flow_station_point_of_control_slot_number=1)

            # POC is managed by FlowStation
            self.config.BaseStation3200[1].points_of_control[1].set_manage_by_flowstation()

            # Adding ML's to FS
            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=1,
                _controller_mainline_address=1,
                _flow_station_mainline_slot_number=1)
            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=1,
                _controller_mainline_address=2,
                _flow_station_mainline_slot_number=2)
            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=1,
                _controller_mainline_address=3,
                _flow_station_mainline_slot_number=3)

            self.config.BaseStation3200[1].mainlines[1].set_manage_by_flowstation()
            self.config.BaseStation3200[1].mainlines[2].set_manage_by_flowstation()
            self.config.BaseStation3200[1].mainlines[3].set_manage_by_flowstation()

            # Connect water paths in the FlowStation to form our full system configuration
            self.config.FlowStations[1].get_water_source(1).add_point_of_control_to_water_source(
                _point_of_control_address=1)

            # Makes our POC 1 to ML 3 connection
            self.config.FlowStations[1].get_point_of_control(pc_number=1).add_mainline_to_point_of_control(
                _mainline_address=3)

            # Making connection between shared POC and our two mainlines
            self.config.FlowStations[1].get_mainline(ml_number=3).add_mainline_to_mainline(_mainline_address=1)
            self.config.FlowStations[1].get_mainline(ml_number=3).add_mainline_to_mainline(_mainline_address=2)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        ###############################
        verify the FlowStation configuration
        ###############################
        This step will verify the system configuration we have set up on the FlowStation, and save all the programming.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            self.config.FlowStations[1].save_programming_to_flow_station()
            self.config.FlowStations[1].verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_11(self):
        """
        #####################
        Start of Scenario
        #####################
        This step is starting our two programs that are driving our test.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            date_mngr.set_current_date_to_match_computer()
            self.config.BaseStation3200[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                             _time='09:59:00')
            self.config.BaseStation3200[2].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                             _time='09:59:00')
            self.config.FlowStations[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                          _time='09:59:00')

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_12(self):
        """
        ###############################
        Set flow meter value
        ###############################
        This step is setting our flow meter value in our shared point of control to 180gpm.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=180)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_13(self):
        """
        ###############################
        Verify Statuses
        ###############################
        This step is advancing the clock 2 minutes and verifying everything in the system is running.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Advances clock to 10:02AM
            helper_methods.increment_controller_flowstation_clocks(self.config.FlowStations[1], self.config.BaseStation3200, minutes=2)

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_14(self):
        """
        ###############################
        Verify device statuses
        ###############################
        This step is advancing the clock by 10 minutes, so that our controller will register the flow fault on
        mainline 1. Also verifies and clears our high flow variance message.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Advances clock to 10:12AM
            helper_methods.increment_controller_flowstation_clocks(self.config.FlowStations[1], self.config.BaseStation3200, minutes=10)

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_error()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_flow_fault()
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].mainlines[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].mainlines[1].messages.get_high_flow_variance_shutdown_message()
            self.config.BaseStation3200[1].mainlines[1].messages.clear_high_flow_variance_shutdown_message()

            # clear the zone high flow variance shutdown message
            # TODO:  extend zone messages to include calls for variance shutdown (verify and clear)
            self.config.BaseStation3200[1].ser.send_and_wait_for_reply(tosend="DO,MG,ZN=1,SS=HS")

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_15(self):
        """
        ###############################
        Verify device statuses
        ###############################
        This step is verifying our mainline 1 status is off, and and that our zone 1 is done.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Advances clock to 10:13AM
            helper_methods.increment_controller_flowstation_clocks(self.config.FlowStations[1],
                                                                   self.config.BaseStation3200, minutes=1)

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_16(self):
        """
        ###############################
        Verify device statuses
        ###############################
        This step is restarting the program since the fault has been cleared and restarts the programs to verify we
        see our devices running.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # Restarting program
            self.config.BaseStation3200[1].programs[1].set_program_to_start()
            self.config.BaseStation3200[1].programs[2].set_program_to_start()

            # Advances time to 10:02AM
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=3)

            # Final check of statuses
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_18(self):
        """
        ###############################
        verify the entire configuration
        ###############################
        This step is used to make sure nothing in our system has gone offline, or been added since the last config check.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.FlowStations[1].save_programming_to_flow_station()
            helper_methods.increment_controller_flowstation_clocks(self.config.FlowStations[1], self.config.BaseStation3200, minutes=1)
            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.BaseStation3200[2].verify_full_configuration()
            self.config.FlowStations[1].verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
