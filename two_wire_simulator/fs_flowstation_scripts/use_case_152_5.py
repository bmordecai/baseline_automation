import sys
from datetime import timedelta, datetime

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

__author__ = 'Eldin'


class ControllerUseCase152_5(object):
    """
    Test name:
        - CN UseCase152_5 Learn Flow Multiple Upstream POC
    purpose:
        - This test is meant to test the learn flow of a zone and program when the zone/program's upstream mainline is
          fed by two control points from different controllers. \n
    User Story:
        1)  As a user, I don't want to have to calculate the amount of water that feeds into my zones.
    Coverage Area: \n
        1. Configure one program with two zones
        2. Setup zones
            - Add a timed zone on each program
        3. Setup Water Sources
            - Add a water source to controller 1
            - Add a water source to controller 2
        4. Setup Points of Control
            - Add a POC on controller 1 with a Master Valve and Flow Meter
            - Add a POC on controller 2 with a Master Valve and Flow Meter
        5. Setup Mainlines
            - Add a mainline to only controller 1
                - Set that mainlines design flow to be 100 GPM, so that way we can make both 50 GPM and 100 GPM work
        6. Share Controller and Flow Objects with the FlowStation
            - Share controller with the FlowStation
                - Share a Water Source on controller with the FlowStation
                - Share a Point of Control on controller with the FlowStation
                - Share a Mainlines on controller with the FlowStation
        7. Setup zones
            - add zone 1 and 2 to mainline 1 on controller 1
        8. Set design flow on all added zones on controller 1 and 2
            - zone 1 and 2 on controller have a design flow of 10 gpm for a default setting, it will eventually be
              overwritten when we do a learn flow.

    Not Covered:
        1. How long it takes to do a learn flow by the exact minute boundaries. This is covered in so many other tests
           where we go minute by minute. This test will instead be more dynamic and let the learn flow do its thing and
           will focus only on verifying the results afterwards.

    Use case explanation: \n

            FlowStation:
                - WS/PC/ML Assignments:

                C1:W1    C2:W8
                  |        |
                C1:P1    C2:P8
                  |        |
                C1:M1 ------
                  |

        Scenario 1:
            - Set the flow rate on C1:P1 to 10 GPM
            - Set the flow rate on C2:P8 to 40 GPM
            - Start Learn Flow Program 1 controller 1
                - Verify that Zone 1 is learning flow
                - Verify that Zone 2 is waiting to water
                - Verify that Programs 1 is learning flow
            - Zone 1 finishes learn flow
                - Verify that Zone 1 is done
                - Verify that Zone 2 is learning flow
                - Verify that Zone 1 generated a "learn flow success" message
                - Verify that Programs 1 is still learning flow
            - Zone 2 finishes learn flow
                - Verify that Zone 1 and 2 are done
                - Verify that Zone 2 generated a "learn flow success" message
                - Verify that Programs 1 is done
                - Verify both Zones design flows are equal to 50 GPM
                - Verify program 1 generated a "learn flow success" message
            - Clear the "learn flow success" messages

        Scenario 2 (Changing Flow Meter Values Mid Reading):
            - Set the flow rate on C1:P1 to 10 GPM
            - Set the flow rate on C2:P8 to 40 GPM
            - Reset the design flows for the new test
            - Start Learn Flow Zone 1 and 2 on controller
                - Verify that Zone 1 is learning flow
                - Verify that Zone 2 is waiting to water
            - Zone 1 finishes learn flow
                - Verify that Zone 1 is done
                - Set the flow rate on C1:P1 to 60 GPM so that the design flow is different between the zones
                - Verify that Zone 2 is learning flow
                - Verify that Zone 1 generated a "learn flow success" message
            - Zone 2 finishes learn flow
                - Verify that Zone 1 and 2 are done
                - Verify that Zone 2 generated a "learn flow success" message
            - Clear the "learn flow success" messages
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/fs_json_config_files'
                                    )
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:

                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]

                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))

                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add and configure Program 1 to controller 1
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # add zones to programs on controller 1
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=10)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_run_time(_minutes=10)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

            #################################

    def step_3(self):
        """
        ############################
        setup WaterSources
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Water Source 1 on controller 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].get_water_source(1).set_enabled()

            # Water Source 8 on controller 2
            self.config.BaseStation3200[2].add_water_source_to_controller(_water_source_address=8)
            self.config.BaseStation3200[2].get_water_source(8).set_enabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

            #################################

    def step_4(self):
        """
        ############################
        setup Point of Controls
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure POC 1 on controller 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].get_point_of_control(1).set_enabled()
            self.config.BaseStation3200[1].get_point_of_control(1).add_master_valve_to_point_of_control(
                _master_valve_address=1)
            self.config.BaseStation3200[1].get_point_of_control(1).add_flow_meter_to_point_of_control(
                _flow_meter_address=1)
            self.config.BaseStation3200[1].get_point_of_control(1).set_target_flow(_gpm=100)
            self.config.BaseStation3200[1].get_water_source(1).add_point_of_control_to_water_source(
                _point_of_control_address=1)

            # Add & Configure POC 8 on controller 2
            self.config.BaseStation3200[2].add_point_of_control_to_controller(_point_of_control_address=8)
            self.config.BaseStation3200[2].get_point_of_control(8).set_enabled()
            self.config.BaseStation3200[2].get_point_of_control(8).add_master_valve_to_point_of_control(
                _master_valve_address=8)
            self.config.BaseStation3200[2].get_point_of_control(8).add_flow_meter_to_point_of_control(
                _flow_meter_address=8)
            self.config.BaseStation3200[2].get_point_of_control(8).set_target_flow(_gpm=100)
            self.config.BaseStation3200[2].get_water_source(8).add_point_of_control_to_water_source(
                _point_of_control_address=8)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

            #################################

    def step_5(self):
        """
        ##################
        setup mainlines
        ##################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure ML 1 on controller 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].get_mainline(1).set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].get_mainline(1).set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].get_mainline(1).set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].get_point_of_control(1).add_mainline_to_point_of_control(_mainline_address=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ##################################
    def step_6(self):
        """
        ###############################
        verify the entire configuration on the 3200 \n
        ###############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.BaseStation3200[2].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_7(self):
        """
        ###############################
        Share controller, water source, point of control, mainline with Flow Station
        ###############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add controller 1 to FlowStation
            self.config.FlowStations[1].add_controller_to_flow_station(
                _controller_address=1,
                _flow_station_slot_number=1)
            # Add controller 2 to FlowStation
            self.config.FlowStations[1].add_controller_to_flow_station(
                _controller_address=2,
                _flow_station_slot_number=2)

            # ******************************************* Add water sources ****************************************** #
            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=1,
                _controller_water_source_address=1,
                _flow_station_water_source_slot_number=1)
            self.config.BaseStation3200[1].get_water_source(1).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=2,
                _controller_water_source_address=8,
                _flow_station_water_source_slot_number=8)
            self.config.BaseStation3200[2].get_water_source(8).set_manage_by_flowstation()

            # ****************************************** Add control points ****************************************** #
            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=1,
                _controller_point_of_control_address=1,
                _flow_station_point_of_control_slot_number=1)
            self.config.BaseStation3200[1].get_point_of_control(1).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=2,
                _controller_point_of_control_address=8,
                _flow_station_point_of_control_slot_number=8)
            self.config.BaseStation3200[2].get_point_of_control(8).set_manage_by_flowstation()

            # ******************************************** Add mainlines ********************************************* #
            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=1,
                _controller_mainline_address=1,
                _flow_station_mainline_slot_number=1)
            self.config.BaseStation3200[1].get_mainline(1).set_manage_by_flowstation()

            # *************************************** FlowStation Assignments **************************************** #
            self.config.FlowStations[1].get_water_source(1).add_point_of_control_to_water_source(_point_of_control_address=1)
            self.config.FlowStations[1].get_water_source(8).add_point_of_control_to_water_source(_point_of_control_address=8)
            self.config.FlowStations[1].get_point_of_control(1).add_mainline_to_point_of_control(_mainline_address=1)
            self.config.FlowStations[1].get_point_of_control(8).add_mainline_to_point_of_control(_mainline_address=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_8(self):
        """
        ###############################
        verify the entire configuration through the Flow Station Serial Port \n
        ###############################
            - Get information for each object from the flow station
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.FlowStations[1].save_programming_to_flow_station()
            self.config.FlowStations[1].verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

            #################################

    def step_9(self):
        """
        ######################
        setup zones on mainlines
        ######################
        - Zones 1 and 2 on CN1 will both be on the same mainline
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Add Zones 1/2 on Controller 1 to mainline 1
            self.config.BaseStation3200[1].get_mainline(1).add_zone_to_mainline(_zone_address=1)
            self.config.BaseStation3200[1].get_mainline(1).add_zone_to_mainline(_zone_address=2)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

            ###############################

    def step_10(self):
        """
        Give each zone a design flow, kind of like a default value that we know will be overwritten by the learn flow
            - zones 1 - 10 gpm
            - zones 2 - 10 gpm
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # set design flow of zones  on controller 1
            # Assign a design flow value to each zone so that they have a default setting
            self.config.BaseStation3200[1].zones[1].set_design_flow(_gallons_per_minute=10)
            self.config.BaseStation3200[1].zones[2].set_design_flow(_gallons_per_minute=10)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_11(self):
        """
        ###############################
        Scenario 1 = program learn flow
        ###############################
        - Set the flow rate on C1:P1 to 10 GPM
        - Set the flow rate on C2:P8 to 40 GPM
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=10.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[2].flow_meters[8].bicoder.set_flow_rate(_gallons_per_minute=40.0)
            self.config.BaseStation3200[2].flow_meters[8].bicoder.self_test_and_update_object_attributes()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

            ###############################

    def step_12(self):
        """
        ###############################
        Scenario 1 = program learn flow continued
        ###############################
        - Start Learn Flow Program 1 controller 1
            - Verify that Zone 1 is learning flow
            - Verify that Zone 2 is waiting to water
            - Verify that Programs 1 and 2 are learning flow
        - Zone 1 finishes learn flow
            - Verify that Zone 1 is done
            - Verify that Zone 2 is learning flow
            - Verify that zone 1 generated a "learn flow success" message
            - Verify that Programs 1 is still learning flow
        - Zone 2 finishes learn flow
            - Verify that Zone 1 and 2 are done
            - Verify that zone 2 generated a "learn flow success" message
            - Verify that Programs 1 is done
            - Verify both zones design flows are not equal to 50 GPM
            - Verify program 1 generated a "learn flow success" message
        - Clear the "learn flow success" messages
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            date_mngr.set_current_date_to_match_computer()
            self.config.FlowStations[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                          _time='05:59:00')
            self.config.BaseStation3200[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                             _time='05:59:00')
            self.config.BaseStation3200[2].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                             _time='05:59:00')
            # This is the max amount of minutes we want to allow both zones to learn flow
            max_tries = 0

            # Start learn flow program 1
            self.config.BaseStation3200[1].programs[1].set_learn_flow_to_start()

            # Increment by two minutes so that our water gets allocated from the FlowStation, and we start learning flow
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=2)

            # Loop until our first zone is done learning flow
            while self.config.BaseStation3200[1].zones[1].statuses.status_is_learning_flow(_get_status=True) and max_tries < 30:
                self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
                self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_learning_flow()
                helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                       controllers=self.config.BaseStation3200,
                                                                       minutes=1)
                max_tries += 1

            # Loop until our second zone is done learning flow
            while self.config.BaseStation3200[1].zones[2].statuses.status_is_learning_flow(_get_status=True) and max_tries < 30:
                self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
                self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_learning_flow()
                helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                       controllers=self.config.BaseStation3200,
                                                                       minutes=1)
                max_tries += 1

            # We are now done learning flow, verify that everything is done and all messages appear
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()

            # Update our objects values to have the correct upstream flow since they have successfully learned flow.
            # Since there are two upstream POCs we need to sum both of their flow rates to get the downstream zone GPM
            total_upstream_flow = self.config.BaseStation3200[1].flow_meters[1].bicoder.vr + \
                                  self.config.BaseStation3200[2].flow_meters[8].bicoder.vr
            self.config.BaseStation3200[1].zones[1].df = total_upstream_flow
            self.config.BaseStation3200[1].zones[2].df = total_upstream_flow

            # Verify the design flow of the zones is up to date on the controller with the values we expect
            self.config.BaseStation3200[1].zones[1].get_data()
            self.config.BaseStation3200[1].zones[1].verify_design_flow()
            self.config.BaseStation3200[1].zones[2].get_data()
            self.config.BaseStation3200[1].zones[2].verify_design_flow()

            # Verify our messages are present and then clear them
            self.config.BaseStation3200[1].zones[1].messages.verify_learn_flow_complete_success_message()
            self.config.BaseStation3200[1].zones[1].messages.clear_learn_flow_complete_success_message()
            self.config.BaseStation3200[1].zones[2].messages.verify_learn_flow_complete_success_message()
            self.config.BaseStation3200[1].zones[2].messages.clear_learn_flow_complete_success_message()
            self.config.BaseStation3200[1].programs[1].messages.verify_learn_flow_complete_success_message()
            self.config.BaseStation3200[1].programs[1].messages.clear_learn_flow_complete_success_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

            ###############################

    def step_13(self):
        """
        ###############################
        Scenario 2 = zone learn flow
        ###############################
        - Set the flow rate on C1:P1 to 10 GPM
        - Set the flow rate on C2:P8 to 40 GPM
        - Reset the design flows for the new test
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=10.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[2].flow_meters[8].bicoder.set_flow_rate(_gallons_per_minute=40.0)
            self.config.BaseStation3200[2].flow_meters[8].bicoder.self_test_and_update_object_attributes()

            # Reset the design flows so we know that the learn flow happened because of this scenario and not previous
            self.config.BaseStation3200[1].zones[1].set_design_flow(_gallons_per_minute=10.0)
            self.config.BaseStation3200[1].zones[2].set_design_flow(_gallons_per_minute=10.0)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

            ###############################

    def step_14(self):
        """
        ###############################
        Scenario 2 = zone learn flow continued
        ###############################
        - Start Learn Flow Zone 1 and 2 on controller
            - Verify that Zone 1 is learning flow
            - Verify that Zone 2 is waiting to water
        - Zone 1 finishes learn flow
            - Verify that Zone 1 is done
            - Set the flow rate on C1:P1 to 60 GPM so that the design flow is different between the zones
            - Verify that Zone 2 is learning flow
            - Verify that zone 1 generated a "learn flow success" message
        - Zone 2 finishes learn flow
            - Verify that Zone 1 and 2 are done
            - Verify that zone 2 generated a "learn flow success" message
        - Clear the "learn flow success" messages
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # This is the max amount of minutes we want to allow both zones to learn flow
            max_tries = 0

            # Start learn flow zone 1 and 2
            self.config.BaseStation3200[1].zones[1].set_learn_flow_to_start()
            self.config.BaseStation3200[1].zones[2].set_learn_flow_to_start()

            # Increment by two minutes so that our water gets allocated from the FlowStation, and we start learning flow
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=2)

            # Loop until our first zone is done learning flow
            while self.config.BaseStation3200[1].zones[1].statuses.status_is_learning_flow(_get_status=True) and max_tries < 30:
                self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_learning_flow()
                self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
                self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
                helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                       controllers=self.config.BaseStation3200,
                                                                       minutes=1)
                max_tries += 1

            # Change the flow rate value on the first flow meter to 60, this will make the second zone have a different
            # design flow. Save the first zones design flow first so we can use them later in the verifiers before the
            # flow rates change
            total_upstream_flow_before_flow_change = self.config.BaseStation3200[1].flow_meters[1].bicoder.vr + \
                                                     self.config.BaseStation3200[2].flow_meters[8].bicoder.vr
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=60.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()

            # Loop until our second zone is done learning flow
            while self.config.BaseStation3200[1].zones[2].statuses.status_is_learning_flow(_get_status=True) and max_tries < 30:
                self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
                self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_learning_flow()
                self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
                helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                       controllers=self.config.BaseStation3200,
                                                                       minutes=1)
                max_tries += 1

            # We are now done learning flow, verify that everything is done and all messages appear
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()

            # Update our objects values to have the correct upstream flow since they have successfully learned flow
            # Since there are two upstream POCs we need to sum both of their flow rates to get the downstream zone GPM
            total_upstream_flow = self.config.BaseStation3200[1].flow_meters[1].bicoder.vr + \
                                  self.config.BaseStation3200[2].flow_meters[8].bicoder.vr
            self.config.BaseStation3200[1].zones[1].df = total_upstream_flow_before_flow_change
            self.config.BaseStation3200[1].zones[2].df = total_upstream_flow

            # Verify the design flow of the zones is up to date on the controller with the values we expect
            self.config.BaseStation3200[1].zones[1].get_data()
            self.config.BaseStation3200[1].zones[1].verify_design_flow()
            self.config.BaseStation3200[1].zones[2].get_data()
            self.config.BaseStation3200[1].zones[2].verify_design_flow()

            # Verify our messages are present and then clear them
            self.config.BaseStation3200[1].zones[1].messages.verify_learn_flow_complete_success_message()
            self.config.BaseStation3200[1].zones[1].messages.clear_learn_flow_complete_success_message()
            self.config.BaseStation3200[1].zones[2].messages.verify_learn_flow_complete_success_message()
            self.config.BaseStation3200[1].zones[2].messages.clear_learn_flow_complete_success_message()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
