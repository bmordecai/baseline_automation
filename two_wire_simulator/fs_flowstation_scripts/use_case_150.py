import sys
from time import sleep

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

# Browser pages used
# import page_factory

__author__ = 'Kent'


class ControllerUseCase150(object):
    """
    Test name:
        - CN UseCase150 Basic Programming
    purpose:
        - This test is meant to test setting up a 3200 with programs, zones, conditions, water sources, and mainlines.
        After that we share the 3200 with the Flow Station as well as a Water Source, Point of Control, and Mainline.
        Once We have those objects shared, we want to verify the objects over the serial port of the FlowStation.
    Coverage Area: \n
        1. Get a 3200 controller into a known state by following these steps:
            a. Set up programs
            b. Set up zones on programs
            c. Create start/stop/pause conditions
            d. Set up Water Sources
            e. Set up Point of Control
            f. Set up Mainline
            g. Add Zones to mainlines

        2. Share the following objects with the FlowStation:
            a. 3200 Controller
            b. Water Source
            c. Point of Control
            d. Mainline

        3. Verify the following objects over the serial port on the FlowStation:
            a. Water Source
            b. Point of Control
            c. Mainline

        4. Stop sharing the objects between the Flow Station and the Controller
            a. Stop sharing 3200 Controller
            b. Stop sharing Mainline
            c. Stop sharing Point of Control
            d. Stop sharing Water Source
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/fs_json_config_files'
                                    )
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)
            
    #################################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """

        program_start_time_8am_9am_10am_11am = [480, 540, 600, 660]
        program_water_windows = ['111111111111111111111111']
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation3200[1].programs[1].set_water_window(_ww=program_water_windows)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(_sun=False,
                                                                                                           _mon=True,
                                                                                                           _tues=False,
                                                                                                           _wed=True,
                                                                                                           _thurs=False,
                                                                                                           _fri=True,
                                                                                                           _sat=False)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_start_time_8am_9am_10am_11am)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=16)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=8)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].add_moisture_sensor_to_primary_zone(
                _moisture_sensor_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_water_strategy_to_lower_limit()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_lower_limit_threshold(_percent=24)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_one_time_calibration_cycle()

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        Create Start/stop/pause conditions \n

        Start PG 1 when MS1 gets above 38 \n
        Pause PG 1 when SW1 gets opened \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure Program 1 Moisture Start Condition
            self.config.BaseStation3200[1].programs[1].add_moisture_start_condition(_moisture_sensor_address=1)
            self.config.BaseStation3200[1].programs[1].moisture_start_conditions[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].moisture_start_conditions[1].set_moisture_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[1].moisture_start_conditions[1].set_moisture_threshold(_percent=38)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget and shutdown state\n
            - set water rationing state \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # Water Source 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[1].set_monthly_watering_budget(_budget=100000,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].water_sources[1].set_water_rationing_to_enabled()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n
        - Add pump sensor  ---> to point of control \n
        - Add point of control ---> to water source
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # Add & Configure POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].points_of_control[1].set_high_flow_limit(_limit=550,
                                                                                    with_shutdown_enabled=True)
            self.config.BaseStation3200[1].points_of_control[1].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=True)
            self.config.BaseStation3200[1].points_of_control[1].add_master_valve_to_point_of_control(
                                                                                        _master_valve_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_flow_meter_to_point_of_control(
                                                                                        _flow_meter_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_pump_to_point_of_control(_pump_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_pressure_sensor_to_point_of_control(
                                                                                            _pressure_sensor_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_high_pressure_limit(_limit=100,
                                                                                        with_shutdown_enabled=True)
            self.config.BaseStation3200[1].points_of_control[1].set_low_pressure_limit(_limit=200,
                                                                                       with_shutdown_enabled=True)

            # Add POC 1 to Water Source 1
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled state \n
                - set pipe stabilization time \n
                - set target flow \n
                - set limit zones by flow to true\n
                - set high flow variance tier 1 and shutdown enabled\n
                - set low flow variance tier 1 and shutdown enabled \n
                - set pipe fill units to time \n
                - set number zones to delay \n
                - set delay first zone \n
                - set delay for next zone \n
                - set delay after zone \n
                - set number zones to delay \n
                - set standard variance limit with shutdown \n
                - set used advanced flow \n
            - Add Mainline ---> to point of control
        """

        # here we can either execute the following uncommented lines in procedural fashion, or we could re-init the
        # object, would have to import Mainline at the top, effectively accomplishing the same thing by:
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_enabled()
            self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=7)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[1].set_high_flow_variance_tier_one(_percent=60,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].mainlines[1].set_low_flow_variance_tier_one(_percent=20,
                                                                                       _with_shutdown_enabled=False)
            self.config.BaseStation3200[1].mainlines[1].set_pipe_fill_units_to_time()
            self.config.BaseStation3200[1].mainlines[1].set_time_delay_before_first_zone(2)
            self.config.BaseStation3200[1].mainlines[1].set_time_delay_between_zone(3)
            self.config.BaseStation3200[1].mainlines[1].set_time_delay_after_zone(4)
            self.config.BaseStation3200[1].mainlines[1].set_number_zones_to_delay(5)
            self.config.BaseStation3200[1].mainlines[1].set_standard_variance_limit_with_shutdown(_percentage=50)
            self.config.BaseStation3200[1].mainlines[1].set_use_advanced_flow_to_true()

            # Add ML 1 to POC 1
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone

        Program | Mainline  | Zones
        ---------------------------------------------
        1       | 1         | 1,2
        3       | 1         | 49,50
        4       | 1         | 50
        99      | 8         | 197,198,199,200
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Add ZN 1,2,49,50 to ML 1
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=1)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=2)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=49)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=50)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        verify the entire configuration on the 3200 \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                    self.config.test_name,
                    sys._getframe().f_code.co_name,
                    date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                    str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_9(self):
        """
        ###############################
        Share controller, water source, point of control, mainline with Flow Station
        ###############################
            - Add the controller to the flow station
            - Share Water Source between the 3200 controller and the FlowStation
            - Share Point of Control between the 3200 controller and the FlowStation
            - Share Mainline between the 3200 controller and the FlowStation

        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            self.config.FlowStations[1].add_controller_to_flow_station(
                                                        _controller_address=1,
                                                        _flow_station_slot_number=1)

            # add Water Source on the Controller to the FlowStation
            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=1,
                _controller_water_source_address=1,
                _flow_station_water_source_slot_number=4)

            # Tell the 3200 that the FlowStation is in control of Water Source 1.
            self.config.BaseStation3200[1].get_water_source(1).set_manage_by_flowstation()

            # add Point of Control on the Controller to the FlowStation
            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                                               _controller_address=1,
                                               _controller_point_of_control_address=1,
                                               _flow_station_point_of_control_slot_number=4)

            # Tell the 3200 that the FlowStation is in control of Point of Control 1.
            self.config.BaseStation3200[1].get_point_of_control(1).set_manage_by_flowstation()

            # add Mainline on the Controller to the FlowStation
            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=1,
                _controller_mainline_address=1,
                _flow_station_mainline_slot_number=4)

            # Tell the 3200 that the FlowStation is in control of Mainline 1.
            self.config.BaseStation3200[1].get_mainline(1).set_manage_by_flowstation()

            self.config.FlowStations[1].get_water_source(ws_number=4).add_point_of_control_to_water_source(_point_of_control_address=4)
            self.config.FlowStations[1].get_point_of_control(pc_number=4).add_mainline_to_point_of_control(_mainline_address=4)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_10(self):
        """
        ###############################
        verify the entire configuration through the Flow Station Serial Port \n
        ###############################
            - Get information for each object from the flow station
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.FlowStations[1].do_increment_clock(minutes=1)
            self.config.FlowStations[1].save_programming_to_flow_station()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            #   allow time for DG packets between 3200 and FlowStation
            time.sleep(5)
            self.config.FlowStations[1].verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_11(self):
        """
        ###############################
        Stop Sharing objects with the Flow Station
        ###############################
            - Stop sharing the Point of Control
            - Stop Sharing the Mainline
            - Stop sharing the Water Source
            - Stop sharing the 3200 Controller
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Remove the Point of Control from the Flow Station
            self.config.FlowStations[1].remove_controller_point_of_control_from_flowstation(
                                                                        _controller_address=1,
                                                                        _controller_point_of_control_address=1,
                                                                        _flowstation_point_of_control_slot_number=4)

            # Tell the 3200 that the FlowStation is no longer in control of Point of Control 1.
            self.config.BaseStation3200[1].get_point_of_control(1).set_do_not_manage_by_flowstation()

            #  Remove the Mainline from the Flow Station
            self.config.FlowStations[1].remove_controller_mainline_from_flowstation(
                                                                        _controller_address=1,
                                                                        _controller_mainline_address=1,
                                                                        _flowstation_mainline_slot_number=4)

            # Tell the 3200 that the FlowStation is no longer in control of Mainline 1.
            self.config.BaseStation3200[1].get_mainline(1).set_do_not_manage_by_flowstation()

            # Remove the Water Source from the Flow Station
            self.config.FlowStations[1].remove_controller_water_source_from_flowstation(
                                                                        _controller_address=1,
                                                                        _controller_water_source_address=1,
                                                                        _flow_station_water_source_slot_number=4)

            # Tell the 3200 that the FlowStation is no longer in control of Water Source 1.
            self.config.BaseStation3200[1].get_water_source(1).set_do_not_manage_by_flowstation()

            # Remove the Controller from the Flow Station
            self.config.FlowStations[1].remove_controller_from_flowstation(_flowstation_slot_number=1)

            self.config.FlowStations[1].do_increment_clock(minutes=1)
            self.config.FlowStations[1].save_programming_to_flow_station()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
