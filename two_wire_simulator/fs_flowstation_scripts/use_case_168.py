import sys
from time import sleep

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
from datetime import timedelta
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from csv_handler import CSVWriter

from common import helper_methods

# Browser pages used
# import page_factory

__author__ = 'Dillon'


class ControllerUseCase168(object):
    """
    Test name:
        - CN UseCase168 Alternate water path is used for program run during high flow shutdown

    Purpose:
        - This test is meant to test that an alternate water path is used, when one water path has been determined to be
          unavailable. As of 06/07/2018 the bug FLOWSTN-339 keeps all zones on a mainline that has two POCs sharing a
          water source in a 'WA' state.

    User Story:
        1) As a user I want my irrigation system to use an alternate water path for a program run, when one water path
           is unavailable due to a high flow shutdown. I want my alternate water path to provide my mainlines
           the water they require, so each of my zones are watered.

    Coverage Area:
        1. Configure program 1 for both controllers

        2. Setup zones
            - Add 1 Primary and 2 Linked zones on controller 1, Zn = 1-3
            - Add 1 Primary and 2 Linked zones on controller 1, Zn = 4-6

        3. Setup Water Sources
            - Add two water sources to controller 1, C1WS1 & C1WS2
            - Add a water source to controller 2, C2WS3

        4. Setup Points of Control
            - Add a POC on controller 1 with a Master Valve and Flow Meter. C1P1
            - Add a POC on controller 2 with a Master Valve and Flow Meter. C2P1
            - Add a POC on controller 1 with a Master Valve and Flow Meter. C1P2
            - Add a POC on controller 2 with a Master Valve and Flow Meter. C2P2

        5. Setup Mainlines
            - Add a Mainline on controller 1. C1M1 --> C1P1 & C2P1
            - Add a Mainline on controller 1. C1M2 --> C1P2 & C2P2

        6. Share Controllers and Flow Objects with the FlowStation
            - Share controller 1 with the FlowStation
                - Share a Water Source on controller 1 with the FlowStation
                - Share a Point of Control on controller 1 with the FlowStation
                - Share a Mainline on controller 1 with the FlowStation
            - Share controller 2 with the FlowStation
                - Share a Water Source on controller 2 with the FlowStation
                - Share a Point of Control on controller 2 with the FlowStation
                - Share a Mainline on controller 2 with the FlowStation

        7. Setup zone one mainlines
            - add zones (1-3) to a mainline on controller 1. C1M1 --> Zn1-3
            - add zones (4-6) to a mainline on controller 1. C1M2 --> Zn4-6

        8. Set design flow on all added zones on controller 1
            - zones (1-3) on controller 1 have a design flow of 50 gpm
            - zones (4-6) on controller 1 have a design flow of 100 gpm

        9. System Layout:
             C1WS1          C1WS2       C2WS3
             /   \            |           |
           C1P1  C2P1       C1P2        C2P2
             \   /             \       /
             C1M1                C1M2
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase166' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/fs_json_config_files'
                                    )
        # initializes the csv writer object so it can be used in the remainder of the use_case
        self.csv_writer = CSVWriter(file_name=test_name,
                                    relative_path='fs_flowstation_scripts',
                                    delimiter=',',
                                    line_terminator='\n')

        try:
            self.csv_writer.open()
        except IOError:
            e_msg = self.csv_writer.fname + ' file is in use'
            raise IOError(e_msg)

        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=False)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        This step is setting up our program, which will be driving this test. We have set a start time as well as the
        days to allow watering.
        """
        program_start_times = [600]  # 10:00am start time
        program_water_windows = ['111111111111111111111111']
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set max zones on the controller to 6 so that it runs all zones
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=6)

            # Add and configure Program 1 to controller 1
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_water_window(_ww=program_water_windows)
            self.config.BaseStation3200[1].programs[1].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=6)
            self.config.BaseStation3200[1].programs[1].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(_sun=True,
                                                                                                           _mon=True,
                                                                                                           _tues=True,
                                                                                                           _wed=True,
                                                                                                           _thurs=True,
                                                                                                           _fri=True,
                                                                                                           _sat=True)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_start_times)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        This step is adding all six program zones to our controller. Controller one will have all six zone, with zone
        1 and 4 being the primaries and the rest being linked.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # add zones to programs on controller 1
            # Add & Configure Program Zone 1
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=5)

            # Add & Configure Program Zone 2
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1)

            # Add & Configure Program Zone 3
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_as_linked_zone(_primary_zone=1)

            # Add & Configure Program Zone 4
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[1].zone_programs[4].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[4].set_run_time(_minutes=5)

            # Add & Configure Program Zone 5
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[5].set_as_linked_zone(_primary_zone=4)

            # Add & Configure Program Zone 6
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=6)
            self.config.BaseStation3200[1].programs[1].zone_programs[6].set_as_linked_zone(_primary_zone=4)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        setup WaterSources
        ############################
        This step is setting up our water sources for our system. Two of our water sources will be on controller 1 and
        the third water source will be on controller 2.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Water Source 1 and 2 on controller 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].set_priority(_priority_for_water_source=2)

            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=2)
            self.config.BaseStation3200[1].water_sources[2].set_enabled()
            self.config.BaseStation3200[1].water_sources[2].set_priority(_priority_for_water_source=2)

            # Water Source 3 on controller 2
            self.config.BaseStation3200[2].add_water_source_to_controller(_water_source_address=3)
            self.config.BaseStation3200[2].water_sources[3].set_enabled()
            self.config.BaseStation3200[2].water_sources[3].set_priority(_priority_for_water_source=2)


        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        setup Point of Controls
        ############################
        This step is setting up all four of our POCs for our system. There will be two POCs on each controller. Each
        POC needs a flow meter and master valve, as well as a set target flow. After creating the POCs they will be
        added to any water sources and mainlines they need to be connected to. We have two on each controller because
        we want to be able to have a "backup" POC on the water path in case the other controller goes down.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Adding POC 1 to controller 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_flow_meter_to_point_of_control(
                _flow_meter_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=50)

            # Adding POC 2 to controller 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=2)
            self.config.BaseStation3200[1].points_of_control[2].set_enabled()
            self.config.BaseStation3200[1].points_of_control[2].add_master_valve_to_point_of_control(
                _master_valve_address=2)
            self.config.BaseStation3200[1].points_of_control[2].add_flow_meter_to_point_of_control(
                _flow_meter_address=2)
            self.config.BaseStation3200[1].points_of_control[2].set_target_flow(_gpm=50)

            # This will create the connection from our water source 1 to POC 1 on controller 1
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=1)

            # This will create the connection from our water source 2 to POC 2 on controller 1
            self.config.BaseStation3200[1].water_sources[2].add_point_of_control_to_water_source(
                _point_of_control_address=2)

            # Adding POC 1 to controller 2
            self.config.BaseStation3200[2].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[2].points_of_control[1].set_enabled()
            self.config.BaseStation3200[2].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=1)
            self.config.BaseStation3200[2].points_of_control[1].add_flow_meter_to_point_of_control(
                _flow_meter_address=1)
            self.config.BaseStation3200[2].points_of_control[1].set_target_flow(_gpm=100)
            self.config.BaseStation3200[2].points_of_control[1].set_high_flow_limit(_limit=150.0,
                                                                                    with_shutdown_enabled=True)

            # Adding POC 2 to controller 2
            self.config.BaseStation3200[2].add_point_of_control_to_controller(_point_of_control_address=2)
            self.config.BaseStation3200[2].points_of_control[2].set_enabled()
            self.config.BaseStation3200[2].points_of_control[2].add_master_valve_to_point_of_control(
                _master_valve_address=2)
            self.config.BaseStation3200[2].points_of_control[2].add_flow_meter_to_point_of_control(
                _flow_meter_address=2)
            self.config.BaseStation3200[2].points_of_control[2].set_target_flow(_gpm=100)
            self.config.BaseStation3200[2].points_of_control[2].set_high_flow_limit(_limit=150.0,
                                                                                    with_shutdown_enabled=True)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ##################
        setup mainlines
        ##################
        This step is setting up our mainlines for our system. There will be two mainlines that are both on controller 1.
        This will give us two water paths for each mainline.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure ML 1 on controller 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=60)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_true()

            # Add & Configure ML 2 on controller 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=2)
            self.config.BaseStation3200[1].mainlines[2].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[2].set_target_flow(_gpm=60)
            self.config.BaseStation3200[1].mainlines[2].set_limit_zones_by_flow_to_true()

            # This makes the connection from mainline 1 to POC 1 on controller
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)

            # This makes the connection from mainline 1 to POC 2 on controller
            self.config.BaseStation3200[1].points_of_control[2].add_mainline_to_point_of_control(_mainline_address=2)

            # This makes our POC object match the controller
            self.config.BaseStation3200[2].points_of_control[1].ml = 1
            self.config.BaseStation3200[2].points_of_control[1].controller_mainline = 1

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ######################
        setup zones on mainlines
        ######################
        This step is attaching certain zones to the mainlines so we can verify that the zones can still water after one
        of the POCs upstream of the mainline becomes unavailable, due to its controller going offline.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Add ZN 1,2,3 to ML 1 on controller 1
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=1)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=2)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=3)

            # Add ZN 4,5,6 to ML 8 on controller 1
            self.config.BaseStation3200[1].mainlines[2].add_zone_to_mainline(_zone_address=4)
            self.config.BaseStation3200[1].mainlines[2].add_zone_to_mainline(_zone_address=5)
            self.config.BaseStation3200[1].mainlines[2].add_zone_to_mainline(_zone_address=6)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ############################
        Give each zone a design flow
        ############################
        This step is setting our design flow for each zone.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # set design flow of zones  on controller 1
            # Assign a design flow value to each zone so that they have a default setting
            self.config.BaseStation3200[1].zones[1].set_design_flow(_gallons_per_minute=20)
            self.config.BaseStation3200[1].zones[2].set_design_flow(_gallons_per_minute=20)
            self.config.BaseStation3200[1].zones[3].set_design_flow(_gallons_per_minute=20)

            # set design flow of zones  on controller 1
            self.config.BaseStation3200[1].zones[4].set_design_flow(_gallons_per_minute=20)
            self.config.BaseStation3200[1].zones[5].set_design_flow(_gallons_per_minute=20)
            self.config.BaseStation3200[1].zones[6].set_design_flow(_gallons_per_minute=20)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        verify the entire configuration
        ###############################
        This step verifies our full configuration on both controllers. Refers to all the objects we created and
        verifies the controller can see all of them.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.BaseStation3200[2].verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        ###############################
        Share controllers, water sources, point of controls, and mainlines with Flow Station
        ###############################
        This step is setting up our FlowStation objects, so the FlowStation can properly help the system learn flow.
        Every object that was created above needs to be shared with the FlowStation.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add controller 1 & 2 to FlowStation
            self.config.FlowStations[1].add_controller_to_flow_station(
                _controller_address=1,
                _flow_station_slot_number=1)
            self.config.FlowStations[1].add_controller_to_flow_station(
                _controller_address=2,
                _flow_station_slot_number=2)

            # Add our three water sources to the FlowStation
            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=1,
                _controller_water_source_address=1,
                _flow_station_water_source_slot_number=1)
            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=1,
                _controller_water_source_address=2,
                _flow_station_water_source_slot_number=2)
            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=2,
                _controller_water_source_address=3,
                _flow_station_water_source_slot_number=3)

            # Tell the 3200 that the FlowStation is in control of Water Sources.
            self.config.BaseStation3200[1].water_sources[1].set_manage_by_flowstation()
            self.config.BaseStation3200[1].water_sources[2].set_manage_by_flowstation()
            self.config.BaseStation3200[2].water_sources[3].set_manage_by_flowstation()

            # Add controller 1 & 2 POCs to FlowStation
            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=1,
                _controller_point_of_control_address=1,
                _flow_station_point_of_control_slot_number=1)
            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=1,
                _controller_point_of_control_address=2,
                _flow_station_point_of_control_slot_number=2)
            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=2,
                _controller_point_of_control_address=1,
                _flow_station_point_of_control_slot_number=3)
            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=2,
                _controller_point_of_control_address=2,
                _flow_station_point_of_control_slot_number=4)

            self.config.BaseStation3200[1].points_of_control[1].set_manage_by_flowstation()
            self.config.BaseStation3200[1].points_of_control[2].set_manage_by_flowstation()
            self.config.BaseStation3200[2].points_of_control[1].set_manage_by_flowstation()
            self.config.BaseStation3200[2].points_of_control[2].set_manage_by_flowstation()

            # Adding ML's to FS
            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=1,
                _controller_mainline_address=1,
                _flow_station_mainline_slot_number=1)
            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=1,
                _controller_mainline_address=2,
                _flow_station_mainline_slot_number=2)

            self.config.BaseStation3200[1].mainlines[1].set_manage_by_flowstation()
            self.config.BaseStation3200[1].mainlines[2].set_manage_by_flowstation()

            # Connect water paths in the FlowStation to form our full system configuration
            # First configuration for C1WS1 --> C1P1 & C2P1 --> C1M1
            self.config.FlowStations[1].get_water_source(1).add_point_of_control_to_water_source(_point_of_control_address=1)

            # Needed to use the get method here so I could reach the correct POC on controller 2
            self.config.FlowStations[1].get_water_source(1).add_point_of_control_to_water_source(_point_of_control_address=3)
            self.config.FlowStations[1].get_point_of_control(1).add_mainline_to_point_of_control(_mainline_address=1)
            self.config.FlowStations[1].get_point_of_control(3).add_mainline_to_point_of_control(_mainline_address=1)

            # Second configuration for C1WS2 --> C1P2 \
            #                          C2WS3 --> C2P2 / C1M2
            self.config.FlowStations[1].get_water_source(2).add_point_of_control_to_water_source(_point_of_control_address=2)

            # Needed to use the get method here so I could reach the correct POC on controller 2
            self.config.FlowStations[1].get_water_source(3).add_point_of_control_to_water_source(_point_of_control_address=4)

            self.config.FlowStations[1].get_point_of_control(2).add_mainline_to_point_of_control(_mainline_address=2)
            self.config.FlowStations[1].get_point_of_control(4).add_mainline_to_point_of_control(_mainline_address=2)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        ###############################
        verify the FlowStation configuration
        ###############################
        This step will verify the system configuration we have set up on the FlowStation, and save all the programming.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            self.config.FlowStations[1].save_programming_to_flow_station()
            self.config.FlowStations[1].verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_11(self):
        """
        #####################
        Set flow meter values
        #####################
        This step is forcing our flow meters we placed on our POCs to have a reading of 20GPM on controller 1, and
        160GPM on controller 2. This is an arbitrary number that was picked.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=20)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].flow_meters[2].bicoder.set_flow_rate(_gallons_per_minute=20)
            self.config.BaseStation3200[1].flow_meters[2].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[2].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=160)
            self.config.BaseStation3200[2].flow_meters[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[2].flow_meters[2].bicoder.set_flow_rate(_gallons_per_minute=160)
            self.config.BaseStation3200[2].flow_meters[2].bicoder.self_test_and_update_object_attributes()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_12(self):
        """
        ###############################
        Verify device statuses
        ###############################
        In this step we are verifying the alternate water path is providing water to our devices in that path and
        our zones. Controller 1 has asked the FlowStation for water allocation and is waiting for a response.
        This step will advance time to 10:01AM
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_date_and_time(_date='05/21/2018', _time='09:59:00')
            self.config.BaseStation3200[2].set_date_and_time(_date='05/21/2018', _time='09:59:00')
            self.config.FlowStations[1].set_date_and_time(_date='05/21/2018', _time='09:59:00')

            helper_methods.increment_controller_flowstation_clocks(self.config.FlowStations[1], self.config.BaseStation3200, minutes=2)

            self.config.BaseStation3200[1].get_water_source(ws_number=1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_point_of_control(pc_number=1).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_point_of_control(pc_number=1).statuses.verify_status_is_flow_fault()
            self.config.BaseStation3200[1].get_mainline(ml_number=1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].get_water_source(ws_number=2).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(ws_number=3).statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].get_point_of_control(pc_number=2).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_point_of_control(pc_number=2).statuses.verify_status_is_flow_fault()
            self.config.BaseStation3200[1].get_mainline(ml_number=2).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[2].get_point_of_control(pc_number=1).messages.verify_high_flow_shutdown_message()
            self.config.BaseStation3200[2].get_point_of_control(pc_number=2).messages.verify_high_flow_shutdown_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_13(self):
        """
        ###############################
        Verify device statuses
        ###############################
        In this step we are verifying the alternate water path is providing water to our devices in that path and
        our zones. Zones have started to water, because controller 1 has acted on the water allocation from the
        FlowStation. This step will advance time to 10:02AM
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # second minute
            helper_methods.increment_controller_flowstation_clocks(self.config.FlowStations[1], self.config.BaseStation3200, minutes=1)
            self.config.BaseStation3200[1].get_water_source(ws_number=1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_point_of_control(pc_number=1).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_point_of_control(pc_number=1).statuses.verify_status_is_flow_fault()
            self.config.BaseStation3200[1].get_mainline(ml_number=1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].get_water_source(ws_number=2).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(ws_number=3).statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].get_point_of_control(pc_number=2).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_point_of_control(pc_number=2).statuses.verify_status_is_flow_fault()
            self.config.BaseStation3200[1].get_mainline(ml_number=2).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_14(self):
        """
        ###############################
        Verify device statuses
        ###############################
        In this step we are verifying the alternate water path is providing water to our devices in that path and
        our zones. We are also clearing our flow fault. This step will advance time to 10:04AM. The 3200 take an
        additional minute to update the status of zones 3 and 6 since we now have more water allocated now that we have
        cleared the flow faults on C2:P1 and C2:P2.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            self.config.BaseStation3200[2].get_point_of_control(pc_number=1).messages.clear_high_flow_shutdown_message()
            self.config.BaseStation3200[2].get_point_of_control(pc_number=2).messages.clear_high_flow_shutdown_message()

            self.config.BaseStation3200[2].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=140)
            self.config.BaseStation3200[2].flow_meters[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[2].flow_meters[2].bicoder.set_flow_rate(_gallons_per_minute=140)
            self.config.BaseStation3200[2].flow_meters[2].bicoder.self_test_and_update_object_attributes()

            # third minute
            helper_methods.increment_controller_flowstation_clocks(self.config.FlowStations[1], self.config.BaseStation3200, minutes=2)
            self.config.BaseStation3200[1].get_water_source(ws_number=1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_point_of_control(pc_number=1).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_point_of_control(pc_number=1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(ml_number=1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].get_water_source(ws_number=2).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(ws_number=3).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_point_of_control(pc_number=2).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_point_of_control(pc_number=2).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(ml_number=2).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].get_point_of_control(pc_number=1).messages.check_for_high_flow_shutdown_message_not_present()
            self.config.BaseStation3200[2].get_point_of_control(pc_number=2).messages.check_for_high_flow_shutdown_message_not_present()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_15(self):
        """
        ###############################
        verify the entire configuration
        ###############################
        This step is used to make sure nothing in our system has gone offline, or been added since the last config check.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.FlowStations[1].save_programming_to_flow_station()
            helper_methods.increment_controller_flowstation_clocks(self.config.FlowStations[1], self.config.BaseStation3200, minutes=1)
            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.BaseStation3200[2].verify_full_configuration()
            self.config.FlowStations[1].verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
