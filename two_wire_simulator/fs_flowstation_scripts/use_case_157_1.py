import sys

from datetime import time, timedelta
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
from common.configuration import Configuration

# import log_handler for logging functionality
from common.logging_handler import log_handler

# Objects
from common.objects.base_classes.web_driver import *
from common.imports import opcodes

from common import helper_methods

__author__ = 'Eldin'


class ControllerUseCase157_1(object):
    """
    Test name:
        - CN UseCase157_1 backup and restore to / from USB on a FlowStation

    User Story:
        1)  As a user, I would like to backup (and restore) my flowstation configuration to / from a USB drive


    Coverage and Objectives:
        1.	When a flow station is backed-up/restored from USB:
            a.	flow station should retain network connectivity settings (BaseManager connection)
            b.	flow station should retain assigned controllers, water sources, points of control, mainlines in their
                respective slots (and their settings)
            c.	flow station should retain all assignments:
                i. water source to point of control
                ii.	point of control to mainline
                iii. mainline to mainline
                iv.	mainline to points of control

    Not Covered:
        1.  User PINs
        2.  Backup / Restore to / from USB
        3.  3200 backup / restore

    - Flow Station Setup:

            Controller 1      Controller 2
            - WS8(20)           - WS7(1)
            - PC1(3)            - PC1(15)
            - PC8(10)           - PC7(1)
            - ML1(2)            - PC8(20)
            - ML2(12)           - ML7(20)
                                - ML8(9)

            -FlowStation assignments
                  C1:W8           C1:W1
                    |               |
                    |              /  \
                  C1:P8       C1:P1   C2:P7
                    |             \    /
                    |              C1:M1
                    |                |
                    |  ------------------
                    |/     |      |      | 
                  C1:M2  C2:M8  C7:P1  C2:P8
                                         |
                                       C2:M7
        """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/fs_json_config_files'
                                    )
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        - Connect to BaseManager
        
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Connect FlowStation to BaseManager
            # TODO: change once the config.py is updated
            self.config.FlowStations[1].add_basemanager_connection_to_controller()
            self.config.FlowStations[1].basemanager_connection[1].verify_ip_address_state()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        Setup WaterSources
        ############################
        Add water sources -----> to controller
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
        - Add a water source empty condition. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Water Source 7
            self.config.BaseStation3200[2].add_water_source_to_controller(_water_source_address=7)
            self.config.BaseStation3200[2].water_sources[7].set_enabled()
            self.config.BaseStation3200[2].water_sources[7].set_priority(_priority_for_water_source=1)
            self.config.BaseStation3200[2].water_sources[7].set_monthly_watering_budget(_budget=10000)
            self.config.BaseStation3200[2].water_sources[7].set_water_rationing_to_disabled()

            # Water Source 8
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=8)
            self.config.BaseStation3200[1].water_sources[8].set_enabled()
            self.config.BaseStation3200[1].water_sources[8].set_priority(_priority_for_water_source=1)
            self.config.BaseStation3200[1].water_sources[8].set_monthly_watering_budget(_budget=10000)
            self.config.BaseStation3200[1].water_sources[8].set_water_rationing_to_disabled()

            # Water Source 8 empty condition
            self.config.BaseStation3200[1].water_sources[8].add_switch_empty_condition(_event_switch_address=1)
            self.config.BaseStation3200[1].water_sources[8].switch_empty_conditions[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[8].switch_empty_conditions[1]. \
                set_switch_empty_condition_to_closed()
            self.config.BaseStation3200[1].water_sources[8].switch_empty_conditions[1].set_empty_wait_time(_minutes=8)
            
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure C1 POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1]. \
                add_master_valve_to_point_of_control(_master_valve_address=1)
            self.config.BaseStation3200[1].points_of_control[1]. \
                add_flow_meter_to_point_of_control(_flow_meter_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].points_of_control[1].set_high_flow_limit(_limit=550,
                                                                                    with_shutdown_enabled=True)
            self.config.BaseStation3200[1].points_of_control[1].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=True)

            # Add & Configure C1 POC 8
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=8)
            self.config.BaseStation3200[1].points_of_control[8].set_enabled()
            self.config.BaseStation3200[1].points_of_control[8]. \
                add_master_valve_to_point_of_control(_master_valve_address=8)
            self.config.BaseStation3200[1].points_of_control[8]. \
                add_flow_meter_to_point_of_control(_flow_meter_address=8)
            self.config.BaseStation3200[1].points_of_control[8].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].points_of_control[8].set_high_flow_limit(_limit=75,
                                                                                    with_shutdown_enabled=False)
            self.config.BaseStation3200[1].points_of_control[8].set_unscheduled_flow_limit(_gallons=5,
                                                                                           with_shutdown_enabled=False)

            # Add & Configure C7 POC 1
            self.config.BaseStation3200[2].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[2].points_of_control[1].set_enabled()
            self.config.BaseStation3200[2].points_of_control[1]. \
                add_master_valve_to_point_of_control(_master_valve_address=1)
            self.config.BaseStation3200[2].points_of_control[1]. \
                add_flow_meter_to_point_of_control(_flow_meter_address=1)
            self.config.BaseStation3200[2].points_of_control[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[2].points_of_control[1].set_high_flow_limit(_limit=550,
                                                                                    with_shutdown_enabled=True)
            self.config.BaseStation3200[2].points_of_control[1].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=True)

            # Add & Configure C2 POC 7
            self.config.BaseStation3200[2].add_point_of_control_to_controller(_point_of_control_address=7)
            self.config.BaseStation3200[2].points_of_control[7].set_enabled()
            self.config.BaseStation3200[2].points_of_control[7]. \
                add_master_valve_to_point_of_control(_master_valve_address=7)
            self.config.BaseStation3200[2].points_of_control[7]. \
                add_flow_meter_to_point_of_control(_flow_meter_address=7)
            self.config.BaseStation3200[2].points_of_control[7].set_target_flow(_gpm=50)
            self.config.BaseStation3200[2].points_of_control[7].set_high_flow_limit(_limit=75,
                                                                                    with_shutdown_enabled=False)
            self.config.BaseStation3200[2].points_of_control[7].set_unscheduled_flow_limit(_gallons=5,
                                                                                           with_shutdown_enabled=False)

            # Add & Configure C2 POC 8
            self.config.BaseStation3200[2].add_point_of_control_to_controller(_point_of_control_address=8)
            self.config.BaseStation3200[2].points_of_control[8].set_enabled()
            self.config.BaseStation3200[2].points_of_control[8]. \
                add_master_valve_to_point_of_control(_master_valve_address=8)
            self.config.BaseStation3200[2].points_of_control[8]. \
                add_flow_meter_to_point_of_control(_flow_meter_address=8)
            self.config.BaseStation3200[2].points_of_control[8].set_target_flow(_gpm=50)
            self.config.BaseStation3200[2].points_of_control[8].set_high_flow_limit(_limit=75,
                                                                                    with_shutdown_enabled=False)
            self.config.BaseStation3200[2].points_of_control[8].set_unscheduled_flow_limit(_gallons=5,
                                                                                           with_shutdown_enabled=False)

            
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure C1 ML 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=4)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[1].set_high_flow_variance_tier_one(_percent=5,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].mainlines[1].set_low_flow_variance_tier_one(_percent=20,
                                                                                       _with_shutdown_enabled=True)

            # Add & Configure C1 ML 2
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=2)
            self.config.BaseStation3200[1].mainlines[2].set_pipe_stabilization_time(_minutes=4)
            self.config.BaseStation3200[1].mainlines[2].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].mainlines[2].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[2].set_high_flow_variance_tier_one(_percent=5,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].mainlines[2].set_low_flow_variance_tier_one(_percent=20,
                                                                                       _with_shutdown_enabled=True)

            # Add & Configure C2 ML 7
            self.config.BaseStation3200[2].add_mainline_to_controller(_mainline_address=7)
            self.config.BaseStation3200[2].mainlines[7].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[2].mainlines[7].set_target_flow(_gpm=50)
            self.config.BaseStation3200[2].mainlines[7].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[2].mainlines[7].set_high_flow_variance_tier_one(_percent=20,
                                                                                        _with_shutdown_enabled=False)
            self.config.BaseStation3200[2].mainlines[7].set_high_flow_variance_tier_one(_percent=5,
                                                                                        _with_shutdown_enabled=False)

            # Add & Configure C2 ML 8
            self.config.BaseStation3200[2].add_mainline_to_controller(_mainline_address=8)
            self.config.BaseStation3200[2].mainlines[8].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[2].mainlines[8].set_target_flow(_gpm=50)
            self.config.BaseStation3200[2].mainlines[8].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[2].mainlines[8].set_high_flow_variance_tier_one(_percent=20,
                                                                                        _with_shutdown_enabled=False)
            self.config.BaseStation3200[2].mainlines[8].set_high_flow_variance_tier_one(_percent=5,
                                                                                        _with_shutdown_enabled=False)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines
            - set up zone  Attributes associated with mainlines
                - set design flow on zone
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Add zone to mainlines 1 and 2 on controller 1
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=1)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=2)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=3)
            self.config.BaseStation3200[1].mainlines[2].add_zone_to_mainline(_zone_address=4)
            self.config.BaseStation3200[1].mainlines[2].add_zone_to_mainline(_zone_address=5)
            self.config.BaseStation3200[1].mainlines[2].add_zone_to_mainline(_zone_address=6)

            # Add zone to mainlines 7 and 8 on controller 2
            self.config.BaseStation3200[2].mainlines[7].add_zone_to_mainline(_zone_address=1)
            self.config.BaseStation3200[2].mainlines[7].add_zone_to_mainline(_zone_address=2)
            self.config.BaseStation3200[2].mainlines[7].add_zone_to_mainline(_zone_address=3)
            self.config.BaseStation3200[2].mainlines[8].add_zone_to_mainline(_zone_address=4)
            self.config.BaseStation3200[2].mainlines[8].add_zone_to_mainline(_zone_address=5)
            self.config.BaseStation3200[2].mainlines[8].add_zone_to_mainline(_zone_address=6)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ###############################
        Share controller, water source, point of control, mainline with Flow Station
        ###############################
            - Flow Station Setup:
        
                Controller 1      Controller 7
                - WS8(20)           - WS7(1)
                - PC1(3)            - PC1(15)
                - PC8(10)           - PC7(1)
                - ML1(2)            - PC8(20)
                - ML2(12)           - ML7(20)
                                    - ML8(9)
    
            -FlowStation assignments
                  C1:W8           C1:W1
                    |               |
                    |              /  \
                  C1:P8       C1:P1   C2:P7
                    |             \    /
                    |              C1:M1
                    |                |
                    |  ------------------
                    |/     |      |      | 
                  C1:M2  C2:M8  C2:P1  C2:P8
                                         |
                                       C2:M7
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            ####################
            # Controller 1

            # Add controller 1 to FlowStation
            self.config.FlowStations[1].add_controller_to_flow_station(
                _controller_address=1,
                _flow_station_slot_number=1)

            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=1,
                _controller_water_source_address=8,
                _flow_station_water_source_slot_number=20)

            # Tell the 3200 that the FlowStation is in control of Water Source 1.
            self.config.BaseStation3200[1].get_water_source(8).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=1,
                _controller_point_of_control_address=1,
                _flow_station_point_of_control_slot_number=3)

            # Tell the 3200 that the FlowStation is in control of Point of Control 1.
            self.config.BaseStation3200[1].get_point_of_control(1).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=1,
                _controller_point_of_control_address=8,
                _flow_station_point_of_control_slot_number=10)

            # Tell the 3200 that the FlowStation is in control of Point of Control 8.
            self.config.BaseStation3200[1].get_point_of_control(8).set_manage_by_flowstation()

            # Mainlines 1
            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=1,
                _controller_mainline_address=1,
                _flow_station_mainline_slot_number=2)

            # Tell the 3200 that the FlowStation is in control of Mainline 1.
            self.config.BaseStation3200[1].get_mainline(1).set_manage_by_flowstation()

            # Mainlines 2
            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=1,
                _controller_mainline_address=2,
                _flow_station_mainline_slot_number=12)

            # Tell the 3200 that the FlowStation is in control of Mainline 1.
            self.config.BaseStation3200[1].get_mainline(2).set_manage_by_flowstation()

            #########################
            # Controller 2

            # Add controller 2 to FlowStation
            self.config.FlowStations[1].add_controller_to_flow_station(
                _controller_address=2,
                _flow_station_slot_number=2)

            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=2,
                _controller_water_source_address=7,
                _flow_station_water_source_slot_number=1)

            # Tell the 3200 that the FlowStation is in control of Water Source 1.
            self.config.BaseStation3200[2].get_water_source(7).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=2,
                _controller_point_of_control_address=1,
                _flow_station_point_of_control_slot_number=15)

            # Tell the 3200 that the FlowStation is in control of Point of Control 1.
            self.config.BaseStation3200[2].get_point_of_control(1).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=2,
                _controller_point_of_control_address=7,
                _flow_station_point_of_control_slot_number=1)

            # Tell the 3200 that the FlowStation is in control of Point of Control 8.
            self.config.BaseStation3200[2].get_point_of_control(7).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=2,
                _controller_point_of_control_address=8,
                _flow_station_point_of_control_slot_number=20)

            # Tell the 3200 that the FlowStation is in control of Point of Control 8.
            self.config.BaseStation3200[2].get_point_of_control(8).set_manage_by_flowstation()

            # Mainlines 7
            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=2,
                _controller_mainline_address=7,
                _flow_station_mainline_slot_number=20)

            # Tell the 3200 that the FlowStation is in control of Mainline 1.
            self.config.BaseStation3200[2].get_mainline(7).set_manage_by_flowstation()

            # Mainlines 8
            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=2,
                _controller_mainline_address=8,
                _flow_station_mainline_slot_number=9)

            # Tell the 3200 that the FlowStation is in control of Mainline 1.
            self.config.BaseStation3200[2].get_mainline(8).set_manage_by_flowstation()

            ####################
            # FlowStation assignments for outputs

            # Assign WSs to POCs
            # WS20 -> PC10
            # WS1 -> PC3
            # WS1 -> PC1
            self.config.FlowStations[1].get_water_source(ws_number=20).add_point_of_control_to_water_source(_point_of_control_address=10)
            self.config.FlowStations[1].get_water_source(ws_number=1).add_point_of_control_to_water_source(_point_of_control_address=3)
            self.config.FlowStations[1].get_water_source(ws_number=1).add_point_of_control_to_water_source(_point_of_control_address=1)

            # Assign POC to ML
            # PC10 -> ML12
            # PC1 -> ML2
            # PC3 -> ML2
            # PC20 -> ML20
            self.config.FlowStations[1].get_point_of_control(pc_number=10).add_mainline_to_point_of_control(_mainline_address=12)
            self.config.FlowStations[1].get_point_of_control(pc_number=1).add_mainline_to_point_of_control(_mainline_address=2)
            self.config.FlowStations[1].get_point_of_control(pc_number=3).add_mainline_to_point_of_control(_mainline_address=2)
            self.config.FlowStations[1].get_point_of_control(pc_number=20).add_mainline_to_point_of_control(_mainline_address=20)

            # Assign ML to ML
            # ML2 -> ML12
            # ML2 -> ML9
            self.config.FlowStations[1].get_mainline(ml_number=2).add_mainline_to_mainline(_mainline_address=12)
            self.config.FlowStations[1].get_mainline(ml_number=2).add_mainline_to_mainline(_mainline_address=9)

            # Assign ML to POC
            # ML2 -> PC15
            # ML2 -> PC20
            self.config.FlowStations[1].get_mainline(ml_number=2).add_poc_to_mainline(_poc_address=15)
            self.config.FlowStations[1].get_mainline(ml_number=2).add_poc_to_mainline(_poc_address=20)
            
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.BaseStation3200[2].verify_full_configuration()
            self.config.FlowStations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        Send backup to USB. This takes all of our programming and creates a backup on a USB driver that we will
        later use in the test to do a restore.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # start clock
            self.config.FlowStations[1].start_clock()
            time.sleep(5)
            self.config.FlowStations[1].do_backup_programming(where_to=opcodes.usb_flash_storage)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        Clears all the programming from the controller so we can do a restore from a clean state. \n
        Wait for the 3200 and FS to reconnect to BaseManager. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # clear all programming
            self.config.FlowStations[1].reinitialize_controller()
            # have to reconnect to BMW before we can do the restore programming
            self.config.FlowStations[1].basemanager_connection[1].wait_for_bm_connection()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        We restore our programming from USB. It should restore from the most recent backup.

        We are stopping the 3200 clocks because they were stopped previously
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Restore programming from USB
            self.config.FlowStations[1].do_restore_programming(opcodes.usb_flash_storage)

            # Controller 1
            for zone in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone].bicoder.self_test_and_update_object_attributes()

            for moisture_sensor in sorted(self.config.BaseStation3200[1].moisture_sensors.keys()):
                self.config.BaseStation3200[1].moisture_sensors[moisture_sensor].bicoder. \
                    self_test_and_update_object_attributes()

            for master_valve in sorted(self.config.BaseStation3200[1].master_valves.keys()):
                self.config.BaseStation3200[1].master_valves[master_valve].bicoder. \
                    self_test_and_update_object_attributes()

            for flow_meter in sorted(self.config.BaseStation3200[1].flow_meters.keys()):
                self.config.BaseStation3200[1].flow_meters[flow_meter].bicoder. \
                    self_test_and_update_object_attributes()

            for event_switch in sorted(self.config.BaseStation3200[1].event_switches.keys()):
                self.config.BaseStation3200[1].event_switches[event_switch].bicoder. \
                    self_test_and_update_object_attributes()

            for temperature_sensor in sorted(self.config.BaseStation3200[1].temperature_sensors.keys()):
                self.config.BaseStation3200[1].temperature_sensors[temperature_sensor].bicoder. \
                    self_test_and_update_object_attributes()

            # Controller 2
            for zone in sorted(self.config.BaseStation3200[2].zones.keys()):
                self.config.BaseStation3200[2].zones[zone].bicoder.self_test_and_update_object_attributes()

            for moisture_sensor in sorted(self.config.BaseStation3200[2].moisture_sensors.keys()):
                self.config.BaseStation3200[2].moisture_sensors[moisture_sensor].bicoder. \
                    self_test_and_update_object_attributes()

            for master_valve in sorted(self.config.BaseStation3200[2].master_valves.keys()):
                self.config.BaseStation3200[2].master_valves[master_valve].bicoder. \
                    self_test_and_update_object_attributes()

            for flow_meter in sorted(self.config.BaseStation3200[2].flow_meters.keys()):
                self.config.BaseStation3200[2].flow_meters[flow_meter].bicoder. \
                    self_test_and_update_object_attributes()

            for event_switch in sorted(self.config.BaseStation3200[2].event_switches.keys()):
                self.config.BaseStation3200[2].event_switches[event_switch].bicoder. \
                    self_test_and_update_object_attributes()

            for temperature_sensor in sorted(self.config.BaseStation3200[2].temperature_sensors.keys()):
                self.config.BaseStation3200[2].temperature_sensors[temperature_sensor].bicoder. \
                    self_test_and_update_object_attributes()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        Verify all configurations
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Need to sync up FS and 3200's clocks here because the FS was taken out of SIM mode
            # which means that it's clock is ahead of the 3200s. This function will get the FS's
            # current date/time, and update the FS/3200's clocks to match the same time.
            helper_methods.sync_controller_clocks_with_flowstation(flowstation=self.config.FlowStations[1])

            # Move clocks forward a minute
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Wait for both controllers reconnect to the FS
            self.config.FlowStations[1].wait_for_flowstation_and_controllers_to_be_connected(max_wait=20)

            # Verify all configurations
            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.BaseStation3200[2].verify_full_configuration()
            self.config.FlowStations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
