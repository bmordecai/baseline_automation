import sys
from time import sleep

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

# Browser pages used
# import page_factory

__author__ = 'Kent'


class ControllerUseCase151(object):
    """
    Share Controller with FlowStation first, then program WS, POC, and ML through the FlowStation. This will be
    just like 150, but program WS, POC, ML through the Flow Station.

    Test name:
        - CN UseCase151 Create Mainline, Point of Control, and Water Source through the Flow Station on a Controller
        and then set parameters
    purpose:
        - This test is meant to test setting up a Flow Station with points of control, water sources, and mainlines.
        Once We have those objects shared, we want to verify the objects over the serial port of the FlowStation.
    Coverage Area: \n
        1. Get a controller into a known state by following these steps:
            a. Set up programs
            b. Set up zones on programs
            c. Create start/stop/pause conditions

        2. Share the following objects with the FlowStation:
            a. 3200 Controller

        3. Create the following objects through the Flow Station
            a. Water Source
            b. Point of Control
            c. Mainline

        4. Program objects through the FlowStation
            a. Water Source
            b. Point of Control
            c. Mainline

        5. Verify the following objects over the serial port on the FlowStation:
            a. Water Source
            b. Point of Control
            c. Mainline

        6. Stop sharing the objects between the Flow Station and the Controller
            a. Stop sharing 3200 Controller
            b. Stop sharing Mainline
            c. Stop sharing Point of Control
            d. Stop sharing Water Source
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase151' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/fs_json_config_files'
                                    )
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                     # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)
            
    #################################
    def step_1(self):
        """
        ###############################
        Share controller, water source, point of control, mainline with Flow Station
        ###############################
            - Share the controller to the flow station
            - Add and share a Water Source between the 3200 controller and the FlowStation
            - Add and share Point of Control between the 3200 controller and the FlowStation
            - Add and share Mainline between the 3200 controller and the FlowStation

        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            self.config.FlowStations[1].add_controller_to_flow_station(
                _controller_address=1,
                _flow_station_slot_number=1)

            # add Water Source on the Controller to the FlowStation
            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=1,
                _controller_water_source_address=1,
                _flow_station_water_source_slot_number=3)

            # Tell the 3200 that the FlowStation is in control of Water Source 1.
            self.config.BaseStation3200[1].get_water_source(1).set_manage_by_flowstation()

            # add Point of Control on the Controller to the FlowStation
            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=1,
                _controller_point_of_control_address=1,
                _flow_station_point_of_control_slot_number=4)
            
            # TODO Setting Master Valve to mimic default behaviour when creating a POC. Jira ZZ-1685
            self.config.BaseStation3200[1].get_point_of_control(1).add_master_valve_to_point_of_control(1)

            # Tell the 3200 that the FlowStation is in control of Point of Control 1.
            self.config.BaseStation3200[1].get_point_of_control(1).set_manage_by_flowstation()

            # add Mainline on the Controller to the FlowStation
            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=1,
                _controller_mainline_address=1,
                _flow_station_mainline_slot_number=5)

            # Tell the 3200 that the FlowStation is in control of Mainline 1.
            self.config.BaseStation3200[1].get_mainline(1).set_manage_by_flowstation()

            self.config.FlowStations[1].get_water_source(ws_number=3).add_point_of_control_to_water_source(_point_of_control_address=4)
            self.config.FlowStations[1].get_point_of_control(pc_number=4).add_mainline_to_point_of_control(_mainline_address=5)

            helper_methods.increment_controller_flowstation_clocks(self.config.FlowStations[1],
                                                                   self.config.BaseStation3200,
                                                                   minutes=1)

            self.config.FlowStations[1].save_programming_to_flow_station()
            self.config.FlowStations[1].verify_full_configuration()
            self.config.BaseStation3200[1].verify_full_configuration()
            time.sleep(5)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception(e_msg)

    def step_2(self):
        """
        ###############################
        Set attributes on Water Source, Point of Control, and Mainline through the FlowStation
        ###############################
            - Set attributes on the Water Source
            - Set attributes on the Point of Control
            - Set attributes on the Mainline

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set attributes on the Mainline through the FlowStation
            self.config.FlowStations[1].get_mainline(5).set_description("test flow station mainline")
            self.config.FlowStations[1].get_mainline(5).set_enabled()
            self.config.FlowStations[1].get_mainline(5).set_pipe_stabilization_time(_minutes=7)
            self.config.FlowStations[1].get_mainline(5).set_target_flow(_gpm=500)
            self.config.FlowStations[1].get_mainline(5).set_limit_zones_by_flow_to_true()
            self.config.FlowStations[1].get_mainline(5).set_high_flow_variance_tier_one(_percent=60,
                                                                                        _with_shutdown_enabled=True)
            self.config.FlowStations[1].get_mainline(5).set_low_flow_variance_tier_one(_percent=20,
                                                                                       _with_shutdown_enabled=False)
            self.config.FlowStations[1].get_mainline(5).set_pipe_fill_units_to_time()
            self.config.FlowStations[1].get_mainline(5).set_time_delay_before_first_zone(_minutes=2)
            self.config.FlowStations[1].get_mainline(5).set_time_delay_between_zone(_minutes=3)
            self.config.FlowStations[1].get_mainline(5).set_time_delay_after_zone(_minutes=4)
            self.config.FlowStations[1].get_mainline(5).set_number_zones_to_delay(5)
            self.config.FlowStations[1].get_mainline(5).set_standard_variance_limit_with_shutdown(_percentage=50)
            self.config.FlowStations[1].get_mainline(5).set_use_advanced_flow_to_true()

            # Set attributes on the Point of Control through the FlowStation
            self.config.FlowStations[1].get_point_of_control(4).set_description("test flow station poc")
            self.config.FlowStations[1].get_point_of_control(4).set_enabled()
            self.config.FlowStations[1].get_point_of_control(4).set_target_flow(_gpm=50)
            self.config.FlowStations[1].get_point_of_control(4).set_high_flow_limit(_limit=10, with_shutdown_enabled=True)
            self.config.FlowStations[1].get_point_of_control(4).set_unscheduled_flow_limit(_gallons=5, with_shutdown_enabled=True)
            self.config.FlowStations[1].get_point_of_control(4).add_flow_meter_to_point_of_control(_flow_meter_address=1)
            self.config.FlowStations[1].get_point_of_control(4).add_master_valve_to_point_of_control(_master_valve_address=1)
            self.config.FlowStations[1].get_point_of_control(4).add_pump_to_point_of_control(_pump_address=1)
            # TODO: Updated 5/8/18 - BEN - Commented this out b/c these assignments were made in prev step.
            # self.config.FlowStations[1].get_point_of_control(4).add_mVainline_to_point_of_control(_mainline_address=5)

            # Set attributes on the water source through the FlowStation
            self.config.FlowStations[1].get_water_source(3).set_enabled()
            self.config.FlowStations[1].get_water_source(3).set_priority(_priority_for_water_source=3)
            self.config.FlowStations[1].get_water_source(3).set_monthly_watering_budget(_budget=100, _with_shutdown_enabled=True)
            self.config.FlowStations[1].get_water_source(3).set_water_rationing_to_enabled()
            self.config.FlowStations[1].get_water_source(3).set_description("test flow station water source")
            # TODO: Updated 5/8/18 - BEN - Commented this out b/c these assignments were made in prev step.
            # self.config.FlowStations[1].get_water_source(3).add_point_of_control_to_water_source(_point_of_control_address=4)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg)

    def step_3(self):
        """
        ###############################
        verify the entire configuration through the Flow Station Serial Port and 3200 Serial Port\n
        ###############################
            - Get information for each object from the flow station
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            helper_methods.increment_controller_flowstation_clocks(self.config.FlowStations[1],
                                                                   self.config.BaseStation3200,
                                                                   minutes=1)

            self.config.FlowStations[1].save_programming_to_flow_station()
            self.config.FlowStations[1].verify_full_configuration()
            self.config.BaseStation3200[1].verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_4(self):
        """
        ###############################
        Stop Sharing objects with the Flow Station
        ###############################
            - Stop sharing the Point of Control
            - Stop Sharing the Mainline
            - Stop sharing the Water Source
            - Stop sharing the 3200 Controller
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Remove the Point of Control from the Flow Station
            self.config.FlowStations[1].remove_controller_point_of_control_from_flowstation(
                _controller_address=1,
                _controller_point_of_control_address=1,
                _flowstation_point_of_control_slot_number=4)

            # Tell the 3200 that the FlowStation is no longer in control of Point of Control 1.
            self.config.BaseStation3200[1].get_point_of_control(1).set_do_not_manage_by_flowstation()

            #  Remove the Mainline from the Flow Station
            self.config.FlowStations[1].remove_controller_mainline_from_flowstation(
                _controller_address=1,
                _controller_mainline_address=1,
                _flowstation_mainline_slot_number=5)

            # Tell the 3200 that the FlowStation is no longer in control of Mainline 1.
            self.config.BaseStation3200[1].get_mainline(1).set_do_not_manage_by_flowstation()

            # Remove the Water Source from the Flow Station
            self.config.FlowStations[1].remove_controller_water_source_from_flowstation(
                _controller_address=1,
                _controller_water_source_address=1,
                _flow_station_water_source_slot_number=3)

            # Tell the 3200 that the FlowStation is no longer in control of Water Source 1.
            self.config.BaseStation3200[1].get_water_source(1).set_do_not_manage_by_flowstation()

            # Remove the Controller from the Flow Station
            self.config.FlowStations[1].remove_controller_from_flowstation(_flowstation_slot_number=1)

            helper_methods.increment_controller_flowstation_clocks(self.config.FlowStations[1],
                                                                   self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.FlowStations[1].save_programming_to_flow_station()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
