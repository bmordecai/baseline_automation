import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

# Browser pages used
# import page_factory

__author__ = 'Tige'

class ControllerUseCase152_4(object):
    """
    Test name:
        - CN UseCase152_4 Test Learn Flow
    purpose:
        - This test is meant to Test Learn Flow on FlowStation using two mainlines of 3200 with one mainline assigned
          to the other mainline. Starting the learn flow by program and by zone.
    User Story:
        1)  As a user, I would like to learn flow on two mainlines when both have common flow and are managed by a
            FlowStation.
    Coverage Area: \n
        1. Configure two programs one zone each
        2. Setup zones
            - Add a timed zone on each program
        3. Setup Water Sources
            - Add a water source to controller
        4. Setup Points of Control
            - Add a POC on controller 1 with a Master Valve and Flow Meter
        5. Setup Mainlines
            - Add two Mainlines on controller
        6. Share Controller and Flow Objects with the FlowStation
            - Share controller with the FlowStation
                - Share a Water Source on controller with the FlowStation
                - Share a Point of Control on controller with the FlowStation
                - Share two Mainlines on controller with the FlowStation
        7. Setup zones
            - add zone 1 to a mainline 1 on controller
            - add zone 2 to a mainline 2 on controller
        8. Set design flow on all added zones on controller 1 and 2
            - zone 1 and 2 on controller have a design flow of 10 gpm

    Use case explanation: \n

            FlowStation:
                - WS/PC/ML Assignments:

                C1:W1
                  |
                C1:P1
                  |
                C1:M1
                  |
                C1:M2

        Scenario 1:
            - Set the flow rate on both flow meters to 50 GPM
            - Start Learn Flow Program 1 and 2 on controller
                - Verify that Zone 1 is learning flow
                - Verify that Zone 2 is waiting to water
                - Verify that Programs 1 and 2 are learning flow
            - Zone 1 finishes learn flow
                - Verify that Zone 1 is done
                - Verify that Zone 2 is learning flow
                - Verify that zone 1 generated a "learn flow success" message
                - Verify that Programs 1 is done
                - Verify that Programs 2 is learning flow
            - Zone 2 finishes learn flow
                - Verify that Zone 1 and 2 are done
                - Verify that zone 2 generated a "learn flow success" message
                - Verify that Programs 1 and 2 are done
            - Clear the "learn flow success" messages

        Scenario 2:
            - Set the flow rate on both flow meters to 40 GPM
            - Start Learn Flow Zone 1 and 2 on controller
                - Verify that Zone 1 is learning flow
                - Verify that Zone 2 is waiting to water
            - Zone 1 finishes learn flow
                - Verify that Zone 1 is done
                - Verify that Zone 2 is learning flow
                - Verify that zone 1 generated a "learn flow success" message
            - Zone 2 finishes learn flow
                - Verify that Zone 1 and 2 are done
                - Verify that zone 2 generated a "learn flow success" message
            - Clear the "learn flow success" messages

        - Verify that everything is in a done state and all of the attributes on the controllers

    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase151' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/fs_json_config_files'
                                    )
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:

                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]

                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))

                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        program_start_times = [600]  # 10:00am start time
        program_watering_days = [1, 1, 1, 1, 1, 1, 1]  # runs all days
        program_water_windows = ['111111111111111111111111']
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add and configure Program 1 to controller 1
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_water_window(_ww=program_water_windows)
            self.config.BaseStation3200[1].programs[1].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[1].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(_sun=True,
                                                                                                           _mon=True,
                                                                                                           _tues=True,
                                                                                                           _wed=True,
                                                                                                           _thurs=True,
                                                                                                           _fri=True,
                                                                                                           _sat=True)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_start_times)

            # Add and configure Program 2 to controller 1
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation3200[1].programs[2].set_enabled()
            self.config.BaseStation3200[1].programs[2].set_water_window(_ww=program_water_windows)
            self.config.BaseStation3200[1].programs[2].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[2].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[2].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[2].set_watering_intervals_to_selected_days_of_the_week(_sun=True,
                                                                                                           _mon=True,
                                                                                                           _tues=True,
                                                                                                           _wed=True,
                                                                                                           _thurs=True,
                                                                                                           _fri=True,
                                                                                                           _sat=True)
            self.config.BaseStation3200[1].programs[2].set_start_times(_st_list=program_start_times)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # add zones to programs on controller 1
            # Add & Configure Program Zone 1
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=10)

            # Add & Configure Program Zone 2
            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[2].zone_programs[2].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[2].zone_programs[2].set_run_time(_minutes=10)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

            #################################

    def step_3(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n

        set up WS 1 \n
            - enabled
            - target flow: 500 \n
            - priority: 2 \n
            - empty conditions:
                - none

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Water Source 1 on controller 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].get_water_source(1).set_enabled()
            self.config.BaseStation3200[1].get_water_source(1).set_priority(_priority_for_water_source=2)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

            #################################

    def step_4(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n
        set up POC 1 \n
            enable POC 1 \n
            assign master valve TMV0003 and flow meter TWF0003 to POC 1 \n
            assign POC 1 a target flow of 50
            assign POC 1 to main line 1 \n
            set POC priority to 2-medium \n
            set high flow limit to 550 and enable high flow shut down \n
            set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            set water budget to 100000 and enable the water budget shut down \n
            enable water rationing \n

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure POC 1 on controller 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].get_point_of_control(1).set_enabled()
            self.config.BaseStation3200[1].get_point_of_control(1).add_master_valve_to_point_of_control(
                _master_valve_address=1)
            self.config.BaseStation3200[1].get_point_of_control(1).add_flow_meter_to_point_of_control(
                _flow_meter_address=1)
            self.config.BaseStation3200[1].get_point_of_control(1).set_target_flow(_gpm=50)
            # Add POC 1 to Water Source 1
            self.config.BaseStation3200[1].get_water_source(1).add_point_of_control_to_water_source(
                _point_of_control_address=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

            #################################

    def step_5(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state
                - set the low variance limit with shut down state
            - Add Mainline ---> to point of control

        - set up main line 1
            - set limit zones by flow to true
            - set the pipe fill time to 1 minutes
            - set the target flow to 500

        - set up main line 2
            - set limit zones by flow to true
            - set the pipe fill time to 1 minutes
            - set the target flow to 500
        """

        # here we can either execute the following uncommented lines in procedural fashion, or we could re-init the
        # object, would have to import Mainline at the top, effectively accomplishing the same thing by:
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure ML 1 on controller 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].get_mainline(1).set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].get_mainline(1).set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].get_mainline(1).set_limit_zones_by_flow_to_true()
            # Add ML 1 to POC 1
            self.config.BaseStation3200[1].get_point_of_control(1).add_mainline_to_point_of_control(_mainline_address=1)

            # Add & Configure ML 2 on controller 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=2)
            self.config.BaseStation3200[1].get_mainline(2).set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].get_mainline(2).set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].get_mainline(2).set_limit_zones_by_flow_to_true()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
    # #################################
    def step_6(self):
        """
        ###############################
        verify the entire configuration on the 3200 \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            self.config.BaseStation3200[1].verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_7(self):
        """
        ###############################
        Share controller, water source, point of control, mainline with Flow Station
        ###############################
            - Share the controller to the flow station
            - Add and share a Water Source between the 3200 controller and the FlowStation
            - Add and share Point of Control between the 3200 controller and the FlowStation
            - Add and share Mainline between the 3200 controller and the FlowStation

        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add controller 1 to FlowStation
            self.config.FlowStations[1].add_controller_to_flow_station(
                _controller_address=1,
                _flow_station_slot_number=1)

            # Add water source
            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=1,
                _controller_water_source_address=1,
                _flow_station_water_source_slot_number=1)

            # Tell the 3200 that the FlowStation is in control of Water Source 1.
            self.config.BaseStation3200[1].get_water_source(1).set_manage_by_flowstation()

            # Add POC
            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=1,
                _controller_point_of_control_address=1,
                _flow_station_point_of_control_slot_number=1)

            self.config.BaseStation3200[1].get_point_of_control(1).set_manage_by_flowstation()

            # Add mainlines
            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=1,
                _controller_mainline_address=1,
                _flow_station_mainline_slot_number=1)

            self.config.BaseStation3200[1].get_mainline(1).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=1,
                _controller_mainline_address=2,
                _flow_station_mainline_slot_number=2)

            self.config.BaseStation3200[1].get_mainline(2).set_manage_by_flowstation()

            # Set assignments
            self.config.FlowStations[1].get_water_source(1).add_point_of_control_to_water_source(_point_of_control_address=1)
            self.config.FlowStations[1].get_point_of_control(1).add_mainline_to_point_of_control(_mainline_address=1)
            self.config.FlowStations[1].get_mainline(1).add_mainline_to_mainline(_mainline_address=2)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_8(self):
        """
        ###############################
        verify the entire configuration through the Flow Station Serial Port \n
        ###############################
            - Get information for each object from the flow station
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.FlowStations[1].save_programming_to_flow_station()
            self.config.FlowStations[1].verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

            #################################

    def step_9(self):
        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone
        Program | Mainline  | Zones
        ---------------------------------------------
        1       | 1         | 1
        2       | 2         | 2
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Add ZN 1 to ML 1 and ZN to ML 2 on controller
            self.config.BaseStation3200[1].get_mainline(1).add_zone_to_mainline(_zone_address=1)
            self.config.BaseStation3200[1].get_mainline(2).add_zone_to_mainline(_zone_address=2)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

            ###############################

    def step_10(self):
        """
        Give each zone a design flow
            - zones 1 - 10 gpm
            - zones 2 - 10 gpm
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # set design flow of zones  on controller 1
            # Assign a design flow value to each zone so that they have a default setting
            self.config.BaseStation3200[1].zones[1].set_design_flow(_gallons_per_minute=10)
            self.config.BaseStation3200[1].zones[2].set_design_flow(_gallons_per_minute=10)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

            ###############################

    def step_11(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            self.config.FlowStations[1].save_programming_to_flow_station()
            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.FlowStations[1].verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

            ###############################

    def step_12(self):
        """
        ###############################
        Scenario 1 = program learn flow
        ###############################
        - set flow meter values
            - set flow meter 1 to 50 gpm

        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=50.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

            ###############################

    def step_13(self):
        """
        ###############################
        Scenario 1 = program learn flow continued
        ###############################
        learn flow success programs/zones 1 and 2
        - We expect the zone to learn flow for 2 minutes plus an added minute for communicating with the FlowStation.
            - The two minutes comes from pipe fill time + 1 minute.
        - Between zones learning flow there is a minute where the next zone is waiting for the 3200 and FlowStation to
          finish communicating.
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            date_mngr.set_current_date_to_match_computer()
            self.config.FlowStations[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                          _time='07:59:00')
            self.config.BaseStation3200[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                             _time='07:59:00')

            # Start learn flow program 1 and 2
            self.config.BaseStation3200[1].programs[1].set_learn_flow_to_start()
            self.config.BaseStation3200[1].programs[2].set_learn_flow_to_start()

            # Increment time to 8:00
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify zone status
            self.config.BaseStation3200[1].zones[1].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[2].verify_status_is_waiting_to_water()

            # Verify program status
            self.config.BaseStation3200[1].programs[1].verify_status_is_learning_flow()
            self.config.BaseStation3200[1].programs[2].verify_status_is_learning_flow()

            # Increment time to 8:01
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify zone status
            self.config.BaseStation3200[1].zones[1].verify_status_is_learning_flow()
            self.config.BaseStation3200[1].zones[2].verify_status_is_waiting_to_water()

            # Verify program status
            self.config.BaseStation3200[1].programs[1].verify_status_is_learning_flow()
            self.config.BaseStation3200[1].programs[2].verify_status_is_learning_flow()

            # Increment time to 8:04
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=3)

            # Verify zone status
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_waiting_to_water()

            # Verify program status
            self.config.BaseStation3200[1].programs[1].verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].verify_status_is_learning_flow()

            # Increment time to 8:05
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify zone status
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_learning_flow()

            # Verify program status
            self.config.BaseStation3200[1].programs[1].verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].verify_status_is_learning_flow()

            # Increment time to 8:08
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=3)

            # Verify zone status
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()

            # Verify program status
            self.config.BaseStation3200[1].programs[1].verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].verify_status_is_done()

            # Verify messages for first zone
            self.config.BaseStation3200[1].zones[1].update_design_flow_value_after_learn_flow(
                _flow_meter_address=1)
            self.config.BaseStation3200[1].zones[1].messages.verify_learn_flow_complete_success_message()
            self.config.BaseStation3200[1].programs[1].messages.verify_learn_flow_complete_success_message()

            # Clear messages on mainline 1
            self.config.BaseStation3200[1].zones[1].messages.clear_learn_flow_complete_success_message()
            self.config.BaseStation3200[1].programs[1].messages.clear_learn_flow_complete_success_message()

            # Verify messages for second zone
            self.config.BaseStation3200[1].zones[2].update_design_flow_value_after_learn_flow(
                _flow_meter_address=1)
            self.config.BaseStation3200[1].zones[2].messages.verify_learn_flow_complete_success_message()
            self.config.BaseStation3200[1].programs[2].messages.verify_learn_flow_complete_success_message()

            # Clear messages on mainline 2
            self.config.BaseStation3200[1].zones[2].messages.clear_learn_flow_complete_success_message()
            self.config.BaseStation3200[1].programs[2].messages.clear_learn_flow_complete_success_message()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

            ###############################

    def step_14(self):
        """
        ###############################
        Scenario 2 = zone learn flow
        ###############################
        - set flow meter values
            - set flow meter 1 to 40 gpm

        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=40.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

            ###############################

    def step_15(self):
        """
        ###############################
        Scenario 2 = zone learn flow continued
        ###############################
        learn flow success zones 1 and 2
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # Start learn flow zone 1 and 2
            self.config.BaseStation3200[1].zones[1].set_learn_flow_to_start()
            self.config.BaseStation3200[1].zones[2].set_learn_flow_to_start()

            # Increment time
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify zone status
            self.config.BaseStation3200[1].zones[1].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[2].verify_status_is_waiting_to_water()

            # Verify program status
            self.config.BaseStation3200[1].programs[1].verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].verify_status_is_done()

            # Increment time
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify zone status
            self.config.BaseStation3200[1].zones[1].verify_status_is_learning_flow()
            self.config.BaseStation3200[1].zones[2].verify_status_is_waiting_to_water()

            # Verify program status
            self.config.BaseStation3200[1].programs[1].verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].verify_status_is_done()

            # Increment time
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=3)

            # Verify zone status
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_waiting_to_water()

            # Verify program status
            self.config.BaseStation3200[1].programs[1].verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].verify_status_is_done()

            # Increment time
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify zone status
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_learning_flow()

            # Verify program status
            self.config.BaseStation3200[1].programs[1].verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].verify_status_is_done()

            # Increment time
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=3)

            # Verify zone status
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()

            # Verify program status
            self.config.BaseStation3200[1].programs[1].verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].verify_status_is_done()

            # Verify messages for both zone
            self.config.BaseStation3200[1].zones[1].update_design_flow_value_after_learn_flow(
                _flow_meter_address=1)
            self.config.BaseStation3200[1].zones[2].update_design_flow_value_after_learn_flow(
                _flow_meter_address=1)

            self.config.BaseStation3200[1].zones[1].messages.verify_learn_flow_complete_success_message()
            self.config.BaseStation3200[1].zones[2].messages.verify_learn_flow_complete_success_message()

            # clear messages
            self.config.BaseStation3200[1].zones[1].messages.clear_learn_flow_complete_success_message()
            self.config.BaseStation3200[1].zones[2].messages.clear_learn_flow_complete_success_message()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_16(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.FlowStations[1].save_programming_to_flow_station()
            helper_methods.increment_controller_flowstation_clocks(self.config.FlowStations[1], self.config.BaseStation3200, minutes=1)
            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.FlowStations[1].verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
