import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

# Browser pages used
# import page_factory

__author__ = 'Tige/Kent'


class ControllerUseCase153(object):
    """
    Test name:
        - CN UseCase153 Mainline-Zone Delays with flowstation (Timed/Pressure)

    User Story: \n
        1)  The user wants to be able to control valve on/off delay times based on either time or pressure. \n
            This is to help valves turn on and off as flow rates and pressure changes with these changes through \n
            the pipes.
        2)  This is needed when controllers are attached to a FlowStation doing water management and allocation. \n

    Coverage are of feature: \n
        This test cover both time and pressure delays on controller when connected to a FlowStation. \n

    Use case explaination: \n
        - Set up a timed-delay program on 3200 #1, and a pressure-delay program on 3200 #2 \n
        - Place both 3200's under flowstation management, along with the WS / POC / ML feeding the zones for the \n
          programs on the two 3200s \n
        - Cross-connect the 3200 WS / POC / ML on the FlowStation as shown in the Test Cofiguration setup shown below \n

        Set up a full configuration on the controller
            - Check for correct watering behavior using timed and pressure delays on a mainline.
                - Verify messages (if any)
                - Verify expected statuses

        Test Configuration setup: \n
            1. Zone On Delay: 2 minutes
            2. Between Zone Delay: 1 minute
            3. Zone off Delay: 2 minutes
            4. Number of Zones to turn on at once: 2
            5. Controller concurrent zones: 15
            6. Configuration:

                3200 #1 FlowStation Assignments [time delay]
                    W1 = assigned to FS W1
                    P1 = assigned to FS P1 [will provide water to C2:M2 through FS]
                        - MV, PM, FM, + PR
                    M1 = assigned to FS M1 [gets water from C2:P2]
                        - Zones 1 to 5
                    PG1 = 8 am start, zones 1-5 8/4/2 minutes, 5 concurrent zones

                3200 #2 FlowStation Assignments [pressure delay]
                    W2 = assigned to FS W8
                    P2 = assigned to FS P8 [provides water to C1:Ml througj FS]
                        - MV, PM, FM, (no PR)
                    M2 = assigned to FS M8 [gets water from C1:P1]
                        - Zones 6 to 10
                    PG1 = 8 am start, zones 6-10 8/4/2 minutes, 5 concurrent zones

                FlowStation Program Setup
                    W1 = C1:W1
                    W8 = C2:W2
                    P1 = C1:P1
                    P8 = C2:P2
                    M1 = C1:M1
                    M8 = C2:M2

                FlowStation Water Paths
                    FS:W1[C1:W1] --> FS:P8[C2:P2] --> FS:M1[C1:M1] = time delays
                    FS:W8[C2:W2] --> FS:P1[C1:P1] --> FS:M8[C2:M2] = pressure delays

                - C1:M1 FS:M1 (time delays)
                    - Design Flow: 100 GPM
                    - Zone On Delay: 2 minutes
                    - Between Zone Delay: 2 minute
                    - Zone off Delay: 2 minutes
                    - Number of Zones to turn on at once: 2

                - C2:M2 FS:M8 (Pressure Delays)
                    - Design Flow: 400 GPM
                    - Zone On Delay: 75 PSI
                    - Between Zone Delay: 100 PSI
                    - Zone off Delay: 125 PSI
                    - Number of Zones to turn on at once: 3

    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase154' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/fs_json_config_files'
                                    )
        self.run_use_case()

    #################################
    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=False)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                     # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break

                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        ############################
        setup controller concurrency.
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=15)
            self.config.BaseStation3200[2].set_max_concurrent_zones(_max_zones=15)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=5)

            self.config.BaseStation3200[2].add_program_to_controller(_program_address=2)
            self.config.BaseStation3200[2].programs[2].set_enabled()
            self.config.BaseStation3200[2].programs[2].set_max_concurrent_zones(_number_of_zones=5)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add zone programs to Program 1
            for zone_ad in range(1, 6):
                self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=zone_ad)
                self.config.BaseStation3200[1].programs[1].zone_programs[zone_ad].set_run_time(_minutes=8)
                self.config.BaseStation3200[1].programs[1].zone_programs[zone_ad].set_cycle_time(_minutes=4)
                self.config.BaseStation3200[1].programs[1].zone_programs[zone_ad].set_soak_time(_minutes=2)

            # Add zone programs to Program 2
            for zone_ad in range(6, 11):
                self.config.BaseStation3200[2].programs[2].add_zone_to_program(_zone_address=zone_ad)
                self.config.BaseStation3200[2].programs[2].zone_programs[zone_ad].set_run_time(_minutes=8)
                self.config.BaseStation3200[2].programs[2].zone_programs[zone_ad].set_cycle_time(_minutes=4)
                self.config.BaseStation3200[2].programs[2].zone_programs[zone_ad].set_soak_time(_minutes=2)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        set_water Source \n
            - set up WaterSource 1 \n
                - enable POC 1 \n
                - assign master valve TMV0003 and flow meter TWF0003 to POC 1 \n
                - assign POC 1 a target flow of 500 \n
                - assign POC 1 to main line 1 \n
                - set POC priority to 2-medium \n
                - set high flow limit to 550 and enable high flow shut down \n
                - set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
                - set water budget to 100000 and enable the water budget shut down \n
                - enable water rationing \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Water Source 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].get_water_source(1).set_enabled()
            self.config.BaseStation3200[1].get_water_source(1).set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].get_water_source(1).set_monthly_watering_budget(_budget=10000,
                                                                                           _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].get_water_source(1).set_water_rationing_to_enabled()

            # Water Source 2
            self.config.BaseStation3200[2].add_water_source_to_controller(_water_source_address=2)
            self.config.BaseStation3200[2].get_water_source(2).set_enabled()
            self.config.BaseStation3200[2].get_water_source(2).set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[2].get_water_source(2).set_monthly_watering_budget(_budget=10000,
                                                                                           _with_shutdown_enabled=True)
            self.config.BaseStation3200[2].get_water_source(2).set_water_rationing_to_enabled()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n
        set_poc_3200 \n
            - set up POC 1 \n
                - enable POC 1 \n
                - assign master valve TMV0003 and flow meter TWF0003 to POC 1 \n
                - assign POC 1 a target flow of 500 \n
                - assign POC 1 to main line 1 \n
                - set POC priority to 2-medium \n
                - set high flow limit to 550 and enable high flow shut down \n
                - set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
                - set water budget to 100000 and enable the water budget shut down \n
                - enable water rationing \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].get_water_source(1).add_point_of_control_to_water_source(
                _point_of_control_address=1)
            self.config.BaseStation3200[1].get_point_of_control(1).set_enabled()
            self.config.BaseStation3200[1].get_point_of_control(1).set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].get_point_of_control(1).add_master_valve_to_point_of_control(
                _master_valve_address=1)
            self.config.BaseStation3200[1].get_point_of_control(1).add_pump_to_point_of_control(
                _pump_address=1)
            self.config.BaseStation3200[1].get_point_of_control(1).add_flow_meter_to_point_of_control(
                _flow_meter_address=1)
            self.config.BaseStation3200[1].get_point_of_control(1).add_pressure_sensor_to_point_of_control(
                _pressure_sensor_address=1)

            # POC 2
            self.config.BaseStation3200[2].add_point_of_control_to_controller(_point_of_control_address=2)
            self.config.BaseStation3200[2].get_water_source(2).add_point_of_control_to_water_source(
                _point_of_control_address=2)
            self.config.BaseStation3200[2].get_point_of_control(2).set_enabled()
            self.config.BaseStation3200[2].get_point_of_control(2).set_target_flow(_gpm=500)
            self.config.BaseStation3200[2].get_point_of_control(2).add_master_valve_to_point_of_control(
                _master_valve_address=2)
            self.config.BaseStation3200[2].get_point_of_control(2).add_pump_to_point_of_control(
                _pump_address=2)
            self.config.BaseStation3200[2].get_point_of_control(2).add_flow_meter_to_point_of_control(
                _flow_meter_address=2)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control
        set_mainlines_3200 \n
            - set up main line 1 \n
                - set limit zones by flow to true \n
                - set the pipe fill time to 4 minutes \n
                - set the target flow to 500 \n
                - set the high variance limit to 5% and enable the high variance shut down \n
                - set the low variance limit to 20% and enable the low variance shut down \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Mainline 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].get_point_of_control(1).add_mainline_to_point_of_control(_mainline_address=1)
            self.config.BaseStation3200[1].get_mainline(1).set_enabled()
            self.config.BaseStation3200[1].get_mainline(1).set_target_flow(_gpm=100)
            self.config.BaseStation3200[1].get_mainline(1).set_limit_zones_by_flow_to_true()
            # Mainline 1 Time Delays
            self.config.BaseStation3200[1].get_mainline(1).set_time_delay_before_first_zone(_minutes=2)
            self.config.BaseStation3200[1].get_mainline(1).set_time_delay_between_zone(_minutes=2)
            self.config.BaseStation3200[1].get_mainline(1).set_time_delay_after_zone(_minutes=2)
            self.config.BaseStation3200[1].get_mainline(1).set_number_zones_to_delay(_new_number=2)

            # Mainline 2
            self.config.BaseStation3200[2].add_mainline_to_controller(_mainline_address=2)
            self.config.BaseStation3200[2].get_point_of_control(2).add_mainline_to_point_of_control(_mainline_address=2)
            self.config.BaseStation3200[2].get_mainline(2).set_enabled()
            self.config.BaseStation3200[2].get_mainline(2).set_target_flow(_gpm=400)
            self.config.BaseStation3200[2].get_mainline(2).set_limit_zones_by_flow_to_true()
            # Mainline 2 Pressure Delays
            self.config.BaseStation3200[2].get_mainline(2).set_pressure_delay_before_first_zone(_psi=50)
            self.config.BaseStation3200[2].get_mainline(2).set_pressure_delay_between_zone(_psi=75)
            self.config.BaseStation3200[2].get_mainline(2).set_pressure_delay_after_zone(_psi=100)
            self.config.BaseStation3200[2].get_mainline(2).set_number_zones_to_delay(_new_number=2)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add Zones 1-5 to Mainline 1 .  Set design flow on each zone to a tiny value (5 gpm) so we don't run
            # into flowstation limits
            for zone_address in range(1, 6):
                self.config.BaseStation3200[1].get_mainline(1).add_zone_to_mainline(_zone_address=zone_address)
                self.config.BaseStation3200[1].zones[zone_address].set_design_flow(_gallons_per_minute=5)
            
            # Add Zones 6-10 to Mainline 2 .  Set design flow on each zone to a tiny value (5 gpm) so we don't run
            # into flowstation limits
            for zone_address in range(6, 11):
                self.config.BaseStation3200[2].get_mainline(2).add_zone_to_mainline(_zone_address=zone_address)
                self.config.BaseStation3200[2].zones[zone_address].set_design_flow(_gallons_per_minute=5)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7_1(self):
        """
        ###############################
        Share controller, water source, point of control, mainline with Flow Station
        ###############################
            - Share controller1 with the flow station
            - Add and share a Water Source between controller1 and the FlowStation
            - Add and share Point of Control between controller1 and the FlowStation
            - Add and share Mainline between controller1 and the FlowStation

            - Share controller2 with the flow station
            - Add and share a Water Source between controller2 and the FlowStation
            - Add and share Point of Control between controller2 and the FlowStation
            - Add and share Mainline between controller2 and the FlowStation

            - Assign WS, POC, and ML on the FlowStation
            - WS1 -> PC8 -> ML1
            - WS8 -> PC1 -> ML8
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add controller 1 to FlowStation
            self.config.FlowStations[1].add_controller_to_flow_station(
                _controller_address=1,
                _flow_station_slot_number=1)

            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=1,
                _controller_water_source_address=1,
                _flow_station_water_source_slot_number=1)

            # Tell the 3200 that the FlowStation is in control of Water Source 1.
            self.config.BaseStation3200[1].get_water_source(1).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=1,
                _controller_point_of_control_address=1,
                _flow_station_point_of_control_slot_number=1)

            # Tell the 3200 that the FlowStation is in control of Point of Control 1.
            self.config.BaseStation3200[1].get_point_of_control(1).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=1,
                _controller_mainline_address=1,
                _flow_station_mainline_slot_number=1)

            # Tell the 3200 that the FlowStation is in control of Mainline 1.
            self.config.BaseStation3200[1].get_mainline(1).set_manage_by_flowstation()

            # Add controller 2 to FlowStation
            self.config.FlowStations[1].add_controller_to_flow_station(
                _controller_address=2,
                _flow_station_slot_number=2)

            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=2,
                _controller_water_source_address=2,
                _flow_station_water_source_slot_number=8)

            # Tell the 3200 that the FlowStation is in control of Water Source 2.
            self.config.BaseStation3200[2].get_water_source(2).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=2,
                _controller_point_of_control_address=2,
                _flow_station_point_of_control_slot_number=8)

            # Tell the 3200 to have the FlowStation manage Point of Control 2.
            self.config.BaseStation3200[2].get_point_of_control(2).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=2,
                _controller_mainline_address=2,
                _flow_station_mainline_slot_number=8)

            # Tell the 3200 to have the FlowStation manage Mainline 2.
            self.config.BaseStation3200[2].get_mainline(2).set_manage_by_flowstation()

            # Assign WS, POC, and ML on FlowStation
            # WS1 -> PC8 -> ML1
            self.config.FlowStations[1].get_point_of_control(pc_number=8).add_mainline_to_point_of_control(_mainline_address=1)
            self.config.FlowStations[1].get_water_source(ws_number=1).add_point_of_control_to_water_source(_point_of_control_address=8)

            # WS8 -> PC1 -> ML8
            self.config.FlowStations[1].get_point_of_control(pc_number=1).add_mainline_to_point_of_control(_mainline_address=8)
            self.config.FlowStations[1].get_water_source(ws_number=8).add_point_of_control_to_water_source(_point_of_control_address=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.BaseStation3200[2].verify_full_configuration()
            self.config.FlowStations[1].save_programming_to_flow_station()
            self.config.FlowStations[1].verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        ###############################
        Program 1 - Verify initial statuses - 7:59am
        ###############################

        Timed Delays Table:

            Time  |WS 1,2 |PC 1,2 |MV 1,2 |PM 1,2 |ML 1,2 |ZN 1,6 |ZN 2,7 |ZN 3,8 |ZN 4,9 |ZN 5,10 |PG 1,2 |
            ------------------------------------------------------------------------------------
            7:59a |  OK   |  OF   |  OF   |  OF   |  OF   |  DN   |  DN   |  DN   |  DN   |  DN    |  DN   |
            8:00a |  OK   |  OF   |  OF   |  OF   |  OF   |  DN   |  DN   |  DN   |  DN   |  DN    |  DN   |
            8:01a |  OK   |  OF   |  OF   |  OF   |  OF   |  DN   |  DN   |  DN   |  DN   |  DN    |  DN   |
            8:02a |  RN   |  RN   |  WT   |  WT   |  RN   |  WA   |  WA   |  WA   |  WA   |  WA    |  WA   |
            8:03a |  RN   |  RN   |  WT   |  WT   |  RN   |  WA   |  WA   |  WA   |  WA   |  WA    |  WA   |
            8:04a |  RN   |  RN   |  WT   |  WT   |  RN   |  WT   |  WT   |  WA   |  WA   |  WA    |  RN   |
            8:05a |  RN   |  RN   |  WT   |  WT   |  RN   |  WT   |  WT   |  WA   |  WA   |  WA    |  RN   |
            8:06a |  RN   |  RN   |  WT   |  WT   |  RN   |  WT   |  WT   |  WT   |  WT   |  WA    |  RN   |
            8:07a |  RN   |  RN   |  WT   |  WT   |  RN   |  WT   |  WT   |  WT   |  WT   |  WA    |  RN   |
            8:08a |  RN   |  RN   |  WT   |  WT   |  RN   |  SO   |  SO   |  WT   |  WT   |  WT    |  RN   |
            8:09a |  RN   |  RN   |  WT   |  WT   |  RN   |  SO   |  SO   |  WT   |  WT   |  WT    |  RN   |
            8:10a |  RN   |  RN   |  WT   |  WT   |  RN   |  WT   |  WT   |  SO   |  SO   |  WT    |  RN   |
            8:11a |  RN   |  RN   |  WT   |  WT   |  RN   |  WT   |  WT   |  SO   |  SO   |  WT    |  RN   |
            8:12a |  RN   |  RN   |  WT   |  WT   |  RN   |  WT   |  WT   |  WT   |  SO   |  SO    |  RN   |
            8:13a |  RN   |  RN   |  WT   |  WT   |  RN   |  WT   |  WT   |  WT   |  SO   |  SO    |  RN   |
            8:14a |  RN   |  RN   |  WT   |  WT   |  RN   |  DN   |  DN   |  WT   |  WT   |  WT    |  RN   |
            8:15a |  RN   |  RN   |  WT   |  WT   |  RN   |  DN   |  DN   |  WT   |  WT   |  WT    |  RN   |
            8:16a |  RN   |  RN   |  WT   |  WT   |  RN   |  DN   |  DN   |  DN   |  WT   |  WT    |  RN   |
            8:17a |  RN   |  RN   |  WT   |  WT   |  RN   |  DN   |  DN   |  DN   |  WT   |  WT    |  RN   |
            8:18a |  RN   |  RN   |  WT   |  WT   |  RN   |  DN   |  DN   |  DN   |  DN   |  DN    |  DN   |
            8:19a |  RN   |  RN   |  WT   |  WT   |  RN   |  DN   |  DN   |  DN   |  DN   |  DN    |  DN   |
            8:20a |  OK   |  OF   |  OF   |  OF   |  OF   |  DN   |  DN   |  DN   |  DN   |  DN    |  DN   |


        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set time to 7:59 am
            self.config.BaseStation3200[1].set_date_and_time(_date='12/18/2017', _time='7:59:00')
            self.config.BaseStation3200[2].set_date_and_time(_date='12/18/2017', _time='7:59:00')
            self.config.FlowStations[1].set_date_and_time(_date='12/18/2017', _time='07:59:00')

            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.set_pressure_value_in_psi(_psi=25)
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.self_test_and_update_object_attributes()

            # Increment clock by 1 minute to change to 8:00 am
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # verify water source
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_ok()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_ok()

            # verify points of control
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_off()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_off()

            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[2].pumps[2].statuses.verify_status_is_off()

            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[2].master_valves[2].statuses.verify_status_is_off()

            # verify Mainlines
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_off()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_off()

            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].zones[6].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].zones[7].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].zones[8].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].zones[9].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].zones[10].statuses.verify_status_is_done()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        ###############################
        Simulate Program 1 - 8:01 am start time hit, then an extra minute for FlowStation response
        ###############################

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment clock by 1 minute to change to 8:01am
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.set_pressure_value_in_psi(_psi=25)
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.self_test_and_update_object_attributes()

            #   8:01 after increment (water request and allocations made - not acted on yet)
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            #   8:02 POC on, zones waiting for delays
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # verify water source
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_water_source(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()

            # verify points of control
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].pumps[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].master_valves[2].statuses.verify_status_is_watering()

            # verify Mainlines
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_mainline(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_running()

            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[6].statuses.verify_status_is_waiting_to_water()
			
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[7].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[8].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[9].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[10].statuses.verify_status_is_waiting_to_water()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_waiting_to_run()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        ###############################
        Simulate Program 1 - 8:03am
        ###############################

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment clock by 1 minute to change to 8:02am
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.set_pressure_value_in_psi(_psi=51)	# minute early for FlowStn delay
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.self_test_and_update_object_attributes()

            #   8:03 after increment
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # verify water source
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()

            # verify points of control
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].pumps[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].master_valves[2].statuses.verify_status_is_watering()

            # verify Mainlines
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_running()

            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[6].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[7].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[8].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[9].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[10].statuses.verify_status_is_waiting_to_water()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_waiting_to_run()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        ###############################
        Simulate Program 1 - 8:04am
        ###############################

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment clock by 1 minute to change to 8:04am
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.set_pressure_value_in_psi(_psi=51)
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.self_test_and_update_object_attributes()

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # verify water source
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()

            # verify points of control
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].pumps[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].master_valves[2].statuses.verify_status_is_watering()

            # verify Mainlines
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_running()

            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[6].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[7].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[8].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[9].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[10].statuses.verify_status_is_waiting_to_water()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_running()


        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        ###############################
        Simulate Program 1 - 8:05am
        ###############################

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment clock by 1 minute to change to 8:05am
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.set_pressure_value_in_psi(_psi=76)	# minute early for FlowStn delay
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.self_test_and_update_object_attributes()

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # verify water source
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()

            # verify points of control
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].pumps[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].master_valves[2].statuses.verify_status_is_watering()

            # verify Mainlines
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_running()

            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[6].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[7].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[8].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[9].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[10].statuses.verify_status_is_waiting_to_water()


            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_running()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_14(self):
        """
        ###############################
        Simulate Program 1 - 8:06am
        ###############################

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment clock by 1 minute to change to 8:06am
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.set_pressure_value_in_psi(_psi=51)
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.self_test_and_update_object_attributes()

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # verify water source
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()

            # verify points of control
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].pumps[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].master_valves[2].statuses.verify_status_is_watering()

            # verify Mainlines
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_running()

            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[6].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[7].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[8].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[9].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[10].statuses.verify_status_is_waiting_to_water()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_running()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_15(self):
        """
        ###############################
        Simulate Program 1 - 8:07am
        ###############################

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment clock by 1 minute to change to 8:07am
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.set_pressure_value_in_psi(_psi=76)
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.self_test_and_update_object_attributes()

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # verify water source
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()

            # verify points of control
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].pumps[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].master_valves[2].statuses.verify_status_is_watering()

            # verify Mainlines
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_running()

            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[6].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[7].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[8].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[9].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[10].statuses.verify_status_is_waiting_to_water()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_running()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_16(self):
        """
        ###############################
        Simulate Program 1 - 8:08am
        ###############################

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment clock by 1 minute to change to 8:08am
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.set_pressure_value_in_psi(_psi=76)
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.self_test_and_update_object_attributes()

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # verify water source
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()

            # verify points of control
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].pumps[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].master_valves[2].statuses.verify_status_is_watering()

            # verify Mainlines
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[2].zones[6].statuses.verify_status_is_soaking()

            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[2].zones[7].statuses.verify_status_is_soaking()

            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[8].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[9].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[10].statuses.verify_status_is_watering()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_running()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_17(self):
        """
        ###############################
        Simulate Program 1 - 8:09am
        ###############################

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment clock by 1 minute to change to 8:09am
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.set_pressure_value_in_psi(_psi=76)
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.self_test_and_update_object_attributes()

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # verify water source
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()

            # verify points of control
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].pumps[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].master_valves[2].statuses.verify_status_is_watering()

            # verify Mainlines
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_running()

            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[2].zones[6].statuses.verify_status_is_soaking()

            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[2].zones[7].statuses.verify_status_is_soaking()

            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[8].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[9].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[10].statuses.verify_status_is_watering()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_running()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_18(self):
        """
        ###############################
        Simulate Program 1 - 8:10am
        ###############################

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment clock by 1 minute to change to 8:10am
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.set_pressure_value_in_psi(_psi=76)
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.self_test_and_update_object_attributes()

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # verify water source
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()

            # verify points of control
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].pumps[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].master_valves[2].statuses.verify_status_is_watering()

            # verify Mainlines
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_running()

            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[6].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[7].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[2].zones[8].statuses.verify_status_is_soaking()

            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[2].zones[9].statuses.verify_status_is_soaking()

            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[10].statuses.verify_status_is_watering()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_running()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_19(self):
        """
        ###############################
        Simulate Program 1 - 8:11am
        ###############################

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment clock by 1 minute to change to 8:11am
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.set_pressure_value_in_psi(_psi=76)
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.self_test_and_update_object_attributes()

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # verify water source
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()

            # verify points of control
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].pumps[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].master_valves[2].statuses.verify_status_is_watering()

            # verify Mainlines
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_running()

            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[6].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[7].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[2].zones[8].statuses.verify_status_is_soaking()

            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[2].zones[9].statuses.verify_status_is_soaking()

            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[10].statuses.verify_status_is_watering()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_running()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_20(self):
        """
        ###############################
        Simulate Program 1 - 8:12am
        ###############################

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment clock by 1 minute to change to 8:12am
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.set_pressure_value_in_psi(_psi=51)	# minute early
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.self_test_and_update_object_attributes()

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # verify water source
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()

            # verify points of control
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].pumps[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].master_valves[2].statuses.verify_status_is_watering()

            # verify Mainlines
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_running()

            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[6].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[7].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[8].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[2].zones[9].statuses.verify_status_is_soaking()

            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[2].zones[10].statuses.verify_status_is_soaking()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_running()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_21(self):
        """
        ###############################
        Simulate Program 1 - 8:13am
        ###############################

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment clock by 1 minute to change to 8:13am
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.set_pressure_value_in_psi(_psi=76)
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.self_test_and_update_object_attributes()

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            # verify water source
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()

            # verify points of control
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].pumps[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].master_valves[2].statuses.verify_status_is_watering()

            # verify Mainlines
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_running()

            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[6].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[7].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[8].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[2].zones[9].statuses.verify_status_is_soaking()

            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[2].zones[10].statuses.verify_status_is_soaking()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_running()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_22(self):
        """
        ###############################
        Simulate Program 1 - 8:14am
        ###############################

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment clock by 1 minute to change to 8:14am
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.set_pressure_value_in_psi(_psi=76)
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.self_test_and_update_object_attributes()

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # verify water source
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()

            # verify points of control
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].pumps[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].master_valves[2].statuses.verify_status_is_watering()

            # verify Mainlines
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_running()

            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].zones[6].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].zones[7].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[8].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[9].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[10].statuses.verify_status_is_watering()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_running()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_23(self):
        """
        ###############################
        Simulate Program 1 - 8:15am
        ###############################

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment clock by 1 minute to change to 8:15am
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.set_pressure_value_in_psi(_psi=76)
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.self_test_and_update_object_attributes()

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # verify water source
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()

            # verify points of control
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].pumps[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].master_valves[2].statuses.verify_status_is_watering()

            # verify Mainlines
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_running()

            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].zones[6].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].zones[7].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[8].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[9].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[10].statuses.verify_status_is_watering()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_running()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_24(self):
        """
        ###############################
        Simulate Program 1 - 8:16am
        ###############################

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment clock by 1 minute to change to 8:16am
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.set_pressure_value_in_psi(_psi=76)
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.self_test_and_update_object_attributes()

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # verify water source
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()

            # verify points of control
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].pumps[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].master_valves[2].statuses.verify_status_is_watering()

            # verify Mainlines
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_running()

            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].zones[6].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].zones[7].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].zones[8].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[9].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[10].statuses.verify_status_is_watering()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_running()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_25(self):
        """
        ###############################
        Simulate Program 1 - 8:17am
        ###############################

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment clock by 1 minute to change to 8:17am
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.set_pressure_value_in_psi(_psi=76)
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.self_test_and_update_object_attributes()

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # verify water source
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()

            # verify points of control
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].pumps[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].master_valves[2].statuses.verify_status_is_watering()

            # verify Mainlines
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_running()

            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].zones[6].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].zones[7].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].zones[8].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[9].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[10].statuses.verify_status_is_watering()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_running()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_26(self):
        """
        ###############################
        Simulate Program 1 - 8:18am
        ###############################

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment clock by 1 minute to change to 8:18am
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.set_pressure_value_in_psi(_psi=76)
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.self_test_and_update_object_attributes()

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # verify water source
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()

            # verify points of control
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].pumps[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].master_valves[2].statuses.verify_status_is_watering()

            # verify Mainlines
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_running()

            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].zones[6].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].zones[7].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].zones[8].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].zones[9].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].zones[10].statuses.verify_status_is_done()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_27(self):
        """
        ###############################
        Simulate Program 1 - 8:19am
        ###############################

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment clock by 1 minute to change to 8:19am
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.set_pressure_value_in_psi(_psi=101)
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.self_test_and_update_object_attributes()

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # verify water source
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()

            # verify points of control
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].pumps[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].master_valves[2].statuses.verify_status_is_watering()

            # verify Mainlines
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_running()

            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].zones[6].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].zones[7].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].zones[8].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].zones[9].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].zones[10].statuses.verify_status_is_done()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_28(self):
        """
        ###############################
        Simulate Program 1 - 8:20am
        ###############################

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment clock by 1 minute to change to 8:20am
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.set_pressure_value_in_psi(_psi=101)
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.self_test_and_update_object_attributes()

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # verify water source
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_ok()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_ok()

            # verify points of control
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_off()
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_off()

            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[2].pumps[2].statuses.verify_status_is_off()

            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[2].master_valves[2].statuses.verify_status_is_off()

            # verify Mainlines
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_off()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_off()

            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].zones[6].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].zones[7].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].zones[8].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].zones[9].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].zones[10].statuses.verify_status_is_done()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
