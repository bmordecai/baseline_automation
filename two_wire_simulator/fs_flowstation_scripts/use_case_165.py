import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration

# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr

__author__ = 'Tim'


class ControllerUseCase165(object):
    """
    Test name:
        - Manual zone run with FlowStation
        - NOTE:  patterned after use case 61
        
    Purpose:
        -   Test and verify that zone manual run operations always turn on the zone and gets water from the FlowStation
            when water is available.  The zone should turn on and run in all situations and conditions.

    User Story:
        1)  As a user I want to be able to manually run a zone and see the zone running (watering) in all conditions.

        2)  I want to do this from the 3200 front panel, BaseManager, and Mobile Access.

        3)  I want to be able to run any number manual zones and have them run independent of which
            mainline they are on.

        4)  When I turn on a "type 5" manual zone, I expect program zones that are watering on the same mainline
            to continue to water.  The only limit to the number of manual zones is the controller concurrent
            zone count.

        5)  As I turn on more manual zones, I expect them to pause other zones as the manual zones take water from
            zones that are running on another mainline/controller that shares water from a common POC, 
			but not from other manual zones from other controllers.

        6)  If the total water used by the manual zones exceeds the design flow of the water path, additional manual
            zones should continue to be turned on as requested.

        7)  If I do a manual zone run while there is a flow shutdown condition (like high flow shutdown), I expect
            the zone to run, but the water to not be on.

        8)  If I do a manual zone run while the FlowStation is off-line, I expect the zone to run.

    Coverage area of feature:
        1)  The 3200 supports the following types of manual run operations:
            Type 1: run one or all zones for fixed time, between zone delays, and Next/Previous keys to move
                    between zones.
                    - supported in test engine
            Type 2: run zones for a fixed time and specify the number to run concurrently.
                    - not supported in test engine
            Type 3: run pump or MV for a fixed time and specify the number to run concurrently.
                    - not supported in test engine
            Type 4: run program zones for a fixed time and specify the number to run concurrently.
                    - not supported in test engine
            Type 5: add another manual run zone with its run time (same as BaseManager).
                    - supported in test engine

        2)  This use case will cover Type 5 manual run operations.

        3)  The following conditions will be applied to manual runs:  these will be applied both while running and
            before running.
            a)  High flow shutdown
            b)  FlowStation off-line
            c)  water source empty

    Not covered in this use case:
        1)  Multiple 3200 controllers

        2)  Manual run types 1, 2, 3, and 4.

        3)  Other conditions:
            a)  Low flow shutdown
            b)  water budget shutdown
            c)  POC offline (other controller)

    Use case explanation:
        -1)  FlowStation Configuration:  the following WS/PC/ML assignments will be established on the FlowStation:
            Note:  the bracket value is the FlowStation slot assignment
            a)  CN1 WS1 [WS1] -> CN1 PC1 [PC1] -> CN1 ML1 [ML1]
            b)  POC-1:  design flow = 80 GPM, High Flow Shutdown = 120 GPM
            c)  ML-1:  design flow = 160 GPM, manage zones by flow = true

        0)  Controller configuration:
            a)  WS-1:  empty condition by event switch = OPEN
            b)  POC-1:  flow meter, normally open master valve
            c)  Program 1:  5 zones (01-05) on ML1 - these can be used in manual operations
            d)  Program 2:  5 zones (11-15) on ML1 - this can be set to run
            e)  Zones:  all with 25 GPM design flow, 30 minutes run time on program
            f)  Concurrent zone count:  programs = 10, controller = 99

        1)  Scenario 1:  Type 5 manual run, 1 zone
            a)  high flow shutdown enabled, water source not empty, set flow to 100 GPM
            b)  run zone 3 for 3 minutes
            c)  verify WS, PC, ML running
            d)  verify all off when done

        2)  Scenario 2:  Type 5 manual run, 1 zone with program running
            a)  high flow shutdown enabled, water source not empty, set flow to 100 GPM
            b)  start program 2, verify three zones running
            c)  run zone 3 for 3 minutes
            d)  verify three zones on program 2 go to soaking
            e)  verify three zone return to watering when manual time up

        3)  Scenario 3:  Type 5 manual run, five zones
            a)  high flow shutdown enabled, water source not empty, set flow to 100 GPM
            b)  run zones:
                - 5 for 6 minutes and 4 for 5 minutes
                - one minute later, 3 for 3 minutes and 2 for 5 minutes
                - one minute later, 1 for 4 minutes
            c)  verify WS, PC, ML running
            d)  verify run times of all zones
            e)  verify all off when done

        4)  Scenario 4:  Type 5 manual run with program running and with high flow shutdown (during and before)
            a)  high flow shutdown enabled, water source not empty, set flow to 200 GPM
            b)  start program 2, verify three zones running
            c)  start zone 3 for 5 minutes
            d)  verify three zones on program 2 go to soaking
            e)  after three minutes, verify high flow shutdown and manual zone watering
            f)  verify three program zones set to done
            g)  start zone 1 for 2 minutes, verify watering
            h)  run to completion

        5)  Scenario 5:  Type 5 manual run with program running and with empty condition (during and before)
            a)  high flow shutdown enabled, water source not empty, set flow to 100 GPM
            b)  start program 2, verify three zones running
            c)  start zone 3 for 5 minutes
            d)  verify three zones on program 2 go to soaking
            c)  set empty condition TRUE
            e)  verify empty shutdown and manual zone watering
            f)  verify three program zones set to soaking
            g)  start zone 1 for 2 minutes, verify watering
            h)  clear empty condition and run past delay time to clear it
            i)  run to completion

        6)  Scenario 6:  Type 5 manual run, 1 zone with unscheduled flow shutdown enabled and triggered
            a)  high flow shutdown enabled, water source not empty, set flow to 100 GPM
            b)  run zone 3 for 5 minutes
            c)  verify WS, PC, ML running
            d)  verify unscheduled flow message after three minutes
            e)  verify all off when done

    """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/fs_json_config_files'
                                    )
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        program_start_times = [600] # 10:00am start time
        program_water_windows = ['111111111111111111111111']
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add and configure Program 1
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_water_window(_ww=program_water_windows)
            self.config.BaseStation3200[1].programs[1].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=10)
            self.config.BaseStation3200[1].programs[1].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(_sun=True,
                                                                                                           _mon=True,
                                                                                                           _tues=True,
                                                                                                           _wed=True,
                                                                                                           _thurs=True,
                                                                                                           _fri=True,
                                                                                                           _sat=True)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_start_times)

            # Add and configure Program 2
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation3200[1].programs[2].set_enabled()
            self.config.BaseStation3200[1].programs[2].set_water_window(_ww=program_water_windows)
            self.config.BaseStation3200[1].programs[2].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[2].set_max_concurrent_zones(_number_of_zones=10)
            self.config.BaseStation3200[1].programs[2].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[2].set_watering_intervals_to_selected_days_of_the_week(_sun=True,
                                                                                                           _mon=True,
                                                                                                           _tues=True,
                                                                                                           _wed=True,
                                                                                                           _thurs=True,
                                                                                                           _fri=True,
                                                                                                           _sat=True)
            self.config.BaseStation3200[1].programs[2].set_start_times(_st_list=program_start_times)

            # controller concurrent zones
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=99)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_3(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zones -----> to program

        Program   Zone      Mode      Minutes   GPM
            1       1       Primary     30      25
            1       2       Linked-1    100%    25
            1       3       Linked-1    100%    25
            1       4       Linked-1    100%    25
            1       5       Linked-1    100%    25
            2       11      Primary     30      25
            2       12      Linked-11   100%    25
            2       13      Linked-11   100%    25
            2       14      Linked-11   100%    25
            2       15      Linked-11   100%    25

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure Program Zone 1
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=30)
            self.config.BaseStation3200[1].zones[1].set_design_flow(_gallons_per_minute=25)

            # Add & Configure Program Zone 2
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1)
            self.config.BaseStation3200[1].zones[2].set_design_flow(_gallons_per_minute=25)

            # Add & Configure Program Zone 3
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_as_linked_zone(_primary_zone=1)
            self.config.BaseStation3200[1].zones[3].set_design_flow(_gallons_per_minute=25)

            # Add & Configure Program Zone 4
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[1].zone_programs[4].set_as_linked_zone(_primary_zone=1)
            self.config.BaseStation3200[1].zones[4].set_design_flow(_gallons_per_minute=25)

            # Add & Configure Program Zone 5
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[5].set_as_linked_zone(_primary_zone=1)
            self.config.BaseStation3200[1].zones[5].set_design_flow(_gallons_per_minute=25)

            # Add & Configure Program Zone 11
            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=11)
            self.config.BaseStation3200[1].programs[2].zone_programs[11].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[2].zone_programs[11].set_run_time(_minutes=30)
            self.config.BaseStation3200[1].zones[11].set_design_flow(_gallons_per_minute=25)
			
            # Add & Configure Program Zone 12
            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=12)
            self.config.BaseStation3200[1].programs[2].zone_programs[12].set_as_linked_zone(_primary_zone=11)
            self.config.BaseStation3200[1].zones[12].set_design_flow(_gallons_per_minute=25)

            # Add & Configure Program Zone 13
            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=13)
            self.config.BaseStation3200[1].programs[2].zone_programs[13].set_as_linked_zone(_primary_zone=11)
            self.config.BaseStation3200[1].zones[13].set_design_flow(_gallons_per_minute=25)

            # Add & Configure Program Zone 14
            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=14)
            self.config.BaseStation3200[1].programs[2].zone_programs[14].set_as_linked_zone(_primary_zone=11)
            self.config.BaseStation3200[1].zones[14].set_design_flow(_gallons_per_minute=25)

            # Add & Configure Program Zone 15
            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=15)
            self.config.BaseStation3200[1].programs[2].zone_programs[15].set_as_linked_zone(_primary_zone=11)
            self.config.BaseStation3200[1].zones[15].set_design_flow(_gallons_per_minute=25)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
			
    #################################
    def step_4(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controller

        set up WS 1
            - enabled
            - empty conditions:
                - event switch = OPEN
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Water Source 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].add_switch_empty_condition(_event_switch_address=1)
            self.config.BaseStation3200[1].water_sources[1].switch_empty_conditions[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].switch_empty_conditions[1].set_switch_empty_condition_to_open()
            self.config.BaseStation3200[1].water_sources[1].switch_empty_conditions[1].set_empty_wait_time(_minutes=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                    self.config.test_name,
                    sys._getframe().f_code.co_name,
                    date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                    str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        setup Point of Controls
        ############################

        set up POC 1
            enable POC 1
            assign master valve TMV0001 as normally open
            assign flow meter TWF0001
            assign POC 1 a target flow of 80
            assign POC 1 to main line 1
            set high flow limit to 120 and enable high flow shut down

        """


        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_flow_meter_to_point_of_control(
                _flow_meter_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=80)
            self.config.BaseStation3200[1].points_of_control[1].set_high_flow_limit(_limit=120.0,
                                                                                    with_shutdown_enabled=True)
            # Add POC 1 to Water Source 1
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=1)

            # change master valve to normally open
            self.config.BaseStation3200[1].master_valves[1].set_normally_open_state(_normally_open=opcodes.true)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                    self.config.test_name,
                    sys._getframe().f_code.co_name,
                    date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                    str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]



    #################################
    def step_6(self):
        """
        ##################
        setup mainlines
        ##################

        - set up main line 1
            - set limit zones by flow to true
            - set the pipe fill time to 1 minutes
            - set the target flow to 200
            - assign ML1 to POC1

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=200)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_true()

            # Add ML 1 to POC 1
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                    self.config.test_name,
                    sys._getframe().f_code.co_name,
                    date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                    str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add all to ML1
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=1)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=2)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=3)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=4)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=5)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=11)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=12)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=13)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=14)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=15)

            # Assign a design flow value to each zone
            self.config.BaseStation3200[1].zones[1].set_design_flow(_gallons_per_minute=25)
            self.config.BaseStation3200[1].zones[2].set_design_flow(_gallons_per_minute=25)
            self.config.BaseStation3200[1].zones[3].set_design_flow(_gallons_per_minute=25)
            self.config.BaseStation3200[1].zones[4].set_design_flow(_gallons_per_minute=25)
            self.config.BaseStation3200[1].zones[5].set_design_flow(_gallons_per_minute=25)
            self.config.BaseStation3200[1].zones[11].set_design_flow(_gallons_per_minute=25)
            self.config.BaseStation3200[1].zones[12].set_design_flow(_gallons_per_minute=25)
            self.config.BaseStation3200[1].zones[13].set_design_flow(_gallons_per_minute=25)
            self.config.BaseStation3200[1].zones[14].set_design_flow(_gallons_per_minute=25)
            self.config.BaseStation3200[1].zones[15].set_design_flow(_gallons_per_minute=25)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        Share controller, water source, point of control, mainline with Flow Station
        ###############################
            - Share controller1 with the flow station
            - Add and share a Water Source between controller1 and the FlowStation
            - Add and share Point of Control between controller1 and the FlowStation
            - Add and share Mainline between controller1 and the FlowStation
			
            - Assign WS, POC, and ML on the FlowStation
            - CN1 WS1 -> CN1 PC1 -> CN1 ML1
        """
    
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # ---------------------
            # Add CN 1 slot 1 on FS
            # ---------------------
        
            self.config.FlowStations[1].add_controller_to_flow_station(
                _controller_address=1,
                _flow_station_slot_number=1)
        
            # ------------------------------------------
            # Add CN 1 WS 1 to slot 1 on FS and share it
            # ------------------------------------------
        
            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=1,
                _controller_water_source_address=1,
                _flow_station_water_source_slot_number=1)
        
            # Tell the 3200 that the FlowStation is in control of Water Source 1.
            self.config.BaseStation3200[1].get_water_source(1).set_manage_by_flowstation()
        
            # ------------------------------------------
            # Add CN 1 PC 1 to slot 1 on FS and share it
            # ------------------------------------------
        
            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=1,
                _controller_point_of_control_address=1,
                _flow_station_point_of_control_slot_number=1)
        
            # Tell the 3200 that the FlowStation is in control of Point of Control 1.
            self.config.BaseStation3200[1].get_point_of_control(1).set_manage_by_flowstation()
        
            # ------------------------------------------
            # Add CN 1 ML 1 to slot 1 on FS and share it
            # ------------------------------------------
        
            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=1,
                _controller_mainline_address=1,
                _flow_station_mainline_slot_number=1)
        
            # Tell the 3200 that the FlowStation is in control of Mainline 1.
            self.config.BaseStation3200[1].get_mainline(1).set_manage_by_flowstation()

            # Tell the 3200 to have the FlowStation manage Point of Control 1.
            self.config.BaseStation3200[1].get_point_of_control(1).set_manage_by_flowstation()
        
            # --------------------------------------
            # Assign WS, POC, and ML on FlowStation:
            # CN1 WS1 -> CN1 PC1 -> CN1 ML1
            # --------------------------------------
        
            self.config.FlowStations[1].get_water_source(ws_number=1).add_point_of_control_to_water_source(_point_of_control_address=1)
            self.config.FlowStations[1].get_point_of_control(pc_number=1).add_mainline_to_point_of_control(_mainline_address=1)
    
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
            Scenario 1:  Type 5 manual run, 1 zone
            a)  high flow shutdown enabled, water source not empty, set flow to 100 GPM
            b)  run zone 3 for 3 minutes
            c)  verify WS, PC, ML running
            d)  verify all off when done
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # flow meter rate - really not used at this point in time
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=100.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()

            # empty condition false
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_closed()
            self.config.BaseStation3200[1].event_switches[1].bicoder.self_test_and_update_object_attributes()

            # one minute to settle readings/values
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # start manual run on zone 3 for three minutes
            self.config.BaseStation3200[1].zones[3].do_run_manual_zone_type_5(_minutes=3)

            # first minute and then check status of everything
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()

            # second minute and then check status of everything
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()

            # third minute and then check status of everything
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()

            # fourth minute and then check status of everything - OFF
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()

            # extra minute for status communications on shared stuff
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_off()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_11(self):
        """
        Scenario 2:  Type 5 manual run, 1 zone with program running
            a)  high flow shutdown enabled, water source not empty, set flow to 100 GPM
            b)  start program 2, verify three zones running
            c)  run zone 3 for 3 minutes
            d)  verify program zones are set to soaking
            e)  verify three zones back to running when manual run done
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # flow meter rate - really not used at this point in time
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=100.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()

            # empty condition false
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_closed()
            self.config.BaseStation3200[1].event_switches[1].bicoder.self_test_and_update_object_attributes()

            # start program 2
            self.config.BaseStation3200[1].programs[2].set_program_to_start_like_controller_manual_start()

            # two minutes to settle readings/values and get zones running
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=2)
            # another for FlowStation communications
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # should be three zones running on program 2
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_waiting_to_water()

            # start manual run on zone 3 for three minutes
            self.config.BaseStation3200[1].zones[3].do_run_manual_zone_type_5(_minutes=3)

            # first minute and then check status of everything
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()

            # should be no zones running on program 2
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_waiting_to_water()

            # second minute and then check status of everything
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()

            # should be no zones running on program 2
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_waiting_to_water()

            # third minute and then check status of everything
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()

            # should be no zones running on program 2
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_waiting_to_water()

            # fourth minute and then check status of everything - OFF
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()

            # another minute for water allocation, communication and status
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=2)

            # should be three zones running on program 2
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_waiting_to_water()

            # shutdown program 2
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_12(self):
        """
        Scenario 3:  Type 5 manual run, five zones
            a)  high flow shutdown enabled, water source not empty, set flow to 100 GPM
            b)  run zones:
                - 5 for 6 minutes and 4 for 5 minutes
                - one minute later, 3 for 3 minutes and 2 for 5 minutes
                - one minute later, 1 for 4 minutes
            c)  verify WS, PC, ML running
            d)  verify run times of all zones
            e)  verify all off when done
        Expected results:
            minute  Z1  Z2  Z3  Z4  Z5
                0   DN  DN  DN  DN  DN
                1   DN  DN  DN  WT  WT
                2   DN  WT  WT  WT  WT
                3   WT  WT  WT  WT  WT
                4   WT  WT  WT  WT  WT
                5   WT  WT  DN  WT  WT
                6   WT  WT  DN  DN  WT
                7   DN  DN  DN  DN  DN
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # flow meter rate - really not used at this point in time
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=100.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()

            # empty condition false
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_closed()
            self.config.BaseStation3200[1].event_switches[1].bicoder.self_test_and_update_object_attributes()

            # manual zones add
            self.config.BaseStation3200[1].zones[5].do_run_manual_zone_type_5(_minutes=6)
            self.config.BaseStation3200[1].zones[4].do_run_manual_zone_type_5(_minutes=5)

            # first minute and then check status of everything
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()

            # manual zones add
            self.config.BaseStation3200[1].zones[3].do_run_manual_zone_type_5(_minutes=3)
            self.config.BaseStation3200[1].zones[2].do_run_manual_zone_type_5(_minutes=5)

            # second minute and then check status of everything
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()

            # manual zones add
            self.config.BaseStation3200[1].zones[1].do_run_manual_zone_type_5(_minutes=4)

            # third minute and then check status of everything
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()

            # fourth minute and then check status of everything
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()

            # fifth minute and then check status of everything
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()

            # sixth minute and then check status of everything
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()

            # seventh minute and then check status of everything - OFF
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()

            # extra minute for communications and status
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_off()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_13(self):
        """
        Scenario 4:  Type 5 manual run with program running and with high flow shutdown (during and before)
            a)  high flow shutdown enabled, water source not empty, set flow to 200 GPM
            b)  start program 2, verify three zones running
            c)  start zone 3 for 5 minutes
            d)  verify three zones on program 2 go to soaking
            e)  after three minutes, verify high flow shutdown and manual zone watering
            f)  verify three program zones set to done
            g)  start zone 1 for 2 minutes, verify watering
            h)  run to completion
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # empty condition false
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_closed()
            self.config.BaseStation3200[1].event_switches[1].bicoder.self_test_and_update_object_attributes()

            # start program 2
            self.config.BaseStation3200[1].programs[2].set_program_to_start_like_controller_manual_start()

            # minute=1 to settle readings/values and get zones running
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # flow meter rate - set to high flow rate
            # move this here because of two minute shutdown time in 3200 w/FlowStation
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=200.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()

            # another for FlowStation communications
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # should be three zones running on program 2
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_waiting_to_water()

            # start manual run on zone 3
            self.config.BaseStation3200[1].zones[3].do_run_manual_zone_type_5(_minutes=5)

            # minute=2 and then check status of everything
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Flow Fault is detected and the zones that are not manually running get set to done, zone 3 was manually
            # run so it should still be running
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_flow_fault()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_done()

            # minute=3 and then check status of everything
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_flow_fault()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].points_of_control[1].messages.verify_high_flow_shutdown_message()

            # start manual run on zone 1
            self.config.BaseStation3200[1].zones[1].do_run_manual_zone_type_5(_minutes=2)

            # minute=4 and then check status of everything
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_flow_fault()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()

            # minute=5 and then check status of everything
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_flow_fault()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()

            # minute=6/7 and then check status of everything - OFF
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=100.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].points_of_control[1].messages.clear_high_flow_shutdown_message()
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=2)
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()

            # extra minute for communications and status
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_off()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_14(self):
        """
        Scenario 5:  Type 5 manual run with program running and with empty condition (during and before)
            a)  high flow shutdown enabled, water source not empty, set flow to 100 GPM
            b)  start program 2, verify three zones running
            c)  start zone 3 for 5 minutes
            d)  verify three zones on program 2 go to soaking
            c)  set empty condition TRUE
            e)  verify empty shutdown and manual zone watering
            f)  verify three program zones set to soaking
            g)  start zone 1 for 2 minutes, verify watering
            h)  clear empty condition and run past delay time to clear it
            i)  run to completion
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # flow meter rate - really not used at this point in time
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=100.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()

            # empty condition false
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_closed()
            self.config.BaseStation3200[1].event_switches[1].bicoder.self_test_and_update_object_attributes()

            # start program 2
            self.config.BaseStation3200[1].programs[2].set_program_to_start_like_controller_manual_start()

            # minute=1+1 to settle readings/values and get zones running
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # minute=1 to settle readings/values and get zones running
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # should be three zones running on program 2
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_waiting_to_water()

            # start manual run on zone 3
            self.config.BaseStation3200[1].zones[3].do_run_manual_zone_type_5(_minutes=5)

            # minute=2 and then check status of everything
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # should be three zones running on program 2
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_waiting_to_water()

            # set water source empty state
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_open()
            self.config.BaseStation3200[1].event_switches[1].bicoder.self_test_and_update_object_attributes()

            # minute=3 and then check status of everything
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=2)
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_water_empty()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].water_sources[1].switch_empty_conditions[1].messages.\
                verify_empty_condition_with_event_switch_message()

            # start manual run on zone 1
            self.config.BaseStation3200[1].zones[1].do_run_manual_zone_type_5(_minutes=2)

            # minute=4 and then check status of everything
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_water_empty()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()

            # minute=5 and then check status of everything
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_water_empty()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()

            # minute=6/7 and then check status of everything - OFF
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_closed()
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=3)

            # extra minute for FlowStation water allocation
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_off()

            # program zones back watering
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_done()

            # clean up
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ##############################
    def step_15(self):
        """
            Scenario 6:  Type 5 manual run, 1 zone with unscheduled flow shutdown enabled and triggered
            a)  high flow shutdown enabled, water source not empty, set flow to 12 GPM
            b)  run zone 3 for 3 minutes
            c)  verify WS, PC, ML running
            d)  verify no unscheduled flow message after three minutes
            e)  verify all off when done
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # set up unexpected flow shutdown
            self.config.BaseStation3200[1].points_of_control[1].set_unscheduled_flow_limit(_gallons=10.0,
                                                                                           with_shutdown_enabled=True)

            # flow meter rate - flow just over limit
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=12.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()

            # empty condition false
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_closed()
            self.config.BaseStation3200[1].event_switches[1].bicoder.self_test_and_update_object_attributes()

            # one+1minute to settle readings/values
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=2)

            # start manual run on zone 3 for three minutes
            self.config.BaseStation3200[1].zones[3].do_run_manual_zone_type_5(_minutes=3)

            # first minute and then check status of everything
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()

            # second minute and then check status of everything
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()

            # third minute and then check status of everything
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_manually_running()
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[1].messages.check_for_unscheduled_flow_shutdown_message_not_present()

            # fourth minute and then check status of everything - OFF
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=2)

            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_off()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]





