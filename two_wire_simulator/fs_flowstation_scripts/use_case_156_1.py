import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

__author__ = 'Ben'


class ControllerUseCase156_1(object):
    """
    Test name:
        - CN UseCase156_1 Advanced flow variance with three strike rule

        User Story:
        1)  As a user, if my mainline has a leak, causing excessive flow then I want my entire mainline to be shut down

        2)  As a user, if I have an obstruction in my mainline, I want my entire mainline to be shut down

    Coverage and Objectives:
        1.	When a mainline is using advanced high/low flow variance with shutdown enabled and the mainline experiences
            three consecutive flow-faults:
            a.	irrigation for mainline with advanced high flow variance tier 1 limit should be shutdown and a alert to
                the user saying the mainline was shutdown due to consecutive flow faults.
            b.	irrigation for mainline with advanced low flow variance tier 4 limit should be shutdown and a alert to
                the user saying the mainline was shutdown due to consecutive flow faults.
            c.	running a single zone on a mainline
            d.	running multiple zones on a mainline

    Not Covered:
        1.	Advanced Flow Variance Disabled 
        2.	Advanced High Flow Variance tiers 2,3,4
        3.	Advanced Low Flow Variance tiers 1,2,3
        4.	Pressure Flow Stabilization
        5.	Single 3200 managed by FlowStation
        6.	Multiple POCs attached to a single mainline
        7.	Nested mainlines
        8.	Mainline with downstream POC
        9.	High flows & shutdowns
        10.	Unscheduled flows & shutdowns
        11.	Mainline Standard variances & shutdowns
        12.	Empty conditions
        13.	Manual runs with flow variance (if manual running, ALL flow variance is ignored)
        14.	When a flow meter has a IO error or is non-responsive, ALL flow variance is ignored
        15.	Learn flows with flow variance (if doing a learn flow, ALL flow variance is ignored)



    Test Overview:
        - Advanced Variance Tier 1:
            - ML 1: High flow variance with shutdown (1 min pipe fill)

        - Advanced Variance Tier 4:
            - ML 2: Low flow variance with shutdown (3 min pipe fill)

        -FlowStation assignments
             C1:W1  C2:W2
               |      |
             C1:P1  C2:P2
               |      |
             C1:M1  C2:M2 

    Test Configuration setup: \n
        1. Test Advance flow Tier 1 with shut down\n
            - Tier 1 range 0 - 25 GPM \n
            - Configuration
                - WS 1 ---> POC 1 ---> ML 1 \n
                    - POC 1
                        - FM 1
                        - design flow 10
                    - ML 1  Advanced high flow Tier 1 With Shutdown\n
                        - ZN 1 design flow 5
                        - ZN 2 design flow 5
                        - ZN 3 design flow 5
                        - Variance set to 80%
                        - Cause HF Variance setting flowmeter to 24 GPM
                        - design flow 10
                    - PG 1
        2. Test Advance flow Tier 4 with shutdown
            - Tier 4 range 300+ GPM \n
            - Configuration
                - WS 2 ---> POC 2 ---> ML 2 \n
                    - POC 2
                        - FM 2
                        - design flow 845
                    -ML 2 Advanced low flow Tier 4 With Shutdown\n
                        - ZN 4 design flow 500
                        - ZN 5 design flow 500
                        - ZN 6 design flow 500
                        - variance set to 20%
                        - Cause LF variance by setting flowmeter to 310 GPM
                        - design flow 845
                    - PG 2
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/fs_json_config_files'
                                    )

        # these are global variables for the test
        # zone design flow values

        # ML 1
        # HF Variance Tier 1 (<25gpm)
        self.zn_1_df = 5
        self.zn_2_df = 5
        self.zn_3_df = 5

        # ML 2
        # LF Variance Tier 4 (>300gpm)
        self.zn_4_df = 500
        self.zn_5_df = 500
        self.zn_6_df = 500

        # mainline flow variance percentages

        # Tier 1
        self.ml_1_hi_fl_vr = 80
        self.ml_1_lo_fl_vr = 0  # only testing HF variance on ML 1

        # Tier 4
        self.ml_2_hi_fl_vr = 0  # only testing LF variance on ML 2
        self.ml_2_lo_fl_vr = 20

        # Placeholders for variance trigger calculations.
        #   -> These values will represent the calculated actual flow needed for FlowMeters in order to trigger the
        #      threshold boundaries.
        self.setting_for_ml_1_high_flow_variance_calculation = None
        self.setting_for_ml_2_low_flow_variance_calculation = None
        # self.setting_for_ml_5_high_flow_variance_calculation = None
        # self.setting_for_ml_6_low_flow_variance_calculation = None
        # self.setting_for_ml_7_high_flow_variance_calculation = None
        # self.setting_for_ml_8_low_flow_variance_calculation = None

        self.run_use_case()

    #################################
    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]

                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))

                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()


                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
            
        - Setting both controller max concurrent zones and programs max concurrent zones so large that they are not a
          limiting factor for # of zones running
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set controller max concurrency to allow for all programs and zones to water
            self.config.BaseStation3200[1].set_max_concurrent_zones(40)
            self.config.BaseStation3200[2].set_max_concurrent_zones(40)

            # Add and configure Programs 1 - 2
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=15)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=[600])  # 10am start time

            self.config.BaseStation3200[2].add_program_to_controller(_program_address=2)
            self.config.BaseStation3200[2].programs[2].set_enabled()
            self.config.BaseStation3200[2].programs[2].set_max_concurrent_zones(_number_of_zones=15)
            self.config.BaseStation3200[2].programs[2].set_start_times(_st_list=[600])  # 10am start time
            
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # Add & Configure Program Zones for all programs 1-2, 3 zones each

            # Add & Configure Program 1 Zones 1 - 3
            for zn_address in range(1, 4):
                self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=zn_address)
                self.config.BaseStation3200[1].programs[1].zone_programs[zn_address].set_as_timed_zone()
                self.config.BaseStation3200[1].programs[1].zone_programs[zn_address].set_run_time(_minutes=10)

            # Add & Configure Program 2 Zones 4 - 6
            for zn_address in range(4, 7):
                self.config.BaseStation3200[2].programs[2].add_zone_to_program(_zone_address=zn_address)
                self.config.BaseStation3200[2].programs[2].zone_programs[zn_address].set_as_timed_zone()
                self.config.BaseStation3200[2].programs[2].zone_programs[zn_address].set_run_time(_minutes=10)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n

        - No budget is used because we do not want the water source to shut down
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Water Source 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].set_priority(_priority_for_water_source=1)
            self.config.BaseStation3200[1].water_sources[1].set_monthly_watering_budget(_budget=0)

            # Water Source 2
            self.config.BaseStation3200[2].add_water_source_to_controller(_water_source_address=2)
            self.config.BaseStation3200[2].water_sources[2].set_enabled()
            self.config.BaseStation3200[2].water_sources[2].set_priority(_priority_for_water_source=1)
            self.config.BaseStation3200[2].water_sources[2].set_monthly_watering_budget(_budget=0)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n

        - set up POCs 1-8
            - set design flow
            - assign POC to mainline
            - assign flow meter

        - Not setting any flow limits because we do not want any flow shutdowns
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure POC 1 - 2
            
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].add_flow_meter_to_point_of_control(
                _flow_meter_address=1)

            self.config.BaseStation3200[2].add_point_of_control_to_controller(_point_of_control_address=2)
            self.config.BaseStation3200[2].points_of_control[2].set_enabled()
            self.config.BaseStation3200[2].points_of_control[2].add_flow_meter_to_point_of_control(
                _flow_meter_address=2)

            # set design flow
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=10)
            self.config.BaseStation3200[2].points_of_control[2].set_target_flow(_gpm=845)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n

        Not added POC -> Mainline assignments at the controller. This is done at the FlowStation later on.
        The mainline target flow are equal to the POC target flows.

        """
        # here we can either execute the following uncommented lines in procedural fashion, or we could re-init the
        # object, would have to import Mainline at the top, effectively accomplishing the same thing by:
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ----------- #
            # MAINLINE 1  #
            # ----------- #

            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=10)
            self.config.BaseStation3200[1].mainlines[1].set_use_advanced_flow_to_true()
            self.config.BaseStation3200[1].mainlines[1].set_high_flow_variance_tier_one(_percent=self.ml_1_hi_fl_vr,
                                                                                        _with_shutdown_enabled=True)

            # ----------- #
            # MAINLINE 2  #
            # ----------- #

            self.config.BaseStation3200[2].add_mainline_to_controller(_mainline_address=2)
            self.config.BaseStation3200[2].mainlines[2].set_pipe_stabilization_time(_minutes=3)
            self.config.BaseStation3200[2].mainlines[2].set_target_flow(_gpm=845)
            self.config.BaseStation3200[2].mainlines[2].set_use_advanced_flow_to_true()
            self.config.BaseStation3200[2].mainlines[2].set_low_flow_variance_tier_four(_percent=self.ml_2_lo_fl_vr,
                                                                                        _with_shutdown_enabled=True)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=1)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=2)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=3)

            self.config.BaseStation3200[2].mainlines[2].add_zone_to_mainline(_zone_address=4)
            self.config.BaseStation3200[2].mainlines[2].add_zone_to_mainline(_zone_address=5)
            self.config.BaseStation3200[2].mainlines[2].add_zone_to_mainline(_zone_address=6)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ######################
        setup zones on mainlines
        ######################
         - set up zone  Attributes associated with main lines \n
            - set design flow on zone
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Assign a design flow value to each zone so that they have a default setting
            self.config.BaseStation3200[1].zones[1].set_design_flow(_gallons_per_minute=self.zn_1_df)
            self.config.BaseStation3200[1].zones[2].set_design_flow(_gallons_per_minute=self.zn_2_df)
            self.config.BaseStation3200[1].zones[3].set_design_flow(_gallons_per_minute=self.zn_3_df)
            
            self.config.BaseStation3200[2].zones[4].set_design_flow(_gallons_per_minute=self.zn_4_df)
            self.config.BaseStation3200[2].zones[5].set_design_flow(_gallons_per_minute=self.zn_5_df)
            self.config.BaseStation3200[2].zones[6].set_design_flow(_gallons_per_minute=self.zn_6_df)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        Share controller, water source, point of control, mainline with Flow Station
        ###############################
            - Share controller1 with the flow station
            - Add and share a Water Source between controller1 and the FlowStation
            - Add and share Point of Control between controller1 and the FlowStation
            - Add and share Mainline between controller1 and the FlowStation

            - Assign WS, POC, and ML on the FlowStation
            - WSn -> PCn -> MLn

            - FlowStation assignments
                 C1:W1  C2:W2
                   |      |
                 C1:P1  C2:P2
                   |      |
                 C1:M1  C2:M2 
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add controller 1 to FlowStation
            self.config.FlowStations[1].add_controller_to_flow_station(
                _controller_address=1,
                _flow_station_slot_number=1)

            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=1,
                _controller_water_source_address=1,
                _flow_station_water_source_slot_number=1)

            # Tell the 3200 that the FlowStation is in control of Water Source
            self.config.BaseStation3200[1].get_water_source(ws_number=1).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=1,
                _controller_point_of_control_address=1,
                _flow_station_point_of_control_slot_number=1)

            # Tell the 3200 that the FlowStation is in control of Point of Control
            self.config.BaseStation3200[1].get_point_of_control(pc_number=1).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=1,
                _controller_mainline_address=1,
                _flow_station_mainline_slot_number=1)

            # Tell the 3200 that the FlowStation is in control of Mainline
            self.config.BaseStation3200[1].get_mainline(ml_number=1).set_manage_by_flowstation()

            # Add controller 2 to FlowStation
            self.config.FlowStations[1].add_controller_to_flow_station(
                _controller_address=2,
                _flow_station_slot_number=2)

            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=2,
                _controller_water_source_address=2,
                _flow_station_water_source_slot_number=2)

            # Tell the 3200 that the FlowStation is in control of Water Source
            self.config.BaseStation3200[2].get_water_source(ws_number=2).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=2,
                _controller_point_of_control_address=2,
                _flow_station_point_of_control_slot_number=2)

            # Tell the 3200 that the FlowStation is in control of Point of Control
            self.config.BaseStation3200[2].get_point_of_control(pc_number=2).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=2,
                _controller_mainline_address=2,
                _flow_station_mainline_slot_number=2)

            # Tell the 3200 that the FlowStation is in control of Mainline
            self.config.BaseStation3200[2].get_mainline(ml_number=2).set_manage_by_flowstation()

            # C1W1->C1P1->C1M1
            self.config.FlowStations[1].get_point_of_control(pc_number=1).add_mainline_to_point_of_control(_mainline_address=1)
            self.config.FlowStations[1].get_water_source(ws_number=1).add_point_of_control_to_water_source(_point_of_control_address=1)
            
            # C2W2->C2P2->C2M2
            self.config.FlowStations[1].get_point_of_control(pc_number=2).add_mainline_to_point_of_control(_mainline_address=2)
            self.config.FlowStations[1].get_water_source(ws_number=2).add_point_of_control_to_water_source(_point_of_control_address=2)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.BaseStation3200[2].verify_full_configuration()
            self.config.FlowStations[1].save_programming_to_flow_station()
            self.config.FlowStations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        ############################
        Configure FlowMeters
        ############################

        - Configure FlowMeters to trigger HF/LF variance with calculated actual flow values
        - FlowMeters 1 will have values to trigger HF variance. 
        - FlowMeters 2 will have values to trigger LF variance

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # --------------------------------------- #
            # Configure High Flow Variance Components #
            # --------------------------------------- #

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _variance_percent=self.ml_1_hi_fl_vr,
                _expected_gpm=self.zn_1_df + self.zn_2_df)

            # -------------------------------------- #
            # Configure Low Flow Variance Components #
            # -------------------------------------- #

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[2].flow_meters[2],
                _variance_percent=self.ml_2_lo_fl_vr,
                _expected_gpm=self.zn_4_df)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        ############################
        Increment clock to start programming
        ############################

        - Set date and time on controller and increment clock to start zones running
        - Increment clock past program's start time of 10am
            - All programs should start
        - Verify initial IDLE system status prior to programming starting (programming will start "AFTER" top of the 
          min)
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ---------------------------------------------------------------------- #
            # Set controllers date/time to 1 minute before program start time (10am) #
            # ---------------------------------------------------------------------- #

            date_mngr.set_current_date_to_match_computer()
            self.config.BaseStation3200[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                             _time='09:59:00')
            self.config.BaseStation3200[2].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                             _time='09:59:00')
            self.config.FlowStations[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                          _time='09:59:00')

            # ------------------------------------------------------------ #
            # Increment controller clock to 10am to trigger program starts #
            # ------------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify system is IDLE until after the top of the minute
            
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_ok()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_ok()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_ok()
            self.config.FlowStations[1].get_water_source(2).statuses.verify_status_is_ok()

            # Controller 1 Program 1
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_off()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_off()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()
                
            for zn_address in self.config.BaseStation3200[1].zones.keys():
                self.config.BaseStation3200[1].zones[zn_address].statuses.verify_status_is_done()

            # Controller 2 Program 2
            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_done()

            # Flow Sources
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_off()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_off()
    
            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_off()
    
            # Flow Devices
            self.config.BaseStation3200[2].flow_meters[2].statuses.verify_status_is_running()
    
            for zn_address in self.config.BaseStation3200[2].zones.keys():
                self.config.BaseStation3200[2].zones[zn_address].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        ############################
        Increment clock from 10:00 - 10:01 AM
        ############################

        Verify programming starts after the top of the minute (10:00:01 am)
            - Everything will go to waiting with the exception of water sources and pocs which will be running.
            - Flow meters will display running status because we set their flow rate in an earlier step to trigger the
              first group of variances.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:01 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify the only water source
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(2).statuses.verify_status_is_running()

            # ------------------------------------------------------ #
            # ADV HIGH FLOW VARIANCE TIER 1 W/ SHUTDOWN              #
            # ------------------------------------------------------ #
            
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()

            # ------------------------------------------------------ #
            # ADV LOW FLOW VARIANCE TIER 4 W/ SHUTDOWN              #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_waiting_to_run()

            # Flow Sources
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[2].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[2].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[6].statuses.verify_status_is_waiting_to_water()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        ############################
        Increment clock from 10:01 - 10:02 AM
        ############################

        Verify that watering starts and valves turn on
            - By this point the FlowStation and 3200 have communicated back and forth with water requests and water
              allocations given
            - 3200 acts on allocations given by the FlowStation by turning on the valves

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:02 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify the only water source
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(2).statuses.verify_status_is_running()

            # ------------------------------------------------------ #
            # ADV HIGH FLOW VARIANCE TIER 1 W/ SHUTDOWN              #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()

            # ------------------------------------------------------ #
            # ADV LOW FLOW VARIANCE TIER 4 W/ SHUTDOWN              #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[2].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[2].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[6].statuses.verify_status_is_waiting_to_water()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_14(self):
        """
        ############################
        Increment clock from 10:02 - 10:03 AM
        ############################

        At this point, the FlowStation has detected variances on mainline 1 (with a variance limit > 0% and have with 1
        min pipe fill times and has sent the variance faults to the 3200.

        Because we are incrementing the FlowStatin before the 3200 (alternating 15 second intervals), our FlowStation
        arrives at the top of the minute before the 3200 which is why the 3200 generates a variance detected message
        but hasn't acted on it yet (by changing statuses).

        Mainline 2 doesn't have a variance detected message b/c it hasn't finished its pipe fill time.

        Mainlines 1 will not have variance detected messages for the mainline because it has shutdown
        enabled on variance.

        Pipe fill configuration:
        - Mainline 1 has a 1 min pipe fill
        - Mainline 2 has a 3 min pipe fill

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:03 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify the only water source
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(2).statuses.verify_status_is_running()

            # ------------------------------------------------------ #
            # ADV HIGH FLOW VARIANCE TIER 1 W/ SHUTDOWN              #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()

            # ------------------------------------------------------ #
            # ADV LOW FLOW VARIANCE TIER 4 W/ SHUTDOWN              #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[2].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[2].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[6].statuses.verify_status_is_waiting_to_water()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_15(self):
        """
        ############################
        Increment clock from 10:03 - 10:04 AM
        ############################

        At this point, the 3200 has acted on the detected variances on mainlines sent by FlowStation by:
            - Gave each active zone (where a variance was detected) a strike
            - Turning on new groups of valves
            - Statuses are updated
            - Requested new water allocation based on the expected flow of the new group of valves turned on.
                (a) If the new group of zones turned on needs more water than what was previously allocated to the
                    mainline, the next group of zones go to waiting until new water allocation is received
                (b) Otherwise, if the new group of zones turned on needs the same amount of water or less, they are
                    turned on immediately and don't need to wait for water allocation requests.

        Mainline 1 experience a flow fault (variance). Since they have shutdown enabled, they strike the
        active zones watering and start a new group of zones.
            - Mainline 1 received its first strike.
            - No need to update their flow meters since the new set of zones has the same water request.

        Mainline 2 doesn't have a variance detected message b/c it hasn't finished its pipe fill time (1 min left).


        Pipe fill configuration:
        - Mainline 1 has a 1 min pipe fill
        - Mainline 2 has a 3 min pipe fill

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:04 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify the only water source
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(2).statuses.verify_status_is_running()

            # ------------------------------------------------------ #
            # ADV HIGH FLOW VARIANCE TIER 1 W/ SHUTDOWN              #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()

            # ------------------------------------------------------ #
            # ADV LOW FLOW VARIANCE TIER 4 W/ SHUTDOWN               #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[2].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[2].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[6].statuses.verify_status_is_waiting_to_water()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_16(self):
        """
        ############################
        Increment clock from 10:04 - 10:05 AM
        ############################

        Only change at this point is that mainline 2 had finished its pipe fill time and had a variance detected.

        Pipe fill configuration:
        - Mainlines 1 has a 1 min pipe fill
        - Mainline 2 has a 3 min pipe fill

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:05 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify the only water source
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(2).statuses.verify_status_is_running()

            # ------------------------------------------------------ #
            # ADV HIGH FLOW VARIANCE TIER 1 W/ SHUTDOWN              #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()

            # ------------------------------------------------------ #
            # ADV LOW FLOW VARIANCE TIER 4 W/ SHUTDOWN              #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[2].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[2].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[6].statuses.verify_status_is_waiting_to_water()


        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_17(self):
        """
        ############################
        Increment clock from 10:05 - 10:06 AM
        ############################

        Mainlines 1 and 2 experience a flow fault (variance). Since they have shutdown enabled, they strike the
        active zones and are set to waiting and start a new group of zones.
            - At this point mainline 1 has two strikes and mainline 2 has one strike.

        Pipe fill configuration:
        - Mainline 1 has a 1 min pipe fill
        - Mainline 2 has a 3 min pipe fill

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:06 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify the only water source
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(2).statuses.verify_status_is_running()

            # ------------------------------------------------------ #
            # ADV HIGH FLOW VARIANCE TIER 1 W/ SHUTDOWN              #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()

            # ------------------------------------------------------ #
            # ADV LOW FLOW VARIANCE TIER 4 W/ SHUTDOWN              #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[2].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[2].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[6].statuses.verify_status_is_waiting_to_water()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_18(self):
        """
        ############################
        Increment clock from 10:06 - 10:07 AM
        ############################

        At this point, the FlowStation has detected variances on mainline 1 and has sent the variance faults to the 3200.

        Because we are incrementing the FlowStatin before the 3200 (alternating 15 second intervals), our FlowStation
        arrives at the top of the minute before the 3200 which is why the 3200 generates a variance shutdown message
        but hasn't acted on it yet (by changing statuses).

        Pipe fill configuration:
        - Mainline 1 has a 1 min pipe fill
        - Mainline 2 has a 3 min pipe fill

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:07 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            # Verify the only water source
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(2).statuses.verify_status_is_running()

            # ------------------------------------------------------ #
            # ADV HIGH FLOW VARIANCE TIER 1 W/ SHUTDOWN              #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            # verify that the high flow variance message was generated.
            self.config.BaseStation3200[1].get_mainline(1).messages.verify_high_flow_variance_shutdown_message()

            # ------------------------------------------------------ #
            # ADV LOW FLOW VARIANCE TIER 4 W/ SHUTDOWN              #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[2].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[2].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[6].statuses.verify_status_is_waiting_to_water()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_19(self):
        """
        ############################
        Increment clock from 10:07 - 10:08 AM
        ############################

        Mainlines 1 has gone to error because it has received three consecutive flow faults. Mainline 1 zones are set to
        done.

        Flowmeter upstream of mainline 1 is set to zero since the mainline has been shutdown.

        Mainline 2 has not had a variance since it has not finished its fill time. Statuses will remain the same.

        Pipe fill configuration:
        - Mainline 1 has a 1 min pipe fill
        - Mainline 2 has a 3 min pipe fill

        This verifies our mainline 3 strike rule with advanced high flow variance shutdown enabled. We previously verified
        the message and this step we verify its zones are done and mainline went to error.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:08 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            helper_methods.update_flow_to_not_trigger_variance(_flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                                                               _expected_gpm=0)

            # Verify the only water source
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_ok()
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_ok()
            self.config.FlowStations[1].get_water_source(2).statuses.verify_status_is_running()

            # ------------------------------------------------------ #
            # ADV HIGH FLOW VARIANCE TIER 1 W/ SHUTDOWN              #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_error()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_error()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_ok()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()

            # ------------------------------------------------------ #
            # ADV LOW FLOW VARIANCE TIER 4 W/ SHUTDOWN              #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[2].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[2].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[6].statuses.verify_status_is_waiting_to_water()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_20(self):
        """
        ############################
        Increment clock from 10:08 - 10:09 AM
        ############################

        Mainline 1 is assumed to be off along with its water source, poc, and program which are all off.

        Only change at this point is that mainline 2 had finished its pipe fill time and had a variance detected.
           - Mainline 2 now has 2 strikes.

        Pipe fill configuration:
        - Mainline 2 has a 3 min pipe fill

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:09 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify the only water source
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(2).statuses.verify_status_is_running()

            # ------------------------------------------------------ #
            # ADV LOW FLOW VARIANCE TIER 4 W/ SHUTDOWN              #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[2].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[2].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[2].zones[6].statuses.verify_status_is_waiting_to_water()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_21(self):
        """
        ############################
        Increment clock from 10:09 - 10:10 AM
        ############################

        Mainline 2 experienced a flow fault so its active zones went to waiting and a new set started.

        We are not updating the flow meter because it is already set at a lower value than the flow of the active zones.

        Pipe fill configuration:
        - Mainline 2 has a 3 min pipe fill

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:10 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify the only water source
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(2).statuses.verify_status_is_running()

            # ------------------------------------------------------ #
            # ADV LOW FLOW VARIANCE TIER 4 W/ SHUTDOWN              #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[2].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[2].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[6].statuses.verify_status_is_watering()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_22(self):
        """
        ############################
        Increment clock from 10:10 - 10:11 AM
        ############################

        No expected flow faults, statuses remain the same because mainline 2 has not finished its pipe fill time.

        Pipe fill configuration:
        - Mainline 2 has a 3 min pipe fill

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:11 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify the only water source
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(2).statuses.verify_status_is_running()

            # ------------------------------------------------------ #
            # ADV LOW FLOW VARIANCE TIER 4 W/ SHUTDOWN              #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[2].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[2].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[6].statuses.verify_status_is_watering()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_23(self):
        """
        ############################
        Increment clock from 10:11 - 10:12 AM
        ############################

        No expected flow faults, statuses remain the same because mainline 2 has not finished its pipe fill time.

        Pipe fill configuration:
        - Mainline 2 has a 3 min pipe fill

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:12 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            # Verify the only water source
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(2).statuses.verify_status_is_running()

            # ------------------------------------------------------ #
            # ADV LOW FLOW VARIANCE TIER 4 W/ SHUTDOWN              #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[2].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[2].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[6].statuses.verify_status_is_watering()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_24(self):
        """
        ############################
        Increment clock from 10:12 - 10:13 AM
        ############################

        Only change at this point is that mainline 2 had finished its pipe fill time and had a variance detected.
           - Mainline 2 now has 3 strikes. The 3200 creates a message for advanced low flow variance shutdown.
           - The 3200 will act on the variance and update statuses during the next minute.

        Pipe fill configuration:
        - Mainline 2 has a 3 min pipe fill

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:13 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            # Verify the only water source
            self.config.BaseStation3200[2].get_water_source(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(2).statuses.verify_status_is_running()

            # ------------------------------------------------------ #
            # ADV LOW FLOW VARIANCE TIER 4 W/ SHUTDOWN              #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[2].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[2].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[2].zones[6].statuses.verify_status_is_watering()

            # verify that the low flow variance shutdown message was generated.
            self.config.BaseStation3200[2].get_mainline(2).messages.verify_low_flow_variance_shutdown_message()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_25(self):
        """
        ############################
        Increment clock from 10:13 - 10:14 AM
        ############################

        Mainlines 2 has gone to error because it has received three consecutive flow faults. Mainline 2 zones are set to
        done.

        Flowmeter upstream of mainline 2 is set to zero since the mainline has been shutdown.

        This verifies our mainline 3 strike rule with advanced low flow variance shutdown enabled. We previously verified
        the message and this step we verify its zones are done and mainline went to error.

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:14 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[2].flow_meters[2],
                _expected_gpm=0)

            # Controller 2 Program 2
            self.config.BaseStation3200[2].programs[2].statuses.verify_status_is_done()

            # Flow Sources
            self.config.BaseStation3200[2].get_point_of_control(2).statuses.verify_status_is_off()
            self.config.BaseStation3200[2].get_mainline(2).statuses.verify_status_is_error()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_error()

            # Flow Devices
            self.config.BaseStation3200[2].flow_meters[2].statuses.verify_status_is_ok()

            for zn_address in self.config.BaseStation3200[2].zones.keys():
                self.config.BaseStation3200[2].zones[zn_address].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

