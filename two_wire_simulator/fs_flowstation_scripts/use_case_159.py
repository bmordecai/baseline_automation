import sys
from time import sleep

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

# Browser pages used
# import page_factory

__author__ = 'Eldin'


class ControllerUseCase159(object):
    """
    Test name:
        - CN UseCase159 Primary/Linked on Split Mainlines

    User Story: \n
        1)  As a user I want to be able get water to my zone from any pipe I want, regardless of any previous
            programming I may have put on the zone or the program it resides on.

    Coverage and Objectives:
        1.	When we have a zone that is linked to a primary zone, but the primary and linked are on different mainlines,
            we can get into a situation where the primary zone can't run because water was allocated to the lowest
            numbered mainline, but the primary zone is on a higher number. So when the lower numbered mainline tries to
            run, it cannot because it is trying to run a linked zone that is waiting on the primary zone on the higher
            numbered mainline to run. This causes an INFINITE WAIT.

    Not Covered:
        1.	Flow Variance
        2.	Over Budget Shutdowns
        3.	Multiple 3200s managed by FlowStation
        4.	Multiple points of control into a single mainline
        5.	High flows & shutdowns
        6.	Unscheduled flows & shutdowns
        7.	Mainline Standard variances & shutdowns
        8.	Empty conditions
        9.	Manual runs with flow variance (if manual running, ALL flow variance is ignored)
        10.	When a flow meter has a IO error or is non-responsive, ALL flow variance is ignored
        11.	Learn flows with flow variance (if doing a learn flow, ALL flow variance is ignored)
        12. Shutting down a zone other than the first
        13. Nested mainlines on FlowStation

    Test Overview:
        - 4 Zones
            - 1 Program with all Zones on it
                - ZN 91: Primary Zone (Mainline 3)
                - ZN 41: Linked to ZN 91 (Mainline 5)
                - ZN 13: Linked to ZN 91 (Mainline 1)
                - ZN 14: Linked to ZN 91 (Mainline 1)

        - FlowStation assignments
                                C1:W1
                                  |
                                C1:P1
                                  |
                            -------------
                            |     |     |
                         C1:M1  C1:M3  C1:M5
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/fs_json_config_files'
                                    )
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        """

        program_start_time_8am = [480]
        program_water_windows = ['111111111111111111111111']
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation3200[1].programs[1].set_water_window(_ww=program_water_windows)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(_sun=True,
                                                                                                           _mon=True,
                                                                                                           _tues=True,
                                                                                                           _wed=True,
                                                                                                           _thurs=True,
                                                                                                           _fri=True,
                                                                                                           _sat=True)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_start_time_8am)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Make Zone 91 the primary zone
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=91)
            self.config.BaseStation3200[1].programs[1].zone_programs[91].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[91].set_run_time(_minutes=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[91].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[91].add_moisture_sensor_to_primary_zone(
                _moisture_sensor_address=1)

            # Link the rest of the zones to the primary
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=41)
            self.config.BaseStation3200[1].programs[1].zone_programs[41].set_as_linked_zone(_primary_zone=91)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=13)
            self.config.BaseStation3200[1].programs[1].zone_programs[13].set_as_linked_zone(_primary_zone=91)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=14)
            self.config.BaseStation3200[1].programs[1].zone_programs[14].set_as_linked_zone(_primary_zone=91)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        setup WaterSources
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Water Source 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        setup Point of Controls
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # Add & Configure POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=100)
            self.config.BaseStation3200[1].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_flow_meter_to_point_of_control(
                _flow_meter_address=1)

            # Add POC 1 to Water Source 1
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ##################
        setup mainlines
        ##################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # The mainline for Zones 13/14 (linked)
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_enabled()
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=100)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_true()

            # The mainline for Zone 91 (primary)
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=3)
            self.config.BaseStation3200[1].mainlines[1].set_enabled()
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=100)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_true()

            # The mainline for Zone 41 (linked)
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=5)
            self.config.BaseStation3200[1].mainlines[1].set_enabled()
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=100)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_true()

            # Add ML 1 to POC 1
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ######################
        setup zones on mainlines
        ######################

        Program | Mainline  | Zones
        ---------------------------------------------
        1       | 1         | 13, 14
        1       | 3         | 91
        1       | 5         | 41
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Add zones to the correct mainlines, make sure the primary is on a mainline of its own
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=13)
            self.config.BaseStation3200[1].zones[13].set_design_flow(_gallons_per_minute=50)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=14)
            self.config.BaseStation3200[1].zones[14].set_design_flow(_gallons_per_minute=50)
            self.config.BaseStation3200[1].mainlines[3].add_zone_to_mainline(_zone_address=91)
            self.config.BaseStation3200[1].zones[91].set_design_flow(_gallons_per_minute=100)
            self.config.BaseStation3200[1].mainlines[5].add_zone_to_mainline(_zone_address=41)
            self.config.BaseStation3200[1].zones[41].set_design_flow(_gallons_per_minute=100)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ###############################
        verify the entire configuration on the 3200 \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_8(self):
        """
        ###############################
        Share controller, water source, point of control, mainline with Flow Station
        ###############################
        - Add the controller to the flow station
        - Share Water Source between the 3200 controller and the FlowStation
        - Share Point of Control between the 3200 controller and the FlowStation
        - Share Mainline 1/3/5 between the 3200 controller and the FlowStation
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            self.config.FlowStations[1].add_controller_to_flow_station(
                _controller_address=1,
                _flow_station_slot_number=1)

            # ******************************************* WATER SOURCE *********************************************** #
            # Add Water source on the Controller to the FlowStation
            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=1,
                _controller_water_source_address=1,
                _flow_station_water_source_slot_number=1)

            # Tell the 3200 that the FlowStation is in control of Water Source 1.
            self.config.BaseStation3200[1].get_water_source(1).set_manage_by_flowstation()

            # ******************************************* CONTROL POINT ********************************************** #
            # Add Point of Control on the Controller to the FlowStation
            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=1,
                _controller_point_of_control_address=1,
                _flow_station_point_of_control_slot_number=1)

            # Tell the 3200 that the FlowStation is in control of Point of Control 1.
            self.config.BaseStation3200[1].get_point_of_control(1).set_manage_by_flowstation()

            # ********************************************** Mainline ************************************************ #
            # add Mainline on the Controller to the FlowStation
            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=1,
                _controller_mainline_address=1,
                _flow_station_mainline_slot_number=1)

            # Tell the 3200 that the FlowStation is in control of Mainline 1.
            self.config.BaseStation3200[1].get_mainline(1).set_manage_by_flowstation()

            # add Mainline on the Controller to the FlowStation
            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=1,
                _controller_mainline_address=3,
                _flow_station_mainline_slot_number=3)

            # Tell the 3200 that the FlowStation is in control of Mainline 1.
            self.config.BaseStation3200[1].get_mainline(3).set_manage_by_flowstation()

            # add Mainline on the Controller to the FlowStation
            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=1,
                _controller_mainline_address=5,
                _flow_station_mainline_slot_number=5)

            # Tell the 3200 that the FlowStation is in control of Mainline 1.
            self.config.BaseStation3200[1].get_mainline(5).set_manage_by_flowstation()

            # Make the assignments for the shared objects on the FlowStation
            self.config.FlowStations[1].get_water_source(ws_number=1).add_point_of_control_to_water_source(
                _point_of_control_address=1)
            self.config.FlowStations[1].get_point_of_control(pc_number=1).add_mainline_to_point_of_control(
                _mainline_address=1)
            self.config.FlowStations[1].get_mainline(ml_number=1).add_mainline_to_mainline(
                _mainline_address=3)
            self.config.FlowStations[1].get_mainline(ml_number=1).add_mainline_to_mainline(
                _mainline_address=5)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_9(self):
        """
        ###############################
        verify the entire configuration through the Flow Station Serial Port \n
        ###############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.FlowStations[1].do_increment_clock(minutes=1)
            self.config.FlowStations[1].save_programming_to_flow_station()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.FlowStations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_10(self):
        """
        #####################
        Verify Starting State
        #####################
        - Nothing is watering or running because the start time of 8:00 AM hasn't been hit yet, everything should be in
          a default state.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            date_mngr.set_current_date_to_match_computer()
            self.config.BaseStation3200[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                             _time='07:58:00')
            self.config.FlowStations[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                          _time='07:58:00')

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_ok()

            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_ok()

            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(5).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(5).statuses.verify_status_is_off()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[91].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[41].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_11(self):
        """
        ################################
        Verify Statuses After Start Time
        ################################
        - The Program 1 Start time on the 3200 has been triggered and will now request water from the FlowStation.
        - The FlowStation will take a minute to do this allocation, and in the mean time the zones should be waiting.
        - The WS/POC/ML will be running because the FlowStation knows water will be going through there, but the zones
          should still not be running because the water isn't allocated to them.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # *********************************************** 8:01 AM ************************************************ #
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=2)

            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(5).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(5).statuses.verify_status_is_off()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].zones[91].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[41].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_waiting_to_water()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_12(self):
        """
        ##############################################
        Verify Statuses After Water Has Been Allocated
        ##############################################
        - The FlowStation has now finished it's allocation and the zones are now able to run.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # ********************************************* 8:02-8:12 AM ********************************************* #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # 8:02AM
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(5).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(5).statuses.verify_status_is_off()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[91].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[41].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_waiting_to_water()

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # 8:03AM
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(5).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(5).statuses.verify_status_is_off()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[91].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[41].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_waiting_to_water()

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # 8:04AM
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(5).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(5).statuses.verify_status_is_off()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[91].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[41].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_waiting_to_water()

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # 8:05AM
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(5).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(5).statuses.verify_status_is_off()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[91].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[41].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_waiting_to_water()

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # 8:06AM
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(5).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(5).statuses.verify_status_is_off()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[91].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[41].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_waiting_to_water()

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # 8:07AM
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(5).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(5).statuses.verify_status_is_off()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[91].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[41].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_watering()

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # 8:08AM
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(5).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(5).statuses.verify_status_is_off()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[91].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[41].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_watering()

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # 8:09AM
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(5).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(5).statuses.verify_status_is_off()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[91].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[41].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_watering()

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # 8:10AM
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(5).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(5).statuses.verify_status_is_off()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[91].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[41].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_watering()

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # 8:11AM
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(5).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(5).statuses.verify_status_is_off()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[91].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[41].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_watering()

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # 8:12AM
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(5).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(5).statuses.verify_status_is_off()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].zones[91].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[41].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
