import sys

from datetime import time, timedelta
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
from common.configuration import Configuration

# import log_handler for logging functionality
from common.logging_handler import log_handler

# Objects
from common.objects.base_classes.web_driver import *
from common.imports import opcodes

__author__ = 'Eldin'


class ControllerUseCase157(object):
    """
    Test name:
        - CN UseCase157 backup and restore on a 3200 connected to a FlowStation
    purpose:
        - This Use Case is a conversion of Use Case 12. The purpose of this is to test and verify that
            the functionality remains the same with a FlowStation as it did just with a single controller.
        - Set up a full configuration on the controller
            - At least one of each device

        - Connect controller to the FlowStation and verify the following:
            - Can do a backup from the FlowStation and 3200 to BaseManager
            - Can do a restore programming on the FlowStation and 3200 from BaseManager after having cleared programming
                - Verify the programming stays the same after the restore
            - Can do a backup from the FlowStation and 3200 to USB
            - Can do a restore programming on the FlowStation and 3200 from USB after having cleared programming
                - Verify the programming stays the same after the restore

    Coverage area: \n
        1. Setting up the 3200 \n
            - Search and assign at least one of each device on the 3200 \n
                - For specific serial numbers/addresses look at FS_back_restore_firmware_updates.json. \n
            - Set up the Programs \n
            - Assign each zone to a program \n
            - Setting up water sources \n
                - Add an empty condition to one \n
            - Setting up Points of Control \n
            - Setting up Mainlines \n
                - Add each zone to a mainline \n
            - Disable arbitrary devices just to verify that disabled devices get transferred over. \n
            - Verify the full configuration \n
        2. Share the controller, mainline, water source, and point of control with a flow station \n

                - Flow Station Setup:

            Controller             FlowStation
            - WS1                   - WS1
            - POC1                  - POC1
            - ML1                   - ML1
            - WS8                   - WS2
            - POC8                  - POC2
            - ML8                   - ML2

            - Flow Station Connectivity
            - WS1 -> POC1 -> ML1
            - WS2 -> POC2 -> ML2

        3. Run Programs with a design flow restriction on program 1 with Limit Concurrent active \n
        4. Run Programs with two design flow one on each program, one with Limit Concurrent active and one without \n
        5. Run Programs with two design flow one on each program, both with Limit Concurrent active \n
        6. Verify the full configuration again
        """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/fs_json_config_files'
                                    )
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """
        time_it_should_take_test_to_run = timedelta(seconds=52, minutes=30)  # this is minutes
        time_test_started = datetime.now()
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=False)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.test_elapsed_time(_time_started=time_test_started,
                                                     _time_expected_to_run=time_it_should_take_test_to_run)
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
            - Set max concurrent zones for the program \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            program_number_1_start_times = [480, 540, 600, 660]
            program_number_3_start_times = [480, 540, 600, 660]
            program_number_4_start_times = [480, 540, 600, 660]
            program_number_99_start_times = [480, 540, 600, 660]
            program_number_99_watering_days = [0, 0, 0, 0, 0, 0, 8, 8, 6, 5, 5, 4, 3, 3, 3, 3, 4, 5, 6, 7, 0, 0, 0, 0]
            program_number_1_water_windows = ['011111100001111111111110']
            program_number_3_water_windows = ['011111100001111111111110']
            program_number_4_water_windows = ['011111100001111111111110']
            program_number_99_water_windows = ['011111100000111111111110',
                                               '011111100001111111111111',
                                               '011111100001111111111110',
                                               '011111100001111111111110',
                                               '011111100001111111111110',
                                               '011111100001111111111110',
                                               '011111100001111111111111']
            # Setup Program 1
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[1].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[1].set_water_window(_ww=program_number_1_water_windows)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(_sun=False,
                                                                                                           _mon=True,
                                                                                                           _tues=False,
                                                                                                           _wed=True,
                                                                                                           _thurs=False,
                                                                                                           _fri=True,
                                                                                                           _sat=False)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_number_1_start_times)

            # Setup Program 3
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=3)
            self.config.BaseStation3200[1].programs[3].set_enabled()
            self.config.BaseStation3200[1].programs[3].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[3].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[3].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[3].set_water_window(_ww=program_number_3_water_windows)
            self.config.BaseStation3200[1].programs[3].set_watering_intervals_to_odd_days()
            self.config.BaseStation3200[1].programs[3].set_start_times(_st_list=program_number_3_start_times)

            # Setup Program 4
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=4)
            self.config.BaseStation3200[1].programs[4].set_enabled()
            self.config.BaseStation3200[1].programs[4].set_priority_level(_pr_level=3)
            self.config.BaseStation3200[1].programs[4].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation3200[1].programs[4].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[4].set_water_window(_ww=program_number_4_water_windows)
            self.config.BaseStation3200[1].programs[4].set_watering_intervals_to_selected_days_of_the_week(_sun=False,
                                                                                                           _mon=True,
                                                                                                           _tues=False,
                                                                                                           _wed=True,
                                                                                                           _thurs=False,
                                                                                                           _fri=True,
                                                                                                           _sat=False)
            self.config.BaseStation3200[1].programs[4].set_start_times(_st_list=program_number_4_start_times)

            # Setup Program 99
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=99)
            self.config.BaseStation3200[1].programs[99].set_enabled()
            self.config.BaseStation3200[1].programs[99].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[99].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[99].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[99]. \
                set_water_window(_ww=program_number_99_water_windows, _is_weekly=False)
            self.config.BaseStation3200[1].programs[99]. \
                set_watering_intervals_to_semi_monthly(_sm=program_number_99_watering_days)
            self.config.BaseStation3200[1].programs[99].set_start_times(_st_list=program_number_99_start_times)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type (Timed, Primary, Linked) \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            - set tracking ration if the zone is linked \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add zone 1 to program 1
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=5)

            # Add zone 2 to program 1
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1)

            # Add zone 49 to program 3
            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=49)
            self.config.BaseStation3200[1].programs[3].zone_programs[49].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[3].zone_programs[49].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[3].zone_programs[49].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[3].zone_programs[49].set_soak_time(_minutes=60)

            # Add zone 50 to program 3
            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=50)
            self.config.BaseStation3200[1].programs[3].zone_programs[50].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[3].zone_programs[50].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[3].zone_programs[50].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[3].zone_programs[50].set_soak_time(_minutes=60)

            # Add zone 50 to program 4
            self.config.BaseStation3200[1].programs[4].add_zone_to_program(_zone_address=50)
            self.config.BaseStation3200[1].programs[4].zone_programs[50].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[4].zone_programs[50].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[4].zone_programs[50].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[4].zone_programs[50].set_soak_time(_minutes=60)

            # Add zone 200 to program 99
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=200)
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_run_time(_minutes=33)
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_cycle_time(_minutes=3)
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_soak_time(_minutes=13)

            # Program 99 Zone Programs (197, 198, 199) linked to Zone Program 200
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=197)
            self.config.BaseStation3200[1].programs[99].zone_programs[197].set_as_linked_zone(_primary_zone=200,
                                                                                              _tracking_ratio=50)

            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=198)
            self.config.BaseStation3200[1].programs[99].zone_programs[198].set_as_linked_zone(_primary_zone=200)

            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=199)
            self.config.BaseStation3200[1].programs[99].zone_programs[199].set_as_linked_zone(_primary_zone=200,
                                                                                              _tracking_ratio=150)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_3(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
        - Add a water source empty condition. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Water Source 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].set_priority(_priority_for_water_source=1)
            self.config.BaseStation3200[1].water_sources[1].set_monthly_watering_budget(_budget=10000)
            self.config.BaseStation3200[1].water_sources[1].set_water_rationing_to_disabled()

            # Water Source 8
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=8)
            self.config.BaseStation3200[1].water_sources[8].set_enabled()
            self.config.BaseStation3200[1].water_sources[8].set_priority(_priority_for_water_source=1)
            self.config.BaseStation3200[1].water_sources[8].set_monthly_watering_budget(_budget=10000)
            self.config.BaseStation3200[1].water_sources[8].set_water_rationing_to_disabled()

            # Water Source 8 empty condition
            self.config.BaseStation3200[1].water_sources[8].add_switch_empty_condition(_event_switch_address=1)
            self.config.BaseStation3200[1].water_sources[8].switch_empty_conditions[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[8].switch_empty_conditions[1]. \
                set_switch_empty_condition_to_closed()
            self.config.BaseStation3200[1].water_sources[8].switch_empty_conditions[1].set_empty_wait_time(_minutes=8)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1]. \
                add_master_valve_to_point_of_control(_master_valve_address=1)
            self.config.BaseStation3200[1].points_of_control[1]. \
                add_flow_meter_to_point_of_control(_flow_meter_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].points_of_control[1].set_high_flow_limit(_limit=550,
                                                                                    with_shutdown_enabled=True)
            self.config.BaseStation3200[1].points_of_control[1].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=True)
            # Add POC 1 to Water Source 1
            self.config.BaseStation3200[1].water_sources[1] \
                .add_point_of_control_to_water_source(_point_of_control_address=1)

            # Add & Configure POC 8
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=8)
            self.config.BaseStation3200[1].points_of_control[8].set_enabled()
            self.config.BaseStation3200[1].points_of_control[8]. \
                add_master_valve_to_point_of_control(_master_valve_address=1)
            self.config.BaseStation3200[1].points_of_control[8]. \
                add_flow_meter_to_point_of_control(_flow_meter_address=1)
            self.config.BaseStation3200[1].points_of_control[8].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].points_of_control[8].set_high_flow_limit(_limit=75,
                                                                                    with_shutdown_enabled=False)
            self.config.BaseStation3200[1].points_of_control[8].set_unscheduled_flow_limit(_gallons=5,
                                                                                           with_shutdown_enabled=False)
            # Add POC 8 to Water Source 8
            self.config.BaseStation3200[1].water_sources[8]. \
                add_point_of_control_to_water_source(_point_of_control_address=8)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure ML 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=4)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[1].set_high_flow_variance_tier_one(_percent=5,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].mainlines[1].set_low_flow_variance_tier_one(_percent=20,
                                                                                       _with_shutdown_enabled=True)
            # Add ML 1 to POC 1
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)
            # Add & Configure ML 8
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=8)
            self.config.BaseStation3200[1].mainlines[8].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[8].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].mainlines[8].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[8].set_high_flow_variance_tier_one(_percent=20,
                                                                                        _with_shutdown_enabled=False)
            self.config.BaseStation3200[1].mainlines[8].set_high_flow_variance_tier_one(_percent=5,
                                                                                        _with_shutdown_enabled=False)
            # Add ML 8 to POC 8
            self.config.BaseStation3200[1].points_of_control[8].add_mainline_to_point_of_control(_mainline_address=8)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone
        Program | Mainline  | Zones
        ---------------------------------------------
        1       | 1         | 1,2,3,4,5
        3       | 1         | 1,2,3,4,5
        4       | 1         | 49, 50
        99      | 8         | 197, 198, 199, 200
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Add ZN 1,2, 49, 50 to ML 1
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=1)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=2)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=49)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=50)

            # Add ZN 197-200 to ML 8
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=197)
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=198)
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=199)
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=200)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        Disable devices
        This area covers not losing programing when a device is disabled during a firmware update or reboot
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # disable Zone (198}
            self.config.BaseStation3200[1].zones[198].set_disabled()

            # disable flow meter {3}
            self.config.BaseStation3200[1].flow_meters[1].set_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.FlowStations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_9(self):
        """
        ###############################
        Share controller, water source, point of control, mainline with Flow Station
        ###############################
            - Share controller 1 with FlowStation 1

            - Share Water Source 1 on controller 1 with FlowStation 1
            - Share Point of Control 1 on controller 1 with FlowStation 1
            - Share Mainline 1 on controller 1 with FlowStation 1

            - Share Water Source 8 on controller 1 with FlowStation 1
            - Share Point of Control 8 on controller 1 with FlowStation 1
            - Share Mainline 8 on controller 1 with FlowStation 1


            - Assign WS, POC, and ML on the FlowStation
            - WS1 -> PC1 -> ML1
            - WS2 -> PC2 -> ML2
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add controller 1 to FlowStation
            self.config.FlowStations[1].add_controller_to_flow_station(
                _controller_address=1,
                _flow_station_slot_number=1)

            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=1,
                _controller_water_source_address=1,
                _flow_station_water_source_slot_number=1)

            # Tell the 3200 that the FlowStation is in control of Water Source 1.
            self.config.BaseStation3200[1].get_water_source(1).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=1,
                _controller_point_of_control_address=1,
                _flow_station_point_of_control_slot_number=1)

            # Tell the 3200 that the FlowStation is in control of Point of Control 1.
            self.config.BaseStation3200[1].get_point_of_control(1).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=1,
                _controller_mainline_address=1,
                _flow_station_mainline_slot_number=1)

            # Tell the 3200 that the FlowStation is in control of Mainline 1.
            self.config.BaseStation3200[1].get_mainline(1).set_manage_by_flowstation()

            # Configure second set of WS, POC, ML
            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=1,
                _controller_water_source_address=8,
                _flow_station_water_source_slot_number=2)

            # Tell the 3200 that the FlowStation is in control of Water Source 2.
            self.config.BaseStation3200[1].get_water_source(8).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=1,
                _controller_point_of_control_address=8,
                _flow_station_point_of_control_slot_number=2)

            # Tell the 3200 to have the FlowStation manage Point of Control 2.
            self.config.BaseStation3200[1].get_point_of_control(8).set_manage_by_flowstation()

            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=1,
                _controller_mainline_address=8,
                _flow_station_mainline_slot_number=2)

            # Tell the 3200 to have the FlowStation manage Mainline 2.
            self.config.BaseStation3200[1].get_mainline(8).set_manage_by_flowstation()

            # Assign WS, POC, and ML on FlowStation
            # WS1 -> PC1 -> ML1
            self.config.FlowStations[1].get_point_of_control(pc_number=1).add_mainline_to_point_of_control(_mainline_address=1)
            self.config.FlowStations[1].get_water_source(ws_number=1).add_point_of_control_to_water_source(_point_of_control_address=1)

            # WS2 -> PC2 -> ML2
            self.config.FlowStations[1].get_point_of_control(pc_number=2).add_mainline_to_point_of_control(_mainline_address=2)
            self.config.FlowStations[1].get_water_source(ws_number=2).add_point_of_control_to_water_source(_point_of_control_address=2)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        Send backup to BaseManager. This takes all of our programming and creates a backup on BaseManager that we will
        later use in the test to do a restore.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # start clock
            self.config.BaseStation3200[1].start_clock()
            self.config.FlowStations[1].start_clock()
            time.sleep(5)
            self.config.BaseStation3200[1].do_backup_programming(where_to=opcodes.basemanager)
            self.config.FlowStations[1].do_backup_programming(where_to=opcodes.basemanager)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        Clears all the programming from the controller so we can do a restore from a clean state. \n
        Wait for the 3200 and FS to reconnect to BaseManager. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # clear all programming
            self.config.BaseStation3200[1].reinitialize_controller()
            self.config.FlowStations[1].reinitialize_controller()
            # have to reconnect to BMW before we can do the restore programming
            self.config.BaseStation3200[1].basemanager_connection[1].wait_for_bm_connection()
            self.config.FlowStations[1].basemanager_connection[1].wait_for_bm_connection()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        We restore our programming from BaseManager. It should restore from the most recent backup.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_restore_programming(opcodes.basemanager)
            self.config.FlowStations[1].do_restore_programming(opcodes.basemanager)

            for zone in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone].bicoder.self_test_and_update_object_attributes()

            for moisture_sensor in sorted(self.config.BaseStation3200[1].moisture_sensors.keys()):
                self.config.BaseStation3200[1].moisture_sensors[moisture_sensor].bicoder. \
                    self_test_and_update_object_attributes()

            for master_valve in sorted(self.config.BaseStation3200[1].master_valves.keys()):
                self.config.BaseStation3200[1].master_valves[master_valve].bicoder. \
                    self_test_and_update_object_attributes()

            for flow_meter in sorted(self.config.BaseStation3200[1].flow_meters.keys()):
                self.config.BaseStation3200[1].flow_meters[flow_meter].bicoder. \
                    self_test_and_update_object_attributes()

            for event_switch in sorted(self.config.BaseStation3200[1].event_switches.keys()):
                self.config.BaseStation3200[1].event_switches[event_switch].bicoder. \
                    self_test_and_update_object_attributes()

            for temperature_sensor in sorted(self.config.BaseStation3200[1].temperature_sensors.keys()):
                self.config.BaseStation3200[1].temperature_sensors[temperature_sensor].bicoder. \
                    self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.FlowStations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_14(self):
        """
        The same thing as before but this time we do the backup to a thumb drive. We will use this backup to do a
        restore later on.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # start clock
            self.config.BaseStation3200[1].start_clock()
            self.config.FlowStations[1].start_clock()
            time.sleep(5)
            self.config.BaseStation3200[1].do_backup_programming(where_to=opcodes.usb_flash_storage)
            self.config.FlowStations[1].do_backup_programming(where_to=opcodes.usb_flash_storage)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_15(self):
        """
        Clears all the programming from the controller so we can do a restore from a clean state.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # clear all programming
            self.config.BaseStation3200[1].reinitialize_controller()
            self.config.FlowStations[1].reinitialize_controller()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_16(self):
        """
        Clear the controller of all of its programming and devices.
        We then restore the programming using a backup from the thumb drive.
        Then verify that all the values that were left in our objects are now back on the controller.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # restore the programming to the controller
            self.config.BaseStation3200[1].do_restore_programming(opcodes.usb_flash_storage, file_number=1)
            self.config.FlowStations[1].do_restore_programming(opcodes.usb_flash_storage, file_number=1)

            for zone in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone].bicoder.self_test_and_update_object_attributes()

            for moisture_sensor in sorted(self.config.BaseStation3200[1].moisture_sensors.keys()):
                self.config.BaseStation3200[1].moisture_sensors[moisture_sensor].bicoder. \
                    self_test_and_update_object_attributes()

            for master_valve in sorted(self.config.BaseStation3200[1].master_valves.keys()):
                self.config.BaseStation3200[1].master_valves[master_valve].bicoder. \
                    self_test_and_update_object_attributes()

            for flow_meter in sorted(self.config.BaseStation3200[1].flow_meters.keys()):
                self.config.BaseStation3200[1].flow_meters[flow_meter].bicoder. \
                    self_test_and_update_object_attributes()

            for event_switch in sorted(self.config.BaseStation3200[1].event_switches.keys()):
                self.config.BaseStation3200[1].event_switches[event_switch].bicoder. \
                    self_test_and_update_object_attributes()

            for temperature_sensor in sorted(self.config.BaseStation3200[1].temperature_sensors.keys()):
                self.config.BaseStation3200[1].temperature_sensors[temperature_sensor].bicoder. \
                    self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_17(self):
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)
            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.FlowStations[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]