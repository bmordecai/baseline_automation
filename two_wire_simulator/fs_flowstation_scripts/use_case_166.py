import sys

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr

# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes

# import log_handler for logging functionality
from common.logging_handler import log_handler

from csv_handler import CSVWriter

from common import helper_methods

# Browser pages used
# import page_factory

__author__ = 'Dillon'


class ControllerUseCase166(object):
    """
    Test name:
        - CN UseCase166 Test Learn Flow with offline controller

    Purpose:
        - This test will check our learn flow on a system with multiple water paths. It will then disconnect
          a controller and attempt to learn flow again. The system should learn flow even with an offline controller,
          since there will be another available water path.

    User Story:
        1) As a user if my irrigation system has multiple water paths that can be used to water zones, and one of my
           controllers in an available water path goes offline. Then my FlowStation should still learn flow using one of
           the alternate water paths.


    Coverage Area:
        1. Configure program one for controller one

        2. Setup zones
            - Add 1 Primary and 2 Linked zones on controller 1, Zn = 1-3
            - Add 1 Primary and 2 Linked zones on controller 1, Zn = 4-6

        3. Setup Water Sources
            - Add two water sources to controller 1, C1WS1 & C1WS2
            - Add a water source to controller 2, C2WS3

        4. Setup Points of Control
            - Add a POC on controller 1 with a Master Valve and Flow Meter. C1P1
            - Add a POC on controller 2 with a Master Valve and Flow Meter. C2P1
            - Add a POC on controller 1 with a Master Valve and Flow Meter. C1P2
            - Add a POC on controller 2 with a Master Valve and Flow Meter. C2P2

        5. Setup Mainlines
            - Add a Mainline on controller 1. C1M1
            - Add a Mainline on controller 1. C1M2

        6. Share Controllers and Flow Objects with the FlowStation
            - Share controller 1 with the FlowStation
                - Share a Water Source on controller 1 with the FlowStation
                - Share a Point of Control on controller 1 with the FlowStation
                - Share a Mainlines on controller 1 with the FlowStation
            - Share controller 2 with the FlowStation
                - Share a Water Source on controller 2 with the FlowStation
                - Share a Point of Control on controller 2 with the FlowStation

        7. Setup zone one mainlines
            - add zones (1-3) to a mainline on controller 1. C1M1 --> Zn1-3
            - add zones (4-6) to a mainline on controller 1. C1M2 --> Zn4-6

        8. Set design flow on all added zones on controller 1
            - zones (1-3) on controller 1 have a design flow of 50 gpm
            - zones (4-6) on controller 1 have a design flow of 100 gpm

        9. System Layout:
             C1WS1          C1WS2       C2WS3
             /   \            |           |
           C1P1  C2P1       C1P2        C2P2
             \   /             \       /
             C1M1                C1M2
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase166' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/fs_json_config_files'
                                    )
        # initializes the csv writer object so it can be used in the remainder of the use_case
        self.csv_writer = CSVWriter(file_name=test_name,
                                    relative_path='fs_flowstation_scripts',
                                    delimiter=',',
                                    line_terminator='\n')
        try:
            self.csv_writer.open()
        except IOError:
            e_msg = self.csv_writer.fname + ' file is in use'
            raise IOError(e_msg)

        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=False)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        This step is setting up our program, which will be driving this test. We have set a start time as well as the
        days to allow watering.
        """
        program_start_times = [600]  # 10:00am start time
        program_water_windows = ['111111111111111111111111']
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set max zones on the controller to 6 so that it runs all zones
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=6)

            # Add and configure Program 1 to controller 1
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_water_window(_ww=program_water_windows)
            self.config.BaseStation3200[1].programs[1].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=6)
            self.config.BaseStation3200[1].programs[1].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(_sun=True,
                                                                                                           _mon=True,
                                                                                                           _tues=True,
                                                                                                           _wed=True,
                                                                                                           _thurs=True,
                                                                                                           _fri=True,
                                                                                                           _sat=True)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_start_times)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        This step is adding all six program zones to our controller. Controller one will have all six zone, with zone
        1 and 4 being the primaries and the rest being linked.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure Program Zone 1
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=5)

            # Add & Configure Program Zone 2
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1)

            # Add & Configure Program Zone 3
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_as_linked_zone(_primary_zone=1)

            # Add & Configure Program Zone 4
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[1].zone_programs[4].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[4].set_run_time(_minutes=5)

            # Add & Configure Program Zone 5
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[5].set_as_linked_zone(_primary_zone=4)

            # Add & Configure Program Zone 6
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=6)
            self.config.BaseStation3200[1].programs[1].zone_programs[6].set_as_linked_zone(_primary_zone=4)
           
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        setup WaterSources
        ############################
        This step is setting up our water sources for our system. Two of our water sources will be on controller 1 and
        the third water source will be on controller 2.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Adding WS1 and WS2 to our controller 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[1].set_monthly_watering_budget(_budget=100000,
                                                                                        _with_shutdown_enabled=True)

            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=2)
            self.config.BaseStation3200[1].water_sources[2].set_enabled()
            self.config.BaseStation3200[1].water_sources[2].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[2].set_monthly_watering_budget(_budget=100000,
                                                                                           _with_shutdown_enabled=True)

            # Adding WS3 to our controller 2
            self.config.BaseStation3200[2].add_water_source_to_controller(_water_source_address=3)
            self.config.BaseStation3200[2].water_sources[3].set_enabled()
            self.config.BaseStation3200[2].water_sources[3].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[2].water_sources[3].set_monthly_watering_budget(_budget=100000,
                                                                                        _with_shutdown_enabled=True)
            
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        setup Point of Controls
        ############################
        This step is setting up all four of our POCs for our system. There will be two POCs on each controller. Each
        POC needs a flow meter and master valve, as well as a set target flow. After creating the POCs they will be
        added to any water sources and mainlines they need to be connected to. We have two on each controller because
        we want to be able to have a "backup" POC on the water path in case the other controller goes down.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Adding POC 1 to controller 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_flow_meter_to_point_of_control(
                _flow_meter_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=50)

            # Adding POC 2 to controller 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=2)
            self.config.BaseStation3200[1].points_of_control[2].set_enabled()
            self.config.BaseStation3200[1].points_of_control[2].add_master_valve_to_point_of_control(
                _master_valve_address=2)
            self.config.BaseStation3200[1].points_of_control[2].add_flow_meter_to_point_of_control(
                _flow_meter_address=2)
            self.config.BaseStation3200[1].points_of_control[2].set_target_flow(_gpm=50)

            # This will create the connection from our water source 1 to POC 1 on controller 1
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=1)

            # This will create the connection from our water source 2 to POC 2 on controller 1
            self.config.BaseStation3200[1].water_sources[2].add_point_of_control_to_water_source(
                _point_of_control_address=2)

            # Adding POC 1 to controller 2
            self.config.BaseStation3200[2].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[2].points_of_control[1].set_enabled()
            self.config.BaseStation3200[2].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=1)
            self.config.BaseStation3200[2].points_of_control[1].add_flow_meter_to_point_of_control(
                _flow_meter_address=1)
            self.config.BaseStation3200[2].points_of_control[1].set_target_flow(_gpm=100)

            # Adding POC 2 to controller 2
            self.config.BaseStation3200[2].add_point_of_control_to_controller(_point_of_control_address=2)
            self.config.BaseStation3200[2].points_of_control[2].set_enabled()
            self.config.BaseStation3200[2].points_of_control[2].add_master_valve_to_point_of_control(
                _master_valve_address=2)
            self.config.BaseStation3200[2].points_of_control[2].add_flow_meter_to_point_of_control(
                _flow_meter_address=2)
            self.config.BaseStation3200[2].points_of_control[2].set_target_flow(_gpm=100)

            # This will create the connection from our water source 3 to POC 2 on controller 2
            self.config.BaseStation3200[2].water_sources[3].add_point_of_control_to_water_source(
                _point_of_control_address=2)
            
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ##################
        setup mainlines
        ##################
        This step is setting up our mainlines for our system. There will be two mainlines that are both on controller 1.
        This will give us two water paths for each mainline.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure ML 1 on controller 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=20)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[1].set_high_flow_variance_tier_one(_percent=5,
                                                                                        _with_shutdown_enabled=False)
            self.config.BaseStation3200[1].mainlines[1].set_low_flow_variance_tier_one(_percent=20,
                                                                                       _with_shutdown_enabled=False)

            # Add & Configure ML 2 on controller 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=2)
            self.config.BaseStation3200[1].mainlines[2].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[2].set_target_flow(_gpm=20)
            self.config.BaseStation3200[1].mainlines[2].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[2].set_high_flow_variance_tier_one(_percent=5,
                                                                                        _with_shutdown_enabled=False)
            self.config.BaseStation3200[1].mainlines[2].set_low_flow_variance_tier_one(_percent=20,
                                                                                       _with_shutdown_enabled=False)

            # This makes the connection from mainline 1 to POC 1 on controller
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)

            # This makes the connection from mainline 1 to POC 2 on controller
            self.config.BaseStation3200[1].points_of_control[2].add_mainline_to_point_of_control(_mainline_address=2)

            # This makes our POC object match the controller.
            self.config.BaseStation3200[2].points_of_control[1].ml = 1
            self.config.BaseStation3200[2].points_of_control[1].controller_mainline = 1

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ######################
        setup zones on mainlines
        ######################
        This step is attaching certain zones to the mainlines so we can verify that the zones can still water after one
        of the POCs upstream of the mainline becomes unavailable, due to its controller going offline.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Adding Zones 1,2,3 to ML 1 on controller 1
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=1)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=2)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=3)

            # Adding Zones 4,5,6 to ML 2 on controller 1
            self.config.BaseStation3200[1].mainlines[2].add_zone_to_mainline(_zone_address=4)
            self.config.BaseStation3200[1].mainlines[2].add_zone_to_mainline(_zone_address=5)
            self.config.BaseStation3200[1].mainlines[2].add_zone_to_mainline(_zone_address=6)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ######################
        Set design flow
        ######################
        This step is setting our design flow for each zone.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # set design flow of zones  on controller 1
            # Assign a design flow value to each zone so that they have a default setting
            self.config.BaseStation3200[1].zones[1].set_design_flow(_gallons_per_minute=20)
            self.config.BaseStation3200[1].zones[2].set_design_flow(_gallons_per_minute=20)
            self.config.BaseStation3200[1].zones[3].set_design_flow(_gallons_per_minute=20)

            # set design flow of zones  on controller 1
            self.config.BaseStation3200[1].zones[4].set_design_flow(_gallons_per_minute=20)
            self.config.BaseStation3200[1].zones[5].set_design_flow(_gallons_per_minute=20)
            self.config.BaseStation3200[1].zones[6].set_design_flow(_gallons_per_minute=20)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        verify the entire configuration
        ###############################
        This step verifies our full configuration on both controllers. Refers to all the objects we created and
        verifies the controller can see all of them.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.BaseStation3200[2].verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        ###############################
        Share controllers, water sources, point of controls, and mainlines with Flow Station
        ###############################
        This step is setting up our FlowStation objects, so the FlowStation can properly help the system learn flow.
        Every object that was created above needs to be shared with the FlowStation.
        We want to make sure that we have two water paths to each mainline, so that way if one of their upstream POCs
        becomes unavailable like we are expecting (due to the controller going offline), there is still another water
        path to keep things running.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add controller 1 & 2 to FlowStation
            self.config.FlowStations[1].add_controller_to_flow_station(
                _controller_address=1,
                _flow_station_slot_number=1)
            self.config.FlowStations[1].add_controller_to_flow_station(
                _controller_address=2,
                _flow_station_slot_number=2)

            # Add our three water sources to the FlowStation
            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=1,
                _controller_water_source_address=1,
                _flow_station_water_source_slot_number=1)
            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=1,
                _controller_water_source_address=2,
                _flow_station_water_source_slot_number=2)
            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=2,
                _controller_water_source_address=3,
                _flow_station_water_source_slot_number=3)

            # Tell the 3200 that the FlowStation is in control of Water Sources.
            self.config.BaseStation3200[1].water_sources[1].set_manage_by_flowstation()
            self.config.BaseStation3200[1].water_sources[2].set_manage_by_flowstation()
            self.config.BaseStation3200[2].water_sources[3].set_manage_by_flowstation()

            # Add controller 1 & 2 POCs to FlowStation
            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=1,
                _controller_point_of_control_address=1,
                _flow_station_point_of_control_slot_number=1)
            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=1,
                _controller_point_of_control_address=2,
                _flow_station_point_of_control_slot_number=2)
            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=2,
                _controller_point_of_control_address=1,
                _flow_station_point_of_control_slot_number=3)
            self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                _controller_address=2,
                _controller_point_of_control_address=2,
                _flow_station_point_of_control_slot_number=4)

            # Places the FlowStation in charge of the POCs
            self.config.BaseStation3200[1].points_of_control[1].set_manage_by_flowstation()
            self.config.BaseStation3200[1].points_of_control[2].set_manage_by_flowstation()
            self.config.BaseStation3200[2].points_of_control[1].set_manage_by_flowstation()
            self.config.BaseStation3200[2].points_of_control[2].set_manage_by_flowstation()

            # Adds the mainlines to the FlowStation
            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=1,
                _controller_mainline_address=1,
                _flow_station_mainline_slot_number=1)

            self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                _controller_address=1,
                _controller_mainline_address=2,
                _flow_station_mainline_slot_number=2)
    
            self.config.BaseStation3200[1].mainlines[1].set_manage_by_flowstation()
            self.config.BaseStation3200[1].mainlines[2].set_manage_by_flowstation()

            # Connect water paths in the FlowStation to form our full system configuration
            # First configuration for C1WS1 --> C1P1 & C2P1 --> C1M1
            self.config.FlowStations[1].water_sources[1].add_point_of_control_to_water_source(_point_of_control_address=1)

            # Needed to use the get method here so I could reach the correct POC on controller 2
            self.config.FlowStations[1].get_water_source(1).add_point_of_control_to_water_source(_point_of_control_address=3)
            self.config.FlowStations[1].get_point_of_control(1).add_mainline_to_point_of_control(_mainline_address=1)
            self.config.FlowStations[1].get_point_of_control(3).add_mainline_to_point_of_control(_mainline_address=1)
            
            # Second configuration for C1WS2 --> C1P2 \
            #                          C2WS3 --> C2P2 / C1M2
            self.config.FlowStations[1].get_water_source(2).add_point_of_control_to_water_source(_point_of_control_address=2)

            # Needed to use the get method here so I could reach the correct POC on controller 2
            self.config.FlowStations[1].get_water_source(3).add_point_of_control_to_water_source(_point_of_control_address=4)

            self.config.FlowStations[1].get_point_of_control(2).add_mainline_to_point_of_control(_mainline_address=2)
            self.config.FlowStations[1].get_point_of_control(4).add_mainline_to_point_of_control(_mainline_address=2)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        ###############################
        verify the FlowStation configuration
        ###############################
        This step will verify the system configuration we have set up on the FlowStation, and save all the programming.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            self.config.FlowStations[1].save_programming_to_flow_station()
            self.config.FlowStations[1].verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_12(self):
        """
        #####################
        Set flow meter values
        #####################
        This step is forcing our flow meters we placed on our POCs to have a reading of 20GPM. This is an arbitrary
        number that was picked.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=20)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].flow_meters[2].bicoder.set_flow_rate(_gallons_per_minute=20)
            self.config.BaseStation3200[1].flow_meters[2].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[2].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=20)
            self.config.BaseStation3200[2].flow_meters[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[2].flow_meters[2].bicoder.set_flow_rate(_gallons_per_minute=20)
            self.config.BaseStation3200[2].flow_meters[2].bicoder.self_test_and_update_object_attributes()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_13(self):
        """
        #########################
        Verify learn flow success
        #########################
        This step is verifying we are able to learn flow with our second controller online. Each zone is timed on how
        long it takes to learn flow, and verifies a learn flow complete as well as clearing the messages for each zone.
        There are two water paths that could be used in this step.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_date_and_time(_date='05/21/2018', _time='09:59:00')
            self.config.BaseStation3200[2].set_date_and_time(_date='05/21/2018', _time='09:59:00')
            self.config.FlowStations[1].set_date_and_time(_date='05/21/2018', _time='09:59:00')

            self.config.BaseStation3200[1].programs[1].set_learn_flow_to_start()

            helper_methods.increment_controller_flowstation_clocks(self.config.FlowStations[1], self.config.BaseStation3200, minutes=1)

            # Controller 1
            not_done = True
            while not_done:
                zones_still_running_1 = False
                # ------------------------------------------------------------------------------------------------------
                # for each zone:
                #   1. get current data from controller (for getting updated status)
                #   2. get current status
                #   3. set flag only if any zones are still running, we want to remain in the while loop checking status
                # Controller 1
                for zone in sorted(self.config.BaseStation3200[1].zones.keys()):
                    self.config.BaseStation3200[1].zones[zone].get_data()
                    _zone_status = self.config.BaseStation3200[1].zones[zone].data.get_value_string_by_key(opcodes.status_code)

                    # for each zone get the ss per zone
                    # update the attribute in the object
                    # (this is so we can store it for what happened during the last run time)
                    if _zone_status == opcodes.learn_flow_active:
                        # using 60 so that we are in seconds
                        self.config.BaseStation3200[1].zones[zone].seconds_zone_ran += 60

                    # flag to true until all zone are done:
                    if _zone_status != opcodes.done_watering and _zone_status != opcodes.error:
                        zones_still_running_1 = True

                # this could say if zones_still_running:
                if zones_still_running_1:
                    not_done = True
                    helper_methods.increment_controller_flowstation_clocks(self.config.FlowStations[1], self.config.BaseStation3200, minutes=1)
                    print self.config.BaseStation3200[1].get_date_and_time()
                else:
                    not_done = False

            # Verifies and clears our learn flow success message for each zone
            for zone in sorted(self.config.BaseStation3200[1].programs[1].zone_programs.keys()):
                self.config.BaseStation3200[1].zones[zone] \
                    .messages.verify_learn_flow_complete_success_message()
                self.config.BaseStation3200[1].zones[zone] \
                    .messages.clear_learn_flow_complete_success_message()

            # Verify and clear Program learn flow complete message for programs on the controller
            for program in sorted(self.config.BaseStation3200[1].programs.keys()):
                self.config.BaseStation3200[1].programs[program] \
                    .messages.verify_learn_flow_complete_success_message()
                self.config.BaseStation3200[1].programs[program] \
                    .messages.clear_learn_flow_complete_success_message()

            # check to see if the time each zone took to learn flow is correct
            seconds_learning_flow = (self.config.BaseStation3200[1].get_mainline(1).ft * 60) + 60
            for zone in range(1, 6):
                if seconds_learning_flow != self.config.BaseStation3200[1].zones[zone].seconds_zone_ran:
                    print ("Zone " + str(zone) + " took " +
                           str(self.config.BaseStation3200[1].zones[zone]
                               .seconds_zone_ran) + " seconds to learn flow and it should have taken "
                           + str(seconds_learning_flow) + " seconds")
                else:
                    print ("Zone " + str(zone) + " took " +
                           str(self.config.BaseStation3200[1].zones[zone].seconds_zone_ran) + " seconds to learn flow")

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_14(self):
        """
        ############################
        Disconnect second controller
        ############################
        This step is taking our second controller offline, so we can learn flow again. We take it off line so we can now
        verify that even if one water path is offline, as long as we have another path, the zones can still learn flow.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Grabs the IP address we will be using to disconnect the controller
            ip_addr = self.config.BaseStation3200[2].ser.s_port
            ip_addr = ip_addr.split("//")[1].split(':')[0]

            self.config.BaseStation3200[2].set_disconnect_to_flowstation(_address=1, _ip_address=ip_addr)
            helper_methods.increment_controller_flowstation_clocks(self.config.FlowStations[1], self.config.BaseStation3200, minutes=1)
            # This needs to be done or else the verify not connected to flowstation does not work
            self.config.BaseStation3200[2].statuses.ty = '32'
            self.config.BaseStation3200[2].statuses.verify_not_connected_to_flowstation()

            # Changing back to CN, we are at a point where we don't want to modify the base class since this work
            # around gets the job done
            self.config.BaseStation3200[2].ty = 'CN'

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_15(self):
        """
        ############################
        Update flow meters
        ############################
        This step is updating the flow meter values, so to better reflect an actual system installation. The self test
        will force a reading for the flow rate update.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=20)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].flow_meters[2].bicoder.set_flow_rate(_gallons_per_minute=20)
            self.config.BaseStation3200[1].flow_meters[2].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[2].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=20)
            self.config.BaseStation3200[2].flow_meters[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[2].flow_meters[2].bicoder.set_flow_rate(_gallons_per_minute=20)
            self.config.BaseStation3200[2].flow_meters[2].bicoder.self_test_and_update_object_attributes()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_16(self):
        """
        #########################
        Verify learn flow success
        #########################
        This step verifies we can learn flow even with a controller offline, because we still have an available
        happy water path.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_date_and_time(_date='05/21/2018', _time='09:59:00')
            self.config.BaseStation3200[2].set_date_and_time(_date='05/21/2018', _time='09:59:00')
            self.config.FlowStations[1].set_date_and_time(_date='05/21/2018', _time='09:59:00')

            self.config.BaseStation3200[1].programs[1].set_learn_flow_to_start()
            helper_methods.increment_controller_flowstation_clocks(self.config.FlowStations[1], self.config.BaseStation3200, minutes=1)

            # Controller 1
            not_done = True
            while not_done:
                zones_still_running_1 = False
                # ------------------------------------------------------------------------------------------------------
                # for each zone:
                #   1. get current data from controller (for getting updated status)
                #   2. get current status
                #   3. set flag only if any zones are still running, we want to remain in the while loop checking status
                # Controller 1
                for zone in sorted(self.config.BaseStation3200[1].zones.keys()):
                    self.config.BaseStation3200[1].zones[zone].get_data()
                    _zone_status = self.config.BaseStation3200[1].zones[zone].data.get_value_string_by_key(opcodes.status_code)

                    # for each zone get the ss per zone
                    # update the attribute in the object
                    # (this is so we can store it for what happened during the last run time)
                    if _zone_status == opcodes.learn_flow_active:
                        # using 60 so that we are in seconds
                        self.config.BaseStation3200[1].zones[zone].seconds_zone_ran += 60

                    # sets flag to true until all zones are done:
                    if _zone_status != opcodes.done_watering and _zone_status != opcodes.error:
                        zones_still_running_1 = True

                if zones_still_running_1:
                    not_done = True
                    helper_methods.increment_controller_flowstation_clocks(self.config.FlowStations[1], self.config.BaseStation3200, minutes=1)
                    print self.config.BaseStation3200[1].get_date_and_time()
                else:
                    not_done = False

            # TODO: Passing current behavior where the zone fails to learn flow when a POC is offline
            # TODO: In the future it may change so the learn flow will use the available POC, not offline, to learn flow
            for zone in sorted(self.config.BaseStation3200[1].programs[1].zone_programs.keys()):
                self.config.BaseStation3200[1].zones[zone] \
                    .messages.verify_learn_flow_complete_error_message()
                self.config.BaseStation3200[1].zones[zone] \
                    .messages.clear_learn_flow_complete_error_message()
                # self.config.BaseStation3200[1].programs[1].zone_programs[zone] \
                #     .messages.verify_learn_flow_complete_success_message()
                # self.config.BaseStation3200[1].programs[1].zone_programs[zone] \
                #     .messages.verify_learn_flow_complete_success_message()

            # # Verify and clear Program learn flow complete message
            # for program in sorted(self.config.BaseStation3200[1].programs.keys()):
            #     self.config.BaseStation3200[1].programs[program] \
            #         .messages.verify_learn_flow_complete_success_message()
            #     self.config.BaseStation3200[1].programs[program] \
            #         .messages.clear_learn_flow_complete_success_message()

            # check to see if the time each zone took to learn flow is correct
            seconds_learning_flow = (self.config.BaseStation3200[1].get_mainline(1).ft * 60) + 60
            for zone in range(1, 6):
                if seconds_learning_flow != self.config.BaseStation3200[1].zones[zone].seconds_zone_ran:
                    print ("Zone " + str(zone) + " took " +
                           str(self.config.BaseStation3200[1].zones[zone]
                               .seconds_zone_ran) + " seconds to learn flow and it should have taken "
                           + str(seconds_learning_flow) + " seconds")
                else:
                    print ("Zone " + str(zone) + " took " +
                           str(self.config.BaseStation3200[1].zones[zone].seconds_zone_ran) + " seconds to learn flow")

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_17(self):
        """
        ###############################
        verify the entire configuration
        ###############################
        This step is used to make sure nothing in our system has gone offline, or been added since the last config check.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.FlowStations[1].save_programming_to_flow_station()
            helper_methods.increment_controller_flowstation_clocks(self.config.FlowStations[1], self.config.BaseStation3200, minutes=1)
            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.BaseStation3200[2].verify_full_configuration()
            self.config.FlowStations[1].verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]