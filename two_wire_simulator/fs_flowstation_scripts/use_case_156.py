import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

__author__ = 'Ben'


class ControllerUseCase156(object):
    """
    Test name:
        - CN UseCase156 Advanced flow variance with and without Shutdowns

        User Story: \n
        1)  As a user I want to be able to set upper flow variance thresholds based on my expected flow and to
            terminate irrigation when shutdown is enabled if consecutive flow faults are detected on my mainline zones.
            I want to be notified with zone messages and appropriate statuses if consecutive flow faults are detected.

        2)  As a user I want to be able to set upper flow variance thresholds based on my expected flow and to
            continue irrigation when shutdown is not enabled and consecutive flow faults are detected on my mainline zones.
            I want to be notified with mainline messages and appropriate statuses if consecutive flow faults are detected.

        3)  As a user I want to be able to set lower flow variance thresholds based on my expected flow and to
            terminate irrigation when shutdown is enabled if consecutive flow faults are detected on my mainline zones.
            I want to be notified with zone messages and appropriate statuses if consecutive flow faults are detected.

        4)  As a user I want to be able to set lower flow variance thresholds based on my expected flow and to
            continue irrigation when shutdown is not enabled and consecutive flow faults are detected on my mainline zones.
            I want to be notified with mainline messages and appropriate statuses if consecutive flow faults are detected.

    Coverage and Objectives:
        1.	When multiple mainlines on a 3200 are managed by flow station and have advanced flow variance enabled and
            advanced high/low variance limits with and without shutdown:
            a.	mainlines with advanced high/low flow variance limit (tiers 1,2,3,4) and shutdown enabled should
                shutdown zones (that receive 3 strikes) and alert user that a zone was shutdown because of high/low flow
                variance detected
            b.	mainlines with advanced high/low flow variance limit (tiers 1,2,3,4) and shutdown is disabled should
                alert user that a zone high/low flow variance was detected and irrigation should continue as normal
            c.	Mainlines with advanced high/low flow variance limit (tiers 1,2,3,4) with a limit of 0% will have
                variances ignored for the specific tiers and watering will continue as normal
            d.	Timed flow stabilization values above and below controller default (default = 2 minutes)
            e.	Running more than 15 concurrent zones

    Not Covered:
        1.	Advanced Flow Variance Disabled
        2.	Pressure Flow Stabilization
        3.	Multiple 3200s managed by FlowStation
        4.	Multiple points of control into a single mainline
        5.	High flows & shutdowns
        6.	Unscheduled flows & shutdowns
        7.	Mainline Standard variances & shutdowns
        8.	Empty conditions
        9.	Manual runs with flow variance (if manual running, ALL flow variance is ignored)
        10.	When a flow meter has a IO error or is non-responsive, ALL flow variance is ignored
        11.	Learn flows with flow variance (if doing a learn flow, ALL flow variance is ignored)
        12. Shutting down a zone other than the first
        13. Zone cycle and soak times with variance testing
        14. Nested mainlines on FlowStation

    Test Overview:
        - Advanced Variance Tier 1:
            - ML 1: High flow variance with shutdown (1 min pipe fill)
            - ML 2: Low flow variance without shutdown (3 min pipe fill)

        - Advanced Variance Tier 2: (these two MLs test functionality of having a 0% value set for variance threshold)
            - ML 3: High flow variance with shutdown (1 min pipe fill)
            - ML 4: Low flow variance without shutdown (1 min pipe fill)

        - Advanced Variance Tier 3:
            - ML 5: High flow variance with shutdown (1 min pipe fill)
            - ML 6: Low flow variance without shutdown (1 min pipe fill)

        - Advanced Variance Tier 4:
            - ML 7: High flow variance with shutdown (1 min pipe fill)
            - ML 8: Low flow variance with shutdown (1 min pipe fill)

        - FlowStation assignments
                                C1:W1
                                  |
            /---------------------+------------------------\
           |      |      |      |      |      |      |      |
         C1:P1  C1:P2  C1:P3  C1:P4  C1:P5  C1:P6  C1:P7  C1:P8
           |      |      |      |      |      |      |      |
         C1:M1  C1:M2  C1:M3  C1:M4  C1:M5  C1:M6  C1:M7  C1:M8

         - Programming Summary:
            PG  ZNs/DFs             CP/DF   ML/FT/DF    Var-Tier/%/SD   Purpose
            1   1-3/10/5/5          1/15    1/1/15      HI-T1/80/SD     2=strikeout
            2   4-6/10/10/10        2/20    2/3/20      LO-T1/10        message
            3   7-9/45/45/45        3/90    3/1/90      X               run
            4   10-12/30/30/30      4/75    4/1/75      X               run
            5   13-15/100x3         5/225   5/1/225     HI-T3/60/SD     ml=strikeout
            6   16-18/100/150/100   6/250   6/1/250     LO-T3/40        message
            7   19-21/150/150/200   7/550   7/1/550     HI-T4/20        message
            8   22-24/325/325/325   8/1200  8/1/1200    LO-T4/70/SD     ml=strikeout

    Test Configuration setup: \n
        1. test Advance flow Tier 1 with and with out shut down\n
            - Tier 1 range 0 - 25 GPM \n
            - Configuration
                - WS 1 ---> POC 1 ---> ML 1 \n
                    - POC 1
                        - FM 1
                        - design flow 10
                    - ML 1  Advanced high flow Tier 1 With Shutdown\n
                        - ZN 1 design flow 10
                        - ZN 2 design flow 5
                        - ZN 3 design flow 5
                        - Variance set to 80%
                        - Cause HF Variance Shutdown on ZN 2
                        - design flow 10
                    - PG 1
                - WS 1 ---> POC 2 ---> ML 2 \n
                    - POC 2
                        - FM 2
                        - design flow 20
                    - ML 2  Advanced low flow Tier 1 Without Shutdown\n
                        - ZN 4 design flow 10
                        - ZN 5 design flow 10
                        - ZN 6 design flow 10
                        - variance set to 10%
                        - Pipe fill 3 minutes
                        - Cause LF Variance Detection
                        - design flow 20
                     - PG 2
        2. test Advanced flow Tier 2 with and with out shut down
            - Tier 2 range 25 - 100 GPM \n
            - Configuration
                - WS 1 ---> POC 3 ---> ML 3 \n
                    - POC 3
                        - FM 3
                        - design flow 90
                    - ML 3  Advanced flow Tier 3 With Shutdown\n
                        - ZN 7 design flow 45
                        - ZN 8 design flow 45
                        - ZN 9 design flow 45
                        - variance set to 0%
                        - Verify variance is ignored and watering continues as expected
                        - design flow 90
                    - PG 3
                - WS 1 ---> POC 4 ---> ML 4 \n
                    - POC 4
                        - FM 4
                        - design flow 75
                    - ML 4  Advanced flow Tier 4 Without Shutdown\n
                        - ZN 10 design flow 30
                        - ZN 11 design flow 30
                        - ZN 12 design flow 30
                        - variance set to 0%
                        - Verify variance is ignored and watering continues as expected
                        - design flow 75
                    - PG 4
        3. test Advance flow Tier 3 with and with out shut down
            - Tier 3 range 100 - 300 GPM \n
            - Configuration
                - WS 1 ---> POC 5 ---> ML 5 \n
                    - POC 5
                        - FM 5
                        - design flow 225
                    - ML 5  Advanced flow Tier 3 With Shutdown\n
                        - ZN 13 design flow 100
                        - ZN 14 design flow 100
                        - ZN 15 design flow 100
                        - variance set to 60%
                        - Verify variance shutdown zone 13
                        - design flow 225
                    - PG 5
                - WS 1 ---> POC 6 ---> ML 6 \n
                    - POC 6
                        - FM 6
                        - design flow 250
                    - ML 6  Advanced flow Tier 3 Without Shutdown\n
                        - ZN 16 design flow 100
                        - ZN 17 design flow 150
                        - ZN 18 design flow 100
                        - variance set to 40%
                        - Verify variance is detected on mainline
                        - design flow 250
                    - PG 6
        4. test Advance flow Tier 4 with and without shutdown
            - Tier 4 range 300+ GPM \n
            - Configuration
                - WS 1 ---> POC 7 ---> ML 7 \n
                    - POC 7
                        - FM 7
                        - design flow 550
                    -ML 7  Advanced high flow Tier 4 With Shutdown\n
                        - ZN 19 design flow 150
                        - ZN 20 design flow 150
                        - ZN 21 design flow 200
                        - variance set to 10%
                        - Verify variance shutdown zone 19
                        - design flow 550
                    - PG 7
                - WS 1 ---> POC 8 ---> ML 8 \n
                    - POC 8
                        - FM 8
                        - design flow 1200
                    -ML 8  Advanced low flow Tier 4 With Shutdown\n
                        - ZN 22 design flow 325
                        - ZN 23 design flow 325
                        - ZN 24 design flow 325
                        - variance set to 70%
                        - Verify variance is detected on mainline
                        - design flow 1200
                    - PG 8

    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    configuration_dir='common/configuration_files/fs_json_config_files'
                                    )

        # these are global variables for the test
        # zone design flow values

        # ML 1
        # HF Variance Tier 1 (<25gpm)
        self.zn_1_df = 10
        self.zn_2_df = 5
        self.zn_3_df = 5

        # ML 2
        # LF Variance Tier 1 (<25gpm)
        self.zn_4_df = 10
        self.zn_5_df = 10
        self.zn_6_df = 10

        # ML 3
        # HF Variance Tier 2 (25gpm - 100gpm)
        self.zn_7_df = 45
        self.zn_8_df = 45
        self.zn_9_df = 45

        # ML 4
        # LF Variance Tier 2 (25gpm - 100gpm)
        self.zn_10_df = 30
        self.zn_11_df = 30
        self.zn_12_df = 30

        # ML 5
        # HF Variance Tier 3 (100gpm - 300gpm)
        self.zn_13_df = 100
        self.zn_14_df = 100
        self.zn_15_df = 100

        # ML 6
        # LF Variance Tier 3 (100gpm - 300gpm)
        self.zn_16_df = 100
        self.zn_17_df = 150
        self.zn_18_df = 100

        # ML 7
        # HF Variance Tier 4 (>300gpm)
        self.zn_19_df = 150
        self.zn_20_df = 150
        self.zn_21_df = 200

        # ML 8
        # LF Variance Tier 4 (>300gpm)
        self.zn_22_df = 325
        self.zn_23_df = 325
        self.zn_24_df = 325

        # mainline flow variance percentages

        # Tier 1
        self.ml_1_hi_fl_vr = 80
        self.ml_1_lo_fl_vr = 0  # only testing HF variance on ML 1

        self.ml_2_hi_fl_vr = 0  # only testing LF variance on ML 2
        self.ml_2_lo_fl_vr = 10

        # Tier 2
        self.ml_3_hi_fl_vr = 0  # testing variance = 0 = watering should complete as expected with no messages
        self.ml_3_lo_fl_vr = 0  # testing variance = 0 = watering should complete as expected with no messages

        self.ml_4_hi_fl_vr = 0  # testing variance = 0 = watering should complete as expected with no messages
        self.ml_4_lo_fl_vr = 0  # testing variance = 0 = watering should complete as expected with no messages

        # Tier 3
        self.ml_5_hi_fl_vr = 60
        self.ml_5_lo_fl_vr = 0  # only testing HF variance on ML 5

        self.ml_6_hi_fl_vr = 0  # only testing LF variance on ML 6
        self.ml_6_lo_fl_vr = 40

        # Tier 4
        self.ml_7_hi_fl_vr = 20
        self.ml_7_lo_fl_vr = 0  # only testing HF variance on ML 7

        self.ml_8_hi_fl_vr = 0  # only testing LF variance on ML 8
        self.ml_8_lo_fl_vr = 70

        # Placeholders for variance trigger calculations.
        #   -> These values will represent the calculated actual flow needed for FlowMeters in order to trigger the
        #      threshold boundaries.
        self.setting_for_ml_1_high_flow_variance_calculation = None
        self.setting_for_ml_2_low_flow_variance_calculation = None
        self.setting_for_ml_5_high_flow_variance_calculation = None
        self.setting_for_ml_6_low_flow_variance_calculation = None
        self.setting_for_ml_7_high_flow_variance_calculation = None
        self.setting_for_ml_8_low_flow_variance_calculation = None

        self.run_use_case()

    #################################
    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=False)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]

                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))

                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set controller max concurrency to allow for all programs and zones to water
            self.config.BaseStation3200[1].set_max_concurrent_zones(40)

            # Add and configure Program 1 - 8
            for pg_address in range(1, 9):
                self.config.BaseStation3200[1].add_program_to_controller(_program_address=pg_address)
                self.config.BaseStation3200[1].programs[pg_address].set_enabled()
                self.config.BaseStation3200[1].programs[pg_address].set_max_concurrent_zones(_number_of_zones=15)
                self.config.BaseStation3200[1].programs[pg_address].set_start_times(_st_list=[600])  # 10am start time

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # Add & Configure Program Zones for all programs 1-8, 3 zones each

            # Add & Configure Program 1 Zones 1 - 3
            for zn_address in range(1, 4):
                self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=zn_address)
                self.config.BaseStation3200[1].programs[1].zone_programs[zn_address].set_as_timed_zone()
                self.config.BaseStation3200[1].programs[1].zone_programs[zn_address].set_run_time(_minutes=10)

            # Add & Configure Program 2 Zones 4 - 6
            for zn_address in range(4, 7):
                self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=zn_address)
                self.config.BaseStation3200[1].programs[2].zone_programs[zn_address].set_as_timed_zone()
                self.config.BaseStation3200[1].programs[2].zone_programs[zn_address].set_run_time(_minutes=10)

            # Add & Configure Program 3 Zones 7 - 9
            for zn_address in range(7, 10):
                self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=zn_address)
                self.config.BaseStation3200[1].programs[3].zone_programs[zn_address].set_as_timed_zone()
                self.config.BaseStation3200[1].programs[3].zone_programs[zn_address].set_run_time(_minutes=10)

            # Add & Configure Program 4 Zones 10 - 12
            for zn_address in range(10, 13):
                self.config.BaseStation3200[1].programs[4].add_zone_to_program(_zone_address=zn_address)
                self.config.BaseStation3200[1].programs[4].zone_programs[zn_address].set_as_timed_zone()
                self.config.BaseStation3200[1].programs[4].zone_programs[zn_address].set_run_time(_minutes=10)

            # Add & Configure Program 5 Zones 13 - 15
            for zn_address in range(13, 16):
                self.config.BaseStation3200[1].programs[5].add_zone_to_program(_zone_address=zn_address)
                self.config.BaseStation3200[1].programs[5].zone_programs[zn_address].set_as_timed_zone()
                self.config.BaseStation3200[1].programs[5].zone_programs[zn_address].set_run_time(_minutes=10)

            # Add & Configure Program 6 Zones 16 - 18
            for zn_address in range(16, 19):
                self.config.BaseStation3200[1].programs[6].add_zone_to_program(_zone_address=zn_address)
                self.config.BaseStation3200[1].programs[6].zone_programs[zn_address].set_as_timed_zone()
                self.config.BaseStation3200[1].programs[6].zone_programs[zn_address].set_run_time(_minutes=10)

            # Add & Configure Program 7 Zones 19 - 21
            for zn_address in range(19, 22):
                self.config.BaseStation3200[1].programs[7].add_zone_to_program(_zone_address=zn_address)
                self.config.BaseStation3200[1].programs[7].zone_programs[zn_address].set_as_timed_zone()
                self.config.BaseStation3200[1].programs[7].zone_programs[zn_address].set_run_time(_minutes=10)

            # Add & Configure Program 8 Zones 22 - 24
            for zn_address in range(22, 25):
                self.config.BaseStation3200[1].programs[8].add_zone_to_program(_zone_address=zn_address)
                self.config.BaseStation3200[1].programs[8].zone_programs[zn_address].set_as_timed_zone()
                self.config.BaseStation3200[1].programs[8].zone_programs[zn_address].set_run_time(_minutes=10)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n

        - No budget is used because we do not want the water source to shut down
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Water Source 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].set_priority(_priority_for_water_source=1)
            self.config.BaseStation3200[1].water_sources[1].set_monthly_watering_budget(_budget=0)
            self.config.BaseStation3200[1].water_sources[1].set_water_rationing_to_disabled()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n

        - set up POCs 1-8
            - set design flow
            - assign POC to mainline
            - assign flow meter

        - Not setting any flow limits because we do not want any flow shutdowns
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure POC 1 - 8
            for pc_address in range(1, 9):
                self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=pc_address)
                self.config.BaseStation3200[1].points_of_control[pc_address].set_enabled()
                self.config.BaseStation3200[1].points_of_control[pc_address].add_flow_meter_to_point_of_control(
                    _flow_meter_address=pc_address)

            # set design flow
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=15)
            self.config.BaseStation3200[1].points_of_control[2].set_target_flow(_gpm=20)
            self.config.BaseStation3200[1].points_of_control[3].set_target_flow(_gpm=90)
            self.config.BaseStation3200[1].points_of_control[4].set_target_flow(_gpm=75)
            self.config.BaseStation3200[1].points_of_control[5].set_target_flow(_gpm=225)
            self.config.BaseStation3200[1].points_of_control[6].set_target_flow(_gpm=250)
            self.config.BaseStation3200[1].points_of_control[7].set_target_flow(_gpm=550)
            self.config.BaseStation3200[1].points_of_control[8].set_target_flow(_gpm=1200)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n

        Not added POC -> Mainline assignments at the controller. This is done at the FlowStation later on.
        The mainline target flow are equal to the POC target flows.

        """
        # here we can either execute the following uncommented lines in procedural fashion, or we could re-init the
        # object, would have to import Mainline at the top, effectively accomplishing the same thing by:
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ----------- #
            # MAINLINE 1  #
            # ----------- #

            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=15)
            self.config.BaseStation3200[1].mainlines[1].set_use_advanced_flow_to_true()
            self.config.BaseStation3200[1].mainlines[1].set_high_flow_variance_tier_one(_percent=self.ml_1_hi_fl_vr,
                                                                                        _with_shutdown_enabled=True)

            # ----------- #
            # MAINLINE 2  #
            # ----------- #

            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=2)
            self.config.BaseStation3200[1].mainlines[2].set_pipe_stabilization_time(_minutes=3)
            self.config.BaseStation3200[1].mainlines[2].set_target_flow(_gpm=20)
            self.config.BaseStation3200[1].mainlines[2].set_use_advanced_flow_to_true()
            self.config.BaseStation3200[1].mainlines[2].set_low_flow_variance_tier_one(_percent=self.ml_2_lo_fl_vr,
                                                                                       _with_shutdown_enabled=False)

            # ----------- #
            # MAINLINE 3  #
            # ----------- #

            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=3)
            self.config.BaseStation3200[1].mainlines[3].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[3].set_target_flow(_gpm=90)
            self.config.BaseStation3200[1].mainlines[3].set_use_advanced_flow_to_true()
            self.config.BaseStation3200[1].mainlines[3].set_high_flow_variance_tier_two(_percent=self.ml_3_hi_fl_vr,
                                                                                        _with_shutdown_enabled=True)

            # ----------- #
            # MAINLINE 4  #
            # ----------- #

            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=4)
            self.config.BaseStation3200[1].mainlines[4].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[4].set_target_flow(_gpm=75)
            self.config.BaseStation3200[1].mainlines[4].set_use_advanced_flow_to_true()
            self.config.BaseStation3200[1].mainlines[4].set_low_flow_variance_tier_two(_percent=self.ml_4_lo_fl_vr,
                                                                                       _with_shutdown_enabled=False)

            # ----------- #
            # MAINLINE 5  #
            # ----------- #

            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=5)
            self.config.BaseStation3200[1].mainlines[5].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[5].set_target_flow(_gpm=225)
            self.config.BaseStation3200[1].mainlines[5].set_use_advanced_flow_to_true()
            self.config.BaseStation3200[1].mainlines[5].set_high_flow_variance_tier_three(_percent=self.ml_5_hi_fl_vr,
                                                                                          _with_shutdown_enabled=True)

            # ----------- #
            # MAINLINE 6  #
            # ----------- #

            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=6)
            self.config.BaseStation3200[1].mainlines[6].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[6].set_target_flow(_gpm=250)
            self.config.BaseStation3200[1].mainlines[6].set_use_advanced_flow_to_true()
            self.config.BaseStation3200[1].mainlines[6].set_low_flow_variance_tier_three(_percent=self.ml_6_lo_fl_vr,
                                                                                         _with_shutdown_enabled=False)

            # ----------- #
            # MAINLINE 7  #
            # ----------- #

            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=7)
            self.config.BaseStation3200[1].mainlines[7].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[7].set_target_flow(_gpm=550)
            self.config.BaseStation3200[1].mainlines[7].set_use_advanced_flow_to_true()
            self.config.BaseStation3200[1].mainlines[7].set_high_flow_variance_tier_four(_percent=self.ml_7_hi_fl_vr,
                                                                                         _with_shutdown_enabled=False)

            # ----------- #
            # MAINLINE 8  #
            # ----------- #

            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=8)
            self.config.BaseStation3200[1].mainlines[8].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[8].set_target_flow(_gpm=1200)
            self.config.BaseStation3200[1].mainlines[8].set_use_advanced_flow_to_true()
            self.config.BaseStation3200[1].mainlines[8].set_low_flow_variance_tier_four(_percent=self.ml_8_lo_fl_vr,
                                                                                        _with_shutdown_enabled=True)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=1)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=2)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=3)

            self.config.BaseStation3200[1].mainlines[2].add_zone_to_mainline(_zone_address=4)
            self.config.BaseStation3200[1].mainlines[2].add_zone_to_mainline(_zone_address=5)
            self.config.BaseStation3200[1].mainlines[2].add_zone_to_mainline(_zone_address=6)

            self.config.BaseStation3200[1].mainlines[3].add_zone_to_mainline(_zone_address=7)
            self.config.BaseStation3200[1].mainlines[3].add_zone_to_mainline(_zone_address=8)
            self.config.BaseStation3200[1].mainlines[3].add_zone_to_mainline(_zone_address=9)

            self.config.BaseStation3200[1].mainlines[4].add_zone_to_mainline(_zone_address=10)
            self.config.BaseStation3200[1].mainlines[4].add_zone_to_mainline(_zone_address=11)
            self.config.BaseStation3200[1].mainlines[4].add_zone_to_mainline(_zone_address=12)

            self.config.BaseStation3200[1].mainlines[5].add_zone_to_mainline(_zone_address=13)
            self.config.BaseStation3200[1].mainlines[5].add_zone_to_mainline(_zone_address=14)
            self.config.BaseStation3200[1].mainlines[5].add_zone_to_mainline(_zone_address=15)

            self.config.BaseStation3200[1].mainlines[6].add_zone_to_mainline(_zone_address=16)
            self.config.BaseStation3200[1].mainlines[6].add_zone_to_mainline(_zone_address=17)
            self.config.BaseStation3200[1].mainlines[6].add_zone_to_mainline(_zone_address=18)

            self.config.BaseStation3200[1].mainlines[7].add_zone_to_mainline(_zone_address=19)
            self.config.BaseStation3200[1].mainlines[7].add_zone_to_mainline(_zone_address=20)
            self.config.BaseStation3200[1].mainlines[7].add_zone_to_mainline(_zone_address=21)

            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=22)
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=23)
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=24)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ######################
        setup zones on mainlines
        ######################
         - set up zone  Attributes associated with main lines \n
            - set design flow on zone
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Assign a design flow value to each zone so that they have a default setting
            self.config.BaseStation3200[1].zones[1].set_design_flow(_gallons_per_minute=self.zn_1_df)
            self.config.BaseStation3200[1].zones[2].set_design_flow(_gallons_per_minute=self.zn_2_df)
            self.config.BaseStation3200[1].zones[3].set_design_flow(_gallons_per_minute=self.zn_3_df)
            self.config.BaseStation3200[1].zones[4].set_design_flow(_gallons_per_minute=self.zn_4_df)
            self.config.BaseStation3200[1].zones[5].set_design_flow(_gallons_per_minute=self.zn_5_df)
            self.config.BaseStation3200[1].zones[6].set_design_flow(_gallons_per_minute=self.zn_6_df)
            self.config.BaseStation3200[1].zones[7].set_design_flow(_gallons_per_minute=self.zn_7_df)
            self.config.BaseStation3200[1].zones[8].set_design_flow(_gallons_per_minute=self.zn_8_df)
            self.config.BaseStation3200[1].zones[9].set_design_flow(_gallons_per_minute=self.zn_9_df)
            self.config.BaseStation3200[1].zones[10].set_design_flow(_gallons_per_minute=self.zn_10_df)
            self.config.BaseStation3200[1].zones[11].set_design_flow(_gallons_per_minute=self.zn_11_df)
            self.config.BaseStation3200[1].zones[12].set_design_flow(_gallons_per_minute=self.zn_12_df)
            self.config.BaseStation3200[1].zones[13].set_design_flow(_gallons_per_minute=self.zn_13_df)
            self.config.BaseStation3200[1].zones[14].set_design_flow(_gallons_per_minute=self.zn_14_df)
            self.config.BaseStation3200[1].zones[15].set_design_flow(_gallons_per_minute=self.zn_15_df)
            self.config.BaseStation3200[1].zones[16].set_design_flow(_gallons_per_minute=self.zn_16_df)
            self.config.BaseStation3200[1].zones[17].set_design_flow(_gallons_per_minute=self.zn_17_df)
            self.config.BaseStation3200[1].zones[18].set_design_flow(_gallons_per_minute=self.zn_18_df)
            self.config.BaseStation3200[1].zones[19].set_design_flow(_gallons_per_minute=self.zn_19_df)
            self.config.BaseStation3200[1].zones[20].set_design_flow(_gallons_per_minute=self.zn_20_df)
            self.config.BaseStation3200[1].zones[21].set_design_flow(_gallons_per_minute=self.zn_21_df)
            self.config.BaseStation3200[1].zones[22].set_design_flow(_gallons_per_minute=self.zn_22_df)
            self.config.BaseStation3200[1].zones[23].set_design_flow(_gallons_per_minute=self.zn_23_df)
            self.config.BaseStation3200[1].zones[24].set_design_flow(_gallons_per_minute=self.zn_24_df)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        Share controller, water source, point of control, mainline with Flow Station
        ###############################
            - Share controller1 with the flow station
            - Add and share a Water Source between controller1 and the FlowStation
            - Add and share Point of Control between controller1 and the FlowStation
            - Add and share Mainline between controller1 and the FlowStation

            - Assign WS, POC, and ML on the FlowStation
            - WSn -> PCn -> MLn

            - Assignments:
                                C1:W1
                                  |
            /---------------------+------------------------\
           |      |      |      |      |      |      |      |
         C1:P1  C1:P2  C1:P3  C1:P4  C1:P5  C1:P6  C1:P7  C1:P8
           |      |      |      |      |      |      |      |
         C1:M1  C1:M2  C1:M3  C1:M4  C1:M5  C1:M6  C1:M7  C1:M8
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add controller 1 to FlowStation
            self.config.FlowStations[1].add_controller_to_flow_station(
                _controller_address=1,
                _flow_station_slot_number=1)

            self.config.FlowStations[1].add_controller_water_source_to_flowstation(
                _controller_address=1,
                _controller_water_source_address=1,
                _flow_station_water_source_slot_number=1)
            # Tell the 3200 that the FlowStation is in control of Water Source
            self.config.BaseStation3200[1].get_water_source(ws_number=1).set_manage_by_flowstation()

            for pc_address in range(1, 9):
                self.config.FlowStations[1].add_controller_point_of_control_to_flowstation(
                    _controller_address=1,
                    _controller_point_of_control_address=pc_address,
                    _flow_station_point_of_control_slot_number=pc_address)

                # Tell the 3200 that the FlowStation is in control of Point of Control
                self.config.BaseStation3200[1].get_point_of_control(pc_number=pc_address).set_manage_by_flowstation()

            for ml_address in range(1, 9):
                self.config.FlowStations[1].add_controller_mainline_to_flowstation(
                    _controller_address=1,
                    _controller_mainline_address=ml_address,
                    _flow_station_mainline_slot_number=ml_address)

                # Tell the 3200 that the FlowStation is in control of Mainline
                self.config.BaseStation3200[1].get_mainline(ml_number=ml_address).set_manage_by_flowstation()

            # Assign WS, POC, and ML on FlowStation
            # WSn -> PCn -> MLn
            for assign_address in range(1, 9):
                self.config.FlowStations[1].get_point_of_control(
                    pc_number=assign_address).add_mainline_to_point_of_control(_mainline_address=assign_address)

                # TODO: allow multiple POC on a water sources
                self.config.FlowStations[1].get_water_source(ws_number=1).add_point_of_control_to_water_source(
                    _point_of_control_address=assign_address)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.FlowStations[1].save_programming_to_flow_station()
            self.config.FlowStations[1].verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        ############################
        Configure FlowMeters
        ############################

        - Configure FlowMeters to trigger HF/LF variance with calculated actual flow values
        - FlowMeters 1,5,7 will have values to trigger HF variance
        - FlowMeters 2,6,8 will have values to trigger LF variance
        - FlowMeters 3,4 will have values equal to half of their Tier's max GPM (100/2 = 50GPM)
            -> 50 GPM would cause both a HF and LF variance if values >0 were entered.
            -> This verifies that having 0 for variance limit causes no messages/errors.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # --------------------------------------- #
            # Configure High Flow Variance Components #
            # --------------------------------------- #

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _variance_percent=self.ml_1_hi_fl_vr,
                _expected_gpm=self.zn_1_df + self.zn_2_df)

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[5],
                _variance_percent=self.ml_5_hi_fl_vr,
                _expected_gpm=self.zn_13_df + self.zn_14_df)

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[7],
                _variance_percent=self.ml_7_hi_fl_vr,
                _expected_gpm=self.zn_19_df + self.zn_20_df + self.zn_21_df)

            # -------------------------------------- #
            # Configure Low Flow Variance Components #
            # -------------------------------------- #

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[2],
                _variance_percent=self.ml_2_lo_fl_vr,
                _expected_gpm=self.zn_4_df + self.zn_5_df)

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[6],
                _variance_percent=self.ml_6_lo_fl_vr,
                _expected_gpm=self.zn_16_df + self.zn_17_df)

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[8],
                _variance_percent=self.ml_8_lo_fl_vr,
                _expected_gpm=self.zn_22_df + self.zn_23_df + self.zn_24_df)

            # ------------------------------------- #
            # Configure No Flow Variance Components #
            # ------------------------------------- #

            # Set FM 3,4 to constant 50GPM which is 1/2 of the variance flow range being used on ML3,4
            self.config.BaseStation3200[1].flow_meters[3].bicoder.set_flow_rate(_gallons_per_minute=95.0)
            self.config.BaseStation3200[1].flow_meters[3].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].flow_meters[4].bicoder.set_flow_rate(_gallons_per_minute=40.0)
            self.config.BaseStation3200[1].flow_meters[4].bicoder.self_test_and_update_object_attributes()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        ############################
        Increment clock to start programming
        ############################

        - Set date and time on controller and increment clock to start zones running
        - Increment clock to program's start time of 10am
        - Verify initial IDLE system status prior to programming starting (programming will start "AFTER" top of the 
          min)
        - flow meters have already been set to a value so they are running
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ---------------------------------------------------------------------- #
            # Set controllers date/time to 1 minute before program start time (10am) #
            # ---------------------------------------------------------------------- #

            date_mngr.set_current_date_to_match_computer()
            self.config.BaseStation3200[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                             _time='09:59:00')
            self.config.FlowStations[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                          _time='09:59:00')

            # ------------------------------------------------------------ #
            # Increment controller clock to 10am to trigger program starts #
            # ------------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify system is IDLE until after the top of the minute

            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_ok()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_ok()

            for address in range(1, 9):
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_done()

                # Flow Sources
                self.config.BaseStation3200[1].get_point_of_control(address).statuses.verify_status_is_off()
                self.config.BaseStation3200[1].get_mainline(address).statuses.verify_status_is_off()

                self.config.FlowStations[1].get_point_of_control(address).statuses.verify_status_is_off()
                self.config.FlowStations[1].get_mainline(address).statuses.verify_status_is_off()

                # Flow Devices
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_running()

            for zn_address in self.config.BaseStation3200[1].zones.keys():
                self.config.BaseStation3200[1].zones[zn_address].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        ############################
        Increment clock from 10:00 - 10:01 AM
        ############################

        Verify programming starts after the top of the minute (10:00 am)
            - Everything will go to waiting with the exception of water sources and pocs and mls which will be running.
            - Flow meters will display running status because we set their flow rate in an earlier step to trigger the
              first group of variances.
            - water has been requested from FlowStation and granted ready for top of minute processing

        Mainline 1:
            - Zones 1, 2, and 3 are waiting to run
        Mainline 2:
            - Zones 4, 5, and 6 are waiting to run
        Mainline 3:
            - Zones 7, 8, and 9 are waiting to run
        Mainline 4:
            - Zones 10, 11, and 12 are waiting to run
        Mainline 5:
            - Zones 13, 14, and 15 are waiting to run
        Mainline 6:
            - Zones 16, 17, and 18 are waiting to run
        Mainline 7:
            - Zones 19, 20, and 21 are waiting to run
        Mainline 8:
            - Zones 22, 23, and 24 are waiting to run

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:01 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify the only water source
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            # ------------------------------------------------------ #
            # ML1 - ADV HIGH FLOW VARIANCE TIER 1 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()

            # ------------------------------------------------------ #
            # ML2 - ADV LOW FLOW VARIANCE TIER 1 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_waiting_to_run()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_waiting_to_water()

            # ------------------------------------------------------ #
            # ML3 - ADV HIGH FLOW VARIANCE TIER 2 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_waiting_to_run()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_running()

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_waiting_to_water()

            # ------------------------------------------------------ #
            # ML4 - ADV LOW FLOW VARIANCE TIER 2 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_waiting_to_run()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(4).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(4).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[4].statuses.verify_status_is_running()

            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_waiting_to_water()

            # ------------------------------------------------------ #
            # ML5 - ADV HIGH FLOW VARIANCE TIER 3 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_waiting_to_run()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(5).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(5).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(5).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(5).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[5].statuses.verify_status_is_running()

            # Mainline 5 Zones
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_waiting_to_water()

            # ------------------------------------------------------ #
            # ML6 - ADV LOW FLOW VARIANCE TIER 3 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_waiting_to_run()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(6).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(6).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()

            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_waiting_to_water()

            # ------------------------------------------------------ #
            # ML7 - ADV HIGH FLOW VARIANCE TIER 4 W/O SHUTDOWN       #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_waiting_to_run()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(7).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(7).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(7).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(7).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[7].statuses.verify_status_is_running()

            # Mainline 7 Zones
            self.config.BaseStation3200[1].zones[19].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[20].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[21].statuses.verify_status_is_waiting_to_water()

            # ------------------------------------------------------ #
            # ML8 - ADV LOW FLOW VARIANCE TIER 4 W/ SHUTDOWN         #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[8].statuses.verify_status_is_waiting_to_run()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(8).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(8).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[8].statuses.verify_status_is_running()

            # Mainline 8 Zones
            self.config.BaseStation3200[1].zones[22].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[23].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[24].statuses.verify_status_is_waiting_to_water()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        ############################
        Increment clock from 10:01 - 10:02 AM
        ############################

        Verify that watering starts and valves turn on
            - By this point the FlowStation and 3200 have communicated back and forth with water requests and water
              allocations given
            - 3200 acts on allocations given by the FlowStation by turning on the valves on all mainlines

        Mainline 1:
            - Zones 1 and 2 are running with high variance
        Mainline 2:
            - Zones 4 and 5 are running with low variance
        Mainline 3:
            - Zones 7 and 8 are running with variance but no message
        Mainline 4:
            - Zones 11 and 11 are running with variance but no message
        Mainline 5:
            - Zones 13 and 14 are running with high variance
        Mainline 6:
            - Zones 16 and 17 are running with low variance
        Mainline 7:
            - Zones 19, 20, and 21 are running with high variance
        Mainline 8:
            - Zones 22, 23, and 24 are running with low variance

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:02 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify the only water source
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            # ------------------------------------------------------ #
            # ML1 - ADV HIGH FLOW VARIANCE TIER 1 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _variance_percent=self.ml_1_hi_fl_vr,
                _expected_gpm=self.zn_1_df + self.zn_2_df)

            # ------------------------------------------------------ #
            # ML2 - ADV LOW FLOW VARIANCE TIER 1 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[2],
                _variance_percent=self.ml_2_lo_fl_vr,
                _expected_gpm=self.zn_4_df + self.zn_5_df)

            # ------------------------------------------------------ #
            # ML3 - ADV HIGH FLOW VARIANCE TIER 2 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_running()

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[3],
                _variance_percent=self.ml_3_hi_fl_vr,
                _expected_gpm=self.zn_7_df + self.zn_8_df)

            # ------------------------------------------------------ #
            # ML4 - ADV LOW FLOW VARIANCE TIER 2 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(4).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(4).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[4].statuses.verify_status_is_running()

            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_waiting_to_water()

            # ------------------------------------------------------ #
            # ML5 - ADV HIGH FLOW VARIANCE TIER 3 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(5).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(5).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(5).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(5).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[5].statuses.verify_status_is_running()

            # Mainline 5 Zones
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[5],
                _variance_percent=self.ml_5_hi_fl_vr,
                _expected_gpm=self.zn_13_df + self.zn_14_df)

            # ------------------------------------------------------ #
            # ML6 - ADV LOW FLOW VARIANCE TIER 3 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(6).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(6).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()

            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[6],
                _variance_percent=self.ml_6_lo_fl_vr,
                _expected_gpm=self.zn_16_df + self.zn_17_df)

            # ------------------------------------------------------ #
            # ML7 - ADV HIGH FLOW VARIANCE TIER 4 W/O SHUTDOWN       #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(7).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(7).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(7).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(7).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[7].statuses.verify_status_is_running()

            # Mainline 7 Zones
            self.config.BaseStation3200[1].zones[19].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[20].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[21].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[7],
                _variance_percent=self.ml_7_hi_fl_vr,
                _expected_gpm=self.zn_19_df + self.zn_20_df + self.zn_21_df)

            # ------------------------------------------------------ #
            # ML8 - ADV LOW FLOW VARIANCE TIER 4 W/ SHUTDOWN         #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[8].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(8).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(8).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[8].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[22].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[23].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[24].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[8],
                _variance_percent=self.ml_8_lo_fl_vr,
                _expected_gpm=self.zn_22_df + self.zn_23_df + self.zn_24_df)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_14(self):
        """
        ############################
        Increment clock from 10:02 - 10:03 AM
        ############################

        At this point, the FlowStation has detected variances on mainlines (with a variance limit > 0% and have with 1
        min pipe fill times and has sent the variance faults to the 3200.

        Mainline 1:
            - Zones 1 and 2 are running with high variance
        Mainline 2:
            - Zones 4 and 5 are running with low variance
        Mainline 3:
            - Zones 7 and 8 are running with variance but no message
        Mainline 4:
            - Zones 11 and 11 are running with variance but no message
        Mainline 5:
            - Zones 13 and 14 are running with high variance
        Mainline 6:
            - Zones 16 and 17 are running with low variance
        Mainline 7:
            - Zones 19, 20, and 21 are running with high variance
        Mainline 8:
            - Zones 22, 23, and 24 are running with low variance

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:03 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify the only water source
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            # ------------------------------------------------------ #
            # ML1 - ADV HIGH FLOW VARIANCE TIER 1 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _variance_percent=self.ml_1_hi_fl_vr,
                _expected_gpm=self.zn_1_df + self.zn_2_df)

            # ------------------------------------------------------ #
            # ML2 - ADV LOW FLOW VARIANCE TIER 1 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[2],
                _variance_percent=self.ml_2_lo_fl_vr,
                _expected_gpm=self.zn_4_df + self.zn_5_df)

            # ------------------------------------------------------ #
            # ML3 - ADV HIGH FLOW VARIANCE TIER 2 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_running()

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[3],
                _variance_percent=self.ml_3_hi_fl_vr,
                _expected_gpm=self.zn_7_df + self.zn_8_df)

            # ------------------------------------------------------ #
            # ML4 - ADV LOW FLOW VARIANCE TIER 2 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(4).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(4).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[4].statuses.verify_status_is_running()

            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[4],
                _variance_percent=self.ml_4_lo_fl_vr,
                _expected_gpm=self.zn_10_df + self.zn_11_df)

            # ------------------------------------------------------ #
            # ML5 - ADV HIGH FLOW VARIANCE TIER 3 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(5).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(5).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(5).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(5).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[5].statuses.verify_status_is_running()

            # Mainline 5 Zones
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[5],
                _variance_percent=self.ml_5_hi_fl_vr,
                _expected_gpm=self.zn_13_df + self.zn_14_df)

            # ------------------------------------------------------ #
            # ML6 - ADV LOW FLOW VARIANCE TIER 3 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(6).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(6).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()

            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[6],
                _variance_percent=self.ml_6_lo_fl_vr,
                _expected_gpm=self.zn_16_df + self.zn_17_df)

            # ------------------------------------------------------ #
            # ML7 - ADV HIGH FLOW VARIANCE TIER 4 W/O SHUTDOWN       #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(7).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(7).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(7).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(7).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[7].statuses.verify_status_is_running()

            # Mainline 7 Zones
            self.config.BaseStation3200[1].zones[19].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[20].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[21].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[7],
                _variance_percent=self.ml_7_hi_fl_vr,
                _expected_gpm=self.zn_19_df + self.zn_20_df + self.zn_21_df)

            # ------------------------------------------------------ #
            # ML8 - ADV LOW FLOW VARIANCE TIER 4 W/ SHUTDOWN         #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[8].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(8).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(8).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[8].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[22].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[23].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[24].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[8],
                _variance_percent=self.ml_8_lo_fl_vr,
                _expected_gpm=self.zn_22_df + self.zn_23_df + self.zn_24_df)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_15(self):
        """
        ############################
        Increment clock from 10:03 - 10:04 AM
        ############################

        Mainline 1:
            - Zones 1 and 2 are running with high variance
            - FlowStation has detected a high flow variance and sent this to the 3200, which will act on this during
                this minute - status displayed at the top of the next minute.
        Mainline 2:
            - Zones 4 and 5 are running with low variance
            - 3 minute fill time will delay action for two more minutes
        Mainline 3:
            - Zones 7 and 8 are running with variance but no message
        Mainline 4:
            - Zones 11 and 11 are running with variance but no message
        Mainline 5:
            - Zones 13 and 14 are running with high variance
            - FlowStation has detected a high flow variance and sent this to the 3200, which will act on this during
                this minute - status displayed at the top of the next minute.
        Mainline 6:
            - Zones 16 and 17 are running with low variance
        Mainline 7:
            - Zones 19, 20, and 21 are running with high variance
        Mainline 8:
            - Zones 22, 23, and 24 are running with low variance
            - FlowStation has detected a low flow variance and sent this to the 3200, which will act on this during
                this minute - status displayed at the top of the next minute.

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:04 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify the only water source
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            # ------------------------------------------------------ #
            # ML1 - ADV HIGH FLOW VARIANCE TIER 1 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _variance_percent=self.ml_1_hi_fl_vr,
                _expected_gpm=self.zn_1_df + self.zn_2_df)

            # ------------------------------------------------------ #
            # ML2 - ADV LOW FLOW VARIANCE TIER 1 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[2],
                _variance_percent=self.ml_2_lo_fl_vr,
                _expected_gpm=self.zn_4_df + self.zn_5_df)

            # ------------------------------------------------------ #
            # ML3 - ADV HIGH FLOW VARIANCE TIER 2 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_running()

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[3],
                _variance_percent=self.ml_3_hi_fl_vr,
                _expected_gpm=self.zn_7_df + self.zn_8_df)

            # ------------------------------------------------------ #
            # ML4 - ADV LOW FLOW VARIANCE TIER 2 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(4).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(4).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[4].statuses.verify_status_is_running()

            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[4],
                _variance_percent=self.ml_4_lo_fl_vr,
                _expected_gpm=self.zn_10_df + self.zn_11_df)

            # ------------------------------------------------------ #
            # ML5 - ADV HIGH FLOW VARIANCE TIER 3 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(5).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(5).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(5).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(5).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[5].statuses.verify_status_is_running()

            # Mainline 5 Zones
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[5],
                _variance_percent=self.ml_5_hi_fl_vr,
                _expected_gpm=self.zn_13_df + self.zn_14_df)

            # ------------------------------------------------------ #
            # ML6 - ADV LOW FLOW VARIANCE TIER 3 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(6).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(6).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()

            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[6],
                _variance_percent=self.ml_6_lo_fl_vr,
                _expected_gpm=self.zn_16_df + self.zn_17_df)

            # ------------------------------------------------------ #
            # ML7 - ADV HIGH FLOW VARIANCE TIER 4 W/O SHUTDOWN       #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(7).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(7).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(7).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(7).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[7].statuses.verify_status_is_running()

            # Mainline 7 Zones
            self.config.BaseStation3200[1].zones[19].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[20].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[21].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[7],
                _variance_percent=self.ml_7_hi_fl_vr,
                _expected_gpm=self.zn_19_df + self.zn_20_df + self.zn_21_df)

            # ------------------------------------------------------ #
            # ML8 - ADV LOW FLOW VARIANCE TIER 4 W/ SHUTDOWN         #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[8].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(8).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(8).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[8].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[22].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[23].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[24].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[8],
                _variance_percent=self.ml_8_lo_fl_vr,
                _expected_gpm=self.zn_22_df + self.zn_23_df + self.zn_24_df)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_16(self):
        """
        ############################
        Increment clock from 10:04 - 10:05 AM
        ############################

        Mainline 1:
            - zones 1 and 2 have a strike each
            - zone 1 is running with zone 3 with NO variance
        Mainline 2:
            - Zones 4 and 5 are running with low variance
            - 3 minute fill time will delay action for two more minutes
        Mainline 3:
            - Zones 7 and 8 are running with variance but no message
        Mainline 4:
            - Zones 11 and 11 are running with variance but no message
        Mainline 5:
            - zones 13 and 14 have a strike each
            - zone 13 is running with zone 15 with high variance
        Mainline 6:
            - Zones 16 and 17 are running with low variance
        Mainline 7:
            - Zones 19, 20, and 21 are running with high variance
        Mainline 8:
            - Zones 22, 23, and 24 all have one strike
            - Zone 22 is running by itself with low variance

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:05 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify the only water source
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            # ------------------------------------------------------ #
            # ML1 - ADV HIGH FLOW VARIANCE TIER 1 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _expected_gpm=self.zn_1_df + self.zn_3_df)

            # ------------------------------------------------------ #
            # ML2 - ADV LOW FLOW VARIANCE TIER 1 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[2],
                _variance_percent=self.ml_2_lo_fl_vr,
                _expected_gpm=self.zn_4_df + self.zn_5_df)

            # ------------------------------------------------------ #
            # ML3 - ADV HIGH FLOW VARIANCE TIER 2 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_running()

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[3],
                _variance_percent=self.ml_3_hi_fl_vr,
                _expected_gpm=self.zn_7_df + self.zn_8_df)

            # ------------------------------------------------------ #
            # ML4 - ADV LOW FLOW VARIANCE TIER 2 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(4).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(4).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[4].statuses.verify_status_is_running()

            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[4],
                _variance_percent=self.ml_4_lo_fl_vr,
                _expected_gpm=self.zn_10_df + self.zn_11_df)

            # ------------------------------------------------------ #
            # ML5 - ADV HIGH FLOW VARIANCE TIER 3 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(5).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(5).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(5).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(5).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[5].statuses.verify_status_is_running()

            # Mainline 5 Zones
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[5],
                _variance_percent=self.ml_5_hi_fl_vr,
                _expected_gpm=self.zn_13_df + self.zn_15_df)

            # ------------------------------------------------------ #
            # ML6 - ADV LOW FLOW VARIANCE TIER 3 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(6).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(6).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()

            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[6],
                _variance_percent=self.ml_6_lo_fl_vr,
                _expected_gpm=self.zn_16_df + self.zn_17_df)

            # ------------------------------------------------------ #
            # ML7 - ADV HIGH FLOW VARIANCE TIER 4 W/O SHUTDOWN       #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(7).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(7).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(7).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(7).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[7].statuses.verify_status_is_running()

            # Mainline 7 Zones
            self.config.BaseStation3200[1].zones[19].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[20].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[21].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[7],
                _variance_percent=self.ml_7_hi_fl_vr,
                _expected_gpm=self.zn_19_df + self.zn_20_df + self.zn_21_df)

            # ------------------------------------------------------ #
            # ML8 - ADV LOW FLOW VARIANCE TIER 4 W/ SHUTDOWN         #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[8].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(8).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(8).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[8].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[22].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[23].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[24].statuses.verify_status_is_soaking()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[8],
                _variance_percent=self.ml_8_lo_fl_vr,
                _expected_gpm=self.zn_22_df)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_17(self):
        """
        ############################
        Increment clock from 10:05 - 10:06 AM
        ############################

        Mainline 1:
            - zone 1 is running with zone 3 with NO variance
        Mainline 2:
            - Zones 4 and 5 are running with low variance
            - variance detection sent from FlowStn - act next minute
        Mainline 3:
            - Zones 7 and 8 are running with variance but no message
        Mainline 4:
            - Zones 11 and 11 are running with variance but no message
        Mainline 5:
            - zones 13 and 14 have a strike each
            - zone 13 is running with zone 15 with high variance
        Mainline 6:
            - Zones 16 and 17 are running with low variance
        Mainline 7:
            - Zones 19, 20, and 21 are running with high variance
        Mainline 8:
            - Zone 22 running by itself with low variance

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:06 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify the only water source
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            # ------------------------------------------------------ #
            # ML1 - ADV HIGH FLOW VARIANCE TIER 1 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _expected_gpm=self.zn_1_df + self.zn_3_df)

            # ------------------------------------------------------ #
            # ML2 - ADV LOW FLOW VARIANCE TIER 1 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[2],
                _variance_percent=self.ml_2_lo_fl_vr,
                _expected_gpm=self.zn_4_df + self.zn_5_df)

            # ------------------------------------------------------ #
            # ML3 - ADV HIGH FLOW VARIANCE TIER 2 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_running()

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[3],
                _variance_percent=self.ml_3_hi_fl_vr,
                _expected_gpm=self.zn_7_df + self.zn_8_df)

            # ------------------------------------------------------ #
            # ML4 - ADV LOW FLOW VARIANCE TIER 2 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(4).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(4).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[4].statuses.verify_status_is_running()

            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[4],
                _variance_percent=self.ml_4_lo_fl_vr,
                _expected_gpm=self.zn_10_df + self.zn_11_df)

            # ------------------------------------------------------ #
            # ML5 - ADV HIGH FLOW VARIANCE TIER 3 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(5).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(5).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(5).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(5).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[5].statuses.verify_status_is_running()

            # Mainline 5 Zones
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[5],
                _variance_percent=self.ml_5_hi_fl_vr,
                _expected_gpm=self.zn_13_df + self.zn_15_df)

            # ------------------------------------------------------ #
            # ML6 - ADV LOW FLOW VARIANCE TIER 3 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(6).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(6).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()

            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[6],
                _variance_percent=self.ml_6_lo_fl_vr,
                _expected_gpm=self.zn_16_df + self.zn_17_df)

            # ------------------------------------------------------ #
            # ML7 - ADV HIGH FLOW VARIANCE TIER 4 W/O SHUTDOWN       #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(7).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(7).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(7).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(7).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[7].statuses.verify_status_is_running()

            # Mainline 7 Zones
            self.config.BaseStation3200[1].zones[19].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[20].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[21].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[7],
                _variance_percent=self.ml_7_hi_fl_vr,
                _expected_gpm=self.zn_19_df + self.zn_20_df + self.zn_21_df)

            # ------------------------------------------------------ #
            # ML8 - ADV LOW FLOW VARIANCE TIER 4 W/ SHUTDOWN         #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[8].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(8).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(8).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[8].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[22].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[23].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[24].statuses.verify_status_is_soaking()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[8],
                _variance_percent=self.ml_8_lo_fl_vr,
                _expected_gpm=self.zn_22_df)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_18(self):
        """
        ############################
        Increment clock from 10:06 - 10:07 AM
        ############################

        Mainline 1:
            - zone 1 is running with zone 3 with NO variance
        Mainline 2:
            - Zones 4 and 5 are running with low variance
            - variance detection message - run to completion
        Mainline 3:
            - Zones 7 and 8 are running with variance but no message
        Mainline 4:
            - Zones 11 and 11 are running with variance but no message
        Mainline 5:
            - zones 13 and 14 have a strike each
            - zone 13 is running with zone 15 with high variance
        Mainline 6:
            - Zones 16 and 17 are running with low variance
        Mainline 7:
            - Zones 19, 20, and 21 are running with high variance
        Mainline 8:
            - Zone 22 is running by itself with low variance (one strike)

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:07 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify the only water source
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            # ------------------------------------------------------ #
            # ML1 - ADV HIGH FLOW VARIANCE TIER 1 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].mainlines[2].messages.verify_low_flow_variance_detected_message()

            # zone 2 is off
            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _expected_gpm=self.zn_1_df + self.zn_3_df)

            # ------------------------------------------------------ #
            # ML2 - ADV LOW FLOW VARIANCE TIER 1 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[2],
                _variance_percent=self.ml_2_lo_fl_vr,
                _expected_gpm=self.zn_4_df + self.zn_5_df)

            # ------------------------------------------------------ #
            # ML3 - ADV HIGH FLOW VARIANCE TIER 2 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_running()

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[3],
                _variance_percent=self.ml_3_hi_fl_vr,
                _expected_gpm=self.zn_7_df + self.zn_8_df)

            # ------------------------------------------------------ #
            # ML4 - ADV LOW FLOW VARIANCE TIER 2 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(4).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(4).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[4].statuses.verify_status_is_running()

            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[4],
                _variance_percent=self.ml_4_lo_fl_vr,
                _expected_gpm=self.zn_10_df + self.zn_11_df)

            # ------------------------------------------------------ #
            # ML5 - ADV HIGH FLOW VARIANCE TIER 3 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(5).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(5).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(5).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(5).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[5].statuses.verify_status_is_running()

            # Mainline 5 Zones
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[5],
                _variance_percent=self.ml_5_hi_fl_vr,
                _expected_gpm=self.zn_13_df + self.zn_15_df)

            # ------------------------------------------------------ #
            # ML6 - ADV LOW FLOW VARIANCE TIER 3 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(6).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(6).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()

            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[6],
                _variance_percent=self.ml_6_lo_fl_vr,
                _expected_gpm=self.zn_16_df + self.zn_17_df)

            # ------------------------------------------------------ #
            # ML7 - ADV HIGH FLOW VARIANCE TIER 4 W/O SHUTDOWN       #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(7).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(7).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(7).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(7).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[7].statuses.verify_status_is_running()

            # Mainline 7 Zones
            self.config.BaseStation3200[1].zones[19].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[20].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[21].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[7],
                _variance_percent=self.ml_7_hi_fl_vr,
                _expected_gpm=self.zn_19_df + self.zn_20_df + self.zn_21_df)

            # ------------------------------------------------------ #
            # ML8 - ADV LOW FLOW VARIANCE TIER 4 W/ SHUTDOWN         #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[8].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(8).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(8).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[8].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[22].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[23].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[24].statuses.verify_status_is_soaking()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[8],
                _variance_percent=self.ml_8_lo_fl_vr,
                _expected_gpm=self.zn_22_df)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_19(self):
        """
        ############################
        Increment clock from 10:07 - 10:08 AM
        ############################

        Mainline 1:
            - zone 1 is running with zone 3 with NO variance
            - strike on zone 1 has been cleared
            - zones will run to completion of their run times
        Mainline 2:
            - Zones 4 and 5 are running with low variance
            - variance detection message - run to completion
        Mainline 3:
            - Zones 7 and 8 are running with variance but no message
        Mainline 4:
            - Zones 11 and 11 are running with variance but no message
        Mainline 5:
            - zones 13 and 15 have an additional strike each
            - zone 13 has two strikes and will run by itself with variance
        Mainline 6:
            - Zones 16 and 17 are running with low variance
        Mainline 7:
            - Zones 19, 20, and 21 are running with high variance
        Mainline 8:
            - Zone 22 has picked up its second strike
            - Zone 22 is running by itself with low variance

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:08 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify the only water source
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            # ------------------------------------------------------ #
            # ML1 - ADV HIGH FLOW VARIANCE TIER 1 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()

            # zone 2 is off
            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _expected_gpm=self.zn_1_df + self.zn_3_df)

            # ------------------------------------------------------ #
            # ML2 - ADV LOW FLOW VARIANCE TIER 1 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[2],
                _variance_percent=self.ml_2_lo_fl_vr,
                _expected_gpm=self.zn_4_df + self.zn_5_df)

            # ------------------------------------------------------ #
            # ML3 - ADV HIGH FLOW VARIANCE TIER 2 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_running()

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[3],
                _variance_percent=self.ml_3_hi_fl_vr,
                _expected_gpm=self.zn_7_df + self.zn_8_df)

            # ------------------------------------------------------ #
            # ML4 - ADV LOW FLOW VARIANCE TIER 2 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(4).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(4).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[4].statuses.verify_status_is_running()

            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[4],
                _variance_percent=self.ml_4_lo_fl_vr,
                _expected_gpm=self.zn_10_df + self.zn_11_df)

            # ------------------------------------------------------ #
            # ML5 - ADV HIGH FLOW VARIANCE TIER 3 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(5).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(5).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(5).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(5).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[5].statuses.verify_status_is_running()

            # Mainline 5 Zones
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_soaking()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[5],
                _variance_percent=self.ml_5_hi_fl_vr,
                _expected_gpm=self.zn_13_df)

            # ------------------------------------------------------ #
            # ML6 - ADV LOW FLOW VARIANCE TIER 3 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(6).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(6).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()

            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[6],
                _variance_percent=self.ml_6_lo_fl_vr,
                _expected_gpm=self.zn_16_df + self.zn_17_df)

            # ------------------------------------------------------ #
            # ML7 - ADV HIGH FLOW VARIANCE TIER 4 W/O SHUTDOWN       #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(7).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(7).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(7).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(7).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[7].statuses.verify_status_is_running()

            # Mainline 7 Zones
            self.config.BaseStation3200[1].zones[19].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[20].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[21].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[7],
                _variance_percent=self.ml_7_hi_fl_vr,
                _expected_gpm=self.zn_19_df + self.zn_20_df + self.zn_21_df)

            # ------------------------------------------------------ #
            # ML8 - ADV LOW FLOW VARIANCE TIER 4 W/ SHUTDOWN         #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[8].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(8).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(8).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[8].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[22].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[23].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[24].statuses.verify_status_is_soaking()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[8],
                _variance_percent=self.ml_8_lo_fl_vr,
                _expected_gpm=self.zn_22_df)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_20(self):
        """
        ############################
        Increment clock from 10:08 - 10:09 AM
        ############################

        Mainline 1:
            - zone 1 is running with zone 3 with NO variance
            - zones will run to completion of their run times
        Mainline 2:
            - Zones 4 and 5 are running with low variance
            - variance detection message - run to completion
        Mainline 3:
            - Zones 7 and 8 are running with variance but no message
        Mainline 4:
            - Zones 11 and 11 are running with variance but no message
        Mainline 5:
            - zone 13 has two strikes and will run by itself with variance
        Mainline 6:
            - Zones 16 and 17 are running with low variance
        Mainline 7:
            - Zones 19, 20, and 21 are running with high variance
        Mainline 8:
            - Zone 22 running, looking for strike three - low variance

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:09 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify the only water source
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            # ------------------------------------------------------ #
            # ML1 - ADV HIGH FLOW VARIANCE TIER 1 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _expected_gpm=self.zn_1_df + self.zn_3_df)

            # ------------------------------------------------------ #
            # ML2 - ADV LOW FLOW VARIANCE TIER 1 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[2],
                _variance_percent=self.ml_2_lo_fl_vr,
                _expected_gpm=self.zn_4_df + self.zn_5_df)

            # ------------------------------------------------------ #
            # ML3 - ADV HIGH FLOW VARIANCE TIER 2 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_running()

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[3],
                _variance_percent=self.ml_3_hi_fl_vr,
                _expected_gpm=self.zn_7_df + self.zn_8_df)

            # ------------------------------------------------------ #
            # ML4 - ADV LOW FLOW VARIANCE TIER 2 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(4).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(4).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[4].statuses.verify_status_is_running()

            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[4],
                _variance_percent=self.ml_4_lo_fl_vr,
                _expected_gpm=self.zn_10_df + self.zn_11_df)

            # ------------------------------------------------------ #
            # ML5 - ADV HIGH FLOW VARIANCE TIER 3 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(5).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(5).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(5).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(5).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[5].statuses.verify_status_is_running()

            # Mainline 5 Zones
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_soaking()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[5],
                _variance_percent=self.ml_5_hi_fl_vr,
                _expected_gpm=self.zn_13_df)

            # ------------------------------------------------------ #
            # ML6 - ADV LOW FLOW VARIANCE TIER 3 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(6).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(6).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()

            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[6],
                _variance_percent=self.ml_6_lo_fl_vr,
                _expected_gpm=self.zn_16_df + self.zn_17_df)

            # ------------------------------------------------------ #
            # ML7 - ADV HIGH FLOW VARIANCE TIER 4 W/O SHUTDOWN       #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(7).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(7).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(7).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(7).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[7].statuses.verify_status_is_running()

            # Mainline 7 Zones
            self.config.BaseStation3200[1].zones[19].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[20].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[21].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[7],
                _variance_percent=self.ml_7_hi_fl_vr,
                _expected_gpm=self.zn_19_df + self.zn_20_df + self.zn_21_df)

            # ------------------------------------------------------ #
            # ML8 - ADV LOW FLOW VARIANCE TIER 4 W/ SHUTDOWN         #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[8].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(8).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(8).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[8].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[22].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[23].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[24].statuses.verify_status_is_soaking()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[8],
                _variance_percent=self.ml_8_lo_fl_vr,
                _expected_gpm=self.zn_22_df)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_21(self):
        """
        ############################
        Increment clock from 10:09 - 10:10 AM
        ############################

        Mainline 1:
            - zone 1 is running with zone 3 with NO variance
            - zones will run to completion of their run times
        Mainline 2:
            - Zones 4 and 5 are running with low variance
            - variance detection message - run to completion
        Mainline 3:
            - Zones 7 and 8 are running with variance but no message
        Mainline 4:
            - Zones 11 and 11 are running with variance but no message
        Mainline 5:
            - the third variance has been detected and the strikeout will be processed during the next minute
        Mainline 6:
            - Zones 16 and 17 are running with low variance
        Mainline 7:
            - Zones 19, 20, and 21 are running with high variance
        Mainline 8:
            - the third variance has been detected and the strikeout will be processed during the next minute

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:10 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify the only water source
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            # ------------------------------------------------------ #
            # ML1 - ADV HIGH FLOW VARIANCE TIER 1 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()

            # zone 2 is off
            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _expected_gpm=self.zn_1_df + self.zn_3_df)

            # ------------------------------------------------------ #
            # ML2 - ADV LOW FLOW VARIANCE TIER 1 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[2],
                _variance_percent=self.ml_2_lo_fl_vr,
                _expected_gpm=self.zn_4_df + self.zn_5_df)

            # ------------------------------------------------------ #
            # ML3 - ADV HIGH FLOW VARIANCE TIER 2 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_running()

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[3],
                _variance_percent=self.ml_3_hi_fl_vr,
                _expected_gpm=self.zn_7_df + self.zn_8_df)

            # ------------------------------------------------------ #
            # ML4 - ADV LOW FLOW VARIANCE TIER 2 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(4).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(4).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[4].statuses.verify_status_is_running()

            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[4],
                _variance_percent=self.ml_4_lo_fl_vr,
                _expected_gpm=self.zn_10_df + self.zn_11_df)

            # ------------------------------------------------------ #
            # ML5 - ADV HIGH FLOW VARIANCE TIER 3 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(5).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(5).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(5).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(5).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[5].statuses.verify_status_is_running()

            # Mainline 5 Zones
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_soaking()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[5],
                _variance_percent=self.ml_5_hi_fl_vr,
                _expected_gpm=self.zn_13_df)

            # ------------------------------------------------------ #
            # ML6 - ADV LOW FLOW VARIANCE TIER 3 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(6).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(6).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()

            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[6],
                _variance_percent=self.ml_6_lo_fl_vr,
                _expected_gpm=self.zn_16_df + self.zn_17_df)

            # ------------------------------------------------------ #
            # ML7 - ADV HIGH FLOW VARIANCE TIER 4 W/O SHUTDOWN       #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(7).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(7).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(7).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(7).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[7].statuses.verify_status_is_running()

            # Mainline 7 Zones
            self.config.BaseStation3200[1].zones[19].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[20].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[21].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[7],
                _variance_percent=self.ml_7_hi_fl_vr,
                _expected_gpm=self.zn_19_df + self.zn_20_df + self.zn_21_df)

            self.config.BaseStation3200[1].mainlines[7].messages.verify_high_flow_variance_detected_message()

            # ------------------------------------------------------ #
            # ML8 - ADV LOW FLOW VARIANCE TIER 4 W/ SHUTDOWN         #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[8].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(8).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(8).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(8).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[8].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[22].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[23].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[24].statuses.verify_status_is_soaking()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[8],
                _variance_percent=self.ml_8_lo_fl_vr,
                _expected_gpm=self.zn_22_df)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_22(self):
        """
        ############################
        Increment clock from 10:10 - 10:11 AM
        ############################

        Mainline 1:
            - zone 1 is running with zone 3 with NO variance
            - zones will run to completion of their run times
        Mainline 2:
            - Zones 4 and 5 are running with low variance
            - variance detection message - run to completion
        Mainline 3:
            - Zones 7 and 8 are running with variance but no message
        Mainline 4:
            - Zones 11 and 11 are running with variance but no message
        Mainline 5:
            - three strike shutdown - all off
        Mainline 6:
            - Zones 16 and 17 are running with low variance
        Mainline 7:
            - Zones 19, 20, and 21 are running with high variance
        Mainline 8:
            - three strike shutdown - all off

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:11 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify the only water source
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            # ------------------------------------------------------ #
            # ML1 - ADV HIGH FLOW VARIANCE TIER 1 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()

            # zone 2 is off
            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _expected_gpm=self.zn_1_df + self.zn_3_df)

            # ------------------------------------------------------ #
            # ML2 - ADV LOW FLOW VARIANCE TIER 1 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[2],
                _variance_percent=self.ml_2_lo_fl_vr,
                _expected_gpm=self.zn_4_df + self.zn_5_df)

            # ------------------------------------------------------ #
            # ML3 - ADV HIGH FLOW VARIANCE TIER 2 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_running()

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[3],
                _variance_percent=self.ml_3_hi_fl_vr,
                _expected_gpm=self.zn_7_df + self.zn_8_df)

            # ------------------------------------------------------ #
            # ML4 - ADV LOW FLOW VARIANCE TIER 2 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(4).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(4).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[4].statuses.verify_status_is_running()

            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[4],
                _variance_percent=self.ml_4_lo_fl_vr,
                _expected_gpm=self.zn_10_df + self.zn_11_df)

            # ------------------------------------------------------ #
            # ML5 - ADV HIGH FLOW VARIANCE TIER 3 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_done()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(5).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(5).statuses.verify_status_is_flow_fault()

            self.config.FlowStations[1].get_point_of_control(5).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(5).statuses.verify_status_is_flow_fault()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[5].statuses.verify_status_is_running()

            # Mainline 5 Zones
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_error()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].mainlines[5].messages.verify_high_flow_variance_shutdown_message(
                _variance_expected_value=self.zn_13_df)

            # ------------------------------------------------------ #
            # ML6 - ADV LOW FLOW VARIANCE TIER 3 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(6).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(6).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()

            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_waiting_to_water()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[6],
                _variance_percent=self.ml_6_lo_fl_vr,
                _expected_gpm=self.zn_16_df + self.zn_17_df)

            # ------------------------------------------------------ #
            # ML7 - ADV HIGH FLOW VARIANCE TIER 4 W/O SHUTDOWN       #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(7).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(7).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(7).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(7).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[7].statuses.verify_status_is_running()

            # Mainline 7 Zones
            self.config.BaseStation3200[1].zones[19].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[20].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[21].statuses.verify_status_is_watering()

            # zone are almost done
            self.config.BaseStation3200[1].flow_meters[7].bicoder.set_flow_rate(_gallons_per_minute=0)

            # ------------------------------------------------------ #
            # ML8 - ADV LOW FLOW VARIANCE TIER 4 W/ SHUTDOWN         #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[8].statuses.verify_status_is_done()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(8).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(8).statuses.verify_status_is_flow_fault()

            self.config.FlowStations[1].get_point_of_control(8).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(8).statuses.verify_status_is_flow_fault()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[8].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[22].statuses.verify_status_is_error()
            self.config.BaseStation3200[1].zones[23].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[24].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].mainlines[8].messages.verify_low_flow_variance_shutdown_message(
                _variance_expected_value=float(self.zn_22_df))

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_23(self):
        """
        ############################
        Increment clock from 10:11 - 10:12 AM
        ############################

        Mainline 1:
            - zone 1 is running with zone 3 with NO variance
            - zones will run to completion of their run times
            - zone 1 will complete its watering time
        Mainline 2:
            - Zones 4 and 5 are done
            - Zone 6 is running with variance
            - variance detection message - run zone 6 to completion
        Mainline 3:
            - Zones 7 and 8 are done
            - Zone 9 is running with variance but no message
        Mainline 4:
            - Zones 10 and 11 are done
            - Zone 12 is running with variance but no message
        Mainline 5:
            - three strikes - all off
        Mainline 6:
            - Zones 16 and 17 are done (run time)
            - Zone 18 is watering
        Mainline 7:
            - Zones 19, 20, and 21 are done (run time)
        Mainline 8:
            - three strikes - all off

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:12 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify the only water source
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            # ------------------------------------------------------ #
            # ML1 - ADV HIGH FLOW VARIANCE TIER 1 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Flow Sources
            # fails here when POC shut off by FlowStation
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()

            # zone 2 is off
            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _expected_gpm=self.zn_1_df + self.zn_3_df)

            # ------------------------------------------------------ #
            # ML2 - ADV LOW FLOW VARIANCE TIER 1 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[2],
                _variance_percent=self.ml_2_lo_fl_vr,
                _expected_gpm=self.zn_6_df)

            # ------------------------------------------------------ #
            # ML3 - ADV HIGH FLOW VARIANCE TIER 2 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_running()

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[3],
                _variance_percent=self.ml_3_hi_fl_vr,
                _expected_gpm=self.zn_9_df)

            # ------------------------------------------------------ #
            # ML4 - ADV LOW FLOW VARIANCE TIER 2 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(4).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(4).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[4].statuses.verify_status_is_running()

            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[4],
                _variance_percent=self.ml_4_lo_fl_vr,
                _expected_gpm=self.zn_12_df)

            # ------------------------------------------------------ #
            # ML5 - ADV HIGH FLOW VARIANCE TIER 3 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            # set flow to zero as everything is off
            self.config.BaseStation3200[1].flow_meters[5].bicoder.set_flow_rate(_gallons_per_minute=0)
            self.config.BaseStation3200[1].flow_meters[5].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_done()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(5).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(5).statuses.verify_status_is_flow_fault()

            self.config.FlowStations[1].get_point_of_control(5).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(5).statuses.verify_status_is_flow_fault()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[5].statuses.verify_status_is_ok()

            # Mainline 5 Zones
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_error()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_done()

            # ------------------------------------------------------ #
            # ML6 - ADV LOW FLOW VARIANCE TIER 3 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(6).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(6).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()

            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[6],
                _variance_percent=self.ml_6_lo_fl_vr,
                _expected_gpm=self.zn_18_df)

            # ------------------------------------------------------ #
            # ML7 - ADV HIGH FLOW VARIANCE TIER 4 W/O SHUTDOWN       #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_done()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(7).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(7).statuses.verify_status_is_off()

            self.config.FlowStations[1].get_point_of_control(7).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(7).statuses.verify_status_is_off()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[7].statuses.verify_status_is_ok()

            # Mainline 7 Zones
            self.config.BaseStation3200[1].zones[19].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[20].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[21].statuses.verify_status_is_done()

            # ------------------------------------------------------ #
            # ML8 - ADV LOW FLOW VARIANCE TIER 4 W/ SHUTDOWN         #
            # ------------------------------------------------------ #

            # set flow to zero as everything is off
            self.config.BaseStation3200[1].flow_meters[8].bicoder.set_flow_rate(_gallons_per_minute=0)
            self.config.BaseStation3200[1].flow_meters[8].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].programs[8].statuses.verify_status_is_done()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(8).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(8).statuses.verify_status_is_flow_fault()

            self.config.FlowStations[1].get_point_of_control(8).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(8).statuses.verify_status_is_flow_fault()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[8].statuses.verify_status_is_ok()

            self.config.BaseStation3200[1].zones[22].statuses.verify_status_is_error()
            self.config.BaseStation3200[1].zones[23].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[24].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_24(self):
        """
        ############################
        Increment clock from 10:12 - 10:13 AM
        ############################

        Mainline 1:
            - zone 1 is done
            - zone 2 and 3 are running with variance
        Mainline 2:
            - Zones 4 and 5 are done
            - Zone 6 is running with variance
            - variance detection message - run zone 6 to completion
        Mainline 3:
            - Zones 7 and 8 are done
            - Zone 9 is running with variance but no message
        Mainline 4:
            - Zones 10 and 11 are done
            - Zone 12 is running with variance but no message
        Mainline 5:
            - mainline shutdown - all off
        Mainline 6:
            - Zone 18 is running with low variance
        Mainline 7:
            - Zones 19, 20, and 21 are done (run time)
        Mainline 8:
            - three strikes - all off

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:13 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify the only water source
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            # ------------------------------------------------------ #
            # ML1 - ADV HIGH FLOW VARIANCE TIER 1 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _variance_percent=self.ml_1_hi_fl_vr,
                _expected_gpm=self.zn_2_df + self.zn_3_df)

            # ------------------------------------------------------ #
            # ML2 - ADV LOW FLOW VARIANCE TIER 1 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[2],
                _variance_percent=self.ml_2_lo_fl_vr,
                _expected_gpm=self.zn_6_df)

            # ------------------------------------------------------ #
            # ML3 - ADV HIGH FLOW VARIANCE TIER 2 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_running()

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[3],
                _variance_percent=self.ml_3_hi_fl_vr,
                _expected_gpm=self.zn_9_df)

            # ------------------------------------------------------ #
            # ML4 - ADV LOW FLOW VARIANCE TIER 2 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(4).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(4).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[4].statuses.verify_status_is_running()

            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[4],
                _variance_percent=self.ml_4_lo_fl_vr,
                _expected_gpm=self.zn_12_df)

            # ------------------------------------------------------ #
            # ML5 - ADV HIGH FLOW VARIANCE TIER 3 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            # ----- Everything Off -----

            # ------------------------------------------------------ #
            # ML6 - ADV LOW FLOW VARIANCE TIER 3 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(6).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(6).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()

            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[6],
                _variance_percent=self.ml_6_lo_fl_vr,
                _expected_gpm=self.zn_18_df)

            # ------------------------------------------------------ #
            # ML7 - ADV HIGH FLOW VARIANCE TIER 4 W/O SHUTDOWN       #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_done()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(7).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(7).statuses.verify_status_is_off()

            self.config.FlowStations[1].get_point_of_control(7).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(7).statuses.verify_status_is_off()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[7].statuses.verify_status_is_ok()

            # Mainline 7 Zones
            self.config.BaseStation3200[1].zones[19].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[20].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[21].statuses.verify_status_is_done()

            # ------------------------------------------------------ #
            # ML8 - ADV LOW FLOW VARIANCE TIER 4 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            # ----- Everything Off -----

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_25(self):
        """
        ############################
        Increment clock from 10:13 - 10:14 AM
        ############################

        Mainline 1:
            - zone 1 is done
            - zone 2 and 3 are running with variance
        Mainline 2:
            - Zones 4 and 5 are done
            - Zone 6 is running with variance
            - variance detection message - run zone 6 to completion
        Mainline 3:
            - Zones 7 and 8 are done
            - Zone 9 is running with variance but no message
        Mainline 4:
            - Zones 10 and 11 are done
            - Zone 12 is running with variance but no message
        Mainline 5:
            - mainline shutdown - all off
        Mainline 6:
            - Zone 18 is running with low variance
        Mainline 7:
            - Zones 19, 20, and 21 are done (run time)
        Mainline 8:
            - three strikes - all off

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:14 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify the only water source
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            # ------------------------------------------------------ #
            # ML1 - ADV HIGH FLOW VARIANCE TIER 1 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _variance_percent=self.ml_1_hi_fl_vr,
                _expected_gpm=self.zn_2_df + self.zn_3_df)

            # ------------------------------------------------------ #
            # ML2 - ADV LOW FLOW VARIANCE TIER 1 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[2],
                _variance_percent=self.ml_2_lo_fl_vr,
                _expected_gpm=self.zn_6_df)

            # ------------------------------------------------------ #
            # ML3 - ADV HIGH FLOW VARIANCE TIER 2 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_running()

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[3],
                _variance_percent=self.ml_3_hi_fl_vr,
                _expected_gpm=self.zn_9_df)

            # ------------------------------------------------------ #
            # ML4 - ADV LOW FLOW VARIANCE TIER 2 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(4).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(4).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[4].statuses.verify_status_is_running()

            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[4],
                _variance_percent=self.ml_4_lo_fl_vr,
                _expected_gpm=self.zn_12_df)

            # ------------------------------------------------------ #
            # ML5 - ADV HIGH FLOW VARIANCE TIER 3 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            # ----- Everything Off -----

            # ------------------------------------------------------ #
            # ML6 - ADV LOW FLOW VARIANCE TIER 3 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(6).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(6).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()

            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[6],
                _variance_percent=self.ml_6_lo_fl_vr,
                _expected_gpm=self.zn_18_df)

            # ------------------------------------------------------ #
            # ML7 - ADV HIGH FLOW VARIANCE TIER 4 W/O SHUTDOWN       #
            # ------------------------------------------------------ #

            # ----- Everything Off -----

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_26(self):
        """
        ############################
        Increment clock from 10:14 - 10:15 AM
        ############################

        Mainline 1:
            - zone 1 and zone 3 are done
            - zone 2 is running with variance
            - the "fill time" has been reset on this zone change
        Mainline 2:
            - Zones 4 and 5 are done
            - Zone 6 is running with variance
            - variance detection message - run zone 6 to completion
        Mainline 3:
            - Zones 7 and 8 are done
            - Zone 9 is running with variance but no message
        Mainline 4:
            - Zones 10 and 11 are done
            - Zone 12 is running with variance but no message
        Mainline 5:
            - mainline shutdown - all off
        Mainline 6:
            - Zone 18 is running with low variance
        Mainline 7:
            - Zones 19, 20, and 21 are done (run time)
        Mainline 8:
            - three strikes - all off

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:15 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify the only water source
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            # ------------------------------------------------------ #
            # ML1 - ADV HIGH FLOW VARIANCE TIER 1 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _variance_percent=self.ml_1_hi_fl_vr,
                _expected_gpm=self.zn_2_df)

            # ------------------------------------------------------ #
            # ML2 - ADV LOW FLOW VARIANCE TIER 1 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[2],
                _variance_percent=self.ml_2_lo_fl_vr,
                _expected_gpm=self.zn_6_df)

            # ------------------------------------------------------ #
            # ML3 - ADV HIGH FLOW VARIANCE TIER 2 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_running()

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[3],
                _variance_percent=self.ml_3_hi_fl_vr,
                _expected_gpm=self.zn_9_df)

            # ------------------------------------------------------ #
            # ML4 - ADV LOW FLOW VARIANCE TIER 2 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(4).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(4).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[4].statuses.verify_status_is_running()

            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[4],
                _variance_percent=self.ml_4_lo_fl_vr,
                _expected_gpm=self.zn_12_df)

            # ------------------------------------------------------ #
            # ML5 - ADV HIGH FLOW VARIANCE TIER 3 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            # ----- Everything Off -----

            # ------------------------------------------------------ #
            # ML6 - ADV LOW FLOW VARIANCE TIER 3 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(6).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(6).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()

            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[6],
                _variance_percent=self.ml_6_lo_fl_vr,
                _expected_gpm=self.zn_18_df)

            # ------------------------------------------------------ #
            # ML7 - ADV HIGH FLOW VARIANCE TIER 4 W/O SHUTDOWN       #
            # ------------------------------------------------------ #

            # ----- Everything Off -----

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_27(self):
        """
        ############################
        Increment clock from 10:15 - 10:16 AM
        ############################

        Mainline 1:
            - zone 2 is running with variance
        Mainline 2:
            - Zones 4 and 5 are done
            - Zone 6 is running with variance
            - variance detection message - run zone 6 to completion
        Mainline 3:
            - Zones 7 and 8 are done
            - Zone 9 is running with variance but no message
        Mainline 4:
            - Zones 10 and 11 are done
            - Zone 12 is running with variance but no message
        Mainline 5:
            - mainline shutdown - all off
        Mainline 6:
            - Zone 18 is running with low variance
        Mainline 7:
            - Zones 19, 20, and 21 are done (run time)
        Mainline 8:
            - three strikes - all off

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:16 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify the only water source
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            # ------------------------------------------------------ #
            # ML1 - ADV HIGH FLOW VARIANCE TIER 1 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _variance_percent=self.ml_1_hi_fl_vr,
                _expected_gpm=self.zn_2_df)

            # ------------------------------------------------------ #
            # ML2 - ADV LOW FLOW VARIANCE TIER 1 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[2],
                _variance_percent=self.ml_2_lo_fl_vr,
                _expected_gpm=self.zn_6_df)

            # ------------------------------------------------------ #
            # ML3 - ADV HIGH FLOW VARIANCE TIER 2 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_running()

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[3],
                _variance_percent=self.ml_3_hi_fl_vr,
                _expected_gpm=self.zn_9_df)

            # ------------------------------------------------------ #
            # ML4 - ADV LOW FLOW VARIANCE TIER 2 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(4).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(4).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[4].statuses.verify_status_is_running()

            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[4],
                _variance_percent=self.ml_4_lo_fl_vr,
                _expected_gpm=self.zn_12_df)

            # ------------------------------------------------------ #
            # ML5 - ADV HIGH FLOW VARIANCE TIER 3 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            # ----- Everything Off -----

            # ------------------------------------------------------ #
            # ML6 - ADV LOW FLOW VARIANCE TIER 3 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(6).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(6).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()

            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[6],
                _variance_percent=self.ml_6_lo_fl_vr,
                _expected_gpm=self.zn_18_df)

            # ------------------------------------------------------ #
            # ML7 - ADV HIGH FLOW VARIANCE TIER 4 W/O SHUTDOWN       #
            # ------------------------------------------------------ #

            # ----- Everything Off -----

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_28(self):
        """
        ############################
        Increment clock from 10:16 - 10:17 AM
        ############################

        Mainline 1:
            - zone 2 is running with variance
        Mainline 2:
            - Zones 4 and 5 are done
            - Zone 6 is running with variance
            - variance detection message - run zone 6 to completion
        Mainline 3:
            - Zones 7 and 8 are done
            - Zone 9 is running with variance but no message
        Mainline 4:
            - Zones 10 and 11 are done
            - Zone 12 is running with variance but no message
        Mainline 5:
            - mainline shutdown - all off
        Mainline 6:
            - Zone 18 is running with low variance
        Mainline 7:
            - Zones 19, 20, and 21 are done (run time)
        Mainline 8:
            - three strikes - all off

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:17 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify the only water source
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            # ------------------------------------------------------ #
            # ML1 - ADV HIGH FLOW VARIANCE TIER 1 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _variance_percent=self.ml_1_hi_fl_vr,
                _expected_gpm=self.zn_2_df)

            # ------------------------------------------------------ #
            # ML2 - ADV LOW FLOW VARIANCE TIER 1 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[2],
                _variance_percent=self.ml_2_lo_fl_vr,
                _expected_gpm=self.zn_6_df)

            # ------------------------------------------------------ #
            # ML3 - ADV HIGH FLOW VARIANCE TIER 2 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_running()

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[3],
                _variance_percent=self.ml_3_hi_fl_vr,
                _expected_gpm=self.zn_9_df)

            # ------------------------------------------------------ #
            # ML4 - ADV LOW FLOW VARIANCE TIER 2 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(4).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(4).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[4].statuses.verify_status_is_running()

            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[4],
                _variance_percent=self.ml_4_lo_fl_vr,
                _expected_gpm=self.zn_12_df)

            # ------------------------------------------------------ #
            # ML5 - ADV HIGH FLOW VARIANCE TIER 3 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            # ----- Everything Off -----

            # ------------------------------------------------------ #
            # ML6 - ADV LOW FLOW VARIANCE TIER 3 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(6).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(6).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()

            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[6],
                _variance_percent=self.ml_6_lo_fl_vr,
                _expected_gpm=self.zn_18_df)

            # ------------------------------------------------------ #
            # ML7 - ADV HIGH FLOW VARIANCE TIER 4 W/O SHUTDOWN       #
            # ------------------------------------------------------ #

            # ----- Everything Off -----

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_29(self):
        """
        ############################
        Increment clock from 10:17 - 10:18 AM
        ############################

        Mainline 1:
            - zone 2 is running with variance
        Mainline 2:
            - Zones 4 and 5 are done
            - Zone 6 is running with variance
            - variance detection message - run zone 6 to completion
        Mainline 3:
            - Zones 7 and 8 are done
            - Zone 9 is running with variance but no message
        Mainline 4:
            - Zones 10 and 11 are done
            - Zone 12 is running with variance but no message
        Mainline 5:
            - mainline shutdown - all off
        Mainline 6:
            - Zone 18 is running with low variance
        Mainline 7:
            - Zones 19, 20, and 21 are done (run time)
        Mainline 8:
            - three strikes - all off

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:18 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify the only water source
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            # ------------------------------------------------------ #
            # ML1 - ADV HIGH FLOW VARIANCE TIER 1 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _variance_percent=self.ml_1_hi_fl_vr,
                _expected_gpm=self.zn_2_df)

            # ------------------------------------------------------ #
            # ML2 - ADV LOW FLOW VARIANCE TIER 1 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[2],
                _variance_percent=self.ml_2_lo_fl_vr,
                _expected_gpm=self.zn_6_df)

            # ------------------------------------------------------ #
            # ML3 - ADV HIGH FLOW VARIANCE TIER 2 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_running()

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[3],
                _variance_percent=self.ml_3_hi_fl_vr,
                _expected_gpm=self.zn_9_df)

            # ------------------------------------------------------ #
            # ML4 - ADV LOW FLOW VARIANCE TIER 2 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(4).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(4).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[4].statuses.verify_status_is_running()

            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[4],
                _variance_percent=self.ml_4_lo_fl_vr,
                _expected_gpm=self.zn_12_df)

            # ------------------------------------------------------ #
            # ML5 - ADV HIGH FLOW VARIANCE TIER 3 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            # ----- Everything Off -----

            # ------------------------------------------------------ #
            # ML6 - ADV LOW FLOW VARIANCE TIER 3 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(6).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(6).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()

            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[6],
                _variance_percent=self.ml_6_lo_fl_vr,
                _expected_gpm=self.zn_18_df)

            # ------------------------------------------------------ #
            # ML7 - ADV HIGH FLOW VARIANCE TIER 4 W/O SHUTDOWN       #
            # ------------------------------------------------------ #

            # ----- Everything Off -----

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_30(self):
        """
        ############################
        Increment clock from 10:18 - 10:19 AM
        ############################

        Mainline 1:
            - zone 2 is running with variance
        Mainline 2:
            - Zones 4 and 5 are done
            - Zone 6 is running with variance
            - variance detection message - run zone 6 to completion
        Mainline 3:
            - Zones 7 and 8 are done
            - Zone 9 is running with variance but no message
        Mainline 4:
            - Zones 10 and 11 are done
            - Zone 12 is running with variance but no message
        Mainline 5:
            - mainline shutdown - all off
        Mainline 6:
            - Zone 18 is running with low variance
        Mainline 7:
            - Zones 19, 20, and 21 are done (run time)
        Mainline 8:
            - three strikes - all off

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:19 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify the only water source
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            # ------------------------------------------------------ #
            # ML1 - ADV HIGH FLOW VARIANCE TIER 1 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            # Fails here...
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _variance_percent=self.ml_1_hi_fl_vr,
                _expected_gpm=self.zn_2_df)

            # ------------------------------------------------------ #
            # ML2 - ADV LOW FLOW VARIANCE TIER 1 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[2],
                _variance_percent=self.ml_2_lo_fl_vr,
                _expected_gpm=self.zn_6_df)

            # ------------------------------------------------------ #
            # ML3 - ADV HIGH FLOW VARIANCE TIER 2 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_running()

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[3],
                _variance_percent=self.ml_3_hi_fl_vr,
                _expected_gpm=self.zn_9_df)

            # ------------------------------------------------------ #
            # ML4 - ADV LOW FLOW VARIANCE TIER 2 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(4).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(4).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[4].statuses.verify_status_is_running()

            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[4],
                _variance_percent=self.ml_4_lo_fl_vr,
                _expected_gpm=self.zn_12_df)

            # ------------------------------------------------------ #
            # ML5 - ADV HIGH FLOW VARIANCE TIER 3 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            # ----- Everything Off -----

            # ------------------------------------------------------ #
            # ML6 - ADV LOW FLOW VARIANCE TIER 3 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(6).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(6).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()

            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[6],
                _variance_percent=self.ml_6_lo_fl_vr,
                _expected_gpm=self.zn_18_df)

            # ------------------------------------------------------ #
            # ML7 - ADV HIGH FLOW VARIANCE TIER 4 W/O SHUTDOWN       #
            # ------------------------------------------------------ #

            # ----- Everything Off -----

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_31(self):
        """
        ############################
        Increment clock from 10:19 - 10:20 AM
        ############################

        Mainline 1:
            - zone 2 is running with variance
        Mainline 2:
            - Zones 4 and 5 are done
            - Zone 6 is running with variance
            - variance detection message - run zone 6 to completion
        Mainline 3:
            - Zones 7 and 8 are done
            - Zone 9 is running with variance but no message
        Mainline 4:
            - Zones 10 and 11 are done
            - Zone 12 is running with variance but no message
        Mainline 5:
            - mainline shutdown - all off
        Mainline 6:
            - Zone 18 is running with low variance
        Mainline 7:
            - Zones 19, 20, and 21 are done (run time)
        Mainline 8:
            - three strikes - all off

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:20 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify the only water source
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            # ------------------------------------------------------ #
            # ML1 - ADV HIGH FLOW VARIANCE TIER 1 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _variance_percent=self.ml_1_hi_fl_vr,
                _expected_gpm=self.zn_2_df)

            # ------------------------------------------------------ #
            # ML2 - ADV LOW FLOW VARIANCE TIER 1 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[2],
                _variance_percent=self.ml_2_lo_fl_vr,
                _expected_gpm=self.zn_6_df)

            # ------------------------------------------------------ #
            # ML3 - ADV HIGH FLOW VARIANCE TIER 2 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_running()

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[3],
                _variance_percent=self.ml_3_hi_fl_vr,
                _expected_gpm=self.zn_9_df)

            # ------------------------------------------------------ #
            # ML4 - ADV LOW FLOW VARIANCE TIER 2 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(4).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(4).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[4].statuses.verify_status_is_running()

            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[4],
                _variance_percent=self.ml_4_lo_fl_vr,
                _expected_gpm=self.zn_12_df)

            # ------------------------------------------------------ #
            # ML5 - ADV HIGH FLOW VARIANCE TIER 3 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            # ----- Everything Off -----

            # ------------------------------------------------------ #
            # ML6 - ADV LOW FLOW VARIANCE TIER 3 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(6).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(6).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()

            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_watering()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[6],
                _variance_percent=self.ml_6_lo_fl_vr,
                _expected_gpm=self.zn_18_df)

            # ------------------------------------------------------ #
            # ML7 - ADV HIGH FLOW VARIANCE TIER 4 W/O SHUTDOWN       #
            # ------------------------------------------------------ #

            # ----- Everything Off -----

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_32(self):
        """
        ############################
        Increment clock from 10:20 - 10:21 AM
        ############################

        Mainline 1:
            - high flow variance detected on the FlowStation, high flow variance sent in the packet to the 3200
            - We need to wait an extra minute for it to be acted on and status' updated by the 3200
        Mainline 2:
            - Zones 4 and 5 are done
            - Zone 6 is running with variance
            - variance detection message - run zone 6 to completion
        Mainline 3:
            - Zones 7 and 8 are done
            - Zone 9 is running with variance but no message
        Mainline 4:
            - Zones 10 and 11 are done
            - Zone 12 is running with variance but no message
        Mainline 5:
            - mainline shutdown - all off
        Mainline 6:
            - Zone 18 is running with low variance
        Mainline 7:
            - Zones 19, 20, and 21 are done (run time)
        Mainline 8:
            - three strikes - all off

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:21 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify the only water source
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_running()

            # ------------------------------------------------------ #
            # ML1 - ADV HIGH FLOW VARIANCE TIER 1 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _variance_percent=self.ml_1_hi_fl_vr,
                _expected_gpm=self.zn_2_df)

            # ------------------------------------------------------ #
            # ML2 - ADV LOW FLOW VARIANCE TIER 1 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(2).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].flow_meters[2].bicoder.set_flow_rate(_gallons_per_minute=0)

            # ------------------------------------------------------ #
            # ML3 - ADV HIGH FLOW VARIANCE TIER 2 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(3).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_running()

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].flow_meters[3].bicoder.set_flow_rate(_gallons_per_minute=0)

            # ------------------------------------------------------ #
            # ML4 - ADV LOW FLOW VARIANCE TIER 2 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(4).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(4).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(4).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[4].statuses.verify_status_is_running()

            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].flow_meters[4].bicoder.set_flow_rate(_gallons_per_minute=0)

            # ------------------------------------------------------ #
            # ML5 - ADV HIGH FLOW VARIANCE TIER 3 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            # ----- Everything Off -----

            # ------------------------------------------------------ #
            # ML6 - ADV LOW FLOW VARIANCE TIER 3 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.BaseStation3200[1].get_mainline(6).statuses.verify_status_is_running()

            self.config.FlowStations[1].get_point_of_control(6).statuses.verify_status_is_running()
            self.config.FlowStations[1].get_mainline(6).statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()

            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].flow_meters[6].bicoder.set_flow_rate(_gallons_per_minute=0)

            # ------------------------------------------------------ #
            # ML7 - ADV HIGH FLOW VARIANCE TIER 4 W/O SHUTDOWN       #
            # ------------------------------------------------------ #

            # ----- Everything Off -----

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_33(self):
        """
        ############################
        Increment clock from 10:21 - 10:22 AM
        ############################

        Mainline 1:
            - high flow variance shutdown message on both the mainline and its struck out zone
            - all zones done, zone 2 is error because it received 3 strikes
            - we also clear the message here so we can verify statuses go back to normal in the next step
        Mainline 2:
            - Zones 4, 5, and 6 are done
            - variance detection message
        Mainline 3:
            - Zones 7, 8, and 9 are done
        Mainline 4:
            - Zones 10, 11, and 12 are done
        Mainline 5:
            - high flow variance shutdown message
            - all zones done
        Mainline 6:
            - Zone 18 is running with low variance
        Mainline 7:
            - Zones 19, 20, and 21 are done (run time)
        Mainline 8:
            - three strikes - all off

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:22 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify the only water source
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_ok()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_ok()

            # ------------------------------------------------------ #
            # ML1 - ADV HIGH FLOW VARIANCE TIER 1 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_flow_fault()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_flow_fault()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_error()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].mainlines[1].messages.verify_high_flow_variance_shutdown_message(
                _variance_expected_value=self.zn_2_df)
            self.config.BaseStation3200[1].mainlines[1].messages.clear_high_flow_variance_shutdown_message()

            # Set the flow meters rate to 0 now that a shutdown happened
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=0.0)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].messages.verify_shutdown_on_high_flow_variance_message()
            self.config.BaseStation3200[1].programs[1].zone_programs[2].messages.clear_shutdown_on_high_flow_variance_message()

            # ------------------------------------------------------ #
            # ML2 - ADV LOW FLOW VARIANCE TIER 1 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(2).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(2).statuses.verify_status_is_off()

            self.config.FlowStations[1].get_point_of_control(2).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(2).statuses.verify_status_is_off()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_ok()

            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_done()

            # ------------------------------------------------------ #
            # ML3 - ADV HIGH FLOW VARIANCE TIER 2 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(3).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(3).statuses.verify_status_is_off()

            self.config.FlowStations[1].get_point_of_control(3).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(3).statuses.verify_status_is_off()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_ok()

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_done()

            # ------------------------------------------------------ #
            # ML4 - ADV LOW FLOW VARIANCE TIER 2 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_done()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(4).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(4).statuses.verify_status_is_off()

            self.config.FlowStations[1].get_point_of_control(4).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(4).statuses.verify_status_is_off()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[4].statuses.verify_status_is_ok()

            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_done()

            # ------------------------------------------------------ #
            # ML5 - ADV HIGH FLOW VARIANCE TIER 3 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            # ----- Everything Off -----

            # ------------------------------------------------------ #
            # ML6 - ADV LOW FLOW VARIANCE TIER 3 W/O SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_done()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(6).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(6).statuses.verify_status_is_off()

            self.config.FlowStations[1].get_point_of_control(6).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(6).statuses.verify_status_is_off()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_ok()

            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_done()

            # ------------------------------------------------------ #
            # ML7 - ADV HIGH FLOW VARIANCE TIER 4 W/O SHUTDOWN       #
            # ------------------------------------------------------ #

            # ----- Everything Off -----

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_34(self):
        """
        ############################
        Increment clock from 10:22 - 10:23 AM
        ############################

        Mainline 1:
            - Here the only status that will change is the 3200 will clear its flow fault and the error status on zn 2
              should be cleared, as well as the flow fault on its mainline

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:23 AM                                               #
            #                                                        #
            # ------------------------------------------------------ #

            helper_methods.increment_controller_flowstation_clocks(flowstation=self.config.FlowStations[1],
                                                                   controllers=self.config.BaseStation3200,
                                                                   minutes=1)

            # Verify the only water source
            self.config.BaseStation3200[1].get_water_source(1).statuses.verify_status_is_ok()
            self.config.FlowStations[1].get_water_source(1).statuses.verify_status_is_ok()

            # ------------------------------------------------------ #
            # ML1 - ADV HIGH FLOW VARIANCE TIER 1 W/ SHUTDOWN        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()

            # Flow Sources
            self.config.BaseStation3200[1].get_point_of_control(1).statuses.verify_status_is_off()
            self.config.BaseStation3200[1].get_mainline(1).statuses.verify_status_is_off()

            self.config.FlowStations[1].get_point_of_control(1).statuses.verify_status_is_off()
            self.config.FlowStations[1].get_mainline(1).statuses.verify_status_is_off()

            # Flow Devices
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_ok()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
