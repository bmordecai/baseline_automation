import sys
from common.configuration import Configuration
from datetime import time, timedelta, datetime, date
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr

# import log_handler for logging functionality
from common.logging_handler import log_handler

# Objects
from common.objects.programming.pg_3200 import PG3200
from common.objects.programming.zp import ZoneProgram
from common.objects.base_classes.web_driver import *
from common.objects.programming.ml import Mainline
from common.imports import opcodes

# Browser pages used
# import page_factory

__author__ = 'Tige'


class ControllerUseCase14(object):
    """
    Test name:
        - CN UseCase14 low flow variance

    User Story: \n
        1) As a user I would like my mainline to shutdown because a pipe cracked so that I don't waste precious water,
           flood my lovely neighbors and have a dead lawn with butterflies.

        2) As a user after I fixed my irrigation issue I would like to be able to resume watering.

    Coverage and Objectives: \n
        1. When there is a mainline with low flow variance enabled on the 3200:
            a.  Mainline with low variance should generate a message and shut down watering when the variance is
                hit and shutdown is enabled.
            b.  Mainline with low variance should generate a message and continue watering when the variance is hit
                and shutdown is disabled.
            c.  After clearing a shutdown message the fault statuses should be cleared off of the mainline.

    Scenarios: \n
        - check for mainline 3 strike rule
        - check for low flow variance on 1 mainline with shut down
        - check for low flow variance on 2 mainlines without shutdown
        - check for low flow variance on both mainline lines at the same time
        - check that clearing messages clears the error status of the zone

    How flow variance works
        f.	Flow Variance:  flow variance is the actual flow compared to the expected flow.
              Both an above and below expected flow conditions can be detected and then acted upon.
        i.	When the actual flow is above the expected flow, then the high flow variance is
               calculated from the following formula:
               percent = 100 * (actual flow - expected flow)/(expected flow)
        1.	If actual flow = 50 and expected flow = 40, then the high variance = 25%
        ii.	When the actual flow is below the expected flow, then the low flow variance is
              calculated from the following formula:
              percent = 100 * (expected flow - actual flow)/(expected flow)
        1.	If actual flow = 40 and expected flow = 50, then the low variance = 20%
        iii.	Basic variance has a single value that will be used to detect and signal a
              high and low flow variance condition.
        iv.	Advanced variance has four values for each of the rate ranges (see table above) for readings
              above and below the estimated flow.
        v.	If the expected flow is zero (zones don't have a design learn flow value)
              then this is always false
        vi.	If the limit is zero, then this is always false
        vii.	If it is disabled, then this is always false
        g.	Flow Variance Actions:
        i.	When a variance condition is detected and shutdown is not enabled:
        1.	Post a message on each detected variance condition but keep zones and mainline running. These will
              be zone variance detected messages only.
        ii.	When a variance condition is detected and shutdown is enabled:
        1.	Give each of the running zones on this mainline a strike and set them to waiting.
        2.	Give the mainline a strike.
        3.	Start a new group of zones  this may have one zone with one strike.  T
            here will never be more than one zone with a strike running at the same time.
        4.	Keep running zone combinations looking for groups that can run to completion, giving strikes to
              zones as additional variance conditions are detected.
        5.	When a zone receives two strikes, it must be run by itself for the third strike before it is
              cleared or marked as bad and shut down.  This will post a zone variance shutdown message.
        6.	The mainline is shutdown if it gets three strikes in a row. This will post a mainline variance
              shutdown message.
        a.	A mainline is shut down by setting all POCs that provide it water to a flow fault status with
              shutdown message.
        b.	All zones are set to done
        c.	The mainline will not be run until a user clears the flow variance message and messages on the POC(s).
        7.	A group of zones that runs without a variance condition will clear all strikes on these zones and
              this mainline.
        """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        # this is set in the PG3200 object

        program_start_time_10am = [600]  # 10:00am start time
        program_start_time_11am = [660]  # 11:00am start time
        program_start_time_2pm = [840]  # 2:00pm start time
        program_watering_days = [1, 1, 1, 1, 1, 1, 1]  # runs all days
        program_water_windows = ['111111111111111111111111']
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add and configure Program 1
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_water_window(_ww=program_water_windows)
            self.config.BaseStation3200[1].programs[1].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[1].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(_sun=True,
                                                                                                           _mon=True,
                                                                                                           _tues=True,
                                                                                                           _wed=True,
                                                                                                           _thurs=True,
                                                                                                           _fri=True,
                                                                                                           _sat=True)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_start_time_10am)

            # Add and configure Program 2
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation3200[1].programs[2].set_enabled()
            self.config.BaseStation3200[1].programs[2].set_water_window(_ww=program_water_windows)
            self.config.BaseStation3200[1].programs[2].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[2].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[2].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[2].set_watering_intervals_to_selected_days_of_the_week(_sun=True,
                                                                                                           _mon=True,
                                                                                                           _tues=True,
                                                                                                           _wed=True,
                                                                                                           _thurs=True,
                                                                                                           _fri=True,
                                                                                                           _sat=True)
            self.config.BaseStation3200[1].programs[2].set_start_times(_st_list=program_start_time_11am)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        - set up Zone Programs
            - add zones to program 1
                - zone 1 is a primary
                    - _rt = runtime 960 seconds
                    - _ct = cycle time = 480 seconds
                    - _so = soak time = 300 seconds
                    - zones 2-5 are linked ot zone 1
            - add zones to program 99
                - zones 6 - 10 are all set to timed
                    - _rt = runtime 360 seconds
                    - _ct = cycle time = 180 seconds
                    - _so = soak time = 120 seconds
                    - _pz = 0 means timed
        Program Zone 1
            - Program: 1
            - Zone: 1
            - Mode: Primary
            - Runtime: 16 min
            - Cycle Time: 8 min
            - Soak time: 5 min

        Program Zone 2:
            - Program: 1
            - Zone: 2
            - Mode: Linked-1
            - Runtime: 16 min
            - Cycle Time: 8 min
            - Soak time: 5 min

        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure Program Zone 1
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=16)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=8)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=5)

            # Add & Configure Program Zone 2
            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[2].zone_programs[2].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[2].zone_programs[2].set_run_time(_minutes=16)
            self.config.BaseStation3200[1].programs[2].zone_programs[2].set_cycle_time(_minutes=8)
            self.config.BaseStation3200[1].programs[2].zone_programs[2].set_soak_time(_minutes=5)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_3(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n

        set up WS 1 \n
            - enabled
            - target flow: 500 \n
            - priority: 2 \n
            - water budget: 100000
            - water budget shutdown: enabled \n
            - water rationing: enabled \n
            - empty conditions:
                - none

        set up WS 8 \n
            - enabled
            - target flow: 500 \n
            - priority: 2 \n
            - water budget: 100000
            - water budget shutdown: enabled \n
            - water rationing: enabled \n
            - empty conditions:
                - none
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Water Source 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[1].set_monthly_watering_budget(_budget=0)
            self.config.BaseStation3200[1].water_sources[1].set_water_rationing_to_disabled()

            # Water Source 8
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=8)
            self.config.BaseStation3200[1].water_sources[8].set_enabled()
            self.config.BaseStation3200[1].water_sources[8].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[8].set_monthly_watering_budget(_budget=0)
            self.config.BaseStation3200[1].water_sources[8].set_water_rationing_to_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n

        - set up POC 1 \n
            - enable POC 1 \n
            - assign master valve TMV0001 and flow meter TWF0001 to POC 1 \n
            - assign POC 1 a target flow of 500 \n
            - assign POC 1 to main line 1 \n
            - set POC priority to 2-medium \n
            - set high flow limit to 550 and enable high flow shut down \n
            - set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            - set water budget to 0 and disable the water budget shut down \n
            - disable water rationing \n
        - set up POC 8 \n
            - enable POC 8 \n
            - assign master valve TMV0002 and flow meter TWF0002 to POC 1 \n
            - assign POC 8 a target flow of 500 \n
            - assign POC 8 to main line 1 \n
            - set POC priority to 2-medium \n
            - set high flow limit to 550 and enable high flow shut down \n
            - set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            - set water budget to 0 and disable the water budget shut down \n
            - disable water rationing \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_flow_meter_to_point_of_control(
                _flow_meter_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].points_of_control[1].set_high_flow_limit(_limit=550,
                                                                                    with_shutdown_enabled=True)
            self.config.BaseStation3200[1].points_of_control[1].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=True)
            # Add POC 1 to Water Source 1
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=1)

            # Add & Configure POC 8
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=8)
            self.config.BaseStation3200[1].points_of_control[8].set_enabled()
            self.config.BaseStation3200[1].points_of_control[8].add_master_valve_to_point_of_control(
                _master_valve_address=2)
            self.config.BaseStation3200[1].points_of_control[8].add_flow_meter_to_point_of_control(
                _flow_meter_address=2)
            self.config.BaseStation3200[1].points_of_control[8].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].points_of_control[8].set_high_flow_limit(_limit=550,
                                                                                    with_shutdown_enabled=True)
            self.config.BaseStation3200[1].points_of_control[8].set_unscheduled_flow_limit(_gallons=40,
                                                                                           with_shutdown_enabled=True)
            # Add POC 8 to Water Source 8
            self.config.BaseStation3200[1].water_sources[8].add_point_of_control_to_water_source(
                _point_of_control_address=8)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control

        - set up main line 1 \n
            - set limit zones by flow to true \n
            - set the pipe fill time to 4 minutes \n
            - set the target flow to 500 \n
            - set the high variance limit to 5% and enable the high variance shut down \n
            - set the low variance limit to 20% and enable the low variance shut down \n
        - set up main line 8 \n
            - set limit zones by flow to true \n
            - set the pipe fill time to 1 minutes \n
            - set the target flow to 500 \n
            - set the high variance limit to 5% and enable the high variance shut down \n
            - set the low variance limit to 20% and enable the low variance shut down \n
        """

        # here we can either execute the following uncommented lines in procedural fashion, or we could re-init the
        # object, would have to import Mainline at the top, effectively accomplishing the same thing by:
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure ML 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=2)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=20)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[1].set_standard_variance_limit_with_shutdown(_percentage=20)
            # Add ML 1 to POC 1
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)

            # Add & Configure ML 8
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=8)
            self.config.BaseStation3200[1].mainlines[8].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[8].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].mainlines[8].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[8].set_standard_variance_limit_without_shutdown(_percentage=20)
            # Add ML 8 to POC 8
            self.config.BaseStation3200[1].points_of_control[8].add_mainline_to_point_of_control(_mainline_address=8)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone
        Program | Mainline  | Zones
        ---------------------------------------------
        1       | 1         | 1,2,3,4,5
        99      | 8         | 6,7,8,9,10
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Add ZN 1 to ML 1
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=1)

            # Add ZN 2 to ML 8
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=2)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):

        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Assign a design flow value to each zone so that they have a default setting
            self.config.BaseStation3200[1].zones[1].set_design_flow(_gallons_per_minute=20.0)

            self.config.BaseStation3200[1].zones[2].set_design_flow(_gallons_per_minute=50.0)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        trigger a standard low flow variance on just the mainline 1 with shutdown enabled
            - mainline 1 has a low flow variance of 20% with shutdown

        set the GPM on flow meter 1 to 15 GPM this is 25% below flow needed
        set the GPM on flow meter 2 to 0 GPM because this program in not running
        lf flow100 * (actual flow = 15 - expected flow = 20 )/(expected flow = 20)
         hf flow 100 * (expected flow = 15 - actual flow = 20 )/(expected flow = 20)

        program 1 has a 10:am start time

        zones 1 will get a low flow variance alarm and will shutdown so there status will be an error
        after a 2 minute pipe fill and 3 strikes the zone should error
        verify that the zones shut down in the appropriate amount of time
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=15.0)
            self.config.BaseStation3200[1].flow_meters[2].bicoder.set_flow_rate(_gallons_per_minute=0)

            date_mngr.set_current_date_to_match_computer()
            self.config.BaseStation3200[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                             _time='09:59:00')
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)

            # verify zones and mainlines turn on correctly
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[8].statuses.verify_status_is_off()

            not_done = True
            while not_done:

                zones_still_running = False

                # ------------------------------------------------------------------------------------------------------
                # 1. get current data from controller (for getting updated status)
                # 2. get current status
                # 3. set flag only if any zones are still running, we want to remain in the while loop checking status
                self.config.BaseStation3200[1].zones[1].get_data()
                _zone_status = self.config.BaseStation3200[1].zones[1].data.get_value_string_by_key(opcodes.status_code)

                # for zone get the ss
                #   update the attribute in the object
                #       (this is so we can store it for what happened during the last run time)
                # check the status of the zone and record each minute it is water so that we can determining number
                # of minute before shutdown
                if _zone_status == opcodes.watering:
                    self.config.BaseStation3200[1].zones[1].seconds_zone_ran += 60  # using 60 so that we are in seconds
                if _zone_status == opcodes.error:
                    self.config.BaseStation3200[1].programs[1].zone_programs[1].messages.\
                        verify_shutdown_on_low_flow_variance_message()
                # set flag not all zones are done
                if _zone_status != opcodes.done_watering and _zone_status != opcodes.error:
                    zones_still_running = True

                    # flag to true until all zone are done:
                # ----- END FOR ----------------------------------------------------------------------------------------
                if zones_still_running:
                    not_done = True
                    self.config.BaseStation3200[1].do_increment_clock(minutes=1)
                    self.config.BaseStation3200[1].verify_date_and_time()
                else:
                    not_done = False
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_error()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_flow_fault()

            self.config.BaseStation3200[1].programs[1].zone_programs[1].messages.\
                clear_shutdown_on_low_flow_variance_message()
            self.config.BaseStation3200[1].mainlines[1].messages.clear_low_flow_variance_shutdown_message()
            # because the message was cleared the status should be cleared
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_off()

            # check to see if each zone shut down in the correct amount of time which is pipe fill plus 1 minute
            seconds_before_error = (self.config.BaseStation3200[1].mainlines[1].ft * 60) + 60
            if seconds_before_error != self.config.BaseStation3200[1].zones[1].seconds_zone_ran:
                print ("Zone " + str(1) + " took " +
                       str(self.config.BaseStation3200[1].zones[1].seconds_zone_ran) +
                       " seconds to fail with a low flow variance " + str(seconds_before_error) + " seconds")
            else:
                print ("Zone " + str(1) + " took " +
                       str(self.config.BaseStation3200[1].zones[1].seconds_zone_ran) +
                       " seconds to fail with a low flow variance")
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        trigger a standard low flow variance on just the mainline 2 with shutdown disabled
            - mainline 8 has a low flow variance of 20% without shutdown

        set the GPM on flow meter 1 to 0 GPM this is because program 1 is not running
        set the GPM on flow meter 2 to 40 GPM%
         lf flow100 * (actual flow = 35 - expected flow = 50 )/(expected flow = 50)

        zones 6-10 will get a low flow variance alarm and will shutdown so there status will be an error

        program 99 has a 2:pm start time

        verify that the zones shut down in the appropriate amount of time
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=0)
            self.config.BaseStation3200[1].flow_meters[2].bicoder.set_flow_rate(_gallons_per_minute=35.0)

            date_mngr.set_current_date_to_match_computer()
            self.config.BaseStation3200[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                             _time='10:59:00')
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)

            # verify zones and mainlines turn on correctly
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[8].statuses.verify_status_is_running()

            not_done = True
            while not_done:

                zones_still_running = False

                # ------------------------------------------------------------------------------------------------------
                # 1. get current data from controller (for getting updated status)
                # 2. get current status
                # 3. set flag only if any zones are still running, we want to remain in the while loop checking status
                self.config.BaseStation3200[1].zones[2].get_data()
                _zone_status = self.config.BaseStation3200[1].zones[2].data.get_value_string_by_key(opcodes.status_code)

                # for zone get the ss
                #   update the attribute in the object
                #       (this is so we can store it for what happened during the last run time)
                # check the status of the zone and record each minute it is water so that we can determining number
                # of minute before shutdown
                if _zone_status == opcodes.watering:
                    self.config.BaseStation3200[1].zones[2].seconds_zone_ran += 60  # using 60 so that we are in seconds
                # set flag not all zones are done
                if _zone_status != opcodes.done_watering:
                    zones_still_running = True

                    # flag to true until all zone are done:
                # ----- END FOR ----------------------------------------------------------------------------------------
                if zones_still_running:
                    not_done = True
                    self.config.BaseStation3200[1].do_increment_clock(minutes=1)
                    self.config.BaseStation3200[1].verify_date_and_time()
                else:
                    not_done = False
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].mainlines[8].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[8].messages.clear_low_flow_variance_detected_message()

            # because the message was cleared the status should be cleared
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].mainlines[8].statuses.verify_status_is_off()

            # check to see if each zone shut down in the correct amount of time which is pipe fill plus 1 minute
            seconds_before_error = (self.config.BaseStation3200[1].mainlines[8].ft * 60) + 60
            if seconds_before_error != self.config.BaseStation3200[1].zones[2].seconds_zone_ran:
                print ("Zone " + str(1) + " took " +
                       str(self.config.BaseStation3200[1].zones[2].seconds_zone_ran) +
                       " seconds to fail with a low flow variance " + str(seconds_before_error) + " seconds")
            else:
                print ("Zone " + str(1) + " took " +
                       str(self.config.BaseStation3200[1].zones[2].seconds_zone_ran) +
                       " seconds to fail with a low flow variance")
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        trigger a standard low flow variance on both mainlines
            - mainline 1 has a low flow variance of 20% with shutdown
            - mainline 8 has a low flow variance of 20%  with no shut down

        set the GPM on flow meter 1 to 15 GPM
        set the GPM on flow meter 2 to 40 GPM%

        zones 1-5 will get a low flow variance alarm and will shut down so there status will be an error
        zones 6-10 will get a low flow variance alarm and will shut down so there status will be an error
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # set controller to be able to run two zones at a time
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=2)
            #this makes both programs have the same start time
            program_start_time_10am = [600]
            self.config.BaseStation3200[1].programs[2].set_start_times(_st_list=program_start_time_10am)
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=2)

            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=15.0)
            self.config.BaseStation3200[1].flow_meters[2].bicoder.set_flow_rate(_gallons_per_minute=35.0)

            date_mngr.set_current_date_to_match_computer()
            self.config.BaseStation3200[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                             _time='09:59:00')
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)

            # this verifies both mainlines turn on the at the same time
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[8].statuses.verify_status_is_running()

            not_done = True
            processed_error = False
            message_verified = False
            while not_done:

                zones_still_running = False

                # ------------------------------------------------------------------------------------------------------
                # for each zone:
                #   1. get current data from controller (for getting updated status)
                #   2. get current status
                #   3. set flag only if any zones are still running, we want to remain in the while loop checking status
                for zone in range(1, 3):

                    self.config.BaseStation3200[1].zones[zone].get_data()
                    _zone_status = self.config.BaseStation3200[1].zones[zone]. \
                        data.get_value_string_by_key(opcodes.status_code)

                    # for each zone get the ss per zone
                    #   update the attribute in the object
                    #       (this is so we can store it for what happened during the last run time)
                    # when zone 5 finishes and gets an error
                    #  set flow meter back to zero because zones are off
                    if _zone_status == opcodes.watering:
                        # using 60 so that we are in seconds
                        self.config.BaseStation3200[1].zones[zone].seconds_zone_ran += 60

                    # only go through this loop 1 time if the zone is in error
                    if zone == 1 and _zone_status == opcodes.error:
                        if not processed_error:
                            self.config.BaseStation3200[1].mainlines[1].messages. \
                                verify_low_flow_variance_shutdown_message()
                            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=0.0)
                            processed_error = True
                    if zone == 2 and _zone_status == opcodes.watering and not message_verified:
                        if self.config.BaseStation3200[1].mainlines[8].messages\
                                .check_for_low_flow_variance_detected_message_present():
                            self.config.BaseStation3200[1].mainlines[8].messages. \
                                verify_low_flow_variance_detected_message()
                            message_verified = True
                    if _zone_status != opcodes.done_watering and _zone_status != opcodes.error:
                        zones_still_running = True

                        # flag to true until all zone are done:
                # ----- END FOR ----------------------------------------------------------------------------------------

                # this could say if zones_still_running:
                if zones_still_running:
                    not_done = True
                    self.config.BaseStation3200[1].do_increment_clock(minutes=1)
                    self.config.BaseStation3200[1].verify_date_and_time()
                else:
                    not_done = False
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_error()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_flow_fault()
            self.config.BaseStation3200[1].mainlines[8].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].\
                messages.clear_shutdown_on_low_flow_variance_message()
            self.config.BaseStation3200[1].mainlines[1].messages.clear_low_flow_variance_shutdown_message()
            self.config.BaseStation3200[1].mainlines[8].messages.clear_low_flow_variance_detected_message()

            # because the message was cleared the status should be cleared
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[8].statuses.verify_status_is_off()

            # check to see if each zone shut down in the correct amount of time which is pipe fill plus 1 minute
            mainline_1_seconds_before_error = (self.config.BaseStation3200[1].mainlines[1].ft * 60) + 60
            mainline_8_seconds_before_error = (self.config.BaseStation3200[1].mainlines[8].ft * 60) + 60
            if self.config.BaseStation3200[1].zones[1]:
                if mainline_1_seconds_before_error != self.config.BaseStation3200[1].zones[1].seconds_zone_ran:
                    print ("Zone " + str(1) + " took " +
                           str(self.config.BaseStation3200[1].zones[1].seconds_zone_ran) +
                           " seconds to fail with a low flow variance " +
                           str(mainline_1_seconds_before_error) + " seconds")
                else:
                    print ("Zone " + str(1) + " took " +
                           str(self.config.BaseStation3200[1].zones[1].seconds_zone_ran) +
                           " seconds to fail with a low flow variance")
            if self.config.BaseStation3200[1].zones[2]:
                if mainline_8_seconds_before_error != self.config.BaseStation3200[1].zones[2].seconds_zone_ran:
                    print ("Zone " + str(2) + " took " +
                           str(self.config.BaseStation3200[1].zones[2].seconds_zone_ran) +
                           " seconds to fail with a low flow variance " +
                           str(mainline_8_seconds_before_error) + " seconds")
                else:
                    print ("Zone " + str(2) + " took " +
                           str(self.config.BaseStation3200[1].zones[2].seconds_zone_ran) +
                           " seconds to fail with a low flow variance")

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]