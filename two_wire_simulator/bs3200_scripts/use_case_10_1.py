import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods


__author__ = 'Tige'


class ControllerUseCase10_1(object):
    """
    Test name:
        - CN UseCase10 seasonal adjust
    purpose:
        - set up a full configuration on the controller
            - check tracking ration
            - Check seasonal adjust with tracking ration added
            - adjust the seasonal adjust :
                - verify primary zones run correctly
                - verify linked zones run correctly
        **********
        - Remove zones from mainlines and verify run times
        **********

    Coverage area: \n
        -setting up devices:
            - Loading \n
            - Searching \n
            - Addressing \n
                - Setting:
                    - descriptions
                    - locations \n

        - setting Programs: \n
            - start times
            - watering days
            - water windows
            - zone concurrency
            - assign Programs to water sources \n
            - assign zones to Programs \n
            - set up primary linked zones \n
            - give each zone a run time of 1 hour and 30 minutes \n
            - assign moisture sensors to a primary zone 1 and set to lower limit watering

        - setting up mainline: \n
            set main line \n
                - limit zones by flow \n
                - pipe fill time\n
                - target flow\n
                - high variance limit
                - high variance shut down \n
                - low variance limit
                - low variance shut down \n

        - setting up poc's: \n
            - enable POC \n
            - assign:
                - master valve
                - flow meter \n
                - target flow\n
                - main line \n
                - priority
                    - low
                    - medium
                    - high\n
            - set high flow limit
            - high flow shut down \n
            - set unscheduled flow limit
            - enable unscheduled flow shut down \n
            - set water budget
            - enable the water budget shut down \n
            - enable water rationing \n
            - empty conditions
                - event switch\n
                - set switch empty condition to closed \n
                - set empty wait time\n


    Date References:
        - configuration for script is located common\configuration_files\EPA_test_configuration.json
        - the devices and addresses range is read from the .json file

    """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                     # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            program_start_times = [480]
            program_water_windows = ['111111111111111111111111']

            # Add and configure Program 1
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_water_window(_ww=program_water_windows)
            self.config.BaseStation3200[1].programs[1].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=3)
            self.config.BaseStation3200[1].programs[1].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(_sun=True,
                                                                                                           _mon=True,
                                                                                                           _tues=True,
                                                                                                           _wed=True,
                                                                                                           _thurs=True,
                                                                                                           _fri=True,
                                                                                                           _sat=True)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_start_times)

            # Add and configure Program 2
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation3200[1].programs[2].set_enabled()
            self.config.BaseStation3200[1].programs[2].set_water_window(_ww=program_water_windows)
            self.config.BaseStation3200[1].programs[2].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[2].set_max_concurrent_zones(_number_of_zones=2)
            self.config.BaseStation3200[1].programs[2].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[2].set_watering_intervals_to_selected_days_of_the_week(_sun=True,
                                                                                                           _mon=True,
                                                                                                           _tues=True,
                                                                                                           _wed=True,
                                                                                                           _thurs=True,
                                                                                                           _fri=True,
                                                                                                           _sat=True)
            self.config.BaseStation3200[1].programs[2].set_start_times(_st_list=program_start_times)

            # Add and configure Program 99
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=99)
            self.config.BaseStation3200[1].programs[99].set_enabled()
            self.config.BaseStation3200[1].programs[99].set_water_window(_ww=program_water_windows)
            self.config.BaseStation3200[1].programs[99].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[99].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation3200[1].programs[99].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[99].set_watering_intervals_to_selected_days_of_the_week(_sun=True,
                                                                                                            _mon=True,
                                                                                                            _tues=True,
                                                                                                            _wed=True,
                                                                                                            _thurs=True,
                                                                                                            _fri=True,
                                                                                                            _sat=True)
            self.config.BaseStation3200[1].programs[99].set_start_times(_st_list=program_start_times)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        # Zone Programs
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=16)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=5)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_run_time(_minutes=16)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_soak_time(_minutes=5)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=50)
            self.config.BaseStation3200[1].programs[2].zone_programs[50].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[2].zone_programs[50].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[2].zone_programs[50].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[2].zone_programs[50].set_soak_time(_minutes=10)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=51)
            self.config.BaseStation3200[1].programs[2].zone_programs[51].set_as_linked_zone(_primary_zone=50)

            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=200)
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_soak_time(_minutes=10)

            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=197)
            self.config.BaseStation3200[1].programs[99].zone_programs[197].set_as_linked_zone(_primary_zone=200)

            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=198)
            self.config.BaseStation3200[1].programs[99].zone_programs[198].set_as_linked_zone(_primary_zone=200)

            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=199)
            self.config.BaseStation3200[1].programs[99].zone_programs[199].set_as_linked_zone(_primary_zone=200)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_3(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[1].set_monthly_watering_budget(_budget=100000,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].water_sources[1].set_water_rationing_to_enabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                    self.config.test_name,
                    sys._getframe().f_code.co_name,
                    date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                    str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_5(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # Add & Configure POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].add_master_valve_to_point_of_control(_master_valve_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_flow_meter_to_point_of_control(_flow_meter_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].points_of_control[1].set_high_flow_limit(_limit=550,
                                                                                    with_shutdown_enabled=True)
            self.config.BaseStation3200[1].points_of_control[1].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=True)
            # Add POC 1 to Water Source 1
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(_point_of_control_address=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                    self.config.test_name,
                    sys._getframe().f_code.co_name,
                    date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                    str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control
        """

        # here we can either execute the following uncommented lines in procedural fashion, or we could re-init the
        # object, would have to import Mainline at the top, effectively accomplishing the same thing by:
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # Add & Configure ML 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=4)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[1].set_high_flow_variance_tier_one(_percent=5,
                                                                                        _with_shutdown_enabled=False)
            self.config.BaseStation3200[1].mainlines[1].set_low_flow_variance_tier_one(_percent=20,
                                                                                       _with_shutdown_enabled=False)

            # Add ML 1 to POC 1
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=zone_address)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        set max concurrency on controller to 9 this will insure that all zones for all Programs start at one time
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=9)

            # Set tracking ratios for linked zones
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_tracking_ratio(_runtime_ratio=100)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_tracking_ratio(_runtime_ratio=50)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_tracking_ratio(_runtime_ratio=100)

            self.config.BaseStation3200[1].programs[2].zone_programs[50].set_tracking_ratio(_runtime_ratio=100)
            self.config.BaseStation3200[1].programs[2].zone_programs[51].set_tracking_ratio(_runtime_ratio=50)

            self.config.BaseStation3200[1].programs[99].zone_programs[197].set_tracking_ratio(_runtime_ratio=50)
            self.config.BaseStation3200[1].programs[99].zone_programs[198].set_tracking_ratio(_runtime_ratio=100)
            self.config.BaseStation3200[1].programs[99].zone_programs[199].set_tracking_ratio(_runtime_ratio=150)
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_tracking_ratio(_runtime_ratio=100)

            # Get data for zone programs
            self.config.BaseStation3200[1].programs[1].zone_programs[1].get_data()
            self.config.BaseStation3200[1].programs[1].zone_programs[2].get_data()
            self.config.BaseStation3200[1].programs[1].zone_programs[3].get_data()

            self.config.BaseStation3200[1].programs[2].zone_programs[50].get_data()
            self.config.BaseStation3200[1].programs[2].zone_programs[51].get_data()

            self.config.BaseStation3200[1].programs[99].zone_programs[197].get_data()
            self.config.BaseStation3200[1].programs[99].zone_programs[198].get_data()
            self.config.BaseStation3200[1].programs[99].zone_programs[199].get_data()
            self.config.BaseStation3200[1].programs[99].zone_programs[200].get_data()

            # FOR EACH ZP DO THE FOLLOWING:
            #   1) If we are a linked zone, verify tracking ratio
            #   2) else if we are a primary zone, verify watering strategy
            #   3) verify runtime
            #   4) verify cycle time
            
            # Program 1 Zone Programs 1-3
            for zone_address in range(1, 4):
                
                if self.config.BaseStation3200[1].programs[1].zone_programs[zone_address].is_linked():
                    self.config.BaseStation3200[1].programs[1].zone_programs[zone_address].verify_runtime_tracking_ratio()
                    
                elif self.config.BaseStation3200[1].programs[1].zone_programs[zone_address].is_primary():
                    self.config.BaseStation3200[1].programs[1].zone_programs[zone_address].verify_water_strategy()

                self.config.BaseStation3200[1].programs[1].zone_programs[zone_address].verify_runtime()
                self.config.BaseStation3200[1].programs[1].zone_programs[zone_address].verify_cycle_time()
                
            # Program 2 Zone Programs 50-51
            for zone_address in range(50, 52):

                if self.config.BaseStation3200[1].programs[2].zone_programs[zone_address].is_linked():
                    self.config.BaseStation3200[1].programs[2].zone_programs[zone_address].verify_runtime_tracking_ratio()

                elif self.config.BaseStation3200[1].programs[2].zone_programs[zone_address].is_primary():
                    self.config.BaseStation3200[1].programs[2].zone_programs[zone_address].verify_water_strategy()

                self.config.BaseStation3200[1].programs[2].zone_programs[zone_address].verify_runtime()
                self.config.BaseStation3200[1].programs[2].zone_programs[zone_address].verify_cycle_time()
            
            # Program 99 Zone Programs 197-200
            for zone_address in range(197, 201):

                if self.config.BaseStation3200[1].programs[99].zone_programs[zone_address].is_linked():
                    self.config.BaseStation3200[1].programs[99].zone_programs[zone_address].verify_runtime_tracking_ratio()

                elif self.config.BaseStation3200[1].programs[99].zone_programs[zone_address].is_primary():
                    self.config.BaseStation3200[1].programs[99].zone_programs[zone_address].verify_water_strategy()

                self.config.BaseStation3200[1].programs[99].zone_programs[zone_address].verify_runtime()
                self.config.BaseStation3200[1].programs[99].zone_programs[zone_address].verify_cycle_time()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
    
    #################################
    def step_10(self):
        """
        Verify all zones done watering.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_date_and_time(_date='11/22/2016', _time='7:58:00')

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone_address].verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
    
    #################################
    def step_11(self):
        """
            REMOVE ZONES FROM MAINLINE
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # need to clear out values from varibles for the new test
            # TODO need to add these to the program
            for program_address in sorted(self.config.BaseStation3200[1].programs.keys()):
                self.config.BaseStation3200[1].programs[program_address].seconds_program_ran = 0
                self.config.BaseStation3200[1].programs[program_address].seconds_program_waited = 0
                
            for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone_address].seconds_zone_ran = 0
                self.config.BaseStation3200[1].zones[zone_address].seconds_zone_soaked = 0
                self.config.BaseStation3200[1].zones[zone_address].seconds_zone_waited = 0
                self.config.BaseStation3200[1].zones[zone_address].seconds_for_each_cycle_duration = []
                self.config.BaseStation3200[1].zones[zone_address].seconds_for_each_soak_duration = []
                self.config.BaseStation3200[1].zones[zone_address].last_ss = ''
                self.config.BaseStation3200[1].mainlines[1].remove_zone_from_mainline(_zone_address=zone_address)
                # ----- END FOR ----------------------------------------------------------------------------------------
                
            # increment the clock 1 minute to 8:01 this will start all of the Programs
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)
            not_done = True

            while not_done:

                zones_still_running = False
                
                # ------------------------------------------------------------------------------------------------------
                # for each zone:
                #   1. get current data from controller (for getting updated status)
                #   2. get current status
                #   3. set flag only if any zones are still running, we want to remain in the while loop checking status
                for zone in sorted(self.config.BaseStation3200[1].zones.keys()):
                    self.config.BaseStation3200[1].zones[zone].get_data()
                    _zone_status = self.config.BaseStation3200[1].zones[zone].data.get_value_string_by_key(opcodes.status_code)
                    
                    # for each zone get the ss per zone
                    #   update the attribute in the object
                    #       (this is so we can store it for what happened during the last run time)
                    if _zone_status == opcodes.watering:
                        self.config.BaseStation3200[1].zones[zone].seconds_zone_ran += 60  # using 60 so that we are in seconds
                    
                    elif _zone_status == opcodes.soak_cycle:
                        self.config.BaseStation3200[1].zones[zone].seconds_zone_soaked += 60
                    
                    elif _zone_status == opcodes.waiting_to_water:
                        self.config.BaseStation3200[1].zones[zone].seconds_zone_waited += 60

                    # If the a zone changes from watering to soaking, put the runtime in the cycle-time list
                    if self.config.BaseStation3200[1].zones[zone].last_ss == opcodes.watering and (_zone_status == opcodes.soaking or _zone_status == opcodes.waiting_to_water or _zone_status == opcodes.done_watering):
                        cycle_duration = self.config.BaseStation3200[1].zones[zone].seconds_zone_ran
                        
                        for cycle in self.config.BaseStation3200[1].zones[zone].seconds_for_each_cycle_duration:
                            # subtract from the list the time that was just in the list so that we can get the next value in the list
                            cycle_duration = cycle_duration-cycle
                        
                        self.config.BaseStation3200[1].zones[zone].seconds_for_each_cycle_duration.append(cycle_duration)

                    # If the zone changes from soaking to watering, put the soak-time in the soak-time list
                    if self.config.BaseStation3200[1].zones[zone].last_ss == opcodes.soaking and _zone_status == opcodes.watering: #and in the past it went from waiting, done or watering
                        soak_duration = self.config.BaseStation3200[1].zones[zone].seconds_zone_soaked
                        
                        for soak in self.config.BaseStation3200[1].zones[zone].seconds_for_each_soak_duration:
                            soak_duration = soak_duration-soak
                        
                        self.config.BaseStation3200[1].zones[zone].seconds_for_each_soak_duration.append(soak_duration)

                    # update last zone status
                    self.config.BaseStation3200[1].zones[zone].last_ss = _zone_status
                    
                    # set flag not all zones are done
                    if _zone_status != opcodes.done_watering:
                        zones_still_running = True
                        
                # ----- END FOR ----------------------------------------------------------------------------------------

                # if any of the zones are not done than go back through loop :
                if zones_still_running:
                    not_done = True
                    self.config.BaseStation3200[1].do_increment_clock(minutes=1)
                    self.config.BaseStation3200[1].verify_date_and_time()
                else:
                    not_done = False
            
            # PRINT ZONE STATS
                    
            # For each program
            for program_address in sorted(self.config.BaseStation3200[1].programs.keys()):
                
                program = self.config.BaseStation3200[1].programs[program_address]
                
                # For each zone program in program
                for zone_address in sorted(program.zone_programs.keys()):

                    zone_program = program.zone_programs[zone_address]

                    print "Zone: {0} runtime: {1}".format(zone_address, zone_program.rt)
                    print "Zone: {0} total seconds ran: {1}".format(zone_address, zone_program.zone.seconds_zone_ran)
                    print "Zone: {0} cycle time: {1}".format(zone_address, zone_program.ct)
                    print "Zone: {0} duration of each cycle time: {1}".format(zone_address, zone_program.zone.seconds_for_each_cycle_duration)
                    print "Zone: {0} soak time: {1}".format(zone_address, zone_program.so)
                    print "Zone: {0} duration of each soak time: {1}".format(zone_address, zone_program.zone.seconds_for_each_soak_duration)
                    print "Zone: {0} total seconds zone soaked: {1}".format(zone_address, zone_program.zone.seconds_zone_soaked)
                    print "Zone: {0} wait time: {1}".format(zone_address, zone_program.zone.seconds_zone_waited)
                    print "Zone: {0} {1}".format(zone_address, zone_program.zone.last_ss)

        # ----- END WHILE ------------------------------------------------------------------------------------------
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        verify that all zones water correctly and that the zones that have tracking ratio adjust there runtime and
        cycle time
        tracking ratio adjusts run time and cycle time not soak time
        Program 1
            - zones
                1 primary zone
                    - runtime 960 seconds
                    - cycle time 300 seconds
                    - soak time  300 seconds
                2 has a 50% tracking ratio of primary zone 1
                    - original runtime = 960 after tracking ration runtime = 480
                    - original cycle 300 after tracking ration cycle time = 150 seconds this gets rounded up to 180 seconds
                    - soak time = 300 seconds
                3 is a timed zone
                    - runtime 960 seconds
                    - cycle time = 300 seconds
                    - soak time = 300 seconds
        Program 2
                50 is a timed zone
                    - runtime = 1200 seconds
                    - cycle time = 600 seconds
                    - soak time = 600 seconds
                51 has a 50% tracking ratio of primary zone 50
                    - original runtime = 1200 seconds after tracking ration runtime = 600 seconds
                    - original cycle time = 600 seconds after tracking ration cycle time = 300 seconds
                    - soak time = 600 seconds
        Program 99
                197 has a 50% tracking ratio of primary zone 200
                    - original runtime = 1200 seconds after tracking ration runtime = 600 seconds
                    - original cycle time = 600 seconds after tracking ration runtime = 300 seconds
                    - soak for  600 seconds
                198 has a 100% tracking ratio of primary zone 200
                    - original runtime = 1200 seconds after tracking ration runtime = 1200 seconds
                    - original cycle time = 600 seconds after tracking ration runtime = 600 seconds
                    - soak for  600 seconds
                199 has a 150% tracking ratio of primary zone 200
                    - original runtime = 1200 seconds after tracking ration runtime = 1800 seconds
                    - original cycle time = 600 seconds after tracking ration runtime = 900 seconds
                    - soak for  600 seconds
                200 primary zone
                    - original runtime = 1200 seconds
                    - cycle time = 600 seconds
                    - soak time =  600 seconds
        :return:
        :rtype:
        """
        # verifiers
            # - Total Run time
            # - Cycle time on controller against Each Cycle time duration
            # - Soak time on controller against Each Soak time duration
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # For each program
            for program_address in sorted(self.config.BaseStation3200[1].programs.keys()):

                program = self.config.BaseStation3200[1].programs[program_address]

                # For each zone program in program
                for zone_address in sorted(program.zone_programs.keys()):

                    zone_program = program.zone_programs[zone_address]
                    zone = zone_program.zone

                    zone_program.get_data()
                    zone.get_data()

            # for zone in self.config.BaseStation3200[1].zn_ad_range:
            #         self.config.BaseStation3200[1].zones[zone].get_data()
            #         self.config.BaseStation3200[1].zone_programs[zone].get_data()
                    
                    # runtime is the expected time calculated for the controller to run
                    # the controller doesnt always round so this rounds everything
                    runtime = 60*(int(round(float(zone_program.rt)/(float(60)))))
                    if zone.seconds_zone_ran == runtime:
                        e_msg = "Zone '{0}' ran for '{1}' seconds which match expected runtime of '{2}' seconds, " \
                                "it soaked for '{3}' seconds, and it waited for '{4}' seconds"\
                            .format(
                                str(zone_address),                                          # {0}
                                str(zone.seconds_zone_ran),      # {1}
                                str(runtime),                                       # {2}
                                str(zone.seconds_zone_soaked),   # {3}
                                str(zone.seconds_zone_waited)    # {4}}

                            )
                        print(e_msg)
                    else:
                        e_msg = "Zone '{0}' ran for '{1}' seconds and was expected run for '{2}' seconds, " \
                                "it soaked for '{3}' seconds, and it waited for '{4}' seconds" \
                            .format(
                                str(zone_address),                                          # {0}
                                str(zone.seconds_zone_ran),      # {1}
                                str(runtime),                                       # {2}
                                str(zone.seconds_zone_soaked),   # {3}
                                str(zone.seconds_zone_waited)    # {4}
                            )
                        raise ValueError(e_msg)


                    # this only looks at the first cycle time in the list an verifies it against the set cycle time 
                    # in the Contorller
                    cycle_time = zone_program.ct

                    first_cycle_time = zone.seconds_for_each_cycle_duration[0]
                    if first_cycle_time == cycle_time:
                        e_msg = "Zone '{0}' cycled for '{1}' seconds which match expected cycle time of '{2}' seconds " \
                           .format(
                            str(zone_address),                  # {0}
                            str(first_cycle_time),  # {1}
                            str(cycle_time),            # {2}
                            )
                        print(e_msg)
                    else:
                        e_msg = "A cycle time from:\n" \
                                "\tZone: {0}\n" \
                                "\tProgram: {1}\n" \
                                "did not match the list. Test Object had {2} while the controller returned {3}\n.".format(
                                    zone_program.zone.ad,       # {0}
                                    zone_program.program.ad,    # {1}
                                    str(first_cycle_time),                      # {2}
                                    str(cycle_time)                             # {3}
                                )
                        raise ValueError(e_msg)
                    
                    # this adds up all cycle times and compares against total run time to verify that total minutes 
                    # cycle is not greater than the total minutes in run time
                    total_of_all_cycle_times = 0
                    for each_cycle_time in zone.seconds_for_each_cycle_duration:
                        total_of_all_cycle_times += each_cycle_time
                        
                    if total_of_all_cycle_times == runtime:
                        e_msg = "Zone '{0}' cycled for a total of '{1}' seconds which match expected runtime of '{2}' seconds " \
                           .format(
                            str(zone_address),                       # {0}
                            str(total_of_all_cycle_times),  # {1}
                            str(runtime),                  # {2}
                            )
                        print(e_msg)
                    else:
                        e_msg = "A cycle time from:\n" \
                                "\tZone: {0}\n" \
                                "\tProgram: {1}\n" \
                                "did not match the list. Test Object had {2} while the controller returned {3}\n.".format(
                                    zone_program.zone.ad,       # {0}
                                    zone_program.program.ad,    # {1}
                                    str(total_of_all_cycle_times),              # {2}
                                    str(runtime)                                # {3}
                                )
                        raise ValueError(e_msg)

                    soak_time = zone_program.so

                    for soak_time_from_list in zone.seconds_for_each_soak_duration:
                        if soak_time_from_list >= soak_time:
                            e_msg = "Zone '{0}' soaked for '{1}' seconds which match expected soak time of '{2}' seconds " \
                               .format(
                                str(zone_address),                  # {0}
                                str(soak_time_from_list),   # {1}
                                str(soak_time),             # {2}
                                )
                            print(e_msg)
                        else:
                            e_msg = "A soak time from:\n" \
                                    "\tZone: {0}\n" \
                                    "\tProgram: {1}\n" \
                                    "did not match the list. Test Object had {2} while the controller returned {3}\n.".format(
                                        zone_program.zone.ad,           # {0}
                                        zone_program.program.ad,        # {1}
                                        str(soak_time_from_list),                       # {2}
                                        str(soak_time)                                  # {3}
                                    )
                            raise ValueError(e_msg)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        
    #################################
    def step_13(self):
        """
        Make a second adjustment keeping all of the tracking ratio the same as the previous steps now add a
        Seasonal adjustment
        set date time on controller to 11/22/2016 at 7:58am
        increment clock one minute to 7:59am which is one minute before start time
        verify that all zones are in a done watering state

        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # TODO need a method to set and verify a seasonal adjust pass in a int percent you decide
            self.config.BaseStation3200[1].programs[1].set_seasonal_adjust(50)
            self.config.BaseStation3200[1].programs[2].set_seasonal_adjust(100)
            self.config.BaseStation3200[1].programs[99].set_seasonal_adjust(150)

            # For each program
            for program_address in sorted(self.config.BaseStation3200[1].programs.keys()):

                program = self.config.BaseStation3200[1].programs[program_address]

                # For each zone program in program
                for zone_address in sorted(program.zone_programs.keys()):

                    zone_program = program.zone_programs[zone_address]
                    print zone_program.rt
                    
            # for zone in self.config.BaseStation3200[1].zn_ad_range:
            #     print self.config.BaseStation3200[1].zone_programs[zone].rt
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
    
    #################################
    def step_14(self):
        """
        set max concurrency on controller to 9 this will insure that all zones start at one time
        set date time on controller to 11/23/2016 at 7:58am
        increment clock one minute to 7:59 am
        verify that all zones are in a done watering state
        increment clock 2 minutes to 8:01am
        verify that all zones get set to watering
        zone run will run for 900 seconds
        cycle for 300
        soak for 300

        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=9)
            self.config.BaseStation3200[1].set_date_and_time(_date='11/23/2016', _time='7:58:00')

            # Increment the clock by 1 minute to 7:59, verify that nothing has started yet
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            
            # for zone in self.config.BaseStation3200[1].zn_ad_range:
            #     self.config.BaseStation3200[1].zones[zone].get_data()
            #     
            # self.config.BaseStation3200[1].programs[1].get_data()
            # 
            # for zones in self.config.BaseStation3200[1].zn_ad_range:
            #     self.config.BaseStation3200[1].zones[zones].verify_status(status=opcodes.done_watering)
            
            # For each program
            for program_address in sorted(self.config.BaseStation3200[1].programs.keys()):

                program = self.config.BaseStation3200[1].programs[program_address]
                program.get_data()

                # For each zone program in program
                for zone_address in sorted(program.zone_programs.keys()):

                    program.zone_programs[zone_address].zone.get_data()
                    program.zone_programs[zone_address].zone.verify_status_is_done()
                    
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
    
    #################################
    def step_15(self):
        """

        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # need to clear out values from varibles for the new test
            # TODO need to add these to the program
            for program_address in sorted(self.config.BaseStation3200[1].programs.keys()):
                self.config.BaseStation3200[1].programs[program_address].seconds_program_ran = 0
                self.config.BaseStation3200[1].programs[program_address].seconds_program_waited = 0

            for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone_address].seconds_zone_ran = 0
                self.config.BaseStation3200[1].zones[zone_address].seconds_zone_soaked = 0
                self.config.BaseStation3200[1].zones[zone_address].seconds_zone_waited = 0
                self.config.BaseStation3200[1].zones[zone_address].seconds_for_each_cycle_duration = []
                self.config.BaseStation3200[1].zones[zone_address].seconds_for_each_soak_duration = []
                self.config.BaseStation3200[1].zones[zone_address].last_ss = ''
                # ----- END FOR ----------------------------------------------------------------------------------------
                
            # increment the clock 1 minute to 8:01 this will start all of the Programs
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)
            not_done = True

            while not_done:

                zones_still_running = False
                # ------------------------------------------------------------------------------------------------------
                # for each zone:
                #   1. get current data from controller (for getting updated status)
                #   2. get current status
                #   3. set flag only if any zones are still running, we want to remain in the while loop checking status
                for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                    zone = self.config.BaseStation3200[1].zones[zone_address]
                    
                    zone.get_data()
                    _zone_status = zone.data.get_value_string_by_key(opcodes.status_code)
                    
                    # for each zone_address get the ss per zone_address
                    #   update the attribute in the object
                    #       (this is so we can store it for what happened during the last run time)
                    if _zone_status == opcodes.watering:
                        zone.seconds_zone_ran += 60  # using 60 so that we are in seconds
                        
                    elif _zone_status == opcodes.soak_cycle:
                        zone.seconds_zone_soaked += 60
                        
                    elif _zone_status == opcodes.waiting_to_water:
                        zone.seconds_zone_waited += 60

                    # If the a zone_address changes from watering to soaking, put the runtime in the cycle-time list
                    if zone.last_ss == opcodes.watering and (
                                _zone_status == opcodes.soaking or _zone_status == opcodes.waiting_to_water or _zone_status == opcodes.done_watering):
                        
                        cycle_duration = zone.seconds_zone_ran
                        
                        for cycle in zone.seconds_for_each_cycle_duration:
                            # subtract from the list the time that was just in the list so that we can get the next value in the list
                            cycle_duration = cycle_duration - cycle
                            
                        zone.seconds_for_each_cycle_duration.append(cycle_duration)

                    # If the zone_address changes from soaking to watering, put the soak-time in the soak-time list
                    if zone.last_ss == opcodes.soaking and _zone_status == opcodes.watering:  # and in the past it went from waiting, done or watering
                        
                        soak_duration = zone.seconds_zone_soaked
                        
                        for soak in zone.seconds_for_each_soak_duration:
                            soak_duration = soak_duration - soak
                            
                        zone.seconds_for_each_soak_duration.append(soak_duration)

                    # update last zone_address status
                    zone.last_ss = _zone_status
                    
                    # set flag not all zones are done
                    if _zone_status != opcodes.done_watering:
                        zones_still_running = True
                        
                    # flag to true until all zone_address are done:
                    # ----- END FOR ----------------------------------------------------------------------------------------

                # if any of the zones are not done than go back through loop :
                if zones_still_running:
                    not_done = True
                    self.config.BaseStation3200[1].do_increment_clock(minutes=1)
                    self.config.BaseStation3200[1].verify_date_and_time()
                else:
                    not_done = False
            
            # PRINT ZONE STATS

            # For each program
            for program_address in sorted(self.config.BaseStation3200[1].programs.keys()):

                program = self.config.BaseStation3200[1].programs[program_address]

                # For each zone program in program
                for zone_address in sorted(program.zone_programs.keys()):

                    zone_program = program.zone_programs[zone_address]

                    print "Zone: {0} runtime: {1}".format(zone_address, zone_program.rt)
                    print "Zone: {0} total seconds ran: {1}".format(zone_address, zone_program.zone.seconds_zone_ran)
                    print "Zone: {0} cycle time: {1}".format(zone_address, zone_program.ct)
                    print "Zone: {0} duration of each cycle time: {1}".format(zone_address, zone_program.zone.seconds_for_each_cycle_duration)
                    print "Zone: {0} soak time: {1}".format(zone_address, zone_program.so)
                    print "Zone: {0} duration of each soak time: {1}".format(zone_address, zone_program.zone.seconds_for_each_soak_duration)
                    print "Zone: {0} total seconds zone soaked: {1}".format(zone_address, zone_program.zone.seconds_zone_soaked)
                    print "Zone: {0} wait time: {1}".format(zone_address, zone_program.zone.seconds_zone_waited)
                    print "Zone: {0} {1}".format(zone_address, zone_program.zone.last_ss)

            # ----- END WHILE ------------------------------------------------------------------------------------------
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
    
    #################################
    def step_16(self):
        """
        increment clock 2 minutes to 8:01am this will allow all Programs to start watering
        verify that all zones water correctly and that the zones that have tracking ration adjust there runtime and
        cycle time the runtime will also be adjusted for the seasonal adjust but the cycle and soak times will not be
        adjusted past the tracking ration
        New program adjustments
        - set program 1 to seasonally adjust 50%
        - Set program 2 to seasonally adjust 100%
        - set program 99 to seasonally adjust 150%
        Program 1 has a seasonal adjustment of 50%
            - zones
                1 primary zone
                    - runtime 960 seconds after seasonal adjustment the runtime = 480
                    - cycle time 300 seconds
                    - soak time  300 seconds
                2 has a 50% tracking ratio of primary zone 1
                    - original runtime = 900 after tracking ration runtime = 480 seconds after seasonal adjustment the runtime = 240 seconds
                    - original cycle 300 after tracking ration cycle time = 150 seconds
                    - soak time = 300 seconds
                3 is a timed zone
                    - runtime 960 seconds after seasonal adjustment the runtime = 480 seconds
                    - cycle time = 300 seconds
                    - soak time = 300 seconds
        Program 2 has a seasonally adjust 100%
                50 is a timed zone
                    - runtime = 1200 seconds after seasonal adjustment the runtime = 1200 seconds
                    - cycle time = 600 seconds
                    - soak time = 600 seconds
                51 has a 50% tracking ratio of primary zone 50
                    - original runtime = 1200 seconds after tracking ration runtime = 600 seconds after seasonal adjustment the runtime = 600 seconds
                    - original cycle time = 600 seconds after tracking ration cycle time = 300 seconds
                    - soak time = 600 seconds
        Program 99 has a seasonally adjust 150%
                197 has a 50% tracking ratio of primary zone 200
                    - original runtime = 1200 seconds after tracking ration runtime = 600 seconds after seasonal adjustment the runtime = 1200 seconds
                    - original cycle time = 600 seconds after tracking ration runtime = 300 seconds
                    - soak for  600 seconds
                198 has a 100% tracking ratio of primary zone 200
                    - original runtime = 1200 seconds after tracking ration runtime = 1200 seconds after seasonal adjustment the runtime = 1800 seconds
                    - original cycle time = 600 seconds after tracking ration runtime = 600 seconds
                    - soak for  600 seconds
                199 has a 150% tracking ratio of primary zone 200
                    - original runtime = 1200 seconds after tracking ration runtime = 1800 seconds after seasonal adjustment the runtime = 2700 seconds
                    - original cycle time = 600 seconds after tracking ration runtime = 900 seconds
                    - soak for  600 seconds
                200 primary zone
                    - original runtime = 1200 seconds after seasonal adjustment the runtime = 1800 seconds
                    - cycle time = 600 seconds
                    - soak time =  600 seconds

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            self.config.BaseStation3200[1].do_increment_clock(minutes=2)
            self.config.BaseStation3200[1].verify_date_and_time()
            
            # For each program
            for program_address in sorted(self.config.BaseStation3200[1].programs.keys()):

                program = self.config.BaseStation3200[1].programs[program_address]

                # For each zone program in program
                for zone_address in sorted(program.zone_programs.keys()):

                    zone_program = program.zone_programs[zone_address]
                    zone = zone_program.zone

                    zone_program.get_data()
                    zone.get_data()

                    # runtime is the expected time calculated for the controller to run
                    # the controller doesnt always round so this rounds everything
                    runtime = 60*(int(round(float(zone_program.rt)/(float(60)))))
                    if zone.seconds_zone_ran == runtime:
                        e_msg = "Zone '{0}' ran for '{1}' seconds which match expected runtime of '{2}' seconds, " \
                                "it soaked for '{3}' seconds, and it waited for '{4}' seconds"\
                            .format(
                                str(zone_address),                                          # {0}
                                str(zone.seconds_zone_ran),      # {1}
                                str(runtime),                                       # {2}
                                str(zone.seconds_zone_soaked),   # {3}
                                str(zone.seconds_zone_waited)    # {4}}

                            )
                        print(e_msg)
                    else:
                        e_msg = "Zone '{0}' ran for '{1}' seconds and was expected run for '{2}' seconds, " \
                                "it soaked for '{3}' seconds, and it waited for '{4}' seconds" \
                            .format(
                                str(zone_address),                                          # {0}
                                str(zone.seconds_zone_ran),      # {1}
                                str(runtime),                                       # {2}
                                str(zone.seconds_zone_soaked),   # {3}
                                str(zone.seconds_zone_waited)    # {4}
                            )
                        raise ValueError(e_msg)


                    # this only looks at the first cycle time in the list an verifies it against the set cycle time in the Contorller
                    cycle_time = zone_program.ct

                    first_cycle_time = zone.seconds_for_each_cycle_duration[0]
                    if first_cycle_time == cycle_time:
                        e_msg = "Zone '{0}' cycled for '{1}' seconds which match expected cycle time of '{2}' seconds " \
                           .format(
                            str(zone_address),                  # {0}
                            str(first_cycle_time),      # {1}
                            str(cycle_time),            # {2}
                            )
                        print(e_msg)
                    else:
                        e_msg = "A cycle time from:\n" \
                                "\tZone: {0}\n" \
                                "\tProgram: {1}\n" \
                                "did not match the list. Test Object had {2} while the controller returned {3}\n.".format(
                                    zone_program.zone.ad,       # {0}
                                    zone_program.program.ad,    # {1}
                                    str(first_cycle_time),                      # {2}
                                    str(cycle_time)                             # {3}
                                )
                        raise ValueError(e_msg)

                    # this adds up all cycle times and compares against total run time to verify that total minutes
                    # cycle is not greater than the total minutes in run time
                    total_of_all_cycle_times = 0
                    for each_cycle_time in zone.seconds_for_each_cycle_duration:
                        total_of_all_cycle_times += each_cycle_time

                    if total_of_all_cycle_times == runtime:
                        e_msg = "Zone '{0}' cycled for a total of '{1}' seconds which match expected runtime of '{2}' seconds " \
                           .format(
                            str(zone_address),                       # {0}
                            str(total_of_all_cycle_times),   # {1}
                            str(runtime),                    # {2}
                            )
                        print(e_msg)
                    else:
                        e_msg = "A cycle time from:\n" \
                                "\tZone: {0}\n" \
                                "\tProgram: {1}\n" \
                                "did not match the list. Test Object had {2} while the controller returned {3}\n.".format(
                                    zone_program.zone.ad,       # {0}
                                    zone_program.program.ad,    # {1}
                                    str(total_of_all_cycle_times),              # {2}
                                    str(runtime)                                # {3}
                                )
                        raise ValueError(e_msg)

                    soak_time = zone_program.so

                    for soak_time_from_list in zone.seconds_for_each_soak_duration:
                        if soak_time_from_list >= soak_time:
                            e_msg = "Zone '{0}' soaked for '{1}' seconds which match expected soak time of '{2}' seconds " \
                               .format(
                                str(zone_address),                  # {0}
                                str(soak_time_from_list),   # {1}
                                str(soak_time),             # {2}
                                )
                            print(e_msg)
                        else:
                            e_msg = "A soak time from:\n" \
                                    "\tZone: {0}\n" \
                                    "\tProgram: {1}\n" \
                                    "did not match the list. Test Object had {2} while the controller returned {3}\n.".format(
                                        zone_program.zone.ad,           # {0}
                                        zone_program.program.ad,        # {1}
                                        str(soak_time_from_list),                       # {2}
                                        str(soak_time)                                  # {3}
                                    )
                            raise ValueError(e_msg)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]