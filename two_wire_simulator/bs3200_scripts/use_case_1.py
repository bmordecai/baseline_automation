import sys
from time import sleep
from datetime import datetime, timedelta
from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.logging_handler import log_handler
from common import helper_methods

__author__ = 'Tige'


class ControllerUseCase1(object):
    """
    Test name:
        - CN UseCase1 Replace Device
    purpose:
        - set up a full configuration on the controller
            - verify that no programming is lost when you:
                - replace a device
                - reboot the controller
            - verify that after a reboot the controller restarts
                - no programming has been lost
            - verify that after replacing devices :
                - no programming has been lost

    Coverage area: \n
        -setting up devices:
        - setting Programs: \n
        - setting up mainline: \n
        - setting up poc's: \n
        - Reboot the controller:
            - verify all setting \n
        - Replace devices: \n
            - zone decoder
                - verify all settings were not lost \n
            - moisture sensor
                - verify the primary zone setting were not lost \n
            - flow decoder
                - verify all setting were deleted\n
            - event decoder
                - verify all setting were deleted \n
            - temperature decoder
                - verify all setting were delete\n
            - pressure sensor
                - verify all setting were delete\n
            - pump
                - verify all setting were delete\n
            - Master Valves
                - verify all setting were delete\n
    Date References:
        - configuration for script is located common\configuration_files\EPA_test_configuration.json
        - the devices and addresses range is read from the .json file

    """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 1
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]

                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # this is set in the PG3200 object
            # TODO need to have concurrent zones per program added

            program_number_1_start_times = [480, 540, 600, 660]
            program_number_3_start_times = [480, 540, 600, 660]
            program_number_4_start_times = [480, 540, 600, 660]
            program_number_99_start_times = [480, 540, 600, 660]
            program_number_99_watering_days = [0, 0, 0, 0, 0, 0, 8, 8, 6, 5, 5, 4, 3, 3, 3, 3, 4, 5, 6, 7, 0, 0, 0, 0]
            program_number_1_water_windows = ['011111100001111111111110']
            program_number_3_water_windows = ['011111100001111111111110']
            program_number_4_water_windows = ['011111100001111111111110']
            program_number_99_water_windows = ['011111100000111111111110',
                                               '011111100001111111111111',
                                               '011111100001111111111110',
                                               '011111100001111111111110',
                                               '011111100001111111111110',
                                               '011111100001111111111110',
                                               '011111100001111111111111']
            # setup Program 1 on controller
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_water_window(_ww=program_number_1_water_windows)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_number_1_start_times)
            self.config.BaseStation3200[1].programs[1].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[1].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(_sun=False,
                                                                                                           _mon=True,
                                                                                                           _tues=False,
                                                                                                           _wed=True,
                                                                                                           _thurs=False,
                                                                                                           _fri=True,
                                                                                                           _sat=False)

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=3)
            self.config.BaseStation3200[1].programs[3].set_enabled()
            self.config.BaseStation3200[1].programs[3].set_water_window(_ww=program_number_3_water_windows)
            self.config.BaseStation3200[1].programs[3].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[3].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[3].set_watering_intervals_to_odd_days()
            self.config.BaseStation3200[1].programs[3].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[3].set_start_times(_st_list=program_number_3_start_times)

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=4)
            self.config.BaseStation3200[1].programs[4].set_enabled()
            self.config.BaseStation3200[1].programs[4].set_water_window(_ww=program_number_4_water_windows)
            self.config.BaseStation3200[1].programs[4].set_priority_level(_pr_level=3)
            self.config.BaseStation3200[1].programs[4].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation3200[1].programs[4].set_watering_intervals_to_odd_days()
            self.config.BaseStation3200[1].programs[4].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[4].set_start_times(_st_list=program_number_4_start_times)
            self.config.BaseStation3200[1].programs[4].set_watering_intervals_to_selected_days_of_the_week(_sun=False,
                                                                                                           _mon=True,
                                                                                                           _tues=False,
                                                                                                           _wed=True,
                                                                                                           _thurs=False,
                                                                                                           _fri=True,
                                                                                                           _sat=False)

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=99)
            self.config.BaseStation3200[1].programs[99].set_enabled()
            self.config.BaseStation3200[1].programs[99].set_water_window(_ww=program_number_99_water_windows, _is_weekly=False)
            self.config.BaseStation3200[1].programs[99].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[99].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[99].set_watering_intervals_to_odd_days()
            self.config.BaseStation3200[1].programs[99].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[99].set_start_times(_st_list=program_number_99_start_times)
            self.config.BaseStation3200[1].programs[99].set_watering_intervals_to_semi_monthly(_sm=program_number_99_watering_days)

            # make master valve two a booster so it can be assigned to a program
            self.config.BaseStation3200[1].master_valves[2].set_booster_enabled()
            # assign the booster to the program
            self.config.BaseStation3200[1].programs[99].add_booster_pump(_mv_address=2)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        #TODO need to add moisture sensors to the primary zones
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # Zone Programs
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=5)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1,
                                                                                           _tracking_ratio=100)

            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=49)
            self.config.BaseStation3200[1].programs[3].zone_programs[49].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[3].zone_programs[49].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[3].zone_programs[49].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[3].zone_programs[49].set_soak_time(_minutes=60)

            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=50)
            self.config.BaseStation3200[1].programs[3].zone_programs[50].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[3].zone_programs[50].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[3].zone_programs[50].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[3].zone_programs[50].set_soak_time(_minutes=60)

            self.config.BaseStation3200[1].programs[4].add_zone_to_program(_zone_address=50)
            self.config.BaseStation3200[1].programs[4].zone_programs[50].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[4].zone_programs[50].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[4].zone_programs[50].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[4].zone_programs[50].set_soak_time(_minutes=60)

            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=200)
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_run_time(_minutes=33)
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_cycle_time(_minutes=3)
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_soak_time(_minutes=13)

            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=197)
            self.config.BaseStation3200[1].programs[99].zone_programs[197].set_as_linked_zone(_primary_zone=200,
                                                                                              _tracking_ratio=50)

            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=198)
            self.config.BaseStation3200[1].programs[99].zone_programs[198].set_as_linked_zone(_primary_zone=200,
                                                                                              _tracking_ratio=100)

            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=199)
            self.config.BaseStation3200[1].programs[99].zone_programs[199].set_as_linked_zone(_primary_zone=200,
                                                                                              _tracking_ratio=150)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_3(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[1].set_monthly_watering_budget(_budget=10000,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].water_sources[1].set_water_rationing_to_enabled()

            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=2)
            self.config.BaseStation3200[1].water_sources[2].set_enabled()
            self.config.BaseStation3200[1].water_sources[2].set_priority(_priority_for_water_source=3)
            self.config.BaseStation3200[1].water_sources[2].set_monthly_watering_budget(_budget=1000,
                                                                                        _with_shutdown_enabled=False)
            self.config.BaseStation3200[1].water_sources[2].set_water_rationing_to_disabled()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_4(self):
        """
        ############################
        setup WaterSources Empty Conditions
        ############################
        Add empty condition with device ---> to water source \n
        - set up empty condition  Attributes \n
            - set enabled state \n
            - set device state \n
            - set wait time  \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # TODO need to add a moisture sensor to an empty conditon
            # add empty condition to the water source
            self.config.BaseStation3200[1].water_sources[1].add_switch_empty_condition(_event_switch_address=1)
            self.config.BaseStation3200[1].water_sources[1].switch_empty_conditions[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].switch_empty_conditions[1] \
                .set_switch_empty_condition_to_closed()
            self.config.BaseStation3200[1].water_sources[1].switch_empty_conditions[1].set_empty_wait_time(
                _minutes=2)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_5(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            # add point of control to water source
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                                                                                _point_of_control_address=1)

            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].points_of_control[1].set_high_flow_limit(_limit=550,
                                                                                    with_shutdown_enabled=True)
            self.config.BaseStation3200[1].points_of_control[1].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=True)
            self.config.BaseStation3200[1].points_of_control[1].set_high_pressure_limit(_limit=240,
                                                                                        with_shutdown_enabled=True)
            self.config.BaseStation3200[1].points_of_control[1].set_low_pressure_limit(_limit=100,
                                                                                       with_shutdown_enabled=True)

            self.config.BaseStation3200[1].points_of_control[1].add_flow_meter_to_point_of_control(
                                                                                        _flow_meter_address=3)
            self.config.BaseStation3200[1].points_of_control[1].add_pump_to_point_of_control(_pump_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_master_valve_to_point_of_control(
                                                                                        _master_valve_address=2)
            self.config.BaseStation3200[1].points_of_control[1].add_pressure_sensor_to_point_of_control(
                                                                                        _pressure_sensor_address=1)


            #set up point of control
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=8)
            self.config.BaseStation3200[1].water_sources[2].add_point_of_control_to_water_source(
                                                                            _point_of_control_address=8)# create object
            self.config.BaseStation3200[1].points_of_control[8].set_enabled()
            self.config.BaseStation3200[1].points_of_control[8].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].points_of_control[8].set_high_flow_limit(_limit=75,
                                                                                    with_shutdown_enabled=False)
            self.config.BaseStation3200[1].points_of_control[8].set_unscheduled_flow_limit(_gallons=5,
                                                                                           with_shutdown_enabled=False)
            self.config.BaseStation3200[1].points_of_control[8].set_high_pressure_limit(_limit=240,
                                                                                        with_shutdown_enabled=False)
            self.config.BaseStation3200[1].points_of_control[8].set_low_pressure_limit(_limit=100,
                                                                                       with_shutdown_enabled=False)

            self.config.BaseStation3200[1].points_of_control[8].add_flow_meter_to_point_of_control(
                                                                                        _flow_meter_address=4)
            self.config.BaseStation3200[1].points_of_control[8].add_pump_to_point_of_control(_pump_address=2)
            self.config.BaseStation3200[1].points_of_control[8].add_master_valve_to_point_of_control(
                                                                                        _master_valve_address=8)

            self.config.BaseStation3200[1].points_of_control[8].add_pressure_sensor_to_point_of_control(
                                                                                        _pressure_sensor_address=8)

            self.config.BaseStation3200[1].flow_meters[3].set_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                         self.config.test_name,
                         sys._getframe().f_code.co_name,
                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                         str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_6(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control
        """

        # here we can either execute the following uncommented lines in procedural fashion, or we could re-init the
        # object, would have to import Mainline at the top, effectively accomplishing the same thing by:
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_enabled()
            self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=4)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[1].set_standard_variance_limit_with_shutdown(_percentage=5)
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)

            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=8)
            self.config.BaseStation3200[1].mainlines[8].set_disabled()
            self.config.BaseStation3200[1].mainlines[8].set_pipe_stabilization_pressure(_pressure=25)
            self.config.BaseStation3200[1].mainlines[8].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].mainlines[8].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[8].set_standard_variance_limit_without_shutdown(_percentage=20)

            # add the mainline to teh point of control
            self.config.BaseStation3200[1].points_of_control[8].add_mainline_to_point_of_control(_mainline_address=8)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                         self.config.test_name,
                         sys._getframe().f_code.co_name,
                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                         str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_7(self):
        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Mainline 1
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=1)
            self.config.BaseStation3200[1].zones[1].set_design_flow(_gallons_per_minute=25)

            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=2)
            self.config.BaseStation3200[1].zones[2].set_design_flow(_gallons_per_minute=25)

            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=49)
            self.config.BaseStation3200[1].zones[49].set_design_flow(_gallons_per_minute=25)

            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=50)
            self.config.BaseStation3200[1].zones[50].set_design_flow(_gallons_per_minute=25)

            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=197)
            self.config.BaseStation3200[1].zones[197].set_design_flow(_gallons_per_minute=25)

            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=198)
            self.config.BaseStation3200[1].zones[198].set_design_flow(_gallons_per_minute=25)
            self.config.BaseStation3200[1].zones[198].set_disabled()

            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=199)
            self.config.BaseStation3200[1].zones[199].set_design_flow(_gallons_per_minute=25)

            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=200)
            self.config.BaseStation3200[1].zones[200].set_design_flow(_gallons_per_minute=25)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            pass
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            # time.sleep(20)
            self.config.BaseStation3200[1].do_reboot_controller()
            self.config.BaseStation3200[1].stop_clock()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        after the reboot all of the devices get set to default values in the controller so you have to do a self test
        and than do an update to get each object to match what the controller has
        perform a test on all zones to verify that they are functioning properly \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for zone in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone].bicoder.\
                    self_test_and_update_object_attributes()
            for moisture_sensors in sorted(self.config.BaseStation3200[1].moisture_sensors.keys()):
                self.config.BaseStation3200[1].moisture_sensors[moisture_sensors].bicoder.\
                    self_test_and_update_object_attributes()
            for master_valves in sorted(self.config.BaseStation3200[1].master_valves.keys()):
                self.config.BaseStation3200[1].master_valves[master_valves].bicoder.\
                    self_test_and_update_object_attributes()
            for flow_meters in sorted(self.config.BaseStation3200[1].flow_meters.keys()):
                self.config.BaseStation3200[1].flow_meters[flow_meters].bicoder.\
                    self_test_and_update_object_attributes()
            for event_switches in sorted(self.config.BaseStation3200[1].event_switches.keys()):
                self.config.BaseStation3200[1].event_switches[event_switches].bicoder.\
                    self_test_and_update_object_attributes()
            for temperature_sensors in sorted(self.config.BaseStation3200[1].temperature_sensors.keys()):
                self.config.BaseStation3200[1].temperature_sensors[temperature_sensors].bicoder.\
                    self_test_and_update_object_attributes()
            for pump in sorted(self.config.BaseStation3200[1].pumps.keys()):
                self.config.BaseStation3200[1].pumps[pump].bicoder.\
                    self_test_and_update_object_attributes()
            for pressure_sensor in sorted(self.config.BaseStation3200[1].pressure_sensors.keys()):
                self.config.BaseStation3200[1].pressure_sensors[pressure_sensor].bicoder.\
                    self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller agaist information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        - load replacement devices:
        - search and address new devices
        Replace zones:
            - ZN address 1 serial number TSD0001 ----> Serial number TSQ0091
            - ZN address 2 serial number TSQ0071 ----> Serial number TSQ0092
            - ZN address 197 serial number TSQ0074 ----> Serial number TSQ0093
            - ZN address 198 serial number TSQ0081 ----> Serial number TSQ0094
        Replace Moisture Sensor
            - MS address 1 serial number SB05308 ----> Serial number SB01250
            - MS address 2 serial number SB06300 ----> Serial number SB01251
            - MS address 3 serial number SB07258 ----> Serial number SB01252
        Replace Temperature Sensor
            - TS address 1 serial number TAT0001 ----> Serial number TAT0003
            - TS address 2 serial number TAT0002 ----> Serial number TAT0004
        Replace Pressure Sensor
            - PS address 1 serial number PSF0001 ----> Serial number PSF0002
        Replace Event Switches
            - SW address 1 serial number TPD0001 ----> Serial number TPD0005
            - SW address 2 serial number TPD0002 ----> Serial number TPD0006
            - SW address 3 serial number TPD0003 ----> Serial number TPD0007
        Replace Flow Meters
            - FM address 1 serial number TWF0001 ----> Serial number TWF0005
            - FM address 2 serial number TWF0001 ----> Serial number TWF0006
            - FM address 3 serial number TWF0001 ----> Serial number TWF0007
            - FM address 4 serial number TWF0001 ----> Serial number TWF0008
        Replace Master Valves
            - MV address 1 serial number TMV0003 ----> Serial number TMV0006
            - MV address 2 serial number TMV0004 ----> Serial number TMV0007
            - MV address 8 serial number TMV0005 ----> Serial number TMV0008
        Replace Pumps
            - PP address 1 serial number TPR0011 ----> Serial number TPR0013
            - PP address 2 serial number TPR0012 ----> Serial number TPR0014
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # Overwrite Zone 1
            self.config.BaseStation3200[1].zones[1].replace_quad_valve_bicoder('TSQ0091')
             # Overwrite Zone 2
            self.config.BaseStation3200[1].zones[2].replace_quad_valve_bicoder('TSQ0092')
             # Overwrite Zone 197
            self.config.BaseStation3200[1].zones[197].replace_quad_valve_bicoder('TSQ0093')
            # Overwrite Zone 198
            self.config.BaseStation3200[1].zones[198].replace_quad_valve_bicoder('TSQ0094')

            # Overwrite Moisture Sensors
            self.config.BaseStation3200[1].moisture_sensors[1].replace_moisture_bicoder('SB01250')
            self.config.BaseStation3200[1].moisture_sensors[2].replace_moisture_bicoder('SB01251')
            self.config.BaseStation3200[1].moisture_sensors[3].replace_moisture_bicoder('SB01252')

            # Overwrite Temperature Sensors
            self.config.BaseStation3200[1].temperature_sensors[1].replace_temperature_bicoder('TAT0003')
            self.config.BaseStation3200[1].temperature_sensors[2].replace_temperature_bicoder('TAT0004')

            # Overwrite pressure Sensor 1
            self.config.BaseStation3200[1].pressure_sensors[1].replace_analog_bicoder('PSF0002')

            # Overwrite Event Switch 1
            self.config.BaseStation3200[1].event_switches[1].replace_switch_bicoder('TPD0004')
            self.config.BaseStation3200[1].event_switches[2].replace_switch_bicoder('TPD0005')
            self.config.BaseStation3200[1].event_switches[3].replace_switch_bicoder('TPD0006')


            # Overwrite Flow Meter 1 serial number
            self.config.BaseStation3200[1].flow_meters[1].replace_flow_bicoder('TWF0005')
            self.config.BaseStation3200[1].flow_meters[2].replace_flow_bicoder('TWF0006')
            self.config.BaseStation3200[1].flow_meters[3].replace_flow_bicoder('TWF0007')
            self.config.BaseStation3200[1].flow_meters[4].replace_flow_bicoder('TWF0008')

            # Overwrite Master Valve 8
            self.config.BaseStation3200[1].master_valves[1].replace_single_valve_bicoder('TMV0006')
            self.config.BaseStation3200[1].master_valves[2].replace_single_valve_bicoder('TMV0007')
            self.config.BaseStation3200[1].master_valves[8].replace_single_valve_bicoder('TMV0008')

            # Overwrite Pumps
            self.config.BaseStation3200[1].pumps[1].replace_single_valve_bicoder('TPR0013')
            self.config.BaseStation3200[1].pumps[2].replace_single_valve_bicoder('TPR0014')

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                            self.config.test_name,
                            sys._getframe().f_code.co_name,
                            date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                            str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                         self.config.test_name,
                         sys._getframe().f_code.co_name,
                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                         str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
