import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

__author__ = "Eldin"


class ControllerUseCase31(object):
    """
    Test name: \n
        Concurrent Zones \n
    Purpose: \n
        Verify that the correct number of zones are running when we set a max concurrent zone on the controller or
        on a program. Also make sure that we can change the max zone concurrency and it actually updates. \n
    Coverage area: \n
        1. able to set the number of concurrent zones for each program \n
        2. able to set the number of concurrent zones for the controller \n
        3. verify that the concurrent zone settings manage the number of active valves \n
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        ############################
        - Set up the controller settings \n
            - Set max concurrency on controller to 1 we want 1 zone max to run at a time \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set concurrent zones \n
            - set start times \n
            - set priority \n
            - set watering intervals \n
        """
        program_week_day_every_day = [1, 1, 1, 1, 1, 1, 1]

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=3)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=[0])
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(_sun=True,
                                                                                                           _mon=True,
                                                                                                           _tues=True,
                                                                                                           _wed=True,
                                                                                                           _thurs=True,
                                                                                                           _fri=True,
                                                                                                           _sat=True)

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation3200[1].programs[2].set_enabled()
            self.config.BaseStation3200[1].programs[2].set_max_concurrent_zones(_number_of_zones=3)
            self.config.BaseStation3200[1].programs[2].set_start_times(_st_list=[0])
            self.config.BaseStation3200[1].programs[2].set_watering_intervals_to_selected_days_of_the_week(_sun=True,
                                                                                                           _mon=True,
                                                                                                           _tues=True,
                                                                                                           _wed=True,
                                                                                                           _thurs=True,
                                                                                                           _fri=True,
                                                                                                           _sat=True)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=60)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[1].zone_programs[4].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=5)
            self.config.BaseStation3200[1].programs[2].zone_programs[5].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[2].zone_programs[5].set_run_time(_minutes=60)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=6)
            self.config.BaseStation3200[1].programs[2].zone_programs[6].set_as_linked_zone(_primary_zone=5)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=7)
            self.config.BaseStation3200[1].programs[2].zone_programs[7].set_as_linked_zone(_primary_zone=5)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=8)
            self.config.BaseStation3200[1].programs[2].zone_programs[8].set_as_linked_zone(_primary_zone=5)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        - Verify that we set up our objects in a way that correctly mirrors what is on the controller. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        - Set the date and time on the controller to be 04/08/2014 07:00:00. \n
        - The time is now 07:00:00. \n
        - Get the data of each zone and program on the controller. \n
        - Verify Statuses: \n
            - Program 1: Done Watering because the time hasn't been incremented to trigger a start condition. \n
                - Zone 1: Done Watering because the time hasn't been incremented to trigger a start condition. \n
                - Zone 2: Done Watering because the time hasn't been incremented to trigger a start condition. \n
                - Zone 3: Done Watering because the time hasn't been incremented to trigger a start condition. \n
                - Zone 4: Done Watering because the time hasn't been incremented to trigger a start condition. \n
            - Program 2: Done Watering because the time hasn't been incremented to trigger a start condition. \n
                - Zone 5: Done Watering because the time hasn't been incremented to trigger a start condition. \n
                - Zone 6: Done Watering because the time hasn't been incremented to trigger a start condition. \n
                - Zone 7: Done Watering because the time hasn't been incremented to trigger a start condition. \n
                - Zone 8: Done Watering because the time hasn't been incremented to trigger a start condition. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set the date and time
            self.config.BaseStation3200[1].set_date_and_time(_date="04/08/2014", _time="07:00:00")

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].verify_status_is_done()

            # Verify all Zones
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        - Manually start program 1 & 2. \n
        - Increment the clock by 1 minute. \n
        - The time is now 07:01:00. \n
        - Get the data of each zone and program on the controller. \n
        - Verify Statuses: \n
            - Program 1: Running because it was manually triggered. \n
                - Zone 1: Watering because the program it is on was manually triggered. \n
                - Zone 2: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 3: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 4: Waiting to water because max concurrent zones on the controller is 1. \n
            - Program 2: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 5: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 6: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 7: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 8: Waiting to water because max concurrent zones on the controller is 1. \n
        - Manually stop program 1 & 2. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Manually start both Programs
            self.config.BaseStation3200[1].programs[1].set_program_to_start()

            self.config.BaseStation3200[1].programs[2].set_program_to_start()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].verify_status_is_waiting_to_run()

            # Verify all Zones
            self.config.BaseStation3200[1].zones[1].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[6].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[7].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[8].verify_status_is_waiting_to_water()

            # Manually stop both Programs
            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        - Change max zone concurrency to 7 on the controller. \n
        - Each program has a max concurrency of 3
        - Zones 1-4 are on program 1 which can run 3 zones
        - Zones 1-2 are on serial numbers TSE0011 this decoder can run both zones at once
        - Zones 3-4 are on serial numbers TSE0021 this decoder can run both zones at once
        - Zones 5-8 are on program 2 which can run 3 zones
        - zones 5-8 a on serial number TVA3301 which is a 12 zones decoder which can on run two zones at once

        - Manually start program 1 & 2. \n
        - Increment the clock by 1 minute. \n
        - The time is now 07:02:00. \n
        - Get the data of each zone and program on the controller. \n
        - Verify Statuses: \n
        Only 6 zones will be watering even though the controller is set to a max of seven zone because of the 12 valve
        Decoder
            - Program 1: Running because it was manually triggered. \n
                - Zone 1: Watering because max concurrent of the program is 3. \n
                - Zone 2: Watering because max concurrent of the program is 3. \n
                - Zone 3: Watering because max concurrent of the program is 3. \n
                - Zone 4: Waiting to water because max concurrent of the program is 3 \n
            - Program 2: Running because it was manually triggered. \n
                - Zone 5: Watering because max concurrent zones on the program is 3. \n
                - Zone 6: Watering because max concurrent zones on the program is 3. \n
                - Zone 7: Waiting to water because max concurrent zones on the program is 3 but the zones are on. \n
                a 12 valve decoder \n.
                - Zone 8: Waiting to water because max concurrent zones on the program is 3 but the zones are on. \n
                a 12 valve decoder \n.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set max concurrent zones on the controller to be 7
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=7)

            # Manually start both Programs
            self.config.BaseStation3200[1].programs[1].set_program_to_start()
            self.config.BaseStation3200[1].programs[2].set_program_to_start()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].verify_status_is_running()

            # Verify all Zones
            self.config.BaseStation3200[1].zones[1].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[7].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[8].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
