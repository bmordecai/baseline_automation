import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

__author__ = 'Ben'


class ControllerUseCase20_5(object):
    """
    Test name:
        - CN UseCase20_5 Verify Mainline 3-strike rule

    User Story: \n
        1)  As a user I would like my zone to shutdown because a sprinkler head was kicked off so that I don't waste
            precious water, flood my lovely neighbors and have a dead lawn with butterflies.

        2)  As a user I would like my mainline to shutdown because a pipe cracked so that I don't waste precious water,
            flood my lovely neighbors and have a dead lawn with butterflies.

    Coverage area and Objectives: \n
        - Covers 3-strike rule on a Mainline with Advanced High Flow Variance with Shutdown
        - Covers 3-strike rule on a Mainline with Advanced Low Flow Variance with Shutdown
        - Covers 3-strike rule on a Mainline with only 1 Zone running at a time
        - Covers 3-strike rule on a Mainline with multiple Zones running at a time
        - Verify Mainline 3-strike shutdown feature (See ERS for details)

    Scenarios: \n
        1) Get a high flow variance shutdown on ML 1
            - Verify the a high flow variance shutdown message is generated when variance triggered
            - Verify clearing the message clears the faults
        2) Get a low flow variance shutdown on ML 8
            - Verify the a low flow variance shutdown message is generated when variance triggered
            - Verify clearing the message clears the faults

    Not Covered: \n
    - Multiple pocs attached to single mainline

    Test Configuration setup: \n
        1. test 3-strike rule on Advance high flow variance Tier 1 with Shutdown\n
            - Tier 1 range 0 - 25 GPM \n
            - Configuration
                - WS 1 ---> POC 1 ---> ML 1 \n
                    - POC 1
                        - FM 1
                        - MV 1
                        - PM 1
                    - ML 1  Advanced high flow variance Tier 1 With Shutdown\n
                        - ZN 1 design flow 1.0
                        - ZN 2 design flow 0.0
                        - ZN 3 design flow 0.8
                        - Variance set to 80%
                        - Cause HF Variance Shutdown on ML 1 and ZN 1
                    - PG 1
        2. test 3-strike rule on Advance low flow variance Tier 4 with Shutdown\n
            - Tier 2 range 300 + GPM \n
            - Configuration
                - WS 8 ---> POC 8 ---> ML 8 \n
                    - POC 8
                        - FM 8
                        - MV 8
                        - PM 8
                    - ML 8  Advanced low flow variance Tier 4 With Shutdown\n
                        - ZN 198 design flow 350.0
                        - ZN 199 design flow 0.0
                        - ZN 200 design flow 500.0
                        - Variance set to 10%
                        - Cause LF Variance Shutdown on ML 8 and ZN 198
                    - PG 99
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        # these are global variables for the test
        # zone design flow values

        # ML 1
        # HF Variance Tier 1 (<25gpm)
        self.zn_1_df = 24.0
        self.zn_2_df = 24.0
        self.zn_3_df = 24.0
        
        # ML 8
        # LF Variance Tier 4 (>300gpm)
        self.zn_198_df = 350.0
        self.zn_199_df = 0.0
        self.zn_200_df = 500.0

        # mainline flow variance percentages
        
        # Tier 1
        self.ml_1_hi_fl_vr = 80
        self.ml_1_lo_fl_vr = 0  # only testing HF variance on ML 1

        # Tier 4
        self.ml_8_hi_fl_vr = 0  # only testing LF variance on ML 8
        self.ml_8_lo_fl_vr = 10

        self.run_use_case()

    #################################
    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                        

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set controller max concurrency to allow for all programs and zones to water
            self.config.BaseStation3200[1].set_max_concurrent_zones(40)

            program_start_time_10am = [600]  # 10:00am start time
            program_start_time_11am = [660]  # 11:00am start time
            program_water_windows = ['111111111111111111111111']

            # Add and configure Program 1
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_water_window(_ww=program_water_windows)
            self.config.BaseStation3200[1].programs[1].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=2)
            self.config.BaseStation3200[1].programs[1].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(_sun=True,
                                                                                                           _mon=True,
                                                                                                           _tues=True,
                                                                                                           _wed=True,
                                                                                                           _thurs=True,
                                                                                                           _fri=True,
                                                                                                           _sat=True)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_start_time_10am)

            # Add and configure Program 99
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=99)
            self.config.BaseStation3200[1].programs[99].set_enabled()
            self.config.BaseStation3200[1].programs[99].set_water_window(_ww=program_water_windows)
            self.config.BaseStation3200[1].programs[99].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[99].set_max_concurrent_zones(_number_of_zones=2)
            self.config.BaseStation3200[1].programs[99].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[99].set_watering_intervals_to_selected_days_of_the_week(_sun=True,
                                                                                                            _mon=True,
                                                                                                            _tues=True,
                                                                                                            _wed=True,
                                                                                                            _thurs=True,
                                                                                                            _fri=True,
                                                                                                            _sat=True)
            self.config.BaseStation3200[1].programs[99].set_start_times(_st_list=program_start_time_11am)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure Program Zone 1
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=10)
            # link zone 2 and 3 to primary zone 1
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1)
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_as_linked_zone(_primary_zone=1)
            
            # Add & Configure Program Zone 198
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=198)
            self.config.BaseStation3200[1].programs[99].zone_programs[198].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[99].zone_programs[198].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[99].zone_programs[198].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[99].zone_programs[198].set_soak_time(_minutes=10)
            # link zone 199 and 200 to primary zone 198
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=199)
            self.config.BaseStation3200[1].programs[99].zone_programs[199].set_as_linked_zone(_primary_zone=198)
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=200)
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_as_linked_zone(_primary_zone=198)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # WS 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[1].set_monthly_watering_budget(_budget=0)
            self.config.BaseStation3200[1].water_sources[1].set_water_rationing_to_disabled()

            # WS 8
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=8)
            self.config.BaseStation3200[1].water_sources[8].set_enabled()
            self.config.BaseStation3200[1].water_sources[8].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[8].set_monthly_watering_budget(_budget=0)
            self.config.BaseStation3200[1].water_sources[8].set_water_rationing_to_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].add_master_valve_to_point_of_control(_master_valve_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_pump_to_point_of_control(_pump_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_flow_meter_to_point_of_control(_flow_meter_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=900)
            # Add POC 1 to Water Source 1
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(_point_of_control_address=1)
            
            # Add & Configure POC 8
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=8)
            self.config.BaseStation3200[1].points_of_control[8].set_enabled()
            self.config.BaseStation3200[1].points_of_control[8].add_master_valve_to_point_of_control(_master_valve_address=8)
            self.config.BaseStation3200[1].points_of_control[8].add_pump_to_point_of_control(_pump_address=8)
            self.config.BaseStation3200[1].points_of_control[8].add_flow_meter_to_point_of_control(_flow_meter_address=8)
            self.config.BaseStation3200[1].points_of_control[8].set_target_flow(_gpm=900)
            # Add POC 8 to Water Source 8
            self.config.BaseStation3200[1].water_sources[8].add_point_of_control_to_water_source(_point_of_control_address=8)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control
        """
        # here we can either execute the following uncommented lines in procedural fashion, or we could re-init the
        # object, would have to import Mainline at the top, effectively accomplishing the same thing by:
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure ML 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=25)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[1].set_use_advanced_flow_to_true()
            self.config.BaseStation3200[1].mainlines[1].set_high_flow_variance_tier_one(_percent=self.ml_1_hi_fl_vr,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].mainlines[1].set_low_flow_variance_tier_one(_percent=self.ml_1_lo_fl_vr,
                                                                                       _with_shutdown_enabled=True)
            # Add ML 1 to POC 1
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)
            
            # Add & Configure ML 8
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=8)
            self.config.BaseStation3200[1].mainlines[8].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[8].set_target_flow(_gpm=900)
            self.config.BaseStation3200[1].mainlines[8].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[8].set_use_advanced_flow_to_true()
            self.config.BaseStation3200[1].mainlines[8].set_high_flow_variance_tier_four(_percent=self.ml_8_hi_fl_vr,
                                                                                         _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].mainlines[8].set_low_flow_variance_tier_four(_percent=self.ml_8_lo_fl_vr,
                                                                                        _with_shutdown_enabled=True)
            # Add ML 8 to POC 8
            self.config.BaseStation3200[1].points_of_control[8].add_mainline_to_point_of_control(_mainline_address=8)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=1)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=2)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=3)
            
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=198)
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=199)
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=200)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ######################
        setup zones on mainlines
        ######################
         - set up zone  Attributes associated with main lines \n
            - set design flow on zone
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Assign a design flow value to each zone so that they have a default setting
            self.config.BaseStation3200[1].zones[1].set_design_flow(_gallons_per_minute=self.zn_1_df)
            self.config.BaseStation3200[1].zones[2].set_design_flow(_gallons_per_minute=self.zn_2_df)
            self.config.BaseStation3200[1].zones[3].set_design_flow(_gallons_per_minute=self.zn_3_df)
            self.config.BaseStation3200[1].zones[198].set_design_flow(_gallons_per_minute=self.zn_198_df)
            self.config.BaseStation3200[1].zones[199].set_design_flow(_gallons_per_minute=self.zn_199_df)
            self.config.BaseStation3200[1].zones[200].set_design_flow(_gallons_per_minute=self.zn_200_df)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        ############################
        Verify 3-Strike rule using Adv. High Flow Variance with Shutdown
        ############################


        (1) Set controller time to 9:59 AM (1 minute before Program 1's start time)

            - Increment clock forward to 10:00 AM
            - Verify initial status' for all flow components
            - Set FM 1's flow rate above the variance threshold to trigger variance on Zone 1

        (2) Increment clock forward from 10:00 to 10:01 AM

            - Verify Zones 1 has "watering" status
                -> b/c not enough flow left for other zones to turn on.
            - Verify Zones 2 and 3 have "waiting" status
                -> b/c not enough flow left for other zones to turn on
            - Verify the following flow components go into "running" status' because of Program 1 starting:
                + Water Source 1 (because it's attached to POC 1)
                + Point of Control 1 (because it's attached to ML 1)
                + Mainline 1 (because the Zones on Program 1 are assigned to Mainline 1)
                + Master Valve 1 (because it's attached to POC 1)
                + Pump 1 (because it's attached to POC 1)
                + Flow Meter 1 (because it's attached to POC 1)

            What happened during this minute on the controller:
                - 1 min flow stabilization period completed
                - FM 1's flow rate was read and will be used to check for and act on any flow variance during the
                  10:01-10:02 min processing

        (3) Increment clock forward from 10:01 to 10:02 AM

            - Expect a high flow variance to be detected on Mainline 1
                - Verify Zone 1 has "watering" status
                    -> Zone 1 received a strike, but will be ran again because Zone with 1 strike has higher priority
                - Verify Zone 2 still "waiting" status
                    -> Nothing happened to turn it on yet
                - Verify Zone 3 is still "waiting" status
                    -> Nothing happened to turn it on yet

            What happened during this minute on the controller:
                - Flow fault (detected from 10:00 to 10:01 min processing) acted on
                    - Mainline 1 gets first strike
                    - Zone 1 gets first strike
                - Started and completed a new 1 min flow stabilization period for Mainline 1 (because of flow fault)
                - FM 1's flow rate was read and will be used to check for and act on any flow variance during the
                  10:02-10:03 min processing

        (4) Increment clock forward from 10:02 to 10:03 AM

            - Expect a high flow variance to be detected on Mainline 1
                - Verify Zone 1 transitions to "soaking" status
                    -> Zone 1 received second strike (IN A ROW...this is key); controller will try to run other zones
                       before trying to run Zone 1 again so that the system doesn't shutdown immediately.
                - Verify Zone 2 goes to "watering" status
                    -> Zone 2 was next in queue to start "watering"
                - Verify Zone 3 is still "waiting" status
                    -> Nothing happened to turn it on yet
                - Update FM 1's flow rate to be above the variance using Zone 2's design flow
                    -> This makes it so we can cause a second flow fault on the Mainline during the next min processing
                       (10:03 to 10:04)

            What happened during this minute on the controller:
                - Flow fault (detected from 10:01 to 10:02 min processing) acted on
                    - Mainline 1 gets second strike
                    - Zone 1 gets second strike
                - Started and completed a new 1 min flow stabilization period for Mainline 1 (because of flow fault)
                - FM 1's flow rate was read and will be used to check for and act on any flow variance during the
                  10:03-10:04 min processing

        (5) Increment clock forward from 10:03 to 10:04 AM
        
            (NOTE: The status verifiers have been modified to verify current v16 functionality so we can ship. The
                   following JIRA story (https://baseline.atlassian.net/browse/ZZ-1642) addresses this issue. When that
                   JIRA story is completed, this use case should fail on this step. At that point, the right status
                   verifiers can be un-commented out)

            - Expect 3rd strike on Mainline -> High flow variance detected while Zone 3 was running
                - Verify Mainline shuts down and has "Flow Fault" status (per spec)
                - Verify POC shuts down and has "Flow Fault" status (per spec)
                - Verify Zones are set to done (per spec)
                - Verify MV 1 has "Off" status
                - Verify PM 1 has "Off" status
                - Verify High flow variance shutdown Mainline message (per spec)
                - Verify High flow variance shutdown POC message (per spec)
                - Update FM 1's rate to 0 (without doing this, because our FM still has a "flow" > 0, it will be
                  running when we aren't expecting)
                    - Verify FM 1 has "OK" status after

            What happened during this minute on the controller:
                - Flow fault (detected from 10:02 to 10:03 min processing) acted on
                    - Mainline 1 gets third strike
                        - Shuts down watering related to Mainline 1
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # Set controllers date/time to 1 minute before program start time (10am)
            date_mngr.set_current_date_to_match_computer()
            self.config.BaseStation3200[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                             _time='09:59:00')

            # ---------------------------------------- #
            #                                          #
            # 9:59 AM                                  #
            #                                          #
            # ---------------------------------------- #

            # Increment controller clock forward 1 minute to trigger program starts
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # ---------------------------------------- #
            #                                          #
            # 10:00 AM                                 #
            #                                          #
            # ---------------------------------------- #

            # Verify initial status' before program starts

            # Verify system is NOT running
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_ok()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()

            # Increment controller clock forward 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Set FM 1's rate to be above variance for first Zone to run (Zone 1)
            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _variance_percent=self.ml_1_hi_fl_vr,
                _expected_gpm=self.zn_1_df)

            # ---------------------------------------- #
            #                                          #
            # 10:01 AM                                 #
            #                                          #
            # ---------------------------------------- #

            # Verify system is running
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Verify ZN 1 starts "Watering" first
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()

            # Increment controller clock forward 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # ---------------------------------------- #
            #                                          #
            # 10:02 AM                                 #
            #                                          #
            # ---------------------------------------- #

            # Verify system is still running
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Verify ZN 1 continues to water after receiving 1st strike
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()

            # Increment controller clock forward 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            
            # TODO: Uncomment below when JIRA story finished in future (https://baseline.atlassian.net/browse/ZZ-1643)
            # Set FM 1's rate to be above variance for the next Zone to turn on
            #   -> We are expecting a flow fault to occur and cause Zone 1 to start soaking and Zone 2 to water
            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _variance_percent=self.ml_1_hi_fl_vr,
                _expected_gpm=self.zn_2_df)

            # ---------------------------------------- #
            #                                          #
            # 10:03 AM                                 #
            #                                          #
            # ---------------------------------------- #

            # Verify system is still running
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # TODO: Uncomment below when JIRA story finished in future (https://baseline.atlassian.net/browse/ZZ-1643)
            # Verify ZN 1 transitioned to "Soaking" after receiving 2nd strike and ZN 2 started "Watering"
            # self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            # self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            # self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            
            # TODO: Current v16 functionality causes Zone 1 to run until both Zone 1 and ML 1 are shutdown. With the
            # TODO: implementation of JIRA (https://baseline.atlassian.net/browse/ZZ-1643), the below 3 status' would be
            # TODO: invalid and the above 3 status' would be valid
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()

            # Increment controller clock forward 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # ---------------------------------------- #
            #                                          #
            # 10:04 AM                                 #
            #                                          #
            # ---------------------------------------- #

            # -------------------------------------------------------- #
            # Verify Feature based on 3200 spec (under ML definition): #
            # -------------------------------------------------------- #

            #
            # (1) ML 1 is shutdown and gets Flow Fault status
            #

            # TODO: Future status (See https://baseline.atlassian.net/browse/ZZ-1642)
            # self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_flow_fault()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_off()

            #
            # (2) ML 1's assigned POC is shutdown and gets Flow Fault status
            #

            # TODO: Future status (See https://baseline.atlassian.net/browse/ZZ-1642)
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_flow_fault()
            
            #
            # (3) ML 1 has high flow shutdown message
            #
            
            # self.config.BaseStation3200[1].mainlines[1].messages.verify_high_flow_variance_shutdown_message()
            self.config.BaseStation3200[1].mainlines[1].messages.clear_high_flow_variance_shutdown_message()

            #
            # (4) ML 1's zones are set to Done
            #
            
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_error()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].messages.verify_shutdown_on_high_flow_variance_message()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].messages.clear_shutdown_on_high_flow_variance_message()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()

            # Set FM 1's rate to 0 because we are expecting the Mainline to get it's 3rd strike when incrementing the
            # clock from 10:03 to 10:04. The FM needs a 0 flow rate in order to be moved into an "Okay" status.
            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _expected_gpm=0.0)

            # -------------------------------------------------------- #
            # Verify Related Flow Components stop "Running"/"Watering" #
            # -------------------------------------------------------- #

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_ok()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        ############################
        Verify 3-Strike rule using Adv. Low Flow Variance with Shutdown
        ############################
        
        
        (1) Set controller time to 10:59 AM (1 minute before Program 1's start time)
            
            - Increment clock forward to 11:00 AM
            - Verify initial status' for all flow components
            - Set FM 8's flow rate below the variance threshold to trigger variance when Zones 198 and 199 are watering
        
        (2) Increment clock forward from 11:00 to 11:01 AM
        
            - Verify Zone 198 and 199 turn on and have "watering" status
                -> Program 99 concurrency set to 2
            - Verify Zone 200 has "waiting" status
                -> b/c max zone concurrency for Program 99 reached
            - Verify the following flow components go into "running" status' because of Program 99 starting:
                + Water Source 8 (because it's attached to POC 8)
                + Point of Control 8 (because it's attached to ML 8)
                + Mainline 8 (because the Zones on Program 99 are assigned to Mainline 8)
                + Master Valve 8 (because it's attached to POC 8)
                + Pump 8 (because it's attached to POC 8)
                + Flow Meter 8 (because it's attached to POC 8)
        
            What happened during this minute on the controller:
                - 1 min flow stabilization period completed
                - FM 8's flow rate was read and will be used to check for and act on any flow variance during the
                  11:01-11:02 min processing
                  
        (3) Increment clock forward from 11:01 to 11:02 AM
            
            - Expect a low flow variance to be detected on Mainline 8
                - Verify Zone 198 and 200 have "watering" status
                    -> Zone 198 received a strike, but remains "watering" and will be turned on with Zone 200 because
                       the controller will run 1 zone with 1 strike plus other zones as long as flow is available and
                       the other zones don't have strikes
                - Verify Zone 199 has "soaking" status
                    -> Received first strike, but since only 1 zone can run with a strike in a group of zones, it stays
                       "soaking"  
                - Update FM 8's flow rate to be below the variance using Zone 198 and 200's combined design flow
                    -> This makes it so we can cause a second flow fault on the Mainline during the next min processing
                       (11:02 to 11:03)
                    
            What happened during this minute on the controller:
                - Flow fault (detected from 11:00 to 11:01 min processing) acted on
                    - Mainline 8 gets first strike
                    - Zone 198 gets first strike
                    - Zone 199 gets first strike
                - Started and completed a new 1 min flow stabilization period for Mainline 8 (because of flow fault)
                - FM 8's flow rate was read and will be used to check for and act on any flow variance during the
                  11:02-11:03 min processing
                
        (4) Increment clock forward from 11:02 to 11:03 AM
            
            - Expect a low flow variance to be detected on Mainline 8
                - Verify Zone 198 remains "watering"
                    -> Zone 198 received second strike; Since there are no other Zones without a strike, Zone 198 will
                       be ran by itself.
                - Verify Zone 199 remains in "soaking" status
                    -> Nothing has happened to turn it on
                - Verify Zone 200 transitioned to "soaking
                    -> Received first strike; Since Zone 198 has 2 strikes, it will be ran before Zone 200.
                - Update FM 8's flow rate to be above the variance using Zone 198's design flow
                    -> This makes it so we can cause a third flow fault on the Mainline during the next min processing
                       (11:03 to 11:04)
                    
            What happened during this minute on the controller:
                - Flow fault (detected from 11:01 to 11:02 min processing) acted on
                    - Mainline 8 gets second strike
                    - Zone 198 gets second strike
                    - Zone 200 gets first strike
                - Started and completed a new 1 min flow stabilization period for Mainline 8 (because of flow fault)
                - FM 8's flow rate was read and will be used to check for and act on any flow variance during the
                  11:03-11:04 min processing
                
        (5) Increment clock forward from 11:03 to 11:04 AM
        
            (NOTE: The status verifiers have been modified to verify current v16 functionality so we can ship. The
                   following JIRA story (https://baseline.atlassian.net/browse/ZZ-1642) addresses this issue. When that
                   JIRA story is completed, this use case should fail on this step. At that point, the right status
                   verifiers can be un-commented out) 
            
            - Expect both:
                1) 3rd strike on Mainline to cause Mainline Shutdown
                2) 3rd strike on Zone 198 to cause Zone Shutdown
                
            - Verify:
                - Mainline shuts down and has "Flow Fault" status (per spec)
                - POC shuts down and has "Flow Fault" status (per spec)
                - Zone 198 is shutdown and has "Error" status (per spec)
                - Zones 199 and 200 are set to done (per spec)
                - MV 8 has "Off" status
                - PM 8 has "Off" status
                - Low flow variance shutdown Mainline message (per spec)
                - Low flow variance shutdown Zone message (per spec)
                - Low flow variance shutdown POC message (per spec)
                - Update FM 8's rate to 0 (without doing this, because our FM still has a "flow" > 0, it will be
                  running when we aren't expecting)
                    - Verify FM 8 has "OK" status after
                    
            What happened during this minute on the controller:
                - Flow fault (detected from 11:02 to 11:03 min processing) acted on
                    - Mainline 8 and Zone 198 gets third strike
                        - Shuts down watering related to Mainline 8
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
        
            # Set controllers date/time to 1 minute before program start time (10am)
            date_mngr.set_current_date_to_match_computer()
            self.config.BaseStation3200[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                             _time='10:59:00')
        
            # ---------------------------------------- #
            #                                          #
            # 10:59 AM                                  #
            #                                          #
            # ---------------------------------------- #
        
            # Increment controller clock forward 1 minute to trigger program starts
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
        
            # ---------------------------------------- #
            #                                          #
            # 11:00 AM                                 #
            #                                          #
            # ---------------------------------------- #
        
            # Verify initial status' before program starts
        
            # Verify system is NOT running
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].water_sources[8].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].points_of_control[8].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[8].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].master_valves[8].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[8].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].flow_meters[8].statuses.verify_status_is_ok()
        
            self.config.BaseStation3200[1].zones[198].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[199].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[200].statuses.verify_status_is_done()
        
            # Increment controller clock forward 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            
            # Set FM 8's rate to be above variance for first zone group to run (Zones 198, 199)
            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[8],
                _variance_percent=self.ml_8_lo_fl_vr,
                _expected_gpm=self.zn_198_df + self.zn_199_df)
        
            # ---------------------------------------- #
            #                                          #
            # 11:01 AM                                 #
            #                                          #
            # ---------------------------------------- #
        
            # Verify system is running
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].water_sources[8].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[8].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[8].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[8].statuses.verify_status_is_running()
        
            # Verify ZN 198 and 199 start "Watering" first
            self.config.BaseStation3200[1].zones[198].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[199].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[200].statuses.verify_status_is_waiting_to_water()
        
            # Increment controller clock forward 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            
            # Set FM 8's rate to be above variance for next zone group to run (Zones 198, 200)
            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[8],
                _variance_percent=self.ml_8_lo_fl_vr,
                _expected_gpm=self.zn_198_df + self.zn_200_df)
        
            # ---------------------------------------- #
            #                                          #
            # 11:02 AM                                 #
            #                                          #
            # ---------------------------------------- #
        
            # Verify system is still running
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].water_sources[8].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[8].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[8].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[8].statuses.verify_status_is_running()
        
            # Verify flow fault causes ZN 198 and 200 to start watering, ZN 199 to start soaking
            # zone 199 got the first strike
            self.config.BaseStation3200[1].zones[198].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[199].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[200].statuses.verify_status_is_watering()
        
            # Increment controller clock forward 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            
            # Set FM 8's rate to be above variance for the next zone group to turn on
            #   -> We are expecting a flow fault to occur and cause Zone 198 to run by itself (it has 2 strikes)
            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[8],
                _variance_percent=self.ml_8_lo_fl_vr,
                _expected_gpm=self.zn_198_df)
        
            # ---------------------------------------- #
            #                                          #
            # 11:03 AM                                 #
            #                                          #
            # ---------------------------------------- #
        
            # Verify system is still running
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].water_sources[8].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[8].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[8].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[8].statuses.verify_status_is_running()

            # Verify flow fault causes ZN 198 water by itself, and ZN 199 and 200 to be soaking
            self.config.BaseStation3200[1].zones[198].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[199].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[200].statuses.verify_status_is_soaking()
        
            # Increment controller clock forward 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
        
            # ---------------------------------------- #
            #                                          #
            # 11:04 AM                                 #
            #                                          #
            # ---------------------------------------- #
            
            # TODO: FUTURE - JIRA item https://baseline.atlassian.net/browse/ZZ-1642 will cause this test to fail,
            # TODO: statuses and such will have to be updated/fixed when the story is fixed.
        
            # -------------------------------------------------------- #
            # Verify Feature based on 3200 spec (under ML definition): #
            # -------------------------------------------------------- #

            #
            # (1) ML 8 is shutdown and gets Flow Fault status
            #
            
            # TODO: Future status (See https://baseline.atlassian.net/browse/ZZ-1642)
            self.config.BaseStation3200[1].mainlines[8].statuses.verify_status_is_flow_fault()

            #
            # (2) ML 8's assigned POC is shutdown and gets Flow Fault status
            #
            
            # TODO: Future status (See https://baseline.atlassian.net/browse/ZZ-1642)
            # self.config.BaseStation3200[1].points_of_control[8].statuses.verify_status_is_flow_fault()
            self.config.BaseStation3200[1].points_of_control[8].statuses.verify_status_is_off()

            #
            # (3) ML 8 has low flow shutdown message
            #
            
            self.config.BaseStation3200[1].mainlines[8].messages.verify_low_flow_variance_shutdown_message()
            self.config.BaseStation3200[1].mainlines[8].messages.clear_low_flow_variance_shutdown_message()

            #
            # (4) ML 8's zone 200 has Error status with shutdown on low flow variance message
            #
            
            self.config.BaseStation3200[1].zones[198].statuses.verify_status_is_error()
            self.config.BaseStation3200[1].programs[99].zone_programs[198].messages.verify_shutdown_on_low_flow_variance_message()
            self.config.BaseStation3200[1].programs[99].zone_programs[198].messages.clear_shutdown_on_low_flow_variance_message()

            #
            # (5) ML 8's other zones are set to Done
            #
            
            # TODO: Future status (See https://baseline.atlassian.net/browse/ZZ-1642)
            self.config.BaseStation3200[1].zones[199].statuses.verify_status_is_done()
            # self.config.BaseStation3200[1].zones[199].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[200].statuses.verify_status_is_done()
            # self.config.BaseStation3200[1].zones[200].statuses.verify_status_is_soaking()

            # Set FM 8's rate to 0 because we are expecting the Mainline to get it's 3rd strike when incrementing the
            # clock from 11:03 to 11:04. The FM needs a 0 flow rate in order to be moved into an "Okay" status.
            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[8],
                _expected_gpm=0.0)

            # -------------------------------------------------------- #
            # Verify Related Flow Components stop "Running"/"Watering" #
            # -------------------------------------------------------- #

            # TODO: Future status (See https://baseline.atlassian.net/browse/ZZ-1642)
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()
            # self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_waiting_to_run()
            
            self.config.BaseStation3200[1].water_sources[8].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].master_valves[8].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[8].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].flow_meters[8].statuses.verify_status_is_ok()
    
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
