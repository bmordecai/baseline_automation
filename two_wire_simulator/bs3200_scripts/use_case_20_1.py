import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

__author__ = 'Tige'


class ControllerUseCase20_1(object):
    """
    Test name:
        - CN UseCase20_1 Advanced High Flow Variance with Shutdown for Tier 1 (0 - 25 GPM range)

    User Story: \n
        1)  As a user I would like my zone to shutdown because a sprinkler head was kicked off so that I don't waste
            precious water, flood my lovely neighbors and have a dead lawn with butterflies.

        2)  As a user after I fixed my irrigation issue I would like to be able to resume watering.

    Coverage and Objectives: \n
        - Covers Advanced High Flow Variance Tier 1 with Shutdown
        - Verifies watering continues when a advanced high variance event is detected and the zone is shutdown
        - Verifies and clears high flow variance shutdown message for zone
        - Verifies watering continues after zone shutdown.

    Scenarios: \n
        - Verifies high flow variance tier 1 high flow shuts down watering with shutdown enabled

    Not Covered: \n
        - Multiple pocs attached to single mainline

    Test Configuration setup: \n
        1. test Advance high flow variance Tier 1 with Shutdown\n
            - Tier 1 range 0 - 25 GPM \n
            - Configuration
                - WS 1 ---> POC 1 ---> ML 1 \n
                    - POC 1
                        - FM 1
                        - MV 1
                        - PM 1
                    - ML 1  Advanced high flow variance Tier 1 With Shutdown\n
                        - ZN 1 design flow 1.0
                        - ZN 2 design flow 1.0
                        - ZN 3 design flow 0.8
                        - Variance set to 80%
                        - Cause HF Variance Shutdown on ZN 2
                    - PG 1
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        # these are global variables for the test
        # zone design flow values

        # ML 1
        # HF Variance Tier 1 (<25gpm)
        self.zn_1_df = 1.0
        self.zn_2_df = 1.0
        self.zn_3_df = 0.8

        # mainline flow variance percentages

        # Tier 1
        self.ml_1_hi_fl_vr = 80
        self.ml_1_lo_fl_vr = 0  # only testing HF variance on ML 1

        # Placeholders for variance trigger calculations.
        #   -> These values will represent the calculated actaul flow needed for FlowMeters in order to trigger the
        #      threshold boundaries.
        self.setting_for_ml_1_high_flow_variance_calculation = None

        self.run_use_case()

    #################################
    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set controller max concurrency to allow for all programs and zones to water
            self.config.BaseStation3200[1].set_max_concurrent_zones(40)

            program_start_time_10am = [600]  # 10:00am start time
            program_water_windows = ['111111111111111111111111']

            # Add and configure Program 1
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_water_window(_ww=program_water_windows)
            self.config.BaseStation3200[1].programs[1].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=2)
            self.config.BaseStation3200[1].programs[1].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(_sun=True,
                                                                                                           _mon=True,
                                                                                                           _tues=True,
                                                                                                           _wed=True,
                                                                                                           _thurs=True,
                                                                                                           _fri=True,
                                                                                                           _sat=True)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_start_time_10am)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure Program Zone 1
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=10)
            # link zone 2 and 3 to primary zone 1
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1)
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_as_linked_zone(_primary_zone=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[1].set_monthly_watering_budget(_budget=0)
            self.config.BaseStation3200[1].water_sources[1].set_water_rationing_to_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].add_master_valve_to_point_of_control(_master_valve_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_pump_to_point_of_control(_pump_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_flow_meter_to_point_of_control(_flow_meter_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=900)
            # Add POC 1 to Water Source 1
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(_point_of_control_address=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control
        """
        # here we can either execute the following uncommented lines in procedural fashion, or we could re-init the
        # object, would have to import Mainline at the top, effectively accomplishing the same thing by:
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure ML 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=25)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[1].set_use_advanced_flow_to_true()
            self.config.BaseStation3200[1].mainlines[1].set_high_flow_variance_tier_one(_percent=self.ml_1_hi_fl_vr,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].mainlines[1].set_low_flow_variance_tier_one(_percent=self.ml_1_lo_fl_vr,
                                                                                       _with_shutdown_enabled=True)
            # Add ML 1 to POC 1
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=1)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=2)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=3)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ######################
        setup zones on mainlines
        ######################
         - set up zone  Attributes associated with main lines \n
            - set design flow on zone
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Assign a design flow value to each zone so that they have a default setting
            self.config.BaseStation3200[1].zones[1].set_design_flow(_gallons_per_minute=self.zn_1_df)
            self.config.BaseStation3200[1].zones[2].set_design_flow(_gallons_per_minute=self.zn_2_df)
            self.config.BaseStation3200[1].zones[3].set_design_flow(_gallons_per_minute=self.zn_3_df)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        ############################
        Configure FlowMeters
        ############################

        - Configure FlowMeters to trigger HF/LF variance with calculated actual flow values
        - FlowMeter 1 will have values to trigger HF variance
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            
            # --------------------------------------- #
            # Configure High Flow Variance Components #
            # --------------------------------------- #
            
            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _variance_percent=self.ml_1_hi_fl_vr,
                _expected_gpm=self.zn_1_df + self.zn_2_df)
            
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        ############################
        Increment clock to start programming
        ############################

        - Set date and time on controller and increment clock to start zones running
        - Increment clock past program's start time of 10am
            - All programs should start
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ---------------------------------------------------------------------- #
            # Set controllers date/time to 1 minute before program start time (10am) #
            # ---------------------------------------------------------------------- #

            date_mngr.set_current_date_to_match_computer()
            self.config.BaseStation3200[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                             _time='09:59:00')

            # ------------------------------------------------------------ #
            # Increment controller clock to 10am to trigger program starts #
            # ------------------------------------------------------------ #

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        ############################
        Increment clock from 10:00 to 10:01 AM
        ############################
        
        What happened during 10:00 to 10:01 AM Minute Processing:
            - Program starts
            - All hydraulics go to running (no delays)
            - Zones 1,2 go to watering (program concurrency of 2)
            - Zone 3 is waiting
            - Stable flow is read
                - High flow is detected during this minute but not acted on until AFTER the top of the next
                  minute (i.e., will act on high flow reading when clock is incremented from 10:01 to 10:02 am)
                  
        Strikes:
            - ZN 1: 0
            - ZN 2: 0
            - ZN 3: 0
            
            - ML 1: 0
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            
            # time should be 10:01am
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify running components
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Verify program statuses are running.
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ---------------------------------------- #
            # Tier 1 High Flow Variance Components     #
            # ---------------------------------------- #

            # Zone 3 is waiting to water because Program 1 has max concurrency of 2
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        ############################
        Increment clock from 10:01 to 10:02 AM
        ############################
        
        ---
        Step Overview
        ---
            * Verify high flow variance event behavior:
                + Verify Zones 1 and 3 start watering
                + Verify Zone 2 is set to soaking
            * Set FM 1 flow rate to be equal to design flow of Zones 1 and 3 so that no high flow variance event
              happens
        
        ---
        What happened during 10:01 to 10:02 AM Minute Processing:
        ---
            * High flow variance event acted on:
                + Zones 1,2 get strike
                + Mainline 1 gets strike
                + Zones 1,3 start watering
                + Zone 2 starts soaking
        ---
        Strikes:
        ---
            * ZN 1: 1
            * ZN 2: 1
            * ZN 3: 0
            * ML 1: 1
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            
            # time should be 10:02am
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify running components
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Verify program statuses are running.
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ---------------------------------------- #
            # Tier 1 High Flow Variance Components     #
            # ---------------------------------------- #

            # FM 1 reading triggers zone variance on ML 1
            #   -> Mainline 1 Zones 1,2 should be marked as suspected, Zones 1,3 will run
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            
            # Update ML 1 (pipe fill time 1min) flow to NOT trigger high flow variance.
            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _expected_gpm=self.zn_1_df + self.zn_3_df)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        ############################
        Increment clock from 10:02 to 10:11 AM
        ############################
        
        ---
        Step Overview
        ---
            * Simulate 9 minutes forward to simulate the rest of the watering cycle of Zones 1 and 3
            * Verify at the top of the minute our Zone status' are still:
                + Zones 1 and 3 are watering
                + Zone 2 is soaking
                
            NOTE:
                Statuses should remain unchanged in this step due to the processing loop in the 3200. No processing
                is done at exactly the top of the minute, thus the top of the minute on the 3200 reflects the status
                of the processing from the last minute.
        
        ---
        What happened during 10:02 to 10:11 AM Minute Processing:
        ---
            * Current watering cycle for Zones 1 and 3 are completed
            * Device flow readings taken to be used for 10:11 to 10:12 AM processing
            
        ---
        Strikes:
        ---
            * ZN 1: 1
            * ZN 2: 1
            * ZN 3: 0
            * ML 1: 1
            
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            
            # time should be 10:11am
            self.config.BaseStation3200[1].do_increment_clock(minutes=9)

            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Verify program statuses are running.
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ---------------------------------------- #
            # Tier 1 High Flow Variance Components     #
            # ---------------------------------------- #

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_14(self):
        """
        ############################
        Increment clock from 10:11 to 10:12 AM
        ############################
        
        ---
        Step Overview
        ---
            * Increments controller clock forward 1 minute to process previous minute data
            * Verifies Zones 1 and 3 finished watering cycle and started soak cycle
            * Verifies Zone 2 started watering cycle
            * Sets FM 1's flow rate to trigger 80% high flow variance event after pipe fill for ML 1 is complete.
        
        ---
        What happened during 10:11 to 10:12 AM Minute Processing:
        ---
            * Zones 1 and 3 are set to soaking
                + Strikes removed from Zones 1 and 3 because of successful completion of watering cycle
                + Strikes removed from Mainline 1 because of successful completion of a watering cycle of any of its
                  Zones
            * Zone 2 is started
            * Device flow readings taken to be used for 10:12 to 10:13 AM processing
            * Stable flow is achieved because ML pipe fill time is 1 minute and controller decides if flow is stable by
              comparing the current controller sim time and the time the new Zones started watering.
                
                => If (current controller sim time) >= (time zones turned on plus pipe fill time minus 15 seconds):
                    return flow is stable
                    
                For example, if the following:
                
                    controller sim time = 10:12
                    time zones finished turning on = 10:11:08
                    mainline pipe fill = 1 minute
                    
                Then, stable flow is true when:
                
                    (10:12) >= (10:11 + (1 minute - 15 seconds))
                 -> (10:12) >= (10:11:45)
                 -> true
            
        ---
        Strikes:
        ---
            * ZN 1: 0
            * ZN 2: 1
            * ZN 3: 0
            * ML 1: 0
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            
            # time should be 10:12am
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Verify program statuses are running.
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ---------------------------------------- #
            # Tier 1 High Flow Variance Components     #
            # ---------------------------------------- #

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()

            # Update flow rates to trigger variance conditions
            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _variance_percent=self.ml_1_hi_fl_vr,
                _expected_gpm=self.zn_2_df)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_15(self):
        """
        ############################
        Increment clock from 10:12 to 10:13 AM
        ############################
        
        ---
        Step Overview
        ---
            * Increments controller clock forward 1 minute to process previous minute data
            * Verify Zones 1 and 3 are still soaking
            * Verify Zone 2 is still watering (gained second strike and must be ran by itself)
            
            NOTE:
                Statuses should remain unchanged in this step due to the Zone (2) having a second flow fault event
                triggered. The zone is then run by itself to determine if it needs to be shutdown or not.
        
        ---
        What happened during 10:12 to 10:13 AM Minute Processing:
        ---
            * High flow variance event acted on:
                + Stable flow value from 10:11 to 10:12 AM processing used to trigger high flow variance event on Zone 2
                + Zone 2 is given a 2nd strike
                + Zone 2 is started watering cycle again by itself (due to it having 2 strikes)
            * Device flow readings taken to be used for 10:13 to 10:14 AM processing
            * Stable flow is achieved because ML pipe fill time is 1 minute and controller decides if flow is stable by
              comparing the current controller sim time and the time the new Zones started watering.
                
                => If (current controller sim time) >= (time zones turned on plus pipe fill time minus 15 seconds):
                    return flow is stable
                    
                For example, if the following:
                
                    controller sim time = 10:13
                    time zones finished turning on = 10:12:08
                    mainline pipe fill = 1 minute
                    
                Then, stable flow is true when:
                
                    (10:13) >= (10:12 + (1 minute - 15 seconds))
                 -> (10:13) >= (10:12:45)
                 -> true
            
        ---
        Strikes:
        ---
            * ZN 1: 0
            * ZN 2: 2
            * ZN 3: 0
            * ML 1: 1
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            
            # time should be 10:13am
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Verify program statuses are running.
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ---------------------------------------- #
            # Tier 1 High Flow Variance Components     #
            # ---------------------------------------- #

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_16(self):
        """
        ############################
        Increment clock from 10:13 to 10:14 AM
        ############################
        
        ---
        Step Overview
        ---
            * Increments controller clock forward 1 minute to process previous minute data
            * Verify Zones 1 and 3 are still finishing up soak cycle
            * Verify Zone 2 shutdown with error status and high flow variance shutdown message due to receiving 3rd
              strike.
            * Set FM 1's flow rate to 0 to account for no zones running
        
        ---
        What happened during 10:13 to 10:14 AM Minute Processing:
        ---
            * High flow variance event acted on:
                + Stable flow value from 10:12 to 10:13 AM processing used to trigger high flow variance event on Zone 2
                + Zone 2 is given a 3rd strike and shutdown
                + High flow variance shutdown alarm is triggered for Zone 2
            * Device flow readings taken to be used for 10:14 to 10:15 AM processing
            * Stable flow is achieved because ML pipe fill time is 1 minute and controller decides if flow is stable by
              comparing the current controller sim time and the time the new Zones started watering.
                
                => If (current controller sim time) >= (time zones turned on plus pipe fill time minus 15 seconds):
                    return flow is stable
                    
                For example, if the following:
                
                    controller sim time = 10:14
                    time zones finished turning on = 10:13:08
                    mainline pipe fill = 1 minute
                    
                Then, stable flow is true when:
                
                    (10:14) >= (10:13 + (1 minute - 15 seconds))
                 -> (10:14) >= (10:13:45)
                 -> true
            
        ---
        Strikes:
        ---
            * ZN 1: 0
            * ZN 2: 3
            * ZN 3: 0
            * ML 1: 2
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
        
            # time should be 10:14am
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_off()
            # Flow status should be running still because we haven't reset the GPM to 0 until after the verification
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Verify program statuses are running.
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()
        
            # ---------------------------------------- #
            # Tier 1 High Flow Variance Components     #
            # ---------------------------------------- #
            
            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_error()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()

            # Zone 2 had 3 strikes, verify high flow variance shutdown message
            self.config.BaseStation3200[1].programs[1].zone_programs[2].messages.verify_shutdown_on_high_flow_variance_message()
            self.config.BaseStation3200[1].programs[1].zone_programs[2].messages.clear_shutdown_on_high_flow_variance_message()

            # Update flow rates to not trigger variance conditions (set to 0 because no zones are running)
            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _expected_gpm=0)
    
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        
    #################################
    def step_17(self):
        """
        ############################
        Increment clock from 10:14 to 10:21 AM
        ############################
        
        ---
        Step Overview
        ---
            * Increments controller clock forward 7 minutes to simulate the rest of Zone 1 and 3's soak cycle
            * Verify Zones 1 and 3 are still finishing up soak cycle (haven't entered minute processing loop for
              10:21 to 10:22 AM where Zones 1 and 3 will be turned on. Verified next step)
            * Verify Zone 2 has done status because we verified and cleared the message
        
        ---
        What happened during 10:14 to 10:21 AM Minute Processing:
        ---
            * Device flow readings taken to be used for 10:21 to 10:22 AM processing
            * No other actions in regards to this use case
            
        ---
        Strikes:
        ---
            * ZN 1: 0
            * ZN 2: 3
            * ZN 3: 0
            * ML 1: 2
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
        
            # time should be 10:21am
            self.config.BaseStation3200[1].do_increment_clock(minutes=7)

            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_ok()

            # Verify program statuses are running.
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()
        
            # ---------------------------------------- #
            # Tier 1 High Flow Variance Components     #
            # ---------------------------------------- #
        
            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()

            # Update flow rates to not trigger variance conditions
            #   -> Set flow rate here so that when Zones 1 and 3 turn on at 10:22, the flow rate we set here is used
            #   -> to determine stable flow
            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _expected_gpm=self.zn_1_df + self.zn_3_df)
    
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_18(self):
        """
        ############################
        Increment clock from 10:21 to 10:22 AM
        ############################
        
        ---
        Step Overview
        ---
            * Increments controller clock forward 1 minute to process previous minute data
            * Verify Zones 1 and 3 start final watering cycle
            * Verify Zone 2 has done status because we verified and cleared the flow fault shutdown message
            * Verify hydraulic components are "running" again because watering is taking place
        
        ---
        What happened during 10:21 to 10:22 AM Minute Processing:
        ---
            * Device flow readings taken to be used for 10:22 to 10:23 AM processing
            * Stable flow is determine to be achieved (see previous steps for how this is done)
            * No other actions in regards to this use case
            
        ---
        Strikes:
        ---
            * ZN 1: 0
            * ZN 2: 3
            * ZN 3: 0
            * ML 1: 2
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
        
            # time should be 10:22 am
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()

            # Verify program statuses are running.
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
        
            # ---------------------------------------- #
            # Tier 1 High Flow Variance Components     #
            # ---------------------------------------- #
        
            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
    
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_19(self):
        """
        ############################
        Increment clock from 10:22 to 10:32 AM
        ############################
        
        ---
        Step Overview
        ---
            * Increments controller clock forward 10 minutes to complete Zones 1 and 3 final watering cycle
            * Verify all zones are done
            * Verify hydraulic components are done or in "idle" state respectively
        
        ---
        What happened during 10:22 to 10:32 AM Minute Processing:
        ---
            * Device flow readings taken to be used for 10:22 to 10:32 AM processing
            * Stable flow was achieved each minute
            * No flow fault detected
            * No other actions in regards to this use case
            * All strikes are removed
            
        ---
        Strikes:
        ---
            * ZN 1: 0
            * ZN 2: 0
            * ZN 3: 0
            * ML 1: 0
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
        
            # time should be 10:31 am
            self.config.BaseStation3200[1].do_increment_clock(minutes=9)

            # Update flow rates to not trigger variance conditions (set to 0 because no zones are running)
            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _expected_gpm=0)
            
            # time should be 10:32 am
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_ok()
        
            # Verify program statuses are running.
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
        
            # ---------------------------------------- #
            # Tier 1 High Flow Variance Components     #
            # ---------------------------------------- #
        
            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
    
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
