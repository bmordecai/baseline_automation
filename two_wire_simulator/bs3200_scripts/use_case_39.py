# import sys
# from time import sleep
#
# from common.configuration import Configuration
# # this import allows us to directly use the date_mngr
# from common.date_package.date_resource import date_mngr
# # Objects
# from common.objects.base_classes.web_driver import *
#
# from common.imports import opcodes, types
#
# # import log_handler for logging functionality
# from common.logging_handler import log_handler
#
# from common import helper_methods
#
# __author__ = 'Tige'
#
#
# class ControllerUseCase39(object):
#     """
#     Coverage area: \n
#     This test covers smart watering with sensors using both upper and lower watering strategy \n
#     1. able to search for and find moisture sensors \n
#     2. able to assign and un-assign moisture sensors to primary zones \n
#     3. able to set the water mode for sensor, with offset \n
#     4. able to set the calibration frequency for sensors \n
#     5. linked zones follow primary zone cycle on upper limit \n
#     6. linked zones follow primary zone cycle on lower limit \n
#     7. Monthly Upper limit calibration \n
#     8. single time Upper limit calibration \n
#     9. Monthly Lower limit calibration \n
#     10. single time Lower limit calibration \n
#     11. adjuster watering days and verify sensor water still works \n
#     ######################################
# TODO need to add to test
#     multiple primary zones with sensors, all the linked zones in a program will run if one primary is below the limit even when its linked to a different zone. Attached it an event file from my controller showing the following scenarios
#
# 1. The lower numbered primary below the lower limit and the higher number primary above it.
#     -Only the higher primary zone did not run
# 2. The lower numbered primary above the lower limit and the higher number primary below it.
#     -Only the lower primary zone did not run
# 3. Both primary zones above the limit
#     -No zones ran
# 4. Both zones below the limit
#     -All zones ran
#
#     When a program has a Primary zone using a moisture sensor in Upper Limit mode, if the Primary zone cuts off early due to hitting the Upper Limit, the Linked zones, instead of short-cycling by a proportional amount, run for their full programmed duration.
#     """
# def __init__(self, test_name, user_configuration_instance, json_configuration_file):
#     """
#     Initialize 'UseCase1' instance with the specified parameters. \n
#
#     :param test_name:                       Name of the test to name the controller. \n
#     :type test_name:                        str \n
#
#     :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
#     :type user_configuration_instance:      UserConfiguration \n
#
#     :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
#     :type json_configuration_file:          str \n
#     """
#     self.config = Configuration(test_name=test_name,
#                                 user_conf_file=user_configuration_instance,
#                                 data_json_file=json_configuration_file)
#
#     #################################
#     def run_use_case(self):
#         try:
#             number_of_retries = 0
#             retries = 0
#             while True:
#                 # executes a "retry"
#                 try:
#                     # Resets objects to a known state, creates serial connections, creates all objects.
#                     self.config.initialize_for_test()
#                     self.step_1()
#                     self.step_2()
#                     self.step_3()
#                     self.step_4()
#                     self.step_5()
#                     self.step_6()
#                     self.step_7()
#                     self.step_8()
#                     self.step_9()
#                     self.step_10()
#                     self.step_11()
#                     self.step_12()
#                     self.step_13()
#                     self.step_14()
#                     self.step_15()
#                     self.step_16()
#                     self.step_17()
#                     self.step_18()
#                     self.step_19()
#                     self.step_20()
#                     helper_methods.print_test_passed(test_name=self.config.test_name)
#                     break
#                 except Exception as e:
#                     if hasattr(e, 'msg'):
#                         error_txt = e.msg
#                     else:
#                         error_txt = e.message
#                     # getting a none I cant explain
#                     print "Exception received: " + str(error_txt)
#                     print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
#                     self.config.resource_handler.restart_connections()
#                     retries += 1
#
#                     # added more sleep time to let controller processes finish whatever they are doing.
#                     time.sleep(10)
#                     if retries >= number_of_retries:
#                         helper_methods.print_test_failed(test_name=self.config.test_name)
#                         # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
#                         # to the next use case in the list
#                         if log_handler.is_enabled():
#                             log_handler.exception(message=e.message)
#                             break
#                         else:
#                             raise
#         finally:
#             helper_methods.end_controller_test(config_object=self.config)
#
#     #################################
#     def step_1(self):
#         """
#         sets up the controller \n
#             - setup controller \n
#             - Stop clock \n
#             - enable faux IO \n
#             - set the time out on the serial port \n
#         verify BaseManager connection \n
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#                 self.config.BaseStation3200[1].basemanager_connection[1].connect_cn_to_ethernet()
#                 self.config.BaseStation3200[1].basemanager_connection[1].verify_ip_address_state()
#
#             self.config.controllers[1].set_max_concurrent_zones_on_cn(_number_of_zones=2)
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#
#     #################################
#     def step_2(self):
#         """
#         - sets the devices that will be used in the configuration of the controller \n
#         - search and address the devices:
#             - zones                 {zn}
#             - Master Valves         {mv}
#             - Moisture Sensors      {ms}
#             - Temperature Sensors   {ts}
#             - Event Switches        {sw}
#             - Flow Meter            {fm}
#         - once the devices are found they can be addressed so that they can be used in the programming
#             - zones can use addresses {1-200}
#         - the 3200 auto address certain devices in the order it receives them:
#             - Moisture Sensors      {ms}
#
#         """
#
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#             # Load all devices to controller
#             self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
#                                                          mv_d1_list=self.config.mv_d1,
#                                                          d2_list=self.config.d2,
#                                                          mv_d2_list=self.config.mv_d2,
#                                                          d4_list=self.config.d4,
#                                                          dd_list=self.config.dd,
#                                                          ms_list=self.config.ms,
#                                                          fm_list=self.config.fm,
#                                                          ts_list=self.config.ts,
#                                                          sw_list=self.config.sw)
#
#             self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
#             # assign zones an address between 1-200
#             self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
#                                                                              zn_ad_range=self.config.zn_ad_range)
#
#             self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
#             self.config.controllers[1].set_address_and_default_values_for_ms(ms_object_dict=self.config.moisture_sensors,
#                                                                              ms_ad_range=self.config.ms_ad_range)
#
#             # need to create the mainline object so that we can use it in the program object
#             self.config.create_mainline_objects()
#
#
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#
#     #################################
#     def step_3(self):
#         """
#         give each program a start time of 8:00 A.M./9:00 A.M./10:00 A.M./11:00 A.M. \n
#         set up program watering days \n
#         set up water windows for programs \n
#         set programs to have a concurrent zone of 4 \n
#         """
#         # this is set in the PG3200 object
#
#         # set days of the week the programs will water
#         # set program 1 water days to sun,mon,tues,wed,thurs,fri,sat
#         # set program 2 water days to sun,tues,fri
#         program_start_time_8am = [480]
#         program_watering_days_all=[1, 1, 1, 1, 1, 1, 1]  # runs monday/wednesday/friday
#         program_watering_days=[1, 0, 1, 0, 0, 1, 0]  # runs sunday/ Tuesday / Friday
#         program_water_windows = ['111111111111111111111111']
#
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#             self.config.programs[1] = PG3200(_ad=1,
#                                              _en=opcodes.true,
#                                              _ww=program_water_windows,
#                                              _mc=2,
#                                              _ci=opcodes.week_days,
#                                              _wd=program_watering_days_all,
#                                              _sm=[],
#                                              _st=program_start_time_8am,
#                                              _ml=1)
#             self.config.programs[2] = PG3200(_ad=1,
#                                              _en=opcodes.true,
#                                              _ww=program_water_windows,
#                                              _mc=2,
#                                              _ci=opcodes.week_days,
#                                              _wd=program_watering_days,
#                                              _sm=[],
#                                              _st=program_start_time_8am,
#                                              _ml=1)
#             self.config.programs[3] = PG3200(_ad=1,
#                                              _en=opcodes.true,
#                                              _ww=program_water_windows,
#                                              _mc=2,
#                                              _ci=opcodes.week_days,
#                                              _wd=program_watering_days_all,
#                                              _sm=[],
#                                              _st=program_start_time_8am,
#                                              _ml=1)
#             self.config.programs[4] = PG3200(_ad=1,
#                                              _en=opcodes.true,
#                                              _ww=program_water_windows,
#                                              _mc=2,
#                                              _ci=opcodes.week_days,
#                                              _wd=program_watering_days_all,
#                                              _sm=[],
#                                              _st=program_start_time_8am,
#                                              _ml=1)
#
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#
#     #################################
#     def step_4(self):
#         """
#         - Set up Zone Programs \n
#             - Zone 1 Program 1 (Primary Zone) \n
#                 - Run Time: 1800 \n
#                 - Cycle Time: 600 \n
#                 - Soak Time: 1800 \n
#             - Zone 2 Program 1 (Linked to Zone 1) \n
#                  - Tracking Ratio: 100% \n
#             - Zone 3 Program 2 (Linked to Zone 1) \n
#                 - Tracking Ratio: 50% \n
#             - Zone 4 Program 2 (Linked to Zone 1) \n
#                  - Tracking Ratio: 150% \n
#             - Zone 5 Program 3 (Linked to Zone 1) \n
#                 - Tracking Ratio: 100% \n
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#
#             self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
#                                                        prog_obj=self.config.programs[1],
#                                                        _rt=5400,
#                                                        _ct=600,
#                                                        _so=3600,
#                                                        _ms=self.config.moisture_sensors[1].ad,
#                                                        _ws=opcodes.upper_limit,
#                                                        _ll=35.0,
#                                                        _cc=opcodes.calibrate_one_time,
#                                                        _pz=1)
#             self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
#                                                        prog_obj=self.config.programs[1],
#                                                        _ra=100,
#                                                        _pz=1)
#             self.config.zone_programs[3] = ZoneProgram(zone_obj=self.config.zones[3],
#                                                        prog_obj=self.config.programs[1],
#                                                        _ra=100,
#                                                        _pz=1)
#
#
#             self.config.zone_programs[3] = ZoneProgram(zone_obj=self.config.zones[11],
#                                                        prog_obj=self.config.programs[2],
#                                                        _rt=5400,
#                                                        _ct=600,
#                                                        _so=3600,
#                                                        _ms=self.config.moisture_sensors[1].ad,
#                                                        _ws=opcodes.upper_limit,
#                                                        _ll=35.0,
#                                                        _cc=opcodes.calibrate_monthly,
#                                                        _pz=11)
#             self.config.zone_programs[4] = ZoneProgram(zone_obj=self.config.zones[12],
#                                                        prog_obj=self.config.programs[2],
#                                                        _ra=150,
#                                                        _pz=11)
#             self.config.zone_programs[5] = ZoneProgram(zone_obj=self.config.zones[13],
#                                                        prog_obj=self.config.programs[2],
#                                                        _ra=100,
#                                                        _pz=11)
#
#
#
#
#             self.config.zone_programs[6] = ZoneProgram(zone_obj=self.config.zones[21],
#                                                        prog_obj=self.config.programs[3],
#                                                        _rt=5400,
#                                                        _ct=600,
#                                                        _so=3600,
#                                                        _ms=self.config.moisture_sensors[1].ad,
#                                                        _ws=opcodes.upper_limit,
#                                                        _ul=25.0,
#                                                        _cc=opcodes.calibrate_one_time,
#                                                        _pz=21)
#             self.config.zone_programs[7] = ZoneProgram(zone_obj=self.config.zones[22],
#                                                        prog_obj=self.config.programs[3],
#                                                        _ra=150,
#                                                        _pz=21)
#             self.config.zone_programs[8] = ZoneProgram(zone_obj=self.config.zones[23],
#                                                        prog_obj=self.config.programs[3],
#                                                        _ra=100,
#                                                        _pz=21)
#
#
#
#             self.config.zone_programs[9] = ZoneProgram(zone_obj=self.config.zones[31],
#                                                        prog_obj=self.config.programs[4],
#                                                        _rt=5400,
#                                                        _ct=600,
#                                                        _so=3600,
#                                                        _ms=self.config.moisture_sensors[1].ad,
#                                                        _ws=opcodes.upper_limit,
#                                                        _ul=25.0,
#                                                        _cc=opcodes.calibrate_monthly,
#                                                        _pz=31)
#             self.config.zone_programs[10] = ZoneProgram(zone_obj=self.config.zones[32],
#                                                        prog_obj=self.config.programs[4],
#                                                        _ra=150,
#                                                        _pz=31)
#             self.config.zone_programs[11] = ZoneProgram(zone_obj=self.config.zones[33],
#                                                        prog_obj=self.config.programs[4],
#                                                        _ra=100,
#                                                        _pz=31)
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#
#     #################################
#     def step_5(self):
#         """
#         Set up POCs \n
#         POC 1 \n
#             enable POC 1 \n
#             assign master valve TMV0003 and flow meter TWF0003 to POC 1 \n
#             assign POC 1 a target flow of 500 \n
#             assign POC 1 to main line 1 \n
#             set POC priority to 2-medium \n
#             set high flow limit to 550 and enable high flow shut down \n
#             set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
#             set water budget to 100000 and enable the water budget shut down \n
#             enable water rationing \n
#         \n
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#             self.config.create_3200_poc_objects()
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#
#     #################################
#     def step_6(self):
#         """
#         set up main line 1 \n
#             set limit zones by flow to true \n
#             set the pipe fill time to 4 minutes \n
#             set the target flow to 500 \n
#             set the high variance limit to 5% and enable the high variance shut down \n
#             set the low variance limit to 20% and enable the low variance shut down \n
#         \n
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#             # don't need this because the create mainline object set this up in the beginning of the test
#             pass
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#
#     #################################
#     def step_7(self):
#         """
#         verify the entire configuration again \n
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#             self.config.verify_full_configuration()
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#
#
#     #################################
#     def step_8(self):
#         """
#         turn sim mode on in the controller \n
#         stop the clock so that it can be incremented manually \n
#         set the controller date and time so that there is a known days of the week and days of the month \n
#         verify that all zones are working properly by verifying them before starting the test \n
#         on the first day verify that program 1 does a calibration cycle and the program 2 waters \n
#         on the second day verify that program 1 waters and program 2 does not run \n
#         on the third day verify that program 1 waters and program 2 does not run \n
#         on the fourth day verify that program 1 waters and program 2 waters \n
#         on the fifth day verify that program 1 waters and program 2 does not run \n
#         on the sixth day verify that program 1 waters and program 2 does its calibration cycle \n
#         on the seventh day verify that program 1 waters and program 2 does not run \n
#         on the eighth day verify that program 1 waters and program 2 waters \n
#         on the ninth day verify that program 1 waters and program 2 does not run \n
#         on the tenth day verify that program 1 waters and program 2 does not run \n
#         on the eleventh day verify that program 1 waters and program 2 waters \n
#         on the twelfth day verify that program 1 waters and program 2 does not run \n
#         on the thirteenth day verify that program 1 waters and program 2 waters \n
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             # the test starts on 06/26/2012 at 7:59 1 minute before the start time
#             # changed year from 2012 to 2014
#             self.config.controllers[1].set_date_and_time_on_cn(_date='06/24/2014',_time='7:45:00')
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.done_watering)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.done_watering)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.done_watering)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.done_watering)
#
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_9(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#             # sim_day = "########## Exercise 1 Day 1 Hour 1##########"
#             # print(sim_day)
#             # try:
#             #     # at 8:01 the test starts.  program 1 will start running and program 2 will wait for program 1 to stop
#             #     self.increment_clock(minutes=2)  # advance the clock 2 minutes
#             #     self.verify_program_running(program=1)  # program 1 is running
#             #     self.verify_program_waiting(program=2)  # program 2 is waiting
#             #     #TODO made change
#             #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#
#     #################################
#     def step_10(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#            # sim_day = "########## Exercise 1 Day 1 Hour 5##########"
#            #  print(sim_day)
#            #  try:
#            #      # at 13:01 program 1 finishes running allowing program 2 to start running
#            #      self.increment_clock(hours=5, minutes=30)  # advance the clock 5 hours
#            #      self.verify_program_done(program=1)  # program 1 is done
#            #      self.verify_program_running(program=2)  # program 2 is running
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#
#     #################################
#     def step_11(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#             # try:
#             #     # at 18:01 both programs are done running
#             #     self.increment_clock(hours=6)  # advance the clock 5 hours
#             #     self.verify_program_done(program=1)  # program 1 is done
#             #     self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#
#     #################################
#     def step_12(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         #     sim_day = "########## Exercise 1 Day 2 Hour 1##########"
#         #     print(sim_day)
#         #     try:
#         #         # on 06/27/2012 at 8:01 program 1 will start while program 2 remains off for this day
#         #         self.set_controller_date_and_time(controller_date='06/25/2012', controller_time='7:59:00')  # this is a wednesday
#         #         self.increment_clock(minutes=2)  # advance the clock 2 minutes
#         #         self.verify_program_running(program=1)  # program 1 is running
#         #         self.verify_program_done(program=2)  # program 2 is done
#
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#
#     #################################
#     def step_13(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#             # sim_day = "########## Exercise 1 Day 2 Hour 5##########"
#             # print(sim_day)
#             # try:
#             #     # at 12:01 both programs are done running
#             #     self.increment_clock(hours=5)  # advance the clock 4 hours
#             #     self.verify_program_done(program=1)  # program 1 is done
#             #     self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#
#     #################################
#     def step_15(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#             # sim_day = "########## Exercise 1 Day 3 Hour 1##########"
#             # print(sim_day)
#             # try:
#             #     # on 06/28/2012 at 8:01 program 1 will start running while program 2 remains off for this day
#             #     self.set_controller_date_and_time(controller_date='06/28/2012', controller_time='7:59:00')  # this is a thursday
#             #     self.increment_clock(minutes=2)  # advance the clock 2 minutes
#             #     self.verify_program_running(program=1)  # program 1 is running
#             #     self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#
#     #################################
#     def step_16(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#             # sim_day = "########## Exercise 1 Day 3 Hour 5##########"
#             # print(sim_day)
#             # try:
#             #     # at 12:01 both programs are done running
#             #     self.increment_clock(hours=4)  # advance the clock 4 hours
#             #     self.verify_program_done(program=1)  # program 1 is done
#             #     self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#
#     #################################
#     def step_17(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#             # sim_day = "########## Exercise 1 Day 4 Hour 1##########"
#             # print(sim_day)
#             # try:
#             #     # on 06/29/2012 at 8:01 program 1 will start runnning and program 2 will be waiting for program to stop
#             #     self.set_controller_date_and_time(controller_date='06/29/2012', controller_time='7:59:00')
#             #     self.increment_clock(minutes=2)  # advance the clock 2 minutes
#             #     self.verify_program_running(program=1)  # program 1 is running
#             #     self.verify_program_waiting(program=2)  # program 2 is waiting
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#
#     #################################
#     def step_18(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#             # sim_day = "########## Exercise 1 Day 4 Hour 6##########"
#             # print(sim_day)
#             # try:
#             #     # at 13:01 program 1 will finish running and program 2 will start running
#             #     self.increment_clock(hours=5)  # advance the clock 4 hours
#             #     self.verify_program_done(program=1)  # program 1 is done
#             #     self.verify_program_running(program=2)  # program 2 is running
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#
#     #################################
#     def step_19(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#             # sim_day = "########## Exercise 1 Day 4 Hour 11##########"
#             # print(sim_day)
#             # try:
#             #     # at 18:01 both programs will be done running
#             #     self.increment_clock(hours=5)  # advance the clock 5 hours
#             #     self.verify_program_done(program=1)  # program 1 is done
#             #     self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_20(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#             # sim_day = "########## Exercise 1 Day 5 Hour 1##########"
#             # print(sim_day)
#             # try:
#             #     # on 06/30/2012 at 8:01 program 1 will start running while program 2 remains off for this day
#             #     self.set_controller_date_and_time(controller_date='06/30/2012', controller_time='7:59:00')
#             #     self.increment_clock(minutes=2)  # advance the clock 2 minutes
#             #     self.verify_program_running(program=1)  # program 2 is running
#             #     self.verify_program_done(program=2)  # program 1 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#
#     #################################
#     def step_21(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#
#             # sim_day = "########## Exercise 1 Day 5 Hour 5##########"
#             # print(sim_day)
#             # try:
#             #     # at 12:01 both programs will be done running
#             #     self.increment_clock(hours=4)  # advance the clock 4 hours
#             #     self.verify_program_done(program=1)  # program 1 is done
#             #     self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_22(self):
#         """
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#             # sim_day = "########## Exercise 1 Day 6 Hour 1##########"
#             # print(sim_day)
#             # try:
#             #     # on 07/01/2012 at 8:01 program 1 will start running while program 2 is waiting for program 1 to stop
#             #     self.set_controller_date_and_time(controller_date='07/01/2012', controller_time='7:59:00')
#             #     self.increment_clock(minutes=2)  # advance the clock 2 minutes
#             #     self.verify_program_running(program=1)  # program 1 is running
#             #     self.verify_program_waiting(program=2)  # program 2 is waiting
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#
#     #################################
#     def step_22(self):
#         """
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#             # sim_day = "########## Exercise 1 Day 6 Hour 6##########"
#             # print(sim_day)
#             # try:
#             #     # at 13:01 program 1 will finish running and program 2 will start running
#             #     self.increment_clock(hours=5)  # advance the clock 4 hours
#             #     self.verify_program_done(program=1)  # program 1 is done
#             #     self.verify_program_running(program=2)  # program 2 is running
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#
#     #################################
#     def step_23(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#             # sim_day = "########## Exercise 1 Day 6 Hour 11##########"
#             # print(sim_day)
#             # try:
#             #     # at 18:01 both programs will be finished running
#             #     self.increment_clock(hours=5)  # advance the clock 5 hours
#             #     self.verify_program_done(program=1)  # program 1 is done
#             #     self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#
#     #################################
#     def step_24(self):
#         """
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#             # sim_day = "########## Exercise 1 Day 7 Hour 1##########"
#             # print(sim_day)
#             # try:
#             #     # on 7/2/2012 at 8:01 program 1 will start while program 2 will not run on this day
#             #     self.set_controller_date_and_time(controller_date='07/02/2012', controller_time='7:59:00')
#             #     self.increment_clock(minutes=2)  # advance the clock 2 minutes
#             #     self.verify_program_running(program=1)  # program 1 is running
#             #     self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#
#     #################################
#     def step_25(self):
#         """
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#             # sim_day = "########## Exercise 1 Day 7 Hour 5##########"
#             # print(sim_day)
#             # try:
#             #     # at 12:01 both programs will be finished running
#             #     self.increment_clock(hours=4)  # advance the clock 4 hours
#             #     self.verify_program_done(program=1)  # program 1 is done
#             #     self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#
#     #################################
#     def step_26(self):
#         """
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#             # sim_day = "########## Exercise 1 Day 8 Hour 1##########"
#             # print(sim_day)
#             # try:
#             #     # on 7/3/2012 at 8:01 program 1 will start running while program 2 will be waiting for program 1 to stop
#             #     self.set_controller_date_and_time(controller_date='07/03/2012', controller_time='7:59:00')
#             #     self.increment_clock(minutes=2)  # advance the clock 2 minutes
#             #     self.verify_program_running(program=1)  # program 1 is running
#             #     self.verify_program_waiting(program=2)  # program 2 is running
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#
#     #################################
#     def step_27(self):
#         """
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#             # sim_day = "########## Exercise 1 Day 8 Hour 6##########"
#             # print(sim_day)
#             # try:
#             #     # at 13:01 program 1 will finish running and program 2 will start running
#             #     self.increment_clock(hours=5)  # advance the clock 5 hours
#             #     self.verify_program_done(program=1)  # program 1 is done
#             #     self.verify_program_running(program=2)  # program 2 is running
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#
#     #################################
#     def step_28(self):
#         """
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#             # sim_day = "########## Exercise 1 Day 8 Hour 11##########"
#             # print(sim_day)
#             # try:
#             #     # at 18:01 both programs will be finished running
#             #     self.increment_clock(hours=5)  # advance the clock 5 hours
#             #     self.verify_program_done(program=1)  # program 1 is done
#             #     self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#
#     #################################
#     def step_29(self):
#         """
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#             # sim_day = "########## Exercise 1 Day 9 Hour 1##########"
#             # print(sim_day)
#             # try:
#             #     # on 7/4/2012 at 8:01 program 1 will start while program 2 will not run on this day
#             #     self.set_controller_date_and_time(controller_date='07/04/2012', controller_time='7:59:00')
#             #     self.increment_clock(minutes=2)  # advance the clock 2 minutes
#             #     self.verify_program_running(program=1)  # program 1 is running
#             #     self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#
#     #################################
#     def step_30(self):
#         """
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#             # sim_day = "########## Exercise 1 Day 9 Hour 5##########"
#             # print(sim_day)
#             # try:
#             #     # at 12:01 both programs will be finished running
#             #     self.increment_clock(hours=4)  # advance the clock 4 hours
#             #     self.verify_program_done(program=1)  # program 1 is done
#             #     self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#
#     #################################
#     def step_31(self):
#         """
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#             # sim_day = "########## Exercise 1 Day 10 Hour 1##########"
#             # print(sim_day)
#             # try:
#             #     # on 7/5/2012 at 8:01 program 1 will start while program 2 will not run on this day
#             #     self.set_controller_date_and_time(controller_date='07/05/2012', controller_time='7:59:00')
#             #     self.increment_clock(minutes=2)  # advance the clock 2 minutes
#             #     self.verify_program_running(program=1)  # program 1 is running
#             #     self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#
#     #################################
#     def step_32(self):
#         """
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#           # sim_day = "########## Exercise 1 Day 10 Hour 5##########"
#             # print(sim_day)
#             # try:
#             #     # at 12:01 both programs will be finished running
#             #     self.increment_clock(hours=4)  # advance the clock 4 hours
#             #     self.verify_program_done(program=1)  # program 1 is done
#             #     self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#           except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#
#     #################################
#     def step_33(self):
#         """
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#             # sim_day = "########## Exercise 1 Day 11 Hour 1##########"
#             # print(sim_day)
#             # try:
#             #     # on 7/6/2012 at 8:01 program 1 will start running while program 2 will be waiting for program 1 to stop
#             #     self.set_controller_date_and_time(controller_date='07/06/2012', controller_time='7:59:00')
#             #     self.increment_clock(minutes=2)  # advance the clock 2 minutes
#             #     self.verify_program_running(program=1)  # program 1 is running
#             #     self.verify_program_waiting(program=2)  # program 2 is running
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_8(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             sim_day = "########## Exercise 1 Day 11 Hour 6##########"
#             print(sim_day)
#             try:
#                 # at 13:01 program 1 will finish running and program 2 will start running
#                 self.increment_clock(hours=5)  # advance the clock 5 hours
#                 self.verify_program_done(program=1)  # program 1 is done
#                 self.verify_program_running(program=2)  # program 2 is running
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_8(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             sim_day = "########## Exercise 1 Day 11 Hour 11##########"
#             print(sim_day)
#             try:
#                 # at 18:01 both programs will be finished running
#                 self.increment_clock(hours=5)  # advance the clock 5 hours
#                 self.verify_program_done(program=1)  # program 1 is done
#                 self.verify_program_done(program=2)  # program 2 is done
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#
#             sim_day = "########## Exercise 1 Day 12 Hour 1##########"
#             print(sim_day)
#             try:
#                 # on 7/5/2012 at 8:01 program 1 will start while program 2 will not run on this day
#                 self.set_controller_date_and_time(controller_date='07/07/2012', controller_time='7:59:00')
#                 self.increment_clock(minutes=2)  # advance the clock 2 minutes
#                 self.verify_program_running(program=1)  # program 1 is running
#                 self.verify_program_done(program=2)  # program 2 is done
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_8(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             sim_day = "########## Exercise 1 Day 12 Hour 5##########"
#             print(sim_day)
#             try:
#                 # at 12:01 both programs will be finished running
#                 self.increment_clock(hours=4)  # advance the clock 4 hours
#                 self.verify_program_done(program=1)  # program 1 is done
#                 self.verify_program_done(program=2)  # program 2 is done
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_8(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             sim_day = "########## Exercise 1 Day 13 Hour 1##########"
#             print(sim_day)
#             try:
#                 # on 7/8/2012 at 8:01 program 1 will start running while program 2 will be waiting for program 1 to stop
#                 self.set_controller_date_and_time(controller_date='07/08/2012', controller_time='7:59:00')
#                 self.increment_clock(minutes=2)  # advance the clock 2 minutes
#                 self.verify_program_running(program=1)  # program 1 is running
#                 self.verify_program_waiting(program=2)  # program 2 is running
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_8(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             sim_day = "########## Exercise 1 Day 13 Hour 6##########"
#             print(sim_day)
#             try:
#                 # at 13:01 program 1 will finish running and program 2 will start running
#                 self.increment_clock(hours=5)  # advance the clock 5 hours
#                 self.verify_program_done(program=1)  # program 1 is done
#                 self.verify_program_running(program=2)  # program 2 is running
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_8(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             sim_day = "########## Exercise 1 Day 13 Hour 11##########"
#             print(sim_day)
#             try:
#                 # at 18:01 both programs will be finished running
#                 self.increment_clock(hours=5)  # advance the clock 5 hours
#                 self.verify_program_done(program=1)  # program 1 is done
#                 self.verify_program_done(program=2)  # program 2 is done
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#
#
#     def run_second_exercise(self):
#         """
#         turn sim mode on in the controller \n
#         stop the clock so that it can be incremented manually \n
#         set the controller date and time so that there is a known days of the week and days of the month \n
#         change the program 1 start days to the default historical calendar \n
#         change the moisture sensor setup for program 2 ( change the calibration to single) \n
#         on the first day verify that both programs run \n
#         on the second day verify that neither program runs \n
#         on the third day verify that program 1 runs its calibration cycle and that program 2 does not run \n
#         on the fourth day verify that program 1 does not run and that program 2 runs its calibration cycle \n
#         on the fifth day verify that program 1 runs and program 2 does not \n
#         on the sixth day verify that program 1 does not run and program 2 does run \n
#         on the seventh day verify that program 1 runs and program 2 does not \n
#         on the eighth day verify that program 1 does not run and program 2 does run \n
#         on the ninth day verify that program 1 does run and program 2 does not \n
#         on the tenth day verify that neither program runs \n
#         on the eleventh day verify that program 1 runs and program 2 does a calibration cycle \n
#         on the twelfth day verify that neither program runs \n
#         on the thirteenth day verify that both programs run \n
#         """
#             #################################
#     def step_8(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#         try:
#             sim_day = "########## Exercise 2 start##########"
#             print(sim_day)
#             try:
#                 # set the date to 6/26/2012, time to 7:59
#                 # set the watering schedule for program 1 to the default historical calendar
#                 # set the water strategy for both programs to upper
#                 # change the calibration from program 2 to single
#                 self.set_sim_mode_to_on()
#                 self.stop_clock()
#                 self.send_and_wait_for_reply('SET,PG=1,CI=ET')
#                 self.send_and_wait_for_reply('SET,PZ=1,PG=1,WS=UL,UL=15,CC=SG')
#                 self.send_and_wait_for_reply('SET,PZ=11,PG=2,WS=UL,UL=15,CC=SG')
#                 self.set_moisture_sensor_values(moisture_sensor_serial_number='SB05308', moisture_value=14)
#                 self.set_moisture_sensor_values(moisture_sensor_serial_number='SB07258', moisture_value=14)
#                 self.test_device(device_type='moisture sensor', serial_number='SB05308')
#                 self.test_device(device_type='moisture sensor', serial_number='SB07258')
#                 self.set_controller_date_and_time(controller_date='06/26/2012', controller_time='7:59:00')
#                 self.verify_program_done(program=1)  # program 1 is paused
#                 self.verify_program_done(program=2)  # program 2 is paused
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_8(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             sim_day = "########## Exercise 2 Day 1 Hour 1##########"
#             print(sim_day)
#             try:
#                 # at 8:01 program 1 will start while program 2 will wait for program 1 to stop
#                 self.increment_clock(minutes=2)  # advance the clock 2 minutes
#                 self.verify_program_running(program=1)  # program 1 is running
#                 self.verify_program_waiting(program=2)  # program 2 is waiting
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_8(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             sim_day = "########## Exercise 2 Day 1 Hour 6##########"
#             print(sim_day)
#             try:
#                 # at 13:01 program 1 will stop running and program 2 will start running because program stopped
#                 self.increment_clock(hours=5)  # advance the clock 5 hours
#                 self.verify_program_done(program=1)  # program 1 is done
#                 self.verify_program_running(program=2)  # program 2 is running
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_8(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             sim_day = "########## Exercise 2 Day 1 Hour 11##########"
#             print(sim_day)
#             try:
#                 # at 18:01 both programs will have finished running
#                 self.increment_clock(hours=5)  # advance the clock 5 hours
#                 self.verify_program_done(program=1)  # program 1 is done
#                 self.verify_program_done(program=2)  # program 2 is done
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_8(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             sim_day = "########## Exercise 2 Day 2 Hour 1##########"
#             print(sim_day)
#             try:
#                 # on 6/27/2012 at 8:01 neither program should start
#                 self.set_controller_date_and_time(controller_date='06/27/2012', controller_time='7:59:00')
#                 self.increment_clock(minutes=2)  # advance the clock 2 minutes
#                 self.verify_program_done(program=1)  # program 1 is done
#                 self.verify_program_done(program=2)  # program 1 is done
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_8(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             sim_day = "########## Exercise 2 Day 3 Hour 1##########"
#             print(sim_day)
#             try:
#                 # on 6/28/2012 at 8:01 program 1 will run its calibration cycle while program 2 will not run this day
#                 self.set_controller_date_and_time(controller_date='06/28/2012', controller_time='7:59:00')
#                 self.increment_clock(minutes=2)  # advance the clock 2 minutes
#                 self.verify_program_running(program=1)  # program 1 is running
#                 self.verify_program_done(program=2)  # program 2 is done
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_8(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             sim_day = "########## Exercise 2 Day 3 Hour 8##########"
#             print(sim_day)
#             try:
#                 # at 13:01 program 1 will finish running
#                 self.increment_clock(hours=7)  # advance the clock 5 hours
#                 self.verify_program_done(program=1)  # program 1 is done
#                 self.verify_program_done(program=2)  # program 2 is done
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_8(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             sim_day = "########## Exercise 2 Day 4 Hour 1##########"
#             print(sim_day)
#             try:
#                 # on 6/29/2012 at 8:01 program 2 will run its calibration cycle while program 1 will not run this day
#                 self.set_controller_date_and_time(controller_date='06/29/2012', controller_time='7:59:00')
#                 self.increment_clock(minutes=2)  # advance the clock 2 minutes
#                 self.verify_program_done(program=1)  # program 1 is done
#                 self.verify_program_running(program=2)  # program 2 is running
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_8(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             sim_day = "########## Exercise 2 Day 4 Hour 6##########"
#             print(sim_day)
#             try:
#                 # at 15:01 program 2 will finish running
#                 self.increment_clock(hours=7)  # advance the clock 7 hours
#                 self.verify_program_done(program=1)  # program 1 is done
#                 self.verify_program_done(program=2)  # program 2 is done
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_8(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             sim_day = "########## Exercise 2 Day 5 Hour 1##########"
#             print(sim_day)
#             try:
#                 # on 6/30/2012 at 8:01 program 1 will run while program 2 will not run this day
#                 self.set_controller_date_and_time(controller_date='06/30/2012', controller_time='7:59:00')
#                 self.increment_clock(minutes=2)  # advance the clock 2 minutes
#                 self.verify_program_running(program=1)  # program 1 is running
#                 self.verify_program_done(program=2)  # program 2 is done
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_8(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             sim_day = "########## Exercise 2 Day 5 Hour 9##########"
#             print(sim_day)
#             try:
#                 # at 13:01 program 1 will finish running
#                 self.increment_clock(hours=8)  # advance the clock 5 hours
#                 self.verify_program_done(program=1)  # program 1 is done
#                 self.verify_program_done(program=2)  # program 2 is done
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_8(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             sim_day = "########## Exercise 2 Day 6 Hour 1##########"
#             print(sim_day)
#             try:
#                 # on 7/1/2012 at 8:01 program 2 will run while program 1 will not run this day
#                 self.set_controller_date_and_time(controller_date='07/01/2012', controller_time='7:59:00')
#                 self.increment_clock(minutes=2)  # advance the clock 2 minutes
#                 self.verify_program_done(program=1)  # program 1 is done
#                 self.verify_program_running(program=2)  # program 2 is running
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_8(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             sim_day = "########## Exercise 2 Day 6 Hour 5##########"
#             print(sim_day)
#             try:
#                 # at 15:01 program 2 will finish running
#                 self.increment_clock(hours=7)  # advance the clock 7 hours
#                 self.verify_program_done(program=1)  # program 1 is done
#                 self.verify_program_done(program=2)  # program 2 is done
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_8(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             sim_day = "########## Exercise 2 Day 7 Hour 1##########"
#             print(sim_day)
#             try:
#                 # on 7/2/2012 at 8:01 program 1 will run while program 2 will not run this day
#                 self.set_controller_date_and_time(controller_date='07/02/2012', controller_time='7:59:00')
#                 self.increment_clock(minutes=2)  # advance the clock 2 minutes
#                 self.verify_program_running(program=1)  # program 1 is running
#                 self.verify_program_done(program=2)  # program 2 is done
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_8(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             sim_day = "########## Exercise 2 Day 7 Hour 9##########"
#             print(sim_day)
#             try:
#                 # at 13:01 program 1 will finish running
#                 self.increment_clock(hours=8)  # advance the clock 5 hours
#                 self.verify_program_done(program=1)  # program 1 is done
#                 self.verify_program_done(program=2)  # program 2 is done
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_8(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             sim_day = "########## Exercise 2 Day 8 Hour 1##########"
#             print(sim_day)
#             try:
#                 # on 7/3/2012 at 8:01 program 2 will run while program 1 will not run this day
#                 self.set_controller_date_and_time(controller_date='07/03/2012', controller_time='7:59:00')
#                 self.increment_clock(minutes=2)  # advance the clock 2 minutes
#                 self.verify_program_done(program=1)  # program 1 is done
#                 self.verify_program_running(program=2)  # program 2 is running
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_8(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             sim_day = "########## Exercise 2 Day 8 Hour 5##########"
#             print(sim_day)
#             try:
#                 # at 15:01 program 2 will finish running
#                 self.increment_clock(hours=7)  # advance the clock 7 hours
#                 self.verify_program_done(program=1)  # program 1 is done
#                 self.verify_program_done(program=2)  # program 2 is done
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_8(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             sim_day = "########## Exercise 2 Day 9 Hour 1##########"
#             print(sim_day)
#             try:
#                 # on 7/4/2012 at 8:01 program 1 will run while program 2 will not run this day
#                 self.set_controller_date_and_time(controller_date='07/04/2012', controller_time='7:59:00')
#                 self.increment_clock(minutes=2)  # advance the clock 2 minutes
#                 self.verify_program_running(program=1)  # program 1 is running
#                 self.verify_program_done(program=2)  # program 2 is done
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_8(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             sim_day = "########## Exercise 2 Day 9 Hour 7##########"
#             print(sim_day)
#             try:
#                 # at 13:01 program 1 will finish running
#                 self.increment_clock(hours=6)  # advance the clock 5 hours
#                 self.verify_program_done(program=1)  # program 1 is done
#                 self.verify_program_done(program=2)  # program 2 is done
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_8(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             sim_day = "########## Exercise 2 Day 10 Hour 1##########"
#             print(sim_day)
#             try:
#                 # on 7/5/2012 at 8:01 neither program should start
#                 self.set_controller_date_and_time(controller_date='07/05/2012', controller_time='7:59:00')
#                 self.increment_clock(minutes=2)  # advance the clock 2 minutes
#                 self.verify_program_done(program=1)  # program 1 is done
#                 self.verify_program_done(program=2)  # program 1 is done
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_8(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             sim_day = "########## Exercise 2 Day 11 Hour 1##########"
#             print(sim_day)
#             try:
#                 # on 7/6/2012 at 8:01 program 1 will start while program 2 will wait for program 1 to stop
#                 self.set_controller_date_and_time(controller_date='07/06/2012', controller_time='7:59:00')
#                 self.increment_clock(minutes=2)  # advance the clock 2 minutes
#                 self.verify_program_running(program=1)  # program 1 is running
#                 self.verify_program_waiting(program=2)  # program 2 is waiting
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_8(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             sim_day = "########## Exercise 2 Day 11 Hour 9##########"
#             print(sim_day)
#             try:
#                 # at 13:01 program 1 will stop running and program 2 will start running its calibration cycle
#                 self.increment_clock(hours=7)  # advance the clock 5 hours
#                 self.verify_program_done(program=1)  # program 1 is done
#                 self.verify_program_running(program=2)  # program 2 is running
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_8(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             sim_day = "########## Exercise 2 Day 11 Hour 15##########"
#             print(sim_day)
#             try:
#                 # at 18:01 both programs will have finished running
#                 self.increment_clock(hours=7)  # advance the clock 5 hours
#                 self.verify_program_done(program=1)  # program 1 is done
#                 self.verify_program_done(program=2)  # program 2 is done
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_8(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             sim_day = "########## Exercise 2 Day 12 Hour 1##########"
#             print(sim_day)
#             try:
#                 # on 7/7/2012 at 8:01 neither program should start
#                 self.set_controller_date_and_time(controller_date='07/07/2012', controller_time='7:59:00')
#                 self.increment_clock(minutes=2)  # advance the clock 2 minutes
#                 self.verify_program_done(program=1)  # program 1 is done
#                 self.verify_program_done(program=2)  # program 1 is done
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_8(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             sim_day = "########## Exercise 2 Day 13 Hour 1##########"
#             print(sim_day)
#             try:
#                 # on 7/8/2012 at 8:01 program 1 will start while program 2 will wait for program 1 to stop
#                 self.set_controller_date_and_time(controller_date='07/08/2012', controller_time='7:59:00')
#                 self.increment_clock(minutes=2)  # advance the clock 2 minutes
#                 self.verify_program_running(program=1)  # program 1 is running
#                 self.verify_program_waiting(program=2)  # program 2 is waiting
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_8(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             sim_day = "########## Exercise 2 Day 13 Hour 6##########"
#             print(sim_day)
#             try:
#                 # at 13:01 program 1 will stop running and program 2 will start running because program 1 stopped
#                 self.increment_clock(hours=5)  # advance the clock 5 hours
#                 self.verify_program_done(program=1)  # program 1 is done
#                 self.verify_program_running(program=2)  # program 2 is running
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#     #################################
#     def step_8(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#             sim_day = "########## Exercise 2 Day 13 Hour 11##########"
#             print(sim_day)
#             try:
#                 # at 18:01 both programs will have finished running
#                 self.increment_clock(hours=5)  # advance the clock 5 hours
#                 self.verify_program_done(program=1)  # program 1 is done
#                 self.verify_program_done(program=2)  # program 2 is done
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]
#
#     #################################
#     def step_8(self):
#         """
#
#         do a self test on all devices \n
#         increment the clock by 1 minute \n
#         - Programs 1 : \n
#            - verify program status Done \n
#                 - Zone 1 verify status Done \n
#                 - Zone 2 verify status Done \n
#                 - Zone 3 verify status Done \n
#         - Programs 2 : \n
#            - verify program status Done \n
#                 - Zone 11 verify status Done \n
#                 - Zone 12 verify status Done \n
#                 - Zone 13 verify status Done \n
#         - Programs 3 : \n
#            - verify program status Done \n
#                 - Zone 21 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 23 verify status Done \n
#         - Programs 4 : \n
#            - verify program status Done \n
#                 - Zone 31 verify status Done \n
#                 - Zone 22 verify status Done \n
#                 - Zone 33 verify status Done \n
#
#
#         """
#         method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
#         print method
#         try:
#         # # at 18:01 both programs will have finished running
#         # self.increment_clock(hours=5)  # advance the clock 5 hours
#         # self.verify_program_done(program=1)  # program 1 is done
#         # self.verify_program_done(program=2)  # program 2 is done
#         #TODO need a get data
#             self.config.moisture_sensors[1].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[2].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[3].set_moisture_percent_on_cn(_percent=24.9)
#             self.config.moisture_sensors[4].set_moisture_percent_on_cn(_percent=24.9)
#             for sensor in self.config.ms_ad_range:
#                 self.config.moisture_sensors[sensor].self_test_and_update_object_attributes()
#
#             self.config.controllers[1].do_increment_clock(hours=5)
#
#             self.config.programs[1].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[1].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[2].verify_status_on_cn(_expected_status=opcodes.watering)
#             self.config.zones[3].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[2].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[11].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[12].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[13].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[3].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[21].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[22].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[23].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             self.config.programs[4].verify_status_on_cn(_expected_status=opcodes.running)
#             self.config.zones[31].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[32].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#             self.config.zones[33].verify_status_on_cn(_expected_status=opcodes.waiting_to_water)
#
#             for zone in self.config.zn_ad_range:
#                 self.config.zones[zone].verify_status_on_cn(_expected_status=opcodes.done_watering)
#
#         except Exception as e:
#             e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
#                     "\tThe Controller Date and time was currently set to {2}\n" \
#                     "\tThe Exception thrown was {3}".format(
#                         self.config.test_name,
#                         sys._getframe().f_code.co_name,
#                         date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
#                         str(e.message))
#             raise Exception, Exception(e_msg), sys.exc_info()[2]