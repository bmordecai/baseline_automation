import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr

# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes

# import log_handler for logging functionality
from common.logging_handler import log_handler

# this import allows us to directly use the date_mngr
from datetime import time
from common import helper_methods

__author__ = 'Eldin'


class ControllerUseCase48(object):
    """
    Test name:
        - CN UseCase 48 Update from BaseManager
    Purpose:
        - Put the controller's firmware into v12
        - Make sure that the update can happen from the BaseManager

    We also reset the state of the controller to the latest version of firmware (using a BaseManager update). This is
    not to test that functionality specifically but more so that we can run use cases after this one on the newest
    firmware version.
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """

        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """

        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)
                    # we have to set a long timeout on the serial port because the test runs for long periods of time
                    self.config.BaseStation3200[1].set_serial_port_timeout(timeout=5040)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        Check if the controller is operating on v16 firmware, if it is, do a firmware update from the local directory
        to 12.34 firmware. If it isn't, do nothing.
        """
        self.config.BaseStation3200[1].do_firmware_update(were_from=opcodes.local_directory,
                                                          directory='common' + os.sep + 'firmware_update_files',
                                                          file_name='v12.34_special')

    #################################
    def step_2(self):
        """
        We do a firmware update from BaseManager to v16 so that the controller will be in a good state for the next
        potential use cases that could run. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # self.config.BaseStation3200[1].basemanager_connection[1].connect_v12_cn_to_ethernet()
            print("You must connect the 3200 back to P2 using alternate IP address after upgrade!")
            self.config.BaseStation3200[1].basemanager_connection[1].verify_use_alternate_ip()
            self.config.BaseStation3200[1].basemanager_connection[1].verify_bm_ip_address_on_cn()
            self.config.BaseStation3200[1].basemanager_connection[1].wait_for_bm_connection()

            # required before requesting firmware update!
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Upgrades the firmware to v16, bm id number must be 160, this folder contains no help files
            self.config.BaseStation3200[1].do_firmware_update(were_from=opcodes.basemanager,
                                                              bm_id_number=opcodes.latest_3200_basemanager_firmware_id)

            self.config.BaseStation3200[1].set_sim_mode_to_on()
            self.config.BaseStation3200[1].stop_clock()
            self.config.BaseStation3200[1].verify_code_version()
            # set the date time of the message to match the controller

            date_mngr.set_current_date_to_match_computer()
            self.config.BaseStation3200[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                             _time=date_mngr.curr_day.time_string_for_controller())
            self.config.BaseStation3200[1].messages.verify_restore_successful_from_internal_flash_message()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]