import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr

# this import allows us to directly use the date_mngr
from datetime import time

__author__ = 'Turu'


class ControllerUseCase8(object):
    """
    Test name:
        - Multiple Decoders Having Start Stop Pause Conditions on Multiple Programs \n
    Purpose:
        - confirming that one event/moisture/temp decoder can have a start/stop/pause condition on multiple Programs \n
        - one SSP condition can:
            - stop multiple Programs at once
            - start a program
    Coverage area: \n
        This test covers having decoders set multiple start stop pause conditions \n
        -setting up devices:
            - Loading \n
            - Searching \n
            - Addressing \n
                - Setting:
                    - descriptions \n
                    - locations \n
        - setting Programs: \n
            - start times \n
            - mainlines \n

        - attaching zones to program \n

        - setting up mainline: \n
            set main line \n
                - limit zones by flow \n
                - pipe fill time\n
                - target flow\n
                - high variance limit
                - high variance shut down \n
                - low variance limit
                - low variance shut down \n

        - setting up poc's: \n
            - enable POC \n
            - assign:
                - master valve
                - flow meter \n
                - target flow\n
                - main line \n

        - turning off multiple Programs at once
        - trigger a SSP condition on a device that then starts, stops, and pauses different Programs \n
        - verifying linked zones work with every SSP condition \n
    Not Covered: \n
        - Two Pause conditions from the same device type on on program \n
            - I thought this wouldn't be necessary because we already do this case for start and stop and if we were
              to do it for pause it may throw off our test patter \n
    Pattern:
        - This test can seem complicated at first with all the SSP conditions, however we tried to incorporate a pattern
          to make it simpler \n
        - The pattern for the first conditions (PT=Start condition, PP=Stop condition, PS=Pause condition):
            PG:  1  | 96 | 97 | 98 | 99
            MS1: PT             PP   PS
            MS2: PS   PT             PP
            MS3: PP   PS   PT
            MS4:      PP   PS   PT
            MS5:           PP   PS   PT
            TS1: PT             PP   PS
            TS2: PS   PT             PP
            TS3: PP   PS   PT
            TS4:      PP   PS   PT
            TS5:           PP   PS   PT
            SW1: PT             PP   PS
            SW2: PS   PT             PP
            SW3: PP   PS   PT
            SW4:      PP   PS   PT
            SW5:           PP   PS   PT
            - This pattern has each device start a program, pause a different program, and stop a different program \n
            - This is done so we know each program has a start, stop, and pause condition from each device type \n
        - The pattern for the second conditions (PT=Start condition, PP=Stop condition, PS=Pause condition):
            PG: 1-10 | 96 | 97 | 98 | 99
            MS4: PT                   PP
            MS5: PP    PT
            MS1:       PP   PT
            MS2:            PP   PT
            MS3:                 PP   PT
            TS4: PT                   PP
            TS5: PP    PT
            TS1:       PP   PT
            TS2:            PP   PT
            TS3:                 PP   PT
            # These have been left out for now
            # SW4: PT                   PP
            # SW5: PP    PT
            # SW1:       PP   PT
            # SW2:            PP   PT
            # SW3:                 PP   PT
            - This pattern sets two more of the same device type to have a start and stop condition on each program \n
            - This is done so we know all Programs can have the same condition from the same device type (but not the
              same actual device) \n
    """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    self.config.BaseStation3200[1].set_serial_port_timeout(timeout=1200)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                     # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            # set controller to run 5 zones at a time
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=5)
            program_2am_start_time = [120]

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=5)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_2am_start_time)

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=96)
            self.config.BaseStation3200[1].programs[96].set_enabled()
            self.config.BaseStation3200[1].programs[96].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation3200[1].programs[96].set_start_times(_st_list=program_2am_start_time)

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=97)
            self.config.BaseStation3200[1].programs[97].set_enabled()
            self.config.BaseStation3200[1].programs[97].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation3200[1].programs[97].set_start_times(_st_list=program_2am_start_time)

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=98)
            self.config.BaseStation3200[1].programs[98].set_enabled()
            self.config.BaseStation3200[1].programs[98].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation3200[1].programs[98].set_start_times(_st_list=program_2am_start_time)

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=99)
            self.config.BaseStation3200[1].programs[99].set_enabled()
            self.config.BaseStation3200[1].programs[99].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation3200[1].programs[99].set_start_times(_st_list=program_2am_start_time)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        setup start/stop/pause conditions: \n
            - make Programs 1
                - start by (MS[1], TS[1], SW[1]), (MS[4], TS[4], SW[4])
                - pause by (MS[2], TS[2], SW[2]),
                - stop by (MS[3], TS[3], SW[3]), (MS[5], TS[5], SW[5])
            - make program 96
                - start by (MS[2], TS[2], SW[2]), (MS[5], TS[5], SW[5])
                - pause by (MS[3], TS[3], SW[3]),
                - stop by (MS[4], TS[4], SW[4]), (MS[1], TS[1], SW[1])
            - make program 97
                - start by (MS[3], TS[3], SW[3]), (MS[1], TS[1], SW[1])
                - pause by (MS[4], TS[4], SW[4]),
                - stop by (MS[5], TS[5], SW[5]), (MS[2], TS[2], SW[2])
            - make program 98
                - start by (MS[4], TS[4], SW[4]), (MS[2], TS[2], SW[2])
                - pause by (MS[5], TS[5], SW[5]),
                - stop by (MS[1], TS[1], SW[1]), (MS[3], TS[3], SW[3])
            - make program 99
                - start by (MS[5], TS[5], SW[5]), (MS[3], TS[3], SW[3])
                - pause by (MS[1], TS[1], SW[1]),
                - stop by (MS[2], TS[2], SW[2]), (MS[4], TS[4], SW[4])
        """
        # this is set in the PG3200 object
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # First 15 start conditions, 1 for each of the 5 moisture sensors, temperature sensors, and event switches

            # ----------------------------------------------------------------------------------------------------------
            # MOISTURE START CONDITIONS GROUP 1
            # ----------------------------------------------------------------------------------------------------------

            self.config.BaseStation3200[1].programs[1].add_moisture_start_condition(_moisture_sensor_address=1)
            self.config.BaseStation3200[1].programs[1].moisture_start_conditions[1].set_moisture_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[1].moisture_start_conditions[1].set_moisture_threshold(_percent=26)

            self.config.BaseStation3200[1].programs[96].add_moisture_start_condition(_moisture_sensor_address=2)
            self.config.BaseStation3200[1].programs[96].moisture_start_conditions[2].set_moisture_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[96].moisture_start_conditions[2]\
                .set_moisture_threshold(_percent=26.0)

            self.config.BaseStation3200[1].programs[97].add_moisture_start_condition(_moisture_sensor_address=3)
            self.config.BaseStation3200[1].programs[97].moisture_start_conditions[3].set_moisture_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[97].moisture_start_conditions[3].set_moisture_threshold(_percent=26)

            self.config.BaseStation3200[1].programs[98].add_moisture_start_condition(_moisture_sensor_address=4)
            self.config.BaseStation3200[1].programs[98].moisture_start_conditions[4].set_moisture_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[98].moisture_start_conditions[4].set_moisture_threshold(_percent=26)

            self.config.BaseStation3200[1].programs[99].add_moisture_start_condition(_moisture_sensor_address=5)
            self.config.BaseStation3200[1].programs[99].moisture_start_conditions[5].set_moisture_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[99].moisture_start_conditions[5].set_moisture_threshold(_percent=26)

            # ----------------------------------------------------------------------------------------------------------
            # TEMPERATURE START CONDITIONS GROUP 1
            # ----------------------------------------------------------------------------------------------------------

            self.config.BaseStation3200[1].programs[1].add_temperature_start_condition(_temperature_sensor_address=1)
            self.config.BaseStation3200[1].programs[1].temperature_start_conditions[1]\
                .set_temperature_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[1].temperature_start_conditions[1]\
                .set_temperature_threshold(_degrees=98)

            self.config.BaseStation3200[1].programs[96].add_temperature_start_condition(_temperature_sensor_address=2)
            self.config.BaseStation3200[1].programs[96].temperature_start_conditions[2]\
                .set_temperature_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[96].temperature_start_conditions[2]\
                .set_temperature_threshold(_degrees=98)

            self.config.BaseStation3200[1].programs[97].add_temperature_start_condition(_temperature_sensor_address=3)
            self.config.BaseStation3200[1].programs[97].temperature_start_conditions[3]\
                .set_temperature_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[97].temperature_start_conditions[3]\
                .set_temperature_threshold(_degrees=98)

            self.config.BaseStation3200[1].programs[98].add_temperature_start_condition(_temperature_sensor_address=4)
            self.config.BaseStation3200[1].programs[98].temperature_start_conditions[4]\
                .set_temperature_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[98].temperature_start_conditions[4]\
                .set_temperature_threshold(_degrees=98)

            self.config.BaseStation3200[1].programs[99].add_temperature_start_condition(_temperature_sensor_address=5)
            self.config.BaseStation3200[1].programs[99].temperature_start_conditions[5]\
                .set_temperature_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[99].temperature_start_conditions[5]\
                .set_temperature_threshold(_degrees=98)

            # ----------------------------------------------------------------------------------------------------------
            # SWITCH START CONDITIONS GROUP 1
            # ----------------------------------------------------------------------------------------------------------

            self.config.BaseStation3200[1].programs[1].add_switch_start_condition(_event_switch_address=1)
            self.config.BaseStation3200[1].programs[1].event_switch_start_conditions[1].set_switch_mode_to_open()

            self.config.BaseStation3200[1].programs[96].add_switch_start_condition(_event_switch_address=2)
            self.config.BaseStation3200[1].programs[96].event_switch_start_conditions[2].set_switch_mode_to_open()

            self.config.BaseStation3200[1].programs[97].add_switch_start_condition(_event_switch_address=3)
            self.config.BaseStation3200[1].programs[97].event_switch_start_conditions[3].set_switch_mode_to_open()

            self.config.BaseStation3200[1].programs[98].add_switch_start_condition(_event_switch_address=4)
            self.config.BaseStation3200[1].programs[98].event_switch_start_conditions[4].set_switch_mode_to_open()

            self.config.BaseStation3200[1].programs[99].add_switch_start_condition(_event_switch_address=5)
            self.config.BaseStation3200[1].programs[99].event_switch_start_conditions[5].set_switch_mode_to_open()

            # First 15 pause conditions, 1 for each of the 5 moisture sensors, temperature sensors, and event switches

            # ----------------------------------------------------------------------------------------------------------
            # MOISTURE PAUSE CONDITIONS GROUP 1
            # ----------------------------------------------------------------------------------------------------------

            self.config.BaseStation3200[1].programs[1].add_moisture_pause_condition(_moisture_sensor_address=2)
            self.config.BaseStation3200[1].programs[1].moisture_pause_conditions[2].set_moisture_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[1].moisture_pause_conditions[2].set_moisture_threshold(_percent=26)
            self.config.BaseStation3200[1].programs[1].moisture_pause_conditions[2].set_moisture_pause_time(_minutes=2)

            self.config.BaseStation3200[1].programs[96].add_moisture_pause_condition(_moisture_sensor_address=3)
            self.config.BaseStation3200[1].programs[96].moisture_pause_conditions[3].set_moisture_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[96].moisture_pause_conditions[3].set_moisture_threshold(_percent=26)
            self.config.BaseStation3200[1].programs[96].moisture_pause_conditions[3].set_moisture_pause_time(_minutes=2)

            self.config.BaseStation3200[1].programs[97].add_moisture_pause_condition(_moisture_sensor_address=4)
            self.config.BaseStation3200[1].programs[97].moisture_pause_conditions[4].set_moisture_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[97].moisture_pause_conditions[4].set_moisture_threshold(_percent=26)
            self.config.BaseStation3200[1].programs[97].moisture_pause_conditions[4].set_moisture_pause_time(_minutes=2)

            self.config.BaseStation3200[1].programs[98].add_moisture_pause_condition(_moisture_sensor_address=5)
            self.config.BaseStation3200[1].programs[98].moisture_pause_conditions[5].set_moisture_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[98].moisture_pause_conditions[5].set_moisture_threshold(_percent=26)
            self.config.BaseStation3200[1].programs[98].moisture_pause_conditions[5].set_moisture_pause_time(_minutes=2)

            self.config.BaseStation3200[1].programs[99].add_moisture_pause_condition(_moisture_sensor_address=1)
            self.config.BaseStation3200[1].programs[99].moisture_pause_conditions[1].set_moisture_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[99].moisture_pause_conditions[1].set_moisture_threshold(_percent=26)
            self.config.BaseStation3200[1].programs[99].moisture_pause_conditions[1].set_moisture_pause_time(_minutes=2)

            # ----------------------------------------------------------------------------------------------------------
            # TEMPERATURE PAUSE CONDITIONS GROUP 1
            # ----------------------------------------------------------------------------------------------------------

            self.config.BaseStation3200[1].programs[1].add_temperature_pause_condition(_temperature_sensor_address=2)
            self.config.BaseStation3200[1].programs[1].temperature_pause_conditions[2]\
                .set_temperature_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[1].temperature_pause_conditions[2]\
                .set_temperature_threshold(_degrees=98)
            self.config.BaseStation3200[1].programs[1].temperature_pause_conditions[2]\
                .set_temperature_pause_time(_minutes=2)

            self.config.BaseStation3200[1].programs[96].add_temperature_pause_condition(_temperature_sensor_address=3)
            self.config.BaseStation3200[1].programs[96].temperature_pause_conditions[3]\
                .set_temperature_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[96].temperature_pause_conditions[3]\
                .set_temperature_threshold(_degrees=98)
            self.config.BaseStation3200[1].programs[96].temperature_pause_conditions[3]\
                .set_temperature_pause_time(_minutes=2)

            self.config.BaseStation3200[1].programs[97].add_temperature_pause_condition(_temperature_sensor_address=4)
            self.config.BaseStation3200[1].programs[97].temperature_pause_conditions[4]\
                .set_temperature_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[97].temperature_pause_conditions[4]\
                .set_temperature_threshold(_degrees=98)
            self.config.BaseStation3200[1].programs[97].temperature_pause_conditions[4]\
                .set_temperature_pause_time(_minutes=2)

            self.config.BaseStation3200[1].programs[98].add_temperature_pause_condition(_temperature_sensor_address=5)
            self.config.BaseStation3200[1].programs[98].temperature_pause_conditions[5]\
                .set_temperature_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[98].temperature_pause_conditions[5]\
                .set_temperature_threshold(_degrees=98)
            self.config.BaseStation3200[1].programs[98].temperature_pause_conditions[5]\
                .set_temperature_pause_time(_minutes=2)

            self.config.BaseStation3200[1].programs[99].add_temperature_pause_condition(_temperature_sensor_address=1)
            self.config.BaseStation3200[1].programs[99].temperature_pause_conditions[1]\
                .set_temperature_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[99].temperature_pause_conditions[1]\
                .set_temperature_threshold(_degrees=98)
            self.config.BaseStation3200[1].programs[99].temperature_pause_conditions[1]\
                .set_temperature_pause_time(_minutes=2)
            # ----------------------------------------------------------------------------------------------------------
            # SWITCH PAUSE CONDITIONS GROUP 1
            # ----------------------------------------------------------------------------------------------------------

            self.config.BaseStation3200[1].programs[1].add_switch_pause_condition(_event_switch_address=2)
            self.config.BaseStation3200[1].programs[1].event_switch_pause_conditions[2].set_switch_mode_to_open()
            self.config.BaseStation3200[1].programs[1].event_switch_pause_conditions[2].set_switch_pause_time(_minutes=2)

            self.config.BaseStation3200[1].programs[96].add_switch_pause_condition(_event_switch_address=3)
            self.config.BaseStation3200[1].programs[96].event_switch_pause_conditions[3].set_switch_mode_to_open()
            self.config.BaseStation3200[1].programs[96].event_switch_pause_conditions[3].set_switch_pause_time(_minutes=2)

            self.config.BaseStation3200[1].programs[97].add_switch_pause_condition(_event_switch_address=4)
            self.config.BaseStation3200[1].programs[97].event_switch_pause_conditions[4].set_switch_mode_to_open()
            self.config.BaseStation3200[1].programs[97].event_switch_pause_conditions[4].set_switch_pause_time(_minutes=2)

            self.config.BaseStation3200[1].programs[98].add_switch_pause_condition(_event_switch_address=5)
            self.config.BaseStation3200[1].programs[98].event_switch_pause_conditions[5].set_switch_mode_to_open()
            self.config.BaseStation3200[1].programs[98].event_switch_pause_conditions[5].set_switch_pause_time(_minutes=2)

            self.config.BaseStation3200[1].programs[99].add_switch_pause_condition(_event_switch_address=1)
            self.config.BaseStation3200[1].programs[99].event_switch_pause_conditions[1].set_switch_mode_to_open()
            self.config.BaseStation3200[1].programs[99].event_switch_pause_conditions[1].set_switch_pause_time(_minutes=2)

            # First 15 stop conditions, 1 for each of the 5 moisture sensors, temperature sensors, and event switches

            # ----------------------------------------------------------------------------------------------------------
            # MOISTURE STOP CONDITIONS GROUP 1
            # ----------------------------------------------------------------------------------------------------------

            self.config.BaseStation3200[1].programs[1].add_moisture_stop_condition(_moisture_sensor_address=3)
            self.config.BaseStation3200[1].programs[1].moisture_stop_conditions[3].set_moisture_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[1].moisture_stop_conditions[3].set_moisture_threshold(_percent=26)

            self.config.BaseStation3200[1].programs[96].add_moisture_stop_condition(_moisture_sensor_address=4)
            self.config.BaseStation3200[1].programs[96].moisture_stop_conditions[4].set_moisture_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[96].moisture_stop_conditions[4].set_moisture_threshold(_percent=26)

            self.config.BaseStation3200[1].programs[97].add_moisture_stop_condition(_moisture_sensor_address=5)
            self.config.BaseStation3200[1].programs[97].moisture_stop_conditions[5].set_moisture_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[97].moisture_stop_conditions[5].set_moisture_threshold(_percent=26)

            self.config.BaseStation3200[1].programs[98].add_moisture_stop_condition(_moisture_sensor_address=1)
            self.config.BaseStation3200[1].programs[98].moisture_stop_conditions[1].set_moisture_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[98].moisture_stop_conditions[1].set_moisture_threshold(_percent=26)

            self.config.BaseStation3200[1].programs[99].add_moisture_stop_condition(_moisture_sensor_address=2)
            self.config.BaseStation3200[1].programs[99].moisture_stop_conditions[2].set_moisture_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[99].moisture_stop_conditions[2].set_moisture_threshold(_percent=26)

            # ----------------------------------------------------------------------------------------------------------
            # TEMPERATURE STOP CONDITIONS GROUP 1
            # ----------------------------------------------------------------------------------------------------------

            self.config.BaseStation3200[1].programs[1].add_temperature_stop_condition(_temperature_sensor_address=3)
            self.config.BaseStation3200[1].programs[1].temperature_stop_conditions[3]\
                .set_temperature_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[1].temperature_stop_conditions[3]\
                .set_temperature_threshold(_degrees=98)

            self.config.BaseStation3200[1].programs[96].add_temperature_stop_condition(_temperature_sensor_address=4)
            self.config.BaseStation3200[1].programs[96].temperature_stop_conditions[4]\
                .set_temperature_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[96].temperature_stop_conditions[4]\
                .set_temperature_threshold(_degrees=98)
            
            self.config.BaseStation3200[1].programs[97].add_temperature_stop_condition(_temperature_sensor_address=5)
            self.config.BaseStation3200[1].programs[97].temperature_stop_conditions[5]\
                .set_temperature_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[97].temperature_stop_conditions[5]\
                .set_temperature_threshold(_degrees=98)

            self.config.BaseStation3200[1].programs[98].add_temperature_stop_condition(_temperature_sensor_address=1)
            self.config.BaseStation3200[1].programs[98].temperature_stop_conditions[1]\
                .set_temperature_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[98].temperature_stop_conditions[1]\
                .set_temperature_threshold(_degrees=98)

            self.config.BaseStation3200[1].programs[99].add_temperature_stop_condition(_temperature_sensor_address=2)
            self.config.BaseStation3200[1].programs[99].temperature_stop_conditions[2]\
                .set_temperature_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[99].temperature_stop_conditions[2]\
                .set_temperature_threshold(_degrees=98)

            # ----------------------------------------------------------------------------------------------------------
            # SWITCH STOP CONDITIONS GROUP 1
            # ----------------------------------------------------------------------------------------------------------

            self.config.BaseStation3200[1].programs[1].add_switch_stop_condition(_event_switch_address=3)
            self.config.BaseStation3200[1].programs[1].event_switch_stop_conditions[3].set_switch_mode_to_open()

            self.config.BaseStation3200[1].programs[96].add_switch_stop_condition(_event_switch_address=4)
            self.config.BaseStation3200[1].programs[96].event_switch_stop_conditions[4].set_switch_mode_to_open()

            self.config.BaseStation3200[1].programs[97].add_switch_stop_condition(_event_switch_address=5)
            self.config.BaseStation3200[1].programs[97].event_switch_stop_conditions[5].set_switch_mode_to_open()

            self.config.BaseStation3200[1].programs[98].add_switch_stop_condition(_event_switch_address=1)
            self.config.BaseStation3200[1].programs[98].event_switch_stop_conditions[1].set_switch_mode_to_open()

            self.config.BaseStation3200[1].programs[99].add_switch_stop_condition(_event_switch_address=2)
            self.config.BaseStation3200[1].programs[99].event_switch_stop_conditions[2].set_switch_mode_to_open()

            # Second 15 start conditions, another 1 for each of the 5 moisture sensors, temperature sensors, and event switches

            # ----------------------------------------------------------------------------------------------------------
            # MOISTURE START CONDITIONS GROUP 2
            # ----------------------------------------------------------------------------------------------------------

            self.config.BaseStation3200[1].programs[1].add_moisture_start_condition(_moisture_sensor_address=4)
            self.config.BaseStation3200[1].programs[1].moisture_start_conditions[4].set_moisture_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[1].moisture_start_conditions[4].set_moisture_threshold(_percent=12)

            self.config.BaseStation3200[1].programs[96].add_moisture_start_condition(_moisture_sensor_address=5)
            self.config.BaseStation3200[1].programs[96].moisture_start_conditions[5].set_moisture_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[96].moisture_start_conditions[5].set_moisture_threshold(_percent=12)

            self.config.BaseStation3200[1].programs[97].add_moisture_start_condition(_moisture_sensor_address=1)
            self.config.BaseStation3200[1].programs[97].moisture_start_conditions[1].set_moisture_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[97].moisture_start_conditions[1].set_moisture_threshold(_percent=12)

            self.config.BaseStation3200[1].programs[98].add_moisture_start_condition(_moisture_sensor_address=2)
            self.config.BaseStation3200[1].programs[98].moisture_start_conditions[2].set_moisture_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[98].moisture_start_conditions[2].set_moisture_threshold(_percent=12)

            self.config.BaseStation3200[1].programs[99].add_moisture_start_condition(_moisture_sensor_address=3)
            self.config.BaseStation3200[1].programs[99].moisture_start_conditions[3].set_moisture_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[99].moisture_start_conditions[3].set_moisture_threshold(_percent=12)

            # ----------------------------------------------------------------------------------------------------------
            # TEMPERATURE START CONDITIONS GROUP 2
            # ----------------------------------------------------------------------------------------------------------

            self.config.BaseStation3200[1].programs[1].add_temperature_start_condition(_temperature_sensor_address=4)
            self.config.BaseStation3200[1].programs[1].temperature_start_conditions[4]\
                .set_temperature_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[1].temperature_start_conditions[4]\
                .set_temperature_threshold(_degrees=36)

            self.config.BaseStation3200[1].programs[96].add_temperature_start_condition(_temperature_sensor_address=5)
            self.config.BaseStation3200[1].programs[96].temperature_start_conditions[5]\
                .set_temperature_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[96].temperature_start_conditions[5]\
                .set_temperature_threshold(_degrees=36)

            self.config.BaseStation3200[1].programs[97].add_temperature_start_condition(_temperature_sensor_address=1)
            self.config.BaseStation3200[1].programs[97].temperature_start_conditions[1]\
                .set_temperature_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[97].temperature_start_conditions[1]\
                .set_temperature_threshold(_degrees=36)

            self.config.BaseStation3200[1].programs[98].add_temperature_start_condition(_temperature_sensor_address=2)
            self.config.BaseStation3200[1].programs[98].temperature_start_conditions[2]\
                .set_temperature_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[98].temperature_start_conditions[2]\
                .set_temperature_threshold(_degrees=36)

            self.config.BaseStation3200[1].programs[99].add_temperature_start_condition(_temperature_sensor_address=3)
            self.config.BaseStation3200[1].programs[99].temperature_start_conditions[3]\
                .set_temperature_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[99].temperature_start_conditions[3]\
                .set_temperature_threshold(_degrees=36)

            # ----------------------------------------------------------------------------------------------------------
            # SWITCH START CONDITIONS GROUP 2
            # ----------------------------------------------------------------------------------------------------------

            # TODO: Why are event switch start conditions group 2 commented out?

            # self.config.BaseStation3200[1].program_start_conditions[26] = StartConditionFor3200(program_ad=1)
            # self.config.BaseStation3200[1].program_start_conditions[26].set_event_switch_condition_on_pg(
            #     serial_number=self.config.BaseStation3200[1].event_switches[4].sn, mode=opcodes.closed)
            # self.config.BaseStation3200[1].programs[1].add_switch_start_condition(_event_switch_address=4)
            # self.config.BaseStation3200[1].programs[1].event_switch_start_conditions[4].set_switch_mode_to_closed()

            # self.config.BaseStation3200[1].program_start_conditions[27] = StartConditionFor3200(program_ad=96)
            # self.config.BaseStation3200[1].program_start_conditions[27].set_event_switch_condition_on_pg(
            #     serial_number=self.config.BaseStation3200[1].event_switches[5].sn, mode=opcodes.closed)
            # self.config.BaseStation3200[1].programs[96].add_switch_start_condition(_event_switch_address=5)
            # self.config.BaseStation3200[1].programs[96].event_switch_start_conditions[5].set_switch_mode_to_closed()

            # self.config.BaseStation3200[1].program_start_conditions[28] = StartConditionFor3200(program_ad=97)
            # self.config.BaseStation3200[1].program_start_conditions[28].set_event_switch_condition_on_pg(
            #     serial_number=self.config.BaseStation3200[1].event_switches[1].sn, mode=opcodes.closed)
            # self.config.BaseStation3200[1].programs[97].add_switch_start_condition(_event_switch_address=1)
            # self.config.BaseStation3200[1].programs[97].event_switch_start_conditions[1].set_switch_mode_to_closed()

            # self.config.BaseStation3200[1].program_start_conditions[29] = StartConditionFor3200(program_ad=98)
            # self.config.BaseStation3200[1].program_start_conditions[29].set_event_switch_condition_on_pg(
            #     serial_number=self.config.BaseStation3200[1].event_switches[2].sn, mode=opcodes.closed)
            # self.config.BaseStation3200[1].programs[98].add_switch_start_condition(_event_switch_address=2)
            # self.config.BaseStation3200[1].programs[98].event_switch_start_conditions[2].set_switch_mode_to_closed()

            # self.config.BaseStation3200[1].program_start_conditions[30] = StartConditionFor3200(program_ad=99)
            # self.config.BaseStation3200[1].program_start_conditions[30].set_event_switch_condition_on_pg(
            #     serial_number=self.config.BaseStation3200[1].event_switches[3].sn, mode=opcodes.closed)
            # self.config.BaseStation3200[1].programs[99].add_switch_start_condition(_event_switch_address=3)
            # self.config.BaseStation3200[1].programs[99].event_switch_start_conditions[3].set_switch_mode_to_closed()

            # Second 15 stop conditions, 1 for each of the 5 moisture sensors, temperature sensors, and event switches

            # ----------------------------------------------------------------------------------------------------------
            # MOISTURE STOP CONDITIONS GROUP 2
            # ----------------------------------------------------------------------------------------------------------

            self.config.BaseStation3200[1].programs[1].add_moisture_stop_condition(_moisture_sensor_address=5)
            self.config.BaseStation3200[1].programs[1].moisture_stop_conditions[5].set_moisture_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[1].moisture_stop_conditions[5].set_moisture_threshold(_percent=12)

            self.config.BaseStation3200[1].programs[96].add_moisture_stop_condition(_moisture_sensor_address=1)
            self.config.BaseStation3200[1].programs[96].moisture_stop_conditions[1].set_moisture_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[96].moisture_stop_conditions[1].set_moisture_threshold(_percent=12)

            self.config.BaseStation3200[1].programs[97].add_moisture_stop_condition(_moisture_sensor_address=2)
            self.config.BaseStation3200[1].programs[97].moisture_stop_conditions[2].set_moisture_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[97].moisture_stop_conditions[2].set_moisture_threshold(_percent=12)

            self.config.BaseStation3200[1].programs[98].add_moisture_stop_condition(_moisture_sensor_address=3)
            self.config.BaseStation3200[1].programs[98].moisture_stop_conditions[3].set_moisture_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[98].moisture_stop_conditions[3].set_moisture_threshold(_percent=12)

            self.config.BaseStation3200[1].programs[99].add_moisture_stop_condition(_moisture_sensor_address=4)
            self.config.BaseStation3200[1].programs[99].moisture_stop_conditions[4].set_moisture_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[99].moisture_stop_conditions[4].set_moisture_threshold(_percent=12)

            # ----------------------------------------------------------------------------------------------------------
            # TEMPERATURE STOP CONDITIONS GROUP 2
            # ----------------------------------------------------------------------------------------------------------

            self.config.BaseStation3200[1].programs[1].add_temperature_stop_condition(_temperature_sensor_address=5)
            self.config.BaseStation3200[1].programs[1].temperature_stop_conditions[5]\
                .set_temperature_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[1].temperature_stop_conditions[5]\
                .set_temperature_threshold(_degrees=36)

            self.config.BaseStation3200[1].programs[96].add_temperature_stop_condition(_temperature_sensor_address=1)
            self.config.BaseStation3200[1].programs[96].temperature_stop_conditions[1]\
                .set_temperature_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[96].temperature_stop_conditions[1]\
                .set_temperature_threshold(_degrees=36)

            self.config.BaseStation3200[1].programs[97].add_temperature_stop_condition(_temperature_sensor_address=2)
            self.config.BaseStation3200[1].programs[97].temperature_stop_conditions[2]\
                .set_temperature_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[97].temperature_stop_conditions[2]\
                .set_temperature_threshold(_degrees=36)

            self.config.BaseStation3200[1].programs[98].add_temperature_stop_condition(_temperature_sensor_address=3)
            self.config.BaseStation3200[1].programs[98].temperature_stop_conditions[3]\
                .set_temperature_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[98].temperature_stop_conditions[3]\
                .set_temperature_threshold(_degrees=36)

            self.config.BaseStation3200[1].programs[99].add_temperature_stop_condition(_temperature_sensor_address=4)
            self.config.BaseStation3200[1].programs[99].temperature_stop_conditions[4]\
                .set_temperature_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[99].temperature_stop_conditions[4]\
                .set_temperature_threshold(_degrees=36)

            # ----------------------------------------------------------------------------------------------------------
            # SWITCH STOP CONDITIONS GROUP 2
            # ----------------------------------------------------------------------------------------------------------

            # TODO: Why are event switch stop conditions group 2 commented out?

            # self.config.BaseStation3200[1].program_stop_conditions[26] = StopConditionFor3200(program_ad=1)
            # self.config.BaseStation3200[1].program_stop_conditions[26].set_event_switch_condition_on_pg(
            #     serial_number=self.config.BaseStation3200[1].event_switches[5].sn, mode=opcodes.closed)
            #
            # self.config.BaseStation3200[1].programs[1].add_switch_stop_condition(_event_switch_address=5)
            # self.config.BaseStation3200[1].programs[1].event_switch_stop_conditions[5].set_switch_mode_to_closed()

            # self.config.BaseStation3200[1].program_stop_conditions[27] = StopConditionFor3200(program_ad=96)
            # self.config.BaseStation3200[1].program_stop_conditions[27].set_event_switch_condition_on_pg(
            #     serial_number=self.config.BaseStation3200[1].event_switches[1].sn, mode=opcodes.closed)
            #
            # self.config.BaseStation3200[1].programs[96].add_switch_stop_condition(_event_switch_address=1)
            # self.config.BaseStation3200[1].programs[96].event_switch_stop_conditions[1].set_switch_mode_to_closed()

            # self.config.BaseStation3200[1].program_stop_conditions[28] = StopConditionFor3200(program_ad=97)
            # self.config.BaseStation3200[1].program_stop_conditions[28].set_event_switch_condition_on_pg(
            #     serial_number=self.config.BaseStation3200[1].event_switches[2].sn, mode=opcodes.closed)
            #
            # self.config.BaseStation3200[1].programs[97].add_switch_stop_condition(_event_switch_address=2)
            # self.config.BaseStation3200[1].programs[97].event_switch_stop_conditions[2].set_switch_mode_to_closed()

            # self.config.BaseStation3200[1].program_stop_conditions[29] = StopConditionFor3200(program_ad=98)
            # self.config.BaseStation3200[1].program_stop_conditions[29].set_event_switch_condition_on_pg(
            #     serial_number=self.config.BaseStation3200[1].event_switches[3].sn, mode=opcodes.closed)
            #
            # self.config.BaseStation3200[1].programs[98].add_switch_stop_condition(_event_switch_address=3)
            # self.config.BaseStation3200[1].programs[98].event_switch_stop_conditions[3].set_switch_mode_to_closed()

            # self.config.BaseStation3200[1].program_stop_conditions[30] = StopConditionFor3200(program_ad=99)
            # self.config.BaseStation3200[1].program_stop_conditions[30].set_event_switch_condition_on_pg(
            #     serial_number=self.config.BaseStation3200[1].event_switches[4].sn, mode=opcodes.closed)
            #
            # self.config.BaseStation3200[1].programs[99].add_switch_stop_condition(_event_switch_address=4)
            # self.config.BaseStation3200[1].programs[99].event_switch_stop_conditions[4].set_switch_mode_to_closed()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n

        Program 1:
            - 20 program zones
            - Zone 1 is primary zone
            - Zone 2-20 are linked to Zone 1
            - Each zone has 15 min runtime (900 sec)

        Program 96:
            - 4 program zones
            - Zone 81-84 are timed
            - Each zone has 15 min runtime (900 sec)

        Program 97:
            - 4 program zones
            - Zone 85-88 are timed
            - Each zone has 15 min runtime (900 sec)

        Program 98:
            - 4 program zones
            - Zone 89-92 are timed
            - Each zone has 15 min runtime (900 sec)

        Program 99:
            - 4 program zones
            - Zone 93-96 are timed
            - Each zone has 15 min runtime (900 sec)
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:

            program_1_primary_zone_ad = 1
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=program_1_primary_zone_ad)
            self.config.BaseStation3200[1].programs[1].zone_programs[program_1_primary_zone_ad].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[program_1_primary_zone_ad].set_run_time(_minutes=15)

            # Add zones 2 - 20 as linked zones to Zone Program 1 for Program 1
            for zone_address in range(2, 21):
                if zone_address in self.config.BaseStation3200[1].zones.keys():
                    self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=zone_address)
                    self.config.BaseStation3200[1].programs[1].zone_programs[zone_address].set_as_linked_zone(
                                                                                _primary_zone=program_1_primary_zone_ad)
                    self.config.BaseStation3200[1].programs[1].zone_programs[zone_address].set_run_time(_minutes=15)

            # Program 96 has 4 zones (81-84)
            for zone_address in range(81, 85):
                if zone_address in self.config.BaseStation3200[1].zones.keys():
                    self.config.BaseStation3200[1].programs[96].add_zone_to_program(_zone_address=zone_address)
                    self.config.BaseStation3200[1].programs[96].zone_programs[zone_address].set_as_timed_zone()
                    self.config.BaseStation3200[1].programs[96].zone_programs[zone_address].set_run_time(_minutes=15)

            # Program 97 has 4 zones (85-88)
            for zone_address in range(85, 89):
                if zone_address in self.config.BaseStation3200[1].zones.keys():
                    self.config.BaseStation3200[1].programs[97].add_zone_to_program(_zone_address=zone_address)
                    self.config.BaseStation3200[1].programs[97].zone_programs[zone_address].set_as_timed_zone()
                    self.config.BaseStation3200[1].programs[97].zone_programs[zone_address].set_run_time(_minutes=15)

            # Program 98 has 4 zones (89-92)
            for zone_address in range(89, 93):
                if zone_address in self.config.BaseStation3200[1].zones.keys():
                    self.config.BaseStation3200[1].programs[98].add_zone_to_program(_zone_address=zone_address)
                    self.config.BaseStation3200[1].programs[98].zone_programs[zone_address].set_as_timed_zone()
                    self.config.BaseStation3200[1].programs[98].zone_programs[zone_address].set_run_time(_minutes=15)

            # Program 99 has 4 zones (93-96)
            for zone_address in range(93, 97):
                if zone_address in self.config.BaseStation3200[1].zones.keys():
                    self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=zone_address)
                    self.config.BaseStation3200[1].programs[99].zone_programs[zone_address].set_as_timed_zone()
                    self.config.BaseStation3200[1].programs[99].zone_programs[zone_address].set_run_time(_minutes=15)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].add_master_valve_to_point_of_control(_master_valve_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_flow_meter_to_point_of_control(_flow_meter_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=500)

            # Add POC 1 to Water Source 1
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(_point_of_control_address=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        # here we can either execute the following uncommented lines in procedural fashion, or we could re-init the
        # object, would have to import Mainline at the top, effectively accomplishing the same thing by:
        try:
            # Add & Configure ML 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=500)

            # Add ML 1 to POC 1
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines
            - set up zone  Attributes associated with mainlines \n
                - set design flow on zone
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=zone_address)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        Set the time to 7:53:00 \n
        Increment the clock by 6 minutes to 7:59:00 \n
        Verify the status for each program and zone on the controller \n
        - Program status: \n
            - program 1 done \n
            - program 96 done \n
            - program 97 done \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - all zones done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_date_and_time(_date='08/28/2014', _time='7:53:00')
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].do_increment_clock(minutes=6)
            self.config.BaseStation3200[1].verify_date_and_time()

            for zone in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        - set ms[1] to 28, which is past its upper limit \n
        - start Programs 1 \n
        - pause program 99 \n
        - stop program 98 \n
        - set the time to 8:10:00 \n
        - Program status: \n
            - Programs 1 running \n
            - program 96 done \n
            - program 97 done \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-5 watering \n
            - zones 6-20 waiting \n
            - zones 81-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=11)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=28)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 6):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(6, 21):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_waiting_to_water()
            for zone in range(81, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=22)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        - set ms[2] to 28, which is past its upper limit \n
        - start Programs 96 \n
        - pause Programs 1 \n
        - stop program 99 \n
        - set the time to 8:15:00 \n
        - Program status: \n
            - Programs 1 paused \n
            - program 96 running \n
            - program 97 done \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-20 paused \n
            - zones 81-84 watering \n
            - zones 85-88 done \n
            - zones 89-92 done \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].moisture_sensors[2].bicoder.set_moisture_percent(_percent=28)
            self.config.BaseStation3200[1].moisture_sensors[2].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_paused()
            for zone in range(81, 85):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(85, 89):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(89, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].moisture_sensors[2].bicoder.set_moisture_percent(_percent=22)
            self.config.BaseStation3200[1].moisture_sensors[2].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        - set ms[3] to 28, which is past its upper limit \n
        - start Programs 97 \n
        - pause Programs 96 \n
        - stop program 1 \n
        - set the time to 8:20:00 \n
        - Program status: \n
            - Programs 1 done \n
            - program 96 paused \n
            - program 97 running \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 paused \n
            - zones 85-88 watering \n
            - zones 89-92 done \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].moisture_sensors[3].bicoder.set_moisture_percent(_percent=28)
            self.config.BaseStation3200[1].moisture_sensors[3].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(81, 85):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_paused()
            for zone in range(85, 89):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(89, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].moisture_sensors[3].bicoder.set_moisture_percent(_percent=22)
            self.config.BaseStation3200[1].moisture_sensors[3].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        - set ms[4] to 28, which is past its upper limit \n
        - start Programs 98 \n
        - pause Programs 97 \n
        - stop program 96 \n
        - set the time to 8:25:00 \n
        - Program status: \n
            - Programs 1 done \n
            - program 96 done \n
            - program 97 paused \n
            - program 98 running \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 done \n
            - zones 85-88 paused \n
            - zones 89-92 watering \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].moisture_sensors[4].bicoder.set_moisture_percent(_percent=28)
            self.config.BaseStation3200[1].moisture_sensors[4].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(81, 85):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(85, 89):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_paused()
            for zone in range(89, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].moisture_sensors[4].bicoder.set_moisture_percent(_percent=22)
            self.config.BaseStation3200[1].moisture_sensors[4].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_14(self):
        """
        - set ms[5] to 28, which is past its upper limit \n
        - start Programs 99 \n
        - pause Programs 98 \n
        - stop program 97 \n
        - set the time to 8:30:00 \n
        - Program status: \n
            - Programs 1 done \n
            - program 96 done \n
            - program 97 done \n
            - program 98 paused \n
            - program 99 running \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 done \n
            - zones 85-88 done \n
            - zones 89-92 paused \n
            - zones 93-96 watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].moisture_sensors[5].bicoder.set_moisture_percent(_percent=28)
            self.config.BaseStation3200[1].moisture_sensors[5].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(81, 85):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(85, 89):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(89, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_paused()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].moisture_sensors[5].bicoder.set_moisture_percent(_percent=22)
            self.config.BaseStation3200[1].moisture_sensors[5].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        
    #################################
    def step_15(self):
        """
        - set ts[1] to 100, which is past its upper limit \n
        - start Programs 1 \n
        - pause program 99 \n
        - stop program 98 \n
        - set the time to 8:35:00 \n
        - Program status: \n
            - Programs 1, 2, 3 running \n
            - Programs 4-10 waiting \n
            - program 96 done \n
            - program 97 done \n
            - program 98 done \n
            - program 99 paused \n
        - Zone status: \n
            - zones 1-5 running \n
            - zones 6-20 waiting \n
            - zones 81-84 done \n
            - zones 85-88 done \n
            - zones 89-92 done \n
            - zones 93-96 paused \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].temperature_sensors[1].bicoder.set_temperature_reading(_degrees=100)
            self.config.BaseStation3200[1].temperature_sensors[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 6):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(6, 21):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_waiting_to_water()
            for zone in range(81, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_paused()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_paused()

            self.config.BaseStation3200[1].temperature_sensors[1].bicoder.set_temperature_reading(_degrees=70)
            self.config.BaseStation3200[1].temperature_sensors[1].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_16(self):
        """
        - set ts[2] to 100, which is past its upper limit \n
        - start Programs 96 \n
        - pause Programs 1 \n
        - stop program 99 \n
        - set the time to 8:40:00 \n
        - Program status: \n
            - Programs 1 paused \n
            - program 96 running \n
            - program 97 done \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-20 paused \n
            - zones 81-84 watering \n
            - zones 85-88 done \n
            - zones 89-92 done \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].temperature_sensors[2].bicoder.set_temperature_reading(_degrees=100)
            self.config.BaseStation3200[1].temperature_sensors[2].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_paused()
            for zone in range(81, 85):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(85, 89):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(89, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].temperature_sensors[2].bicoder.set_temperature_reading(_degrees=70)
            self.config.BaseStation3200[1].temperature_sensors[2].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_17(self):
        """
        - set ts[3] to 100, which is past its upper limit \n
        - start Programs 97 \n
        - pause Programs 96 \n
        - stop program 1 \n
        - set the time to 8:45:00 \n
        - Program status: \n
            - Programs 1 done \n
            - program 96 paused \n
            - program 97 running \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 paused \n
            - zones 85-88 watering \n
            - zones 89-92 done \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].temperature_sensors[3].bicoder.set_temperature_reading(_degrees=100)
            self.config.BaseStation3200[1].temperature_sensors[3].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(81, 85):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_paused()
            for zone in range(85, 89):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(89, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].temperature_sensors[3].bicoder.set_temperature_reading(_degrees=70)
            self.config.BaseStation3200[1].temperature_sensors[3].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        
    #################################
    def step_18(self):
        """
        - set ts[4] to 100, which is past its upper limit \n
        - start Programs 98 \n
        - pause Programs 97 \n
        - stop program 96 \n
        - set the time to 8:50:00 \n
        - Program status: \n
            - Programs 1 done \n
            - program 96 done \n
            - program 97 paused \n
            - program 98 running \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 done \n
            - zones 85-88 paused \n
            - zones 89-92 watering \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].temperature_sensors[4].bicoder.set_temperature_reading(_degrees=100)
            self.config.BaseStation3200[1].temperature_sensors[4].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(81, 85):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(85, 89):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_paused()
            for zone in range(89, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].temperature_sensors[4].bicoder.set_temperature_reading(_degrees=70)
            self.config.BaseStation3200[1].temperature_sensors[4].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_19(self):
        """
        - set ts[5] to 100, which is past its upper limit \n
        - start Programs 99 \n
        - pause Programs 98 \n
        - stop program 97 \n
        - set the time to 8:55:00 \n
        - Program status: \n
            - Programs 1 done \n
            - program 96 done \n
            - program 97 done \n
            - program 98 paused \n
            - program 99 running \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 done \n
            - zones 85-88 done \n
            - zones 89-92 paused \n
            - zones 93-96 watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].temperature_sensors[5].bicoder.set_temperature_reading(_degrees=100)
            self.config.BaseStation3200[1].temperature_sensors[5].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(81, 85):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(85, 89):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(89, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_paused()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].temperature_sensors[5].bicoder.set_temperature_reading(_degrees=70)
            self.config.BaseStation3200[1].temperature_sensors[5].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_20(self):
        """
        - set sw[1] to open \n
        - start Programs 1 \n
        - pause program 99 \n
        - stop program 98 \n
        - set the time to 9:00:00 \n
        - Program status: \n
            - Programs 1, 2, 3 running \n
            - Programs 4-10 waiting \n
            - program 96 done \n
            - program 97 done \n
            - program 98 done \n
            - program 99 paused \n
        - Zone status: \n
            - zones 1-5 running \n
            - zones 6-20 waiting \n
            - zones 81-84 done \n
            - zones 85-88 done \n
            - zones 89-92 done \n
            - zones 93-96 paused \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_open()
            self.config.BaseStation3200[1].event_switches[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 6):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(6, 21):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_waiting_to_water()
            for zone in range(81, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_paused()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_paused()

            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_closed()
            self.config.BaseStation3200[1].event_switches[1].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_21(self):
        """
        - set sw[2] to open \n
        - start program 96 \n
        - pause Programs 1 \n
        - stop program 99 \n
        - set the time to 9:05:00 \n
        - Program status: \n
            - Programs 1 paused \n
            - program 96 running \n
            - program 97 done \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-20 paused \n
            - zones 81-84 watering \n
            - zones 85-88 done \n
            - zones 89-92 done \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].event_switches[2].bicoder.set_contact_open()
            self.config.BaseStation3200[1].event_switches[2].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_paused()
            for zone in range(81, 85):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(85, 89):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(89, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].event_switches[2].bicoder.set_contact_closed()
            self.config.BaseStation3200[1].event_switches[2].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_22(self):
        """
        - set sw[3] to open \n
        - start program 97 \n
        - pause program 96 \n
        - stop Programs 1 \n
        - set the time to 9:10:00 \n
        - Program status: \n
            - Programs 1 done \n
            - program 96 paused \n
            - program 97 running \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 paused \n
            - zones 85-88 watering \n
            - zones 89-92 done \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].event_switches[3].bicoder.set_contact_open()
            self.config.BaseStation3200[1].event_switches[3].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(81, 85):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_paused()
            for zone in range(85, 89):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(89, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].event_switches[3].bicoder.set_contact_closed()
            self.config.BaseStation3200[1].event_switches[3].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_23(self):
        """
        - set sw[4] to 100, which is past its upper limit \n
        - start program 98 \n
        - pause program 97 \n
        - stop program 96 \n
        - set the time to 9:15:00 \n
        - Program status: \n
            - Programs 1 done \n
            - program 96 done \n
            - program 97 paused \n
            - program 98 running \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 done \n
            - zones 85-88 paused \n
            - zones 89-92 watering \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].event_switches[4].bicoder.set_contact_open()
            self.config.BaseStation3200[1].event_switches[4].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(81, 85):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(85, 89):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_paused()
            for zone in range(89, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].event_switches[4].bicoder.set_contact_closed()
            self.config.BaseStation3200[1].event_switches[4].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_24(self):
        """
        - set sw[5] to 100, which is past its upper limit \n
        - start program 99 \n
        - pause program 98 \n
        - stop program 97 \n
        - set the time to 9:20:00 \n
        - Program status: \n
            - Programs 1 done \n
            - program 96 done \n
            - program 97 done \n
            - program 98 paused \n
            - program 99 running \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 done \n
            - zones 85-88 done \n
            - zones 89-92 paused \n
            - zones 93-96 watering \n
        - At the end we briefly change moisture sensor's 1 value to trigger some Programs upper threshold. We do this
          to reset some of status' to "beginning conditions" for this test.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].event_switches[5].bicoder.set_contact_open()
            self.config.BaseStation3200[1].event_switches[5].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(81, 85):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(85, 89):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(89, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_paused()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].event_switches[5].bicoder.set_contact_closed()
            self.config.BaseStation3200[1].event_switches[5].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_25(self):
        """
        - set ms[4] to 10, which is past its lower limit \n
        - We stop every program that has been running up until this point so we can have a fresh start on the second
          round of start/stop conditions \n
        - Increment the time by 1 minute so controller can update \n
        - Set moisture sensor 1's moisture percent to 10 which will trigger a start/stop condition \n
            - start Programs 1 \n
            - stop program 99 \n
        - Increment the clock by 1 minute to give the controller time to process \n
        - The time should be 9:26:00 \n
        - Program status: \n
            - Programs 1, 2, 3 running \n
            - Programs 4-10 waiting \n
            - program 96 done \n
            - program 97 done \n
            - program 98 paused \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-5 watering \n
            - zones 6-20 waiting \n
            - zones 81-84 done \n
            - zones 85-88 done \n
            - zones 89-92 done \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            # Stop all Programs so we can start our second round of conditions
            for program in sorted(self.config.BaseStation3200[1].programs.keys()):
                self.config.BaseStation3200[1].programs[program].set_program_to_stop()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].moisture_sensors[4].bicoder.set_moisture_percent(_percent=10)
            self.config.BaseStation3200[1].moisture_sensors[4].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # TODO, this is the start of the second batch of Starts/stops. Make sure nothing is running up until here

            for zone in range(1, 6):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(6, 21):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_waiting_to_water()
            for zone in range(81, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].moisture_sensors[4].bicoder.set_moisture_percent(_percent=22)
            self.config.BaseStation3200[1].moisture_sensors[4].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_26(self):
        """
        - set ms[5] to 10, which is past its lower limit \n
        - start Programs 96 \n
        - stop program 1 \n
        - set the time to 9:30:00 \n
        - Program status: \n
            - Programs 1 done \n
            - program 96 running \n
            - program 97 done \n
            - program 98 paused \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 watering \n
            - zones 85-88 done \n
            - zones 89-92 done \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=3)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].moisture_sensors[5].bicoder.set_moisture_percent(_percent=10)
            self.config.BaseStation3200[1].moisture_sensors[5].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(81, 85):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(85, 89):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(89, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].moisture_sensors[5].bicoder.set_moisture_percent(_percent=22)
            self.config.BaseStation3200[1].moisture_sensors[5].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_27(self):
        """
        - set ms[1] to 10, which is past its lower limit \n
        - start Programs 97 \n
        - stop program 96 \n
        - set the time to 9:35:00 \n
        - Program status: \n
            - Programs 1 done \n
            - program 96 done \n
            - program 97 running \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 done \n
            - zones 85-88 watering \n
            - zones 89-92 done \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=10)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(81, 85):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(85, 89):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(89, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=22)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_28(self):
        """
        - set ms[2] to 10, which is past its lower limit \n
        - start Programs 98 \n
        - stop program 97 \n
        - set the time to 9:40:00 \n
        - Program status: \n
            - Programs 1 done \n
            - program 96 done \n
            - program 97 done \n
            - program 98 running \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 done \n
            - zones 85-88 done \n
            - zones 89-92 watering \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].moisture_sensors[2].bicoder.set_moisture_percent(_percent=10)
            self.config.BaseStation3200[1].moisture_sensors[2].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(81, 85):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(85, 89):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(89, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].moisture_sensors[2].bicoder.set_moisture_percent(_percent=20)
            self.config.BaseStation3200[1].moisture_sensors[2].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
    
    #################################
    def step_29(self):
        """
        - set ms[3] to 10, which is past its lower limit \n
        - start Programs 99 \n
        - stop program 98 \n
        - set the time to 9:45:00 \n
        - Program status: \n
            - Programs 1 done \n
            - program 96 done \n
            - program 97 done \n
            - program 98 done \n
            - program 99 running \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 done \n
            - zones 85-88 done \n
            - zones 89-92 watering \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].moisture_sensors[3].bicoder.set_moisture_percent(_percent=10)
            self.config.BaseStation3200[1].moisture_sensors[3].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(81, 85):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(85, 89):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(89, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].moisture_sensors[3].bicoder.set_moisture_percent(_percent=20)
            self.config.BaseStation3200[1].moisture_sensors[3].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_30(self):
        """
        - set ts[4] to 30, which is past its lower limit \n
        - start Programs 1 \n
        - stop program 99 \n
        - set the time to 9:50:00 \n
        - Program status: \n
            - Programs 1, 2, 3 running \n
            - Programs 4-10 waiting \n
            - program 96 done \n
            - program 97 done \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-5 watering \n
            - zones 6-20 waiting \n
            - zones 81-84 done \n
            - zones 85-88 done \n
            - zones 89-92 done \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].temperature_sensors[4].bicoder.set_temperature_reading(_degrees=30)
            self.config.BaseStation3200[1].temperature_sensors[4].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 6):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(6, 21):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_waiting_to_water()
            for zone in range(81, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].temperature_sensors[4].bicoder.set_temperature_reading(_degrees=70)
            self.config.BaseStation3200[1].temperature_sensors[4].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_31(self):
        """
        - set ts[5] to 30, which is past its lower limit \n
        - start Programs 96 \n
        - stop program 1 \n
        - set the time to 9:55:00 \n
        - Program status: \n
            - Programs 1 done \n
            - program 96 running \n
            - program 97 done \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 watering \n
            - zones 85-88 done \n
            - zones 89-92 done \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].temperature_sensors[5].bicoder.set_temperature_reading(_degrees=30)
            self.config.BaseStation3200[1].temperature_sensors[5].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(81, 85):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(85, 89):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(89, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].temperature_sensors[5].bicoder.set_temperature_reading(_degrees=70)
            self.config.BaseStation3200[1].temperature_sensors[5].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
    
    #################################
    def step_32(self):
        """
        - set ts[1] to 30, which is past its lower limit \n
        - start Programs 97 \n
        - stop program 96 \n
        - set the time to 10:00:00 \n
        - Program status: \n
            - Programs 1 done \n
            - program 96 done \n
            - program 97 running \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 done \n
            - zones 85-88 watering \n
            - zones 89-92 done \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].temperature_sensors[1].bicoder.set_temperature_reading(_degrees=30)
            self.config.BaseStation3200[1].temperature_sensors[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(81, 85):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(85, 89):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(89, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].temperature_sensors[1].bicoder.set_temperature_reading(_degrees=70)
            self.config.BaseStation3200[1].temperature_sensors[1].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
    
    #################################
    def step_33(self):
        """
        - set ts[2] to 30, which is past its lower limit \n
        - start Programs 98 \n
        - stop program 97 \n
        - set the time to 10:05:00 \n
        - Program status: \n
            - Programs 1 done \n
            - program 96 done \n
            - program 97 done \n
            - program 98 running \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 done \n
            - zones 85-88 done \n
            - zones 89-92 watering \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].temperature_sensors[2].bicoder.set_temperature_reading(_degrees=30)
            self.config.BaseStation3200[1].temperature_sensors[2].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(81, 85):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(85, 89):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(89, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].temperature_sensors[2].bicoder.set_temperature_reading(_degrees=70)
            self.config.BaseStation3200[1].temperature_sensors[2].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_34(self):
        """
        - set ts[3] to 30, which is past its lower limit \n
        - start Programs 99 \n
        - stop program 98 \n
        - set the time to 10:10:00 \n
        - Program status: \n
            - Programs 1 done \n
            - program 96 done \n
            - program 97 done \n
            - program 98 done \n
            - program 99 running \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 done \n
            - zones 85-88 done \n
            - zones 89-92 done \n
            - zones 93-96 watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].temperature_sensors[3].bicoder.set_temperature_reading(_degrees=30)
            self.config.BaseStation3200[1].temperature_sensors[3].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(81, 85):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(85, 89):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(89, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].temperature_sensors[3].bicoder.set_temperature_reading(_degrees=70)
            self.config.BaseStation3200[1].temperature_sensors[3].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
    
    ################################
    def step_35(self):
        """
        setup start/stop/pause conditions: \n
            - make Programs 1
                - start by (SW[4])
                - stop by (SW[5])
            - make program 96
                - start by (SW[5])
                - stop by (SW[1])
            - make program 97
                - start by (SW[1])
                - stop by (SW[2])
            - make program 98
                - start by (SW[2])
                - stop by (SW[3])
            - make program 99
                - start by (SW[3])
                - stop by (SW[4])
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set switch mode to off for old event switch START conditions so they don't trigger
            self.config.BaseStation3200[1].programs[1].event_switch_start_conditions[1].set_switch_mode_to_off()
            self.config.BaseStation3200[1].programs[96].event_switch_start_conditions[2].set_switch_mode_to_off()
            self.config.BaseStation3200[1].programs[97].event_switch_start_conditions[3].set_switch_mode_to_off()
            self.config.BaseStation3200[1].programs[98].event_switch_start_conditions[4].set_switch_mode_to_off()
            self.config.BaseStation3200[1].programs[99].event_switch_start_conditions[5].set_switch_mode_to_off()

            # Set switch mode to off for old event switch PAUSE conditions so they don't trigger
            self.config.BaseStation3200[1].programs[1].event_switch_pause_conditions[2].set_switch_mode_to_off()
            self.config.BaseStation3200[1].programs[96].event_switch_pause_conditions[3].set_switch_mode_to_off()
            self.config.BaseStation3200[1].programs[97].event_switch_pause_conditions[4].set_switch_mode_to_off()
            self.config.BaseStation3200[1].programs[98].event_switch_pause_conditions[5].set_switch_mode_to_off()
            self.config.BaseStation3200[1].programs[99].event_switch_pause_conditions[1].set_switch_mode_to_off()

            # Set switch mode to off for old event switch STOP conditions so they don't trigger
            self.config.BaseStation3200[1].programs[1].event_switch_stop_conditions[3].set_switch_mode_to_off()
            self.config.BaseStation3200[1].programs[96].event_switch_stop_conditions[4].set_switch_mode_to_off()
            self.config.BaseStation3200[1].programs[97].event_switch_stop_conditions[5].set_switch_mode_to_off()
            self.config.BaseStation3200[1].programs[98].event_switch_stop_conditions[1].set_switch_mode_to_off()
            self.config.BaseStation3200[1].programs[99].event_switch_stop_conditions[2].set_switch_mode_to_off()

            # Set all the event switches to an open contact state so they aren't triggering the new conditions
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_open()
            self.config.BaseStation3200[1].event_switches[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].event_switches[2].bicoder.set_contact_open()
            self.config.BaseStation3200[1].event_switches[2].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].event_switches[3].bicoder.set_contact_open()
            self.config.BaseStation3200[1].event_switches[3].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].event_switches[4].bicoder.set_contact_open()
            self.config.BaseStation3200[1].event_switches[4].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].event_switches[5].bicoder.set_contact_open()
            self.config.BaseStation3200[1].event_switches[5].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # ----------------------------------------------------------------------------------------------------------
            # SWITCH START CONDITIONS GROUP 2
            # ----------------------------------------------------------------------------------------------------------
            self.config.BaseStation3200[1].programs[1].add_switch_start_condition(_event_switch_address=4)
            self.config.BaseStation3200[1].programs[1].event_switch_start_conditions[4].set_switch_mode_to_closed()

            self.config.BaseStation3200[1].programs[96].add_switch_start_condition(_event_switch_address=5)
            self.config.BaseStation3200[1].programs[96].event_switch_start_conditions[5].set_switch_mode_to_closed()

            self.config.BaseStation3200[1].programs[97].add_switch_start_condition(_event_switch_address=1)
            self.config.BaseStation3200[1].programs[97].event_switch_start_conditions[1].set_switch_mode_to_closed()

            self.config.BaseStation3200[1].programs[98].add_switch_start_condition(_event_switch_address=2)
            self.config.BaseStation3200[1].programs[98].event_switch_start_conditions[2].set_switch_mode_to_closed()

            self.config.BaseStation3200[1].programs[99].add_switch_start_condition(_event_switch_address=3)
            self.config.BaseStation3200[1].programs[99].event_switch_start_conditions[3].set_switch_mode_to_closed()

            # ----------------------------------------------------------------------------------------------------------
            # SWITCH STOP CONDITIONS GROUP 2
            # ----------------------------------------------------------------------------------------------------------
            self.config.BaseStation3200[1].programs[1].add_switch_stop_condition(_event_switch_address=5)
            self.config.BaseStation3200[1].programs[1].event_switch_stop_conditions[5].set_switch_mode_to_closed()

            self.config.BaseStation3200[1].programs[96].add_switch_stop_condition(_event_switch_address=1)
            self.config.BaseStation3200[1].programs[96].event_switch_stop_conditions[1].set_switch_mode_to_closed()

            self.config.BaseStation3200[1].programs[97].add_switch_stop_condition(_event_switch_address=2)
            self.config.BaseStation3200[1].programs[97].event_switch_stop_conditions[2].set_switch_mode_to_closed()

            self.config.BaseStation3200[1].programs[98].add_switch_stop_condition(_event_switch_address=3)
            self.config.BaseStation3200[1].programs[98].event_switch_stop_conditions[3].set_switch_mode_to_closed()

            self.config.BaseStation3200[1].programs[99].add_switch_stop_condition(_event_switch_address=4)
            self.config.BaseStation3200[1].programs[99].event_switch_stop_conditions[4].set_switch_mode_to_closed()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ################################
    def step_36(self):
        """
        - set sw[4] to closed \n
        - start Programs 1 \n
        - stop program 99 \n
        - set the time to 10:15:00 \n
        - Program status: \n
            - Programs 1 running \n
            - program 96 done \n
            - program 97 done \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-20 watering \n
            - zones 81-84 done \n
            - zones 85-88 done \n
            - zones 89-92 done \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=3)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].event_switches[4].bicoder.set_contact_closed()
            self.config.BaseStation3200[1].event_switches[4].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 6):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(6, 21):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_waiting_to_water()
            for zone in range(81, 85):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(85, 89):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(89, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].event_switches[4].bicoder.set_contact_open()
            self.config.BaseStation3200[1].event_switches[4].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
    
    ################################
    def step_37(self):
        """
        - set sw[5] to closed \n
        - start Programs 96 \n
        - stop program 1 \n
        - set the time to 10:20:00 \n
        - Program status: \n
            - Programs 1 done \n
            - program 96 running \n
            - program 97 done \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 watering \n
            - zones 85-88 done \n
            - zones 89-92 done \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].event_switches[5].bicoder.set_contact_closed()
            self.config.BaseStation3200[1].event_switches[5].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(81, 85):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(85, 89):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(89, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].event_switches[5].bicoder.set_contact_open()
            self.config.BaseStation3200[1].event_switches[5].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ################################
    def step_38(self):
        """
        - set sw[1] to closed \n
        - start Programs 97 \n
        - stop program 96 \n
        - set the time to 10:25:00 \n
        - Program status: \n
            - Programs 1 done \n
            - program 96 done \n
            - program 97 running \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 done \n
            - zones 85-88 watering \n
            - zones 89-92 done \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_closed()
            self.config.BaseStation3200[1].event_switches[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(81, 85):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(85, 89):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(89, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_open()
            self.config.BaseStation3200[1].event_switches[1].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ################################
    def step_39(self):
        """
        - set sw[2] to closed \n
        - start Programs 98 \n
        - stop program 97 \n
        - set the time to 10:30:00 \n
        - Program status: \n
            - Programs 1 done \n
            - program 96 done \n
            - program 97 done \n
            - program 98 running \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 done \n
            - zones 85-88 done \n
            - zones 89-92 watering \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].event_switches[2].bicoder.set_contact_closed()
            self.config.BaseStation3200[1].event_switches[2].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(81, 85):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(85, 89):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(89, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].event_switches[2].bicoder.set_contact_open()
            self.config.BaseStation3200[1].event_switches[2].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
    
    ################################
    def step_40(self):
        """
        - set sw[3] to closed \n
        - start Programs 99 \n
        - stop program 98 \n
        - set the time to 10:35:00 \n
        - Program status: \n
            - Programs 1 done \n
            - program 96 done \n
            - program 97 done \n
            - program 98 done \n
            - program 99 running \n
        - Zone status: \n
            - zones 1-20 done \n
            - zones 81-84 done \n
            - zones 85-88 done \n
            - zones 89-92 done \n
            - zones 93-96 watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].event_switches[3].bicoder.set_contact_closed()
            self.config.BaseStation3200[1].event_switches[3].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 21):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(81, 85):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(85, 89):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(89, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].event_switches[3].bicoder.set_contact_open()
            self.config.BaseStation3200[1].event_switches[3].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ################################
    def step_41(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

