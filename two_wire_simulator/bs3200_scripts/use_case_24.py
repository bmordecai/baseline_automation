import sys
from time import sleep
from datetime import datetime, timedelta

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

from common import helper_methods

__author__ = "Eldin"


class ControllerUseCase24(object):
    """
    Test name: \n
        Primary Zone Test \n
        
    Purpose: \n
        The purpose of this test is to verify the functionality of zone Programs. We have zones on multiple Programs and
        different configurations for each program. These are documented below in steps 3-8. \n
        
    Coverage area: \n
        1.  Set up 4 primary zones \n
        2.  Link 4 zones to each primary \n
        3.  Assign each primary to two different Programs \n
        4.  Assign a one moisture sensor to each primary for the first four Programs make sure it is the same sensor \n
        5.  Assign a one moisture sensor to each primary for the second four Programs make sure it is the same sensor \n
        6.  Verify correct zone information stays in the controller when primary zones are associated with a second
            program \n
        7.  Verify correct primary linked information stays in the controller when primary zones are associated with a
            second program \n
        8.  Verify correct program information stays in the controller when primary zones are associated with a second
            program \n
        9.  Verify percentage tracking follows the primary zone in the program it is associated with and doesnt affect
            the other program \n
        10. Verify that design flow is followed when primary zones that are associated with multiple program  share main
            lines \n
        11. Verify that zone concurrency is followed when multiple program turn on at the same time that share the same
            zones \n
        12. Verify that water window are followed when multiple primary zone on multiple Programs \n
        13. Verify that watering days are followed when multiple primary zone on multiple Programs are associated with
            different watering schedule \n
    """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)


    ###############################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        - Initialize Each Program. \n
            - Program 1:
                - Max Concurrent Zones: 5 \n
                - Calendar Interval: Week Days \n
                - Week Days: Every Day \n
                - Water Windows: Weekly Water Windows \n
                - Start Times: 9 AM, 1 AM \n
            - Program 2:
                - Max Concurrent Zones: 5 \n
                - Calendar Interval: Week Days \n
                - Week Days: Monday, Wednesday, Friday \n
                - Water Windows: Weekly Water Windows \n
                - Start Times: 9 AM, 2 AM \n
            - Program 3:
                - Max Concurrent Zones: 5 \n
                - Calendar Interval: Odd Days \n
                - Water Windows: Weekly Water Windows \n
                - Start Times: 9 AM, 3 AM \n
            - Program 4:
                - Max Concurrent Zones: 5 \n
                - Calendar Interval: Even Days \n
                - Water Windows: Daily Water Windows \n
                - Start Times: 9 AM, 4 AM \n
            - Program 5:
                - Max Concurrent Zones: 10 \n
                - Calendar Interval: Week Days \n
                - Week Days: Every Day \n
                - Water Windows: Weekly Water Windows \n
                - Start Times: 9 AM, 5 AM \n
            - Program 6:
                - Max Concurrent Zones: 10 \n
                - Calendar Interval: Week Days \n
                - Week Days: Every Day \n
                - Water Windows: Weekly Water Windows \n
                - Start Times: 9 AM, 6 AM \n
            - Program 7:
                - Max Concurrent Zones: 10 \n
                - Calendar Interval: Week Days \n
                - Week Days: Every Day \n
                - Water Windows: Weekly Water Windows \n
                - Start Times: 9 AM, 7 AM \n
            - Program 8:
                - Max Concurrent Zones: 10 \n
                - Calendar Interval: Week Days \n
                - Week Days: Every Day \n
                - Water Windows: Daily Water Windows \n
                - Start Times: 9 AM, 8 AM \n
        """
        # this is set in the PG3200 object
        program_start_time_9_and_1_am = [540, 60]  # 9:00 am start time and 1:00 am start time
        program_start_time_9_and_2_am = [540, 120]  # 9:00 am start time and 2:00 am start time
        program_start_time_9_and_3_am = [540, 180]  # 9:00 am start time and 3:00 am start time
        program_start_time_9_and_4_am = [540, 240]  # 9:00 am start time and 4:00 am start time
        program_start_time_9_and_5_am = [540, 300]  # 9:00 am start time and 5:00 am start time
        program_start_time_9_and_6_am = [540, 360]  # 9:00 am start time and 6:00 am start time
        program_start_time_9_and_7_am = [540, 420]  # 9:00 am start time and 7:00 am start time
        program_start_time_9_and_8_am = [540, 480]  # 9:00 am start time and 8:00 am start time

        program_week_day_every_day = [1, 1, 1, 1, 1, 1, 1]
        program_week_day_mon_wed_fri = [0, 1, 0, 1, 0, 1, 0]

        program_water_windows_weekly = ["011111100001111111111110"]  # can water in: [1am-7am, 11am-10pm]
        
        program_water_windows_daily = ["011111100000111111111110",  # SUN can water in: [1am-7am, 12pm-10pm]
                                       "011111100001111111111111",  # MON can water in: [1am-7am, 11am-11pm]
                                       "011111100001111111111110",  # TUE can water in: [1am-7am, 11am-10pm]
                                       "011111100001111111111110",  # WED can water in: [1am-7am, 11am-10pm]
                                       "011111100001111111111110",  # THU can water in: [1am-7am, 11am-10pm]
                                       "011111100001111111111110",  # FRI can water in: [1am-7am, 11am-10pm]
                                       "011111100001111111111111"]  # SAT can water in: [1am-7am, 11am-11pm]

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # TODO: Updated - 1/10/18 - Ben
            #
            # Need to set max concurrency on the controller in order to allow more than 1 zone to water. Wasn't set,
            # which was causing this test to fail pretty quick.
            #
            # Here setting it to 15 because of what step 11 said in the program comments.
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=15)
            
            # Add and configure Program 1
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=5)
            self.config.BaseStation3200[1].programs[1].set_water_window(_ww=program_water_windows_weekly)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(
                _sun=True, _mon=True, _tues=True, _wed=True, _thurs=True, _fri=True, _sat=True
            )
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_start_time_9_and_1_am)

            # Add and configure Program 2
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation3200[1].programs[2].set_enabled()
            self.config.BaseStation3200[1].programs[2].set_max_concurrent_zones(_number_of_zones=5)
            self.config.BaseStation3200[1].programs[2].set_water_window(_ww=program_water_windows_weekly)
            self.config.BaseStation3200[1].programs[2].set_watering_intervals_to_selected_days_of_the_week(
                _sun=False, _mon=True, _tues=False, _wed=True, _thurs=False, _fri=True, _sat=False
            )
            self.config.BaseStation3200[1].programs[2].set_start_times(_st_list=program_start_time_9_and_2_am)

            # Add and configure Program 3
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=3)
            self.config.BaseStation3200[1].programs[3].set_enabled()
            self.config.BaseStation3200[1].programs[3].set_max_concurrent_zones(_number_of_zones=5)
            self.config.BaseStation3200[1].programs[3].set_water_window(_ww=program_water_windows_weekly)
            self.config.BaseStation3200[1].programs[3].set_watering_intervals_to_odd_days()
            self.config.BaseStation3200[1].programs[3].set_start_times(_st_list=program_start_time_9_and_3_am)

            # Add and configure Program 4
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=4)
            self.config.BaseStation3200[1].programs[4].set_enabled()
            self.config.BaseStation3200[1].programs[4].set_max_concurrent_zones(_number_of_zones=5)
            self.config.BaseStation3200[1].programs[4].set_water_window(_ww=program_water_windows_daily, _is_weekly=False)
            self.config.BaseStation3200[1].programs[4].set_watering_intervals_to_even_days()
            self.config.BaseStation3200[1].programs[4].set_start_times(_st_list=program_start_time_9_and_4_am)

            # Add and configure Program 5
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=5)
            self.config.BaseStation3200[1].programs[5].set_enabled()
            self.config.BaseStation3200[1].programs[5].set_max_concurrent_zones(_number_of_zones=10)
            self.config.BaseStation3200[1].programs[5].set_water_window(_ww=program_water_windows_weekly)
            self.config.BaseStation3200[1].programs[5].set_watering_intervals_to_selected_days_of_the_week(
                _sun=True, _mon=True, _tues=True, _wed=True, _thurs=True, _fri=True, _sat=True
            )
            self.config.BaseStation3200[1].programs[5].set_start_times(_st_list=program_start_time_9_and_5_am)

            # Add and configure Program 6
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=6)
            self.config.BaseStation3200[1].programs[6].set_enabled()
            self.config.BaseStation3200[1].programs[6].set_max_concurrent_zones(_number_of_zones=10)
            self.config.BaseStation3200[1].programs[6].set_water_window(_ww=program_water_windows_weekly)
            self.config.BaseStation3200[1].programs[6].set_watering_intervals_to_selected_days_of_the_week(
                _sun=True, _mon=True, _tues=True, _wed=True, _thurs=True, _fri=True, _sat=True
            )
            self.config.BaseStation3200[1].programs[6].set_start_times(_st_list=program_start_time_9_and_6_am)

            # Add and configure Program 7
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=7)
            self.config.BaseStation3200[1].programs[7].set_enabled()
            self.config.BaseStation3200[1].programs[7].set_max_concurrent_zones(_number_of_zones=10)
            self.config.BaseStation3200[1].programs[7].set_water_window(_ww=program_water_windows_weekly)
            self.config.BaseStation3200[1].programs[7].set_watering_intervals_to_selected_days_of_the_week(
                _sun=True, _mon=True, _tues=True, _wed=True, _thurs=True, _fri=True, _sat=True
            )
            self.config.BaseStation3200[1].programs[7].set_start_times(_st_list=program_start_time_9_and_7_am)

            # Add and configure Program 8
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=8)
            self.config.BaseStation3200[1].programs[8].set_enabled()
            self.config.BaseStation3200[1].programs[8].set_max_concurrent_zones(_number_of_zones=10)
            self.config.BaseStation3200[1].programs[8].set_water_window(_ww=program_water_windows_daily, _is_weekly=False)
            self.config.BaseStation3200[1].programs[8].set_watering_intervals_to_selected_days_of_the_week(
                _sun=True, _mon=True, _tues=True, _wed=True, _thurs=True, _fri=True, _sat=True
            )
            self.config.BaseStation3200[1].programs[8].set_start_times(_st_list=program_start_time_9_and_8_am)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        - Set up Zone Programs \n
            - Zone 1 Program 1 (Primary Zone) \n
                - Run Time: 900 \n
                - Cycle Time: 300 \n
                - Soak Time: 600 \n
                - Moisture Sensor: SB05308 (1) \n
                - Watering Strategy: Lower Limit \n
                - Calibration Cycle: Single \n
                - Linked Zones: \n
                    - Zone 2 Program 1 \n
                        - Tracking Ratio: 100% \n
                    - Zone 3 Program 1 \n
                        - Tracking Ratio: 100% \n
                    - Zone 4 Program 1 \n
                        - Tracking Ratio: 100% \n
                    - Zone 5 Program 1 \n
                        - Tracking Ratio: 100% \n
            - Zone 6 Program 2 (Primary Zone) \n
                - Run Time: 900 \n
                - Cycle Time: 300 \n
                - Soak Time: 600 \n
                - Moisture Sensor: SB05308 (1) \n
                - Watering Strategy: Lower Limit \n
                - Calibration Cycle: Single \n
                - Linked Zones: \n
                    - Zone 7 Program 2 \n
                        - Tracking Ratio: 100% \n
                    - Zone 8 Program 2 \n
                        - Tracking Ratio: 100% \n
                    - Zone 9 Program 2 \n
                        - Tracking Ratio: 100% \n
                    - Zone 10 Program 2 \n
                        - Tracking Ratio: 100% \n
            - Zone 11 Program 3 (Primary Zone) \n
                - Run Time: 900 \n
                - Cycle Time: 300 \n
                - Soak Time: 600 \n
                - Moisture Sensor: SB05308 (1) \n
                - Watering Strategy: Lower Limit \n
                - Calibration Cycle: Single \n
                - Linked Zones: \n
                    - Zone 12 Program 3 \n
                        - Tracking Ratio: 150% \n
                    - Zone 13 Program 3 \n
                        - Tracking Ratio: 100% \n
                    - Zone 14 Program 3 \n
                        - Tracking Ratio: 100% \n
                    - Zone 15 Program 3 \n
                        - Tracking Ratio: 100% \n
            - Zone 16 Program 4 (Primary Zone) \n
                - Run Time: 900 \n
                - Cycle Time: 300 \n
                - Soak Time: 600 \n
                - Moisture Sensor: SB05308 (1) \n
                - Watering Strategy: Lower Limit \n
                - Calibration Cycle: Single \n
                - Linked Zones: \n
                    - Zone 17 Program 4 \n
                        - Tracking Ratio: 100% \n
                    - Zone 18 Program 4 \n
                        - Tracking Ratio: 100% \n
                    - Zone 19 Program 4 \n
                        - Tracking Ratio: 50% \n
                    - Zone 20 Program 4 \n
                        - Tracking Ratio: 100% \n
            - Zone 1 Program 5 (Primary Zone) \n
                - Run Time: 1800 \n
                - Cycle Time: 300 \n
                - Soak Time: 600 \n
                - Moisture Sensor: SB07258 (2) \n
                - Watering Strategy: Lower Limit \n
                - Calibration Cycle: Single \n
                - Linked Zones: \n
                    - Zone 2 Program 5 \n
                        - Tracking Ratio: 100% \n
                    - Zone 3 Program 5 \n
                        - Tracking Ratio: 100% \n
                    - Zone 4 Program 5 \n
                        - Tracking Ratio: 100% \n
                    - Zone 5 Program 5 \n
                        - Tracking Ratio: 100% \n
            - Zone 6 Program 6 (Primary Zone) \n
                - Run Time: 1800 \n
                - Cycle Time: 300 \n
                - Soak Time: 600 \n
                - Moisture Sensor: SB07258 (2) \n
                - Watering Strategy: Lower Limit \n
                - Calibration Cycle: Single \n
                - Linked Zones: \n
                    - Zone 7 Program 6 \n
                        - Tracking Ratio: 100% \n
                    - Zone 8 Program 6 \n
                        - Tracking Ratio: 100% \n
                    - Zone 9 Program 6 \n
                        - Tracking Ratio: 100% \n
                    - Zone 10 Program 6 \n
                        - Tracking Ratio: 100% \n
            - Zone 11 Program 7 (Primary Zone) \n
                - Run Time: 1800 \n
                - Cycle Time: 300 \n
                - Soak Time: 600 \n
                - Moisture Sensor: SB07258 (2) \n
                - Watering Strategy: Lower Limit \n
                - Calibration Cycle: Single \n
                - Linked Zones: \n
                    - Zone 12 Program 7 \n
                        - Tracking Ratio: 100% \n
                    - Zone 13 Program 7 \n
                        - Tracking Ratio: 100% \n
                    - Zone 14 Program 7 \n
                        - Tracking Ratio: 100% \n
                    - Zone 15 Program 7 \n
                        - Tracking Ratio: 100% \n
            - Zone 16 Program 8 (Primary Zone) \n
                - Run Time: 1800 \n
                - Cycle Time: 300 \n
                - Soak Time: 600 \n
                - Moisture Sensor: SB07258 (2) \n
                - Watering Strategy: Lower Limit \n
                - Calibration Cycle: Single \n
                - Linked Zones: \n
                    - Zone 17 Program 8 \n
                        - Tracking Ratio: 100% \n
                    - Zone 18 Program 8 \n
                        - Tracking Ratio: 100% \n
                    - Zone 19 Program 8 \n
                        - Tracking Ratio: 100% \n
                    - Zone 20 Program 8 \n
                        - Tracking Ratio: 100% \n
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=10)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].add_moisture_sensor_to_primary_zone(_moisture_sensor_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_water_strategy_to_lower_limit()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_lower_limit_threshold(_percent=35.0)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_one_time_calibration_cycle()

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[1].zone_programs[4].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[5].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=6)
            self.config.BaseStation3200[1].programs[2].zone_programs[6].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[2].zone_programs[6].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[2].zone_programs[6].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[2].zone_programs[6].set_soak_time(_minutes=10)
            self.config.BaseStation3200[1].programs[2].zone_programs[6].add_moisture_sensor_to_primary_zone(
                _moisture_sensor_address=1)
            self.config.BaseStation3200[1].programs[2].zone_programs[6].set_water_strategy_to_lower_limit()
            self.config.BaseStation3200[1].programs[2].zone_programs[6].set_lower_limit_threshold(_percent=35.0)
            self.config.BaseStation3200[1].programs[2].zone_programs[6].set_one_time_calibration_cycle()

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=7)
            self.config.BaseStation3200[1].programs[2].zone_programs[7].set_as_linked_zone(_primary_zone=6)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=8)
            self.config.BaseStation3200[1].programs[2].zone_programs[8].set_as_linked_zone(_primary_zone=6)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=9)
            self.config.BaseStation3200[1].programs[2].zone_programs[9].set_as_linked_zone(_primary_zone=6)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=10)
            self.config.BaseStation3200[1].programs[2].zone_programs[10].set_as_linked_zone(_primary_zone=6)

            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=11)
            self.config.BaseStation3200[1].programs[3].zone_programs[11].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[3].zone_programs[11].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[3].zone_programs[11].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[3].zone_programs[11].set_soak_time(_minutes=10)
            self.config.BaseStation3200[1].programs[3].zone_programs[11].add_moisture_sensor_to_primary_zone(
                _moisture_sensor_address=1)
            self.config.BaseStation3200[1].programs[3].zone_programs[11].set_water_strategy_to_lower_limit()
            self.config.BaseStation3200[1].programs[3].zone_programs[11].set_lower_limit_threshold(_percent=35.0)
            self.config.BaseStation3200[1].programs[3].zone_programs[11].set_one_time_calibration_cycle()

            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=12)
            self.config.BaseStation3200[1].programs[3].zone_programs[12].set_as_linked_zone(_primary_zone=11,
                                                                                            _tracking_ratio=150)

            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=13)
            self.config.BaseStation3200[1].programs[3].zone_programs[13].set_as_linked_zone(_primary_zone=11)

            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=14)
            self.config.BaseStation3200[1].programs[3].zone_programs[14].set_as_linked_zone(_primary_zone=11)

            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=15)
            self.config.BaseStation3200[1].programs[3].zone_programs[15].set_as_linked_zone(_primary_zone=11)

            self.config.BaseStation3200[1].programs[4].add_zone_to_program(_zone_address=16)
            self.config.BaseStation3200[1].programs[4].zone_programs[16].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[4].zone_programs[16].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[4].zone_programs[16].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[4].zone_programs[16].set_soak_time(_minutes=10)
            self.config.BaseStation3200[1].programs[4].zone_programs[16].add_moisture_sensor_to_primary_zone(
                _moisture_sensor_address=1)
            self.config.BaseStation3200[1].programs[4].zone_programs[16].set_water_strategy_to_lower_limit()
            self.config.BaseStation3200[1].programs[4].zone_programs[16].set_lower_limit_threshold(_percent=35.0)
            self.config.BaseStation3200[1].programs[4].zone_programs[16].set_one_time_calibration_cycle()

            self.config.BaseStation3200[1].programs[4].add_zone_to_program(_zone_address=17)
            self.config.BaseStation3200[1].programs[4].zone_programs[17].set_as_linked_zone(_primary_zone=16)

            self.config.BaseStation3200[1].programs[4].add_zone_to_program(_zone_address=18)
            self.config.BaseStation3200[1].programs[4].zone_programs[18].set_as_linked_zone(_primary_zone=16)

            self.config.BaseStation3200[1].programs[4].add_zone_to_program(_zone_address=19)
            self.config.BaseStation3200[1].programs[4].zone_programs[19].set_as_linked_zone(_primary_zone=16,
                                                                                            _tracking_ratio=50)

            self.config.BaseStation3200[1].programs[4].add_zone_to_program(_zone_address=20)
            self.config.BaseStation3200[1].programs[4].zone_programs[20].set_as_linked_zone(_primary_zone=16)

            self.config.BaseStation3200[1].programs[5].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[5].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[5].zone_programs[1].set_run_time(_minutes=30)
            self.config.BaseStation3200[1].programs[5].zone_programs[1].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[5].zone_programs[1].set_soak_time(_minutes=10)
            self.config.BaseStation3200[1].programs[5].zone_programs[1].add_moisture_sensor_to_primary_zone(
                _moisture_sensor_address=2)
            self.config.BaseStation3200[1].programs[5].zone_programs[1].set_water_strategy_to_lower_limit()
            self.config.BaseStation3200[1].programs[5].zone_programs[1].set_lower_limit_threshold(_percent=35.0)
            self.config.BaseStation3200[1].programs[5].zone_programs[1].set_one_time_calibration_cycle()

            self.config.BaseStation3200[1].programs[5].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[5].zone_programs[2].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[5].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[5].zone_programs[3].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[5].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[5].zone_programs[4].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[5].add_zone_to_program(_zone_address=5)
            self.config.BaseStation3200[1].programs[5].zone_programs[5].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[6].add_zone_to_program(_zone_address=6)
            self.config.BaseStation3200[1].programs[6].zone_programs[6].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[6].zone_programs[6].set_run_time(_minutes=30)
            self.config.BaseStation3200[1].programs[6].zone_programs[6].set_cycle_time(_minutes=6)
            self.config.BaseStation3200[1].programs[6].zone_programs[6].set_soak_time(_minutes=10)
            self.config.BaseStation3200[1].programs[6].zone_programs[6].add_moisture_sensor_to_primary_zone(
                _moisture_sensor_address=2)
            self.config.BaseStation3200[1].programs[6].zone_programs[6].set_water_strategy_to_lower_limit()
            self.config.BaseStation3200[1].programs[6].zone_programs[6].set_lower_limit_threshold(_percent=35.0)
            self.config.BaseStation3200[1].programs[6].zone_programs[6].set_one_time_calibration_cycle()

            self.config.BaseStation3200[1].programs[6].add_zone_to_program(_zone_address=7)
            self.config.BaseStation3200[1].programs[6].zone_programs[7].set_as_linked_zone(_primary_zone=6)

            self.config.BaseStation3200[1].programs[6].add_zone_to_program(_zone_address=8)
            self.config.BaseStation3200[1].programs[6].zone_programs[8].set_as_linked_zone(_primary_zone=6)

            self.config.BaseStation3200[1].programs[6].add_zone_to_program(_zone_address=9)
            self.config.BaseStation3200[1].programs[6].zone_programs[9].set_as_linked_zone(_primary_zone=6)

            self.config.BaseStation3200[1].programs[6].add_zone_to_program(_zone_address=10)
            self.config.BaseStation3200[1].programs[6].zone_programs[10].set_as_linked_zone(_primary_zone=6)

            self.config.BaseStation3200[1].programs[7].add_zone_to_program(_zone_address=11)
            self.config.BaseStation3200[1].programs[7].zone_programs[11].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[7].zone_programs[11].set_run_time(_minutes=30)
            self.config.BaseStation3200[1].programs[7].zone_programs[11].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[7].zone_programs[11].set_soak_time(_minutes=10)
            self.config.BaseStation3200[1].programs[7].zone_programs[11].add_moisture_sensor_to_primary_zone(
                _moisture_sensor_address=2)
            self.config.BaseStation3200[1].programs[7].zone_programs[11].set_water_strategy_to_lower_limit()
            self.config.BaseStation3200[1].programs[7].zone_programs[11].set_lower_limit_threshold(_percent=35.0)
            self.config.BaseStation3200[1].programs[7].zone_programs[11].set_one_time_calibration_cycle()

            self.config.BaseStation3200[1].programs[7].add_zone_to_program(_zone_address=12)
            self.config.BaseStation3200[1].programs[7].zone_programs[12].set_as_linked_zone(_primary_zone=11)

            self.config.BaseStation3200[1].programs[7].add_zone_to_program(_zone_address=13)
            self.config.BaseStation3200[1].programs[7].zone_programs[13].set_as_linked_zone(_primary_zone=11)

            self.config.BaseStation3200[1].programs[7].add_zone_to_program(_zone_address=14)
            self.config.BaseStation3200[1].programs[7].zone_programs[14].set_as_linked_zone(_primary_zone=11)

            self.config.BaseStation3200[1].programs[7].add_zone_to_program(_zone_address=15)
            self.config.BaseStation3200[1].programs[7].zone_programs[15].set_as_linked_zone(_primary_zone=11)

            self.config.BaseStation3200[1].programs[8].add_zone_to_program(_zone_address=16)
            self.config.BaseStation3200[1].programs[8].zone_programs[16].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[8].zone_programs[16].set_run_time(_minutes=30)
            self.config.BaseStation3200[1].programs[8].zone_programs[16].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[8].zone_programs[16].set_soak_time(_minutes=10)
            self.config.BaseStation3200[1].programs[8].zone_programs[16].add_moisture_sensor_to_primary_zone(
                _moisture_sensor_address=2)
            self.config.BaseStation3200[1].programs[8].zone_programs[16].set_water_strategy_to_lower_limit()
            self.config.BaseStation3200[1].programs[8].zone_programs[16].set_lower_limit_threshold(_percent=35.0)
            self.config.BaseStation3200[1].programs[8].zone_programs[16].set_one_time_calibration_cycle()

            self.config.BaseStation3200[1].programs[8].add_zone_to_program(_zone_address=17)
            self.config.BaseStation3200[1].programs[8].zone_programs[17].set_as_linked_zone(_primary_zone=16)

            self.config.BaseStation3200[1].programs[8].add_zone_to_program(_zone_address=18)
            self.config.BaseStation3200[1].programs[8].zone_programs[18].set_as_linked_zone(_primary_zone=16)

            self.config.BaseStation3200[1].programs[8].add_zone_to_program(_zone_address=19)
            self.config.BaseStation3200[1].programs[8].zone_programs[19].set_as_linked_zone(_primary_zone=16)

            self.config.BaseStation3200[1].programs[8].add_zone_to_program(_zone_address=20)
            self.config.BaseStation3200[1].programs[8].zone_programs[20].set_as_linked_zone(_primary_zone=16)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        - Setup Zones: \n
            - ZN 1-10: \n
                - Design Flow: 10 \n
            - ZN 11-15: \n
                - Design Flow: 30 \n
            - ZN 16-20: \n
                - Design Flow: 50 \n
            - ZN 20: \n
                - Disable Zone \n
        - Setup Moisture Sensors: \n
            - MS 1 (SB05308): \n
                - Moisture Value: 10.0 \n
            - MS 2 (SB07258(: \n
                - Moisture Value: 26.0
        - Setup Temperature Sensors: \n
            - TS 1 (TAT0001): \n
                - Temperature Value: 33.0 \n
        - Setup Event Switch: \n
            - SW 1 (TPD0001): \n
                - Closed \n
        - Setup Master Valves: \n
            - MV 1 (TSD0005): \n
                - Normally Closed \n
            - MV 2 (TMV0003): \n
                - Normally Closed \n
            - MV 8 (TMV0004): \n
                - Normally Open \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Zones
            for zone in range(1, 11):
                self.config.BaseStation3200[1].zones[zone].set_design_flow(_gallons_per_minute=10)
            for zone in range(11, 16):
                self.config.BaseStation3200[1].zones[zone].set_design_flow(_gallons_per_minute=30)
            for zone in range(16, 21):
                self.config.BaseStation3200[1].zones[zone].set_design_flow(_gallons_per_minute=50)

            self.config.BaseStation3200[1].zones[20].set_disabled()

            # Moisture Sensors
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=10.0)
            self.config.BaseStation3200[1].moisture_sensors[2].bicoder.set_moisture_percent(_percent=26.0)

            # Temperature Sensor
            self.config.BaseStation3200[1].temperature_sensors[1].bicoder.set_temperature_reading(_degrees=33.0)

            # Event Switches
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_closed()

            # Master Valves
            self.config.BaseStation3200[1].master_valves[1].set_booster_enabled()
            self.config.BaseStation3200[1].master_valves[1].set_normally_open_state(_normally_open=opcodes.false)
            self.config.BaseStation3200[1].master_valves[2].set_normally_open_state(_normally_open=opcodes.false)
            self.config.BaseStation3200[1].master_valves[8].set_normally_open_state(_normally_open=opcodes.true)

            # Flow Meters
            self.config.BaseStation3200[1].flow_meters[1].set_enabled()
            self.config.BaseStation3200[1].flow_meters[2].set_enabled()
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_k_value(_value=3.10)
            self.config.BaseStation3200[1].flow_meters[2].bicoder.set_k_value(_value=5.01)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=0)
            self.config.BaseStation3200[1].flow_meters[2].bicoder.set_flow_rate(_gallons_per_minute=0)
            
            # # UPDATE - 1/10/18 - Ben:
            # #   Had to add this in order to get the initial status verification to work on step 10 (verifying that
            # #   MV 8 is watering because it is set to Normally Open.
            # self.config.BaseStation3200[1].do_increment_clock(minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Set up WS 1 \n
            - Enable WS 1 \n
            - Assign WS 1 a target flow of 500 \n
            - Set WS priority to 2-medium \n
            - Set water budget to 100000 and enable the water budget shut down \n
            - Enable water rationing \n
        - Set up WS 8 \n
            - Enable WS 8 \n
            - Assign WS 8 a target flow of 50 \n
            - Set WS priority to 3-low \n
            - Set water budget to 1000 and disable water budget shut down \n
            - Disable water rationing \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Water Source 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[1].set_monthly_watering_budget(_budget=100000,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].water_sources[1].set_water_rationing_to_enabled()

            # Water Source 8
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=8)
            self.config.BaseStation3200[1].water_sources[8].set_enabled()
            self.config.BaseStation3200[1].water_sources[8].set_priority(_priority_for_water_source=3)
            self.config.BaseStation3200[1].water_sources[8].set_monthly_watering_budget(_budget=1000,
                                                                                        _with_shutdown_enabled=False)
            self.config.BaseStation3200[1].water_sources[8].set_water_rationing_to_disabled()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n
        - Set up POC 1 \n
            - Enable POC 1 \n
            - Assign master valve TMV0003 and flow meter TWF0003 to POC 1 \n
            - Assign POC 1 a target flow of 500 \n
            - Assign POC 1 to main line 1 \n
            - Set POC priority to 2-medium \n
            - Set high flow limit to 550 and enable high flow shut down \n
            - Set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            - Set water budget to 100000 and enable the water budget shut down \n
            - Enable water rationing \n
        - Set up POC 8 \n
            - Enable POC 8 \n
            - Assign master valve TMV0004 and flow meter TWF0004 to POC 8 \n
            - Assign POC 8 a target flow of 50 \n
            - Assign POC 8 to main line 8 \n
            - Set POC priority to 3-low \n
            - Set high flow limit to 75 and disable high flow shut down \n
            - Set unscheduled flow limit to 5 and disable unscheduled flow shut down \n
            - Set water budget to 1000 and disable water budget shut down \n
            - Disable water rationing \n

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # Add & Configure POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=2)
            self.config.BaseStation3200[1].points_of_control[1].add_flow_meter_to_point_of_control(
                _flow_meter_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].points_of_control[1].set_high_flow_limit(_limit=550,
                                                                                    with_shutdown_enabled=True)
            self.config.BaseStation3200[1].points_of_control[1].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=True)
            # Add POC 1 to Water Source 1
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=1)

            # Add & Configure POC 8
            self.config.BaseStation3200[1].add_point_of_control_to_controller(
                _point_of_control_address=8)
            self.config.BaseStation3200[1].points_of_control[8].set_enabled()
            self.config.BaseStation3200[1].points_of_control[8].add_master_valve_to_point_of_control(
                _master_valve_address=8)
            self.config.BaseStation3200[1].points_of_control[8].add_flow_meter_to_point_of_control(
                _flow_meter_address=2)
            self.config.BaseStation3200[1].points_of_control[8].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].points_of_control[8].set_high_flow_limit(_limit=75,
                                                                                    with_shutdown_enabled=False)
            self.config.BaseStation3200[1].points_of_control[8].set_unscheduled_flow_limit(_gallons=5,
                                                                                           with_shutdown_enabled=False)
            # Add POC 8 to Water Source 8
            self.config.BaseStation3200[1].water_sources[8].add_point_of_control_to_water_source(
                _point_of_control_address=8)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control
        - Set up main line 1 \n
            - Set limit zones by flow to true \n
            - Set the pipe fill time to 4 minutes (The conversion is done in our objects to convert to 240 seconds) \n
            - Set the target flow to 500 \n
            - Set the high variance limit to 5% and enable the high variance shut down \n
            - Set the low variance limit to 20% and enable the low variance shut down \n
        \n
        - Set the Mainline on each program to be 1. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # Add & Configure ML 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=4)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[1].set_high_flow_variance_tier_four(_percent=5,
                                                                                         _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].mainlines[1].set_low_flow_variance_tier_four(_percent=20,
                                                                                        _with_shutdown_enabled=False)
            # Add ML 1 to POC 1
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)
            # Add ML 1 to POC 8
            self.config.BaseStation3200[1].points_of_control[8].add_mainline_to_point_of_control(_mainline_address=1)

            # Add all zones to mainline 1
            for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=zone_address)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        - Create Start/Stop/Pause Conditions. \n
        - Start Conditions: \n
            - Temperature Sensor 1: Lower Limit Threshold 60.0. \n
        - Pause Conditions: \n
            - Event Switch 1: When switch is opened. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].programs[3].add_temperature_stop_condition(_temperature_sensor_address=1)
            self.config.BaseStation3200[1].programs[3].temperature_stop_conditions[1]\
                .set_temperature_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[3].temperature_stop_conditions[1].set_temperature_threshold(
                _degrees=66)
            self.config.BaseStation3200[1].programs[8].add_switch_pause_condition(_event_switch_address=1)
            self.config.BaseStation3200[1].programs[8].event_switch_pause_conditions[1].set_switch_mode_to_open()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        - Test all devices. \n
        - Verify the full configuration that was set up. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Test Devices
            for zone in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone].bicoder.self_test_and_update_object_attributes()
            for ms in sorted(self.config.BaseStation3200[1].moisture_sensors.keys()):
                self.config.BaseStation3200[1].moisture_sensors[ms].bicoder.self_test_and_update_object_attributes()
            for ts in sorted(self.config.BaseStation3200[1].temperature_sensors.keys()):
                self.config.BaseStation3200[1].temperature_sensors[ts].bicoder.self_test_and_update_object_attributes()
            for mv in sorted(self.config.BaseStation3200[1].master_valves.keys()):
                self.config.BaseStation3200[1].master_valves[mv].bicoder.self_test_and_update_object_attributes()
            for fm in sorted(self.config.BaseStation3200[1].flow_meters.keys()):
                self.config.BaseStation3200[1].flow_meters[fm].bicoder.do_self_test()
            for sw in sorted(self.config.BaseStation3200[1].event_switches.keys()):
                self.config.BaseStation3200[1].event_switches[sw].bicoder.do_self_test()
            for pump in sorted(self.config.BaseStation3200[1].pumps.keys()):
                self.config.BaseStation3200[1].pumps[pump].bicoder.self_test_and_update_object_attributes()
            for pressure_sensor in sorted(self.config.BaseStation3200[1].pressure_sensors.keys()):
                self.config.BaseStation3200[1].pressure_sensors[pressure_sensor].\
                    bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller agaist information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        - Set the date and time. \n
        - Verify that all zones have a 'done' status. \n
            - Except Zone 20, which should be disabled. \n
        - Verify that all Programs have a 'done' status. \n
        - Verify that only the master valve that is normally open is 'watering'. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # make sure all programs are not running
            for program in [1, 2, 3, 4, 5, 6, 7, 8]:
                self.config.BaseStation3200[1].programs[program].set_program_to_stop()

            self.config.BaseStation3200[1].set_date_and_time(_date='04/08/2014', _time='08:44:00')
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Get data from each Zone, Master Valve, and Program on the controller
            # Verify that all zones are done watering, except zone 20 which is disabled
            for zone in range(1, 20):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].zones[20].statuses.verify_status_is_disabled()

            for pg in range(1, 9):
                self.config.BaseStation3200[1].programs[pg].statuses.verify_status_is_done()

            # Verify that only the master valve that is 'normally open' is watering
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].master_valves[8].statuses.verify_status_is_watering()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        - The date was set to 4/8/2014 \n
        - The water window for all programming until 11:00 am all Programs have a start time of 9:00am \n
        - Increment clock 135 minutes to cross the start time of 9:00 am adn allow for the water window to open back up
        - When the water window opens at 11:00 all Programs will try to water \n
        - Watering is taking place on a tuesday \n
        - Watering is taking place on an even day \n
        - Which means: \n
            - Program 1 should water because it is set to water all 7 days in a week it should run 5 zones because
              that is the max concurrence adn it hasn't hit the 500gpm flow rate which is restricted on poc 1 \n
            - Program 2 should not water because it is set to water mon, wen, fri \n
            - Program 3 should not water because it is set to water odd days and it's an even \n
            - Program 4 should water because it is set to water even days it should run 5 zones 16-20 because that is
              the max concurrence an it hasn't hit the 500gpm flow rate which is restricted on poc 1 but zone 20 is
              disabled so it will only water 4 \n
            - Program 5 should water because it is set to water all 7 days in a week it is not watering any zones because
              they are watering on program 1 \n
            - Program 6 should water because it is set to water all 7 days in a week zones 6 through 10 are set to watering
              because the total concurrent zones for the controller is set to 15 \n
            - Program 7 should water because it is set to water all 7 days in a week 1 zone get set to watering and all the
              other zone should be set to waiting because the total concurrent zones for the controller is set to 15 and
              has been met \n
            - Program 8 should water because it is set to water all 7 days in a week all zone should be set to waiting
              because the total concurrent zones for the controller is set to 15 \n
        - Statuses: \n
            - Zone 1-5: Watering because of program 1 \n
            - Zone 6-10: Watering because of program 6 \n
            - Zone 11: Watering because program 7, and program 4 is only watering 4 zones because ZN 20 is disabled \n
            - Zone 12-15: Waiting to water because of program 7 (Max Concurrent Zones is 15 on controller) \n
            - Zone 16-19: Watering because of program 4 \n
            - Zone 20: Disabled \n

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment clock from 8:45am to 9:05am this here so the clock increments past the water window closing
            self.config.BaseStation3200[1].do_increment_clock(minutes=20)
            self.config.BaseStation3200[1].set_date_and_time(_date='04/08/2014', _time='10:45:00')
            
            # Increment clock from 10:45a to 11:01a to enter available water window
            self.config.BaseStation3200[1].do_increment_clock(minutes=16)  # 11:01

            # --------------------#
            # Verify all Programs #
            # --------------------#

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[8].statuses.verify_status_is_waiting_to_run()

            # -----------------#
            # Verify all Zones #
            # -----------------#

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[19].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[20].statuses.verify_status_is_disabled()

            # -------------------------#
            # Verify all master valves #
            # -------------------------#
            
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].master_valves[8].statuses.verify_status_is_watering()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        - Increment 3 minutes \n
        - Zone 19 should be set to soaking because in only has a three minute cycle time do to the fact it is set to
          50% of the primary zone \n
        - Statuses: \n
            - Zone 1-5: Watering because of program 1 \n
            - Zone 6-10: Watering because of program 6 \n
            - Zone 11: Watering because program 7, and program 4 is only watering 4 zones because ZN 20 is disabled \n
            - Zone 12: Watering because program 7, ZN 19 on program 4 went to soaking, allowing this zone to run \n
            - Zone 13-15: Waiting to water because of program 7 (Max Concurrent Zones is 15 on controller) \n
            - Zone 16-18: Watering because of program 4 \n
            - Zone 19: Soaking because it has a tracking ratio of 50% \n
            - Zone 20: Disabled \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment by 3 minutes -> 11:04am
            self.config.BaseStation3200[1].do_increment_clock(minutes=3)

            # --------------------#
            # Verify all Programs #
            # --------------------#
            
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[8].statuses.verify_status_is_waiting_to_run()

            # -----------------#
            # Verify all Zones #
            # -----------------#
            
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_waiting_to_water()
            
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[19].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[20].statuses.verify_status_is_disabled()
            
            # -------------------------#
            # Verify all master valves #
            # -------------------------#

            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].master_valves[8].statuses.verify_status_is_watering()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        - Increment 1 minutes \n
        - Not all 15 zones can  run concurrently because of the design flow and zones changing into a soaking state \n
        - Statuses: \n
            - Zone 1-5: Watering because of program 1 \n
            - Zone 6-10: Soaking because of program 6, they ran for their cycle time \n
            - Zone 11: Soaking because program 7, it was watering previously because of the the disabled zone freed
                       up a spot in the 15 allowed concurrent zones \n
            - Zone 12: Watering because program 7, ZN 19 on program 4 went to soaking, allowing this zone to run \n
            - Zone 13-15: Watering because of program 7, and other other zones went to soaking \n
            - Zone 16-19: Watering because of program 4 \n
            - Zone 20: Disabled \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment by 3 minutes -> 11:07a
            self.config.BaseStation3200[1].do_increment_clock(minutes=3)

            # --------------------#
            # Verify all Programs #
            # --------------------#
            
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[8].statuses.verify_status_is_running()

            # -----------------#
            # Verify all Zones #
            # -----------------#

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_soaking()

            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[19].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[20].statuses.verify_status_is_disabled()

            # -------------------------#
            # Verify all master valves #
            # -------------------------#
            
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].master_valves[8].statuses.verify_status_is_watering()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
