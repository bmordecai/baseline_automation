import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

from common import helper_methods

__author__ = "Eldin"


class ControllerUseCase29(object):
    """
    Test name: \n
        Event Decoders \n
    Purpose: \n
        To set the start/stop/pause condition on Programs using an event decoder and verifying that the program and its
        zones have the proper status when conditions are triggered. \n
    Coverage area: \n
        - Able to search and find event decoders \n
        - Able to Start 1 program on open contacts and stop the same program on closed contacts \n
        - Able to Start 1 program on closed contacts and stop the same program on open contacts \n
        - Able to Start a 1 program and have an event switch pause the program on open it for a period of time then resume
        - watering \n
        - Able to start 1 program and have an event switch pause the same program on closed for a period of time then
          resume watering \n
        - Not in test Able to stop all Programs on an open Event switch \n
        - Able to stop all Programs on a closed event switch \n
        - Able to pause all Programs on an open Event switch and resume watering on a closed switch \n
        - Not in test Able to pause all Programs on a closed event switch and resume watering on an open switch \n
    """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                     # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        # These are set in the PG3200 object
        program_start_time_8am = [480]

        program_week_day_every_day = [1, 1, 1, 1, 1, 1, 1]

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=3)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(
                _sun=True, _mon=True, _tues=True, _wed=True, _thurs=True, _fri=True, _sat=True
            )
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_start_time_8am)

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation3200[1].programs[2].set_enabled()
            self.config.BaseStation3200[1].programs[2].set_max_concurrent_zones(_number_of_zones=3)
            self.config.BaseStation3200[1].programs[2].set_watering_intervals_to_selected_days_of_the_week(
                _sun=True, _mon=True, _tues=True, _wed=True, _thurs=True, _fri=True, _sat=True
            )
            self.config.BaseStation3200[1].programs[2].set_start_times(_st_list=program_start_time_8am)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=45)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=30)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=4)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[1].zone_programs[4].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=5)
            self.config.BaseStation3200[1].programs[2].zone_programs[5].set_run_time(_minutes=45)
            self.config.BaseStation3200[1].programs[2].zone_programs[5].set_cycle_time(_minutes=30)
            self.config.BaseStation3200[1].programs[2].zone_programs[5].set_soak_time(_minutes=4)
            self.config.BaseStation3200[1].programs[2].zone_programs[5].set_as_primary_zone()

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=6)
            self.config.BaseStation3200[1].programs[2].zone_programs[6].set_as_linked_zone(_primary_zone=5)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=7)
            self.config.BaseStation3200[1].programs[2].zone_programs[7].set_as_linked_zone(_primary_zone=5)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=8)
            self.config.BaseStation3200[1].programs[2].zone_programs[8].set_as_linked_zone(_primary_zone=5)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        - Set event switch to start and stop a program \n
            - Set event switch TPD0001 to start program 1 on closed contacts \n
            - Set event switch TPD0001 to stop program 1 on open contacts \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            self.config.BaseStation3200[1].programs[1].add_switch_start_condition(_event_switch_address=1)
            self.config.BaseStation3200[1].programs[1].event_switch_start_conditions[1].set_switch_mode_to_closed()

            self.config.BaseStation3200[1].programs[1].add_switch_stop_condition(_event_switch_address=1)
            self.config.BaseStation3200[1].programs[1].event_switch_stop_conditions[1].set_switch_mode_to_open()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        - Set the date and time on the controller to be 04/08/2014 07:00:00. \n
        - The time is now 07:00:00. \n
        - Get the data of each zone and program on the controller. \n
        - Verify Statuses: \n
            - Program 1: Done Watering because the time hasn't been incremented to trigger a start condition. \n
                - Zone 1: Done Watering because the time hasn't been incremented to trigger a start condition. \n
                - Zone 2: Done Watering because the time hasn't been incremented to trigger a start condition. \n
                - Zone 3: Done Watering because the time hasn't been incremented to trigger a start condition. \n
                - Zone 4: Done Watering because the time hasn't been incremented to trigger a start condition. \n
            - Program 2: Done Watering because the time hasn't been incremented to trigger a start condition. \n
                - Zone 5: Done Watering because the time hasn't been incremented to trigger a start condition. \n
                - Zone 6: Done Watering because the time hasn't been incremented to trigger a start condition. \n
                - Zone 7: Done Watering because the time hasn't been incremented to trigger a start condition. \n
                - Zone 8: Done Watering because the time hasn't been incremented to trigger a start condition. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set the date and time
            self.config.BaseStation3200[1].set_date_and_time(_date="04/08/2014", _time="07:00:00")

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].verify_status_is_done()

            # Verify all Zones
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        - Manually start Program 2 so that both Programs will be running. \n
        - Event Switch 1 is closed, triggering the start condition of Program 1, causing it to run. \n
        - Increment the clock by 2 minutes. \n
        - The time is now 07:02:00. \n
        - Get the data of each zone and program on the controller. \n
        - Verify Statuses: \n
            - Program 1: Running because of Event Switch 1 being open.
                - Zone 1: Watering because the program it is attached to is running. \n
                - Zone 2: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 3: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 4: Waiting to water because max concurrent zones on the controller is 1. \n
            - Program 2: Waiting to water because it was manually triggered but max concurrent zones is 1. \n
                - Zone 5: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 6: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 7: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 8: Waiting to water because max concurrent zones on the controller is 1. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            self.config.BaseStation3200[1].programs[2].set_program_to_start()

            # Increment the time by two minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)

            # Verify all Programs and their zones
            self.config.BaseStation3200[1].programs[1].verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].programs[2].verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].zones[5].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[6].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[7].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[8].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        - Open Event Switch 1, therefore stopping program 1 because of its condition. \n
        - Increment the clock by 4 minutes. \n
        - The time is now 07:06:00. \n
        - Get the data of each zone and program on the controller. \n
        - Verify Statuses: \n
            - Program 1: Done because event switch 1 was opened, causing this program's stop condition to trigger. \n
                - Zone 1: Done Watering because the time hasn't been incremented to trigger a start condition. \n
                - Zone 2: Done Watering because the time hasn't been incremented to trigger a start condition. \n
                - Zone 3: Done Watering because the time hasn't been incremented to trigger a start condition. \n
                - Zone 4: Done Watering because the time hasn't been incremented to trigger a start condition. \n
            - Program 2:  Running because program 1 was stopped due to the condition. \n
                - Zone 5: Watering because the program it is attached to is running. \n
                - Zone 6: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 7: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 8: Waiting to water because max concurrent zones on the controller is 1. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set event switch 1 to open
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_open()

            # Increment the time by four minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)

            # Verify all Programs and their zones
            self.config.BaseStation3200[1].programs[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_done()

            self.config.BaseStation3200[1].programs[2].verify_status_is_running()
            self.config.BaseStation3200[1].zones[5].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[7].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[8].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        - Close Event Switch 1, therefore starting program 1 because of its condition. \n
        - Increment the clock by 2 minutes. \n
        - The time is now 07:08:00. \n
        - Get the data of each zone and program on the controller. \n
        - Verify Statuses: \n
            - Program 1: Waiting to water because the start condition for this program was triggered. \n
                - Zone 1: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 2: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 3: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 4: Waiting to water because max concurrent zones on the controller is 1. \n
            - Program 2:  Running because program 1 was stopped due to the condition. \n
                - Zone 5: Watering because the program it is attached to is running. \n
                - Zone 6: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 7: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 8: Waiting to water because max concurrent zones on the controller is 1. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set event switch 1 to closed
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_closed()

            # Increment the time by two minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)

            # Verify all Programs and their zones
            self.config.BaseStation3200[1].programs[1].verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].zones[1].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[2].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].programs[2].verify_status_is_running()
            self.config.BaseStation3200[1].zones[5].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[7].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[8].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        - Manually stop both Programs to reset the test. \n
        - Set event switch to start and stop a program \n
            - Set event switch TPD0001 to start program 1 on open contacts \n
            - Set event switch TPD0001 to stop program 1 on closed contacts \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Start Program 1 & 2s manually
            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()

            self.config.BaseStation3200[1].programs[1].event_switch_start_conditions[1].set_switch_mode_to_open()

            self.config.BaseStation3200[1].programs[1].event_switch_stop_conditions[1].set_switch_mode_to_closed()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        - Close Event Switch 1, therefore stopping program 1 because of its condition that we just changed. \n
        - Manually start both Programs. Only program 2 should actually start because of the stop condition on PG 1. \n
        - Increment the clock by 2 minutes. \n
        - The time is now 07:10:00. \n
        - Get the data of each zone and program on the controller. \n
        - Verify Statuses: \n
            - Program 1: Done because event switch 1 was opened, causing this program's stop condition to trigger. \n
                - Zone 1: Done Watering because the time hasn't been incremented to trigger a start condition. \n
                - Zone 2: Done Watering because the time hasn't been incremented to trigger a start condition. \n
                - Zone 3: Done Watering because the time hasn't been incremented to trigger a start condition. \n
                - Zone 4: Done Watering because the time hasn't been incremented to trigger a start condition. \n
            - Program 2:  Running because it was never stopped and its zones are still watering. \n
                - Zone 5: Watering because the program it is attached to is running. \n
                - Zone 6: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 7: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 8: Waiting to water because max concurrent zones on the controller is 1. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set event switch 1 to closed
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_closed()

            # Start Program 1 & 2s manually
            self.config.BaseStation3200[1].programs[1].set_program_to_start()
            self.config.BaseStation3200[1].programs[2].set_program_to_start()

            # Enable sim mode
            self.config.BaseStation3200[1].set_sim_mode_to_on()

            # Increment the time by two minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)

            # Verify all Programs and their zones
            self.config.BaseStation3200[1].programs[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_done()

            self.config.BaseStation3200[1].programs[2].verify_status_is_running()
            self.config.BaseStation3200[1].zones[5].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[7].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[8].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        - Open Event Switch 1, therefore starting program 1 because of its condition that we just changed. \n
        - Increment the clock by 4 minutes. \n
        - The time is now 07:14:00. \n
        - Get the data of each zone and program on the controller. \n
        - Verify Statuses: \n
            - Program 1: Waiting to water because the start condition for this program was triggered. \n
                - Zone 1: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 2: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 3: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 4: Waiting to water because max concurrent zones on the controller is 1. \n
            - Program 2:  Running because it was never stopped and its zones are still watering. \n
                - Zone 5: Watering because the program it is attached to is running. \n
                - Zone 6: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 7: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 8: Waiting to water because max concurrent zones on the controller is 1. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set event switch 1 to open
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_open()

            # Increment the time by four minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)

            # Verify all Programs and their zones
            self.config.BaseStation3200[1].programs[1].verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].zones[1].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[2].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].programs[2].verify_status_is_running()
            self.config.BaseStation3200[1].zones[5].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[7].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[8].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        - Close Event Switch 1, therefore stopping program 1 because of its condition that we just changed. \n
        - Increment the clock by 2 minutes. \n
        - The time is now 07:16:00. \n
        - Get the data of each zone and program on the controller. \n
        - Verify Statuses: \n
            - Program 1: Done because event switch 1 was closed, causing this program's stop condition to trigger. \n
                - Zone 1: Done Watering because the time hasn't been incremented to trigger a start condition. \n
                - Zone 2: Done Watering because the time hasn't been incremented to trigger a start condition. \n
                - Zone 3: Done Watering because the time hasn't been incremented to trigger a start condition. \n
                - Zone 4: Done Watering because the time hasn't been incremented to trigger a start condition. \n
            - Program 2:  Running because it was never stopped and its zones are still watering. \n
                - Zone 5: Watering because the program it is attached to is running. \n
                - Zone 6: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 7: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 8: Waiting to water because max concurrent zones on the controller is 1. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set event switch 1 to closed
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_closed()

            # Increment the time by two minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)

            # Verify all Programs and their zones
            self.config.BaseStation3200[1].programs[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_done()

            self.config.BaseStation3200[1].programs[2].verify_status_is_running()
            self.config.BaseStation3200[1].zones[5].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[7].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[8].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        - Manually stop both Programs so that the test is 'reset'. \n
        - Open Event Switch 1, therefore starting program 1 again. \n
        - Manually start both Programs so that both of them turn on. \n
        - Increment the clock by 2 minutes. \n
        - The time is now 07:18:00. \n
        - Get the data of each zone and program on the controller. \n
        - Verify Statuses: \n
            - Program 1: Waiting to water because the start condition for this program was triggered. \n
                - Zone 1: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 2: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 3: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 4: Waiting to water because max concurrent zones on the controller is 1. \n
            - Program 2:  Running because it was never stopped and its zones are still watering. \n
                - Zone 5: Watering because the program it is attached to is running. \n
                - Zone 6: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 7: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 8: Waiting to water because max concurrent zones on the controller is 1. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Start Program 1 & 2s manually
            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()

            # Set event switch 1 to open
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_open()

            # Start Program 1 & 2s manually
            self.config.BaseStation3200[1].programs[1].set_program_to_start()
            self.config.BaseStation3200[1].programs[2].set_program_to_start()

            # Increment the time by two minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)

            # Verify all Programs and their zones
            self.config.BaseStation3200[1].programs[1].verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].zones[1].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[2].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].programs[2].verify_status_is_running()
            self.config.BaseStation3200[1].zones[5].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[7].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[8].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_14(self):
        """
        - Manually stop both Programs to reset the test. \n
        - Set event switch to start and stop a program \n
            - Set event switch TPD0001 to never start program 1 \n
            - Set event switch TPD0001 to never stop program 1 \n
            - Set event switch TPD0001 to pause program 1 when it is open, and have a pause time of 5 minutes. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Start Program 1 & 2s manually
            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()

            self.config.BaseStation3200[1].programs[1].event_switch_start_conditions[1].set_switch_mode_to_off()

            self.config.BaseStation3200[1].programs[1].event_switch_stop_conditions[1].set_switch_mode_to_off()

            self.config.BaseStation3200[1].programs[1].add_switch_pause_condition(_event_switch_address=1)
            self.config.BaseStation3200[1].programs[1].event_switch_pause_conditions[1].set_switch_mode_to_open()
            self.config.BaseStation3200[1].programs[1].event_switch_pause_conditions[1].set_switch_pause_time(_minutes=5)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_15(self):
        """
        - Manually start both Programs so that they are both running. \n
        - Increment the clock by 1 minutes. \n
        - The time is now 07:19:00. \n
        - Get the data of each zone and program on the controller. \n
        - Verify Statuses: \n
            - Program 1: Paused because the event switch is open, triggering this Programs pause condition. \n
                - Zone 1: Paused because the program it is on is paused. \n
                - Zone 2: Paused because the program it is on is paused. \n
                - Zone 3: Paused because the program it is on is paused. \n
                - Zone 4: Paused because the program it is on is paused. \n
            - Program 2:  Running because it was manually triggered to run again. \n
                - Zone 5: Watering because the program it is attached to is running. \n
                - Zone 6: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 7: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 8: Waiting to water because max concurrent zones on the controller is 1. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Start Program 1 & 2s manually
            self.config.BaseStation3200[1].programs[1].set_program_to_start()
            self.config.BaseStation3200[1].programs[2].set_program_to_start()

            # Increment the time by one minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all Programs and their zones
            self.config.BaseStation3200[1].programs[1].verify_status_is_paused()
            self.config.BaseStation3200[1].zones[1].verify_status_is_paused()
            self.config.BaseStation3200[1].zones[2].verify_status_is_paused()
            self.config.BaseStation3200[1].zones[3].verify_status_is_paused()
            self.config.BaseStation3200[1].zones[4].verify_status_is_paused()

            self.config.BaseStation3200[1].programs[2].verify_status_is_running()
            self.config.BaseStation3200[1].zones[5].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[7].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[8].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_16(self):
        """
        - Close Event Switch 1, which should allow program 1 to come out of its pause state after pausing for time. \n
        - Increment the clock by 2 minutes. \n
        - The time is now 07:21:00. \n
        - Get the data of each zone and program on the controller. \n
        - Verify Statuses: \n
            - Program 1: Paused because it should be paused for 5 minutes. \n
                - Zone 1: Paused because the program it is on is paused. \n
                - Zone 2: Paused because the program it is on is paused. \n
                - Zone 3: Paused because the program it is on is paused. \n
                - Zone 4: Paused because the program it is on is paused. \n
            - Program 2:  Running because it was manually triggered to run again. \n
                - Zone 5: Watering because the program it is attached to is running. \n
                - Zone 6: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 7: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 8: Waiting to water because max concurrent zones on the controller is 1. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set event switch 1 to closed
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_closed()

            # Increment the time by two minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)
            # Verify all Programs and their zones
            self.config.BaseStation3200[1].programs[1].verify_status_is_paused()
            self.config.BaseStation3200[1].zones[1].verify_status_is_paused()
            self.config.BaseStation3200[1].zones[2].verify_status_is_paused()
            self.config.BaseStation3200[1].zones[3].verify_status_is_paused()
            self.config.BaseStation3200[1].zones[4].verify_status_is_paused()

            self.config.BaseStation3200[1].programs[2].verify_status_is_running()
            self.config.BaseStation3200[1].zones[5].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[7].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[8].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_17(self):
        """
        - Increment the clock by 4 minutes. \n
        - The time is now 07:24:00. \n
        - Get the data of each zone and program on the controller. \n
        - Verify Statuses: \n
            - Program 1: Waiting because it's pause time of 5 minutes has expired. \n
                - Zone 1: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 2: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 3: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 4: Waiting to water because max concurrent zones on the controller is 1. \n
            - Program 2:  Running because its zones haven't finished watering yet. \n
                - Zone 5: Watering because the program it is attached to is running. \n
                - Zone 6: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 7: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 8: Waiting to water because max concurrent zones on the controller is 1. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the time by four minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)

            # Verify all Programs and their zones
            self.config.BaseStation3200[1].programs[1].verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].zones[1].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[2].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].programs[2].verify_status_is_running()
            self.config.BaseStation3200[1].zones[5].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[7].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[8].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_18(self):
        """
        - Manually stop both Programs to reset the test. \n
        - Set event switch to start and stop a program \n
            - Set event switch TPD0001 to never start program 1 \n
            - Set event switch TPD0001 to stop all Programs on closed switch \n
            - Set event switch TPD0001 to never pause program 1 \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Start Program 1 & 2s manually
            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()

            # Hard coded for now because we don't have the logic to setup 'all' Programs on a condition
            self.config.BaseStation3200[1].ser.send_and_wait_for_reply('SET,PP=AL,SW=TPD0001,SM=OP')

            self.config.BaseStation3200[1].programs[1].event_switch_pause_conditions[1].set_switch_mode_to_off()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_19(self):
        """
        - Manually start both Programs so that they are both running. \n
        - Increment the clock by 1 minutes. \n
        - The time is now 07:25:00. \n
        - Get the data of each zone and program on the controller. \n
        - Verify Statuses: \n
            - Program 1: Running because it was manually triggered to run again. \n
                - Zone 1: Watering because the program it is attached to is running. \n
                - Zone 2: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 3: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 4: Waiting to water because max concurrent zones on the controller is 1. \n
            - Program 2: Waiting because it was triggered but can't run because max concurrent zones is 1. \n
                - Zone 5: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 6: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 7: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 8: Waiting to water because max concurrent zones on the controller is 1. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Start Program 1 & 2s manually
            self.config.BaseStation3200[1].programs[1].set_program_to_start()
            self.config.BaseStation3200[1].programs[2].set_program_to_start()

            # Increment the time by one minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all Programs and their zones
            self.config.BaseStation3200[1].programs[1].verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].programs[2].verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].zones[5].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[6].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[7].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[8].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_20(self):
        """
        - Open Event Switch 1, triggering a stop on all Programs. \n
        - Increment the clock by 3 minutes. \n
        - The time is now 07:28:00. \n
        - Get the data of each zone and program on the controller. \n
        - Verify Statuses: \n
            - Program 1: Done because event switch 1 was closed, triggering all program's stop condition. \n
                - Zone 1: Done because the program it is attached to was stopped due to a stop condition. \n
                - Zone 2: Done because the program it is attached to was stopped due to a stop condition. \n
                - Zone 3: Done because the program it is attached to was stopped due to a stop condition. \n
                - Zone 4: Done because the program it is attached to was stopped due to a stop condition. \n
            - Program 2: Done because event switch 1 was closed, triggering all program's stop condition. \n
                - Zone 5: Done because the program it is attached to was stopped due to a stop condition. \n
                - Zone 6: Done because the program it is attached to was stopped due to a stop condition. \n
                - Zone 7: Done because the program it is attached to was stopped due to a stop condition. \n
                - Zone 8: Done because the program it is attached to was stopped due to a stop condition. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set event switch 1 to closed
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_open()

            # Increment the time by three minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=3)

            # Verify all Programs and their zones
            self.config.BaseStation3200[1].programs[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_done()

            self.config.BaseStation3200[1].programs[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_21(self):
        """
        - Manually stop both Programs to reset the test. \n
        - Set event switch to start and stop a program \n
            - Set event switch TPD0001 to never start program 1 \n
            - Clear event switch TPD0001 condition that stops all Programs on an open switch \n
            - Set event switch TPD0001 to pause all Programs on an open switch. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Start Program 1 & 2s manually
            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()

            # Hard coded for now because we don't have the logic to setup 'all' Programs on a condition
            self.config.BaseStation3200[1].ser.send_and_wait_for_reply('SET,PP=AL,SW=TPD0001,SM=OF')
            self.config.BaseStation3200[1].ser.send_and_wait_for_reply('SET,PS=AL,SW=TPD0001,SM=OP,SP=2')
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_22(self):
        """
        - Set Event Switch 1 to closed \n
        - Manually start both Programs so that they are both running. \n
        - Increment the clock by 2 minutes. \n
        - The time is now 07:30:00. \n
        - Get the data of each zone and program on the controller. \n
        - Verify Statuses: \n
            - Program 1: Running because it was manually triggered to run again. \n
                - Zone 1: Watering because the program it is attached to is running. \n
                - Zone 2: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 3: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 4: Waiting to water because max concurrent zones on the controller is 1. \n
            - Program 2: Waiting because it was triggered but can't run because max concurrent zones is 1. \n
                - Zone 5: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 6: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 7: Waiting to water because max concurrent zones on the controller is 1. \n
                - Zone 8: Waiting to water because max concurrent zones on the controller is 1. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set event switch 1 to closed
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_closed()
            self.config.BaseStation3200[1].event_switches[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)

            # Start Program 1 & 2s manually
            self.config.BaseStation3200[1].programs[1].set_program_to_start()
            self.config.BaseStation3200[1].programs[2].set_program_to_start()

            # Increment the time by two minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)
            # Verify all Programs and their zones
            self.config.BaseStation3200[1].programs[1].verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].programs[2].verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].zones[5].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[6].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[7].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[8].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_23(self):
        """
        - Set Event Switch 1 to open, which will pause all Programs. \n
        - Manually start both Programs so that they are both running. \n
        - Increment the clock by 3 minutes. \n
        - The time is now 07:33:00. \n
        - Get the data of each zone and program on the controller. \n
        - Verify Statuses: \n
            - Program 1: Paused because event switch was opened which triggers a pause on all Programs. \n
                - Zone 1: Paused because the program it is was paused by an event switch. \n
                - Zone 2: Paused because the program it is was paused by an event switch. \n
                - Zone 3: Paused because the program it is was paused by an event switch. \n
                - Zone 4: Paused because the program it is was paused by an event switch. \n
            - Program 2: Paused because event switch was opened which triggers a pause on all Programs. \n
                - Zone 5: Paused because the program it is was paused by an event switch. \n
                - Zone 6: Paused because the program it is was paused by an event switch. \n
                - Zone 7: Paused because the program it is was paused by an event switch. \n
                - Zone 8: Paused because the program it is was paused by an event switch. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set event switch 1 to open
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_open()

            # Increment the time by three minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=3)
            # Verify all Programs and their zones
            self.config.BaseStation3200[1].programs[1].verify_status_is_paused()
            self.config.BaseStation3200[1].zones[1].verify_status_is_paused()
            self.config.BaseStation3200[1].zones[2].verify_status_is_paused()
            self.config.BaseStation3200[1].zones[3].verify_status_is_paused()
            self.config.BaseStation3200[1].zones[4].verify_status_is_paused()

            self.config.BaseStation3200[1].programs[2].verify_status_is_paused()
            self.config.BaseStation3200[1].zones[5].verify_status_is_paused()
            self.config.BaseStation3200[1].zones[6].verify_status_is_paused()
            self.config.BaseStation3200[1].zones[7].verify_status_is_paused()
            self.config.BaseStation3200[1].zones[8].verify_status_is_paused()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
