import csv
import os
import logging
import sys

from csv_handler import CSVWriter

from common.imports import opcodes, csv

from common import helper_methods
from common.configuration import Configuration

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr

# import log_handler for logging functionality
from common.logging_handler import log_handler

from datetime import time, timedelta, datetime, date
from time import sleep

__author__ = 'Tige'


class ControllerUseCase2(object):
    """
    Test name:
        - EPA Test Configuration
    purpose:
        - set up a the EPA test
            - setup 6 different zones using Weather based watering
            - verify that teh ETc runtime values change when the eto input value changes
            - verify that correct cycle and soak are performed
            - verify that controller starts watering when deficit is below 50%
            - verify that the controller meets a 80% efficacy rating

    Coverage area: \n
        -setting up devices:
            - Loading \n
            - Searching \n
            - Addressing \n
                - Setting:
                    - descriptions
        - setting Programs: \n
            - start times
            - watering days
            - water windows
            - zone concurrency
            - assign zones to Programs \n
            - set each zone to run with ETc run times \n
            - set each zone to have a cycle and soak based on zone properties\n
    Date References:
        - configuration for script is located common\configuration_files\update_cn.json
        - the devices and addresses range is read from the .json file

    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        controller_type = 'BaseStation3200'
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        # initializes the csv writer object so it can be used in the remainder of the use_case
        self.csv_writer = CSVWriter(file_name=test_name + controller_type,
                                    relative_path='bs3200_scripts',
                                    delimiter=',',
                                    line_terminator='\n')
        # Configures how the logging will appear in the rest of the use case
        # logging.basicConfig(filename=str(os.path.dirname(os.path.abspath(__file__))) + '/' + test_name + ".txt",
        #                     level=logging.INFO, filemode='w', format='%(levelname)s:%(message)s')
        try:
            self.csv_writer.open()
        except IOError:
            e_msg = self.csv_writer.fname + ' file is in use'
            raise IOError(e_msg)

        self.logger = logging.getLogger()
        self.hdlr = logging.FileHandler(str(os.path.dirname(os.path.abspath(__file__))) + '/' + test_name +
                                        controller_type + ".txt", mode="w")
        self.formatter = logging.Formatter('%(levelname)s: %(message)s')
        self.hdlr.setFormatter(self.formatter)
        self.logger.addHandler(self.hdlr)
        self.logger.setLevel(logging.INFO)
        self.run_use_case()

    def run_use_case(self):
        ###############################################################################################################
        # TODO: EXAMPLE USAGE WITH NEW WEATHER_BASE_WATERING 'PACKAGE'
        #
        # TODO: 1. USE THIS IMPORT AT THE TOP OF THE SCRIPT
        #   from common.epa_package import wbw_imports, ir_asa_resources
        #
        # TODO: 2. GETTING A SOIL TYPE
        #   clay_soil_type = wbw_imports.SoilTextureTypes.CLAY
        #
        # TODO: 3. ASA VALUE FOR CLAY WITH A 5% SLOPE:
        #   asa_value = ir_asa_resources.dict_of_allowable_surface_accumulation[clay_soil_type][0.05]
        # or
        #   asa_value = ir_asa_resources.dict_of_allowable_surface_accumulation[wbw_imports.SoilTextureTypes.CLAY][0.05]
        ###############################################################################################################
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)
                    self.config.load_eto_and_rain_fall_values()

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_2(self):
        """
        - Set all attribute for a zone the a client could specify
        - Set the new descriptions to match EPA test for each zone
            - set the zone attribute for soil type for each zone
            - set the zone attribute for slope for each zone
            - set the zone attribute for sun exposure for each zone
            - set the zone attribute for allowable depletion for each zone
            - set the zone attribute for root depth for each zone
            - set the zone precipitation rates for each zone for each zone
            - set the zone distribution uniformity for each zone for each zone
            - set the zone attribute for Gross are a for each zone
        - set crop coefficient for each zone
            - Set zone 1 is attribute for sun exposure shade tall Fescue
            - Set zone 2 is attribute for sun exposure Bermuda
            - Set zone 6 is attribute for sun exposure Bermuda
        - set the root water holding capacity for each zone
            - this uses an equation to get the root zone water holding capacity and then sends the value \n
              to the controller \n
        """
        #######################################
        # user input fields simulating coming from the client
        #######################################

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # set descriptions to match EPA test for each zone
            self.config.BaseStation3200[1].zones[1].set_description(_ds='Loam Soil')
            self.config.BaseStation3200[1].zones[2].set_description(_ds='Silty Clay Soil')
            self.config.BaseStation3200[1].zones[3].set_description(_ds='Loamy Sand Soil')
            self.config.BaseStation3200[1].zones[4].set_description(_ds='Sandy Loam Soil')
            self.config.BaseStation3200[1].zones[5].set_description(_ds='Clay Loam soil')
            self.config.BaseStation3200[1].zones[6].set_description(_ds='Clay soil')

            # set the zone attribute for soil type
            self.config.BaseStation3200[1].zones[1].soil_type = "Loam"
            self.config.BaseStation3200[1].zones[2].soil_type = "Silty Clay"
            self.config.BaseStation3200[1].zones[3].soil_type = "Loamy Sand"
            self.config.BaseStation3200[1].zones[4].soil_type = "Sandy Loam"
            self.config.BaseStation3200[1].zones[5].soil_type = "Clay Loam"
            self.config.BaseStation3200[1].zones[6].soil_type = "Clay"

            # set the zone attribute for slope
            self.config.BaseStation3200[1].zones[1].slope = .06
            self.config.BaseStation3200[1].zones[2].slope = .10
            self.config.BaseStation3200[1].zones[3].slope = .08
            self.config.BaseStation3200[1].zones[4].slope = .12
            self.config.BaseStation3200[1].zones[5].slope = .02
            self.config.BaseStation3200[1].zones[6].slope = .20

            # set the zone attribute for sun exposure
            self.config.BaseStation3200[1].zones[1].sun_exposure = .75
            self.config.BaseStation3200[1].zones[2].sun_exposure = 1.00
            self.config.BaseStation3200[1].zones[3].sun_exposure = 1.00
            self.config.BaseStation3200[1].zones[4].sun_exposure = .50
            self.config.BaseStation3200[1].zones[5].sun_exposure = 1.00
            self.config.BaseStation3200[1].zones[6].sun_exposure = 1.00

            # set the zone attribute for allowable depletion
            self.config.BaseStation3200[1].zones[1].allowable_depletion = .50
            self.config.BaseStation3200[1].zones[2].allowable_depletion = .40
            self.config.BaseStation3200[1].zones[3].allowable_depletion = .50
            self.config.BaseStation3200[1].zones[4].allowable_depletion = .55
            self.config.BaseStation3200[1].zones[5].allowable_depletion = .50
            self.config.BaseStation3200[1].zones[6].allowable_depletion = .35

            # set the zone attribute for root depth
            self.config.BaseStation3200[1].zones[1].root_depth = 10.0
            self.config.BaseStation3200[1].zones[2].root_depth = 8.1
            self.config.BaseStation3200[1].zones[3].root_depth = 20.0
            self.config.BaseStation3200[1].zones[4].root_depth = 28.0
            self.config.BaseStation3200[1].zones[5].root_depth = 25.0
            self.config.BaseStation3200[1].zones[6].root_depth = 9.2

            # set zone precipitation rates for each zone
            self.config.BaseStation3200[1].zones[1].set_precipitation_rate_value(_pr_value=1.60)
            self.config.BaseStation3200[1].zones[2].set_precipitation_rate_value(_pr_value=1.60)
            self.config.BaseStation3200[1].zones[3].set_precipitation_rate_value(_pr_value=1.40)
            self.config.BaseStation3200[1].zones[4].set_precipitation_rate_value(_pr_value=1.40)
            self.config.BaseStation3200[1].zones[5].set_precipitation_rate_value(_pr_value=0.20)
            self.config.BaseStation3200[1].zones[6].set_precipitation_rate_value(_pr_value=0.35)

            # set the zone attribute for distribution uniformity
            self.config.BaseStation3200[1].zones[1].set_distribution_uniformity_value(_du_value=55)
            self.config.BaseStation3200[1].zones[2].set_distribution_uniformity_value(_du_value=60)
            self.config.BaseStation3200[1].zones[3].set_distribution_uniformity_value(_du_value=70)
            self.config.BaseStation3200[1].zones[4].set_distribution_uniformity_value(_du_value=75)
            self.config.BaseStation3200[1].zones[5].set_distribution_uniformity_value(_du_value=80)
            self.config.BaseStation3200[1].zones[6].set_distribution_uniformity_value(_du_value=65)

            # set the zone attribute for Gross area
            self.config.BaseStation3200[1].zones[1].gross_area = 1000
            self.config.BaseStation3200[1].zones[2].gross_area = 1200
            self.config.BaseStation3200[1].zones[3].gross_area = 800
            self.config.BaseStation3200[1].zones[4].gross_area = 500
            self.config.BaseStation3200[1].zones[5].gross_area = 650
            self.config.BaseStation3200[1].zones[6].gross_area = 1600

            # this has to come from some table associated back to a date
            self.config.BaseStation3200[1].zones[1].set_crop_coefficient_value(_kc_value=0.50)

            # Set zone 2 is full sun Bermuda
            self.config.BaseStation3200[1].zones[2].set_crop_coefficient_value(_kc_value=0.60)
            self.config.BaseStation3200[1].zones[3].set_crop_coefficient_value(_ks_value=0.50, _kd_value=1.00, _kmc_value=1.10)
            self.config.BaseStation3200[1].zones[4].set_crop_coefficient_value(_ks_value=0.50, _kd_value=1.00, _kmc_value=.80)
            self.config.BaseStation3200[1].zones[5].set_crop_coefficient_value(_ks_value=0.50, _kd_value=1.10, _kmc_value=1.10)

            # Set zone 6 is full sun Bermuda
            self.config.BaseStation3200[1].zones[6].set_crop_coefficient_value(_kc_value=0.73)

            # set root zone working water holding storage this uses an equation to get the root zone water holding capacity
            # and then sends the value to the controller
            for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone_address].set_root_zone_working_storage_capacity()

            self.logger.info("Set all of the zone's attributes.")
            self.logger.info(sys._getframe().f_code.co_name + " complete!")
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_3(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            program_number_1_start_times = [120]  # 2:00 am start time
            program_number_1_water_windows = ['111111111111111111111111']
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_description(_ds='EPA Test Program')
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_water_window(_ww=program_number_1_water_windows)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_number_1_start_times)
            self.config.BaseStation3200[1].programs[1].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[1].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(_sun=True,
                                                                                                           _mon=True,
                                                                                                           _tues=True,
                                                                                                           _wed=True,
                                                                                                           _thurs=True,
                                                                                                           _fri=True,
                                                                                                           _sat=True)
            self.logger.info("Program has been setup.")
            self.logger.info(sys._getframe().f_code.co_name + " complete!")
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_4(self):
        """
        enable the ET runtime for each zone by assigning it to a program and then enabling it
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n

        """


        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=zone_address)
                # enable each zone to run  on ET run times
                # set _rt = 1 so that there was just a value
                # enable each zone to run  on ET run times
                self.config.BaseStation3200[1].programs[1].zone_programs[zone_address].enable_et_calculated_run_times()
                self.config.BaseStation3200[1].programs[1].zone_programs[zone_address].set_run_time(_minutes=1)
                self.logger.info("Zone's successfully assigned to a zone program and ET runtime has been enabled.")
                self.logger.info(sys._getframe().f_code.co_name + " complete!")



            # Set the new cycle time values
            # Need this to come for the answer to the wrapper
            for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].programs[1].zone_programs[zone_address].set_cycle_time(_use_calculated_cycle_time=True)
                self.logger.info("Cycle time for each zone successfully set.")

            # Set the new soak time values
            # Need this to come for the answer to the wrapper
            for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].programs[1].zone_programs[zone_address].set_soak_time(_use_calculated_soak_time=True)
                self.logger.info("Soak time for each zone successfully set.")

            # self.csv_writer.write_dict_of_obj(dict_of_objs= self.config.BaseStation3200[1].zones,
            #                                   template=csv.EPA.hydro_zone_headers_zn_var_names)
            # self.csv_writer.write_dict_of_obj(dict_of_objs= self.config.BaseStation3200[1].zone_programs,
            #                                   template=csv.EPA.hydro_zone_headers_zp_var_names)
            #
            # self.logger.info(sys._getframe().f_code.co_name + " complete!")
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_5(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        ############################
        setup WaterSources Empty Conditions
        ############################
        Add empty condition with device ---> to water source \n
        - set up empty condition  Attributes \n
            - set enabled state \n
            - set device state \n
            - set wait time  \n
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)

            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(_point_of_control_address=1)

            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            # add the mainline to teh point of control
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_enabled()

            for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=zone_address)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_6(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_7(self):

        """
        input 30 days of eto
        input 30 days of rain fall

        Run controller for x number of days adjusting the Eto and rain fall value for each day


        Build a list of dates by passing in a date range
        build a list of ETO and rain fall data points by accessing the list of eto and rain fall values
        using date manager select the first day
        for each date assign an eto and rain fall value
        start by setting controller time to 11:55pm of the first date of the date range
        increment clock to 12:03  this will allow the controller to do all necessary operations
        Set clock to 12:45 am
        Send Eto and Rain fail data to the controller
        increment clock to 1:01 am this will allow controller to make all necessary calculations
        Compare the calculated moisture balance from the test to the moisture balance in the controller
        Compare the calculated Run time from the test to the Run time in the controller
        Compare the calculated Soak and Cycle Time balance from the test to the Soak and Cycle Time in the controller
        run program until all zones are done
        Compare minute my minute what is happening

        :return:
        :rtype:
        """

        # while date_mngr.reached_last_date() == False:
        #   1. Start by setting controller time to 11:55pm of the first date of the date range
        #   2. While there are days left to complete: (for each date)
        #   3. .... Assign an eto and rain fall value
        #   4. .... Increment clock to 12:03am (allow controller to do calculations)
        #   5. .... Set clock to 12:45am
        #   6. .... Send Eto and Rain fail data to the controller
        #   7. .... Increment clock to 1:01 am this will allow controller to make all necessary calculations
        #   8. .... Compare the calculated moisture balance from the test to the moisture balance in the controller
        #   9. .... Compare the calculated Run time from the test to the Run time in the controller
        #   10..... Compare the calculated Soak and Cycle Time balance from the test to the Soak and Cycle Time in
        #           the controller
        #   11..... Run program until all zones are done
        #   12..... Compare minute my minute what is happening

        # START
        # This will be at the beginning of the script before step one
        # when it wants to run seven minutes it drops the last minute

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            date_mngr.set_dates_from_date_range(start_date='12-01-2015', end_date='12-28-2015')
            self.csv_writer.writerow(["Date range of test: ", "", str(date_mngr.begin_date) + " to " + str(date_mngr.end_date)])
            # With current eto/rainfall list:
            print "Day Before Test Starts", date_mngr.prev_day.date_string_for_controller()
            print "First Day of Test", date_mngr.curr_day.date_string_for_controller()

            self.logger.info("Day Before Test Starts: " + date_mngr.prev_day.date_string_for_controller())
            self.logger.info("First Day of Test: " + date_mngr.curr_day.date_string_for_controller())

            # set date and time on the controller to be 2 minutes before midnight than increment the clock past
            # midnight so the controller can make of of its decisions for the new day

            self.config.BaseStation3200[1].set_date_and_time(_date=date_mngr.prev_day.date_string_for_controller(),
                                                               _time='23:59:00')
            # increment clock past midnight this will allow the controller to run all of its own calculations on things like
            # day intervals
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)
            self.config.BaseStation3200[1].verify_date_and_time()

            # First time through test set default values on et to zero
            first_time = True

            # -------------------------------------------------------------------------------------------------------------

            # Start of MAIN LOOP: This loop runs through each day of the test
            while not date_mngr.reached_last_date():

                # set controller to current date or first day of test
                # set the controller to 12:45am
                # the controller takes in all of its eto data at 12:45 and than does all of its eto calculations for the day
                self.config.BaseStation3200[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                                   _time='00:45:00')

                index = date_mngr.get_current_day_index()
                eto = self.config.daily_eto_value[index]
                rain_fall = self.config.daily_rainfall_value[index]

                # if first day, set initial eto value on all zones to eto (zero for first time through)
                if first_time:
                    self.config.BaseStation3200[1].set_initial_eto_value(_initial_et_value=eto)
                    first_time = False

                # else, set controller et and rain value for current day
                else:
                    self.config.BaseStation3200[1].set_eto_and_date_stamp(_controller_et_value=eto,
                                                                            _controller_rain_value=rain_fall,
                                                                            _controller_date=date_mngr.curr_day.
                                                                            formatted_date_string('%Y%m%d'))

                # increment clock to 1:02 this allows the et equations in the controller to be calculated
                self.config.BaseStation3200[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                                   _time='00:59:00')
                self.config.BaseStation3200[1].do_increment_clock(minutes=2)
                self.config.BaseStation3200[1].verify_date_and_time()

                # outputs the date to the log file
                self.logger.info(date_mngr.curr_day.date_string_for_controller())

                # outputting the date to a csv file
                self.csv_writer.writerow("\n")
                self.csv_writer.writerow(["Date: " + str(date_mngr.curr_day.date_string_for_controller())])
                self.csv_writer.writerow(["ETO Value: " + str(eto)])
                self.csv_writer.writerow(["Rain Fall Value: " + str(rain_fall)])
                self.csv_writer.writerow("\n")

                # ----------------------------------------------------------------------------------------------------------
                # for each zone:
                #   1. get current data from controller
                #   2. verify zone mb
                #   3. get current data for zone program from controller
                #   4. verify zone program runtime
                for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                    self.config.BaseStation3200[1].zones[zone_address].get_data()
                    self.config.BaseStation3200[1].zones[zone_address].verify_moisture_balance()

                    self.config.BaseStation3200[1].programs[1].zone_programs[zone_address].get_data()
                    self.config.BaseStation3200[1].programs[1].zone_programs[zone_address].verify_runtime()
                    # output to log file
                    logging.info(opcodes.zone + ": " + str(zone_address) +
                                 ", Calculated MB: " + str(self.config.BaseStation3200[1].zones[zone_address].mb) +
                                 ", Controller MB: " + self.config.BaseStation3200[1].zones[zone_address].data.get_value_string_by_key(opcodes.daily_moisture_balance) +
                                 ", Calculated RT: " + str(self.config.BaseStation3200[1].programs[1].zone_programs[zone_address].rt) +
                                 ", Controller RT: " + self.config.BaseStation3200[1].programs[1].zone_programs[zone_address].data.get_value_string_by_key(opcodes.runtime_total))


                # ----- END FOR --------------------------------------------------------------------------------------------

                self.config.BaseStation3200[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                                   _time='01:59:00')
                self.config.BaseStation3200[1].do_increment_clock(minutes=2)
                self.config.BaseStation3200[1].verify_date_and_time()

                self.csv_writer.write_dict_of_obj(self.config.BaseStation3200[1].zones)
                self.csv_writer.write_dict_of_obj(self.config.BaseStation3200[1].programs[1].zone_programs,
                                                  template=csv.EPA.hydro_zone_headers_zp_var_names)
                self.csv_writer.writerow(["\n"])

                clock = time(hour=2, minute=0, second=15)
                # this should go minute by minute until all zones are done

                #  need to clear out values for accumulating the new day
                for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                    self.config.BaseStation3200[1].zones[zone_address].seconds_zone_ran = 0
                    self.config.BaseStation3200[1].zones[zone_address].seconds_zone_soaked = 0
                    self.config.BaseStation3200[1].zones[zone_address].seconds_zone_waited = 0
                # ----- END FOR ----------------------------------------------------------------------------------------

                not_done = True

                while not_done:

                    zones_still_running = False
                    log_string = "Time: " + str(clock.isoformat())

                    # ------------------------------------------------------------------------------------------------------
                    # for each zone:
                    #   1. get current data from controller (for getting updated status)
                    #   2. get current status
                    #   3. set flag only if any zones are still running, we want to remain in the while loop checking status
                    for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                        self.config.BaseStation3200[1].zones[zone_address].get_data()
                        _zone_status = self.config.BaseStation3200[1].zones[zone_address]\
                            .data.get_value_string_by_key(opcodes.status_code)
                        # for each zone get the ss per zone
                        #   update the attribute in the object
                        #       (this is so we can store it for what happened during the last run time)
                        if _zone_status == opcodes.watering:
                            # using 60 so that we are in seconds
                            self.config.BaseStation3200[1].zones[zone_address].seconds_zone_ran += 60
                        elif _zone_status == opcodes.soak_cycle:
                            self.config.BaseStation3200[1].zones[zone_address].seconds_zone_soaked += 60
                        elif _zone_status == opcodes.waiting_to_water:
                            self.config.BaseStation3200[1].zones[zone_address].seconds_zone_waited += 60
                        # set flag not all zones are done
                        if _zone_status != opcodes.done_watering:
                            zones_still_running = True

                        # Prepare a string for logging
                        log_string += ", " + opcodes.status_code + "=" + str(_zone_status)
                        # flag to true until all zone are done:
                    # ----- END FOR ----------------------------------------------------------------------------------------


                    self.logger.info(log_string)
                    log_string = ""

                    # this could say if zones_still_running:
                    if zones_still_running:
                        not_done = True
                        self.config.BaseStation3200[1].do_increment_clock(minutes=1)
                        self.config.BaseStation3200[1].verify_date_and_time()
                        try:
                            dummy_date = date(1, 1, 1)
                            full_datetime = datetime.combine(dummy_date, clock)
                            added_datetime = full_datetime + timedelta(minutes=1)
                            clock = added_datetime.time()
                        except Exception, ae:
                            raise Exception("catch clock error: " + ae.message)
                    else:
                        not_done = False

                # ----- END WHILE ------------------------------------------------------------------------------------------
        #TODO need to log the values for each runtime soaktime and cycle time below
                # increment controller clock to the next day 2 minutes before midnight than increment the clock past
                # midnight so the controller can make of of its decisions for the new day
                self.config.BaseStation3200[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                                   _time='23:59:00')
                self.config.BaseStation3200[1].do_increment_clock(minutes=2)
                self.config.BaseStation3200[1].verify_date_and_time()

                # move date manager to the next day to remain in sync with controller's current date
                date_mngr.skip_to_next_day()

                for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                    self.config.BaseStation3200[1].zones[zone_address].yesterdays_mb = \
                        self.config.BaseStation3200[1].zones[zone_address].mb
                    #runtime is the expected time calculated for the controller to run
                    runtime = 60*(int(self.config.BaseStation3200[1].programs[1].zone_programs[zone_address].rt/60))
                    if runtime <= 239:
                        runtime = 0
                    # Outputs the seconds ran, soaked, and cycled to the CSV
                    # TODO tune all of these into the log file
                    # log_string += ", " + opcodes.runtime + "=" + str(.rt)
                    #log_string += (str(self.config.BaseStation3200[1].zones[zone_address].seconds_zone_ran))
                    # output[1].append(str(runtime))
                    # output[2].append(str(self.config.BaseStation3200[1].zones[zone_address].seconds_zone_soaked))
                    # output[3].append(str(self.config.BaseStation3200[1].zones[zone_address].seconds_zone_waited))
                    if self.config.BaseStation3200[1].zones[zone_address].seconds_zone_ran == runtime:
                        e_msg = "Zone '{0}' ran for '{1}' seconds which match expected runtime of '{2}' seconds, " \
                                "it soaked for '{3}' seconds, and it waited for '{4}' seconds"\
                            .format(
                                str(zone_address),                                          # {0}
                                str(self.config.BaseStation3200[1].zones[zone_address].seconds_zone_ran),      # {1}
                                str(runtime),                                       # {2}
                                str(self.config.BaseStation3200[1].zones[zone_address].seconds_zone_soaked),   # {3}
                                str(self.config.BaseStation3200[1].zones[zone_address].seconds_zone_waited)    # {4}}

                            )
                        print(e_msg)
                        self.logger.info(e_msg)
                    elif self.config.BaseStation3200[1].zones[zone_address].seconds_zone_ran > runtime and \
                            abs(self.config.BaseStation3200[1].zones[zone_address].seconds_zone_ran - runtime) <= 60:
                        e_msg = "Zone '{0}' ran for a longer period of time than was expected: Zone '{0}' " \
                                "It was expected to run for '{1}' but it ran for " \
                                "'{2}' seconds, it soaked for '{3}' seconds, and it waited for '{4}' seconds"\
                            .format(
                                str(zone_address),                                          # {0}
                                str(runtime),                                       # {1}
                                str(self.config.BaseStation3200[1].zones[zone_address].seconds_zone_ran),      # {2}
                                str(self.config.BaseStation3200[1].zones[zone_address].seconds_zone_soaked),   # {3}
                                str(self.config.BaseStation3200[1].zones[zone_address].seconds_zone_waited)    # {4}
                            )
                        print(e_msg)
                        self.logger.info(e_msg)
                    elif self.config.BaseStation3200[1].zones[zone_address].seconds_zone_ran < runtime and \
                            abs(self.config.BaseStation3200[1].zones[zone_address].seconds_zone_ran - runtime) <= 60:
                        e_msg = "Zone '{0}' ran for a shorter period of time than was expected: Zone '{0}'" \
                                " It was expected to run for '{1}' but it " \
                                "ran for '{2}' seconds, it soaked for '{3}' seconds, and it waited for '{4}' seconds"\
                            .format(
                                str(zone_address),                                          # {0}
                                str(runtime),                                       # {1}
                                str(self.config.BaseStation3200[1].zones[zone_address].seconds_zone_ran),      # {2}
                                str(self.config.BaseStation3200[1].zones[zone_address].seconds_zone_soaked),   # {3}
                                str(self.config.BaseStation3200[1].zones[zone_address].seconds_zone_waited)    # {4}
                            )
                        print(e_msg)
                        self.logger.info(e_msg)
                    else:
                        e_msg = "Zone '{0}' ran for '{1}' seconds and was expected run for '{2}' seconds, " \
                                "it soaked for '{3}' seconds, and it waited for '{4}' seconds" \
                            .format(
                                str(zone_address),                                          # {0}
                                str(self.config.BaseStation3200[1].zones[zone_address].seconds_zone_ran),      # {1}
                                str(runtime),                                       # {2}
                                str(self.config.BaseStation3200[1].zones[zone_address].seconds_zone_soaked),   # {3}
                                str(self.config.BaseStation3200[1].zones[zone_address].seconds_zone_waited)    # {4}
                            )
                        self.logger.info("Value Error: " + e_msg)
                        raise ValueError(e_msg)
                    self.config.BaseStation3200[1].zones[zone_address].yesterdays_rt = \
                        self.config.BaseStation3200[1].zones[zone_address].seconds_zone_ran
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]