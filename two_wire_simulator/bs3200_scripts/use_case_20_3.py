import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

__author__ = 'Tige'


class ControllerUseCase20_3(object):
    """
    Test name:
        - CN UseCase20_3 Advanced High Flow Variance without Shutdown for Tier 2 (25 -100 GPM range)

    User Story: \n
        1)  As a user I would like to have an option to disable the messages generated from leaks and blown off
            sprinkler heads.

    Coverage and Objective: \n
        - Covers Advanced High Flow Variance Tier 2 without Shutdown
        - Verifies high flow variance detection/shutdown doesn't occur when variance is set to 0%

    Scenario: \n
        - set Advanced High Flow Variance percent to 0%
        - run program
        - verify no variance event acted on when variance percent is set at 0 (per 3200 v16 spec)

    Not Covered: \n
    - Multiple pocs attached to single mainline

    Test Configuration setup: \n
        1. test Advance high flow Tier 2 without shutdown\n
            - Tier 2 range 25 - 100 GPM \n
            - Configuration
                - WS 3 ---> POC 3 ---> ML 3 \n
                    - POC 3
                        - FM 3
                        - MV 3
                        - PM 3
                    - ML 3  Advanced high flow Tier 2 Without Shutdown\n
                        - ZN 7 design flow 25
                        - ZN 8 design flow 50
                        - ZN 9 design flow 75
                        - Variance set to 0%
                    - PG 3
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        # these are global variables for the test
        # zone design flow values
        
        self.zn_7_df = 50.0
        self.zn_8_df = 25.0
        self.zn_9_df = 50.0

        # ML 3
        # HF Variance Tier 2 (25gpm - 100gpm)
        self.ml_3_hi_fl_vr = 0  # testing variance = 0 = watering should complete as expected with no messages
        self.ml_3_lo_fl_vr = 0  # testing variance = 0 = watering should complete as expected with no messages

        self.run_use_case()

    #################################
    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set controller max concurrency to allow for all programs and zones to water
            self.config.BaseStation3200[1].set_max_concurrent_zones(40)

            program_start_time_10am = [600]  # 10:00am start time
            program_water_windows = ['111111111111111111111111']

            # Add and configure Program 3
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=3)
            self.config.BaseStation3200[1].programs[3].set_enabled()
            self.config.BaseStation3200[1].programs[3].set_water_window(_ww=program_water_windows)
            self.config.BaseStation3200[1].programs[3].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[3].set_max_concurrent_zones(_number_of_zones=2)
            self.config.BaseStation3200[1].programs[3].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[3].set_watering_intervals_to_selected_days_of_the_week(_sun=True,
                                                                                                           _mon=True,
                                                                                                           _tues=True,
                                                                                                           _wed=True,
                                                                                                           _thurs=True,
                                                                                                           _fri=True,
                                                                                                           _sat=True)
            self.config.BaseStation3200[1].programs[3].set_start_times(_st_list=program_start_time_10am)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure Program ZN 7
            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=7)
            self.config.BaseStation3200[1].programs[3].zone_programs[7].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[3].zone_programs[7].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[3].zone_programs[7].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[3].zone_programs[7].set_soak_time(_minutes=10)
            # link ZN 8 and 9 to primary ZN 7
            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=8)
            self.config.BaseStation3200[1].programs[3].zone_programs[8].set_as_linked_zone(_primary_zone=7)
            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=9)
            self.config.BaseStation3200[1].programs[3].zone_programs[9].set_as_linked_zone(_primary_zone=7)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=3)
            self.config.BaseStation3200[1].water_sources[3].set_enabled()
            self.config.BaseStation3200[1].water_sources[3].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[3].set_monthly_watering_budget(_budget=0)
            self.config.BaseStation3200[1].water_sources[3].set_water_rationing_to_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure POC 3
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=3)
            self.config.BaseStation3200[1].points_of_control[3].set_enabled()
            self.config.BaseStation3200[1].points_of_control[3].add_master_valve_to_point_of_control(_master_valve_address=3)
            self.config.BaseStation3200[1].points_of_control[3].add_pump_to_point_of_control(_pump_address=3)
            self.config.BaseStation3200[1].points_of_control[3].add_flow_meter_to_point_of_control(_flow_meter_address=3)
            self.config.BaseStation3200[1].points_of_control[3].set_target_flow(_gpm=900)
            # Add POC 3 to Water Source 3
            self.config.BaseStation3200[1].water_sources[3].add_point_of_control_to_water_source(_point_of_control_address=3)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control
        """
        # here we can either execute the following uncommented lines in procedural fashion, or we could re-init the
        # object, would have to import Mainline at the top, effectively accomplishing the same thing by:
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure ML 3
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=3)
            self.config.BaseStation3200[1].mainlines[3].set_pipe_stabilization_time(_minutes=3)
            self.config.BaseStation3200[1].mainlines[3].set_target_flow(_gpm=75)
            self.config.BaseStation3200[1].mainlines[3].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[3].set_use_advanced_flow_to_true()
            self.config.BaseStation3200[1].mainlines[3].set_high_flow_variance_tier_two(_percent=self.ml_3_hi_fl_vr,
                                                                                        _with_shutdown_enabled=False)
            self.config.BaseStation3200[1].mainlines[3].set_low_flow_variance_tier_two(_percent=self.ml_3_lo_fl_vr,
                                                                                       _with_shutdown_enabled=False)
            # Add ML 3 to POC 3
            self.config.BaseStation3200[1].points_of_control[3].add_mainline_to_point_of_control(_mainline_address=3)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].mainlines[3].add_zone_to_mainline(_zone_address=7)
            self.config.BaseStation3200[1].mainlines[3].add_zone_to_mainline(_zone_address=8)
            self.config.BaseStation3200[1].mainlines[3].add_zone_to_mainline(_zone_address=9)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ######################
        setup zones on mainlines
        ######################
         - set up zone  Attributes associated with main lines \n
            - set design flow on zone
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Assign a design flow value to each zone so that they have a default setting
            self.config.BaseStation3200[1].zones[7].set_design_flow(_gallons_per_minute=self.zn_7_df)
            self.config.BaseStation3200[1].zones[8].set_design_flow(_gallons_per_minute=self.zn_8_df)
            self.config.BaseStation3200[1].zones[9].set_design_flow(_gallons_per_minute=self.zn_9_df)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        ############################
        Configure FlowMeters
        ############################

        - Configure FlowMeters to trigger HF/LF variance with calculated actual flow values
        - FlowMeter 3 will have values to trigger HF variance
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            
            # --------------------------------------- #
            # Configure High Flow Variance Components #
            # --------------------------------------- #
            
            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[3],
                _variance_percent=self.ml_3_lo_fl_vr,
                _expected_gpm=self.zn_7_df + self.zn_8_df)
            
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        ############################
        Increment clock to start programming
        ############################

        - Set date and time on controller and increment clock to start zones running
        - Increment clock past program's start time of 10am
            - All programs should start
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ---------------------------------------------------------------------- #
            # Set controllers date/time to 1 minute before program start time (10am) #
            # ---------------------------------------------------------------------- #

            date_mngr.set_current_date_to_match_computer()
            self.config.BaseStation3200[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                             _time='09:59:00')

            # ------------------------------------------------------------ #
            # Increment controller clock to 10am to trigger program starts #
            # ------------------------------------------------------------ #

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        ############################
        Increment clock from 10:00 to 10:02 AM
        ############################
        
        ---
        Step Overview
        ---
            * Simulate first 2 minutes of ML 3's flow stabilization time
            * Verify hydraulic components turn on and start watering
        
        ---
        What happened during 10:00 to 10:02 AM Minute Processing:
        ---
            * Program starts
            * All hydraulics go to running (no delays)
            * ZNs 7,8 go to watering (program concurrency of 2)
            * ZN 9 is waiting
            
        ---
        Strikes:
        ---
            * ZN 7: 0
            * ZN 8: 0
            * ZN 9: 0
            * ML 3: 0
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            
            # time should be 10:02am
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)

            # Verify running components
            self.config.BaseStation3200[1].water_sources[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_running()

            # Verify program statuses are running.
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()

            # ---------------------------------------- #
            # Tier 2 High Flow Variance Components      #
            # ---------------------------------------- #

            # ZN 9 is waiting to water because PG 3 has max concurrency of 2
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_waiting_to_water()
            
            # Verify no ML 3 low/high flow detected message
            self.config.BaseStation3200[1].mainlines[3].messages.check_for_low_flow_variance_detected_message_not_present()
            self.config.BaseStation3200[1].mainlines[3].messages.check_for_high_flow_variance_detected_message_not_present()
            
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        ############################
        Increment clock from 10:02 to 10:03 AM
        ############################
        
        ---
        Step Overview
        ---
            * Simulate final minute of ML 3's flow stabilization time
        
        ---
        What happened during 10:02 to 10:03 AM Minute Processing:
        ---
            * Stable flow is read
                + High flow is detected from 10:02 to 10:03 minute processing but not acted on until AFTER the top of the
                  next minute (i.e., will act on high flow reading when clock is incremented from 10:03 to 10:04 am)
                  
        ---
        Strikes:
        ---
            * ZN 7: 0
            * ZN 8: 0
            * ZN 9: 0
            * ML 3: 0
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
        
            # time should be 10:03am
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
        
            # Verify running components
            self.config.BaseStation3200[1].water_sources[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_running()
        
            # Verify program statuses are running.
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()
        
            # ---------------------------------------- #
            # Tier 2 High Flow Variance Components      #
            # ---------------------------------------- #
        
            # ZN 9 is waiting to water because PG 3 has max concurrency of 2
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_waiting_to_water()
        
            # Because ML 3's flow stabilization time is 3 minutes, we shouldn't have a variance detected message yet.
            self.config.BaseStation3200[1].mainlines[3].messages.check_for_low_flow_variance_detected_message_not_present()
            self.config.BaseStation3200[1].mainlines[3].messages.check_for_high_flow_variance_detected_message_not_present()
    
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        ############################
        Increment clock from 10:03 to 10:04 AM
        ############################
        
        ---
        Step Overview
        ---
            * Verify no flow variance message because our variance percentages were set to 0% which means that the 
              controller should ignore flow fault events on the ML 3
            * Verify high flow variance event behavior:
                + Verify and clear alarm message on controller
            * Set FM 3 flow rate to be equal to design flow of ZNs 7 and 8 so that no high flow variance event
              happens again
        
        ---
        What happened during 10:03 to 10:04 AM Minute Processing:
        ---
            * High flow variance event not acted on:
                + No Message posted on ML 3 because variance was set at 0%
                
        ---
        Strikes:
        ---
            * ZN 7: 0
            * ZN 8: 0
            * ZN 9: 0
            * ML 3: 0
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            
            # time should be 10:04am
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify running components
            self.config.BaseStation3200[1].water_sources[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_running()

            # Verify program statuses are running.
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()

            # ---------------------------------------- #
            # Tier 2 High Flow Variance Components     #
            # ---------------------------------------- #

            # FM 3's flow reading shouldn't affect watering in this step due to variance being set to 0%
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_waiting_to_water()

            # At this point, if we had a non-zero value for our variance percentage, we would have a variance message,
            # but since we have a 0% variance setting on ML 3's Adv. High Flow Variance Tier2, no message/action should 
            # occur.
            self.config.BaseStation3200[1].mainlines[3].messages.check_for_low_flow_variance_detected_message_not_present()
            self.config.BaseStation3200[1].mainlines[3].messages.check_for_high_flow_variance_detected_message_not_present()
            
            # Update ML 3 flow to NOT trigger high flow variance.
            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[3],
                _expected_gpm=self.zn_7_df + self.zn_8_df)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_14(self):
        """
        ############################
        Increment clock from 10:04 to 10:10 AM
        ############################
        
        ---
        Step Overview
        ---
            * Simulate 6 minutes forward to simulate the rest of the watering cycle of ZNs 7 and 8
            * Verify at the top of the minute our ZN status' are still:
                + ZNs 7 and 8 are watering
                + ZN 9 is waiting
                
            NOTE:
                Statuses should remain unchanged in this step due to the processing loop in the 3200. No processing
                is done at exactly the top of the minute, thus the top of the minute on the 3200 reflects the status
                of the processing from the last minute.
        
        ---
        What happened during 10:04 to 10:10 AM Minute Processing:
        ---
            * Current watering cycle for ZNs 7 and 8 are completed
            * Device flow readings taken to be used for 10:10 to 10:11 AM processing
            
        ---
        Strikes:
        ---
            * ZN 7: 0
            * ZN 8: 0
            * ZN 9: 0
            * ML 3: 0
            
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            
            # time should be 10:10am
            self.config.BaseStation3200[1].do_increment_clock(minutes=6)

            self.config.BaseStation3200[1].water_sources[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_running()

            # Verify program statuses are running.
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()

            # ---------------------------------------- #
            # Tier 2 High Flow Variance Components     #
            # ---------------------------------------- #

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_waiting_to_water()
            
            # Verify no ML 3 low/high flow detected message
            self.config.BaseStation3200[1].mainlines[3].messages.check_for_low_flow_variance_detected_message_not_present()
            self.config.BaseStation3200[1].mainlines[3].messages.check_for_high_flow_variance_detected_message_not_present()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_15(self):
        """
        ############################
        Increment clock from 10:10 to 10:11 AM
        ############################
        
        ---
        Step Overview
        ---
            * Increments controller clock forward 1 minute to process previous minute data
            * Verifies ZNs 7 and 8 finished watering cycle and started soak cycle
            * Verifies ZN 9 started watering cycle
            * Sets FM 3's flow rate equal to ZN 8 so that no variance is triggered
        
        ---
        What happened during 10:10 to 10:11 AM Minute Processing:
        ---
            * ZNs 7 and 8 are set to soaking
            * ZN 9 is started
            * Device flow readings taken to be used for 10:11 to 10:12 AM processing
            * Flow is not stable yet because of how the controller determines timed flow stabilization:
                
                => If (current controller sim time) >= (time zones turned on plus pipe fill time minus 15 seconds):
                    return flow is stable
                    
                For example, if the following:
                
                    controller sim time = 10:12
                    time zones finished turning on = 10:11:08
                    mainline pipe fill = 3 minutes
                    
                Then, stable flow is false when:
                
                    (10:12) >= (10:11 + (3 minutes - 15 seconds))
                 -> (10:12) >= (10:13:45)
                 -> false
                 
                 If the following:
                 
                    controller sim time = 10:14
                    time zones finished turning on = 10:11:08
                    mainline pipe fill = 3 minutes
                    
                Then, stable flow is true when:
                
                    (10:14) >= (10:11 + (3 minutes - 15 seconds))
                 -> (10:14) >= (10:13:45)
                 -> true
            
        ---
        Strikes:
        ---
            * ZN 7: 0
            * ZN 8: 0
            * ZN 9: 0
            * ML 3: 0
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Update ML 3 flow to NOT trigger high flow variance at this point because ZNs 7 and 8 are finished and
            # ZN 9 is going to start watering
            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[3],
                _expected_gpm=self.zn_9_df)
    
            # time should be 10:11am
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            self.config.BaseStation3200[1].water_sources[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_running()

            # Verify program statuses are running.
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()

            # ---------------------------------------- #
            # Tier 2 High Flow Variance Components     #
            # ---------------------------------------- #

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_watering()

            # Verify no ML 3 low/high flow detected message after new group of zones being turned on
            self.config.BaseStation3200[1].mainlines[3].messages.check_for_low_flow_variance_detected_message_not_present()
            self.config.BaseStation3200[1].mainlines[3].messages.check_for_high_flow_variance_detected_message_not_present()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_16(self):
        """
        ############################
        Increment clock from 10:11 to 10:20 AM
        ############################
        
        ---
        Step Overview
        ---
            * Simulate 9 minutes forward to simulate the rest of the watering cycle of ZN 9
            * Verify at the top of the minute our ZN status' are still:
                + ZNs 7 and 8 are soaking 
                + ZN 9 is still watering 
                
            NOTE:
                Statuses should remain unchanged in this step due to the processing loop in the 3200. No processing
                is done at exactly the top of the minute, thus the top of the minute on the 3200 reflects the status
                of the processing from the last minute.
        
        ---
        What happened during 10:11 to 10:20 AM Minute Processing:
        ---
            * Current watering cycle for ZNs 7 and 8 are completed
            * Device flow readings taken to be used for 10:20 to 10:21 AM processing
            
        ---
        Strikes:
        ---
            * ZN 7: 0
            * ZN 8: 0
            * ZN 9: 0
            * ML 3: 0
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            
            # time should be 10:20am
            self.config.BaseStation3200[1].do_increment_clock(minutes=9)

            self.config.BaseStation3200[1].water_sources[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_running()

            # Verify program statuses are running.
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()

            # ---------------------------------------- #
            # Tier 2 High Flow Variance Components      #
            # ---------------------------------------- #

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_watering()

            # Verify no ML 3 low/high flow detected message after ZN 9 completes watering
            self.config.BaseStation3200[1].mainlines[3].messages.check_for_low_flow_variance_detected_message_not_present()
            self.config.BaseStation3200[1].mainlines[3].messages.check_for_high_flow_variance_detected_message_not_present()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        
    #################################
    def step_17(self):
        """
        ############################
        Increment clock from 10:20 to 10:22 AM
        ############################
        
        ---
        Step Overview
        ---
            * Increment clock forward 2 minutes so that we still have 1 minute left on ML 3's flow stabilization time
              before a high flow variance is detected.
            * Verify at the top of the minute our ZN status' are still:
                + ZNs 7 and 8 start final watering cycle
                + ZN 9 goes to soaking
        
        ---
        What happened during 10:20 to 10:22 AM Minute Processing:
        ---
            * ZNs 7,8 go to watering
            * ZN 9 starts soaking
            
        ---
        Strikes:
        ---
            * ZN 7: 0
            * ZN 8: 0
            * ZN 9: 0
            * ML 3: 0
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            
            # Set FM 3's flow rate to trigger high flow variance before incrementing clock so that the rate we set here
            # will be used in the variance detection. Set it here so that we can verify that the variance detection
            # doesn't happen until after the flow stabilization time.
            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[3],
                _variance_percent=self.ml_3_lo_fl_vr,
                _expected_gpm=self.zn_7_df + self.zn_8_df)
            
            # time should be 10:22am
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)

            # Verify running components
            self.config.BaseStation3200[1].water_sources[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_running()

            # Verify program statuses are running.
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()

            # ---------------------------------------- #
            # Tier 2 High Flow Variance Components      #
            # ---------------------------------------- #

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_soaking()
            
            # Verify no ML 3 high/high flow detected message
            self.config.BaseStation3200[1].mainlines[3].messages.check_for_low_flow_variance_detected_message_not_present()
            self.config.BaseStation3200[1].mainlines[3].messages.check_for_high_flow_variance_detected_message_not_present()
            
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_18(self):
        """
        ############################
        Increment clock from 10:22 to 10:23 AM
        ############################
        
        ---
        Step Overview
        ---
            * Increment clock forward 1 minutes so that we finish the final minute of ML 3's flow stabilization time
            * Verify that high flow variance message isn't present
        
        ---
        What happened during 10:22 to 10:23 AM Minute Processing:
        ---
            * Stable flow is read
                + High flow is detected from 10:22 to 10:23 minute processing but not acted on until AFTER the top of the
                  next minute (i.e., will NOT act on high flow reading when clock is incremented from 10:23 to 10:24 am
                  because variance value is 0%)
                  
        ---
        Strikes:
        ---
            * ZN 7: 0
            * ZN 8: 0
            * ZN 9: 0
            * ML 3: 0
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
        
            # Set FM 3's flow rate to trigger high flow variance before incrementing clock so that the rate we set here
            # will be used in the variance detection
            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[3],
                _variance_percent=self.ml_3_lo_fl_vr,
                _expected_gpm=self.zn_7_df + self.zn_8_df)
        
            # time should be 10:23am
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
        
            # Verify running components
            self.config.BaseStation3200[1].water_sources[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_running()
        
            # Verify program statuses are running.
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()
        
            # ---------------------------------------- #
            # Tier 2 High Flow Variance Components      #
            # ---------------------------------------- #

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_soaking()

            # Because ML 3's flow stabilization time is 3 minutes, we shouldn't have a variance detected message yet.
            self.config.BaseStation3200[1].mainlines[3].messages.check_for_low_flow_variance_detected_message_not_present()
            self.config.BaseStation3200[1].mainlines[3].messages.check_for_high_flow_variance_detected_message_not_present()
    
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_19(self):
        """
        ############################
        Increment clock from 10:23 to 10:24 AM
        ############################
        
        ---
        Step Overview
        ---
            * Verify high flow variance event behavior:
                + Verify no flow fault message posted due to our variance being 0%.
            * Set FM 3 flow rate to be equal to design flow of ZNs 7 and 8 so that no high flow variance event
              happens again
        
        ---
        What happened during 10:23 to 10:24 AM Minute Processing:
        ---
            * High flow variance event not acted on:
                + No Message posted on Mainline
                
        ---
        Strikes:
        ---
            * ZN 7: 0
            * ZN 8: 0
            * ZN 9: 0
            * ML 3: 0
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            
            # time should be 10:24am
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify running components
            self.config.BaseStation3200[1].water_sources[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_running()

            # Verify program statuses are running.
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()

            # ---------------------------------------- #
            # Tier 2 High Flow Variance Components     #
            # ---------------------------------------- #

            # FM 3 reading triggers zone variance on ML 3
            #   -> Mainline 3 ZNs continue watering as is because shutdown was disabled
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_soaking()

            # At this point, if we had a non-zero value for our variance percentage, we would have a variance message,
            # but since we have a 0% variance setting on ML 3's Adv. High Flow Variance Tier2, no message/action should
            # occur.
            self.config.BaseStation3200[1].mainlines[3].messages.check_for_low_flow_variance_detected_message_not_present()
            self.config.BaseStation3200[1].mainlines[3].messages.check_for_high_flow_variance_detected_message_not_present()
            
            # Update ML 3 flow to NOT trigger high flow variance.
            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[3],
                _expected_gpm=self.zn_7_df + self.zn_8_df)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_20(self):
        """
        ############################
        Increment clock from 10:24 to 10:30 AM
        ############################
        
        ---
        Step Overview
        ---
            * Simulate 6 minutes forward to simulate the rest of the watering cycle of ZNs 7 and 8
            * Verify at the top of the minute our ZN status' are still:
                + ZNs 7 and 8 are watering
                + ZN 9 is soaking 
                
            NOTE:
                Statuses should remain unchanged in this step due to the processing loop in the 3200. No processing
                is done at exactly the top of the minute, thus the top of the minute on the 3200 reflects the status
                of the processing from the last minute.
        
        ---
        What happened during 10:24 to 10:30 AM Minute Processing:
        ---
            * Current watering cycle for ZNs 7 and 8 are completed
            * Device flow readings taken to be used for 10:30 to 10:31 AM processing
            
        ---
        Strikes:
        ---
            * ZN 7: 0
            * ZN 8: 0
            * ZN 9: 0
            * ML 3: 0
            
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            
            # time should be 10:30am
            self.config.BaseStation3200[1].do_increment_clock(minutes=6)

            self.config.BaseStation3200[1].water_sources[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_running()

            # Verify program statuses are running.
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()

            # ---------------------------------------- #
            # Tier 2 High Flow Variance Components     #
            # ---------------------------------------- #

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_soaking()
            
            # Verify no ML 3 low/high flow detected message
            self.config.BaseStation3200[1].mainlines[3].messages.check_for_low_flow_variance_detected_message_not_present()
            self.config.BaseStation3200[1].mainlines[3].messages.check_for_high_flow_variance_detected_message_not_present()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_21(self):
        """
        ############################
        Increment clock from 10:30 to 10:31 AM
        ############################
        
        ---
        Step Overview
        ---
            * Increments controller clock forward 1 minute to process previous minute data
            * Verifies ZNs 7 and 8 finished watering cycle and are set to done
            * Verifies ZN 9 started final watering cycle
            * Sets FM 3's flow rate equal to ZN 8 so that no variance is triggered
        
        ---
        What happened during 10:30 to 10:31 AM Minute Processing:
        ---
            * ZNs 7 and 8 are set to done (finished final watering cycle)
            * ZN 9 is started
            * Device flow readings taken to be used for 10:31 to 10:32 AM processing
            * Flow is not stable yet because of how the controller determines timed flow stabilization:
                
                => If (current controller sim time) >= (time zones turned on plus pipe fill time minus 15 seconds):
                    return flow is stable
                    
                For example, if the following:
                
                    controller sim time = 10:12
                    time zones finished turning on = 10:11:08
                    mainline pipe fill = 3 minutes
                    
                Then, stable flow is false when:
                
                    (10:12) >= (10:11 + (3 minutes - 15 seconds))
                 -> (10:12) >= (10:13:45)
                 -> false
                 
                 If the following:
                 
                    controller sim time = 10:14
                    time zones finished turning on = 10:11:08
                    mainline pipe fill = 3 minutes
                    
                Then, stable flow is true when:
                
                    (10:14) >= (10:11 + (3 minutes - 15 seconds))
                 -> (10:14) >= (10:13:45)
                 -> true
            
        ---
        Strikes:
        ---
            * ZN 7: 0
            * ZN 8: 0
            * ZN 9: 0
            * ML 3: 0
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Update ML 3 flow to NOT trigger high flow variance at this point because ZNs 7 and 8 are finished and
            # ZN 9 is going to start watering
            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[3],
                _expected_gpm=self.zn_9_df)
        
            # time should be 10:31am
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
        
            self.config.BaseStation3200[1].water_sources[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_running()
        
            # Verify program statuses are running.
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()
        
            # ---------------------------------------- #
            # Tier 2 High Flow Variance Components     #
            # ---------------------------------------- #
        
            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_watering()
        
            # Verify no ML 3 low/high flow detected message after new group of zones being turned on
            self.config.BaseStation3200[1].mainlines[3].messages.check_for_low_flow_variance_detected_message_not_present()
            self.config.BaseStation3200[1].mainlines[3].messages.check_for_high_flow_variance_detected_message_not_present()
    
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_22(self):
        """
        ############################
        Increment clock from 10:31 to 10:40 AM
        ############################
        
        ---
        Step Overview
        ---
            * Simulate 9 minutes forward to simulate the rest of the watering cycle of ZN 9
            * Verify at the top of the minute our ZN status' are still:
                + ZNs 7 and 8 are done
                + ZN 9 is still watering 
                
            NOTE:
                Statuses should remain unchanged in this step due to the processing loop in the 3200. No processing
                is done at exactly the top of the minute, thus the top of the minute on the 3200 reflects the status
                of the processing from the last minute.
        
        ---
        What happened during 10:31 to 10:40 AM Minute Processing:
        ---
            * Current watering cycle for ZNs 7, 8 and 9 are completed
            * ZN 9 won't be set to done until minute processing of 10:40 to 10:41 AM
            * Rest of hydraulic components will not go to "idle" state until minute processing of 10:40 to 10:41 AM
            
        ---
        Strikes:
        ---
            * ZN 7: 0
            * ZN 8: 0
            * ZN 9: 0
            * ML 3: 0
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
        
            # time should be 10:40am
            self.config.BaseStation3200[1].do_increment_clock(minutes=9)
        
            self.config.BaseStation3200[1].water_sources[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_running()
        
            # Verify program statuses are running.
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()
        
            # ---------------------------------------- #
            # Tier 2 High Flow Variance Components      #
            # ---------------------------------------- #
        
            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_watering()
        
            # Verify no ML 3 low/high flow detected message after ZN 9 completes watering
            self.config.BaseStation3200[1].mainlines[3].messages.check_for_low_flow_variance_detected_message_not_present()
            self.config.BaseStation3200[1].mainlines[3].messages.check_for_high_flow_variance_detected_message_not_present()
    
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_23(self):
        """
        ############################
        Increment clock from 10:40 to 10:41 AM
        ############################
        
        ---
        Step Overview
        ---
            * Increments controller clock forward 1 minutes to complete program 3's run.
            * Verify all zones are done
            * Verify hydraulic components are done or in "idle" state respectively
        
        ---
        What happened during 10:40 to 10:41 AM Minute Processing:
        ---
            * No flow fault detected
            * No other actions in regards to this use case
            
        ---
        Strikes:
        ---
            * ZN 7: 0
            * ZN 8: 0
            * ZN 9: 0
            * ML 3: 0
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
        
            # Update flow rates to not trigger variance conditions (set to 0 because no zones are running, want
            # flow meter to return to "idle" state)
            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[3],
                _expected_gpm=0)
            
            # time should be 10:41 am
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            self.config.BaseStation3200[1].water_sources[3].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].points_of_control[3].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[3].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[3].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].flow_meters[3].statuses.verify_status_is_ok()
        
            # Verify program statuses are running.
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
        
            # ---------------------------------------- #
            # Tier 2 High Flow Variance Components      #
            # ---------------------------------------- #
        
            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_done()
            
            # Verify no ML 3 low/high flow detected message after ZN 9 completes watering
            self.config.BaseStation3200[1].mainlines[3].messages.check_for_low_flow_variance_detected_message_not_present()
            self.config.BaseStation3200[1].mainlines[3].messages.check_for_high_flow_variance_detected_message_not_present()
    
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
