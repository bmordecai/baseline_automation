import sys
from time import sleep

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

# Random so that we can generate random values for our zones
from random import randint


__author__ = 'Eldin'


class ControllerUseCase12_2(object):
    """
    Test name:
        - CN UseCase 12.2 Backup/Restore BM Stress Test

    User Story: \n

        1)  As a user I want to be able to save my backup to BaseManager at any point in the day, as many times as I
            want, and I want to be able to restore from that backup at any point.

    Coverage and Objectives:
        1.	A backup and restore can be done from BaseManager 11 times in a row and it should work the same every time.
            It also needs to backup the correct information, and restore that same information, exactly.

    Use Case explanation:
        - We load up some basic programming and then edit a variable, do a backup, reboot, then restore. We then change
          more values, randomly, and we should be able to get those values back on a backup/restore. They should also
          persist after the reboot.

    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    #################################
    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)
                    # we have to set a long timeout on the serial port because the test runs for long periods of time
                    self.config.BaseStation3200[1].set_serial_port_timeout(timeout=5040)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            program_number_1_start_times = [480, 540, 600, 660]
            program_number_3_start_times = [480, 540, 600, 660]
            program_number_4_start_times = [480, 540, 600, 660]
            program_number_99_start_times = [480, 540, 600, 660]
            program_number_99_watering_days = [0, 0, 0, 0, 0, 0, 8, 8, 6, 5, 5, 4, 3, 3, 3, 3, 4, 5, 6, 7, 0, 0, 0, 0]
            program_number_1_water_windows = ['011111100001111111111110']
            program_number_3_water_windows = ['011111100001111111111110']
            program_number_4_water_windows = ['011111100001111111111110']
            program_number_99_water_windows = ['011111100000111111111110',
                                               '011111100001111111111111',
                                               '011111100001111111111110',
                                               '011111100001111111111110',
                                               '011111100001111111111110',
                                               '011111100001111111111110',
                                               '011111100001111111111111']
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[1].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[1].set_water_window(_ww=program_number_1_water_windows)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(_sun=False,
                                                                                                           _mon=True,
                                                                                                           _tues=False,
                                                                                                           _wed=True,
                                                                                                           _thurs=False,
                                                                                                           _fri=True,
                                                                                                           _sat=False)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_number_1_start_times)

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=3)
            self.config.BaseStation3200[1].programs[3].set_enabled()
            self.config.BaseStation3200[1].programs[3].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[3].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[3].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[3].set_water_window(_ww=program_number_3_water_windows)
            self.config.BaseStation3200[1].programs[3].set_watering_intervals_to_odd_days()
            self.config.BaseStation3200[1].programs[3].set_start_times(_st_list=program_number_3_start_times)

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=4)
            self.config.BaseStation3200[1].programs[4].set_enabled()
            self.config.BaseStation3200[1].programs[4].set_priority_level(_pr_level=3)
            self.config.BaseStation3200[1].programs[4].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation3200[1].programs[4].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[4].set_water_window(_ww=program_number_4_water_windows)
            self.config.BaseStation3200[1].programs[4].set_watering_intervals_to_selected_days_of_the_week(_sun=False,
                                                                                                           _mon=True,
                                                                                                           _tues=False,
                                                                                                           _wed=True,
                                                                                                           _thurs=False,
                                                                                                           _fri=True,
                                                                                                           _sat=False)
            self.config.BaseStation3200[1].programs[4].set_start_times(_st_list=program_number_4_start_times)

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=99)
            self.config.BaseStation3200[1].programs[99].set_enabled()
            self.config.BaseStation3200[1].programs[99].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[99].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[99].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[99]. \
                set_water_window(_ww=program_number_99_water_windows, _is_weekly=False)
            self.config.BaseStation3200[1].programs[99]. \
                set_watering_intervals_to_semi_monthly(_sm=program_number_99_watering_days)
            self.config.BaseStation3200[1].programs[99].set_start_times(_st_list=program_number_99_start_times)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=5)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=49)
            self.config.BaseStation3200[1].programs[3].zone_programs[49].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[3].zone_programs[49].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[3].zone_programs[49].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[3].zone_programs[49].set_soak_time(_minutes=60)

            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=50)
            self.config.BaseStation3200[1].programs[3].zone_programs[50].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[3].zone_programs[50].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[3].zone_programs[50].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[3].zone_programs[50].set_soak_time(_minutes=60)

            self.config.BaseStation3200[1].programs[4].add_zone_to_program(_zone_address=50)
            self.config.BaseStation3200[1].programs[4].zone_programs[50].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[4].zone_programs[50].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[4].zone_programs[50].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[4].zone_programs[50].set_soak_time(_minutes=60)

            # Program 99 Zone Programs
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=200)
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_run_time(_minutes=33)
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_cycle_time(_minutes=3)
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_soak_time(_minutes=13)

            # Program 99 Zone Programs linked to Zone Program 200
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=197)
            self.config.BaseStation3200[1].programs[99].zone_programs[197].set_as_linked_zone(_primary_zone=200,
                                                                                              _tracking_ratio=50)

            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=198)
            self.config.BaseStation3200[1].programs[99].zone_programs[198].set_as_linked_zone(_primary_zone=200)

            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=199)
            self.config.BaseStation3200[1].programs[99].zone_programs[199].set_as_linked_zone(_primary_zone=200,
                                                                                              _tracking_ratio=150)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        setup WaterSources
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Water Source 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[1].set_monthly_watering_budget(_budget=100000,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].water_sources[1].set_water_rationing_to_enabled()

            # Water Source 8
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=8)
            self.config.BaseStation3200[1].water_sources[8].set_enabled()
            self.config.BaseStation3200[1].water_sources[8].set_priority(_priority_for_water_source=3)
            self.config.BaseStation3200[1].water_sources[8].set_monthly_watering_budget(_budget=1000,
                                                                                        _with_shutdown_enabled=False)
            self.config.BaseStation3200[1].water_sources[8].set_water_rationing_to_disabled()

            # Water Source 8 empty conditions
            self.config.BaseStation3200[1].water_sources[8].add_switch_empty_condition(_event_switch_address=1)
            self.config.BaseStation3200[1].water_sources[8].switch_empty_conditions[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[8].switch_empty_conditions[1]. \
                set_switch_empty_condition_to_closed()
            self.config.BaseStation3200[1].water_sources[8].switch_empty_conditions[1].set_empty_wait_time(_minutes=8)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        setup Point of Controls
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # Add & Configure POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1]. \
                add_master_valve_to_point_of_control(_master_valve_address=1)
            self.config.BaseStation3200[1].points_of_control[1]. \
                add_flow_meter_to_point_of_control(_flow_meter_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].points_of_control[1].set_high_flow_limit(_limit=550,
                                                                                    with_shutdown_enabled=True)
            self.config.BaseStation3200[1].points_of_control[1].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=True)
            # Add POC 1 to Water Source 1
            self.config.BaseStation3200[1].water_sources[1] \
                .add_point_of_control_to_water_source(_point_of_control_address=1)

            # Add & Configure POC 8
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=8)
            self.config.BaseStation3200[1].points_of_control[8].set_enabled()
            self.config.BaseStation3200[1].points_of_control[8]. \
                add_master_valve_to_point_of_control(_master_valve_address=8)
            self.config.BaseStation3200[1].points_of_control[8]. \
                add_flow_meter_to_point_of_control(_flow_meter_address=8)
            self.config.BaseStation3200[1].points_of_control[8].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].points_of_control[8].set_high_flow_limit(_limit=75,
                                                                                    with_shutdown_enabled=False)
            self.config.BaseStation3200[1].points_of_control[8].set_unscheduled_flow_limit(_gallons=5,
                                                                                           with_shutdown_enabled=False)
            # Add POC 8 to Water Source 8
            self.config.BaseStation3200[1].water_sources[8]. \
                add_point_of_control_to_water_source(_point_of_control_address=8)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ##################
        setup mainlines
        ##################
        """
        # here we can either execute the following uncommented lines in procedural fashion, or we could re-init the
        # object, would have to import Mainline at the top, effectively accomplishing the same thing by:
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure ML 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=4)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[1].set_high_flow_variance_tier_one(_percent=5,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].mainlines[1].set_low_flow_variance_tier_one(_percent=20,
                                                                                       _with_shutdown_enabled=True)
            # Add ML 1 to POC 1
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)
            # Add & Configure ML 8
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=8)
            self.config.BaseStation3200[1].mainlines[8].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[8].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].mainlines[8].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[8].set_high_flow_variance_tier_one(_percent=20,
                                                                                        _with_shutdown_enabled=False)
            self.config.BaseStation3200[1].mainlines[8].set_high_flow_variance_tier_one(_percent=5,
                                                                                        _with_shutdown_enabled=False)
            # Add ML 8 to POC 8
            self.config.BaseStation3200[1].points_of_control[8].add_mainline_to_point_of_control(_mainline_address=8)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ######################
        setup zones on mainlines
        ######################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Add ZN 1,2, 49, 50 to ML 1
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=1)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=2)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=49)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=50)

            # Add ZN 197-200 to ML 8
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=197)
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=198)
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=199)
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=200)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        Verify that our object's variables contain the same values as the values in the controller.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # this also verifies firmware version
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        We set an attributes for zones, programs, water sources, mainlines, and points of control so we can make sure
        the backup we are sending will be containing new data, so when we restore we expect the new data to be
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for i in range(0, 11):  # repeat 11 times
                # Change some values of the programming every loop to make sure we have new data for each backup
                self.config.BaseStation3200[1].zones[1].set_design_flow(randint(1, 100))
                self.config.BaseStation3200[1].zones[2].set_design_flow(randint(1, 100))
                self.config.BaseStation3200[1].zones[49].set_design_flow(randint(1, 100))
                self.config.BaseStation3200[1].zones[50].set_design_flow(randint(1, 100))
                self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(randint(1, 10))
                self.config.BaseStation3200[1].programs[99].set_max_concurrent_zones(randint(1, 10))
                self.config.BaseStation3200[1].water_sources[1].set_monthly_watering_budget(randint(1, 10))
                self.config.BaseStation3200[1].water_sources[8].set_monthly_watering_budget(randint(1, 10))
                self.config.BaseStation3200[1].mainlines[1].set_high_flow_variance_tier_one(randint(1, 25),
                                                                                            _with_shutdown_enabled=False)
                self.config.BaseStation3200[1].mainlines[8].set_high_flow_variance_tier_one(randint(1, 25),
                                                                                            _with_shutdown_enabled=False)
                self.config.BaseStation3200[1].points_of_control[1].set_high_flow_limit(randint(1, 500))
                self.config.BaseStation3200[1].points_of_control[8].set_high_flow_limit(randint(1, 500))

                # start clock and wait so the controller can do its backup at top of minute
                self.config.BaseStation3200[1].start_clock()
                self.config.BaseStation3200[1].set_sim_mode_to_off()
                time.sleep(80)
                self.config.BaseStation3200[1].do_backup_programming(where_to=opcodes.basemanager)

                # clear all programming, doesn't reset the date
                self.config.BaseStation3200[1].reinitialize_controller()

                # have to reconnect to BMW before we can do the restore programming
                self.config.BaseStation3200[1].basemanager_connection[1].wait_for_bm_connection()
                self.config.BaseStation3200[1].do_restore_programming(where_from=opcodes.basemanager)

                # Make sure our two wire values are correct
                for zone in sorted(self.config.BaseStation3200[1].zones.keys()):
                    self.config.BaseStation3200[1].zones[zone].bicoder.self_test_and_update_object_attributes()

                for moisture_sensor in sorted(self.config.BaseStation3200[1].moisture_sensors.keys()):
                    self.config.BaseStation3200[1].moisture_sensors[moisture_sensor].bicoder. \
                        self_test_and_update_object_attributes()

                for master_valve in sorted(self.config.BaseStation3200[1].master_valves.keys()):
                    self.config.BaseStation3200[1].master_valves[master_valve].bicoder. \
                        self_test_and_update_object_attributes()

                for flow_meter in sorted(self.config.BaseStation3200[1].flow_meters.keys()):
                    self.config.BaseStation3200[1].flow_meters[flow_meter].bicoder. \
                        self_test_and_update_object_attributes()

                for event_switch in sorted(self.config.BaseStation3200[1].event_switches.keys()):
                    self.config.BaseStation3200[1].event_switches[event_switch].bicoder. \
                        self_test_and_update_object_attributes()

                for temperature_sensor in sorted(self.config.BaseStation3200[1].temperature_sensors.keys()):
                    self.config.BaseStation3200[1].temperature_sensors[temperature_sensor].bicoder. \
                        self_test_and_update_object_attributes()

                # Finally we want to verify what we restored on the controller still matches our objects
                self.config.BaseStation3200[1].verify_full_configuration()

                # Now reboot the controller and verify the programming is still there
                self.config.BaseStation3200[1].do_reboot_controller()
                self.config.BaseStation3200[1].verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
