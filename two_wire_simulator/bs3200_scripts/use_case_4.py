import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods


__author__ = 'Tige'


class ControllerUseCase4(object):
    """
    Test name:
        - Lower Limit One Time Calibration
    purpose:
        - Run a program using a moisture Sensor and have it do a lower limit Calibration cycles
        - verify linked zones run during calibration cycle
        - verify a calibration cycle will fail because the saturation level wasn't met
        - verify a calibration cycle will pass and set the correct values


    Coverage area: \n
        This test covers smart watering with sensors. It covers the one time calibration for moisture sensor water for
        both
            upper and lower limits. \n
        -setting up devices:
            - Loading \n
            - Searching \n
            - Addressing \n
                - Setting:
                    - descriptions
                    - locations \n

            - do a single lower limit calibration \n
            - Normal calibration
            - calibration that moisture doesnt hit saturation

    Date References:
        - configuration for script is located common\configuration_files\One_time_calibration.json
        - the devices and addresses range is read from the .json file

    """
    # TODO Things to add to the test or to a new test \n
    # TODO calibration that moisture level doesnt move
    # TODO upper limit calibration
    # TODO do a single upper limit calibration \n (this not covered)
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)
                    # we have to set a long timeout on the serial port because the test runs for long periods of time
                    self.config.BaseStation3200[1].set_serial_port_timeout(timeout=5040)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                     # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        # this is set in the PG3200 object
        # TODO need to have concurrent zones per program added

        program_8am_start_time = [480]
        program_full_open_water_windows = ['111111111111111111111111']
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_description(_ds='EPA Test Program')
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_water_window(_ww=program_full_open_water_windows)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_8am_start_time)
            self.config.BaseStation3200[1].programs[1].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[1].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(_sun=True,
                                                                                                           _mon=True,
                                                                                                           _tues=True,
                                                                                                           _wed=True,
                                                                                                           _thurs=True,
                                                                                                           _fri=True,
                                                                                                           _sat=True)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=20)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].add_moisture_sensor_to_primary_zone(
                _moisture_sensor_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_water_strategy_to_lower_limit()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_one_time_calibration_cycle()

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1,
                                                                                           _tracking_ratio=100)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_as_linked_zone(_primary_zone=1,
                                                                                           _tracking_ratio=50)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[1].zone_programs[4].set_as_linked_zone(_primary_zone=1,
                                                                                           _tracking_ratio=150)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[5].set_as_linked_zone(_primary_zone=1,
                                                                                           _tracking_ratio=100)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_3(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        set_water Source \n
            - set up WaterSource 1 \n
                - enable POC 1 \n
                - assign master valve TMV0003 and flow meter TWF0003 to POC 1 \n
                - assign POC 1 a target flow of 500 \n
                - assign POC 1 to main line 1 \n
                - set POC priority to 2-medium \n
                - set high flow limit to 550 and enable high flow shut down \n
                - set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
                - set water budget to 100000 and enable the water budget shut down \n
                - enable water rationing \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[1].set_monthly_watering_budget(_budget=10000,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].water_sources[1].set_water_rationing_to_enabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n
        set_poc_3200 \n
            - set up POC 1 \n
                - enable POC 1 \n
                - assign master valve TMV0003 and flow meter TWF0003 to POC 1 \n
                - assign POC 1 a target flow of 500 \n
                - assign POC 1 to main line 1 \n
                - set POC priority to 2-medium \n
                - set high flow limit to 550 and enable high flow shut down \n
                - set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
                - set water budget to 100000 and enable the water budget shut down \n
                - enable water rationing \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_flow_meter_to_point_of_control(
                _flow_meter_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].points_of_control[1].set_high_flow_limit(_limit=550,
                                                                                    with_shutdown_enabled=True)
            self.config.BaseStation3200[1].points_of_control[1].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=True)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control
        set_mainlines_3200 \n
            - set up main line 1 \n
                - set limit zones by flow to true \n
                - set the pipe fill time to 4 minutes \n
                - set the target flow to 500 \n
                - set the high variance limit to 5% and enable the high variance shut down \n
                - set the low variance limit to 20% and enable the low variance shut down \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_enabled()
            self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=4)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[1].set_high_flow_variance_tier_one(_percent=5,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].mainlines[1].set_low_flow_variance_tier_one(_percent=20,
                                                                                       _with_shutdown_enabled=False)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=zone_address)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]


    #################################
    def step_7(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        - how it works run cycle time than soak time than take reading
        - set the controller date and time so that there is a known days of the week and days of the month \n
        - verify that all zones are working properly by verifying them before starting the test \n
        - start the moisture level at 20% and raise it to 25.5%
        - verify that the primary zones shut off
        - Verify that all link zone shut off
        - each zone has a 15 run time and a 20 minute cycle and a 30 minute soak
        - zone 3 is 50% watering time set to 15 minutes and 10 minute cycle time and a 30 minute soak
        - zone 4 is 150% watering time set to 45 minutes and 30 minute cycle time and a 30 minute soak
        when you first install a sensor you must wait 24 hours in order to do a calibration cycle \n
        set the moisture sensor to be below the upper limit \n
        the upper limit is set to 25 \n
        program 1 has a start time of 8:00 am \n
        set the clock to 7:45 and then increment time to verify the program starts \n
        do a self test on the moisture sensor so that the 3200 will read the sensors and update there current values \n
        verify that none of the zones have started watering \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=26.0) # set initial value for moisture sensor to 26%
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].set_date_and_time(_date='08/27/2014', _time='23:45:00')
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].do_increment_clock(minutes=20)
            self.config.BaseStation3200[1].verify_date_and_time()
        except AssertionError, ae:
            raise Exception(method + " " +  self.config.BaseStation3200[1].controller_object_current_date_time.
                            formatted_date_string("%m-%d-%Y %H:%M:%S") + " " + ae.message)
        try:
            self.config.BaseStation3200[1].set_date_and_time(_date='08/28/2014', _time='7:53:00')
            self.config.BaseStation3200[1].do_increment_clock(minutes=6)
            self.config.BaseStation3200[1].verify_date_and_time()
            for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_done()
                self.config.BaseStation3200[1].zones[zone_address].get_data()
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        set moisture sensor values so the Programs can start watering and not be effected \n
        set moisture sensor value to 24% \n
        Do a self test to update the value in the controller \n
        advance the clock 2 minutes to make the time 8:01 this should start the zones watering \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=24.0)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)
            self.config.BaseStation3200[1].verify_date_and_time()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        advance the clock 10 minutes to make the time 8:11 \n
        set the moisture sensor to 24.3% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=10)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=24.3)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        advance the clock 10 minutes to make the time 8:21 \n
        set the moisture sensor to 24.5% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=10)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(24.5)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        advance the clock 10 minutes to make the time 8:31 \n
        set the moisture sensor to 25.0% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 soaking \n
            - zone 4 watering \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=10)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=25.0)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        advance the clock 10 minutes to make the time 8:41 \n
        set the moisture sensor to 25.5% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 soaking \n
            - zone 3 soaking \n
            - zone 4 soaking \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=10)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=25.5)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_14(self):
        """
        advance the clock 10 minutes to make the time 8:51 \n
        set the moisture sensor to 25.6% \n
        Do a self test to update the value in the controller \n
        beacause saturation was not hit zone 1 is set to soaking \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 watering \n
            - zone 3 soaking \n
            - zone 4 soaking \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=10)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=25.6)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_15(self):
        """
        advance the clock 10 minutes to make the time 9:01 \n
        set the moisture sensor to 26.0% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 soaking \n
            - zone 5 watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=10)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=26.0)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()
            time.sleep(10)

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                    self.config.test_name,
                    sys._getframe().f_code.co_name,
                    date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                    str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_16(self):
        """
        advance the clock 10 minutes to make the time 9:11 \n
        set the moisture sensor to 26.1% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 watering \n
            - zone 5 soaking \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=10)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=26.1)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_soaking()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_17(self):
        """
        advance the clock 10 minutes to make the time 9:21 \n
        set the moisture sensor to 26.3% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 soaking \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=10)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=26.3)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_soaking()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_18(self):
        """
        advance the clock 10 minutes to make the time 9:27 \n
        set the moisture sensor to 26.4% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=6)
            self.config.BaseStation3200[1].verify_date_and_time()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_soaking()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_19(self):
        """
        Check the message on the controller
        Check the message on the controller
        - expected =    (DT = 08/29/14 22:00:00'),
                        (ID = 001_ZN_1_CT
                        (TX = Zone 1: SB05308\n
                        Moisture biSensor Calibration Failed:\n
                        Did not reach Saturation.\n
                        Primary Zone = 1\n
                        Moisture biSensor = SB05308\n
                        Moisture = 26.3 \n),
                        ('PR', 'NN'), ('V1', '20.125'), ('V2', '25.125'), ('PC', '1'), ('PG', '1'), ('ZN', '1')]
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            #TODO fix when messages are completed
            self.config.BaseStation3200[1].programs[1].zone_programs[1].messages.\
                verify_moisture_sensor_calibration_failure_no_saturation_message()
            # # this clears the message and then verifies that it was cleared
            self.config.BaseStation3200[1].programs[1].zone_programs[1].messages.\
                clear_moisture_sensor_calibration_failure_no_saturation_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_20(self):
        """
        set the moisture sensor to 26.4% \n
        Do a self test to update the value in the controller \n
        advance the clock 10 minutes to make the time 9:41 \n
        set the moisture sensor to 26.5% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=26.4)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=14)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=26.5)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_21(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        Zone Programs \n
            - the controller failed a moisture sensor calibration  because saturation was not reached so the runtime \n
            - is increased so that the saturation can be reached the runtime went from 900 seconds to 1800 seconds \n
            - but the cycle and soak time remained the same also reset primary zone to calibrate one time \n
            - the zone program objects have to be reinitialized in order get all attributes reset \n
        Verify full configuration to verify everything got set correctly \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=30)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=20)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].add_moisture_sensor_to_primary_zone(
                _moisture_sensor_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_water_strategy_to_lower_limit()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_one_time_calibration_cycle()

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1,
                                                                                           _tracking_ratio=100)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_as_linked_zone(_primary_zone=1,
                                                                                           _tracking_ratio=50)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[1].zone_programs[4].set_as_linked_zone(_primary_zone=1,
                                                                                           _tracking_ratio=150)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[5].set_as_linked_zone(_primary_zone=1,
                                                                                           _tracking_ratio=100)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_22(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
    #################################
    def step_23(self):
        """
        set the moisture sensor to be below the upper limit \n
        the upper limit is set to 25 \n
        program 1 has a start time of 8:00 am \n
        set the clock to 7:45 and then increment time to verify the program starts \n
        do a self test on the moisture sensor so that the 3200 will read the sensors and update there current values \n
        verify that none of the zones have started watering \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=24.0)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].set_date_and_time(_date='08/28/2014', _time='23:45:00')
            self.config.BaseStation3200[1].do_increment_clock(minutes=20)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].set_date_and_time(_date='08/29/2014', _time='7:53:00')
            self.config.BaseStation3200[1].do_increment_clock(minutes=6)
            self.config.BaseStation3200[1].verify_date_and_time()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_24(self):
        """
        set moisture sensor values so the Programs can start watering and not be effected \n
        set moisture sensor value to 24% \n
        Do a self test to update the value in the controller \n
        advance the clock 2 minutes to make the time 8:01 this should st art the zones watering \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 waiting to water \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=24.0)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)
            self.config.BaseStation3200[1].verify_date_and_time()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_25(self):
        """
        advance the clock 10 minutes to make the time 8:11 \n
        set the moisture sensor to 24.3% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 watering \n
            - zone 3 waiting to water \n
            - zone 4 waiting to water \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=10)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=24.3)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_26(self):
        """
        advance the clock 10 minutes to make the time 8:21 \n
        set the moisture sensor to 24.5% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 watering \n
            - zone 4 waiting to water \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=10)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=24.5)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_27(self):
        """
        advance the clock 10 minutes to make the time 8:31 \n
        set the moisture sensor to 25.0% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 soaking \n
            - zone 4 watering \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=10)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=25.0)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_28(self):
        """
        advance the clock 10 minutes to make the time 8:41 \n
        set the moisture sensor to 25.5% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 soaking \n
            - zone 3 soaking \n
            - zone 4 soaking \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=10)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=25.5)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_29(self):
        """
        advance the clock 10 minutes to make the time 8:51 \n
        set the moisture sensor to 25.6% \n
        Do a self test to update the value in the controller \n
        beacause saturation was not hit zone 1 is set to soaking \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 watering \n
            - zone 3 soaking \n
            - zone 4 soaking \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=10)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=25.6)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_30(self):
        """
        advance the clock 10 minutes to make the time 9:01 \n
        set the moisture sensor to 26.0% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 watering \n
            - zone 4 soaking \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=10)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=26.0)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()
            time.sleep(10)

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_31(self):
        """
        advance the clock 10 minutes to make the time 9:11 \n
        set the moisture sensor to 26.5% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 soaking \n
            - zone 3 soaking \n
            - zone 4 watering \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=10)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=26.1)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_32(self):
        """
        advance the clock 10 minutes to make the time 9:21 \n
        set the moisture sensor to 26.3% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program waiting to water \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 soaking \n
            - zone 3 soaking \n
            - zone 4 soaking \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=10)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=26.5)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_33(self):
        """
        advance the clock 10 minutes to make the time 9:31 \n
        set the moisture sensor to 26.4% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 watering \n
            - zone 3 soaking \n
            - zone 4 soaking \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=10)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=26.4)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_34(self):
        """
        advance the clock 10 minutes to make the time 9:41 \n
        set the moisture sensor to 26.5% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 done watering \n
            - zone 3 watering \n
            - zone 4 soaking \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=10)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=26.5)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_35(self):
        """
        advance the clock 7 minutes to make the time 9:48 \n
        set the moisture sensor to 26.5% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 soaking \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 watering \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=7)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=26.5)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_36(self):
        """
        advance the clock 13 minutes to make the time 10:01 \n
        set the moisture sensor to 26.5% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 waiting to water \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=13)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=26.5)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_37(self):
        """
        advance the clock 6 minutes to make the time 10:07 \n
        set the moisture sensor to 26.5% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program running \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=6)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=26.5)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_38(self):
        """
        advance the clock 1 hour and 11 minutes to make the time 11:18 \n
        increment to completely finish last zone \n
        set the moisture sensor to 26.5% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(hours=1, minutes=11)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=26.5)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_39(self):
        """
        advance the clock 2 hours to make the time 13:18 \n
        set the moisture sensor to 26.0% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(hours=2)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=26.0)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_40(self):
        """
        advance the clock 2 hours to make the time 15:18 \n
        set the moisture sensor to 25.5% \n
        Do a self test to update the value in the controller \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(hours=2)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=25.5)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_41(self):
        """
        set the moisture sensor to 24.0% \n
        Do a self test to update the value in the controller \n
        advance the clock 6 hours 43 minutes to make the time 22:01 \n
        \n
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program done watering \n
        - Zone status: \n
            - zone 1 done watering \n
            - zone 2 done watering \n
            - zone 3 done watering \n
            - zone 4 done watering \n
            - zone 5 done watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=24.0)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(hours=6, minutes=43)
            self.config.BaseStation3200[1].verify_date_and_time()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_42(self):
        """
        Need to set the object to have the correct upper and lower limits this will allow us to verify the correct
        values that the controller set for both upper and lower limits
        also have to resent the calibration cycle to NV because the calibration completed
        Check the message on the controller
        expected =  (DT = 08/29/14 22:00:00'),
                    (ID = 001_ZN_1_CS
                    (TX = Zone 1: SB05308\n
                    Moisture biSensor Calibration Done:\n
                    New watering limits set.\n
                    Primary Zone = 1\n
                    Moisture biSensor = SB05308\n
                    Limit Lower/Upper = 20.125n / 25.125),
                    ('PR', 'NN'), ('V1', '20.125'), ('V2', '25.125'), ('PC', '1'), ('PG', '1'), ('ZN', '1')]
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            pass

            self.config.BaseStation3200[1].programs[1].zone_programs[1].ll = 20.125
            self.config.BaseStation3200[1].programs[1].zone_programs[1].ul = 25.125
            self.config.BaseStation3200[1].programs[1].zone_programs[1].cc = opcodes.calibrate_never
            # TODO need to fix message
            self.config.BaseStation3200[1].programs[1].zone_programs[1]\
                .messages.verify_moisture_sensor_calibration_success_message()
            print ('all is good')
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_43(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

