import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration

# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr

__author__ = 'Tige'


class ControllerUseCase5_4(object):
    """
    Test name:
        - Water source restrictions month rollover

        TEST VERIFIES JIRA BUG: ZZ-1691

    User Story: \n
        1)  As a user I want to monitor my water usage month by month, and for each month to start from a clean slate.

    Coverage area of feature: \n
        This test covers 1st point in the user story:
            A single water source with a budget that shuts down watering and notifies the user when the budget is met \n
            A single water source with a budget that does not shutdown watering but still notifies the user when the \n
            budget is met \n
            After the budgets are met  during the month a second start time will be hit in order to verify that the \n
            that the budge stays in effect\n
            After the month is over verify that the budget is reset for the next month and water can resume\n

    Use Case explanation:
        Two water sources will be setup to verify the behavior \n
        First water source will be set to have shutdown enabled \n
        Second water source with have the shutdown disabled \n
        Each water source will be assigned a single poc, mainline, and program. \n
        Three zones will be assigned to each mainline with one being a primary and the other two being linked \n

        First Scenario:
            The programs will be told to start manually
            - First water source will hit the budget value with shutdown is enabled
                - water source will no longer supply water
                - message will be generated
                - operate status will be displayed
            - Second water source will hit the budget value with shutdown is disabled
                - water source cut will continue to supply water
                - message will still be generated
            - the controller will be rebooted during the irrigation from  scenario  1
            - and verify that the controller maintains the budgets
        Second Scenario:
            -  Start the water again in the same month
                - verify:
                    - First water source will remain shutdown
                        - messages and statuses will remain from before
                    - Second water source will remain watering
                        - water source cut will continue to supply water
                        - message will still be generated
        Third Scenario:
            - adjust the date and time to move to the next month
            - a new set of start time will be met
            - verify that the water budget has been reset so that the water sources can supply water

    Date References:
        - configuration for script is located common\configuration_files\using_real_time_flow.json
        - the devices and addresses range is read from the .json file


    """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        full_open_water_windows = ['111111111111111111111111']

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=2)

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_water_window(_ww=full_open_water_windows)
            self.config.BaseStation3200[1].programs[1].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[1].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(_sun=True,
                                                                                                           _mon=True,
                                                                                                           _tues=True,
                                                                                                           _wed=True,
                                                                                                           _thurs=True,
                                                                                                           _fri=True,
                                                                                                           _sat=True)

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation3200[1].programs[2].set_enabled()
            self.config.BaseStation3200[1].programs[2].set_water_window(_ww=full_open_water_windows)
            self.config.BaseStation3200[1].programs[2].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[2].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[2].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[2].set_watering_intervals_to_selected_days_of_the_week(_sun=True,
                                                                                                           _mon=True,
                                                                                                           _tues=True,
                                                                                                           _wed=True,
                                                                                                           _thurs=True,
                                                                                                           _fri=True,
                                                                                                           _sat=True)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=5)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1,
                                                                                           _tracking_ratio=100)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1,
                                                                                           _tracking_ratio=100)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[2].zone_programs[4].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[2].zone_programs[4].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[2].zone_programs[4].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[2].zone_programs[4].set_soak_time(_minutes=5)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=5)
            self.config.BaseStation3200[1].programs[2].zone_programs[5].set_as_linked_zone(_primary_zone=4,
                                                                                           _tracking_ratio=100)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=6)
            self.config.BaseStation3200[1].programs[2].zone_programs[6].set_as_linked_zone(_primary_zone=4,
                                                                                           _tracking_ratio=100)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_3(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[1].set_water_rationing_to_disabled()

            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=2)
            self.config.BaseStation3200[1].water_sources[2].set_enabled()
            self.config.BaseStation3200[1].water_sources[2].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[2].set_water_rationing_to_disabled()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n
        \n

        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_pump_to_point_of_control(_pump_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_flow_meter_to_point_of_control(
                _flow_meter_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=500)

            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=2)
            self.config.BaseStation3200[1].water_sources[2].add_point_of_control_to_water_source(
                _point_of_control_address=2)
            self.config.BaseStation3200[1].points_of_control[2].set_enabled()
            self.config.BaseStation3200[1].points_of_control[2].add_master_valve_to_point_of_control(
                _master_valve_address=2)
            self.config.BaseStation3200[1].points_of_control[2].add_pump_to_point_of_control(_pump_address=2)
            self.config.BaseStation3200[1].points_of_control[2].add_flow_meter_to_point_of_control(
                _flow_meter_address=2)
            self.config.BaseStation3200[1].points_of_control[2].set_target_flow(_gpm=500)


        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_enabled()
            self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=4)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_true()

            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=2)
            self.config.BaseStation3200[1].points_of_control[2].add_mainline_to_point_of_control(_mainline_address=2)
            self.config.BaseStation3200[1].mainlines[2].set_enabled()
            self.config.BaseStation3200[1].mainlines[2].set_pipe_stabilization_time(_minutes=4)
            self.config.BaseStation3200[1].mainlines[2].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].mainlines[2].set_limit_zones_by_flow_to_true()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=1)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=2)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=3)

            self.config.BaseStation3200[1].mainlines[2].add_zone_to_mainline(_zone_address=4)
            self.config.BaseStation3200[1].mainlines[2].add_zone_to_mainline(_zone_address=5)
            self.config.BaseStation3200[1].mainlines[2].add_zone_to_mainline(_zone_address=6)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        set the initial water budget for each Water source to used in budget \n
        WS1 has shutdown enabled (after reaching budget)
        WS2 does not have shutdown enabled
        Set the flow meter to zero usage this gives us a starting point \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation3200[1].water_sources[1].set_monthly_watering_budget(_budget=1000,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_water_usage(_water_usage=0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].water_sources[2].set_monthly_watering_budget(_budget=2400,
                                                                                        _with_shutdown_enabled=False)
            self.config.BaseStation3200[1].flow_meters[2].bicoder.set_water_usage(_water_usage=0)
            self.config.BaseStation3200[1].flow_meters[2].bicoder.self_test_and_update_object_attributes()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        - Run scenario 1
           - set each flow meter to have a set GPM
           - calculate how much time it take to reach the budget point for each water source
           - verify that when the first budget is met that all devices shut down because shut down is enabled
           - also verify message is triggered
           - continue to run the second water source until its budget is met
           - verify that when the second budget is met that all devices do not shut down because shut down is disabled
           - verify message is triggered
           - reboot controller and verify statuses and messages stay
           - water source one remain shut down and water source two continues to run
           - reset controller to a known state
        \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # set an initial time on the controller
            self.config.BaseStation3200[1].set_date_and_time(_date='08/27/2017', _time='23:40:00')
            self.config.BaseStation3200[1].verify_date_and_time()
            # set the flow meter GPM
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=200)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].flow_meters[2].bicoder.set_flow_rate(_gallons_per_minute=400)
            self.config.BaseStation3200[1].flow_meters[2].bicoder.self_test_and_update_object_attributes()

            # this gets the water source budget value
            first_ws_budget = self.config.BaseStation3200[1].water_sources[1].wb
            # this gets the value the flow meter was set to
            first_flow_meter_rate = self.config.BaseStation3200[1].flow_meters[1].bicoder.vr
            # this calculate how many minutes the water source has to run in order to reach the budget Value
            minutes_to_for_first_budget = int(first_ws_budget / first_flow_meter_rate)

            second_ws_budget = self.config.BaseStation3200[1].water_sources[2].wb
            second_flow_meter_rate = self.config.BaseStation3200[1].flow_meters[2].bicoder.vr
            minutes_to_for_second_budget = int(second_ws_budget / second_flow_meter_rate)

            # calculate left over minutes after first second water source to finish after the first water souces errors
            minutes_left_after_first_budget_used = minutes_to_for_second_budget - minutes_to_for_first_budget

            # Verify zero usage on both flow meters
            self.config.BaseStation3200[1].flow_meters[1].bicoder.verify_water_usage()
            self.config.BaseStation3200[1].flow_meters[2].bicoder.verify_water_usage()

            # start both programs
            self.config.BaseStation3200[1].programs[1].set_program_to_start()
            self.config.BaseStation3200[1].programs[2].set_program_to_start()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            # verify both water sources turn on
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].mainlines[2].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Run the clock until we expect the first water source to be used up
            self.config.BaseStation3200[1].do_increment_clock(minutes=minutes_to_for_first_budget-1)
            self.config.BaseStation3200[1].verify_date_and_time()

            # verify message
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].flow_meters[1].bicoder.vg = first_ws_budget
            # At this point, we expect that the WS1 has used up to its budget
            self.config.BaseStation3200[1].flow_meters[1].bicoder.verify_water_usage()

            # shut off first flow meter so we don't get an unexpected flow fault
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=0.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            # Bump clock by a minute to give controller time to react to the over-budget situation
            self.config.BaseStation3200[1].verify_date_and_time()

            # Now verify the the WS is over budget (OB), the downstream POC, mastervalve, mainlines, are off,
            # and associated program & zone are now done
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_over_budget()

            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_off()

            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_off()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            # verify we got the over budget message
            self.config.BaseStation3200[1].water_sources[1]. \
                messages.verify_exceed_monthly_budget_with_shutdown_message()

            # verify second water source is still supplying water to its downsteam POC, etc.
            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].mainlines[2].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Run the clock until we expect the second water source to be used up
            self.config.BaseStation3200[1].do_increment_clock(minutes=minutes_left_after_first_budget_used)
            self.config.BaseStation3200[1].verify_date_and_time()

            # we incremented the clock with the flow meter running so the we have to add in one more minute of flow
            self.config.BaseStation3200[1].flow_meters[2].bicoder.vg = second_ws_budget + second_flow_meter_rate
            self.config.BaseStation3200[1].flow_meters[2].bicoder.verify_water_usage()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            # verify we got the over budget message, it is generated when we hit the budget
            self.config.BaseStation3200[1].flow_meters[2].bicoder.vg = second_ws_budget
            self.config.BaseStation3200[1].water_sources[2].messages.verify_exceed_monthly_budget_message()

            # verify second water source is still supplying water to its downsteam POC, etc.
            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].mainlines[2].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # reboot the controller here to verify the state of the controller doesnt change
            self.config.BaseStation3200[1].do_reboot_controller()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            self.config.BaseStation3200[1].flow_meters[1].bicoder.verify_water_usage()
            # because we increment the clock another minute flow usage goes up by the flow
            self.config.BaseStation3200[1].flow_meters[2].bicoder.vg = \
                second_ws_budget + second_flow_meter_rate + second_flow_meter_rate
            self.config.BaseStation3200[1].flow_meters[2].bicoder.verify_water_usage()

            # reset flow meter value after a reboot
            self.config.BaseStation3200[1].flow_meters[2].bicoder.set_flow_rate(_gallons_per_minute=400.0)
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify that water sources, POCs, associated devices, programs, and zones haven't changed status,
            # and we still have the over budget messages on the controller
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_over_budget()

            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_off()

            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_off()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].mainlines[2].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # make sure that both flow meters are set to zero
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=0.0)
            self.config.BaseStation3200[1].flow_meters[2].bicoder.set_flow_rate(_gallons_per_minute=0.0)

            # stop both programs so we are ready for the next scenario
            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()

            # increment clock to get all statuses correct
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # clear both messages so we are ready for the next scenario
            self.config.BaseStation3200[1].water_sources[1].messages.clear_exceed_monthly_budget_with_shutdown_message()
            self.config.BaseStation3200[1].water_sources[2].messages.clear_exceed_monthly_budget_message()
            self.config.BaseStation3200[1].messages.clear_boot_up_message()
            self.config.BaseStation3200[1].messages.clear_restore_successful_from_internal_flash_message()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        - Run scenario 2
           - set each flow meter to have a set GPM
           - calculate how much time it take to reach the budget point for each water source
           - verify that when the first budget is met that all devices stayed shut down because shut down is enabled
           - also verify message is triggered
           - verify that the second water source starts running
           - verify message is triggered with new water used values
        \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # set the flow meter GPM
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=0.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].flow_meters[2].bicoder.set_flow_rate(_gallons_per_minute=400)
            self.config.BaseStation3200[1].flow_meters[2].bicoder.self_test_and_update_object_attributes()

            # this gets the water source budget value
            first_ws_budget = self.config.BaseStation3200[1].water_sources[1].wb
            # this gets the value the flow meter was set to
            first_flow_meter_rate = self.config.BaseStation3200[1].flow_meters[1].bicoder.vr
            # this calculate how many minutes the water source has to run in order to reach the budget Value

            second_ws_budget = self.config.BaseStation3200[1].water_sources[2].wb
            second_flow_meter_rate = self.config.BaseStation3200[1].flow_meters[2].bicoder.vr

            # start both programs
            self.config.BaseStation3200[1].programs[1].set_program_to_start()
            self.config.BaseStation3200[1].programs[2].set_program_to_start()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            # Verify water sources turn on
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_over_budget()

            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_off()

            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_off()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            # Verify message
            self.config.BaseStation3200[1].water_sources[1].messages.verify_exceed_monthly_budget_with_shutdown_message()

            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].mainlines[2].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # At this point, we expect that the WS2 has exceeded its budget for the second month
            self.config.BaseStation3200[1].water_sources[2].messages.verify_exceed_monthly_budget_message()

            # At this point, we expect that the WS1 has used up to its budget for the second month
            # this is a little higher than budget cause it ran a minute befor it could get all the way shut off
            self.config.BaseStation3200[1].flow_meters[1].bicoder.vg = first_ws_budget + first_flow_meter_rate

            # flow meter has last month and this month usage
            self.config.BaseStation3200[1].flow_meters[1].bicoder.verify_water_usage()

            # shut off both flow meter so we don't get an unexpected flow fault
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=0.0)
            self.config.BaseStation3200[1].flow_meters[2].bicoder.set_flow_rate(_gallons_per_minute=0.0)

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # stop both programs so we are ready for the next scenario
            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()

            # clear both messages so we are ready for the next scenario
            self.config.BaseStation3200[1].water_sources[1].messages.clear_exceed_monthly_budget_with_shutdown_message()
            # because the controller ran an extra minute the water usage needs to be updated
            self.config.BaseStation3200[1].flow_meters[2].bicoder.vg = \
                second_ws_budget + second_flow_meter_rate + second_flow_meter_rate
            self.config.BaseStation3200[1].water_sources[2].messages.clear_exceed_monthly_budget_message()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        - Run scenario 3
            - set date and time on controller to be just before next month
            - increment clock to go past the date and time of month
            - set each flow meter to have a set GPM
            - calculate how much time it take to reach the budget point for each water source
            - verify that when the first budget is met that all devices shut down because shut down is enabled
            - also verify message is triggered
            - continue to run the second water source until its budget is met
            - verify that when the second budget is met that all devices do not shut down because shut down is disabled
            - verify message is triggered
        \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Roll over the time to the next month so that budgets are reset
            self.config.BaseStation3200[1].set_date_and_time(_date='08/31/2014', _time='23:58:00')
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].do_increment_clock(minutes=3)
            self.config.BaseStation3200[1].verify_date_and_time()

            # set the flow meter GPM
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=200)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].flow_meters[2].bicoder.set_flow_rate(_gallons_per_minute=400)
            self.config.BaseStation3200[1].flow_meters[2].bicoder.self_test_and_update_object_attributes()

            # this gets the water source budget value
            first_ws_budget = self.config.BaseStation3200[1].water_sources[1].wb
            # this gets the value the flow meter was set to
            first_flow_meter_rate = self.config.BaseStation3200[1].flow_meters[1].bicoder.vr
            # this calculate how many minutes the water source has to run in order to reach the budget Value
            minutes_to_for_first_budget = int(first_ws_budget / first_flow_meter_rate)

            second_ws_budget = self.config.BaseStation3200[1].water_sources[2].wb
            second_flow_meter_rate = self.config.BaseStation3200[1].flow_meters[2].bicoder.vr
            minutes_to_for_second_budget = int(second_ws_budget / second_flow_meter_rate)

            # start both programs
            self.config.BaseStation3200[1].programs[1].set_program_to_start()
            self.config.BaseStation3200[1].programs[2].set_program_to_start()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            # verify first water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # verify second water source
            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].mainlines[2].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Run the clock until we expect the first water source to be used up
            # (we've already run one minute, so run the rest)
            self.config.BaseStation3200[1].do_increment_clock(minutes=minutes_to_for_first_budget-1)
            self.config.BaseStation3200[1].verify_date_and_time()

            # verify message
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()
            # At this point, we expect that the WS1 has used up to its budget for the second month
            self.config.BaseStation3200[1].flow_meters[1].bicoder.vg = (2 * first_ws_budget)
            # flow meter has last month and this month usage
            self.config.BaseStation3200[1].flow_meters[1].bicoder.verify_water_usage()

            # shut off first flow meter so we dont get an unexpected flow fault
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=0.0)

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            # Now verify the the WS is over budget (OB), the downstream POC, mastervalve, mainlines, are off,
            # and associated program & zone are now done
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_over_budget()

            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_off()

            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_off()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Set the budget to only account for this months watering, because that is what the message expects
            self.config.BaseStation3200[1].flow_meters[1].bicoder.vg = first_ws_budget

            # Verify ws 1 was shutdown and has message
            self.config.BaseStation3200[1].water_sources[1].messages.verify_exceed_monthly_budget_with_shutdown_message()
            self.config.BaseStation3200[1].water_sources[1].messages.clear_exceed_monthly_budget_with_shutdown_message()

            # verify second water source has continues to run
            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].mainlines[2].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # We need to account for minutes already ran from above and subtract total minutes away that we have already
            # ran.
            #   - The "- 1" accounts for the last increment clock by 1 minute above
            remaining_minutes_to_for_second_budget = minutes_to_for_second_budget - minutes_to_for_first_budget - 1

            # Run the clock until we expect the second water source to be used up, if the time is greater than 0
            if remaining_minutes_to_for_second_budget > 0:
                self.config.BaseStation3200[1].do_increment_clock(minutes=remaining_minutes_to_for_second_budget)
            self.config.BaseStation3200[1].verify_date_and_time()

            # verify message
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()
            # At this point, we expect that the WS1 has used up to its budget for the second month
            self.config.BaseStation3200[1].flow_meters[2].bicoder.vg = (2 * second_ws_budget)
            # flow meter has last month and this month usage
            self.config.BaseStation3200[1].flow_meters[2].bicoder.verify_water_usage()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            # verify second water source has continues to run
            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].mainlines[2].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Verify ws 2 was shutdown and has message
            self.config.BaseStation3200[1].water_sources[2].messages.verify_exceed_monthly_budget_message()
            self.config.BaseStation3200[1].water_sources[2].messages.clear_exceed_monthly_budget_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
