import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

__author__ = 'Eldin'


class ControllerUseCase16(object):
    """
    Test name:
        - CN UseCase16 Timed Zones
    purpose:
        - set up a zones on the controller and attach them to a program
            - Make one zone timed
            - Make one zone the primary zone
                - Make the other three into linked zones with different ratios
    Coverage Area: \n
        1. able to change zone modes: timed, primary, and linked \n
        2. able to set up run times and soak cycles and verify that they run properly \n
        3. verify linked zones follow the primary zone ratios \n
        4. able to disable zones \n
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=[1200])
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # TODO The runtime of ZoneProgram 1 was actually 3600, it just takes a long time to run
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=10)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=8)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_run_time(_minutes=12)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_cycle_time(_minutes=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_soak_time(_minutes=5)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_as_linked_zone(_primary_zone=2, _tracking_ratio=150)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[1].zone_programs[4].set_as_linked_zone(_primary_zone=2, _tracking_ratio=50)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[5].set_as_linked_zone(_primary_zone=2)

            self.config.BaseStation3200[1].zones[5].set_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller agaist information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        turn on sim mode in the controller \n
        stop the clock so that it can be incremented manually \n
        set the dial to off \n
        set the dial to run \n
        stop both Programs \n
        advance the clock 1 second \n
        set the date and time of the controller so that there is a known days of the week and days of the month \n
        advance the clock 1 second \n
        verify that the zones are a functioning correctly by getting their statuses before starting \n
        start program \n
        advance the clock 1 minute \n
        Zone Behavior:
            verify that zone 1 waits for 2 minutes at the start of the test then waters for 2 minutes and soaks for 8
            minutes then repeats \n
            verify that zone 2 starts out watering for 2 minutes then soaks for 5 minutes then repeats \n
            verify that zone 3 waits for 3 minutes at the start of the test then waters for 3 minutes and soaks for 9
            minutes then repeats \n
            verify that zone 4 waits for 9 minutes at the start of the test then waters for 1 minute and soaks for 9
            minutes then repeats \n
            verify that zone 5 is disable for the entire test \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            date_mngr.set_controller_datetime(date="04/08/2014", time="02:38:00")

            self.config.BaseStation3200[1].set_date_and_time(
                _date=date_mngr.controller_datetime.date_string_for_controller(),
                _time=date_mngr.controller_datetime.time_string_for_controller()
            )

            self.config.BaseStation3200[1].verify_date_and_time()
            
            for zone_address in range(1, 5):
                self.config.BaseStation3200[1].zones[zone_address].verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[1].verify_status_is_done()

            self.config.BaseStation3200[1].programs[1].set_program_to_start()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        zone 2 start first because it is a primary zone and zone is a timed zone
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 waiting to water
                - Zone 2 watering
                - Zone 3 waiting to water
                - Zone 4 waiting to water
                - Zone 5 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            self.config.BaseStation3200[1].zones[1].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[2].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 waiting to water
                - Zone 2 watering
                - Zone 3 waiting to water
                - Zone 4 waiting to water
                - Zone 5 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            self.config.BaseStation3200[1].zones[1].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[2].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 watering
                - Zone 2 soaking
                - Zone 3 waiting to water
                - Zone 4 waiting to water
                - Zone 5 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            self.config.BaseStation3200[1].zones[1].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 watering
                - Zone 2 soaking
                - Zone 3 waiting to water
                - Zone 4 waiting to water
                - Zone 5 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            self.config.BaseStation3200[1].zones[1].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 soaking
                - Zone 2 soaking
                - Zone 3 watering
                - Zone 4 waiting to water
                - Zone 5 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            self.config.BaseStation3200[1].zones[1].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 soaking
                - Zone 2 soaking
                - Zone 3 watering
                - Zone 4 waiting to water
                - Zone 5 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            self.config.BaseStation3200[1].zones[1].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 soaking
                - Zone 2 soaking
                - Zone 3 watering
                - Zone 4 waiting to water
                - Zone 5 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            self.config.BaseStation3200[1].zones[1].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 soaking
                - Zone 2 watering
                - Zone 3 soaking
                - Zone 4 waiting to water
                - Zone 5 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            self.config.BaseStation3200[1].zones[1].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        Verify the status for each program and zone and on the controller: \n
            - Program Status
                - Program 1 running
            - Zone Status
                - Zone 1 soaking
                - Zone 2 watering
                - Zone 3 soaking
                - Zone 4 waiting to water
                - Zone 5 disabled
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            self.config.BaseStation3200[1].zones[1].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_14(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 soaking
                 - Zone 3 soaking
                 - Zone 4 watering
                 - Zone 5 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            self.config.BaseStation3200[1].zones[1].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_15(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 soaking
                 - Zone 3 soaking
                 - Zone 4 soaking
                 - Zone 5 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            self.config.BaseStation3200[1].zones[1].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_16(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 soaking
                 - Zone 3 soaking
                 - Zone 4 soaking
                 - Zone 5 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            self.config.BaseStation3200[1].zones[1].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_17(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 watering
                 - Zone 2 soaking
                 - Zone 3 soaking
                 - Zone 4 soaking
                 - Zone 5 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            self.config.BaseStation3200[1].zones[1].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_18(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 watering
                 - Zone 2 soaking
                 - Zone 3 soaking
                 - Zone 4 soaking
                 - Zone 5 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            self.config.BaseStation3200[1].zones[1].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_19(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 watering
                 - Zone 3 soaking
                 - Zone 4 soaking
                 - Zone 5 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            self.config.BaseStation3200[1].zones[1].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_20(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 watering
                 - Zone 3 soaking
                 - Zone 4 soaking
                 - Zone 5 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            self.config.BaseStation3200[1].zones[1].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_21(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 soaking
                 - Zone 3 watering
                 - Zone 4 soaking
                 - Zone 5 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            self.config.BaseStation3200[1].zones[1].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_22(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 soaking
                 - Zone 3 watering
                 - Zone 4 soaking
                 - Zone 5 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            self.config.BaseStation3200[1].zones[1].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_23(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 soaking
                 - Zone 3 watering
                 - Zone 4 soaking
                 - Zone 5 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            self.config.BaseStation3200[1].zones[1].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_24(self):
        """
         Verify the status for each program and zone and on the controller: \n
             - Program Status
                 - Program 1 running
             - Zone Status
                 - Zone 1 soaking
                 - Zone 2 soaking
                 - Zone 3 soaking
                 - Zone 4 watering
                 - Zone 5 disabled
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            self.config.BaseStation3200[1].zones[1].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_25(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller agaist information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
