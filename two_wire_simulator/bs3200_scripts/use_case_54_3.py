import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration

# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr

__author__ = 'Tim'


class ControllerUseCase54_3(object):
    """
    Test name:
        - POC unscheduled flow shutdown
    purpose:
        -test and verify that a POC will shutdown when unscheduled flow above the limit is detected
    How the 3200 ERS says this is supposed to work:
        4.	Unscheduled Flow:
            a.	The following conditions must be met to have an unscheduled flow event:
                i.	There are no active zones watering on the mainline of the POC.
                ii.	There are no manual zones watering on the mainline of the POC.
                iii. There are no learn flow zones watering on the mainline of the POC.
                iv.	The zone delay (to MV off time) has expired.
                v.	The flow meter reading is above the limit.
                vi.	There have been three readings in a row above the limit.
            b.	There will be a message posted on an unscheduled flow event.
            c.	The POC will be shut down if the shutdown is set.
    """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                     # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        program_610am_start_time = [370]
        every_day_watering_days = [1, 1, 1, 1, 1, 1, 1]  # run every day

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_description(_ds='Unscheduled Flow Test Program')
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_610am_start_time)  # We'll start testing at 6AM, so the program shouldn't be running right away
            self.config.BaseStation3200[1].programs[1].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[1].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(_sun=True,
                                                                                                           _mon=True,
                                                                                                           _tues=True,
                                                                                                           _wed=True,
                                                                                                           _thurs=True,
                                                                                                           _fri=True,
                                                                                                           _sat=True)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=30)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=15)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_run_time(_minutes=30)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_soak_time(_minutes=15)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_run_time(_minutes=30)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_soak_time(_minutes=15)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_3(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - ws 1: keep all default attributes \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        - set up point of control 1 Attributes \n
            - attach flowmeter to POC
            - set unscheduled flow shutdown = 20 gpm
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=3)  # Have to put a MV on a POC, or 3200 will default to one!
            self.config.BaseStation3200[1].points_of_control[1].add_flow_meter_to_point_of_control(
                _flow_meter_address=2)
            self.config.BaseStation3200[1].points_of_control[1].set_unscheduled_flow_limit(_gallons=20,
                                                                                           with_shutdown_enabled=True)

            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up mainline 1 Attributes \n
                - keep all defaults
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(
                _mainline_address=1)  # Have to put a mainline on a POC, or 3200 will default to one!

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=zone_address)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        ###############################
        unscheduled flow limit shutdown  \n
        ###############################
            - Set date and time to 5:59 AM
            - set flow to 5 gpm so that it is below the unscheduled flow shutdown limit
            - advance clock 3 minutes
            - verify watering is happening
            - set flow to 21 gpm so that it is above the unscheduled flow shutdown limit
            - Because the default value of the mainline stabilization is set to time and the default value is 2 minutes
            - the controller needs to run (pipe file time + 1 minute) is order to see the unscheduled flow and trigger
            - the point of control must run for in order to trigger a shut down
            - after advancing the clock and triggering a unscheduled flow variance
            - verify that the message is posted and accurate
            - verify that the zones and programs are set to done
            - verify that the water source
            - because there is only one mainline attached to the poc the mainline should be set to error status
            - verify that the poc is set to error status
            - set flow back to 5 gpm (under the unscheduled flow shutdown limit)
            - have the controller run (pipe fill time + 1 minute) is order to make sure the unscheduled flow shutdown
              doesn't go away
            - increment the clock past the time the program would have started watering
            - verify that the program didn't start
            - reboot controller, verify that the status and messages are not lost
            - verify that the message is posted and accurate
            - verify that the zones and programs are set to done
            - verify that the water source and mainline are set to ok status and not to error or watering
            - verify that the poc is set to error status
            - verify everything stopped
            - clear message to move onto next step
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            time_for_expected_variance = self.config.BaseStation3200[1].mainlines[1].ft + 1
            # set initial time
            self.config.BaseStation3200[1].set_date_and_time(_date='2/15/2018', _time='5:59:00')
            self.config.BaseStation3200[1].flow_meters[2].bicoder.set_flow_rate(_gallons_per_minute=5)
            self.config.BaseStation3200[1].flow_meters[2].bicoder.self_test_and_update_object_attributes()

            # run clock past program start time
            self.config.BaseStation3200[1].do_increment_clock(minutes=3)

            # check zones and program watering
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_off()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()

            # set flow above the unscheduled flow shutdown limit
            self.config.BaseStation3200[1].flow_meters[2].bicoder.set_flow_rate(_gallons_per_minute=21)
            # run a self test to update the controller readings
            self.config.BaseStation3200[1].flow_meters[2].bicoder.self_test_and_update_object_attributes()
            saved_flow = self.config.BaseStation3200[1].flow_meters[2].bicoder.vr

            # let flow stabilize and then get a reading
            self.config.BaseStation3200[1].do_increment_clock(minutes=time_for_expected_variance)

            # unscheduled flow uses the three-strikes rule, so run long enough to get multiple readings
            self.config.BaseStation3200[1].do_increment_clock(minutes=3)

            # It's now controller time 6:08AM...
            # check for message
            self.config.BaseStation3200[1].points_of_control[1].messages.verify_unscheduled_flow_shutdown_message()

            # check zones and program watering
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_flow_fault()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_flow_fault()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()

            # Increment the clock past 6:10AM, so the the program would have started under normal circumstances.
            # Verify it didn't
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)

            # It's now controller time 6:12AM...
            self.config.BaseStation3200[1].points_of_control[1].messages.verify_unscheduled_flow_shutdown_message()

            # check zones and program watering
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_flow_fault()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_flow_fault()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()

            # set the flow back under the unscheduled flow limit
            self.config.BaseStation3200[1].flow_meters[2].bicoder.set_flow_rate(_gallons_per_minute=5)
            self.config.BaseStation3200[1].flow_meters[2].bicoder.self_test_and_update_object_attributes()

            # run clock long enough that flow stabilizes again
            self.config.BaseStation3200[1].do_increment_clock(minutes=time_for_expected_variance)

            # Check that the high flow shutdown message sticks around around after flow returns to normal
            # To be able to verify the text of the message correctly, we have to set the flow sensor object
            # reading to what the controller used when it fired the message.  Ugh.
            self.config.BaseStation3200[1].flow_meters[2].bicoder.vr = saved_flow
            self.config.BaseStation3200[1].points_of_control[1].messages.verify_unscheduled_flow_shutdown_message()

            # check zones and program watering
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_flow_fault()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_flow_fault()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].do_reboot_controller()

            self.config.BaseStation3200[1].do_increment_clock(minutes=3)

            # Check that the flow flow shutdown message sticks around around after a reboot.
            # To be able to verify the text of the message correctly, we have to set the flow sensor object
            # reading to what the controller used when it fired the message.  Ugh.
            self.config.BaseStation3200[1].flow_meters[2].bicoder.vr = saved_flow
            self.config.BaseStation3200[1].points_of_control[1].messages.verify_unscheduled_flow_shutdown_message()

            # check zones and program watering
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_flow_fault()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_flow_fault()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].points_of_control[1].messages.clear_unscheduled_flow_shutdown_message()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
