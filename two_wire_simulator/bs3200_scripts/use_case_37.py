import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods


__author__ = 'Tige'


class ControllerUseCase37(object):
    """
    Test name:
        - CN UseCase37 lower limit watering
    purpose:

        This test covers smart watering with sensors using lower limit watering strategy \n
        Set a program with a Primary zone using a moisture sensor in Lower Limit mode, if the Primary zone cuts off early
        due to hitting the Upper Limit, the Linked zones, should short-cycling by a proportional amount.

        1. able to search for and find moisture sensors \n
        2. able to assign and un-assign moisture sensors to primary zones \n
        3. able to set the water mode for sensor, with offset \n
        4. able to set the calibration frequency for sensors \n
        5. linked zone percentage adjust \n
        6. set zone to water using lower limit \n
        7. lower the limit and have the zones shut off \n
        8. have linked zones that are percentage adjusted against the primary zone shut off correctly in relationship to
        the primary zone \n
    Coverage Area: \n
        1. Program doesn't start at the scheduled time when moisture is above the lower limit threshold. \n
        2. Program does start at the scheduled time when moisture is below the threshold. \n
        3. Linked zones cycle through their watering and soak cycles based on their ratio of the Primary Zone. \n
        4. When the moisture goes above the threshold, current water and soak cycles are completed. \n
        5. All zones will complete their scheduled runtimes once they have started. \n
        6. No more watering occurs once the current cycle is complete, until the moisture drops below the threshold. \n
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        ############################
        Set up controller concurrency
        ############################

        - Set controller concurrent zones.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=2)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        Set up programs
        ############################

        Add program -----> to controller
            - set up program  Attributes \n
                - set enabled state  \n
                - set water window\n
                - set start times \n
                - set priority \n
                - set seasonal adjust \n
                - set watering intervals \n

        If not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n

        If using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # this is set in the PG3200 object
            program_start_time_8am = [480]
            program_watering_days = [1, 1, 1, 1, 1, 1, 1]  # runs every day
            program_water_windows = ['111111111111111111111111']

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_water_window(_ww=program_water_windows)
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=2)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(
                _sun=True, _mon=True, _tues=True, _wed=True, _thurs=True, _fri=True, _sat=True
            )
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_start_time_8am)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        Set up zones on programs
        ############################

        Add zone -----> to program
            - set up zone program  Attributes \n
                - set zone type |Timed, Primary, Linked | \n
                - set runtime\n
                - set cycle time \n
                - set soak time \n
            - if zone type |Linked| \n
                - set tracking ratio \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=30)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=30)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].add_moisture_sensor_to_primary_zone(
                _moisture_sensor_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_water_strategy_to_lower_limit()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_lower_limit_threshold(_percent=25.0)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_one_time_calibration_cycle()

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_as_linked_zone(_primary_zone=1,
                                                                                           _tracking_ratio=50)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[1].zone_programs[4].set_as_linked_zone(_primary_zone=1,
                                                                                           _tracking_ratio=150)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[5].set_as_linked_zone(_primary_zone=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ###############################
        Verify the entire configuration before reboot
        ###############################

        - Increment clock to save configuration
        - Get information for each object from controller
        - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        set the controller date and time so that there is a known days of the week and days of the month \n
        verify that all zones are working properly by verifying them before starting the test \n
         verify that the primary zones shut off
        Verify that all link zone shut off
        each zone has a 30 run time and a 10 minute cycle and a 30 minute soak
        zone 3 is 50% watering time set to 15 minutes and 10 minute cycle time and a 30 minute soak
        zone 4 is 150% watering time set to 45 minutes and 30 minute cycle time and a 30 minute soak
        do a self test on all devices \n
        increment the clock by 1 minute \n
        - Programs 1 : \n
           - verify program status Done \n
                - Zone 1 verify status Done \n
                - Zone 2 verify status Done \n
                - Zone 3 verify status Done \n
                - Zone 4 verify status Done \n
                - Zone 5 verify status Done \n

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # set the moisture sensor to be above the lower limit
            # the lower limit is set to 25
            # program 1 has a start time of 8:00 am
            # set the clock to 7:45 and then increment time to verify the program doesn't start as we are above the limit.
            # do a self test on the moisture sensor so that the 3200 will read the sensors and update there current values
            
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=26.0)
            
            for ms in sorted(self.config.BaseStation3200[1].moisture_sensors.keys()):
                self.config.BaseStation3200[1].moisture_sensors[ms].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].set_date_and_time(_date='08/28/2014',_time='7:55:00')

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)    # 7:56

            # Verify that all zones are working properly before starting the test.
            for zone in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone].verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        increment the clock by 5 minute to 8:01 verify the program doesnt start because the moisture is above the \n
        threshold\n
        - Programs 1 : \n
           - verify program status Done \n
                - Zone 1 verify status Done \n
                - Zone 2 verify status Done \n
                - Zone 3 verify status Done \n
                - Zone 4 verify status Done \n
                - Zone 5 verify status Done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # set the moisture sensor to be above the lower limit
            # the lower limit is set to 25%
            # do a self test on the moisture sensor so that the 3200 will read the sensors and update there current values
            
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=26.0)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].do_increment_clock(minutes=5)   # 8:01

            for zone in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone].verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        Set the moisture sensor below the lower limit and reset the clock to 7:45;
        increment the clock by 16 minute to 8:01 to start the program\n
        - Programs 1 : \n
           - verify program status Running \n
                - Zone 1 verify status Watering \n
                - Zone 2 verify status Watering \n
                - Zone 3 verify status Waiting \n
                - Zone 4 verify status Waiting \n
                - Zone 5 verify status Waiting \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=24.9)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].set_date_and_time(_date='08/29/2014',_time='7:55:00')
            self.config.BaseStation3200[1].do_increment_clock(minutes=6)   # 8:01

            # Expect Zones 1 and 2 to start, since we have configured 2 concurrent zones; rest should be waiting.
            self.config.BaseStation3200[1].programs[1].verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        Adjust moisture sensor by setting it to 25.1%
        increment the clock by 10 to 8:11 \n
        - Programs 1 : \n
           - verify program status Running \n
                - Zone 1 verify status Soaking \n
                - Zone 2 verify status Soaking \n
                - Zone 3 verify status Watering \n
                - Zone 4 verify status Watering\n
                - Zone 5 verify status Waiting \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=25.1)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].do_increment_clock(minutes=10)   # 8:11

            # Expect Zones 1 and 2 to be soaking now, since they have completed their first cycle,
            # and Zones 3 and 4 to start watering, with Zone 5 still waiting.
            self.config.BaseStation3200[1].programs[1].verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        Adjust moisture sensor by setting it to 25.1% this is above the lower limit
        increment the clock by 16 to 8:16 \n
        - Programs 1 : \n
           - verify program status Running \n
                - Zone 1 verify status Soaking \n
                - Zone 2 verify status Soaking \n
                - Zone 3 verify status Soaking \n
                - Zone 4 verify status Watering \n
                - Zone 5 verify status Watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=25.1)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].do_increment_clock(minutes=5)    # 8:16

            # Zone 3 is now finished, so Zone 5 gets some water.
            self.config.BaseStation3200[1].programs[1].verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].verify_status_is_watering()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        Adjust moisture sensor by setting it to 25.1% this is above the lower limit
        increment the clock by 10 to 8:26 \n
        - Programs 1 : \n
           - verify program status Running \n
                - Zone 1 verify status Soaking \n
                - Zone 2 verify status Soaking \n
                - Zone 3 verify status Soaking \n
                - Zone 4 verify status Soaking \n
                - Zone 5 verify status Soaking \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=25.1)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].do_increment_clock(minutes=10)   # 8:26

            # All zones have completed their first watering cycle; need to finish soak cycles now.
            self.config.BaseStation3200[1].programs[1].verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].zones[1].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].verify_status_is_soaking()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
       Keep Moisture set to 25.1%
        increment the clock by 15 to 8:41 \n
        - Programs 1 : \n
           - verify program status Running \n
                - Zone 1 verify status Watering \n
                - Zone 2 verify status Watering \n
                - Zone 3 verify status Soaking \n
                - Zone 4 verify status Soaking \n
                - Zone 5 verify status Soaking \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=25.1)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].do_increment_clock(minutes=15)   # 8:41

            self.config.BaseStation3200[1].programs[1].verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].verify_status_is_soaking()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
       Keep Moisture set to 25.1%
        increment the clock by 10 to 8:51 \n
        - Programs 1 : \n
           - verify program status Running \n
                - Zone 1 verify status Soaking \n
                - Zone 2 verify status Soaking \n
                - Zone 3 verify status Watering \n
                - Zone 4 verify status Soaking \n
                - Zone 5 verify status Soaking \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=25.1)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].do_increment_clock(minutes=10)   # 8:51

            # Zones 1 and 2 finish their 2nd watering cycle; Zone 3 starts water, but Zones 4 and 5 are still soaking.
            self.config.BaseStation3200[1].programs[1].verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].verify_status_is_soaking()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
       Keep Moisture set to 25.1%
        increment the clock by 5 to 8:56 \n
        - Programs 1 : \n
           - verify program status Running \n
                - Zone 1 verify status Soaking \n
                - Zone 2 verify status Soaking \n
                - Zone 3 verify status Done \n
                - Zone 4 verify status Watering \n
                - Zone 5 verify status Watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=25.1)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].do_increment_clock(minutes=5)    # 8:56

            # Zone 3 is done with 2nd watering cycle; Zones 4 and 5 can start their 2nd cycle.
            self.config.BaseStation3200[1].programs[1].verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].verify_status_is_watering()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_14(self):
        """
        Keep Moisture set to 25.1%
        increment the clock by 10 to 9:06 \n
        - Programs 1 : \n
           - verify program status Running \n
                - Zone 1 verify status Soaking \n
                - Zone 2 verify status Soaking \n
                - Zone 3 verify status Soaking \n
                - Zone 4 verify status Watering \n
                - Zone 5 verify status Soaking \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=25.1)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].do_increment_clock(minutes=10)   #  9:06

            # Zone 4 is only zone that hasn't completed its 2nd watering cycle; needs 5 more minutes.
            self.config.BaseStation3200[1].programs[1].verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].verify_status_is_soaking()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_15(self):
        """
        Keep Moisture set to 25.1%
        increment the clock by 5 to 9:11 \n
        - Programs 1 : \n
           - verify program status Running \n
                - Zone 1 verify status Soaking \n
                - Zone 2 verify status Soaking \n
                - Zone 3 verify status Soaking \n
                - Zone 4 verify status Soaking \n
                - Zone 5 verify status Soaking \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=25.1)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].do_increment_clock(minutes=5)   # 9:11

            # Zone 4 finally completes its 2nd watering cycle; all zones are done with 2nd cycle.
            self.config.BaseStation3200[1].programs[1].verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].zones[1].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].verify_status_is_soaking()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_16(self):
        """
        Keep Moisture set to 25.1%
        increment the clock by 10 to 9:21 \n
        - Programs 1 : \n
           - verify program status Running \n
                - Zone 1 verify status Watering \n
                - Zone 2 verify status Watering \n
                - Zone 3 verify status Soaking \n
                - Zone 4 verify status Soaking \n
                - Zone 5 verify status Soaking \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=25.1)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].do_increment_clock(minutes=10)    # 9:21

            # Zones 1 and 2 can start their 3rd and final watering cycle.
            self.config.BaseStation3200[1].programs[1].verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].verify_status_is_soaking()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_17(self):
        """
        Keep Moisture set to 25.1%
        increment the clock by 5 to 9:26 \n
        - Programs 1 : \n
           - verify program status Running \n
                - Zone 1 verify status Watering \n
                - Zone 2 verify status Watering \n
                - Zone 3 verify status Soaking \n
                - Zone 4 verify status Soaking \n
                - Zone 5 verify status Soaking \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=25.1)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].do_increment_clock(minutes=5)    # 9:26

            # Zones 3 and 5 have finished their 2nd soak cycle.
            self.config.BaseStation3200[1].programs[1].verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].verify_status_is_soaking()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_18(self):
        """
        Keep Moisture set to 25.1%
        increment the clock by 5 to 9:31 \n
        - Programs 1 : \n
           - verify program status Running \n
                - Zone 1 verify status Done \n
                - Zone 2 verify status Done \n
                - Zone 3 verify status Watering \n
                - Zone 4 verify status Soaking \n
                - Zone 5 verify status Soaking \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=25.1)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].do_increment_clock(minutes=5)    # 9:31

            # Zones 1 and 2 are completely done now; zones 4 and 5 need a little more soaking before they can run.
            self.config.BaseStation3200[1].programs[1].verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].verify_status_is_soaking()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_19(self):
        """
        Keep Moisture set to 25.1%
        increment the clock by 5 to 9:36 \n
        - Programs 1 : \n
           - verify program status Running \n
                - Zone 1 verify status Done \n
                - Zone 2 verify status Done \n
                - Zone 3 verify status Done \n
                - Zone 4 verify status Soaking \n
                - Zone 5 verify status Watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=25.1)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].do_increment_clock(minutes=5)    # 9:36

            # Zones 1, 2, and 3 are all now completely done; Zone 5 gets water; Zone 4 needs 5 more minutes of soaking.
            self.config.BaseStation3200[1].programs[1].verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].verify_status_is_watering()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_20(self):
        """
        Keep Moisture set to 25.1%
        increment the clock by 5 to 9:41 \n
        - Programs 1 : \n
           - verify program status Running \n
                - Zone 1 verify status Done \n
                - Zone 2 verify status Done \n
                - Zone 3 verify status Done \n
                - Zone 4 verify status Watering \n
                - Zone 5 verify status Watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=25.1)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].do_increment_clock(minutes=5)    # 9:41

            # Zone 4 is done with its 2nd soak cycle, so it can start watering.
            self.config.BaseStation3200[1].programs[1].verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].verify_status_is_watering()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_21(self):
        """
        Keep Moisture set to 25.1%
        increment the clock by 5 to 9:46 \n
        - Programs 1 : \n
           - verify program status Running \n
                - Zone 1 verify status Done \n
                - Zone 2 verify status Done \n
                - Zone 3 verify status Done \n
                - Zone 4 verify status Watering \n
                - Zone 5 verify status Done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=25.1)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].do_increment_clock(minutes=5)    # 9:46

            # Zone 5 is all done now; Zone 4 is the only zone left to water.
            self.config.BaseStation3200[1].programs[1].verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_22(self):
        """
        Keep Moisture set to 25.1%
        increment the clock by 10 to 9:56 \n
        - Programs 1 : \n
           - verify program status Running \n
                - Zone 1 verify status Done \n
                - Zone 2 verify status Done \n
                - Zone 3 verify status Done \n
                - Zone 4 verify status Done \n
                - Zone 5 verify status Done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=25.1)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].do_increment_clock(minutes=10)    # 9:56

            # All zones are done now!
            self.config.BaseStation3200[1].programs[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
