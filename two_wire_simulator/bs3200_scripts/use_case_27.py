import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

__author__ = "Eldin"


class ControllerUseCase27(object):
    """
    Test name: \n
        Watering Day Schedule \n
    Purpose: \n
        Ensure that Programs are triggered correctly using different watering schedules. We test odd days, even days,
        odd days besides 31st, specific days of the week, and semi-month intervals. \n
    Coverage area: \n
        1.	Set up a Programs to water for the different watering scheduled modes: \n
        2.  Set up a week days watering schedule and verify that the program only runs on week days set in the watering
            schedule \n
        3.  Set up an interval watering schedule for 3 days and verify that the program runs then skips 3 days and runs on
            the 4th \n
        4.  Set up an even days watering schedule and verify that the program only runs on even days of the week \n
        5.  Set up an odd days watering schedule and verify that the program only runs on odd days of the week \n
        6.  Set up an odd days skip 31st watering schedule and verify that the program only runs on odd days of the week
            and skips the 31st \n
        7.  Set up a historical ET calendar and verify that the set intervals work and that the interval changes every half
            a month \n
    """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                     # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        - Initialize Each Program. \n
            - Program 1:
                - Max Concurrent Zones: 1 \n
                - Calendar Interval: Week Days \n
                - Week Days: Monday, Wednesday, Friday \n
                - Start Times: 8 PM \n
            - Program 2:
                - Max Concurrent Zones: 1 \n
                - Calendar Interval: Even Days \n
                - Start Times: 8 PM \n
            - Program 3:
                - Max Concurrent Zones: 1 \n
                - Calendar Interval: Odd Days \n
                - Start Times: 8 PM \n
            - Program 4:
                - Max Concurrent Zones: 1 \n
                - Calendar Interval: Odd Days Skip 31 \n
                - Day Interval: 3 \n
                - Start Times: 8 PM \n
            - Program 5:
                - Max Concurrent Zones: 1 \n
                - Calendar Interval: Historical Calendar \n
                - Semi-Monthly: 13, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 \n
                - Start Times: 8 PM \n
            - Program 6:
                - Max Concurrent Zones: 1 \n
                - Calendar Interval: Interval Days \n
                - Day Interval: 3 \n
                - Start Times: 8 PM \n
            - Program 7:
                - Disabled \n
                - Max Concurrent Zones: 1 \n
                - Calendar Interval: Interval Days \n
                - Day Interval: 3 \n
                - Start Times: 8 PM \n
        """
        # this is set in the PG3200 object
        program_start_time_8_pm = [1200]

        program_week_day_mon_wed_fri = [0, 1, 0, 1, 0, 1, 0]

        semi_monthly_interval = [13, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_start_time_8_pm)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(
                _sun=False, _mon=True, _tues=False, _wed=True, _thurs=False, _fri=True, _sat=False
            )

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation3200[1].programs[2].set_enabled()
            self.config.BaseStation3200[1].programs[2].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[2].set_start_times(_st_list=program_start_time_8_pm)
            self.config.BaseStation3200[1].programs[2].set_watering_intervals_to_even_days()

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=3)
            self.config.BaseStation3200[1].programs[3].set_enabled()
            self.config.BaseStation3200[1].programs[3].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[3].set_start_times(_st_list=program_start_time_8_pm)
            self.config.BaseStation3200[1].programs[3].set_watering_intervals_to_odd_days()

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=4)
            self.config.BaseStation3200[1].programs[4].set_enabled()
            self.config.BaseStation3200[1].programs[4].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[4].set_start_times(_st_list=program_start_time_8_pm)
            self.config.BaseStation3200[1].programs[4].set_watering_intervals_to_odd_days_but_skipping_the_31st_day_of_the_month()

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=5)
            self.config.BaseStation3200[1].programs[5].set_enabled()
            self.config.BaseStation3200[1].programs[5].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[5].set_start_times(_st_list=program_start_time_8_pm)
            self.config.BaseStation3200[1].programs[5].set_watering_intervals_to_semi_monthly(_sm=semi_monthly_interval)

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=6)
            self.config.BaseStation3200[1].programs[6].set_enabled()
            self.config.BaseStation3200[1].programs[6].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[6].set_start_times(_st_list=program_start_time_8_pm)
            self.config.BaseStation3200[1].programs[6].set_watering_intervals_to_day_intervals(_di=3)

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=7)
            self.config.BaseStation3200[1].programs[7].set_disabled()
            self.config.BaseStation3200[1].programs[7].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[7].set_start_times(_st_list=program_start_time_8_pm)
            self.config.BaseStation3200[1].programs[7].set_watering_intervals_to_day_intervals(_di=3)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        - Set up Zone Programs \n
            - Zone 1 Program 1 (Timed Zone) \n
                - Run Time: 300 \n
            - Zone 2 Program 2 (Timed Zone) \n
                - Run Time: 300 \n
            - Zone 3 Program 3 (Timed Zone) \n
                - Run Time: 300 \n
            - Zone 4 Program 4 (Timed Zone) \n
                - Run Time: 300 \n
            - Zone 5 Program 5 (Timed Zone) \n
                - Run Time: 300 \n
            - Zone 6 Program 6 (Timed Zone) \n
                - Run Time: 300 \n
            - Zone 7 Program 7 (Primary Zone) \n
                - Run Time: 300 \n
                - Moisture Sensor: SB05308 (1) \n
                - Watering Strategy: Lower Limit \n
                - Lower Limit: 26 \n
                - Calibration Cycle: Single \n
                - Linked Zones: \n
                    - Zone 9 Program 7 \n
                        - Tracking Ratio: 100% \n
            - Zone 8 Program 7 (Timed Zone) \n
                - Run Time: 300 \n
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=5)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[2].zone_programs[2].set_run_time(_minutes=5)

            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[3].zone_programs[3].set_run_time(_minutes=5)

            self.config.BaseStation3200[1].programs[4].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[4].zone_programs[4].set_run_time(_minutes=5)

            self.config.BaseStation3200[1].programs[5].add_zone_to_program(_zone_address=5)
            self.config.BaseStation3200[1].programs[5].zone_programs[5].set_run_time(_minutes=5)

            self.config.BaseStation3200[1].programs[6].add_zone_to_program(_zone_address=6)
            self.config.BaseStation3200[1].programs[6].zone_programs[6].set_run_time(_minutes=5)

            self.config.BaseStation3200[1].programs[7].add_zone_to_program(_zone_address=7)
            self.config.BaseStation3200[1].programs[7].zone_programs[7].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[7].zone_programs[7].set_run_time(_minutes=5)
            self.config.BaseStation3200[1].programs[7].zone_programs[7].add_moisture_sensor_to_primary_zone(_moisture_sensor_address=1)
            self.config.BaseStation3200[1].programs[7].zone_programs[7].set_water_strategy_to_lower_limit()
            self.config.BaseStation3200[1].programs[7].zone_programs[7].set_lower_limit_threshold(_percent=26.0)
            self.config.BaseStation3200[1].programs[7].zone_programs[7].set_one_time_calibration_cycle()

            self.config.BaseStation3200[1].programs[7].add_zone_to_program(_zone_address=8)
            self.config.BaseStation3200[1].programs[7].zone_programs[8].set_run_time(_minutes=5)

            self.config.BaseStation3200[1].programs[7].add_zone_to_program(_zone_address=9)
            self.config.BaseStation3200[1].programs[7].zone_programs[9].set_as_linked_zone(_primary_zone=7)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        - Set the date and time to 07/25/2012 (Day 1) (Odd Day), 8:00 PM \n
            - Increment the time by 1 minute. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Running because its start time of 8:00 PM was triggered. \n
                - Program 2: Done Watering because it only runs on even days, and 07/25 (date on controller) is odd. \n
                - Program 3: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 4: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 5: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 6: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 7: disabled. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done because it ran for it's runtime of 5 minutes. \n
                - Program 2: Done Watering because it only runs on even days, and 07/25 (date on controller) is odd. \n
                - Program 3: Running because its start time of 8:00 PM was triggered. \n
                - Program 4: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 5: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 6: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 7: disabled. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done because it ran for it's runtime of 5 minutes. \n
                - Program 2: Done Watering because it only runs on even days, and 07/25 (date on controller) is odd. \n
                - Program 3: Done because it ran for it's runtime of 5 minutes. \n
                - Program 4: Running because its start time of 8:00 PM was triggered. \n
                - Program 5: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 6: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 7: disabled. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done because it ran for it's runtime of 5 minutes. \n
                - Program 2: Done Watering because it only runs on even days, and 07/25 (date on controller) is odd. \n
                - Program 3: Done because it ran for it's runtime of 5 minutes. \n
                - Program 4: Done because it ran for it's runtime of 5 minutes. \n
                - Program 5: Running because its start time of 8:00 PM was triggered. \n
                - Program 6: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 7: disabled. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done because it ran for it's runtime of 5 minutes. \n
                - Program 2: Done Watering because it only runs on even days, and 07/25 (date on controller) is odd. \n
                - Program 3: Done because it ran for it's runtime of 5 minutes. \n
                - Program 4: Done because it ran for it's runtime of 5 minutes. \n
                - Program 5: Done because it ran for it's runtime of 5 minutes. \n
                - Program 6: Running because its start time of 8:00 PM was triggered. \n
                - Program 7: disabled. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # *********************************************** Day One ************************************************ #
            # Set the controller's time to 8:00 PM
            self.config.BaseStation3200[1].set_date_and_time(_date='07/25/2012', _time='19:59:00')
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        - Set the date and time to 07/26/2012 (Day 2) (Even Day), 8:00 PM \n
            - Increment the time by 1 minute. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done Watering because it only waters Monday, Wednesday, and Friday. \n
                - Program 2: Running because it's start time was triggered and it is an even day. \n
                - Program 3: Done Watering because it only waters on odd days, and the controller is on an even day. \n
                - Program 4: Done Watering because it only waters on odd days, and the controller is on an even day. \n
                - Program 5: Done Watering because of it's semi-month interval. \n
                - Program 6: Done Watering because it waters every third day. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done Watering because it only waters Monday, Wednesday, and Friday. \n
                - Program 2: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 3: Done Watering because it only waters on odd days, and the controller is on an even day. \n
                - Program 4: Done Watering because it only waters on odd days, and the controller is on an even day. \n
                - Program 5: Done Watering because of it's semi-month interval. \n
                - Program 6: Done Watering because it waters every third day. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # *********************************************** Day Two ************************************************ #
            # Set the controller's time to 8:00 PM
            self.config.BaseStation3200[1].set_date_and_time(_date='07/26/2012', _time='20:00:00')
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        - Set the date and time to 07/27/2012 (Day 3) (Odd Day), 8:00 PM \n
            - Increment the time by 1 minute. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Running because its start time of 8:00 PM was triggered. \n
                - Program 2: Done Watering because it only runs on even days, and 07/27 (date on controller) is odd. \n
                - Program 3: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 4: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 5: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 6: Done watering because it waters every third day. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 2: Done Watering because it only runs on even days, and 07/27 (date on controller) is odd. \n
                - Program 3: Running because its start time of 8:00 PM was triggered, and it's an odd day. \n
                - Program 4: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 5: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 6: Done watering because it waters every third day. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 2: Done Watering because it only runs on even days, and 07/27 (date on controller) is odd. \n
                - Program 3: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 4: Running because its start time of 8:00 PM was triggered, and it's an odd day. \n
                - Program 5: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 6: Done watering because it waters every third day. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 2: Done Watering because it only runs on even days, and 07/27 (date on controller) is odd. \n
                - Program 3: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 4: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 5: Running because its start time of 8:00 PM was triggered. \n
                - Program 6: Done watering because it waters every third day. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 2: Done Watering because it only runs on even days, and 07/27 (date on controller) is odd. \n
                - Program 3: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 4: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 5: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 6: Done watering because it waters every third day. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # ********************************************** Day Three *********************************************** #
            # Set the controller's time to 8:00 PM
            self.config.BaseStation3200[1].set_date_and_time(_date='07/27/2012', _time='20:00:00')
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        - Set the date and time to 07/28/2012 (Day 4) (Even Day), 8:00 PM \n
            - Increment the time by 1 minute. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done Watering because it only runs on Monday, Wednesday, and Friday. \n
                - Program 2: Running because its start time of 8:00 PM was triggered and it's an even day. \n
                - Program 3: Done Watering because it only runs on odd days, and 07/25 (date on controller) is even. \n
                - Program 4: Done Watering because it only runs on odd days, and 07/25 (date on controller) is even. \n
                - Program 5: Done Watering because of it's semi-monthly interval. \n
                - Program 6: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done Watering because it only runs on Monday, Wednesday, and Friday. \n
                - Program 2: Done because it ran for it's runtime of 5 minutes. \n
                - Program 3: Done Watering because it only runs on odd days, and 07/25 (date on controller) is even. \n
                - Program 4: Done Watering because it only runs on odd days, and 07/25 (date on controller) is even. \n
                - Program 5: Done Watering because of it's semi-monthly interval. \n
                - Program 6: Running because its start time of 8:00 PM was triggered and it's a 3 day interval. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done Watering because it only runs on Monday, Wednesday, and Friday. \n
                - Program 2: Done because it ran for it's runtime of 5 minutes. \n
                - Program 3: Done Watering because it only runs on odd days, and 07/25 (date on controller) is even. \n
                - Program 4: Done Watering because it only runs on odd days, and 07/25 (date on controller) is even. \n
                - Program 5: Done Watering because of it's semi-monthly interval. \n
                - Program 2: Done because it ran for it's runtime of 5 minutes. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # *********************************************** Day Four *********************************************** #
            # Set the controller's time to 8:00 PM
            self.config.BaseStation3200[1].set_date_and_time(_date='07/28/2012', _time='20:00:00')
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)


            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        - Set the date and time to 07/29/2012 (Day 5) (Odd Day), 8:00 PM \n
            - Increment the time by 1 minute. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done Watering because it only runs on Monday, Wednesday, and Friday. \n
                - Program 2: Done Watering because it only runs on even days, and 07/29 (date on controller) is odd. \n
                - Program 3: Running because its start time of 8:00 PM was triggered and it's an odd day. \n
                - Program 4: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 5: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 6: Done Watering because it waters every third day. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done Watering because it only runs on Monday, Wednesday, and Friday. \n
                - Program 2: Done Watering because it only runs on even days, and 07/29 (date on controller) is odd. \n
                - Program 3: Done because it ran for it's runtime of 5 minutes. \n
                - Program 4: Running because its start time of 8:00 PM was triggered and it's an odd day. \n
                - Program 5: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 6: Done Watering because it waters every third day. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done Watering because it only runs on Monday, Wednesday, and Friday. \n
                - Program 2: Done Watering because it only runs on even days, and 07/29 (date on controller) is odd. \n
                - Program 3: Done because it ran for it's runtime of 5 minutes. \n
                - Program 4: Done because it ran for it's runtime of 5 minutes. \n
                - Program 5: Running because its start time of 8:00 PM was triggered. \n
                - Program 6: Done Watering because it waters every third day. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done Watering because it only runs on Monday, Wednesday, and Friday. \n
                - Program 2: Done Watering because it only runs on even days, and 07/29 (date on controller) is odd. \n
                - Program 3: Done because it ran for it's runtime of 5 minutes. \n
                - Program 4: Done because it ran for it's runtime of 5 minutes. \n
                - Program 5: Done because it ran for it's runtime of 5 minutes. \n
                - Program 6: Done Watering because it waters every third day. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # *********************************************** Day Five *********************************************** #
            # Set the controller's time to 8:00 PM
            self.config.BaseStation3200[1].set_date_and_time(_date='07/29/2012', _time='20:00:00')
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        - Set the date and time to 07/30/2012 (Day 6) (Even Day), 8:00 PM \n
            - Increment the time by 1 minute. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Running because its start time of 8:00 PM was triggered and it's a Monday. \n
                - Program 2: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 3: Done Watering because it only runs on odd days, and 07/25 (date on controller) is even. \n
                - Program 4: Done Watering because it only runs on odd days, and 07/25 (date on controller) is even. \n
                - Program 5: Done Watering because of it's semi-monthly interval. \n
                - Program 6: Done Watering because it runs on every third day. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done because it ran for it's runtime of 5 minutes. \n
                - Program 2: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 3: Done Watering because it only runs on odd days, and 07/25 (date on controller) is even. \n
                - Program 4: Done Watering because it only runs on odd days, and 07/25 (date on controller) is even. \n
                - Program 5: Done Watering because of it's semi-monthly interval. \n
                - Program 6: Done Watering because it runs on every third day. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done because it ran for it's runtime of 5 minutes. \n
                - Program 2: Done because it ran for it's runtime of 5 minutes. \n
                - Program 3: Done Watering because it only runs on odd days, and 07/25 (date on controller) is even. \n
                - Program 4: Done Watering because it only runs on odd days, and 07/25 (date on controller) is even. \n
                - Program 5: Done Watering because of it's semi-monthly interval. \n
                - Program 6: Done Watering because it runs on every third day. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # *********************************************** Day Six ************************************************ #
            # Set the controller's time to 8:00 PM
            self.config.BaseStation3200[1].set_date_and_time(_date='07/30/2012', _time='20:00:00')
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        - Set the date and time to 07/31/2012 (Day 7) (Odd Day), 8:00 PM \n
            - Increment the time by 1 minute. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done Watering because it only runs on Monday, Wednesday, and Friday. \n
                - Program 2: Done Watering because it only runs on even days, and 07/29 (date on controller) is odd. \n
                - Program 3: Running because its start time of 8:00 PM was triggered and it's an odd day. \n
                - Program 4: Done Watering because it skips the 31st of each month. \n
                - Program 5: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 6: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done Watering because it only runs on Monday, Wednesday, and Friday. \n
                - Program 2: Done Watering because it only runs on even days, and 07/29 (date on controller) is odd. \n
                - Program 3: Done because it ran for it's runtime of 5 minutes. \n
                - Program 4: Done Watering because it skips the 31st of each month. \n
                - Program 5: Running because its start time of 8:00 PM was triggered. \n
                - Program 6: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done Watering because it only runs on Monday, Wednesday, and Friday. \n
                - Program 2: Done Watering because it only runs on even days, and 07/29 (date on controller) is odd. \n
                - Program 3: Done because it ran for it's runtime of 5 minutes. \n
                - Program 4: Done Watering because it skips the 31st of each month. \n
                - Program 5: Done because it ran for it's runtime of 5 minutes. \n
                - Program 6: Running because its start time of 8:00 PM was triggered. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done Watering because it only runs on Monday, Wednesday, and Friday. \n
                - Program 2: Done Watering because it only runs on even days, and 07/29 (date on controller) is odd. \n
                - Program 3: Done because it ran for it's runtime of 5 minutes. \n
                - Program 4: Done Watering because it skips the 31st of each month. \n
                - Program 5: Done because it ran for it's runtime of 5 minutes. \n
                - Program 6: Done because it ran for it's runtime of 5 minutes. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # *********************************************** Day Seven ********************************************** #
            # Set the controller's time to 8:00 PM
            self.config.BaseStation3200[1].set_date_and_time(_date='07/31/2012', _time='20:00:00')
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        - Set the date and time to 08/01/2012 (Day 8) (Odd Day), 8:00 PM \n
            - Increment the time by 1 minute. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Running because its start time of 8:00 PM was triggered and it's a Wednesday. \n
                - Program 2: Done Watering because it only runs on even days, and 08/01 (date on controller) is odd. \n
                - Program 3: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 4: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 5: Done Watering because of it's semi-monthly interval. \n
                - Program 6: Done Watering because it only runs on every third day. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done because it ran for it's runtime of 5 minutes. \n
                - Program 2: Done Watering because it only runs on even days, and 08/01 (date on controller) is odd. \n
                - Program 3: Running because its start time of 8:00 PM was triggered and it's an odd day. \n
                - Program 4: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 5: Done Watering because of it's semi-monthly interval. \n
                - Program 6: Done Watering because it only runs on every third day. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done because it ran for it's runtime of 5 minutes. \n
                - Program 2: Done Watering because it only runs on even days, and 08/01 (date on controller) is odd. \n
                - Program 3: Done because it ran for it's runtime of 5 minutes. \n
                - Program 4: Running because its start time of 8:00 PM was triggered and it's an odd day. \n
                - Program 5: Done Watering because of it's semi-monthly interval. \n
                - Program 6: Done Watering because it only runs on every third day. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done because it ran for it's runtime of 5 minutes. \n
                - Program 2: Done Watering because it only runs on even days, and 08/01 (date on controller) is odd. \n
                - Program 3: Done because it ran for it's runtime of 5 minutes. \n
                - Program 4: Done because it ran for it's runtime of 5 minutes. \n
                - Program 5: Done Watering because of it's semi-monthly interval. \n
                - Program 6: Done Watering because it only runs on every third day. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # *********************************************** Day Eight ********************************************** #
            # Set the controller's time to 8:00 PM
            self.config.BaseStation3200[1].set_date_and_time(_date='08/01/2012', _time='20:00:00')
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)


            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        - Set the date and time to 08/02/2012 (Day 9) (Even Day), 8:00 PM \n
            - Increment the time by 1 minute. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done Watering because it only waters Monday, Wednesday, and Friday. \n
                - Program 2: Running because it's start time was triggered and it is an even day. \n
                - Program 3: Done Watering because it only waters on odd days, and the controller is on an even day. \n
                - Program 4: Done Watering because it only waters on odd days, and the controller is on an even day. \n
                - Program 5: Done Watering because of it's semi-month interval. \n
                - Program 6: Done Watering because it waters every third day. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done Watering because it only waters Monday, Wednesday, and Friday. \n
                - Program 2: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 3: Done Watering because it only waters on odd days, and the controller is on an even day. \n
                - Program 4: Done Watering because it only waters on odd days, and the controller is on an even day. \n
                - Program 5: Done Watering because of it's semi-month interval. \n
                - Program 6: Done Watering because it waters every third day. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # *********************************************** Day Nine *********************************************** #
            # Set the controller's time to 8:00 PM
            self.config.BaseStation3200[1].set_date_and_time(_date='08/02/2012', _time='20:00:00')
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        - Set the date and time to 08/03/2012 (Day 10) (Odd Day), 8:00 PM \n
            - Increment the time by 1 minute. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Running because its start time of 8:00 PM was triggered, and it's a Friday. \n
                - Program 2: Done Watering because it only runs on even days, and 07/27 (date on controller) is odd. \n
                - Program 3: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 4: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 5: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 6: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 2: Done Watering because it only runs on even days, and 07/27 (date on controller) is odd. \n
                - Program 3: Running because its start time of 8:00 PM was triggered, and it's an odd day. \n
                - Program 4: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 5: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 6: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 2: Done Watering because it only runs on even days, and 07/27 (date on controller) is odd. \n
                - Program 3: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 4: Running because its start time of 8:00 PM was triggered, and it's an odd day. \n
                - Program 5: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 6: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 2: Done Watering because it only runs on even days, and 07/27 (date on controller) is odd. \n
                - Program 3: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 4: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 5: Running because its start time of 8:00 PM was triggered. \n
                - Program 6: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 2: Done Watering because it only runs on even days, and 07/27 (date on controller) is odd. \n
                - Program 3: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 4: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 5: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 6: Running because its start time of 8:00 PM was triggered, and it is a third day. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 2: Done Watering because it only runs on even days, and 07/27 (date on controller) is odd. \n
                - Program 3: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 4: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 5: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 6: Done Watering because it ran for it's runtime of 5 minutes. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # ********************************************** Day Ten ************************************************* #
            # Set the controller's time to 8:00 PM
            self.config.BaseStation3200[1].set_date_and_time(_date='08/03/2012', _time='20:00:00')
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        - Set the date and time to 08/04/2012 (Day 11) (Even Day), 8:00 PM \n
            - Increment the time by 1 minute. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done Watering because it only waters Monday, Wednesday, and Friday. \n
                - Program 2: Running because it's start time was triggered and it is an even day. \n
                - Program 3: Done Watering because it only waters on odd days, and the controller is on an even day. \n
                - Program 4: Done Watering because it only waters on odd days, and the controller is on an even day. \n
                - Program 5: Done Watering because of it's semi-month interval. \n
                - Program 6: Done Watering because it waters every third day. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done Watering because it only waters Monday, Wednesday, and Friday. \n
                - Program 2: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 3: Done Watering because it only waters on odd days, and the controller is on an even day. \n
                - Program 4: Done Watering because it only waters on odd days, and the controller is on an even day. \n
                - Program 5: Done Watering because of it's semi-month interval. \n
                - Program 6: Done Watering because it waters every third day. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # *********************************************** Day Eleven ********************************************* #
            # Set the controller's time to 8:00 PM
            self.config.BaseStation3200[1].set_date_and_time(_date='08/04/2012', _time='20:00:00')
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_14(self):
        """
        - Set the date and time to 08/05/2012 (Day 12) (Odd Day), 8:00 PM \n
            - Increment the time by 1 minute. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done Watering because it only runs on Monday, Wednesday, and Friday. \n
                - Program 2: Done Watering because it only runs on even days, and 07/25 (date on controller) is odd. \n
                - Program 3: Running because its start time of 8:00 PM was triggered and it's an odd day. \n
                - Program 4: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 5: Done Watering because of its semi-monthly interval. \n
                - Program 6: Done Watering because of it only runs on every third day. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done Watering because it only runs on Monday, Wednesday, and Friday. \n
                - Program 2: Done Watering because it only runs on even days, and 07/25 (date on controller) is odd. \n
                - Program 3: Done because it ran for it's runtime of 5 minutes. \n
                - Program 4: Running because its start time of 8:00 PM was triggered and it's an odd day. \n
                - Program 5: Done Watering because of its semi-monthly interval. \n
                - Program 6: Done Watering because of it only runs on every third day. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done Watering because it only runs on Monday, Wednesday, and Friday. \n
                - Program 2: Done Watering because it only runs on even days, and 07/25 (date on controller) is odd. \n
                - Program 3: Done because it ran for it's runtime of 5 minutes. \n
                - Program 4: Done because it ran for it's runtime of 5 minutes. \n
                - Program 5: Done Watering because of its semi-monthly interval. \n
                - Program 6: Done Watering because of it only runs on every third day. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # *********************************************** Day Twelve ********************************************* #
            # Set the controller's time to 8:00 PM
            self.config.BaseStation3200[1].set_date_and_time(_date='08/05/2012', _time='20:00:00')
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)


            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Get data from each Program on the controller
            # for pg in sorted(self.config.BaseStation3200[1].programs.keys()):
            #     self.config.BaseStation3200[1].programs[pg].get_data()

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_15(self):
        """
        - Set the date and time to 08/06/2012 (Day 13) (Even Day), 8:00 PM \n
            - Increment the time by 1 minute. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Running because its start time of 8:00 PM was triggered, and it's a Friday. \n
                - Program 2: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 3: Done Watering because it only runs on odd days, and 07/27 (date on controller) is even. \n
                - Program 4: Done Watering because it only runs on odd days, and 07/27 (date on controller) is even. \n
                - Program 5: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 6: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 2: Done Watering because it only runs on even days, and 07/27 (date on controller) is odd. \n
                - Program 3: Running because its start time of 8:00 PM was triggered, and it's an odd day. \n
                - Program 4: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 5: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 6: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 2: Done Watering because it only runs on even days, and 07/27 (date on controller) is odd. \n
                - Program 3: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 4: Running because its start time of 8:00 PM was triggered, and it's an odd day. \n
                - Program 5: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
                - Program 6: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 2: Done Watering because it only runs on even days, and 07/27 (date on controller) is odd. \n
                - Program 3: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 4: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 5: Running because its start time of 8:00 PM was triggered. \n
                - Program 6: Waiting because its start time of 8:00 PM was triggered, but max concurrent zones are 1. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 2: Done Watering because it only runs on even days, and 07/27 (date on controller) is odd. \n
                - Program 3: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 4: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 5: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 6: Running because its start time of 8:00 PM was triggered, and it is a third day. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 2: Done Watering because it only runs on even days, and 07/27 (date on controller) is odd. \n
                - Program 3: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 4: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 5: Done Watering because it ran for it's runtime of 5 minutes. \n
                - Program 6: Done Watering because it ran for it's runtime of 5 minutes. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # ********************************************** Day Thirteen ******************************************** #
            # Set the controller's time to 8:00 PM
            self.config.BaseStation3200[1].set_date_and_time(_date='08/03/2012', _time='20:00:00')
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Get data from each Program on the controller
            # for pg in sorted(self.config.BaseStation3200[1].programs.keys()):
            #     self.config.BaseStation3200[1].programs[pg].get_data()

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)
            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_16(self):
        """
        - Disable Programs: 1, 2, 3, 4, 5, 6 \n
        - Enable Program: 7 \n
            - This has 3 zones attached to it: 7, 8, 9 \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].programs[1].set_disabled()
            self.config.BaseStation3200[1].programs[2].set_disabled()
            self.config.BaseStation3200[1].programs[3].set_disabled()
            self.config.BaseStation3200[1].programs[4].set_disabled()
            self.config.BaseStation3200[1].programs[5].set_disabled()
            self.config.BaseStation3200[1].programs[6].set_disabled()
            self.config.BaseStation3200[1].programs[7].set_enabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_17(self):
        """
        - Set moisture sensor SB05308's moisture percent to be 27.0, which is above the lower limit of 26.0. \n
        - Set the date and time to 07/25/2012 (Day 1) (Odd Day), 8:00 PM \n
            - Increment the time by 1 minute. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Disabled. \n
                - Program 2: Disabled. \n
                - Program 3: Disabled. \n
                - Program 4: Disabled. \n
                - Program 5: Disabled. \n
                - Program 6: Disabled. \n
                - Program 7: Running because the program starts every three days. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Disabled. \n
                - Program 2: Disabled. \n
                - Program 3: Disabled. \n
                - Program 4: Disabled. \n
                - Program 5: Disabled. \n
                - Program 6: Disabled. \n
                - Program 7: Done Watering because it ran for it's runtime of 15 minutes. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # *********************************************** Day One ************************************************ #
            # Set the moisture sensor to a time above the lower limit and do a self test on it
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=27.0)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.do_self_test()

            # Set the controller's time to 8:00 PM
            self.config.BaseStation3200[1].set_date_and_time(_date='07/25/2012', _time='20:00:00')
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_running()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=15)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_18(self):
        """
        - Set the date and time to 07/26/2012 (Day 2) (Even Day), 8:00 PM \n
            - Increment the time by 1 minute. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Disabled. \n
                - Program 2: Disabled. \n
                - Program 3: Disabled. \n
                - Program 4: Disabled. \n
                - Program 5: Disabled. \n
                - Program 6: Disabled. \n
                - Program 7: Done watering because it only waters on every third day. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Disabled. \n
                - Program 2: Disabled. \n
                - Program 3: Disabled. \n
                - Program 4: Disabled. \n
                - Program 5: Disabled. \n
                - Program 6: Disabled. \n
                - Program 7: Done watering because it only waters on every third day. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # *********************************************** Day Two ************************************************ #
            # Set the controller's time to 8:00 PM
            self.config.BaseStation3200[1].set_date_and_time(_date='07/26/2012', _time='20:00:00')
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_done()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_19(self):
        """
        - Set the date and time to 07/27/2012 (Day 3) (Even Day), 8:00 PM \n
            - Increment the time by 1 minute. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Disabled. \n
                - Program 2: Disabled. \n
                - Program 3: Disabled. \n
                - Program 4: Disabled. \n
                - Program 5: Disabled. \n
                - Program 6: Disabled. \n
                - Program 7: Done watering because it only waters on every third day. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Disabled. \n
                - Program 2: Disabled. \n
                - Program 3: Disabled. \n
                - Program 4: Disabled. \n
                - Program 5: Disabled. \n
                - Program 6: Disabled. \n
                - Program 7: Done watering because it only waters on every third day. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # *********************************************** Day Three ********************************************** #
            # Set the controller's time to 8:00 PM
            self.config.BaseStation3200[1].set_date_and_time(_date='07/27/2012', _time='20:00:00')
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_done()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_20(self):
        """
        - Set the date and time to 07/28/2012 (Day 4) (Even Day), 8:00 PM \n
            - Increment the time by 1 minute. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Disabled. \n
                - Program 2: Disabled. \n
                - Program 3: Disabled. \n
                - Program 4: Disabled. \n
                - Program 5: Disabled. \n
                - Program 6: Disabled. \n
                - Program 7: Running because the program starts every three days. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Disabled. \n
                - Program 2: Disabled. \n
                - Program 3: Disabled. \n
                - Program 4: Disabled. \n
                - Program 5: Disabled. \n
                - Program 6: Disabled. \n
                - Program 7: Done Watering because it ran for it's runtime of 15 minutes. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # *********************************************** Day Four *********************************************** #
            # Set the controller's time to 8:00 PM
            self.config.BaseStation3200[1].set_date_and_time(_date='07/28/2012', _time='20:00:00')
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_running()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=15)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_21(self):
        """
        - Set moisture sensor SB05308's moisture percent to be 23.0, which is below the lower limit of 26.0. \n
            - This will trigger, allowing the program to water. \n
        - Set the date and time to 07/29/2012 (Day 5) (Odd Day), 8:00 PM \n
            - Increment the time by 1 minute. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Disabled. \n
                - Program 2: Disabled. \n
                - Program 3: Disabled. \n
                - Program 4: Disabled. \n
                - Program 5: Disabled. \n
                - Program 6: Disabled. \n
                - Program 7: Done watering because it only waters on every third day. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Disabled. \n
                - Program 2: Disabled. \n
                - Program 3: Disabled. \n
                - Program 4: Disabled. \n
                - Program 5: Disabled. \n
                - Program 6: Disabled. \n
                - Program 7: Done watering because it only waters on every third day. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # *********************************************** Day Five *********************************************** #
            # Set the moisture sensor to a time above the lower limit and do a self test on it
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=23.0)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.do_self_test()

            # Set the controller's time to 8:00 PM
            self.config.BaseStation3200[1].set_date_and_time(_date='07/29/2012', _time='20:00:00')
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_done()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_22(self):
        """
        - Set the date and time to 07/30/2012 (Day 6) (Even Day), 8:00 PM \n
            - Increment the time by 1 minute. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Disabled. \n
                - Program 2: Disabled. \n
                - Program 3: Disabled. \n
                - Program 4: Disabled. \n
                - Program 5: Disabled. \n
                - Program 6: Disabled. \n
                - Program 7: Done watering because it only waters on every third day. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Disabled. \n
                - Program 2: Disabled. \n
                - Program 3: Disabled. \n
                - Program 4: Disabled. \n
                - Program 5: Disabled. \n
                - Program 6: Disabled. \n
                - Program 7: Done watering because it only waters on every third day. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # *********************************************** Day Six ************************************************ #
            # Set the controller's time to 8:00 PM
            self.config.BaseStation3200[1].set_date_and_time(_date='07/30/2012', _time='20:00:00')
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_done()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_23(self):
        """
        - Set the date and time to 07/31/2012 (Day 7) (Odd Day), 8:00 PM \n
            - Increment the time by 1 minute. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Disabled. \n
                - Program 2: Disabled. \n
                - Program 3: Disabled. \n
                - Program 4: Disabled. \n
                - Program 5: Disabled. \n
                - Program 6: Disabled. \n
                - Program 7: Running because it runs every third day. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Disabled. \n
                - Program 2: Disabled. \n
                - Program 3: Disabled. \n
                - Program 4: Disabled. \n
                - Program 5: Disabled. \n
                - Program 6: Disabled. \n
                - Program 7: Running because it runs every third day, and it's second zone is now watering for it's
                             runtime of 5 minutes. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Disabled. \n
                - Program 2: Disabled. \n
                - Program 3: Disabled. \n
                - Program 4: Disabled. \n
                - Program 5: Disabled. \n
                - Program 6: Disabled. \n
                - Program 7: Running because it runs every third day, and it's third zone is now watering for it's
                             runtime of 5 minutes. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Disabled. \n
                - Program 2: Disabled. \n
                - Program 3: Disabled. \n
                - Program 4: Disabled. \n
                - Program 5: Disabled. \n
                - Program 6: Disabled. \n
                - Program 7: Done watering because it only waters on every third day. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # *********************************************** Day Seven ********************************************** #
            # Set the controller's time to 8:00 PM
            self.config.BaseStation3200[1].set_date_and_time(_date='07/31/2012', _time='20:00:00')
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_running()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_running()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_running()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_24(self):
        """
        - Set the date and time to 08/01/2012 (Day 8) (Odd Day), 8:00 PM \n
            - Increment the time by 1 minute. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Disabled. \n
                - Program 2: Disabled. \n
                - Program 3: Disabled. \n
                - Program 4: Disabled. \n
                - Program 5: Disabled. \n
                - Program 6: Disabled. \n
                - Program 7: Done watering because it only waters on every third day. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Disabled. \n
                - Program 2: Disabled. \n
                - Program 3: Disabled. \n
                - Program 4: Disabled. \n
                - Program 5: Disabled. \n
                - Program 6: Disabled. \n
                - Program 7: Done watering because it only waters on every third day. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # *********************************************** Day Eight ********************************************** #
            # Set the controller's time to 8:00 PM
            self.config.BaseStation3200[1].set_date_and_time(_date='08/01/2012', _time='20:00:00')
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_done()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_25(self):
        """
        - Set the date and time to 08/02/2012 (Day 9) (Even Day), 8:00 PM \n
            - Increment the time by 1 minute. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Disabled. \n
                - Program 2: Disabled. \n
                - Program 3: Disabled. \n
                - Program 4: Disabled. \n
                - Program 5: Disabled. \n
                - Program 6: Disabled. \n
                - Program 7: Done watering because it only waters on every third day. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Disabled. \n
                - Program 2: Disabled. \n
                - Program 3: Disabled. \n
                - Program 4: Disabled. \n
                - Program 5: Disabled. \n
                - Program 6: Disabled. \n
                - Program 7: Done watering because it only waters on every third day. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # *********************************************** Day Nine *********************************************** #
            # Set the controller's time to 8:00 PM
            self.config.BaseStation3200[1].set_date_and_time(_date='08/02/2012', _time='20:00:00')
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_done()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_26(self):
        """
        - Set the date and time to 08/03/2012 (Day 10) (Odd Day), 8:00 PM \n
            - Increment the time by 1 minute. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Disabled. \n
                - Program 2: Disabled. \n
                - Program 3: Disabled. \n
                - Program 4: Disabled. \n
                - Program 5: Disabled. \n
                - Program 6: Disabled. \n
                - Program 7: Running because it runs every third day. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Disabled. \n
                - Program 2: Disabled. \n
                - Program 3: Disabled. \n
                - Program 4: Disabled. \n
                - Program 5: Disabled. \n
                - Program 6: Disabled. \n
                - Program 7: Running because it runs every third day, and it's second zone is now watering for it's
                             runtime of 5 minutes. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Disabled. \n
                - Program 2: Disabled. \n
                - Program 3: Disabled. \n
                - Program 4: Disabled. \n
                - Program 5: Disabled. \n
                - Program 6: Disabled. \n
                - Program 7: Running because it runs every third day, and it's third zone is now watering for it's
                             runtime of 5 minutes. \n
            - Increment the time by 5 minutes. \n
            - Get data from all the Programs. \n
            - Verify Programs: \n
                - Program 1: Disabled. \n
                - Program 2: Disabled. \n
                - Program 3: Disabled. \n
                - Program 4: Disabled. \n
                - Program 5: Disabled. \n
                - Program 6: Disabled. \n
                - Program 7: Done watering because it only waters on every third day. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # *********************************************** Day Ten ************************************************ #
            # Set the controller's time to 8:00 PM
            self.config.BaseStation3200[1].set_date_and_time(_date='07/31/2012', _time='20:00:00')
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_running()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_running()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_running()

            # Increment the clock by 5 minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[5].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[7].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
