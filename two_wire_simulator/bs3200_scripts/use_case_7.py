import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

from datetime import time

__author__ = 'Eldin'


class ControllerUseCase7(object):
    """
    Test name:
        - Multiple Start Stop Pause on event decoder
    purpose:
        - this test was create for to verify a customer configuration that want to use the 3200 for a fire suppression\n
            system
        - confirming that one event decoder can have a start, stop, condition on multiple Programs
        - one SSP condition can:
            - stop multiple Programs at once
            - start a program
    Coverage area: \n
        This test covers using an event switch to set and activate SSP conditions on multiple Programs at once \n
        -setting up devices:
            - Loading \n
            - Searching \n
            - Addressing \n
                - Setting:
                    - descriptions
                    - locations \n
        - setting Programs: \n
            - start times
            - mainlines

        - attaching zones to program \n

        - setting up mainline: \n
            set main line \n
                - limit zones by flow \n
                - pipe fill time\n
                - target flow\n
                - high variance limit
                - high variance shut down \n
                - low variance limit
                - low variance shut down \n

        - setting up poc's: \n
            - enable POC \n
            - assign:
                - master valve
                - flow meter \n
                - target flow\n
                - main line \n

        - turning off multiple Programs at once
        - starting a program with the same event decoder state that stopped another program
        - checking when Programs turn on they obey zone concurrency
    """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                     # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        program_8am_start_time = [480]
        program_no_start_time = []

        try:
            self.config.BaseStation3200[1].set_serial_port_timeout(timeout=1200)
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=5)

            # Add & Configure Program 1
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=2)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_8am_start_time)
            
            # Add & Configure Program 2
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation3200[1].programs[2].set_enabled()
            self.config.BaseStation3200[1].programs[2].set_max_concurrent_zones(_number_of_zones=2)
            self.config.BaseStation3200[1].programs[2].set_start_times(_st_list=program_8am_start_time)

            # Add & Configure Program 3
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=3)
            self.config.BaseStation3200[1].programs[3].set_enabled()
            self.config.BaseStation3200[1].programs[3].set_max_concurrent_zones(_number_of_zones=2)
            self.config.BaseStation3200[1].programs[3].set_start_times(_st_list=program_8am_start_time)


            # Add & Configure Program 4
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=4)
            self.config.BaseStation3200[1].programs[4].set_enabled()
            self.config.BaseStation3200[1].programs[4].set_max_concurrent_zones(_number_of_zones=2)
            self.config.BaseStation3200[1].programs[4].set_start_times(_st_list=program_8am_start_time)


            # Add & Configure Program 5
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=5)
            self.config.BaseStation3200[1].programs[5].set_enabled()
            self.config.BaseStation3200[1].programs[5].set_max_concurrent_zones(_number_of_zones=2)
            self.config.BaseStation3200[1].programs[5].set_start_times(_st_list=program_8am_start_time)

            # Add & Configure Program 6
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=6)
            self.config.BaseStation3200[1].programs[6].set_enabled()
            self.config.BaseStation3200[1].programs[6].set_max_concurrent_zones(_number_of_zones=2)
            self.config.BaseStation3200[1].programs[6].set_start_times(_st_list=program_8am_start_time)

            # Add & Configure Program 7
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=7)
            self.config.BaseStation3200[1].programs[7].set_enabled()
            self.config.BaseStation3200[1].programs[7].set_max_concurrent_zones(_number_of_zones=2)
            self.config.BaseStation3200[1].programs[7].set_start_times(_st_list=program_8am_start_time)

            # Add & Configure Program 8
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=8)
            self.config.BaseStation3200[1].programs[8].set_enabled()
            self.config.BaseStation3200[1].programs[8].set_max_concurrent_zones(_number_of_zones=2)
            self.config.BaseStation3200[1].programs[8].set_start_times(_st_list=program_8am_start_time)

            # Add & Configure Program 9
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=9)
            self.config.BaseStation3200[1].programs[9].set_enabled()
            self.config.BaseStation3200[1].programs[9].set_max_concurrent_zones(_number_of_zones=2)
            self.config.BaseStation3200[1].programs[9].set_start_times(_st_list=program_8am_start_time)

            # Add & Configure Program 10
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=10)
            self.config.BaseStation3200[1].programs[10].set_enabled()
            self.config.BaseStation3200[1].programs[10].set_max_concurrent_zones(_number_of_zones=2)
            self.config.BaseStation3200[1].programs[10].set_start_times(_st_list=program_8am_start_time)

            # Add & Configure Program 96
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=96)
            self.config.BaseStation3200[1].programs[96].set_enabled()
            self.config.BaseStation3200[1].programs[96].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation3200[1].programs[96].set_start_times(_st_list=program_no_start_time)

            # Add & Configure Program 97
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=97)
            self.config.BaseStation3200[1].programs[97].set_enabled()
            self.config.BaseStation3200[1].programs[97].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation3200[1].programs[97].set_start_times(_st_list=program_no_start_time)


            # Add & Configure Program 98
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=98)
            self.config.BaseStation3200[1].programs[98].set_enabled()
            self.config.BaseStation3200[1].programs[98].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation3200[1].programs[98].set_start_times(_st_list=program_no_start_time)


            # Add & Configure Program 99
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=99)
            self.config.BaseStation3200[1].programs[99].set_enabled()
            self.config.BaseStation3200[1].programs[99].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation3200[1].programs[99].set_start_times(_st_list=program_no_start_time)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        setup start/stop/pause conditions
            - Programs 1-10 should stop when any of the 4 event switches opens
            - program 96 should start when SW1 opens, and close on the other 3
            - program 97 should start when SW2 opens, and close on the other 3
            - program 98 should start when SW3 opens, and close on the other 3
            - program 99 should start when SW4 opens, and close on the other 3
        """
        # this is set in the PG3200 object
        # TODO need to have concurrent zones per program added
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # condition_counter = 1
            # Loops through the first 10 Programs and sets them to stop whenever an event switch 1-4 is on
            for i in range(1, 11):
                self.config.BaseStation3200[1].programs[i].add_switch_stop_condition(_event_switch_address=1)
                self.config.BaseStation3200[1].programs[i].event_switch_stop_conditions[1].set_switch_mode_to_open()
                self.config.BaseStation3200[1].programs[i].add_switch_stop_condition(_event_switch_address=2)
                self.config.BaseStation3200[1].programs[i].event_switch_stop_conditions[2].set_switch_mode_to_open()
                self.config.BaseStation3200[1].programs[i].add_switch_stop_condition(_event_switch_address=3)
                self.config.BaseStation3200[1].programs[i].event_switch_stop_conditions[3].set_switch_mode_to_open()
                self.config.BaseStation3200[1].programs[i].add_switch_stop_condition(_event_switch_address=4)
                self.config.BaseStation3200[1].programs[i].event_switch_stop_conditions[4].set_switch_mode_to_open()


                # # We increment by 4 because we are adding 4 conditions each time we go through the loop
                # condition_counter += 4

            # Start/Stop/Pause conditions for program 96, started by SW[1] and stopped by SW[2-4]

            # We start at 41 because that is where condition counter will end after assigning 4 switch events to 10
            # Programs

            self.config.BaseStation3200[1].programs[96].add_switch_stop_condition(_event_switch_address=2)
            self.config.BaseStation3200[1].programs[96].event_switch_stop_conditions[2].set_switch_mode_to_open()

            self.config.BaseStation3200[1].programs[96].add_switch_stop_condition(_event_switch_address=3)
            self.config.BaseStation3200[1].programs[96].event_switch_stop_conditions[3].set_switch_mode_to_open()

            self.config.BaseStation3200[1].programs[96].add_switch_stop_condition(_event_switch_address=4)
            self.config.BaseStation3200[1].programs[96].event_switch_stop_conditions[4].set_switch_mode_to_open()

            self.config.BaseStation3200[1].programs[96].add_switch_start_condition(_event_switch_address=1)
            self.config.BaseStation3200[1].programs[96].event_switch_start_conditions[1].set_switch_mode_to_open()

            # Start/Stop/Pause conditions for program 97, started by SW[2] and stopped by SW[3,4]

            self.config.BaseStation3200[1].programs[97].add_switch_stop_condition(_event_switch_address=3)
            self.config.BaseStation3200[1].programs[97].event_switch_stop_conditions[3].set_switch_mode_to_open()

            self.config.BaseStation3200[1].programs[97].add_switch_stop_condition(_event_switch_address=4)
            self.config.BaseStation3200[1].programs[97].event_switch_stop_conditions[4].set_switch_mode_to_open()

            self.config.BaseStation3200[1].programs[97].add_switch_start_condition(_event_switch_address=2)
            self.config.BaseStation3200[1].programs[97].event_switch_start_conditions[2].set_switch_mode_to_open()

            # Start/Stop/Pause conditions for program 98, started by SW[3] and stopped by SW[4]
            self.config.BaseStation3200[1].programs[98].add_switch_stop_condition(_event_switch_address=4)
            self.config.BaseStation3200[1].programs[98].event_switch_stop_conditions[4].set_switch_mode_to_open()

            self.config.BaseStation3200[1].programs[98].add_switch_start_condition(_event_switch_address=3)
            self.config.BaseStation3200[1].programs[98].event_switch_start_conditions[3].set_switch_mode_to_open()

            # Start/Stop/Pause conditions for program 99, started by SW[4] and not stopped by any switch
            self.config.BaseStation3200[1].programs[99].add_switch_start_condition(_event_switch_address=4)
            self.config.BaseStation3200[1].programs[99].event_switch_start_conditions[4].set_switch_mode_to_open()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            # Programs 1-10 have 2 zones each

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_run_time(_minutes=15)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[2].zone_programs[3].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[2].zone_programs[4].set_run_time(_minutes=15)

            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=5)
            self.config.BaseStation3200[1].programs[3].zone_programs[5].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=6)
            self.config.BaseStation3200[1].programs[3].zone_programs[6].set_run_time(_minutes=15)

            self.config.BaseStation3200[1].programs[4].add_zone_to_program(_zone_address=7)
            self.config.BaseStation3200[1].programs[4].zone_programs[7].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[4].add_zone_to_program(_zone_address=8)
            self.config.BaseStation3200[1].programs[4].zone_programs[8].set_run_time(_minutes=15)

            self.config.BaseStation3200[1].programs[5].add_zone_to_program(_zone_address=9)
            self.config.BaseStation3200[1].programs[5].zone_programs[9].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[5].add_zone_to_program(_zone_address=10)
            self.config.BaseStation3200[1].programs[5].zone_programs[10].set_run_time(_minutes=15)

            self.config.BaseStation3200[1].programs[6].add_zone_to_program(_zone_address=11)
            self.config.BaseStation3200[1].programs[6].zone_programs[11].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[6].add_zone_to_program(_zone_address=12)
            self.config.BaseStation3200[1].programs[6].zone_programs[12].set_run_time(_minutes=15)

            self.config.BaseStation3200[1].programs[7].add_zone_to_program(_zone_address=13)
            self.config.BaseStation3200[1].programs[7].zone_programs[13].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[7].add_zone_to_program(_zone_address=14)
            self.config.BaseStation3200[1].programs[7].zone_programs[14].set_run_time(_minutes=15)

            self.config.BaseStation3200[1].programs[8].add_zone_to_program(_zone_address=15)
            self.config.BaseStation3200[1].programs[8].zone_programs[15].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[8].add_zone_to_program(_zone_address=16)
            self.config.BaseStation3200[1].programs[8].zone_programs[16].set_run_time(_minutes=15)

            self.config.BaseStation3200[1].programs[9].add_zone_to_program(_zone_address=17)
            self.config.BaseStation3200[1].programs[9].zone_programs[17].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[9].add_zone_to_program(_zone_address=18)
            self.config.BaseStation3200[1].programs[9].zone_programs[18].set_run_time(_minutes=15)

            self.config.BaseStation3200[1].programs[10].add_zone_to_program(_zone_address=19)
            self.config.BaseStation3200[1].programs[10].zone_programs[19].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[10].add_zone_to_program(_zone_address=20)
            self.config.BaseStation3200[1].programs[10].zone_programs[20].set_run_time(_minutes=15)

            # Program 96 has 4 zones (81-84)
            self.config.BaseStation3200[1].programs[96].add_zone_to_program(_zone_address=81)
            self.config.BaseStation3200[1].programs[96].zone_programs[81].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[96].add_zone_to_program(_zone_address=82)
            self.config.BaseStation3200[1].programs[96].zone_programs[82].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[96].add_zone_to_program(_zone_address=83)
            self.config.BaseStation3200[1].programs[96].zone_programs[83].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[96].add_zone_to_program(_zone_address=84)
            self.config.BaseStation3200[1].programs[96].zone_programs[84].set_run_time(_minutes=15)

            # Program 97 has 4 zones (85-88)
            self.config.BaseStation3200[1].programs[97].add_zone_to_program(_zone_address=85)
            self.config.BaseStation3200[1].programs[97].zone_programs[85].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[97].add_zone_to_program(_zone_address=86)
            self.config.BaseStation3200[1].programs[97].zone_programs[86].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[97].add_zone_to_program(_zone_address=87)
            self.config.BaseStation3200[1].programs[97].zone_programs[87].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[97].add_zone_to_program(_zone_address=88)
            self.config.BaseStation3200[1].programs[97].zone_programs[88].set_run_time(_minutes=15)

            # Program 98 has 4 zones (89-92)
            self.config.BaseStation3200[1].programs[98].add_zone_to_program(_zone_address=89)
            self.config.BaseStation3200[1].programs[98].zone_programs[89].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[98].add_zone_to_program(_zone_address=90)
            self.config.BaseStation3200[1].programs[98].zone_programs[90].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[98].add_zone_to_program(_zone_address=91)
            self.config.BaseStation3200[1].programs[98].zone_programs[91].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[98].add_zone_to_program(_zone_address=92)
            self.config.BaseStation3200[1].programs[98].zone_programs[92].set_run_time(_minutes=15)

            # Program 99 has 4 zones (93-96)
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=93)
            self.config.BaseStation3200[1].programs[99].zone_programs[93].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=94)
            self.config.BaseStation3200[1].programs[99].zone_programs[94].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=95)
            self.config.BaseStation3200[1].programs[99].zone_programs[95].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=96)
            self.config.BaseStation3200[1].programs[99].zone_programs[96].set_run_time(_minutes=15)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].add_master_valve_to_point_of_control(_master_valve_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_flow_meter_to_point_of_control(_flow_meter_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=500)

            # Add POC 1 to Water Source 1
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(_point_of_control_address=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure ML 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=500)

            # Add ML 1 to POC 1
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        Set the time to 7:56:00 \n
        Increment the clock by 3 minutes to 7:59:00 \n
        Verify the status for each program and zone on the controller \n
        - Program status: \n
            - program 1-10 done \n
            - program 96 done \n
            - program 97 done \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - all zones done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_date_and_time(_date='08/28/2014', _time='7:56:00')
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].do_increment_clock(minutes=3)
            self.config.BaseStation3200[1].verify_date_and_time()

            # For now, the zones are expected to have the same status
            for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_done()

            for program_address in range(1, 11):
                self.config.BaseStation3200[1].programs[program_address].statuses.verify_status_is_done()

            # These 4 Programs will differ in future steps so we don't have them in a loop
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        Increment the clock by 2 minutes to 8:01:00 \n
        Programs 1- 10 have a start time of 8:00am  Programs 96-99 don't have a start time
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - Programs 1, 2, 3 running \n
            - Programs 4-10 waiting \n
            - program 96 done \n
            - program 97 done \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-5 watering \n
            - zones 6-20 waiting \n
            - zones 81-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)
            self.config.BaseStation3200[1].verify_date_and_time()

            for zone_address in range(1, 6):
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_watering()

            for zone_address in range(6, 21):
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_waiting_to_water()

            for zone_address in range(81, 97):
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_done()

            # Programs 1-3 are running since each program holds 2 zones and the concurrency is 5
            for program_address in range(1, 4):
                self.config.BaseStation3200[1].programs[program_address].statuses.verify_status_is_running()

            for program_address in range(4, 11):
                self.config.BaseStation3200[1].programs[program_address].statuses.verify_status_is_waiting_to_run()

            # These 4 Programs will differ in future steps so we don't have them in a loop

            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        Increment the clock by 5 minutes to 8:05:00 \n
        Open event switch 1 \n
        The the opening the switch should stop the first 10 Programs and start program 96
        you have to increment the clock a minute in order for this to take place this makes the time 8:06am
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1-10 done \n
            - program 96 running \n
            - program 97-99 done \n

        - Zone status: \n
            - zone 1-20 done \n
            - zone 81-85 watering \n
            - zone 85-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_open()
            self.config.BaseStation3200[1].event_switches[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone_address in range(1, 21):
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_done()

            for zone_address in range(81, 85):
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_watering()

            for zone_address in range(85, 97):
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_done()

            for program_address in range(1, 11):
                self.config.BaseStation3200[1].programs[program_address].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_running()

            for program_address in range(97, 100):
                self.config.BaseStation3200[1].programs[program_address].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        Increment the clock by 4 minutes to 8:10:00 \n
        Open event switch 2 \n
        The the opening the switch should stop program 96 Programs and start program 97
        you have to increment the clock a minute in order for this to take place this makes the time 8:11am
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1-10 done \n
            - program 96 done \n
            - program 97 running \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - zone 1-20 done \n
            - zone 81-84 done \n
            - zone 85-88 watering \n
            - zone 89-96 done \n
          """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].event_switches[2].bicoder.set_contact_open()
            self.config.BaseStation3200[1].event_switches[2].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone_address in range(1, 21):
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_done()

            for zone_address in range(81, 85):
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_done()

            for zone_address in range(85, 89):
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_watering()

            for zone_address in range(89, 97):
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_done()

            for program_address in range(1, 11):
                self.config.BaseStation3200[1].programs[program_address].statuses.verify_status_is_done()

            # These 4 Programs will differ in future steps so we don't have them in a loop
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        Increment the clock by 4 minutes to 8:15:00 \n
        Open event switch 3 \n
        The the opening the switch should stop program 97 Programs and start program 98
        you have to increment the clock a minute in order for this to take place this makes the time 8:16am
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1-10 done \n
            - program 96 done \n
            - program 97 done \n
            - program 98 running \n
            - program 99 done \n
        - Zone status: \n
            - zone 1-20 done \n
            - zone 81-84 done \n
            - zone 85-88 done \n
            - zone 89-92 watering \n
            - zone 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].event_switches[3].bicoder.set_contact_open()
            self.config.BaseStation3200[1].event_switches[3].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone_address in range(1, 21):
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_done()

            for zone_address in range(81, 85):
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_done()

            for zone_address in range(85, 89):
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_done()

            for zone_address in range(89, 93):
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_watering()

            for zone_address in range(93, 97):
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_done()

            for program_address in range(1, 11):
                self.config.BaseStation3200[1].programs[program_address].statuses.verify_status_is_done()

            # These 4 Programs will differ in future steps so we don't have them in a loop
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        Increment the clock by 4 minutes to 8:20:00 \n
        Open event switch 4 \n
        The the opening the switch should stop program 98 Programs and start program 99
        you have to increment the clock a minute in order for this to take place this makes the time 8:21am
        Verify the status for each program and zone and on the controller \n
        - Program status: \n
            - program 1-10 done \n
            - program 96 - 98 done \n
            - program 99 running \n
        - Zone status: \n
            - zone 1-20 done \n
            - zone 81-92 done \n
            - zone 93-96 watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].event_switches[4].bicoder.set_contact_open()
            self.config.BaseStation3200[1].event_switches[4].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone_address in range(1, 21):
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_done()

            for zone_address in range(81, 89):
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_done()

            for zone_address in range(89, 93):
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_done()

            for zone_address in range(93, 97):
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_watering()

            # These ten Programs will always be the same so we have them in a loop together
            for program_address in range(1, 11):
                self.config.BaseStation3200[1].programs[program_address].statuses.verify_status_is_done()

            # These 4 Programs will differ in future steps so we don't have them in a loop
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_running()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_14(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
            