import sys
from common.configuration import Configuration
from datetime import time, timedelta, datetime

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr

# import log_handler for logging functionality
from common.logging_handler import log_handler

# Objects
from common.objects.programming.pg_3200 import PG3200
from common.objects.programming.zp import ZoneProgram
from common.objects.base_classes.web_driver import *
from common.imports import opcodes

from common import helper_methods

__author__ = "Eldin"


class ControllerUseCase21(object):
    """
    Test name: \n
        Start Times Test \n
    Purpose: \n
        To test that multiple zones can turn on at the same time by using a start time on a program. Also verify
        that multiple zones running at the same start time on different Programs can run for different durations. Test
        that max zone concurrency works on both the controller and program level.
    Coverage area: \n
        1. Test many zones/Programs starting at the same start time. \n
        2. Test the controller hitting a second start time while zones are watering. \n
        3. Verify that odd zones run for 1 minute and even zones run for 2 minutes (set it up this way). \n
        4. Able to set zone concurrency for the controller and each program. Controller has 2 max zones and each
           program has 1 max zone. \n
        5. Verify that 2 zones are watering because we set concurrent zones to 2. \n
    """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                     # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        # this is set in the PG3200 object
        program_start_time_11_45_pm_and_12_am = [1425, 0]  # 11:45 pm start time and 12:00 am start time

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # set controller to run 2 zone at a time
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=2)
            # Create 20 programs
            for program_address in range(1, 21):
                self.config.BaseStation3200[1].add_program_to_controller(_program_address=program_address)
                self.config.BaseStation3200[1].programs[program_address].set_enabled()
                self.config.BaseStation3200[1].programs[program_address].set_max_concurrent_zones(_number_of_zones=1)
                self.config.BaseStation3200[1].programs[program_address].\
                    set_start_times(_st_list=program_start_time_11_45_pm_and_12_am)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=1)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[2].zone_programs[2].set_run_time(_minutes=2)

            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[3].zone_programs[3].set_run_time(_minutes=1)

            self.config.BaseStation3200[1].programs[4].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[4].zone_programs[4].set_run_time(_minutes=2)

            self.config.BaseStation3200[1].programs[5].add_zone_to_program(_zone_address=5)
            self.config.BaseStation3200[1].programs[5].zone_programs[5].set_run_time(_minutes=1)

            self.config.BaseStation3200[1].programs[6].add_zone_to_program(_zone_address=6)
            self.config.BaseStation3200[1].programs[6].zone_programs[6].set_run_time(_minutes=2)

            self.config.BaseStation3200[1].programs[7].add_zone_to_program(_zone_address=7)
            self.config.BaseStation3200[1].programs[7].zone_programs[7].set_run_time(_minutes=1)

            self.config.BaseStation3200[1].programs[8].add_zone_to_program(_zone_address=8)
            self.config.BaseStation3200[1].programs[8].zone_programs[8].set_run_time(_minutes=2)

            self.config.BaseStation3200[1].programs[9].add_zone_to_program(_zone_address=9)
            self.config.BaseStation3200[1].programs[9].zone_programs[9].set_run_time(_minutes=1)

            self.config.BaseStation3200[1].programs[10].add_zone_to_program(_zone_address=10)
            self.config.BaseStation3200[1].programs[10].zone_programs[10].set_run_time(_minutes=2)

            self.config.BaseStation3200[1].programs[11].add_zone_to_program(_zone_address=11)
            self.config.BaseStation3200[1].programs[11].zone_programs[11].set_run_time(_minutes=1)

            self.config.BaseStation3200[1].programs[12].add_zone_to_program(_zone_address=12)
            self.config.BaseStation3200[1].programs[12].zone_programs[12].set_run_time(_minutes=2)

            self.config.BaseStation3200[1].programs[13].add_zone_to_program(_zone_address=17)
            self.config.BaseStation3200[1].programs[13].zone_programs[17].set_run_time(_minutes=1)

            self.config.BaseStation3200[1].programs[14].add_zone_to_program(_zone_address=18)
            self.config.BaseStation3200[1].programs[14].zone_programs[18].set_run_time(_minutes=2)

            self.config.BaseStation3200[1].programs[15].add_zone_to_program(_zone_address=19)
            self.config.BaseStation3200[1].programs[15].zone_programs[19].set_run_time(_minutes=1)

            self.config.BaseStation3200[1].programs[16].add_zone_to_program(_zone_address=196)
            self.config.BaseStation3200[1].programs[16].zone_programs[196].set_run_time(_minutes=2)

            self.config.BaseStation3200[1].programs[17].add_zone_to_program(_zone_address=197)
            self.config.BaseStation3200[1].programs[17].zone_programs[197].set_run_time(_minutes=1)

            self.config.BaseStation3200[1].programs[18].add_zone_to_program(_zone_address=198)
            self.config.BaseStation3200[1].programs[18].zone_programs[198].set_run_time(_minutes=2)

            self.config.BaseStation3200[1].programs[19].add_zone_to_program(_zone_address=199)
            self.config.BaseStation3200[1].programs[19].zone_programs[199].set_run_time(_minutes=1)

            self.config.BaseStation3200[1].programs[20].add_zone_to_program(_zone_address=200)
            self.config.BaseStation3200[1].programs[20].zone_programs[200].set_run_time(_minutes=2)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        - Set the date (arbitrary) and time to be 11:44 pm, which is 1 minute before the start time of every program
          that we created at the beginning of this test. (The start time of every program is 11:45 pm. \n
        - Verify that the date and time were correctly set on the controller. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.verify_full_configuration()
            self.config.BaseStation3200[1].set_date_and_time(_date='04/08/2014', _time='23:44:00')
            self.config.BaseStation3200[1].verify_date_and_time()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        - Increment the clock by one minute, putting us at 11:45 pm. \n
        - Verify that nothing has started running yet because we have not yet passed the start time. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone_address].verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        - Increment the clock by one minute, putting us at 11:46 pm. \n
        - Get data of all zones to update our object attributes. \n
        - We have passed the start time of 11:45 pm, meaning we should now be watering. \n
        - Verify that 2 zones are running because the controller has max concurrent zones set to 2. \n
        - Verify Zones Running: \n
            - Zone 1: The first zone on the controller, so it should be running first. \n
            - Zone 2: The second zone on the controller, so it should be running as the second zone. \n
        - Verify Zones Waiting: \n
            - All other zones that aren't mentioned above. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the clock by 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all the zone statuses
            self.config.BaseStation3200[1].zones[1].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[6].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[7].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[8].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[9].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[10].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[11].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[12].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[17].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[18].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[19].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[196].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[197].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[198].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[199].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[200].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        - Increment the clock by one minute, putting us at 11:47 pm. \n
        - Get data of all zones to update our object attributes. \n
        - We have passed the start time of 11:45 pm, meaning we should now be watering. \n
        - Verify that 2 zones are running because the controller has max concurrent zones set to 2. \n
        - Verify Zones Done: (In order of when they were done watering) \n
            - Zone 1: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
        - Verify Zones Watering: \n
            - Zone 2: It is still running because it's has only run 60 seconds of its 120 second runtime. \n
            - Zone 3: The next zone that is supposed to run on the controller. \n
        - Verify Zones Waiting: \n
            - All other zones that aren't mentioned above. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the clock by 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all the zone statuses
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[6].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[7].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[8].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[9].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[10].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[11].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[12].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[17].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[18].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[19].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[196].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[197].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[198].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[199].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[200].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        - Increment the clock by one minute, putting us at 11:48 pm. \n
        - Get data of all zones to update our object attributes. \n
        - We have passed the start time of 11:45 pm, meaning we should now be watering. \n
        - Verify that 2 zones are running because the controller has max concurrent zones set to 2. \n
        - Verify Zones Done: (In order of when they were done watering) \n
            - Zone 1: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 2: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 3: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
        - Verify Zones Watering: \n
            - Zone 4: The next zone that is supposed to run on the controller. \n
            - Zone 5: The next zone that is supposed to run on the controller. \n
        - Verify Zones Waiting: \n
            - All other zones that aren't mentioned above. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the clock by 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all the zone statuses
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[7].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[8].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[9].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[10].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[11].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[12].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[17].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[18].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[19].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[196].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[197].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[198].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[199].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[200].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        - Increment the clock by one minute, putting us at 11:49 pm. \n
        - Get data of all zones to update our object attributes. \n
        - We have passed the start time of 11:45 pm, meaning we should now be watering. \n
        - Verify that 2 zones are running because the controller has max concurrent zones set to 2. \n
        - Verify Zones Done: (In order of when they were done watering) \n
            - Zone 1: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 2: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 3: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 5: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
        - Verify Zones Watering: \n
            - Zone 4: It is still running because it's has only run 60 seconds of its 120 second runtime. \n
            - Zone 6: The next zone that is supposed to run on the controller. \n
        - Verify Zones Waiting: \n
            - All other zones that aren't mentioned above. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the clock by 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all the zone statuses
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[7].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[8].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[9].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[10].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[11].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[12].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[17].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[18].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[19].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[196].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[197].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[198].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[199].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[200].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        - Increment the clock by one minute, putting us at 11:50 pm. \n
        - Get data of all zones to update our object attributes. \n
        - We have passed the start time of 11:45 pm, meaning we should now be watering. \n
        - Verify that 2 zones are running because the controller has max concurrent zones set to 2. \n
        - Verify Zones Done: (In order of when they were done watering) \n
            - Zone 1: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 2: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 3: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 5: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 4: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
        - Verify Zones Watering: \n
            - Zone 6: It is still running because it's has only run 60 seconds of its 120 second runtime. \n
            - Zone 7: The next zone that is supposed to run on the controller. \n
        - Verify Zones Waiting: \n
            - All other zones that aren't mentioned above. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the clock by 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all the zone statuses
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[7].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[8].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[9].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[10].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[11].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[12].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[17].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[18].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[19].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[196].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[197].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[198].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[199].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[200].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        - Increment the clock by one minute, putting us at 11:51 pm. \n
        - Get data of all zones to update our object attributes. \n
        - We have passed the start time of 11:45 pm, meaning we should now be watering. \n
        - Verify that 2 zones are running because the controller has max concurrent zones set to 2. \n
        - Verify Zones Done: (In order of when they were done watering) \n
            - Zone 1: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 2: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 3: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 5: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 4: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 6: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 7: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
        - Verify Zones Watering: \n
            - Zone 8: The next zone that is supposed to run on the controller. \n
            - Zone 9: The next zone that is supposed to run on the controller. \n
        - Verify Zones Waiting: \n
            - All other zones that aren't mentioned above. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the clock by 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all the zone statuses
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[9].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[10].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[11].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[12].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[17].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[18].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[19].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[196].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[197].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[198].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[199].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[200].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        - Increment the clock by one minute, putting us at 11:52 pm. \n
        - Get data of all zones to update our object attributes. \n
        - We have passed the start time of 11:45 pm, meaning we should now be watering. \n
        - Verify that 2 zones are running because the controller has max concurrent zones set to 2. \n
        - Verify Zones Done: (In order of when they were done watering) \n
            - Zone 1: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 2: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 3: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 5: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 4: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 6: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 7: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 9: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
        - Verify Zones Watering: \n
            - Zone 8: It is still running because it's has only run 60 seconds of its 120 second runtime. \n
            - Zone 10: The next zone that is supposed to run on the controller. \n
        - Verify Zones Waiting: \n
            - All other zones that aren't mentioned above. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the clock by 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all the zone statuses
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[9].verify_status_is_done()
            self.config.BaseStation3200[1].zones[10].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[11].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[12].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[17].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[18].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[19].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[196].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[197].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[198].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[199].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[200].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        - Increment the clock by one minute, putting us at 11:53 pm. \n
        - Get data of all zones to update our object attributes. \n
        - We have passed the start time of 11:45 pm, meaning we should now be watering. \n
        - Verify that 2 zones are running because the controller has max concurrent zones set to 2. \n
        - Verify Zones Done: (In order of when they were done watering) \n
            - Zone 1: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 2: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 3: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 5: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 4: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 6: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 7: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 9: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 8: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
        - Verify Zones Watering: \n
            - Zone 10: It is still running because it's has only run 60 seconds of its 120 second runtime. \n
            - Zone 11: The next zone that is supposed to run on the controller. \n
        - Verify Zones Waiting: \n
            - All other zones that aren't mentioned above. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the clock by 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all the zone statuses
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].verify_status_is_done()
            self.config.BaseStation3200[1].zones[10].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[11].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[12].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[17].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[18].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[19].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[196].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[197].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[198].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[199].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[200].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_14(self):
        """
        - Increment the clock by one minute, putting us at 11:54 pm. \n
        - Get data of all zones to update our object attributes. \n
        - We have passed the start time of 11:45 pm, meaning we should now be watering. \n
        - Verify that 2 zones are running because the controller has max concurrent zones set to 2. \n
        - Verify Zones Done: (In order of when they were done watering) \n
            - Zone 1: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 2: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 3: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 5: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 4: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 6: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 7: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 9: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 8: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 10: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 11: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
        - Verify Zones Watering: \n
            - Zone 12: The next zone that is supposed to run on the controller. \n
            - Zone 17: The next zone that is supposed to run on the controller. \n
        - Verify Zones Waiting: \n
            - All other zones that aren't mentioned above. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the clock by 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all the zone statuses
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].verify_status_is_done()
            self.config.BaseStation3200[1].zones[10].verify_status_is_done()
            self.config.BaseStation3200[1].zones[11].verify_status_is_done()
            self.config.BaseStation3200[1].zones[12].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[17].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[18].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[19].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[196].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[197].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[198].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[199].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[200].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_15(self):
        """
        - Increment the clock by one minute, putting us at 11:55 pm. \n
        - Get data of all zones to update our object attributes. \n
        - We have passed the start time of 11:45 pm, meaning we should now be watering. \n
        - Verify that 2 zones are running because the controller has max concurrent zones set to 2. \n
        - Verify Zones Done: (In order of when they were done watering) \n
            - Zone 1: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 2: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 3: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 5: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 4: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 6: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 7: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 9: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 8: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 10: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 11: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 17: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
        - Verify Zones Watering: \n
            - Zone 12: It is still running because it's has only run 60 seconds of its 120 second runtime. \n
            - Zone 18: The next zone that is supposed to run on the controller. \n
        - Verify Zones Waiting: \n
            - All other zones that aren't mentioned above. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the clock by 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all the zone statuses
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].verify_status_is_done()
            self.config.BaseStation3200[1].zones[10].verify_status_is_done()
            self.config.BaseStation3200[1].zones[11].verify_status_is_done()
            self.config.BaseStation3200[1].zones[12].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[17].verify_status_is_done()
            self.config.BaseStation3200[1].zones[18].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[19].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[196].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[197].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[198].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[199].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[200].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_16(self):
        """
        - Increment the clock by one minute, putting us at 11:56 pm. \n
        - Get data of all zones to update our object attributes. \n
        - We have passed the start time of 11:45 pm, meaning we should now be watering. \n
        - Verify that 2 zones are running because the controller has max concurrent zones set to 2. \n
        - Verify Zones Done: (In order of when they were done watering) \n
            - Zone 1: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 2: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 3: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 5: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 4: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 6: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 7: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 9: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 8: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 10: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 11: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 17: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 12: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
        - Verify Zones Watering: \n
            - Zone 18: It is still running because it's has only run 60 seconds of its 120 second runtime. \n
            - Zone 19: The next zone that is supposed to run on the controller. \n
        - Verify Zones Waiting: \n
            - All other zones that aren't mentioned above. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the clock by 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all the zone statuses
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].verify_status_is_done()
            self.config.BaseStation3200[1].zones[10].verify_status_is_done()
            self.config.BaseStation3200[1].zones[11].verify_status_is_done()
            self.config.BaseStation3200[1].zones[12].verify_status_is_done()
            self.config.BaseStation3200[1].zones[17].verify_status_is_done()
            self.config.BaseStation3200[1].zones[18].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[19].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[196].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[197].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[198].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[199].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[200].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_17(self):
        """
        - Increment the clock by one minute, putting us at 11:57 pm. \n
        - Get data of all zones to update our object attributes. \n
        - We have passed the start time of 11:45 pm, meaning we should now be watering. \n
        - Verify that 2 zones are running because the controller has max concurrent zones set to 2. \n
        - Verify Zones Done: (In order of when they were done watering) \n
            - Zone 1: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 2: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 3: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 5: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 4: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 6: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 7: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 9: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 8: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 10: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 11: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 17: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 12: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 18: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 19: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
        - Verify Zones Watering: \n
            - Zone 196: The next zone that is supposed to run on the controller. \n
            - Zone 197: The next zone that is supposed to run on the controller. \n
        - Verify Zones Waiting: \n
            - All other zones that aren't mentioned above. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the clock by 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all the zone statuses
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].verify_status_is_done()
            self.config.BaseStation3200[1].zones[10].verify_status_is_done()
            self.config.BaseStation3200[1].zones[11].verify_status_is_done()
            self.config.BaseStation3200[1].zones[12].verify_status_is_done()
            self.config.BaseStation3200[1].zones[17].verify_status_is_done()
            self.config.BaseStation3200[1].zones[18].verify_status_is_done()
            self.config.BaseStation3200[1].zones[19].verify_status_is_done()
            self.config.BaseStation3200[1].zones[196].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[197].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[198].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[199].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[200].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_18(self):
        """
        - Increment the clock by one minute, putting us at 11:58 pm. \n
        - Get data of all zones to update our object attributes. \n
        - We have passed the start time of 11:45 pm, meaning we should now be watering. \n
        - Verify that 2 zones are running because the controller has max concurrent zones set to 2. \n
        - Verify Zones Done: (In order of when they were done watering) \n
            - Zone 1: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 2: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 3: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 5: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 4: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 6: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 7: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 9: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 8: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 10: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 11: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 17: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 12: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 18: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 19: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 197: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
        - Verify Zones Watering: \n
            - Zone 196: It is still running because it's has only run 60 seconds of its 120 second runtime. \n
            - Zone 198: The next zone that is supposed to run on the controller. \n
        - Verify Zones Waiting: \n
            - All other zones that aren't mentioned above. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the clock by 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all the zone statuses
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].verify_status_is_done()
            self.config.BaseStation3200[1].zones[10].verify_status_is_done()
            self.config.BaseStation3200[1].zones[11].verify_status_is_done()
            self.config.BaseStation3200[1].zones[12].verify_status_is_done()
            self.config.BaseStation3200[1].zones[17].verify_status_is_done()
            self.config.BaseStation3200[1].zones[18].verify_status_is_done()
            self.config.BaseStation3200[1].zones[19].verify_status_is_done()
            self.config.BaseStation3200[1].zones[196].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[197].verify_status_is_done()
            self.config.BaseStation3200[1].zones[198].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[199].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[200].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_19(self):
        """
        - Increment the clock by one minute, putting us at 11:59 pm. \n
        - Get data of all zones to update our object attributes. \n
        - We have passed the start time of 11:45 pm, meaning we should now be watering. \n
        - Verify that 2 zones are running because the controller has max concurrent zones set to 2. \n
        - Verify Zones Done: (In order of when they were done watering) \n
            - Zone 1: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 2: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 3: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 5: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 4: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 6: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 7: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 9: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 8: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 10: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 11: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 17: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 12: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 18: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 19: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 197: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 196: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
        - Verify Zones Watering: \n
            - Zone 198: It is still running because it's has only run 60 seconds of its 120 second runtime. \n
            - Zone 199: The next zone that is supposed to run on the controller. \n
        - Verify Zones Waiting: \n
            - All other zones that aren't mentioned above. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the clock by 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all the zone statuses
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].verify_status_is_done()
            self.config.BaseStation3200[1].zones[10].verify_status_is_done()
            self.config.BaseStation3200[1].zones[11].verify_status_is_done()
            self.config.BaseStation3200[1].zones[12].verify_status_is_done()
            self.config.BaseStation3200[1].zones[17].verify_status_is_done()
            self.config.BaseStation3200[1].zones[18].verify_status_is_done()
            self.config.BaseStation3200[1].zones[19].verify_status_is_done()
            self.config.BaseStation3200[1].zones[196].verify_status_is_done()
            self.config.BaseStation3200[1].zones[197].verify_status_is_done()
            self.config.BaseStation3200[1].zones[198].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[199].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[200].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_20(self):
        """
        - Increment the clock by one minute, putting us at 12:00 am. \n
        - Get data of all zones to update our object attributes. \n
        - We have hit the start time of 12:00 am, but we haven't gone past it yet so the zones haven't started yet. \n
        - Verify that 2 zones are running because the controller has max concurrent zones set to 2. \n
        - Verify Zones Done: (In order of when they were done watering) \n
            - Zone 1: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 2: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 3: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 5: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 4: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 6: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 7: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 9: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 8: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 10: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 11: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 17: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 12: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 18: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 19: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 197: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 196: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 198: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 199: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
        - Verify Zones Watering: \n
            - Zone 200: The next zone that is supposed to run on the controller. \n
        - Verify Zones Waiting: \n
            - All other zones that aren't mentioned above. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the clock by 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all the zone statuses
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].verify_status_is_done()
            self.config.BaseStation3200[1].zones[10].verify_status_is_done()
            self.config.BaseStation3200[1].zones[11].verify_status_is_done()
            self.config.BaseStation3200[1].zones[12].verify_status_is_done()
            self.config.BaseStation3200[1].zones[17].verify_status_is_done()
            self.config.BaseStation3200[1].zones[18].verify_status_is_done()
            self.config.BaseStation3200[1].zones[19].verify_status_is_done()
            self.config.BaseStation3200[1].zones[196].verify_status_is_done()
            self.config.BaseStation3200[1].zones[197].verify_status_is_done()
            self.config.BaseStation3200[1].zones[198].verify_status_is_done()
            self.config.BaseStation3200[1].zones[199].verify_status_is_done()
            self.config.BaseStation3200[1].zones[200].verify_status_is_watering()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_21(self):
        """
        - Increment the clock by one minute, putting us at 12:01 am. \n
        - Get data of all zones to update our object attributes. \n
        - We have passed the start time of 12:00 am, meaning we should now be watering. \n
        - Verify that 2 zones are running because the controller has max concurrent zones set to 2. \n
        - Since Zone 200 didn't fully finish watering before the new start time, it goes to soaking to 'queue' it to run
          after everything else. \n
        - Verify Zones Running: \n
            - Zone 1: The first zone on the controller, so it should be running first. \n
            - Zone 2: The second zone on the controller, so it should be running as the second zone. \n
        - Verify Zone Soaking: \n
            - Zone 200: The controller 'queues' this Zone to run by putting it to soaking. \n
        - Verify Zones Waiting: \n
            - All other zones that aren't mentioned above. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the clock by 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all the zone statuses
            self.config.BaseStation3200[1].zones[1].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[6].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[7].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[8].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[9].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[10].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[11].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[12].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[17].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[18].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[19].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[196].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[197].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[198].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[199].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[200].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_22(self):
        """
        - Increment the clock by one minute, putting us at 12:02 am. \n
        - Get data of all zones to update our object attributes. \n
        - We have passed the start time of 12:00 am, meaning we should now be watering. \n
        - Verify that 2 zones are running because the controller has max concurrent zones set to 2. \n
        - Verify Zones Done: (In order of when they were done watering) \n
            - Zone 1: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
        - Verify Zones Watering: \n
            - Zone 2: It is still running because it's has only run 60 seconds of its 120 second runtime. \n
            - Zone 3: The next zone that is supposed to run on the controller. \n
        - Verify Zone Soaking: \n
            - Zone 200: The controller 'queues' this Zone to run by putting it to soaking. \n
        - Verify Zones Waiting: \n
            - All other zones that aren't mentioned above. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the clock by 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all the zone statuses
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[6].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[7].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[8].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[9].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[10].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[11].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[12].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[17].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[18].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[19].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[196].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[197].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[198].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[199].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[200].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_23(self):
        """
        - Increment the clock by one minute, putting us at 12:03 am. \n
        - Get data of all zones to update our object attributes. \n
        - We have passed the start time of 12:00 am, meaning we should now be watering. \n
        - Verify that 2 zones are running because the controller has max concurrent zones set to 2. \n
        - Verify Zones Done: (In order of when they were done watering) \n
            - Zone 1: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 2: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 3: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
        - Verify Zones Watering: \n
            - Zone 4: The next zone that is supposed to run on the controller. \n
            - Zone 5: The next zone that is supposed to run on the controller. \n
        - Verify Zone Soaking: \n
            - Zone 200: The controller 'queues' this Zone to run by putting it to soaking. \n
        - Verify Zones Waiting: \n
            - All other zones that aren't mentioned above. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the clock by 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all the zone statuses
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[7].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[8].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[9].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[10].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[11].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[12].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[17].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[18].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[19].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[196].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[197].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[198].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[199].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[200].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_24(self):
        """
        - Increment the clock by one minute, putting us at 12:04 am. \n
        - Get data of all zones to update our object attributes. \n
        - We have passed the start time of 12:00 am, meaning we should now be watering. \n
        - Verify that 2 zones are running because the controller has max concurrent zones set to 2. \n
        - Verify Zones Done: (In order of when they were done watering) \n
            - Zone 1: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 2: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 3: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 5: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
        - Verify Zones Watering: \n
            - Zone 4: It is still running because it's has only run 60 seconds of its 120 second runtime. \n
            - Zone 6: The next zone that is supposed to run on the controller. \n
        - Verify Zone Soaking: \n
            - Zone 200: The controller 'queues' this Zone to run by putting it to soaking. \n
        - Verify Zones Waiting: \n
            - All other zones that aren't mentioned above. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the clock by 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all the zone statuses
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[7].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[8].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[9].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[10].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[11].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[12].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[17].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[18].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[19].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[196].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[197].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[198].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[199].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[200].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_25(self):
        """
        - Increment the clock by one minute, putting us at 12:05 am. \n
        - Get data of all zones to update our object attributes. \n
        - We have passed the start time of 12:00 am, meaning we should now be watering. \n
        - Verify that 2 zones are running because the controller has max concurrent zones set to 2. \n
        - Verify Zones Done: (In order of when they were done watering) \n
            - Zone 1: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 2: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 3: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 5: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 4: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
        - Verify Zones Watering: \n
            - Zone 6: It is still running because it's has only run 60 seconds of its 120 second runtime. \n
            - Zone 7: The next zone that is supposed to run on the controller. \n
        - Verify Zone Soaking: \n
            - Zone 200: The controller 'queues' this Zone to run by putting it to soaking. \n
        - Verify Zones Waiting: \n
            - All other zones that aren't mentioned above. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the clock by 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all the zone statuses
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[7].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[8].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[9].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[10].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[11].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[12].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[17].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[18].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[19].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[196].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[197].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[198].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[199].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[200].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_26(self):
        """
        - Increment the clock by one minute, putting us at 12:06 am. \n
        - Get data of all zones to update our object attributes. \n
        - We have passed the start time of 12:00 am, meaning we should now be watering. \n
        - Verify that 2 zones are running because the controller has max concurrent zones set to 2. \n
        - Verify Zones Done: (In order of when they were done watering) \n
            - Zone 1: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 2: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 3: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 5: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 4: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 6: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 7: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
        - Verify Zones Watering: \n
            - Zone 8: The next zone that is supposed to run on the controller. \n
            - Zone 9: The next zone that is supposed to run on the controller. \n
        - Verify Zone Soaking: \n
            - Zone 200: The controller 'queues' this Zone to run by putting it to soaking. \n
        - Verify Zones Waiting: \n
            - All other zones that aren't mentioned above. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the clock by 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all the zone statuses
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[9].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[10].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[11].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[12].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[17].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[18].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[19].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[196].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[197].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[198].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[199].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[200].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_27(self):
        """
        - Increment the clock by one minute, putting us at 12:07 am. \n
        - Get data of all zones to update our object attributes. \n
        - We have passed the start time of 12:00 am, meaning we should now be watering. \n
        - Verify that 2 zones are running because the controller has max concurrent zones set to 2. \n
        - Verify Zones Done: (In order of when they were done watering) \n
            - Zone 1: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 2: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 3: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 5: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 4: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 6: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 7: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 9: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
        - Verify Zones Watering: \n
            - Zone 8: It is still running because it's has only run 60 seconds of its 120 second runtime. \n
            - Zone 10: The next zone that is supposed to run on the controller. \n
        - Verify Zone Soaking: \n
            - Zone 200: The controller 'queues' this Zone to run by putting it to soaking. \n
        - Verify Zones Waiting: \n
            - All other zones that aren't mentioned above. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the clock by 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all the zone statuses
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[9].verify_status_is_done()
            self.config.BaseStation3200[1].zones[10].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[11].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[12].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[17].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[18].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[19].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[196].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[197].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[198].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[199].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[200].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_28(self):
        """
        - Increment the clock by one minute, putting us at 12:08 am. \n
        - Get data of all zones to update our object attributes. \n
        - We have passed the start time of 12:00 am, meaning we should now be watering. \n
        - Verify that 2 zones are running because the controller has max concurrent zones set to 2. \n
        - Verify Zones Done: (In order of when they were done watering) \n
            - Zone 1: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 2: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 3: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 5: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 4: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 6: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 7: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 9: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 8: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
        - Verify Zones Watering: \n
            - Zone 10: It is still running because it's has only run 60 seconds of its 120 second runtime. \n
            - Zone 11: The next zone that is supposed to run on the controller. \n
        - Verify Zone Soaking: \n
            - Zone 200: The controller 'queues' this Zone to run by putting it to soaking. \n
        - Verify Zones Waiting: \n
            - All other zones that aren't mentioned above. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the clock by 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all the zone statuses
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].verify_status_is_done()
            self.config.BaseStation3200[1].zones[10].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[11].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[12].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[17].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[18].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[19].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[196].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[197].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[198].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[199].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[200].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_29(self):
        """
        - Increment the clock by one minute, putting us at 12:09 am. \n
        - Get data of all zones to update our object attributes. \n
        - We have passed the start time of 12:00 am, meaning we should now be watering. \n
        - Verify that 2 zones are running because the controller has max concurrent zones set to 2. \n
        - Verify Zones Done: (In order of when they were done watering) \n
            - Zone 1: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 2: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 3: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 5: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 4: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 6: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 7: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 9: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 8: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 10: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 11: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
        - Verify Zones Watering: \n
            - Zone 12: The next zone that is supposed to run on the controller. \n
            - Zone 17: The next zone that is supposed to run on the controller. \n
        - Verify Zone Soaking: \n
            - Zone 200: The controller 'queues' this Zone to run by putting it to soaking. \n
        - Verify Zones Waiting: \n
            - All other zones that aren't mentioned above. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the clock by 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all the zone statuses
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].verify_status_is_done()
            self.config.BaseStation3200[1].zones[10].verify_status_is_done()
            self.config.BaseStation3200[1].zones[11].verify_status_is_done()
            self.config.BaseStation3200[1].zones[12].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[17].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[18].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[19].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[196].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[197].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[198].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[199].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[200].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_30(self):
        """
        - Increment the clock by one minute, putting us at 12:10 am. \n
        - Get data of all zones to update our object attributes. \n
        - We have passed the start time of 12:00 am, meaning we should now be watering. \n
        - Verify that 2 zones are running because the controller has max concurrent zones set to 2. \n
        - Verify Zones Done: (In order of when they were done watering) \n
            - Zone 1: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 2: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 3: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 5: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 4: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 6: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 7: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 9: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 8: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 10: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 11: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 17: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
        - Verify Zones Watering: \n
            - Zone 12: It is still running because it's has only run 60 seconds of its 120 second runtime. \n
            - Zone 18: The next zone that is supposed to run on the controller. \n
        - Verify Zone Soaking: \n
            - Zone 200: The controller 'queues' this Zone to run by putting it to soaking. \n
        - Verify Zones Waiting: \n
            - All other zones that aren't mentioned above. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the clock by 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all the zone statuses
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].verify_status_is_done()
            self.config.BaseStation3200[1].zones[10].verify_status_is_done()
            self.config.BaseStation3200[1].zones[11].verify_status_is_done()
            self.config.BaseStation3200[1].zones[12].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[17].verify_status_is_done()
            self.config.BaseStation3200[1].zones[18].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[19].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[196].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[197].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[198].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[199].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[200].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_31(self):
        """
        - Increment the clock by one minute, putting us at 12:11 am. \n
        - Get data of all zones to update our object attributes. \n
        - We have passed the start time of 12:00 am, meaning we should now be watering. \n
        - Verify that 2 zones are running because the controller has max concurrent zones set to 2. \n
        - Verify Zones Done: (In order of when they were done watering) \n
            - Zone 1: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 2: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 3: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 5: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 4: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 6: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 7: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 9: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 8: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 10: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 11: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 17: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 12: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
        - Verify Zones Watering: \n
            - Zone 18: It is still running because it's has only run 60 seconds of its 120 second runtime. \n
            - Zone 19: The next zone that is supposed to run on the controller. \n
        - Verify Zone Soaking: \n
            - Zone 200: The controller 'queues' this Zone to run by putting it to soaking. \n
        - Verify Zones Waiting: \n
            - All other zones that aren't mentioned above. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the clock by 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all the zone statuses
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].verify_status_is_done()
            self.config.BaseStation3200[1].zones[10].verify_status_is_done()
            self.config.BaseStation3200[1].zones[11].verify_status_is_done()
            self.config.BaseStation3200[1].zones[12].verify_status_is_done()
            self.config.BaseStation3200[1].zones[17].verify_status_is_done()
            self.config.BaseStation3200[1].zones[18].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[19].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[196].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[197].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[198].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[199].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[200].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_32(self):
        """
        - Increment the clock by one minute, putting us at 12:12 am. \n
        - Get data of all zones to update our object attributes. \n
        - We have passed the start time of 12:00 am, meaning we should now be watering. \n
        - Verify that 2 zones are running because the controller has max concurrent zones set to 2. \n
        - Verify Zones Done: (In order of when they were done watering) \n
            - Zone 1: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 2: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 3: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 5: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 4: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 6: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 7: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 9: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 8: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 10: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 11: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 17: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 12: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 18: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 19: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
        - Verify Zones Watering: \n
            - Zone 196: The next zone that is supposed to run on the controller. \n
            - Zone 197: The next zone that is supposed to run on the controller. \n
        - Verify Zone Soaking: \n
            - Zone 200: The controller 'queues' this Zone to run by putting it to soaking. \n
        - Verify Zones Waiting: \n
            - All other zones that aren't mentioned above. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the clock by 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all the zone statuses
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].verify_status_is_done()
            self.config.BaseStation3200[1].zones[10].verify_status_is_done()
            self.config.BaseStation3200[1].zones[11].verify_status_is_done()
            self.config.BaseStation3200[1].zones[12].verify_status_is_done()
            self.config.BaseStation3200[1].zones[17].verify_status_is_done()
            self.config.BaseStation3200[1].zones[18].verify_status_is_done()
            self.config.BaseStation3200[1].zones[19].verify_status_is_done()
            self.config.BaseStation3200[1].zones[196].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[197].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[198].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[199].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[200].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_33(self):
        """
        - Increment the clock by one minute, putting us at 12:13 am. \n
        - Get data of all zones to update our object attributes. \n
        - We have passed the start time of 12:00 am, meaning we should now be watering. \n
        - Verify that 2 zones are running because the controller has max concurrent zones set to 2. \n
        - Verify Zones Done: (In order of when they were done watering) \n
            - Zone 1: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 2: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 3: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 5: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 4: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 6: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 7: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 9: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 8: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 10: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 11: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 17: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 12: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 18: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 19: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 197: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
        - Verify Zones Watering: \n
            - Zone 196: It is still running because it's has only run 60 seconds of its 120 second runtime. \n
            - Zone 198: The next zone that is supposed to run on the controller. \n
        - Verify Zone Soaking: \n
            - Zone 200: The controller 'queues' this Zone to run by putting it to soaking. \n
        - Verify Zones Waiting: \n
            - All other zones that aren't mentioned above. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the clock by 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all the zone statuses
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].verify_status_is_done()
            self.config.BaseStation3200[1].zones[10].verify_status_is_done()
            self.config.BaseStation3200[1].zones[11].verify_status_is_done()
            self.config.BaseStation3200[1].zones[12].verify_status_is_done()
            self.config.BaseStation3200[1].zones[17].verify_status_is_done()
            self.config.BaseStation3200[1].zones[18].verify_status_is_done()
            self.config.BaseStation3200[1].zones[19].verify_status_is_done()
            self.config.BaseStation3200[1].zones[196].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[197].verify_status_is_done()
            self.config.BaseStation3200[1].zones[198].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[199].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[200].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_34(self):
        """
        - Increment the clock by one minute, putting us at 12:14 am. \n
        - Get data of all zones to update our object attributes. \n
        - We have passed the start time of 12:00 am, meaning we should now be watering. \n
        - Verify that 2 zones are running because the controller has max concurrent zones set to 2. \n
        - Verify Zones Done: (In order of when they were done watering) \n
            - Zone 1: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 2: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 3: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 5: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 4: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 6: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 7: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 9: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 8: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 10: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 11: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 17: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 12: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 18: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 19: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 197: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 196: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
        - Verify Zones Watering: \n
            - Zone 198: It is still running because it's has only run 60 seconds of its 120 second runtime. \n
            - Zone 199: The next zone that is supposed to run on the controller. \n
        - Verify Zone Soaking: \n
            - Zone 200: The controller 'queues' this Zone to run by putting it to soaking. \n
        - Verify Zones Waiting: \n
            - All other zones that aren't mentioned above. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the clock by 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all the zone statuses
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].verify_status_is_done()
            self.config.BaseStation3200[1].zones[10].verify_status_is_done()
            self.config.BaseStation3200[1].zones[11].verify_status_is_done()
            self.config.BaseStation3200[1].zones[12].verify_status_is_done()
            self.config.BaseStation3200[1].zones[17].verify_status_is_done()
            self.config.BaseStation3200[1].zones[18].verify_status_is_done()
            self.config.BaseStation3200[1].zones[19].verify_status_is_done()
            self.config.BaseStation3200[1].zones[196].verify_status_is_done()
            self.config.BaseStation3200[1].zones[197].verify_status_is_done()
            self.config.BaseStation3200[1].zones[198].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[199].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[200].verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_35(self):
        """
        - Increment the clock by one minute, putting us at 12:15 am. \n
        - Get data of all zones to update our object attributes. \n
        - We have hit the start time of 12:00 pm, but we haven't gone past it yet so the zones haven't started yet. \n
        - Verify that 2 zones are running because the controller has max concurrent zones set to 2. \n
        - Verify Zones Done: (In order of when they were done watering) \n
            - Zone 1: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 2: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 3: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 5: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 4: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 6: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 7: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 9: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 8: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 10: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 11: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 17: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 12: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 18: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 19: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 197: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 196: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 198: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 199: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
        - Verify Zones Watering: \n
            - Zone 200: The next zone that is supposed to run on the controller. \n
        - Verify Zones Waiting: \n
            - All other zones that aren't mentioned above. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the clock by 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all the zone statuses
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].verify_status_is_done()
            self.config.BaseStation3200[1].zones[10].verify_status_is_done()
            self.config.BaseStation3200[1].zones[11].verify_status_is_done()
            self.config.BaseStation3200[1].zones[12].verify_status_is_done()
            self.config.BaseStation3200[1].zones[17].verify_status_is_done()
            self.config.BaseStation3200[1].zones[18].verify_status_is_done()
            self.config.BaseStation3200[1].zones[19].verify_status_is_done()
            self.config.BaseStation3200[1].zones[196].verify_status_is_done()
            self.config.BaseStation3200[1].zones[197].verify_status_is_done()
            self.config.BaseStation3200[1].zones[198].verify_status_is_done()
            self.config.BaseStation3200[1].zones[199].verify_status_is_done()
            self.config.BaseStation3200[1].zones[200].verify_status_is_watering()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_36(self):
        """
        - Increment the clock by one minute, putting us at 12:16 am. \n
        - Get data of all zones to update our object attributes. \n
        - We have hit the start time of 12:00 pm, but we haven't gone past it yet so the zones haven't started yet. \n
        - Verify that 2 zones are running because the controller has max concurrent zones set to 2. \n
        - Verify Zones Done: (In order of when they were done watering) \n
            - Zone 1: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 2: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 3: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 5: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 4: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 6: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 7: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 9: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 8: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 10: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 11: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 17: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 12: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 18: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 19: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 197: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
            - Zone 196: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 198: It ran for it's runtime of 120 seconds (2 minutes) (all even zones have a 120 second runtime) \n
            - Zone 199: It ran for it's runtime of 60 seconds (1 minute) (all odd zones have a 60 second runtime) \n
        - Verify Zones Watering: \n
            - Zone 200: It is still running because it's has only run 60 seconds of its 120 second runtime. \n
        - Verify Zones Waiting: \n
            - All other zones that aren't mentioned above. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the clock by 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all the zone statuses
            self.config.BaseStation3200[1].zones[1].verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].verify_status_is_done()
            self.config.BaseStation3200[1].zones[10].verify_status_is_done()
            self.config.BaseStation3200[1].zones[11].verify_status_is_done()
            self.config.BaseStation3200[1].zones[12].verify_status_is_done()
            self.config.BaseStation3200[1].zones[17].verify_status_is_done()
            self.config.BaseStation3200[1].zones[18].verify_status_is_done()
            self.config.BaseStation3200[1].zones[19].verify_status_is_done()
            self.config.BaseStation3200[1].zones[196].verify_status_is_done()
            self.config.BaseStation3200[1].zones[197].verify_status_is_done()
            self.config.BaseStation3200[1].zones[198].verify_status_is_done()
            self.config.BaseStation3200[1].zones[199].verify_status_is_done()
            self.config.BaseStation3200[1].zones[200].verify_status_is_watering()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
