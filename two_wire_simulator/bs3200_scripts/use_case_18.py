import sys
from time import sleep
from datetime import datetime, timedelta
from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

__author__ = 'Eldin'


class ControllerUseCase18(object):
    """
    Test name:
        - CN UseCase18 Basic Design Flow
    purpose:
        - This test is two test desing flow not using a flow meter
        Set up a zones on the controller and attach them to a program
            - Make Zones 1 and 11 primary zones, and attach the rest of the zones to these two zones
            - Set up a design flow for all zones
        - Set up a POC and Mainline to manipulate the zones in a certain way described in comments in each step \n
    Coverage Area: \n
        1. Manage flow using design flow instead of a flow meter \n
        2. Able to assign flow to zones \n
        3. Able to assign design flow to main lines \n
        4. Run Programs with a design flow restriction on program 1 with Limit Concurrent active \n
        5. Run Programs with two design flow one on each program, one with Limit Concurrent active and one without \n
        6. Run Programs with two design flow one on each program, both with Limit Concurrent active \n
    """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                     # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        ######################
        setup zones on mainlines
        ######################
        - set up zone  Attributes associated with main lines \n
            - set design flow on zone
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].zones[1].set_design_flow(_gallons_per_minute=20)
            self.config.BaseStation3200[1].zones[2].set_design_flow(_gallons_per_minute=20)
            self.config.BaseStation3200[1].zones[3].set_design_flow(_gallons_per_minute=20)
            self.config.BaseStation3200[1].zones[4].set_design_flow(_gallons_per_minute=12)
            self.config.BaseStation3200[1].zones[5].set_design_flow(_gallons_per_minute=7.5)
            self.config.BaseStation3200[1].zones[11].set_design_flow(_gallons_per_minute=15)
            self.config.BaseStation3200[1].zones[12].set_design_flow(_gallons_per_minute=15)
            self.config.BaseStation3200[1].zones[13].set_design_flow(_gallons_per_minute=7.5)
            self.config.BaseStation3200[1].zones[14].set_design_flow(_gallons_per_minute=7.5)
            self.config.BaseStation3200[1].zones[15].set_design_flow(_gallons_per_minute=7.5)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=10)

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=[480])

            # Add & Configure Program 2
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation3200[1].programs[2].set_enabled()
            self.config.BaseStation3200[1].programs[2].set_max_concurrent_zones(_number_of_zones=3)
            self.config.BaseStation3200[1].programs[2].set_start_times(_st_list=[480])
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # -------------------------------------------------------------------------------------------------------- #
            # Program 1 Zone Programs
            # -------------------------------------------------------------------------------------------------------- #
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=60)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=4)
    
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1)
     
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_as_linked_zone(_primary_zone=1)
      
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[1].zone_programs[4].set_as_linked_zone(_primary_zone=1)
   
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[5].set_as_linked_zone(_primary_zone=1)

            # -------------------------------------------------------------------------------------------------------- #
            # Program 2 Zone Programs
            # -------------------------------------------------------------------------------------------------------- #
            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=11)
            self.config.BaseStation3200[1].programs[2].zone_programs[11].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[2].zone_programs[11].set_run_time(_minutes=60)
            self.config.BaseStation3200[1].programs[2].zone_programs[11].set_cycle_time(_minutes=2)
            self.config.BaseStation3200[1].programs[2].zone_programs[11].set_soak_time(_minutes=4)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=12)
            self.config.BaseStation3200[1].programs[2].zone_programs[12].set_as_linked_zone(_primary_zone=11)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=13)
            self.config.BaseStation3200[1].programs[2].zone_programs[13].set_as_linked_zone(_primary_zone=11)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=14)
            self.config.BaseStation3200[1].programs[2].zone_programs[14].set_as_linked_zone(_primary_zone=11)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=15)
            self.config.BaseStation3200[1].programs[2].zone_programs[15].set_as_linked_zone(_primary_zone=11)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Water Source 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            # Water Source 2
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=2)
            self.config.BaseStation3200[1].water_sources[2].set_enabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=50)

            # Add POC 1 to Water Source 1
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=1)
            # Add & Configure POC 2
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=2)
            self.config.BaseStation3200[1].points_of_control[2].set_enabled()
            self.config.BaseStation3200[1].points_of_control[2].set_target_flow(_gpm=50)
            
            # Add POC 2 to Water Source 2
            self.config.BaseStation3200[1].water_sources[2].add_point_of_control_to_water_source(
                _point_of_control_address=2)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure ML 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_true()
            # Add ML 1 to POC 1
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)

            # Add & Configure ML 2
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=2)
            self.config.BaseStation3200[1].mainlines[2].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].mainlines[2].set_limit_zones_by_flow_to_true()
            # Add ML 2 to POC 2
            self.config.BaseStation3200[1].points_of_control[2].add_mainline_to_point_of_control(_mainline_address=2)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone

        Program | Mainline  | Zones
        ---------------------------------------------
        1       | 1         | 1,2,3,4,5,11,12,13,14,15
        2       | 1         | 
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Add all zones to mainline 1
            for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=zone_address)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        Set the time to be 7:59, which is 1 minute before both of our program's start times \n
        Verify that nothing is started: \n
            - Verify that no Programs are running \n
            - Verify that no zones are watering \n
        Increment the clock two minutes which should trigger both of our Programs to start \n
        Verify that specific zones have started: \n
            Program 1: \n
                - Verify that only zones 1, 2, and 5 are watering because water source 1 is limited to 50 GPM while zones 1
                  and 2 run at 20 GPM each and zone 5 runs at 7.5 bring it up to a grand total of 47.5 GPM leaving
                  nothing for the rest of the water source \n
            Program 2: \n
                - Verify that program 2 does not run because it runs off the flow from water source 1 and there is not
                  enough flow left to run any zones on the controller \n
            Stop the Programs after we verify they started correctly \n
                - This test only covers that the correct zones are running when there are flow restrictions, for a test
                  that verifies every minute ran, look at use_case_17 and use_case 16 \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_date_and_time(_date='02/12/2017', _time='7:59:00')
            self.config.BaseStation3200[1].verify_date_and_time()
            
            for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_done()
            
            for program in range(1, 3):
                self.config.BaseStation3200[1].programs[program].statuses.verify_status_is_done()

            # This should trigger the Programs to start watering
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)
            # primary zones run first than linkined when the controller is deciding which zones to turn ofn
            # Verify that zones 1, 11, are both primary zones so they should be on
            for program in range(1, 3):
                self.config.BaseStation3200[1].programs[program].statuses.verify_status_is_running()
            # program 1 can only run a max of 4 zone and mainline 1 can supply 50 gpm
            # program 2 can only run a max of 3 zone and mainline 1 can supply 50 gpm
            # these are the two primary zones
            for zone_address in [1, 11]:  # this gives you 35 gpm
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_watering()
                # linked zones on program 1 that can be watering
            for zone_address in [4]:  # this give  us 12 gpm
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_watering()
                # linked zones on program 1 that don't have enough water to run
            for zone_address in [2, 3, 5]:  # this give  us 12 gpm
                    self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_waiting_to_water()
                # linked zones on program 2
            for zone_address in [12, 13, 14, 15]:
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_waiting_to_water()

            # Stop the Programs after we verify everything ran as expected
            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        Change flow settings: \n
            - POC 1: Change flow to 75 GPM \n
            - POC 2: Change mainline to 2, and flow to 75 GPM \n
            - Mainline 2: Flow to 25 GPM, LC (limit zones by flow) to false \n

            - Program 2: Set mainline to 2 \n
                ** Update - Programs aren't assigned a mainline anymore, instead add zones to mainline..
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=75)

            self.config.BaseStation3200[1].points_of_control[2].set_target_flow(_gpm=75)

            self.config.BaseStation3200[1].points_of_control[2].add_mainline_to_point_of_control(_mainline_address=2)

            self.config.BaseStation3200[1].mainlines[2].set_target_flow(_gpm=25)
            self.config.BaseStation3200[1].mainlines[2].set_limit_zones_by_flow_to_false()

            # Add zones 11,12,13,14,15 to mainline 2
            for zone_address in [11, 12, 13, 14, 15]:
                self.config.BaseStation3200[1].mainlines[2].add_zone_to_mainline(_zone_address=zone_address)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        Verify that nothing is started: \n
            - Verify that no Programs are running \n
            - Verify that no zones are watering \n
        Start both Programs using commands, and then increment the clock to update the status of the zones \n
        Verify that specific zones have started: \n
        Program 1: \n
            - Verify that only zones 1, 2, and 5 are watering because water source 1 is limited to 50 GPM while zones 1
              and 2 run at 20 GPM each and zone 5 runs at 7.5 bring it up to a grand total of 47.5 GPM leaving
              nothing for the rest of the water source \n
        Program 2: \n
            - Verify that zones 11, 12, and 13 are the only zones running on program 2 because water source 2 is not
              limited by flow so the program runs off the controller and program zone concurrency \n
        Stop the Programs after we verify they started correctly \n
            - This test only covers that the correct zones are running when there are flow restrictions, for a test
              that verifies every minute ran, look at use_case_17 and use_case 16 \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_done()
            
            for program in range(1, 3):
                self.config.BaseStation3200[1].programs[program].statuses.verify_status_is_done()

            # This should trigger the Programs to start watering
            self.config.BaseStation3200[1].programs[1].set_program_to_start()
            self.config.BaseStation3200[1].programs[2].set_program_to_start()
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            # Verify that zones 1, 11, are both primary zones so they should be on
            for program in range(1, 3):
                self.config.BaseStation3200[1].programs[program].statuses.verify_status_is_running()
            for mainline in range(1, 3):
                self.config.BaseStation3200[1].mainlines[mainline].statuses.verify_status_is_running()
            # program 1 can only run a max of 4 zone and mainline 1 can supply 50 gpm and is running on design flow
            # program 2 can only run a max of 3 zone and no water supply for mainline 2 is set to mange water false
            # these are the two primary zones
            for zone_address in [1, 2]:  # this gives you 20 gpm
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_watering()
                # linked zones on program 1 that can be watering
            for zone_address in [5]:  # this give  us 27.5gpm
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_watering()
            for zone_address in [3, 4]:  # this gives us 3 zones running
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_waiting_to_water()
                # linked zones on program 1 that can be watering
            for zone_address in [11, 12, 13]:  # this gives you 35 gpm
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_watering()
            for zone_address in [14, 15]:  #  this gives us 3 zones running
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_waiting_to_water()

             # Stop the Programs after we verify everything ran as expected
            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        Change flow settings: \n
            - Mainline 2: Flow to 25 GPM, LC (limit zones by flow) to true \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].mainlines[2].set_target_flow(_gpm=25)
            self.config.BaseStation3200[1].mainlines[2].set_limit_zones_by_flow_to_true()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_14(self):
        """
        Verify that nothing is started: \n
            - Verify that no Programs are running \n
            - Verify that no zones are watering \n
        Start both Programs using commands, and then increment the clock to update the status of the zones \n
        Verify that specific zones have started: \n
        Program 1: \n
            - Verify that only zones 1, 2, and 5 are watering because water source 1 is limited to 50 GPM while zones 1
              and 2 run at 20 GPM each and zone 5 runs at 7.5 bring it up to a grand total of 47.5 GPM leaving
              nothing for the rest of the water source \n
        Program 2: \n
            - Verify that zones 11, 12, and 13 are the only zones running on program 2 because water source 2 is not
              limited by flow so the program runs off the controller and program zone concurrency \n
        Stop the Programs after we verify they started correctly \n
            - Verify that zones 11 and 13 are the only zones running on program 2 because water source 2 is limited to
              25 GPM because it is now limited by flow and both zones take up 22.5 GPM \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_done()
            
            for program in range(1, 3):
                self.config.BaseStation3200[1].programs[program].statuses.verify_status_is_done()

            # This should trigger the Programs to start watering
            self.config.BaseStation3200[1].programs[1].set_program_to_start()
            self.config.BaseStation3200[1].programs[2].set_program_to_start()
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify that zones 1, 11, are both primary zones so they should be on
            for program in range(1, 3):
                self.config.BaseStation3200[1].programs[program].statuses.verify_status_is_running()
            for mainline in range(1, 3):
                self.config.BaseStation3200[1].mainlines[mainline].statuses.verify_status_is_running()
            # program 1 can only run a max of 4 zone and mainline 1 can supply 50 gpm
            # program 2 can only run a max of 3 zone and mainline 1 can supply 25 gpm
            # these are the two primary zones
            for zone_address in [1, 2]:  # this gives you 20 gpm
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_watering()
                # linked zones on program 1 that can be watering
            for zone_address in [5]:  # this give  us 27.5gpm
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_watering()
            for zone_address in [3, 4]:  # this gives us 3 zones running
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_waiting_to_water()
                # linked zones on program 1 that can be watering
            for zone_address in [11, 13]:  # this give  us 12 gpm
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_watering()
                # linked zones on program 2
            for zone_address in [12, 14, 15]:
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_waiting_to_water()

            # Stop the Programs after we verify everything ran as expected
            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
