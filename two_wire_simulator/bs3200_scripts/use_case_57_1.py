import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration

# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr

from common import helper_methods

__author__ = 'Tim'


class ControllerUseCase57_1(object):
    """
    Test name:
        - CN UseCase57_1 Program Ignore Global Conditions: Stop/Pause Conditions

    Purpose:
        - Set up a full configuration on the controller
        - Set stop and pause conditions on program and verify that they are ignored
          when the program is started.

    Coverage area:
        - program stop condition
        - program pause condition
        - ignore global conditions

    Areas not covered:
        - event days
        - rain pause delay days
        - jumpers

    Test Configuration setup: \n
        1. zones 1-5 on program 1
        2. zones 6 to 10 on program 2
        3. program 1:
            - 8 am start time
            - concurrent zones = 5
        4. program 2:
            - 8 am start time
            - concurrent zones = 5
        5. controller:
            - concurrent zones = 15
            - stop condition = moisture sensor above 25.0 %
            - pause condition = event switch open

    Test Steps - four cases to be tested:
        1. Test 1
            - set time and run through start time
            - verify all zones/programs watering
            - set stop condition
            - run time
            - verify zones/programs stopped
        2. Test 2
            - set time and run through start time
            - verify all zones/programs paused
            - set pause condition
            - run time
            - verify zones/programs paused
        3. Test 3
            - set ignore global conditions on program 1
            - set time and run through start time
            - verify all zones/programs watering
            - set stop condition
            - run time
            - verify zones/programs watering and stopped
        4. Test 4
            - set ignore global conditions on program 1
            - set time and run through start time
            - verify all zones/programs watering
            - set pause condition
            - run time
            - verify zones/programs stopped and paused

    """

    ###############################
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase44' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    ###############################
    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        setup controller
        ############################
        - current zones
        - stop condition = moisture sensor above 25.0%
        - pause condition = event switch open
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=15)

            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=22.5)
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_closed()

            self.config.BaseStation3200[1].add_moisture_stop_condition(_moisture_sensor_address=1)
            self.config.BaseStation3200[1].moisture_stop_conditions[1].set_moisture_mode_to_upper_limit()
            self.config.BaseStation3200[1].moisture_stop_conditions[1].set_moisture_threshold(_percent=25.0)

            self.config.BaseStation3200[1].add_switch_pause_condition(_event_switch_address=1)
            self.config.BaseStation3200[1].event_switch_pause_conditions[1].set_switch_pause_time(_minutes=5)
            self.config.BaseStation3200[1].event_switch_pause_conditions[1].set_switch_mode_to_open()



        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_2(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=5)

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation3200[1].programs[2].set_enabled()
            self.config.BaseStation3200[1].programs[2].set_max_concurrent_zones(_number_of_zones=5)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_3(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add zone programs to Program 1
            for zone_ad in range(1, 6):
                self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=zone_ad)
                self.config.BaseStation3200[1].programs[1].zone_programs[zone_ad].set_run_time(_minutes=10)
                self.config.BaseStation3200[1].programs[1].zone_programs[zone_ad].set_cycle_time(_minutes=10)
                self.config.BaseStation3200[1].programs[1].zone_programs[zone_ad].set_soak_time(_minutes=10)

            # Add zone programs to Program 2
            for zone_ad in range(6, 11):
                self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=zone_ad)
                self.config.BaseStation3200[1].programs[2].zone_programs[zone_ad].set_run_time(_minutes=10)
                self.config.BaseStation3200[1].programs[2].zone_programs[zone_ad].set_cycle_time(_minutes=10)
                self.config.BaseStation3200[1].programs[2].zone_programs[zone_ad].set_soak_time(_minutes=10)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ###############################
        Test 1 -  start program verify zones stopped on condition
        ###############################

        1. Test 1
            - set time and run through start time
            - verify all zones watering
            - set stop condition
            - run time
            - verify zones stopped and paused

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set time to 7:59 am
            self.config.BaseStation3200[1].set_date_and_time(_date='1/31/2018', _time='7:59:00')

            # initial values = NOT triggered
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_closed()
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=22.5)

            # start both programs = everything running
            self.config.BaseStation3200[1].do_increment_clock(minutes=3)

            # verify zones
            for zone in range(1, 6):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(6, 11):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # trigger condition
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=25.5)

            # advance to cause conditions to execute
            self.config.BaseStation3200[1].do_increment_clock(minutes=3)

            # verify zones
            for zone in range(1, 6):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(6, 11):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()

            # verify message for stop condition
            self.config.BaseStation3200[1].moisture_stop_conditions[1].messages.verify_stop_condition_with_moisture_sensor_on_controller_message()

            # clean up after this step, ready for next
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=22.5)

            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # remove the pause message - self removal
            # self.config.BaseStation3200[1].moisture_stop_conditions[1].messages.clear_stop_condition_with_moisture_sensor_on_controller_message()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ###############################
        Test 2 -  start program verify zones paused on condition
        ###############################

        2. Test 2
            - set time and run through start time
            - verify all zones/programs paused
            - set pause condition
            - run time
            - verify zones/programs paused

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set time to 7:59 am
            self.config.BaseStation3200[1].set_date_and_time(_date='1/31/2018', _time='7:59:00')

            # initial values = NOT triggered
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_closed()
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=22.5)

            # start both programs = everything running
            self.config.BaseStation3200[1].do_increment_clock(minutes=3)

            # verify zones
            for zone in range(1, 6):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(6, 11):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # trigger condition
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_open()

            # advance to cause conditions to execute
            self.config.BaseStation3200[1].do_increment_clock(minutes=3)

            # verify zones
            for zone in range(1, 6):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_paused()
            for zone in range(6, 11):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_paused()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_paused()

            # verify message for pause condition
            self.config.BaseStation3200[1].event_switch_pause_conditions[1].messages.verify_pause_condition_with_event_switch_on_controller_message()

            # clean up after this step, ready for next
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_closed()

            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # clear message for pause condition
            self.config.BaseStation3200[1].event_switch_pause_conditions[1].messages.clear_pause_condition_with_event_switch_on_controller_message()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ###############################
        Test 3 -  start program verify zones paused and not paused on condition
        ###############################

        3. Test 3
            - set ignore global conditions on program 1
            - set time and run through start time
            - verify all zones/programs watering
            - set stop condition
            - run time
            - verify:
                - Zones 1-5 are still watering because program 1 was set to ignore global conditions.
                - Zones 6-10 are not watering because program 2 does not ignore global conditions.

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set time to 7:59 am
            self.config.BaseStation3200[1].set_date_and_time(_date='1/31/2018', _time='7:59:00')

            # initial values = NOT triggered
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_closed()
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=22.5)

            # change program 1 to ignore global conditions
            self.config.BaseStation3200[1].programs[1].set_ignore_global_conditions()

            # start both programs = everything running
            self.config.BaseStation3200[1].do_increment_clock(minutes=3)

            # verify zones
            for zone in range(1, 6):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(6, 11):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # trigger condition
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=25.5)

            # advance to cause conditions to execute
            self.config.BaseStation3200[1].do_increment_clock(minutes=3)

            # verify zones
            for zone in range(1, 6):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(6, 11):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()

            # verify stop message
            self.config.BaseStation3200[1].moisture_stop_conditions[1].messages.verify_stop_condition_with_moisture_sensor_on_controller_message()

            # clean up after this step, ready for next
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=22.5)
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_closed()

            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # clear stop message - self clearing
            # self.config.BaseStation3200[1].moisture_stop_conditions[1].messages.clear_stop_condition_with_moisture_sensor_on_controller_message()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ###############################
        Test 4 -  start program verify zones paused/running on condition
        ###############################

        4. Test 4
            - set ignore global conditions on program 1
            - set time and run through start time
            - verify all zones/programs watering
            - set pause condition
            - run time
            - verify:
                - Zones 1-5 are still watering because program 1 was set to ignore global conditions.
                - Zones 6-10 are paused because program 2 does not ignore global conditions.

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set time to 7:59 am
            self.config.BaseStation3200[1].set_date_and_time(_date='1/31/2018', _time='7:59:00')

            # initial values = NOT triggered
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_closed()
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=22.5)

            # start both programs = everything running
            self.config.BaseStation3200[1].do_increment_clock(minutes=3)

            # verify zones
            for zone in range(1, 6):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(6, 11):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # trigger condition
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_open()

            # advance to cause conditions to execute
            self.config.BaseStation3200[1].do_increment_clock(minutes=3)

            # verify zones
            for zone in range(1, 6):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(6, 11):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_paused()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_paused()

            # verify message for pause condition
            self.config.BaseStation3200[1].event_switch_pause_conditions[1].messages.verify_pause_condition_with_event_switch_on_controller_message()

            # clean up after this step, ready for next
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_closed()

            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # clear message for pause condition
            self.config.BaseStation3200[1].event_switch_pause_conditions[1].messages.clear_pause_condition_with_event_switch_on_controller_message()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

