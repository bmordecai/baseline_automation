
import sys
from time import sleep
from datetime import datetime, timedelta
from common.configuration import Configuration

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
from common.objects.base_classes.web_driver import *
# Objects

# import log_handler for logging functionality
from common.logging_handler import log_handler

__author__ = 'Tige'


class ControllerUseCase9(object):
    """
    Test name:
        - 3200 RAM Test

    purpose:
        - set up a test that will max out the Ram on the 3200
            - setup 6 different zones using Weather based watering
            - verify that teh ETc runtime values change when the eto input value changes
            - verify that correct cycle and soak are performed
            - verify that controller starts watering when deficit is below 50%
            - verify that the controller meets a 80% efficacy rating

    Coverage area: \n
        -setting up devices:
            - Loading \n
            - Searching \n
            - Addressing \n
                - Setting:
                    - descriptions
        - setting Programs: \n
            - start times
            - watering days
            - water windows
            - zone concurrency
            - assign zones to Programs \n
        - create 99 Programs
        - create 200 Zones
        - Add zones to Programs until the 3200 runs out of ram
            - Create a program
            - add zone
            - look for message say low memory
    Date References:
        - configuration for script is located common\configuration_files\ram_test_3200.json
        - the devices and addresses range is read from the .json file

    """
# program_range = range(1, 100)
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)


    ###############################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            if self.config.BaseStation3200[1].mu >= 99:
                self.config.BaseStation3200[1].do_increment_clock(minutes=1)
                self.config.BaseStation3200[1].messages.verify_memory_usage_message()
                self.config.BaseStation3200[1].messages.clear_memory_usage_message()

            program_start_times = [120]  # 2:00 am start time
            program_water_windows = ['111111111111111111111111']

            for program_address in range(1, 99):

                self.config.BaseStation3200[1].add_program_to_controller(_program_address=program_address)
                self.config.BaseStation3200[1].programs[program_address].set_enabled()
                self.config.BaseStation3200[1].programs[program_address].set_water_window(_ww=program_water_windows)
                self.config.BaseStation3200[1].programs[program_address].set_priority_level(_pr_level=1)
                self.config.BaseStation3200[1].programs[program_address].set_max_concurrent_zones(_number_of_zones=1)
                self.config.BaseStation3200[1].programs[program_address].set_seasonal_adjust(_percent=100)
                self.config.BaseStation3200[1].programs[
                    program_address].set_watering_intervals_to_selected_days_of_the_week(_sun=True, _mon=True, _tues=True,
                                                                                         _wed=True, _thurs=True, _fri=True,
                                                                                         _sat=True)
                self.config.BaseStation3200[1].programs[program_address].set_start_times(_st_list=program_start_times)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        for each program add a zone than check to see what the memory usage of the controller is
        after 200 zones are add to the program increment the clock 1 minute
        after the controller reach 99% usage verify that a message is set "low memory detected"
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for program_address in range(1, 4):

                for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):

                    # Add Program Zone to Program
                    self.config.BaseStation3200[1].programs[program_address].add_zone_to_program(
                        _zone_address=zone_address)
                    self.config.BaseStation3200[1].programs[program_address].zone_programs[zone_address]\
                        .set_as_timed_zone()
                    self.config.BaseStation3200[1].programs[program_address].zone_programs[zone_address]\
                        .set_run_time(_minutes=1)

                    self.config.BaseStation3200[1].get_memory_usage()

                    if self.config.BaseStation3200[1].mu >= 99:
                        self.config.BaseStation3200[1].messages.verify_memory_usage_message()
                        self.config.BaseStation3200[1].messages.clear_memory_usage_message()
                    else:
                        print "Controller '{0}' is using '{1}' total Programs with a total of '{2}' zone programs and " \
                              "is currently using {3}% of controllers ram memory." \
                            .format(
                                self.config.BaseStation3200[1].mac,             # {0} Controller mac address
                                program_address,                                # {1} Total number of Programs used
                                str(200*(program_address-1) + zone_address),    # {2} Total number of zones used
                                self.config.BaseStation3200[1].mu               # {3} memory the controller has used
                                )
                start = datetime.now()
                self.config.BaseStation3200[1].do_increment_clock(minutes=1)
                finish = datetime.now()
                elapsed_time = finish-start
                if elapsed_time >= timedelta(seconds=25):
                    e_msg = "It's taking to long to process the command do to the controller response time. It is " \
                            "taking '{0}' seconds to increment clock waiting on the reply from the controller. The " \
                            "response time can not be greater than '25'seconds  " \
                        .format(
                                elapsed_time
                        )
                    raise Exception, Exception(e_msg)
                else:
                    e_msg = "It took '{0}' seconds to increment clock waiting on the reply from the controller"\
                        .format(
                            elapsed_time
                            )
                    print e_msg

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]


