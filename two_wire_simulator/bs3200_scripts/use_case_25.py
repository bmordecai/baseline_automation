import sys
from time import sleep

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

__author__ = 'Eldin'


class ControllerUseCase25(object):
    """
    Test name:
        - CN UseCase25 Primary Zone Zero Runtime
    Purpose:
        - Replicate a bug that was found on the 3200 where we would get a 'FAIL' when trying to connect to BaseManager
          after a reboot where we had set the runtime of a primary zone to 0. \n
    Coverage Area: \n
        1. Makes sure that the bug mentioned above doesn't appear anymore. (Meaning that we can connect to BaseManager
           after we do the reboot. \n
        2. Make sure that the linked zones of the primary zone which had it's runtime set to zero are no longer on the
           program. \n
    """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        program_start_time_8am_9am_10am_11am = [480, 540, 600, 660]
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # Add and configure Program 1
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_start_time_8am_9am_10am_11am)

            # Add and configure Program 2
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation3200[1].programs[2].set_enabled()
            self.config.BaseStation3200[1].programs[2].set_start_times(_st_list=program_start_time_8am_9am_10am_11am)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        - Set up zones with Programs. \n
            - Zone 1 Program 1 (Primary Zone)
                - Run Time: 120 seconds \n
                - Cycle Time: 120 seconds \n
                - Soak Time: 240 seconds \n
            - Zone 2 Program 1 (Linked Zone to Primary Zone 1)
                - Tracking Ratio: 100% \n
                - Primary Zone: 1 \n
            - Zone 3 Program 1 (Linked Zone to Primary Zone 1)
                - Tracking Ratio: 150% \n
                - Primary Zone: 1 \n
            - Zone 4 Program 2 (Primary Zone)
                - Run Time: 120 seconds \n
                - Cycle Time: 120 seconds \n
                - Soak Time: 240 seconds \n
            - Zone 5 Program 2 (Linked Zone to Primary Zone 4)
                - Tracking Ratio: 100% \n
                - Primary Zone: 4 \n
            - Zone 6 Program 2 (Linked Zone to Primary Zone 4)
                - Tracking Ratio: 50% \n
                - Primary Zone: 4 \n
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=4)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_as_linked_zone(_primary_zone=1, _tracking_ratio=150)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[2].zone_programs[4].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[2].zone_programs[4].set_run_time(_minutes=4)
            self.config.BaseStation3200[1].programs[2].zone_programs[4].set_cycle_time(_minutes=4)
            self.config.BaseStation3200[1].programs[2].zone_programs[4].set_soak_time(_minutes=8)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=5)
            self.config.BaseStation3200[1].programs[2].zone_programs[5].set_as_linked_zone(_primary_zone=4)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=6)
            self.config.BaseStation3200[1].programs[2].zone_programs[6].set_as_linked_zone(_primary_zone=4, _tracking_ratio=50)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        - Increment the clock by a minute so the controller has time to save it's programming. \n
        - Put the controller in real time \n
        - Reboot the controller \n
        - Wait for 1 minute \n
        - Verify that BaseManager is reconnected (And give it three minutes to reconnect)\n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].set_sim_mode_to_off()
            time.sleep(60)
            self.config.BaseStation3200[1].do_reboot_controller()
            print "Waiting for 1 minute..."
            time.sleep(60)
            time.sleep(60)
            self.config.BaseStation3200[1].basemanager_connection[1].wait_for_bm_connection()

            for mv in sorted(self.config.BaseStation3200[1].master_valves.keys()):
                self.config.BaseStation3200[1].master_valves[mv].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        - Set the Zone 4 Program runtime to 0. \n
        This should remove zone from program
        - verify zone 4 is gone
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].programs[2].zone_programs[4].set_run_time(_minutes=0)
            # verify that zone is remove from the program
            # verify all linked zones get remove
            # verify that basemanager can still edit programs
            self.config.BaseStation3200[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        - Put the controller in real time \n
        - Reboot the controller \n
        - Wait for 1 minute \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_sim_mode_to_off()
            time.sleep(60)
            self.config.BaseStation3200[1].do_reboot_controller()
            print "Waiting for 1 minute..."
            time.sleep(60)
            time.sleep(60)
            self.config.BaseStation3200[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        - Verify that the Primary Zone and its Linked Zones are no longer on the program. \n
        - Verify that BaseManager is reconnected (And give it three minutes to reconnect)\n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            try:
                self.config.BaseStation3200[1].programs[2].zone_programs[4].get_data()
            except Exception as ae:
                if ae.message == 'BC response received from controller':
                    print "Controller Returned a BC for command 'get,pz=4,pg=2', which is what we expected since we" \
                          "put the runtime of PZ=4,PG=2 to 0, which should clear the zone."
            try:
                self.config.BaseStation3200[1].programs[2].zone_programs[5].get_data()
            except Exception as ae:
                if ae.message == 'BC response received from controller':
                    print "Controller Returned a BC for command 'get,pz=5,pg=2', which is what we expected since we" \
                          "put the runtime of Zone 4 Program 2 to 0, which was the primary zone of this linked zone."
            try:
                self.config.BaseStation3200[1].programs[2].zone_programs[6].get_data()
            except Exception as ae:
                if ae.message == 'BC response received from controller':
                    print "Controller Returned a BC for command 'get,pz=6,pg=2', which is what we expected since we" \
                          "put the runtime of Zone 4 Program 2 to 0, which was the primary zone of this linked zone."

            self.config.BaseStation3200[1].basemanager_connection[1].wait_for_bm_connection()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

