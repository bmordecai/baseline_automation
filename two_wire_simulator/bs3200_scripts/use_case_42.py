import sys
from time import sleep

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

__author__ = 'Tige'


class ControllerUseCase1(object):
    """
    Test name:
        - Substation Disconnect/Reconnection with 3200.

    Multi-Controller Setup Guide:

    Purpose:
        - Test that the 3200 and Substation will actively attempt to reconnect after losing connection to each other.

    Coverage:
        After establishing connection between 3200 and Substation initially,

        1. Disable substation on 3200 and verify:
            - Substation is disconnected from 3200 via BM status
            - 3200 disconnected from substation status

        2. Re-enable substation on 3200 and verify:
            - 3200 is connected to substation via status
            - Substation is connected via BM status

        3. Reboot 3200 and wait up to 5 minutes verifying at 1 minute intervals:
            - 3200 is connected to substation via status
            - Substation is connected via BM status

        4. Reboot Substation and wait up to 5 minutes verifying at 1 minute intervals:
            - 3200 is connected to substation via status
            - Substation is connected via BM status

    IMPORTANT NOTES FOR CURRENT RESULTS FOR THIS TEST:
        - Currently on a basic disable/enable substation on the 3200, it takes roughly 2-6 increment clock calls per
          controller to get the expected status.

        - Currently on a 3200 reboot, it takes 4-6 increment clock calls per controller to get the expected status.

        - Currently on a Substation reboot, it takes 11-12 increment clock calls per controller to get the expected
          status.

    Test name:
        - CN UseCase1 RebootConfigVerify
    purpose:

        - set up a full configuration on the controller
            - verify that no programming is lost when you:
                - reboot the controller
                - update the firmware of the controller
                - replace a device
            - verify that after a reboot the controller restarts
                - program return to current watering state
                - zones return to current watering state
                - device return to current conditions (event paused stays paused)
            - verify that firmware update take effects:
                - even with certain devices or attributes are disabled
        - set up a full configuration on the Flowstation
            - verify that no programming is lost when you:
                - reboot the controller
                - update the firmware of the controller
                - replace a device
            - verify that after a reboot the controller restarts
                - program return to current watering state
                - zones return to current watering state
                - device return to current conditions (event paused stays paused)
            - verify that firmware update take effects:
                - even with certain devices or attributes are disabled
    Coverage area: \n
        -setting up devices:
            - Loading \n
            - Searching \n
            - Addressing \n
                - Setting:
                    - descriptions
                    - locations \n

        - setting Programs: \n
            - start times
            - watering days
            - water windows
            - zone concurrency
            - assign Programs to water sources \n
            - assign zones to Programs \n
            - set up primary linked zones \n
            - give each zone a run time of 1 hour and 30 minutes \n
            - assign moisture sensors to a primary zone 1 and set to lower limit watering

        - setting up mainline: \n
            set main line \n
                - limit zones by flow \n
                - pipe fill time\n
                - target flow\n
                - high variance limit
                - high variance shut down \n
                - low variance limit
                - low variance shut down \n

        - setting up poc's: \n
            - enable POC \n
            - assign:
                - master valve
                - flow meter \n
                - target flow\n
                - main line \n
                - priority
                    - low
                    - medium
                    - high\n
            - set high flow limit
            - high flow shut down \n
            - set unscheduled flow limit
            - enable unscheduled flow shut down \n
            - set water budget
            - enable the water budget shut down \n
            - enable water rationing \n
            - empty conditions
                - event switch\n
                - set switch empty condition to closed \n
                - set empty wait time\n

        - Reboot the controller:
            - verify all setting \n
                - zone decoder
                    - verify all settings were not lost \n
                - moisture sensor
                    - verify all settings were not lost \n
                - flow decoder
                    - verify all settings were not lost \n
                - event decoder
                    - verify all settings were not lost  \n
                - temperature decoder
                    - verify all settings were not lost \n
        - Replace devices: \n
            - zone decoder
                - verify all settings were not lost \n
            - moisture sensor
                - verify the primary zone setting were not lost \n
            - flow decoder
                - verify all setting were deleted\n
            - event decoder
                - verify all setting were deleted \n
            - temperature decoder
                - verify all setting were delete\n
        - firmware update: \n
            - disable
                - all one of each device type \n
                - program
                - mainline
                - poc
            - verify configuration all stayed \n
    Date References:
        - configuration for script is located common\configuration_files\EPA_test_configuration.json
        - the devices and addresses range is read from the .json file

    """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.flowstation = self.config.flowstations
        self.controller = self.config.controllers
        self.substation = self.config.substations


    def run_use_case(self):
        """
        Step 1:
            - configure controller:
                -  initiate controller to a known state so that it doesnt have a configuration or any devices loaded
                - turn on echo so the commands are displayed in the console
                - turn on sim mode so the clock can be stopped
                - stop the clock
                - Set the date and time so  that the controller is in a known state
                - turn on faux IO
                - clear all devices
                - clear all programming

            - configure basemanager: \n
                - verify the controller is connected to basemanager \n

        step 2:
            - setting up devices:
                - Loading devices into controller
                - Searching for devices so that they can be addressed
                - Address zones and master valves
                - Set all devices:
                    - descriptions
                    - locations
                    - default parameters
        Step 3:
            - setting up programming:
                - set_up_programs
                - assign zones to Programs \n
                - set up primary linked zones \n
                - give each zone a run time of 1 hour and 30 minutes \n
                - give each program a start time of 8:00 A.M. \n
        step 4:
            - setup zone Programs: \n
                - must make zone 200 a primary zone before you can link zones to it \n
                - assign sensor to primary zone 1
                - assign moisture sensors to a primary zone 1 and set to lower limit watering
        Step 5:
            - setting up mainlines: \n
                - set up main line 1 \n
                    set limit zones by flow to true \n
                    set the pipe fill time to 4 minutes \n
                    set the target flow to 500 \n
                    set the high variance limit to 5% and enable the high variance shut down \n
                    set the low variance limit to 20% and enable the low variance shut down \n
                \n
                - set up main line 8 \n
                    set limit zones by flow to true \n
                    set the pipe fill time to 1 minute \n
                    set the target flow to 50 \n
                    set the high variance limit to 20% and disable the high variance shut down \n
                    set the low variance limit to 5% and disable the low variance shut down \n
        Step 6:
            - setting up POCs: \n
                - set up POC 1 \n
                    - enable POC 1 \n
                    - assign master valve TMV0003 and flow meter TWF0003 to POC 1 \n
                    - assign POC 1 a target flow of 500 \n
                    - assign POC 1 to main line 1 \n
                    - set POC priority to 2-medium \n
                    - set high flow limit to 550 and enable high flow shut down \n
                    - set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
                    - set water budget to 100000 and enable the water budget shut down \n
                    - enable water rationing \n
                \n
                - set up POC 8 \n
                    - enable POC 8 \n
                    - assign master valve TMV0004 and flow meter TWF0004 to POC 8 \n
                    - assign POC 8 a target flow of 50 \n
                    - assign POC 8 to main line 8 \n
                    - set POC priority to 3-low \n
                    - set high flow limit to 75 and disable high flow shut down \n
                    - set unscheduled flow limit to 5 and disable unscheduled flow shut down \n
                    - set water budget to 1000 and disable water budget shut down \n
                    - disable water rationing \n
                    - assign event switch TPD0001 to POC 8 \n
                    - set switch empty condition to closed \n
                    - set empty wait time to 540 minutes \n
        Step 7:
            - reboot the controller:
                - verify that all attributes for each device did not change
                - because the clock was increment we can verify status and verify that the zone didn't start \n
                  watering after the reboot \n

        """
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    self.step_1()
                    self.step_2()
                    self.step_3()
                    self.step_4()
                    self.step_5()
                    self.step_6()
                    self.step_7()
                    self.step_8()
                    self.step_9()
                    self.step_10()
                    self.step_11()
                    self.step_12()
                    self.step_13()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    # self.update_controller_firmware_from_bm()
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    def step_1(self):
        """
        1) Initialize controllers FlowStation, and SubStations.
        """
        method = "\n########################     Running " + sys._getframe().f_code.co_name +"  ######################"
        print method
        try:
            # Init controller
            self.controller[1].initialize_for_test()
            self.controller[2].initialize_for_test()

            # init Flow Station
            self.flowstation[1].init_fs()

            # Init each substation created/configured.
            for substation_address in self.config.substations.keys():
                self.substation[substation_address].init_sb()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_2(self):
        """
        connect controllers to BaseManager and verify
        connect flowstation to BaseManager and verify

        :param self:
        :type self:
        :return:
        :rtype:
        """
        method = "\n########################     Running " + sys._getframe().f_code.co_name +"  ######################"
        print method
        try:
            self.config.BaseStation3200[1].basemanager_connection[1].set_controller_to_connect_using_ethernet()
            self.config.BaseStation3200[1].basemanager_connection[1].verify_ip_address_state()


            self.config.BaseStation3200[2].basemanager_connection[1].verify_ip_address_state()

            self.config.flowstation[1].basemanager_connection[1].set_controller_to_connect_using_ethernet()
            self.config.flowstation[1].basemanager_connection[1].verify_ip_address_state()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_3(self):
        """
        connect controllers to flowstation and verify
        connect flowstation to BaseManager and verify

        :param self:
        :type self:
        :return:
        :rtype:
        """
        method = "\n########################     Running " + sys._getframe().f_code.co_name + "  ######################"
        print method
        try:
            self.config.BaseStation3200[1].flowstation_connection[1].connect_cn_to_flowstation()
            self.config.BaseStation3200[2].flowstation_connection[1].connect_cn_to_flowstation()

            self.config.BaseStation3200[1].flowstation_connection[1].verify_ip_address_state()
            self.config.BaseStation3200[2].flowstation_connection[1].verify_ip_address_state()


        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_4(self):
        """
        assign controller to Flowstation

        :param self:
        :type self:
        :return:
        :rtype:
        """
        method = "\n########################     Running " + sys._getframe().f_code.co_name + "  ######################"
        print method
        try:
            self.config.flowstation[1].BaseStation3200[1].assign_cn_to_flowstation()
            self.config.flowstation[1].BaseStation3200[2].assign_cn_to_flowstation()
            self.config.flowstation[1].BaseStation3200[1].verify_cn_assinged_to_flowstation()
            self.config.flowstation[1].BaseStation3200[2].verify_cn_assinged_to_flowstation()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_6(self):
        """

        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # TODO verify that each controller create these objects
            for controller in range(1,3):
                self.config.BaseStation3200[controller].create_watersource_objects()
                self.config.BaseStation3200[controller].create_mainline_objects()
                self.config.BaseStation3200[controller].create_points_of_control_objects()
                # make master valve two a booster so it can be assigned to a program
                self.config.BaseStation3200[1].master_valves[2].set_booster_enable_state(booster_state=opcodes.true)
            # TODO need to add a pump to the program as a booster
            # self.config.pumps[1].set_booster_enable_state_on_cn(booster_state=opcodes.true)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_7(self):
        """
        set_up_programs
        assign zones to Programs \n
        set up primary linked zones \n
        give each zone a run time of 1 hour and 30 minutes \n
        give each program a start time of 8:00 A.M. \n
        must make zone 200 a primary zone before you can link zones to it \n
        assign sensor to primary zone 1
        #assign moisture sensors to a primary zone 1 and set to lower limit watering
        Assign boost pump to program 99
        """
        # this is set in the PG3200 object
        # TODO need to have concurrent zones per program added
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            program_number_1_start_times = [480, 540, 600, 660]
            program_number_3_start_times = [480, 540, 600, 660]
            program_number_4_start_times = [480, 540, 600, 660]
            program_number_99_start_times = [480, 540, 600, 660]
            program_number_1_watering_days = [0, 1, 0, 1, 0, 1, 0]  # runs monday, wednesday, friday
            program_number_3_watering_days = []
            program_number_4_watering_days = [0, 1, 0, 1, 0, 1, 0]  # runs monday, wednesday, friday
            program_number_99_watering_days = [0, 0, 0, 0, 0, 0, 8, 8, 6, 5, 5, 4, 3, 3, 3, 3, 4, 5, 6, 7, 0, 0, 0, 0]
            program_number_1_water_windows = ['011111100001111111111110']
            program_number_3_water_windows = ['011111100001111111111110']
            program_number_4_water_windows = ['011111100001111111111110']
            program_number_99_water_windows = ['011111100000111111111110',
                                               '011111100001111111111111',
                                               '011111100001111111111110',
                                               '011111100001111111111110',
                                               '011111100001111111111110',
                                               '011111100001111111111110',
                                               '011111100001111111111111']
            self.config.BaseStation3200[1].programs[1] = PG3200V16(_ad=1,
                                                _en=opcodes.true,
                                                _ww=program_number_1_water_windows,
                                                _pr=1,
                                                _mc=1,
                                                _sa=100,
                                                _ci=opcodes.week_days,
                                                _di=None,
                                                _wd=program_number_1_watering_days,
                                                _sm=[],
                                                _st=program_number_1_start_times,
                                                _bp='')
            self.config.BaseStation3200[1].programs[3] = PG3200V16(_ad=3,
                                                _en=opcodes.true,
                                                _ww=program_number_3_water_windows,
                                                _pr=1,
                                                _mc=1,
                                                _sa=100,
                                                _ci=opcodes.odd_day,
                                                _di=None,
                                                _wd=program_number_3_watering_days,
                                                _sm=[],
                                                _st=program_number_3_start_times,
                                                _bp='')
            self.config.BaseStation3200[1].programs[4] = PG3200V16(_ad=4,
                                                _en=opcodes.true,
                                                _ww=program_number_4_water_windows,
                                                _pr=3,
                                                _mc=4,
                                                _sa=100,
                                                _ci=opcodes.week_days,
                                                _di=None,
                                                _wd=program_number_4_watering_days,
                                                _sm=[],
                                                _st=program_number_4_start_times,
                                                _bp='')
            self.config.BaseStation3200[1].programs[99] = PG3200V16(_ad=99,
                                                 _en=opcodes.true,
                                                 _ww=program_number_99_water_windows,
                                                 _pr=1,
                                                 _mc=1,
                                                 _sa=100,
                                                 _ci=opcodes.historical_calendar,
                                                 _di=None,
                                                 _wd=[],
                                                 _sm=program_number_99_watering_days,
                                                 _st=program_number_99_start_times,
                                                 _bp='2')
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_5(self):
        """

        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Zone Programs
            self.config.BaseStation3200[1].zone_programs[1] = ZoneProgram(zone_ad=self.config.zones[1],
                                                                     prog_ad=self.config.programs[1],
                                                                     _rt=900,
                                                                     _ct=300,
                                                                     _so=300,
                                                                     _pz=1)
            self.config.BaseStation3200[1].zone_programs[2] = ZoneProgram(zone_ad=self.config.zones[2],
                                                                     prog_ad=self.config.programs[1],
                                                                     _rt=self.config.zone_programs[1].rt,
                                                                     _ct=self.config.zone_programs[1].ct,
                                                                     _so=self.config.zone_programs[1].so,
                                                                     _ra=100,
                                                                     _pz=1)
            self.config.BaseStation3200[1].zone_programs[49] = ZoneProgram(zone_ad=self.config.zones[49],
                                                                      prog_ad=self.config.programs[3],
                                                                      _rt=1200,
                                                                      _ct=600,
                                                                      _so=3600,
                                                                      _ws=opcodes.timed)
            self.config.BaseStation3200[1].zone_programs[50] = ZoneProgram(zone_ad=self.config.zones[50],
                                                                      prog_ad=self.config.programs[3],
                                                                      _rt=1200,
                                                                      _ct=600,
                                                                      _so=3600,
                                                                      _ws=opcodes.timed)
            self.config.BaseStation3200[1].zone_programs[50] = ZoneProgram(zone_ad=self.config.zones[50],
                                                                      prog_ad=self.config.programs[4],
                                                                      _rt=1200,
                                                                      _ct=600,
                                                                      _so=3600,
                                                                      _ws=opcodes.timed)

            self.config.BaseStation3200[1].zone_programs[200] = ZoneProgram(zone_ad=self.config.zones[200],
                                                                       prog_ad=self.config.programs[99],
                                                                       _rt=1980,
                                                                       _pz=200,
                                                                       _ct=180,
                                                                       _so=780)

            # Zone Programs linked to Zone 200
            self.config.BaseStation3200[1].zone_programs[197] = ZoneProgram(zone_ad=self.config.zones[197],
                                                                       prog_ad=self.config.programs[99],
                                                                       _rt=self.config.zone_programs[200].rt,
                                                                       _ct=self.config.zone_programs[200].ct,
                                                                       _so=self.config.zone_programs[200].so,
                                                                       _pz=200,
                                                                       _ra=50)
            self.config.BaseStation3200[1].zone_programs[198] = ZoneProgram(zone_ad=self.config.zones[198],
                                                                       prog_ad=self.config.programs[99],
                                                                       _rt=self.config.zone_programs[200].rt,
                                                                       _ct=self.config.zone_programs[200].ct,
                                                                       _so=self.config.zone_programs[200].so,
                                                                       _pz=200,
                                                                       _ra=100)
            self.config.BaseStation3200[1].zone_programs[199] = ZoneProgram(zone_ad=self.config.zones[199],
                                                                       prog_ad=self.config.programs[99],
                                                                       _rt=self.config.zone_programs[200].rt,
                                                                       _ct=self.config.zone_programs[200].ct,
                                                                       _so=self.config.zone_programs[200].so,
                                                                       _pz=200,
                                                                       _ra=150)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_6(self):
        """
        set_Water Source _3200
        set up WS 1 \n
            enable WS 1 \n
            set WS priority to 2-medium \n
            set water budget to 100000 and enable the water budget shut down \n
            enable water rationing \n
            assign WS 1 to POC 1 \n

        \n
            set_Water Source _3200
            set up WS 2 \n
            set WS priority to 3-medium \n
            set water budget to 1000 and disable the water budget shut down \n
            disable water rationing \n
            assign WS 8 to POC 8 \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].water_sources[1] = WaterSource(
                _ad=1,
                _en=opcodes.true,
                _pr=2,
                _wb=100000,
                _ws=opcodes.true,
                _wr=opcodes.true,
                _pc=1
            )
            # add empty condition to the water source
            self.config.water_sources[1].add_empty_condition_to_ws(_ad=1, _sw=1, _se=opcodes.closed)
            self.config.water_sources[1].empty_conditions[1].set_switch_empty_condition_limit_on_ws(_state=opcodes.open)
            self.config.BaseStation3200[1].water_source[1].share(fs_id=1)
            #BaseStation3200 is an atribute of the flowstation
            self.config.BaseStation3200[1].water_sources[8] = WaterSource(
                _ad=8,
                _en=opcodes.true,
                _pr=3,
                _wb=1000,
                _ws=opcodes.false,
                _wr=opcodes.false,
                _pc=8
            )
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_7(self):
        """
        set_Point of Control on 3200
        set up PC 1 \n
            enable POC 1 \n
            assign master valve TMV0003 and flow meter TWF0003 to POC 1 \n
            assign POC 1 a target flow of 500 \n
            assign POC 1 to main line 1 \n
            set POC priority to 2-medium \n
            set high flow limit to 550 and enable high flow shut down \n
            set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            set water budget to 100000 and enable the water budget shut down \n
            enable water rationing \n
        \n
        set up PC 8 \n
            enable POC 8 \n
            assign master valve TMV0004 and flow meter TWF0004 to POC 8 \n
            assign POC 8 a target flow of 50 \n
            assign POC 8 to main line 8 \n
            set POC priority to 3-low \n
            set high flow limit to 75 and disable high flow shut down \n
            set unscheduled flow limit to 5 and disable unscheduled flow shut down \n
            set water budget to 1000 and disable water budget shut down \n
            disable water rationing \n
            assign event switch TPD0001 to POC 8 \n
            set switch empty condition to closed \n
            set empty wait time to 540 minutes \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].poc[1] = PointOfControl(
                _ad=1,
                _en=opcodes.true,
                _mv=1,
                _fm=2,
                _fl=500,
                _hf=550,
                _hs=opcodes.true,
                _uf=10,
                _us=opcodes.true,
                _ml=1

            )
            self.config.BaseStation3200[1].poc[8] = PointOfControl(
                _ad=8,
                _en=opcodes.true,
                _mv=8,
                _fm=4,
                _fl=50,
                _hf=75,
                _hs=opcodes.false,
                _uf=5,
                _us=opcodes.false,
                _ml=8
            )
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_8(self):
        """
        set_mainlines_3200
        set up main line 1 \n
            set limit zones by flow to true \n
            set the pipe fill time to 4 minutes \n
            set the target flow to 500 \n
            set the high variance limit to 5% and enable the high variance shut down \n
            set the low variance limit to 20% and enable the low variance shut down \n
        \n
        set up main line 8 \n
            set limit zones by flow to true \n
            set the pipe fill time to 1 minute \n
            set the target flow to 50 \n
            set the high variance limit to 20% and disable the high variance shut down \n
            set the low variance limit to 5% and disable the low variance shut down \n
        """

        # here we can either execute the following uncommented lines in procedural fashion, or we could re-init the
        # object, would have to import Mainline at the top, effectively accomplishing the same thing by:
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].mainlines[1] = Mainline(_ad=1,
                                                _ft=4,
                                                _fl=500,
                                                _lc=opcodes.true,
                                                _hv=5,
                                                _hs=opcodes.true,
                                                _lv=20,
                                                _ls=opcodes.true)
            self.config.BaseStation3200[1].mainlines[8] = Mainline(_ad=8,
                                                _ft=1,
                                                _fl=50,
                                                _lc=opcodes.true,
                                                _hv=20,
                                                _hs=opcodes.false,
                                                _lv=5,
                                                _ls=opcodes.false)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_9(self):
        """
        Assign zones to Mainlines
            Zone 1 ---> Mainline 1
            Zone 2 ---> Mainline 1
            Zone 49 ---> Mainline 2
            Zone 50 ---> Mainline 2
            Zone 197 ---> Mainline 8
            Zone 198 ---> Mainline 8
            Zone 199 ---> Mainline 8
            Zone 200 ---> Mainline 8

        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Mainline 1
            self.config.BaseStation3200[1].zones[1].set_mainline_assignment(_mainline_number=1)
            self.config.BaseStation3200[1].zones[2].set_mainline_assignment(_mainline_number=1)
            # Mainline 2
            self.config.BaseStation3200[1].zones[49].set_mainline_assignment(_mainline_number=2)
            self.config.BaseStation3200[1].zones[50].set_mainline_assignment(_mainline_number=2)
            # Mainline 8
            self.config.BaseStation3200[1].zones[197].set_mainline_assignment(_mainline_number=8)
            self.config.BaseStation3200[1].zones[198].set_mainline_assignment(_mainline_number=8)
            self.config.BaseStation3200[1].zones[199].set_mainline_assignment(_mainline_number=8)
            self.config.BaseStation3200[1].zones[200].set_mainline_assignment(_mainline_number=8)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        verify the entire configuration again \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.BaseStation3200[1].ts_send_403_packet()
            self.BaseStation3200[1].tq_verify_403_packet()
            self.BaseStation3200[1].te_verify_403_packet()
            self.BaseStation3200[1].te_verify_403_packet_against_full_configuration()
            self.config.BaseStation3200[1].verify_full_configuration()

            self.config.BaseStation3200[1].verify_full_configuration()
            self.config.BaseStation3200[2].verify_full_configuration()
            self.config.flowstation[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for controler in range(1,3):
                self.config.BaseStation3200[controler].do_increment_clock(minutes=1)
                # time.sleep(20)

                self.config.BaseStation3200[controler].do_reboot_controller()
                self.config.BaseStation3200[controler].stop_clock()
            self.config.flowstation[1].do_increment_clock(minutes=1)
            # time.sleep(20)

            self.config.flowstation[1].do_reboot_controller()
            self.config.flowstation[1].stop_clock()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        after the reboot all of the devices get set to default values in the controller so you have to do a self test
        and than do an update to get each object to match what the controller has
        perform a test on all zones to verify that they are functioning properly \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for zone in self.config.BaseStation3200[1].zn_ad_range:
                self.config.BaseStation3200[1].zones[zone].self_test_and_update_object_attributes()
            for moisture_sensors in self.config.BaseStation3200[1].ms_ad_range:
                self.config.BaseStation3200[1].moisture_sensors[moisture_sensors].self_test_and_update_object_attributes()
            for master_valves in self.config.BaseStation3200[1].mv_ad_range:
                self.config.BaseStation3200[1].master_valves[master_valves].self_test_and_update_object_attributes()
            for pumps in self.config.BaseStation3200[1].pm_ad_range:
                self.config.BaseStation3200[1].pumps[pumps].self_test_and_update_object_attributes()
            for flow_meters in self.config.BaseStation3200[1].fm_ad_range:
                self.config.BaseStation3200[1].flow_meters[flow_meters].self_test_and_update_object_attributes()
            for event_switches in self.config.BaseStation3200[1].sw_ad_range:
                self.config.BaseStation3200[1].event_switches[event_switches].self_test_and_update_object_attributes()
            for temperature_sensors in self.config.BaseStation3200[1].ts_ad_range:
                self.config.BaseStation3200[1].temperature_sensors[temperature_sensors].self_test_and_update_object_attributes()
            for pressure_sensors in self.config.BaseStation3200[1].ps_ad_range:
                self.config.BaseStation3200[1].analog_bicoder[pressure_sensors].self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        verify the entire configuration again \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
