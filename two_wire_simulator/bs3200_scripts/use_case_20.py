import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

__author__ = 'Ben'


class ControllerUseCase20(object):
    """
    Test name:
        - CN UseCase20 Advanced flow variance with and without Shutdowns

    User Story: \n
        1)  As a user I would like my zone to shutdown because a sprinkler head was kicked off so that I don't waste
            precious water, flood my lovely neighbors and have a dead lawn with butterflies.

        2)  As a user I would like my mainline to shutdown because a pipe cracked so that I don't waste precious water,
            flood my lovely neighbors and have a dead lawn with butterflies.

        3)  As a user after I fixed my irrigation issue I would like to be able to resume watering.

    Coverage and Objectives: \n
        - Configure a v16 3200 with advanced high/low flow variance tiers spread across multiple Mainlines
        - Also test running more than 15 zones at one time
        - Covers timed flow stabilization with values above and below default for Mainlines
        - Covers multiple advanced flow variance tiers spread across different Mainlines
        - Covers advanced variance limits with shutdowns and without shutdowns
        - Covers no action with advanced variance limits set to 0% with and without shutdowns

    Scenarios: \n
        - Verifies high flow variance with/without shutdown messages
        - Verifies low flow variance with/without shutdown messages
        - Verifies no variance message when advanced variance is enabled and threshold is 0%

    Not Covered: \n
        - Multiple pocs attached to single mainline

    Test Overview:
        - Advanced Variance Tier 1:
            - ML 1: High flow variance with shutdown (1 min pipe fill)
            - ML 2: Low flow variance without shutdown (3 min pipe fill)
            
        - Advanced Variance Tier 2: (these two MLs test functionality of having a 0% value set for variance threshold)
            - ML 3: High flow variance with shutdown (1 min pipe fill)
            - ML 4: Low flow variance without shutdown (1 min pipe fill)
          
        - Advanced Variance Tier 3:
            - ML 5: High flow variance with shutdown (1 min pipe fill)
            - ML 6: Low flow variance without shutdown (1 min pipe fill)
            
        - Advanced Variance Tier 4:
            - ML 7: High flow variance with shutdown (1 min pipe fill)
            - ML 8: Low flow variance with shutdown (1 min pipe fill)
    
    Test Configuration setup: \n
        1. test Advance flow Tier 1 with and with out shut down\n
            - Tier 1 range 0 - 25 GPM \n
            - Configuration
                - WS 1 ---> POC 1 ---> ML 1 \n
                    - POC 1
                        - FM 1
                        - MV 1
                        - PM 1
                    - ML 1  Advanced high flow Tier 1 With Shutdown\n
                        - ZN 1 design flow 1.0
                        - ZN 2 design flow 1.0
                        - ZN 3 design flow 0.8
                        - Variance set to 80%
                        - Cause HF Variance Shutdown on ZN 1
                    - PG 1
                - WS 2 ---> POC 2 ---> ML 2 \n
                    - POC 2
                        - FM 2
                        - MV 2
                        - PM 2
                    - ML 2  Advanced low flow Tier 1 Without Shutdown\n
                        - ZN 4 design flow 24.0
                        - ZN 5 design flow 0.0
                        - ZN 6 design flow 0.8
                        - variance set to 10%
                        - Cause LF Variance Detection
                     - PG 2
        2. test Advanced flow Tier 2 with and with out shut down
            - Tier 2 range 25 - 100 GPM \n
            - Configuration
                - WS 3 ---> POC 3 ---> ML 3 \n
                    - POC 3
                        - FM 3
                        - MV 3
                        - PM 3
                    - ML 3  Advanced flow Tier 3 With Shutdown\n
                        - ZN 7 design flow 50.0
                        - ZN 8 design flow 25.0
                        - ZN 9 design flow 50.0
                        - variance set to 0%
                        - Verify variance is ignored and watering continues as expected
                    - PG 3
                - WS 4 ---> POC 4 ---> ML 4 \n
                    - POC 4
                        - FM 4
                        - MV 4
                        - PM 4
                    - ML 4  Advanced flow Tier 4 Without Shutdown\n
                        - ZN 10 design flow 50.0
                        - ZN 11 design flow 25.0
                        - ZN 12 design flow 50.0
                        - variance set to 0%
                        - Verify variance is ignored and watering continues as expected
                    - PG 4
        3. test Advance flow Tier 3 with and with out shut down
            - Tier 3 range 100 - 300 GPM \n
            - Configuration
                - WS 5 ---> POC 5 ---> ML 5 \n
                    - POC 5
                        - FM 5
                        - MV 5
                        - PM 5
                    - ML 5  Advanced flow Tier 3 With Shutdown\n
                        - ZN 13 design flow 100
                        - ZN 14 design flow 150
                        - ZN 15 design flow 299
                        - variance set to 60%
                        - Verify variance shutdown zone 13
                    - PG 5
                - WS 6 ---> POC 6 ---> ML 6 \n
                    - POC 6
                        - FM 6
                        - MV 6
                        - PM 6
                    - ML 6  Advanced flow Tier 4 Without Shutdown\n
                        - ZN 16 design flow 100
                        - ZN 17 design flow 150
                        - ZN 18 design flow 299
                        - variance set to 40%
                        - Verify variance is detected on mainline
                    - PG 6
        4. test Advance flow Tier 4 with and without shutdown
            - Tier 4 range 300+ GPM \n
            - Configuration
                - WS 7 ---> POC 7 ---> ML 7 \n
                    - POC 7
                        - FM 7
                        - MV 7
                        - PM 7
                    -ML 7  Advanced high flow Tier 4 With Shutdown\n
                        - ZN 19 design flow 350
                        - ZN 20 design flow 350
                        - ZN 21 design flow 301
                        - variance set to 10%
                        - Verify variance shutdown zone 19
                    - PG 7
                - WS 8 ---> POC 8 ---> ML 8 \n
                    - POC 8
                        - FM 8
                        - MV 8
                        - PM 8
                    -ML 8  Advanced low flow Tier 4 With Shutdown\n
                        - ZN 22 design flow 350
                        - ZN 23 design flow 350
                        - ZN 24 design flow 301
                        - variance set to 80%
                        - Verify variance is detected on mainline
                    - PG 8

    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        # these are global variables for the test
        # zone design flow values

        # ML 1
        # HF Variance Tier 1 (<25gpm)
        self.zn_1_df = 1.0
        self.zn_2_df = 1.0
        self.zn_3_df = 0.8

        # ML 2
        # LF Variance Tier 1 (<25gpm)
        self.zn_4_df = 24.0
        self.zn_5_df = 0.0
        self.zn_6_df = 0.8

        # ML 3
        # HF Variance Tier 2 (25gpm - 100gpm)
        self.zn_7_df = 50.0
        self.zn_8_df = 25.0
        self.zn_9_df = 50.0

        # ML 4
        # LF Variance Tier 2 (25gpm - 100gpm)
        self.zn_10_df = 50.0
        self.zn_11_df = 25.0
        self.zn_12_df = 50.0

        # ML 5
        # HF Variance Tier 3 (100gpm - 300gpm)
        self.zn_13_df = 150.0
        self.zn_14_df = 150.0
        self.zn_15_df = 100.0

        # ML 6
        # LF Variance Tier 3 (100gpm - 300gpm)
        self.zn_16_df = 100.0
        self.zn_17_df = 150.0
        self.zn_18_df = 299.0

        # ML 7
        # HF Variance Tier 4 (>300gpm)
        self.zn_19_df = 350.0
        self.zn_20_df = 350.0
        self.zn_21_df = 301.0

        # ML 8
        # LF Variance Tier 4 (>300gpm)
        self.zn_22_df = 350.0
        self.zn_23_df = 350.0
        self.zn_24_df = 301.0

        # mainline flow variance percentages

        # Tier 1
        self.ml_1_hi_fl_vr = 80
        self.ml_1_lo_fl_vr = 0  # only testing HF variance on ML 1

        self.ml_2_hi_fl_vr = 0  # only testing LF variance on ML 2
        self.ml_2_lo_fl_vr = 10

        # Tier 2
        self.ml_3_hi_fl_vr = 0  # testing variance = 0 = watering should complete as expected with no messages
        self.ml_3_lo_fl_vr = 0  # testing variance = 0 = watering should complete as expected with no messages

        self.ml_4_hi_fl_vr = 0  # testing variance = 0 = watering should complete as expected with no messages
        self.ml_4_lo_fl_vr = 0  # testing variance = 0 = watering should complete as expected with no messages

        # Tier 3
        self.ml_5_hi_fl_vr = 60
        self.ml_5_lo_fl_vr = 0  # only testing HF variance on ML 5

        self.ml_6_hi_fl_vr = 0  # only testing LF variance on ML 6
        self.ml_6_lo_fl_vr = 40

        # Tier 4
        self.ml_7_hi_fl_vr = 10
        self.ml_7_lo_fl_vr = 0  # only testing HF variance on ML 7

        self.ml_8_hi_fl_vr = 0  # only testing LF variance on ML 8
        self.ml_8_lo_fl_vr = 80

        # Placeholders for variance trigger calculations.
        #   -> These values will represent the calculated actaul flow needed for FlowMeters in order to trigger the
        #      threshold boundaries.
        self.setting_for_ml_1_high_flow_variance_calculation = None
        self.setting_for_ml_2_low_flow_variance_calculation = None
        self.setting_for_ml_5_high_flow_variance_calculation = None
        self.setting_for_ml_6_low_flow_variance_calculation = None
        self.setting_for_ml_7_high_flow_variance_calculation = None
        self.setting_for_ml_8_low_flow_variance_calculation = None

        self.run_use_case()

    #################################
    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set controller max concurrency to allow for all programs and zones to water
            self.config.BaseStation3200[1].set_max_concurrent_zones(40)

            program_start_time_10am = [600]  # 10:00am start time
            program_water_windows = ['111111111111111111111111']

            # Add and configure Program 1 - 8
            for pg_address in range(1, 9):
                self.config.BaseStation3200[1].add_program_to_controller(_program_address=pg_address)
                self.config.BaseStation3200[1].programs[pg_address].set_enabled()
                self.config.BaseStation3200[1].programs[pg_address].set_water_window(_ww=program_water_windows)
                self.config.BaseStation3200[1].programs[pg_address].set_priority_level(_pr_level=1)
                self.config.BaseStation3200[1].programs[pg_address].set_max_concurrent_zones(_number_of_zones=2)
                self.config.BaseStation3200[1].programs[pg_address].set_seasonal_adjust(_percent=100)
                self.config.BaseStation3200[1].programs[pg_address].\
                    set_watering_intervals_to_selected_days_of_the_week(_sun=True,
                                                                        _mon=True,
                                                                        _tues=True,
                                                                        _wed=True,
                                                                        _thurs=True,
                                                                        _fri=True,
                                                                        _sat=True)
                self.config.BaseStation3200[1].programs[pg_address].set_start_times(_st_list=program_start_time_10am)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure Program Zone 1
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=10)
            # link zone 2 and 3 to primary zone 1
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1)
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_as_linked_zone(_primary_zone=1)

            # Add & Configure Program Zone 4
            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[2].zone_programs[4].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[2].zone_programs[4].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[2].zone_programs[4].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[2].zone_programs[4].set_soak_time(_minutes=10)
            # link zone 4 and 5 to primary zone 4
            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=5)
            self.config.BaseStation3200[1].programs[2].zone_programs[5].set_as_linked_zone(_primary_zone=4)
            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=6)
            self.config.BaseStation3200[1].programs[2].zone_programs[6].set_as_linked_zone(_primary_zone=4)

            # Add & Configure Program Zone 7
            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=7)
            self.config.BaseStation3200[1].programs[3].zone_programs[7].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[3].zone_programs[7].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[3].zone_programs[7].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[3].zone_programs[7].set_soak_time(_minutes=10)
            # link zone 8 - 9 to primary zone 7
            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=8)
            self.config.BaseStation3200[1].programs[3].zone_programs[8].set_as_linked_zone(_primary_zone=7)
            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=9)
            self.config.BaseStation3200[1].programs[3].zone_programs[9].set_as_linked_zone(_primary_zone=7)
            
            # Add & Configure Program Zone 10
            self.config.BaseStation3200[1].programs[4].add_zone_to_program(_zone_address=10)
            self.config.BaseStation3200[1].programs[4].zone_programs[10].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[4].zone_programs[10].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[4].zone_programs[10].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[4].zone_programs[10].set_soak_time(_minutes=10)
            # link zone 11-12  to primary zone 10
            self.config.BaseStation3200[1].programs[4].add_zone_to_program(_zone_address=11)
            self.config.BaseStation3200[1].programs[4].zone_programs[11].set_as_linked_zone(_primary_zone=10)
            self.config.BaseStation3200[1].programs[4].add_zone_to_program(_zone_address=12)
            self.config.BaseStation3200[1].programs[4].zone_programs[12].set_as_linked_zone(_primary_zone=10)
            
            # Add & Configure Program Zone 13
            self.config.BaseStation3200[1].programs[5].add_zone_to_program(_zone_address=13)
            self.config.BaseStation3200[1].programs[5].zone_programs[13].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[5].zone_programs[13].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[5].zone_programs[13].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[5].zone_programs[13].set_soak_time(_minutes=10)
            # link zone 14 - 15 to primary zone 13
            self.config.BaseStation3200[1].programs[5].add_zone_to_program(_zone_address=14)
            self.config.BaseStation3200[1].programs[5].zone_programs[14].set_as_linked_zone(_primary_zone=13)
            self.config.BaseStation3200[1].programs[5].add_zone_to_program(_zone_address=15)
            self.config.BaseStation3200[1].programs[5].zone_programs[15].set_as_linked_zone(_primary_zone=13)
            
            # Add & Configure Program Zone 16
            self.config.BaseStation3200[1].programs[6].add_zone_to_program(_zone_address=16)
            self.config.BaseStation3200[1].programs[6].zone_programs[16].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[6].zone_programs[16].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[6].zone_programs[16].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[6].zone_programs[16].set_soak_time(_minutes=10)
            # link zone 17 - 18  to primary zone 16
            self.config.BaseStation3200[1].programs[6].add_zone_to_program(_zone_address=17)
            self.config.BaseStation3200[1].programs[6].zone_programs[17].set_as_linked_zone(_primary_zone=16)
            self.config.BaseStation3200[1].programs[6].add_zone_to_program(_zone_address=18)
            self.config.BaseStation3200[1].programs[6].zone_programs[18].set_as_linked_zone(_primary_zone=16)
            
            # Add & Configure Program Zone 19
            self.config.BaseStation3200[1].programs[7].add_zone_to_program(_zone_address=19)
            self.config.BaseStation3200[1].programs[7].zone_programs[19].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[7].zone_programs[19].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[7].zone_programs[19].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[7].zone_programs[19].set_soak_time(_minutes=10)
            # link zone 20-21 to primary zone 19
            self.config.BaseStation3200[1].programs[7].add_zone_to_program(_zone_address=20)
            self.config.BaseStation3200[1].programs[7].zone_programs[20].set_as_linked_zone(_primary_zone=19)
            self.config.BaseStation3200[1].programs[7].add_zone_to_program(_zone_address=21)
            self.config.BaseStation3200[1].programs[7].zone_programs[21].set_as_linked_zone(_primary_zone=19)
            
            # Add & Configure Program Zone 22
            self.config.BaseStation3200[1].programs[8].add_zone_to_program(_zone_address=22)
            self.config.BaseStation3200[1].programs[8].zone_programs[22].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[8].zone_programs[22].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[8].zone_programs[22].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[8].zone_programs[22].set_soak_time(_minutes=10)
            # link zone 23 - 24  to primary zone 22
            self.config.BaseStation3200[1].programs[8].add_zone_to_program(_zone_address=23)
            self.config.BaseStation3200[1].programs[8].zone_programs[23].set_as_linked_zone(_primary_zone=22)
            self.config.BaseStation3200[1].programs[8].add_zone_to_program(_zone_address=24)
            self.config.BaseStation3200[1].programs[8].zone_programs[24].set_as_linked_zone(_primary_zone=22)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        set up WS 1 \n
            - enabled
            - target flow: 500 \n
            - priority: 2 \n
            - water budget: 100000
            - water budget shutdown: enabled \n
            - water rationing: enabled \n
            - empty conditions:
                - none
        set up WS 8 \n
            - enabled
            - target flow: 500 \n
            - priority: 2 \n
            - water budget: 100000
            - water budget shutdown: enabled \n
            - water rationing: enabled \n
            - empty conditions:
                - none
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Water Source 1 - 8
            for ws_address in range(1, 9):
                self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=ws_address)
                self.config.BaseStation3200[1].water_sources[ws_address].set_enabled()
                self.config.BaseStation3200[1].water_sources[ws_address].set_priority(_priority_for_water_source=2)
                self.config.BaseStation3200[1].water_sources[ws_address].set_monthly_watering_budget(_budget=0)
                self.config.BaseStation3200[1].water_sources[ws_address].set_water_rationing_to_disabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n

        - set up POC 1 \n
            - enable POC 1 \n
            - assign master valve TMV0001 and flow meter TWF0001 to POC 1 \n
            - assign POC 1 a target flow of 500 \n
            - assign POC 1 to main line 1 \n
            - set POC priority to 2-medium \n
            - set high flow limit to 550 and enable high flow shut down \n
            - set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            - set water budget to 0 and disable the water budget shut down \n
            - disable water rationing \n
        - set up POC 8 \n
            - enable POC 8 \n
            - assign master valve TMV0002 and flow meter TWF0002 to POC 1 \n
            - assign POC 8 a target flow of 500 \n
            - assign POC 8 to main line 1 \n
            - set POC priority to 2-medium \n
            - set high flow limit to 550 and enable high flow shut down \n
            - set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            - set water budget to 0 and disable the water budget shut down \n
            - disable water rationing \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure POC 1 - 8
            for pc_address in range(1, 9):
                self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=pc_address)
                self.config.BaseStation3200[1].points_of_control[pc_address].set_enabled()
                self.config.BaseStation3200[1].points_of_control[pc_address].add_master_valve_to_point_of_control(
                    _master_valve_address=pc_address)
                self.config.BaseStation3200[1].points_of_control[pc_address].add_pump_to_point_of_control(
                    _pump_address=pc_address)
                self.config.BaseStation3200[1].points_of_control[pc_address].add_flow_meter_to_point_of_control(
                    _flow_meter_address=pc_address)
                self.config.BaseStation3200[1].points_of_control[pc_address].set_target_flow(_gpm=900)
                # Add POC 1 to Water Source 1
                self.config.BaseStation3200[1].water_sources[pc_address].add_point_of_control_to_water_source(
                    _point_of_control_address=pc_address)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control

        - set up main line 1 \n
            - set limit zones by flow to true \n
            - set the pipe fill time to 4 minutes \n
            - set the target flow to 500 \n
            - set the high variance limit to 5% and enable the high variance shut down \n
            - set the low variance limit to 20% and enable the low variance shut down \n
        - set up main line 8 \n
            - set limit zones by flow to true \n
            - set the pipe fill time to 1 minutes \n
            - set the target flow to 500 \n
            - set the high variance limit to 5% and enable the high variance shut down \n
            - set the low variance limit to 20% and enable the low variance shut down \n
        """
        # here we can either execute the following uncommented lines in procedural fashion, or we could re-init the
        # object, would have to import Mainline at the top, effectively accomplishing the same thing by:
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            
            # ----------- #
            # MAINLINE 1  #
            # ----------- #
            
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=25)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[1].set_use_advanced_flow_to_true()
            self.config.BaseStation3200[1].mainlines[1].set_high_flow_variance_tier_one(_percent=self.ml_1_hi_fl_vr,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].mainlines[1].set_low_flow_variance_tier_one(_percent=self.ml_1_lo_fl_vr,
                                                                                       _with_shutdown_enabled=True)
            # Add ML 1 to POC 1
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)

            # ----------- #
            # MAINLINE 2  #
            # ----------- #
            
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=2)
            self.config.BaseStation3200[1].mainlines[2].set_pipe_stabilization_time(_minutes=3)
            self.config.BaseStation3200[1].mainlines[2].set_target_flow(_gpm=25)
            self.config.BaseStation3200[1].mainlines[2].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[2].set_use_advanced_flow_to_true()
            self.config.BaseStation3200[1].mainlines[2].set_high_flow_variance_tier_one(_percent=self.ml_2_hi_fl_vr,
                                                                                        _with_shutdown_enabled=False)
            self.config.BaseStation3200[1].mainlines[2].set_low_flow_variance_tier_one(_percent=self.ml_2_lo_fl_vr,
                                                                                       _with_shutdown_enabled=False)
            # Add ML 2 to POC 2
            self.config.BaseStation3200[1].points_of_control[2].add_mainline_to_point_of_control(_mainline_address=2)

            # ----------- #
            # MAINLINE 3  #
            # ----------- #
            
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=3)
            self.config.BaseStation3200[1].mainlines[3].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[3].set_target_flow(_gpm=100)
            self.config.BaseStation3200[1].mainlines[3].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[3].set_use_advanced_flow_to_true()
            self.config.BaseStation3200[1].mainlines[3].set_high_flow_variance_tier_two(_percent=self.ml_3_hi_fl_vr,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].mainlines[3].set_low_flow_variance_tier_two(_percent=self.ml_3_lo_fl_vr,
                                                                                       _with_shutdown_enabled=True)
            # Add ML 3 to POC 3
            self.config.BaseStation3200[1].points_of_control[3].add_mainline_to_point_of_control(_mainline_address=3)

            # ----------- #
            # MAINLINE 4  #
            # ----------- #
            
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=4)
            self.config.BaseStation3200[1].mainlines[4].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[4].set_target_flow(_gpm=100)
            self.config.BaseStation3200[1].mainlines[4].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[4].set_use_advanced_flow_to_true()
            self.config.BaseStation3200[1].mainlines[4].set_high_flow_variance_tier_two(_percent=self.ml_4_hi_fl_vr,
                                                                                        _with_shutdown_enabled=False)
            self.config.BaseStation3200[1].mainlines[4].set_low_flow_variance_tier_two(_percent=self.ml_4_lo_fl_vr,
                                                                                       _with_shutdown_enabled=False)
            # Add ML 4 to POC 4
            self.config.BaseStation3200[1].points_of_control[4].add_mainline_to_point_of_control(_mainline_address=4)

            # ----------- #
            # MAINLINE 5  #
            # ----------- #
            
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=5)
            self.config.BaseStation3200[1].mainlines[5].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[5].set_target_flow(_gpm=300)
            self.config.BaseStation3200[1].mainlines[5].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[5].set_use_advanced_flow_to_true()
            self.config.BaseStation3200[1].mainlines[5].set_high_flow_variance_tier_three(_percent=self.ml_5_hi_fl_vr,
                                                                                          _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].mainlines[5].set_low_flow_variance_tier_three(_percent=self.ml_5_lo_fl_vr,
                                                                                         _with_shutdown_enabled=True)
            # Add ML 5 to POC 5
            self.config.BaseStation3200[1].points_of_control[5].add_mainline_to_point_of_control(_mainline_address=5)

            # ----------- #
            # MAINLINE 6  #
            # ----------- #
            
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=6)
            self.config.BaseStation3200[1].mainlines[6].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[6].set_target_flow(_gpm=300)
            self.config.BaseStation3200[1].mainlines[6].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[6].set_use_advanced_flow_to_true()
            self.config.BaseStation3200[1].mainlines[6].set_high_flow_variance_tier_three(_percent=self.ml_6_hi_fl_vr,
                                                                                          _with_shutdown_enabled=False)
            self.config.BaseStation3200[1].mainlines[6].set_low_flow_variance_tier_three(_percent=self.ml_6_lo_fl_vr,
                                                                                         _with_shutdown_enabled=False)
            # Add ML 6 to POC 6
            self.config.BaseStation3200[1].points_of_control[6].add_mainline_to_point_of_control(_mainline_address=6)

            # ----------- #
            # MAINLINE 7  #
            # ----------- #
            
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=7)
            self.config.BaseStation3200[1].mainlines[7].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[7].set_target_flow(_gpm=900)
            self.config.BaseStation3200[1].mainlines[7].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[7].set_use_advanced_flow_to_true()
            self.config.BaseStation3200[1].mainlines[7].set_high_flow_variance_tier_four(_percent=self.ml_7_hi_fl_vr,
                                                                                         _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].mainlines[7].set_low_flow_variance_tier_four(_percent=self.ml_7_lo_fl_vr,
                                                                                        _with_shutdown_enabled=True)
            # Add ML 7 to POC 7
            self.config.BaseStation3200[1].points_of_control[7].add_mainline_to_point_of_control(_mainline_address=7)

            # ----------- #
            # MAINLINE 8  #
            # ----------- #
            
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=8)
            self.config.BaseStation3200[1].mainlines[8].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[8].set_target_flow(_gpm=900)
            self.config.BaseStation3200[1].mainlines[8].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[8].set_use_advanced_flow_to_true()
            self.config.BaseStation3200[1].mainlines[8].set_high_flow_variance_tier_four(_percent=self.ml_8_hi_fl_vr,
                                                                                         _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].mainlines[8].set_low_flow_variance_tier_four(_percent=self.ml_8_lo_fl_vr,
                                                                                        _with_shutdown_enabled=True)
            # Add ML 8 to POC 8
            self.config.BaseStation3200[1].points_of_control[8].add_mainline_to_point_of_control(_mainline_address=8)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=1)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=2)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=3)

            self.config.BaseStation3200[1].mainlines[2].add_zone_to_mainline(_zone_address=4)
            self.config.BaseStation3200[1].mainlines[2].add_zone_to_mainline(_zone_address=5)
            self.config.BaseStation3200[1].mainlines[2].add_zone_to_mainline(_zone_address=6)

            self.config.BaseStation3200[1].mainlines[3].add_zone_to_mainline(_zone_address=7)
            self.config.BaseStation3200[1].mainlines[3].add_zone_to_mainline(_zone_address=8)
            self.config.BaseStation3200[1].mainlines[3].add_zone_to_mainline(_zone_address=9)
            
            self.config.BaseStation3200[1].mainlines[4].add_zone_to_mainline(_zone_address=10)
            self.config.BaseStation3200[1].mainlines[4].add_zone_to_mainline(_zone_address=11)
            self.config.BaseStation3200[1].mainlines[4].add_zone_to_mainline(_zone_address=12)
            
            self.config.BaseStation3200[1].mainlines[5].add_zone_to_mainline(_zone_address=13)
            self.config.BaseStation3200[1].mainlines[5].add_zone_to_mainline(_zone_address=14)
            self.config.BaseStation3200[1].mainlines[5].add_zone_to_mainline(_zone_address=15)
            
            self.config.BaseStation3200[1].mainlines[6].add_zone_to_mainline(_zone_address=16)
            self.config.BaseStation3200[1].mainlines[6].add_zone_to_mainline(_zone_address=17)
            self.config.BaseStation3200[1].mainlines[6].add_zone_to_mainline(_zone_address=18)
            
            self.config.BaseStation3200[1].mainlines[7].add_zone_to_mainline(_zone_address=19)
            self.config.BaseStation3200[1].mainlines[7].add_zone_to_mainline(_zone_address=20)
            self.config.BaseStation3200[1].mainlines[7].add_zone_to_mainline(_zone_address=21)
            
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=22)
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=23)
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=24)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ######################
        setup zones on mainlines
        ######################
         - set up zone  Attributes associated with main lines \n
            - set design flow on zone
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Assign a design flow value to each zone so that they have a default setting
            self.config.BaseStation3200[1].zones[1].set_design_flow(_gallons_per_minute=self.zn_1_df)
            self.config.BaseStation3200[1].zones[2].set_design_flow(_gallons_per_minute=self.zn_2_df)
            self.config.BaseStation3200[1].zones[3].set_design_flow(_gallons_per_minute=self.zn_3_df)
            self.config.BaseStation3200[1].zones[4].set_design_flow(_gallons_per_minute=self.zn_4_df)
            self.config.BaseStation3200[1].zones[5].set_design_flow(_gallons_per_minute=self.zn_5_df)
            self.config.BaseStation3200[1].zones[6].set_design_flow(_gallons_per_minute=self.zn_6_df)
            self.config.BaseStation3200[1].zones[7].set_design_flow(_gallons_per_minute=self.zn_7_df)
            self.config.BaseStation3200[1].zones[8].set_design_flow(_gallons_per_minute=self.zn_8_df)
            self.config.BaseStation3200[1].zones[9].set_design_flow(_gallons_per_minute=self.zn_9_df)
            self.config.BaseStation3200[1].zones[10].set_design_flow(_gallons_per_minute=self.zn_10_df)
            self.config.BaseStation3200[1].zones[11].set_design_flow(_gallons_per_minute=self.zn_11_df)
            self.config.BaseStation3200[1].zones[12].set_design_flow(_gallons_per_minute=self.zn_12_df)
            self.config.BaseStation3200[1].zones[13].set_design_flow(_gallons_per_minute=self.zn_13_df)
            self.config.BaseStation3200[1].zones[14].set_design_flow(_gallons_per_minute=self.zn_14_df)
            self.config.BaseStation3200[1].zones[15].set_design_flow(_gallons_per_minute=self.zn_15_df)
            self.config.BaseStation3200[1].zones[16].set_design_flow(_gallons_per_minute=self.zn_16_df)
            self.config.BaseStation3200[1].zones[17].set_design_flow(_gallons_per_minute=self.zn_17_df)
            self.config.BaseStation3200[1].zones[18].set_design_flow(_gallons_per_minute=self.zn_18_df)
            self.config.BaseStation3200[1].zones[19].set_design_flow(_gallons_per_minute=self.zn_19_df)
            self.config.BaseStation3200[1].zones[20].set_design_flow(_gallons_per_minute=self.zn_20_df)
            self.config.BaseStation3200[1].zones[21].set_design_flow(_gallons_per_minute=self.zn_21_df)
            self.config.BaseStation3200[1].zones[22].set_design_flow(_gallons_per_minute=self.zn_22_df)
            self.config.BaseStation3200[1].zones[23].set_design_flow(_gallons_per_minute=self.zn_23_df)
            self.config.BaseStation3200[1].zones[24].set_design_flow(_gallons_per_minute=self.zn_24_df)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        ############################
        Configure FlowMeters
        ############################

        - Configure FlowMeters to trigger HF/LF variance with calculated actual flow values
        - FlowMeters 1,5,7 will have values to trigger HF variance
        - FlowMeters 2,6,8 will have values to trigger LF variance
        - FlowMeters 3,4 will have values equal to half of their Tier's max GPM (100/2 = 50GPM)
            -> 50 GPM would cause both a HF and LF variance if values >0 were entered.
            -> This verifies that having 0 for variance limit causes no messages/errors.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # --------------------------------------- #
            # Configure High Flow Variance Components #
            # --------------------------------------- #

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _variance_percent=self.ml_1_hi_fl_vr,
                _expected_gpm=self.zn_1_df + self.zn_2_df)
            
            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[5],
                _variance_percent=self.ml_5_hi_fl_vr,
                _expected_gpm=self.zn_13_df + self.zn_14_df)
            
            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[7],
                _variance_percent=self.ml_7_hi_fl_vr,
                _expected_gpm=self.zn_19_df + self.zn_20_df)

            # -------------------------------------- #
            # Configure Low Flow Variance Components #
            # -------------------------------------- #

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[2],
                _variance_percent=self.ml_2_lo_fl_vr,
                _expected_gpm=self.zn_4_df + self.zn_5_df)
            
            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[6],
                _variance_percent=self.ml_6_lo_fl_vr,
                _expected_gpm=self.zn_16_df + self.zn_17_df)
            
            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[8],
                _variance_percent=self.ml_8_lo_fl_vr,
                _expected_gpm=self.zn_22_df + self.zn_23_df)

            # ------------------------------------- #
            # Configure No Flow Variance Components #
            # ------------------------------------- #

            # Set FM 3,4 to constant 50GPM which is 1/2 of the variance flow range being used on ML3,4
            self.config.BaseStation3200[1].flow_meters[3].bicoder.set_flow_rate(_gallons_per_minute=50.0)
            self.config.BaseStation3200[1].flow_meters[3].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].flow_meters[4].bicoder.set_flow_rate(_gallons_per_minute=50.0)
            self.config.BaseStation3200[1].flow_meters[4].bicoder.self_test_and_update_object_attributes()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        ############################
        Increment clock to start programming
        ############################

        - Set date and time on controller and increment clock to start zones running
        - Increment clock past program's start time of 10am
            - All programs should start
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ---------------------------------------------------------------------- #
            # Set controllers date/time to 1 minute before program start time (10am) #
            # ---------------------------------------------------------------------- #

            date_mngr.set_current_date_to_match_computer()
            self.config.BaseStation3200[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                             _time='09:59:00')

            # ------------------------------------------------------------ #
            # Increment controller clock to 10am to trigger program starts #
            # ------------------------------------------------------------ #

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        ############################
        Increment clock from 10:00 - 10:01 AM
        ############################
        
        --
        ML 1,5,7:
        --
            * Complete 1 min flow stabilization period
            * Take FM reading to read in flow fault
            * Wait to act on flow fault reading until next minute processing
        
        -- 
        ML 6:
        --
            * Complete 1 min flow stabilization period
            * Take FM reading to read in flow fault
            * Wait to act on flow fault reading until next minute processing
            
        -- 
        ML 8:
        --
            * Complete 1 min flow stabilization period
            * Take FM reading to read in flow fault
            * Wait to act on flow fault reading until next minute processing
            
        --
        ML 2:
        --
            * Continue 1/3 min flow stabilization period
            
        --
        ML 3,4:
        --
            * Complete 1 min flow stabilization period
            * Take FM reading and ignore flow faults (variance set to 0%)
            
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            
            # ------------------------------------------------------ #
            #                                                        #
            # 10:00 - 10:01 AM                                       #
            #                                                        #
            # ------------------------------------------------------ #
            
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 1,3,4 HIGH FLOW VARIANCE WITH SHUTDOWN            #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # For each flow component with addresses 1, 5 and 7 verify:
            for address in [1, 5, 7]:
                
                # Programs
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_running()
                
                # Flow Sources
                self.config.BaseStation3200[1].water_sources[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].points_of_control[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].mainlines[address].statuses.verify_status_is_running()
                
                # Flow Devices
                self.config.BaseStation3200[1].master_valves[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].pumps[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_running()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            
            # Mainline 5 Zones
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_waiting_to_water()
            
            # Mainline 7 Zones
            self.config.BaseStation3200[1].zones[19].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[20].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[21].statuses.verify_status_is_waiting_to_water()
            
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 1 LOW FLOW VARIANCE WITHOUT SHUTDOWN              #
            #                                                        #
            #   + Mainlines have 3 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # Programs
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()
            
            # Flow Sources
            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[2].statuses.verify_status_is_running()
            
            # Flow Devices
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_waiting_to_water()

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 3 LOW FLOW VARIANCE WITHOUT SHUTDOWN              #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #
                
            # Programs
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()
            
            # Flow Sources
            self.config.BaseStation3200[1].water_sources[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[6].statuses.verify_status_is_running()
            
            # Flow Devices
            self.config.BaseStation3200[1].master_valves[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()

            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_waiting_to_water()
            
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 4 LOW FLOW VARIANCE WITH SHUTDOWN                 #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # Programs
            self.config.BaseStation3200[1].programs[8].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].water_sources[8].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[8].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[8].statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].master_valves[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[8].statuses.verify_status_is_running()

            # Mainline 8 Zones
            self.config.BaseStation3200[1].zones[22].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[23].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[24].statuses.verify_status_is_waiting_to_water()

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 2 HIGH/LOW FLOW VARIANCE WITH/WITHOUT SHUTDOWN    #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #   + ML 3 = High Flow Variance With Shutdown            #
            #   + ML 4 = Low Flow Variance Without Shutdown          #
            #                                                        #
            # ------------------------------------------------------ #

            # For each flow component with addresses 3 and 4 verify:
            for address in [3, 4]:
    
                # Programs
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_running()
    
                # Flow Sources
                self.config.BaseStation3200[1].water_sources[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].points_of_control[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].mainlines[address].statuses.verify_status_is_running()
    
                # Flow Devices
                self.config.BaseStation3200[1].master_valves[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].pumps[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_running()

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_waiting_to_water()

            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_waiting_to_water()
            
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        ############################
        Increment clock from 10:01 - 10:02 AM
        ############################
        
        --
        ML 1,5,7:
        --
            * Controller acts on high flow variance read from 10:00 to 10:01 min processing
            * Strikes given to running zones
            * Strikes given to mainlines
            * Next group of zones turned on
        
        --
        ML 6:
        --
            * Controller acts on low flow variance read from 10:00 to 10:01 min processing
            * Verify/clear low flow variance message posted
            
        --
        ML 8:
        --
            * Controller acts on high flow variance read from 10:00 to 10:01 min processing
            * Strikes given to running zones
            * Strikes given to mainlines
            * Next group of zones turned on
            
        --
        ML 2:
        --
            * Continue 2/3 min flow stabilization period
            
        --
        ML 3,4:
        --
            * Watering should continue, even with expected flow != actual flow, variance is set to 0%, thus no
              action should be taken by controller. No messages should be posted
            * Verify ML 4 has no message posted for low variance detected (ML 5 has shutdown enabled and wouldn't have
              a msg yet)
            
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
    
            # ------------------------------------------------------ #
            #                                                        #
            # 10:01 - 10:02 AM                                       #
            #                                                        #
            # ------------------------------------------------------ #
    
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
    
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 1,3,4 HIGH FLOW VARIANCE WITH SHUTDOWN            #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #
    
            # For each flow component with addresses 1, 5 and 7 verify:
            for address in [1, 5, 7]:
        
                # Programs
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_running()
        
                # Flow Sources
                self.config.BaseStation3200[1].water_sources[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].points_of_control[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].mainlines[address].statuses.verify_status_is_running()
        
                # Flow Devices
                self.config.BaseStation3200[1].master_valves[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].pumps[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_running()
    
            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
    
            # Mainline 5 Zones
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_watering()
    
            # Mainline 7 Zones
            self.config.BaseStation3200[1].zones[19].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[20].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[21].statuses.verify_status_is_watering()

            # ---
            # NOTE:
            #
            # At this point, a High Flow Variance has been detected on ML's 1, 5 and 7 but not acted on until next
            # minute processing..
            #
            #   -> This means that we need to set FM 1, 5 and 7's rate to be equal to the next group of zones that will
            #      turn on so that the correct flow rate is read and used for flow stabilization determination.
            # ---

            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _expected_gpm=self.zn_1_df + self.zn_3_df)

            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[5],
                _expected_gpm=self.zn_13_df + self.zn_15_df)

            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[7],
                _expected_gpm=self.zn_19_df + self.zn_21_df)
    
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 1 LOW FLOW VARIANCE WITHOUT SHUTDOWN              #
            #                                                        #
            #   + Mainlines have 3 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #
    
            # Programs
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()
    
            # Flow Sources
            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[2].statuses.verify_status_is_running()
    
            # Flow Devices
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()
    
            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_waiting_to_water()
    
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 3 LOW FLOW VARIANCE WITHOUT SHUTDOWN              #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #
        
            # Programs
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()
    
            # Flow Sources
            self.config.BaseStation3200[1].water_sources[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[6].statuses.verify_status_is_running()
    
            # Flow Devices
            self.config.BaseStation3200[1].master_valves[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()
    
            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_waiting_to_water()
            
            # ---
            # NOTE:
            #
            # At this point, a Low Flow Variance detected message for ML 6 should be posted because they had
            # shutdown disabled.
            #
            #   -> Thus, verify and clear their messages
            # ---
            
            # ML 6
            self.config.BaseStation3200[1].mainlines[6].messages.verify_low_flow_variance_detected_message()
            self.config.BaseStation3200[1].mainlines[6].messages.clear_low_flow_variance_detected_message()

            # ---
            # NOTE:
            #
            # At this point, a Low Flow Variance has been detected on ML 6..
            #
            #   -> In order to not trigger messages on each minute, set FM 6's rate equal to their expected
            #      rate.
            #
            #       This will allow us to:
            #           - verify additional low flow variance messages aren't posted when the variance isn't reached
            # ---

            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[6],
                _expected_gpm=self.zn_16_df + self.zn_17_df)

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 4 LOW FLOW VARIANCE WITH SHUTDOWN                 #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # Programs
            self.config.BaseStation3200[1].programs[8].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].water_sources[8].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[8].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[8].statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].master_valves[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[8].statuses.verify_status_is_running()

            # Mainline 8 Zones
            self.config.BaseStation3200[1].zones[22].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[23].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[24].statuses.verify_status_is_watering()

            # ---
            # NOTE:
            #
            # At this point, a Low Flow Variance has been detected on ML 8 but not acted on until next minute 
            # processing..
            #
            #   -> This means that we need to set FM 8's rate to be equal to the next group of zones that will
            #      turn on so that the correct flow rate is read and used for flow stabilization determination.
            # ---

            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[8],
                _expected_gpm=self.zn_22_df + self.zn_24_df)
    
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 2 HIGH/LOW FLOW VARIANCE WITH/WITHOUT SHUTDOWN    #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #   + ML 3 = High Flow Variance With Shutdown            #
            #   + ML 4 = Low Flow Variance Without Shutdown          #
            #                                                        #
            # ------------------------------------------------------ #
    
            # For each flow component with addresses 3 and 4 verify:
            for address in [3, 4]:
        
                # Programs
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_running()
        
                # Flow Sources
                self.config.BaseStation3200[1].water_sources[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].points_of_control[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].mainlines[address].statuses.verify_status_is_running()
        
                # Flow Devices
                self.config.BaseStation3200[1].master_valves[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].pumps[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_running()
    
            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_waiting_to_water()
    
            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_waiting_to_water()

            # ---
            # NOTE:
            #
            # At this point, a Low Flow Variance would be detected on ML 4 if variance threshold was not 0%. The spec
            # says no variance event should be acted on in this case.
            #
            #   -> Thus, verify no message posted
            # ---
            
            self.config.BaseStation3200[1].mainlines[4].messages.check_for_low_flow_variance_detected_message_not_present()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        ############################
        Increment clock from 10:02 - 10:03 AM
        ############################
        
        --
        ML 1,5,7:
        --
            * Flow stabilization is achieved for new group of running zones
            * No variance detected
        
        --
        ML 6:
        --
            * Watering continues as expected
            
        --
        ML 8:
        --
            * Flow stabilization is achieved for new group of running zones
            * No variance detected
            
        --
        ML 2:
        --
            * Finish 3/3 min flow stabilization period
            * FM 2's rate is read and a low flow variance is detected but not acted on until next min processing
            * Verify no low variance detected message is posted yet, should be posted next min
            
        --
        ML 3,4:
        --
            * Watering should continue, even with expected flow != actual flow, variance is set to 0%, thus no
              action should be taken by controller. No messages should be posted
            * Verify ML 4 has no message posted for low variance detected (ML 5 has shutdown enabled and wouldn't have
              a msg yet)
            
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
        
            # ------------------------------------------------------ #
            #                                                        #
            # 10:02 - 10:03 AM                                       #
            #                                                        #
            # ------------------------------------------------------ #
        
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
        
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 1,3,4 HIGH FLOW VARIANCE WITH SHUTDOWN            #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #
        
            # For each flow component with addresses 1, 5 and 7 verify:
            for address in [1, 5, 7]:
            
                # Programs
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_running()
            
                # Flow Sources
                self.config.BaseStation3200[1].water_sources[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].points_of_control[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].mainlines[address].statuses.verify_status_is_running()
            
                # Flow Devices
                self.config.BaseStation3200[1].master_valves[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].pumps[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_running()
        
            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
        
            # Mainline 5 Zones
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_watering()
        
            # Mainline 7 Zones
            self.config.BaseStation3200[1].zones[19].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[20].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[21].statuses.verify_status_is_watering()
        
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 1 LOW FLOW VARIANCE WITHOUT SHUTDOWN              #
            #                                                        #
            #   + Mainlines have 3 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #
        
            # Programs
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()
        
            # Flow Sources
            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[2].statuses.verify_status_is_running()
        
            # Flow Devices
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()
        
            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_waiting_to_water()
            
            # ---
            # NOTE:
            #
            # At this point, a Low Flow Variance was detected on ML 2, but won't be acted on until next minute.
            #
            #   -> Thus, verify no message posted yet
            # ---
            
            self.config.BaseStation3200[1].mainlines[2].messages.check_for_low_flow_variance_detected_message_not_present()
        
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 3 LOW FLOW VARIANCE WITHOUT SHUTDOWN              #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #
            
            # Programs
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()
        
            # Flow Sources
            self.config.BaseStation3200[1].water_sources[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[6].statuses.verify_status_is_running()
        
            # Flow Devices
            self.config.BaseStation3200[1].master_valves[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()
        
            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_waiting_to_water()
            
            # ---
            # NOTE:
            #
            # At this point, ML's 6 and 8 had their flow rates set to a rate that would no trigger additional
            # variance messages.
            #
            #   -> Thus, verify no message posted after updating the flow rates
            # ---

            self.config.BaseStation3200[1].mainlines[6].messages.check_for_low_flow_variance_detected_message_not_present()
            
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 4 LOW FLOW VARIANCE WITH SHUTDOWN                 #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # Programs
            self.config.BaseStation3200[1].programs[8].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].water_sources[8].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[8].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[8].statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].master_valves[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[8].statuses.verify_status_is_running()

            # Mainline 8 Zones
            self.config.BaseStation3200[1].zones[22].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[23].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[24].statuses.verify_status_is_watering()
        
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 2 HIGH/LOW FLOW VARIANCE WITH/WITHOUT SHUTDOWN    #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #   + ML 3 = High Flow Variance With Shutdown            #
            #   + ML 4 = Low Flow Variance Without Shutdown          #
            #                                                        #
            # ------------------------------------------------------ #
        
            # For each flow component with addresses 3 and 4 verify:
            for address in [3, 4]:
            
                # Programs
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_running()
            
                # Flow Sources
                self.config.BaseStation3200[1].water_sources[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].points_of_control[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].mainlines[address].statuses.verify_status_is_running()
            
                # Flow Devices
                self.config.BaseStation3200[1].master_valves[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].pumps[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_running()
        
            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_waiting_to_water()
        
            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_waiting_to_water()
        
            # ---
            # NOTE:
            #
            # At this point, a Low Flow Variance would be detected on ML 4 if variance threshold was not 0%. The spec
            # says no variance event should be acted on in this case.
            #
            #   -> Thus, verify no message posted
            # ---
            
            self.config.BaseStation3200[1].mainlines[4].messages.check_for_low_flow_variance_detected_message_not_present()
    
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_14(self):
        """
        ############################
        Increment clock from 10:03 - 10:04 AM
        ############################
        
        --
        ML 1,5,7:
        --
            * Watering continues
            * No variance detected
        
        --
        ML 6:
        --
            * Watering continues as expected
            
        --
        ML 8:
        --
            * Watering continues
            * No variance detected
            
        --
        ML 2:
        --
            * Controller should act on flow fault reading from previous min processing
            * Verify/clear low variance detected message is posted
            
        --
        ML 3,4:
        --
            * Watering should continue, even with expected flow != actual flow, variance is set to 0%, thus no
              action should be taken by controller. No messages should be posted
            * Verify ML 4 has no message posted for low variance detected (ML 5 has shutdown enabled and wouldn't have
              a msg yet)
            
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
        
            # ------------------------------------------------------ #
            #                                                        #
            # 10:03 - 10:04 AM                                       #
            #                                                        #
            # ------------------------------------------------------ #
        
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
        
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 1,3,4 HIGH FLOW VARIANCE WITH SHUTDOWN            #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #
        
            # For each flow component with addresses 1, 5 and 7 verify:
            for address in [1, 5, 7]:
            
                # Programs
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_running()
            
                # Flow Sources
                self.config.BaseStation3200[1].water_sources[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].points_of_control[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].mainlines[address].statuses.verify_status_is_running()
            
                # Flow Devices
                self.config.BaseStation3200[1].master_valves[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].pumps[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_running()
        
            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
        
            # Mainline 5 Zones
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_watering()
        
            # Mainline 7 Zones
            self.config.BaseStation3200[1].zones[19].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[20].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[21].statuses.verify_status_is_watering()
        
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 1 LOW FLOW VARIANCE WITHOUT SHUTDOWN              #
            #                                                        #
            #   + Mainlines have 3 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #
        
            # Programs
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()
        
            # Flow Sources
            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[2].statuses.verify_status_is_running()
        
            # Flow Devices
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()
        
            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_waiting_to_water()
        
            # ---
            # NOTE:
            #
            # At this point, a Low Flow Variance was detected on ML 2, but won't be acted on until next minute.
            #
            #   -> Thus, verify no message posted yet
            # ---
        
            self.config.BaseStation3200[1].mainlines[2].messages.verify_low_flow_variance_detected_message()
            self.config.BaseStation3200[1].mainlines[2].messages.clear_low_flow_variance_detected_message()
            
            # ---
            # NOTE:
            #
            # At this point, a Low Flow Variance has been detected on ML 2
            #
            #   -> In order to not trigger messages on each minute, set FM 2's rate equal to their expected
            #      rate.
            #
            #       This will allow us to:
            #           - verify additional low flow variance messages aren't posted when the variance isn't reached
            # ---

            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[2],
                _expected_gpm=self.zn_4_df + self.zn_5_df)
        
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 3 LOW FLOW VARIANCE WITHOUT SHUTDOWN              #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #
        
            # Programs
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()
        
            # Flow Sources
            self.config.BaseStation3200[1].water_sources[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[6].statuses.verify_status_is_running()
        
            # Flow Devices
            self.config.BaseStation3200[1].master_valves[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()
        
            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_waiting_to_water()
            
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 4 LOW FLOW VARIANCE WITH SHUTDOWN                 #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # Programs
            self.config.BaseStation3200[1].programs[8].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].water_sources[8].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[8].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[8].statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].master_valves[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[8].statuses.verify_status_is_running()

            # Mainline 8 Zones
            self.config.BaseStation3200[1].zones[22].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[23].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[24].statuses.verify_status_is_watering()
        
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 2 HIGH/LOW FLOW VARIANCE WITH/WITHOUT SHUTDOWN    #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #   + ML 3 = High Flow Variance With Shutdown            #
            #   + ML 4 = Low Flow Variance Without Shutdown          #
            #                                                        #
            # ------------------------------------------------------ #
        
            # For each flow component with addresses 3 and 4 verify:
            for address in [3, 4]:
            
                # Programs
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_running()
            
                # Flow Sources
                self.config.BaseStation3200[1].water_sources[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].points_of_control[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].mainlines[address].statuses.verify_status_is_running()
            
                # Flow Devices
                self.config.BaseStation3200[1].master_valves[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].pumps[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_running()
        
            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_waiting_to_water()
        
            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_waiting_to_water()
        
            # ---
            # NOTE:
            #
            # At this point, a Low Flow Variance would be detected on ML 4 if variance threshold was not 0%. The spec
            # says no variance event should be acted on in this case.
            #
            #   -> Thus, verify no message posted
            # ---
            
            self.config.BaseStation3200[1].mainlines[4].messages.check_for_low_flow_variance_detected_message_not_present()
    
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_15(self):
        """
        ############################
        Increment clock from 10:04 - 10:05 AM
        ############################
        
        --
        ML 1,5,7:
        --
            * Watering continues
            * No variance detected
        
        --
        ML 6:
        --
            * Watering continues as expected
        
        --
        ML 8:
        --
            * Watering continues
            * No variance detected
            
        --
        ML 2:
        --
            * Watering should continue
            * Verify no additional variance detected message posted after setting FM 2's rate equal to expected flow
            
        --
        ML 3,4:
        --
            * Watering should continue, even with expected flow != actual flow, variance is set to 0%, thus no
              action should be taken by controller. No messages should be posted
            * Verify ML 4 has no message posted for low variance detected (ML 5 has shutdown enabled and wouldn't have
              a msg yet)
            
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
        
            # ------------------------------------------------------ #
            #                                                        #
            # 10:04 - 10:05 AM                                       #
            #                                                        #
            # ------------------------------------------------------ #
        
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
        
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 1,3,4 HIGH FLOW VARIANCE WITH SHUTDOWN            #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #
        
            # For each flow component with addresses 1, 5 and 7 verify:
            for address in [1, 5, 7]:
            
                # Programs
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_running()
            
                # Flow Sources
                self.config.BaseStation3200[1].water_sources[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].points_of_control[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].mainlines[address].statuses.verify_status_is_running()
            
                # Flow Devices
                self.config.BaseStation3200[1].master_valves[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].pumps[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_running()
        
            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
        
            # Mainline 5 Zones
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_watering()
        
            # Mainline 7 Zones
            self.config.BaseStation3200[1].zones[19].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[20].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[21].statuses.verify_status_is_watering()
        
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 1 LOW FLOW VARIANCE WITHOUT SHUTDOWN              #
            #                                                        #
            #   + Mainlines have 3 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #
        
            # Programs
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()
        
            # Flow Sources
            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[2].statuses.verify_status_is_running()
        
            # Flow Devices
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()
        
            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_waiting_to_water()
        
            # ---
            # NOTE:
            #
            # At this point, no Low Flow Variance detected message should be posted because our flow was adjusted to
            # not cause a variance.
            #
            #   -> Thus, verify no message posted
            # ---
        
            self.config.BaseStation3200[1].mainlines[2].messages.check_for_low_flow_variance_detected_message_not_present()
        
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 3 LOW FLOW VARIANCE WITHOUT SHUTDOWN              #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #
            
            # Programs
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()
        
            # Flow Sources
            self.config.BaseStation3200[1].water_sources[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[6].statuses.verify_status_is_running()
        
            # Flow Devices
            self.config.BaseStation3200[1].master_valves[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()
        
            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_waiting_to_water()
        
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 4 LOW FLOW VARIANCE WITH SHUTDOWN                 #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # Programs
            self.config.BaseStation3200[1].programs[8].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].water_sources[8].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[8].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[8].statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].master_valves[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[8].statuses.verify_status_is_running()

            # Mainline 8 Zones
            self.config.BaseStation3200[1].zones[22].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[23].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[24].statuses.verify_status_is_watering()
        
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 2 HIGH/LOW FLOW VARIANCE WITH/WITHOUT SHUTDOWN    #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #   + ML 3 = High Flow Variance With Shutdown            #
            #   + ML 4 = Low Flow Variance Without Shutdown          #
            #                                                        #
            # ------------------------------------------------------ #
        
            # For each flow component with addresses 3 and 4 verify:
            for address in [3, 4]:
            
                # Programs
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_running()
            
                # Flow Sources
                self.config.BaseStation3200[1].water_sources[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].points_of_control[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].mainlines[address].statuses.verify_status_is_running()
            
                # Flow Devices
                self.config.BaseStation3200[1].master_valves[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].pumps[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_running()
        
            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_waiting_to_water()
        
            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_waiting_to_water()
        
            # ---
            # NOTE:
            #
            # At this point, a Low Flow Variance would be detected on ML 4 if variance threshold was not 0%. The spec
            # says no variance event should be acted on in this case.
            #
            #   -> Thus, verify no message posted
            # ---
            
            self.config.BaseStation3200[1].mainlines[4].messages.check_for_low_flow_variance_detected_message_not_present()
    
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ################################
    def step_16(self):
        """
        ############################
        Increment clock from 10:05 - 10:10 AM
        ############################

        --
        ML 1,5,7:
        --
            * Watering continues, current zone group has 1 minute left of watering
            * No variance detected

        --
        ML 6:
        --
            * Watering continues as expected.
            * Current group of zones finishing up current watering cycle.
            * Next group of zones getting ready to be started.
            * FM 6 rates are set to trigger variance for next group of zones to water.
            * Flow stabilization will be determined from 10:10 to 10:11 min processing.
            * Flow variance will be acted on from 10:11 to 10:12 min processing.
            
        --
        ML 8:
        --
            * Watering continues, current zone group has 1 minute left of watering
            * No variance detected

        --
        ML 2:
        --
            * Current group of zones finishing up watering cycle.
            * Next group of zones are queued up to start watering.
            * FM 2 rate is set to NOT trigger variance for next group of zones to water.
            * Flow stabilization will be determined from 10:10 to 10:11 min processing.
            * Flow variance will be acted on from 10:11 to 10:12 min processing (expecting no variance event).

        --
        ML 3,4:
        --
            * Watering should continue, even with expected flow != actual flow, variance is set to 0%, thus no
              action should be taken by controller. No messages should be posted
            * Verify ML 4 has no message posted for low variance detected (ML 5 has shutdown enabled and wouldn't have
              a msg yet)

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:05 - 10:10 AM                                       #
            #                                                        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 1,3,4 HIGH FLOW VARIANCE WITH SHUTDOWN            #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # For each flow component with addresses 1, 5 and 7 verify:
            for address in [1, 5, 7]:

                # Programs
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_running()

                # Flow Sources
                self.config.BaseStation3200[1].water_sources[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].points_of_control[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].mainlines[address].statuses.verify_status_is_running()

                # Flow Devices
                self.config.BaseStation3200[1].master_valves[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].pumps[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_running()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()

            # Mainline 5 Zones
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_watering()

            # Mainline 7 Zones
            self.config.BaseStation3200[1].zones[19].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[20].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[21].statuses.verify_status_is_watering()

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 1 LOW FLOW VARIANCE WITHOUT SHUTDOWN              #
            #                                                        #
            #   + Mainlines have 3 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # Programs
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[2].statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_waiting_to_water()

            # ---
            # NOTE:
            #
            # At this point, current zone group is finishing up watering and the next zone group will start next step.
            #
            #   -> In order to not trigger messages on each minute, set FM 2's rate equal to next zone group's expected
            #      rate.
            # ---

            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[2],
                _expected_gpm=self.zn_6_df)

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 3 LOW FLOW VARIANCE WITHOUT SHUTDOWN              #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # Programs
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].water_sources[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[6].statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].master_valves[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()

            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_waiting_to_water()

            # ---
            # NOTE:
            #
            # At this point, the next group of zones will be turned on during the next min's processing. The next group
            # of zones will not have a variance event triggered.
            #
            #   -> Thus, set FM 6 rate equal to the actual flow to not trigger a variance event.
            # ---

            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[6],
                _expected_gpm=self.zn_18_df)

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 4 LOW FLOW VARIANCE WITH SHUTDOWN                 #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # Programs
            self.config.BaseStation3200[1].programs[8].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].water_sources[8].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[8].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[8].statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].master_valves[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[8].statuses.verify_status_is_running()

            # Mainline 8 Zones
            self.config.BaseStation3200[1].zones[22].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[23].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[24].statuses.verify_status_is_watering()

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 2 HIGH/LOW FLOW VARIANCE WITH/WITHOUT SHUTDOWN    #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #   + ML 3 = High Flow Variance With Shutdown            #
            #   + ML 4 = Low Flow Variance Without Shutdown          #
            #                                                        #
            # ------------------------------------------------------ #

            # For each flow component with addresses 3 and 4 verify:
            for address in [3, 4]:

                # Programs
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_running()

                # Flow Sources
                self.config.BaseStation3200[1].water_sources[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].points_of_control[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].mainlines[address].statuses.verify_status_is_running()

                # Flow Devices
                self.config.BaseStation3200[1].master_valves[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].pumps[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_running()

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_waiting_to_water()

            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_waiting_to_water()

            # ---
            # NOTE:
            #
            # At this point, a Low Flow Variance would be detected on ML 4 if variance threshold was not 0%. The spec
            # says no variance event should be acted on in this case.
            #
            #   -> Thus, verify no message posted
            # ---
            
            self.config.BaseStation3200[1].mainlines[4].messages.check_for_low_flow_variance_detected_message_not_present()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ################################
    def step_17(self):
        """
        ############################
        Increment clock from 10:10 - 10:11 AM
        ############################

        --
        ML 1,5,7:
        --
            * Current zone group finishing up current watering cycle.
            * Next zone group gets queued to start watering.
            * FM's 1, 5 and 7 rates are set to trigger high flow variance events.
            * Flow stabilization will be determined from 10:11 to 10:12 min processing.
            * Flow variance will be acted on from 10:12 to 10:13 min processing (expecting variance event)

        --
        ML 6:
        --
            * Watering continues as expected.
            * Last group of zones watering went to soaking.
            * Next group of zones started watering.
            * Flow stabilization completed and no variance expected.
            
        --
        ML 8:
        --
            * Current zone group finishing up current watering cycle.
            * Next zone group gets queued to start watering.
            * FM 8 rate is set to trigger low flow variance events.
            * Flow stabilization will be determined from 10:11 to 10:12 min processing.
            * Flow variance will be acted on from 10:12 to 10:13 min processing (expecting variance event)

        --
        ML 2:
        --
            * Current group of zones finished up watering cycle and went to soaking.
            * Next group of zones started watering.
            * Flow stabilization (1/3 minutes completed)
            * Flow stabilization will be determined from 10:10 to 10:11 min processing.
            * Flow variance will be acted on from 10:11 to 10:12 min processing (expecting no variance event).

        --
        ML 3,4:
        --
            * Watering should continue, even with expected flow != actual flow, variance is set to 0%, thus no
              action should be taken by controller. No messages should be posted
            * Verify ML 4 has no message posted for low variance detected (ML 5 has shutdown enabled and wouldn't have
              a msg yet)

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
        
            # ------------------------------------------------------ #
            #                                                        #
            # 10:10 - 10:11 AM                                       #
            #                                                        #
            # ------------------------------------------------------ #
        
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
        
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 1,3,4 HIGH FLOW VARIANCE WITH SHUTDOWN            #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #
        
            # For each flow component with addresses 1, 5 and 7 verify:
            for address in [1, 5, 7]:
            
                # Programs
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_running()
            
                # Flow Sources
                self.config.BaseStation3200[1].water_sources[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].points_of_control[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].mainlines[address].statuses.verify_status_is_running()
            
                # Flow Devices
                self.config.BaseStation3200[1].master_valves[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].pumps[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_running()
        
            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
        
            # Mainline 5 Zones
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_watering()
        
            # Mainline 7 Zones
            self.config.BaseStation3200[1].zones[19].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[20].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[21].statuses.verify_status_is_watering()

            # # ---
            # # NOTE:
            # #
            # # At this point, the next group of zones is queued up to start watering during the next minute processing..
            # #
            # #   -> This means that we need to set FM 1, 5 and 7's rate to trigger variance because we are trying to fail
            # #      the next group of zones.
            # # ---
            #
            # helper_methods.update_flow_to_trigger_high_flow_variance(
            #     _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
            #     _variance_percent=self.ml_1_hi_fl_vr,
            #     _expected_gpm=self.zn_2_df)
            #
            # helper_methods.update_flow_to_trigger_high_flow_variance(
            #     _flow_meter=self.config.BaseStation3200[1].flow_meters[5],
            #     _variance_percent=self.ml_5_hi_fl_vr,
            #     _expected_gpm=self.zn_14_df)
            #
            # helper_methods.update_flow_to_trigger_high_flow_variance(
            #     _flow_meter=self.config.BaseStation3200[1].flow_meters[7],
            #     _variance_percent=self.ml_7_hi_fl_vr,
            #     _expected_gpm=self.zn_20_df)
        
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 1 LOW FLOW VARIANCE WITHOUT SHUTDOWN              #
            #                                                        #
            #   + Mainlines have 3 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #
        
            # Programs
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()
        
            # Flow Sources
            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[2].statuses.verify_status_is_running()
        
            # Flow Devices
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()
        
            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_watering()
        
            # ---
            # NOTE:
            #
            # At this point, no Low Flow Variance detected message should be posted because our flow was adjusted to
            # not cause a variance.
            #
            #   -> Thus, verify no message posted
            # ---
        
            self.config.BaseStation3200[1].mainlines[2].messages.check_for_low_flow_variance_detected_message_not_present()
        
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 3 LOW FLOW VARIANCE WITHOUT SHUTDOWN              #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #
            
            # Programs
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()
        
            # Flow Sources
            self.config.BaseStation3200[1].water_sources[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[6].statuses.verify_status_is_running()
        
            # Flow Devices
            self.config.BaseStation3200[1].master_valves[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()
        
            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_watering()
        
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 4 LOW FLOW VARIANCE WITH SHUTDOWN                 #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # Programs
            self.config.BaseStation3200[1].programs[8].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].water_sources[8].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[8].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[8].statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].master_valves[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[8].statuses.verify_status_is_running()

            # Mainline 8 Zones
            self.config.BaseStation3200[1].zones[22].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[23].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[24].statuses.verify_status_is_watering()

            # # ---
            # # NOTE:
            # #
            # # At this point, the next group of zones is queued up to start watering during the next minute processing..
            # #
            # #   -> This means that we need to set FM 8's rate to trigger variance because we are trying to fail the
            # #      next group of zones.
            # # ---
            #
            # helper_methods.update_flow_to_trigger_low_flow_variance(
            #     _flow_meter=self.config.BaseStation3200[1].flow_meters[8],
            #     _variance_percent=self.ml_8_lo_fl_vr,
            #     _expected_gpm=self.zn_23_df)
        
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 2 HIGH/LOW FLOW VARIANCE WITH/WITHOUT SHUTDOWN    #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #   + ML 3 = High Flow Variance With Shutdown            #
            #   + ML 4 = Low Flow Variance Without Shutdown          #
            #                                                        #
            # ------------------------------------------------------ #
        
            # For each flow component with addresses 3 and 4 verify:
            for address in [3, 4]:
            
                # Programs
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_running()
            
                # Flow Sources
                self.config.BaseStation3200[1].water_sources[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].points_of_control[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].mainlines[address].statuses.verify_status_is_running()
            
                # Flow Devices
                self.config.BaseStation3200[1].master_valves[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].pumps[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_running()
        
            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_watering()
        
            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_watering()
        
            # ---
            # NOTE:
            #
            # At this point, a Low Flow Variance would be detected on ML 4 if variance threshold was not 0%. The spec
            # says no variance event should be acted on in this case.
            #
            #   -> Thus, verify no message posted
            # ---
            
            self.config.BaseStation3200[1].mainlines[4].messages.check_for_low_flow_variance_detected_message_not_present()
    
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ################################
    def step_18(self):
        """
        ############################
        Increment clock from 10:11 - 10:12 AM
        ############################

        --
        ML 1,5,7:
        --
            * Last zone group finished watering and started soaking.
            * New zone group started watering.
            * Flow stabilization completes and flow fault detected.
            * Flow variance will be acted on from 10:12 to 10:13 min processing (expecting variance event)

        --
        ML 6:
        --
            * Watering continues as expected.
            * Flow stabilization completes.
            * No variance expected, will be verified in 10:12 to 10:13 min processing.
            
        --
        ML 8:
        --
            * Last zone group finished watering and started soaking.
            * New zone group started watering.
            * Flow stabilization completes and flow fault detected.
            * Flow variance will be acted on from 10:12 to 10:13 min processing (expecting variance event)

        --
        ML 2:
        --
            * Flow stabilization (2/3 minutes completed)
            * Flow stabilization will be determined from 10:12 to 10:13 min processing.
            * Flow variance will be acted on from 10:13 to 10:14 min processing (expecting no variance event).

        --
        ML 3,4:
        --
            * Watering should continue, even with expected flow != actual flow, variance is set to 0%, thus no
              action should be taken by controller. No messages should be posted
            * Verify ML 4 has no message posted for low variance detected (ML 5 has shutdown enabled and wouldn't have
              a msg yet)

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
        
            # ------------------------------------------------------ #
            #                                                        #
            # 10:11 - 10:12 AM                                       #
            #                                                        #
            # ------------------------------------------------------ #
        
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # ---
            # NOTE:
            #
            # At this point, the next group of zones is queued up to start watering during the next minute processing..
            #
            #   -> This means that we need to set FM 1, 5 and 7's rate to trigger variance because we are trying to fail
            #      the next group of zones.
            # ---

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _variance_percent=self.ml_1_hi_fl_vr,
                _expected_gpm=self.zn_2_df)

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[5],
                _variance_percent=self.ml_5_hi_fl_vr,
                _expected_gpm=self.zn_14_df)

            helper_methods.update_flow_to_trigger_high_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[7],
                _variance_percent=self.ml_7_hi_fl_vr,
                _expected_gpm=self.zn_20_df)

            # ---
            # NOTE:
            #
            # At this point, the next group of zones is queued up to start watering during the next minute processing..
            #
            #   -> This means that we need to set FM 8's rate to trigger variance because we are trying to fail the
            #      next group of zones.
            # ---

            helper_methods.update_flow_to_trigger_low_flow_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[8],
                _variance_percent=self.ml_8_lo_fl_vr,
                _expected_gpm=self.zn_23_df)
        
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 1,3,4 HIGH FLOW VARIANCE WITH SHUTDOWN            #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #
        
            # For each flow component with addresses 1, 5 and 7 verify:
            for address in [1, 5, 7]:
            
                # Programs
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_running()
            
                # Flow Sources
                self.config.BaseStation3200[1].water_sources[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].points_of_control[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].mainlines[address].statuses.verify_status_is_running()
            
                # Flow Devices
                self.config.BaseStation3200[1].master_valves[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].pumps[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_running()
        
            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
        
            # Mainline 5 Zones
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_soaking()
        
            # Mainline 7 Zones
            self.config.BaseStation3200[1].zones[19].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[20].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[21].statuses.verify_status_is_soaking()
        
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 1 LOW FLOW VARIANCE WITHOUT SHUTDOWN              #
            #                                                        #
            #   + Mainlines have 3 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #
        
            # Programs
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()
        
            # Flow Sources
            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[2].statuses.verify_status_is_running()
        
            # Flow Devices
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()
        
            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_watering()
        
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 3 LOW FLOW VARIANCE WITHOUT SHUTDOWN              #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #
            
            # Programs
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()
        
            # Flow Sources
            self.config.BaseStation3200[1].water_sources[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[6].statuses.verify_status_is_running()
        
            # Flow Devices
            self.config.BaseStation3200[1].master_valves[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()
        
            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_watering()
        
            # ---
            # NOTE:
            #
            # At this point, no Low Flow Variance detected message should be posted because our flow was adjusted to
            # not cause a variance.
            #
            #   -> Thus, verify no message posted
            # ---

            self.config.BaseStation3200[1].mainlines[6].messages.check_for_low_flow_variance_detected_message_not_present()
            
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 4 LOW FLOW VARIANCE WITH SHUTDOWN                 #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # Programs
            self.config.BaseStation3200[1].programs[8].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].water_sources[8].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[8].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[8].statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].master_valves[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[8].statuses.verify_status_is_running()

            # Mainline 8 Zones
            self.config.BaseStation3200[1].zones[22].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[23].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[24].statuses.verify_status_is_soaking()
        
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 2 HIGH/LOW FLOW VARIANCE WITH/WITHOUT SHUTDOWN    #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #   + ML 3 = High Flow Variance With Shutdown            #
            #   + ML 4 = Low Flow Variance Without Shutdown          #
            #                                                        #
            # ------------------------------------------------------ #
        
            # For each flow component with addresses 3 and 4 verify:
            for address in [3, 4]:
            
                # Programs
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_running()
            
                # Flow Sources
                self.config.BaseStation3200[1].water_sources[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].points_of_control[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].mainlines[address].statuses.verify_status_is_running()
            
                # Flow Devices
                self.config.BaseStation3200[1].master_valves[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].pumps[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_running()
        
            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_watering()
        
            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_watering()
        
            # ---
            # NOTE:
            #
            # At this point, a Low Flow Variance would be detected on ML 4 if variance threshold was not 0%. The spec
            # says no variance event should be acted on in this case.
            #
            #   -> Thus, verify no message posted
            # ---
            
            self.config.BaseStation3200[1].mainlines[4].messages.check_for_low_flow_variance_detected_message_not_present()
    
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        
    ################################
    def step_19(self):
        """
        ############################
        Increment clock from 10:12 - 10:13 AM
        ############################

        --
        ML 1,5,7:
        --
            * High flow variance acted on, current status's should stay the same.
            * There is 1 suspect zone per ML 1, 5 and 7 that will be ran by itself due to it having 2 strikes:
                + ZN 2 (on ML 1) - should stay watering
                + ZN 14 (on ML 5) - should stay watering
                + ZN 20 (on ML 7)  - should stay watering
            * Since flow variance was detected, flow stabilization starts and completes for ML's 1, 5 and 7 during
              the minute processing of 10:12 to 10:13.
            * Lastly, high flow variance detected during this minute's processing will be acted on during 10:13 to
              10:14 and the zones listed above should receive their 3rd strike and error out.
                + When this happens, the other watering components (WS,PC,ML,FM,MV,PM) go into a "waiting" state
                  respectively. In order to get FM's to get into their waiting state, we must update FM 1, 5 and 7's
                  rates to 0 GPM because there are no other zones running on their respective ML's, which causes their
                  status to be "Ok" while the rest of the Zones finish soaking. (Setting FM was moved to 10:13-10:14
                  (step 20) so that we could get the zones above to error out)

        --
        ML 6:
        --
            * Watering continues as expected.
            * Verify no variance detected.
            
        --
        ML 8:
        --
            * Low flow variance acted on, current status's should stay the same.
            * There is 1 suspect zone that will be ran by itself due to it having 2 strikes:
                + ZN 23 (on ML 8) - should stay watering
            * Since flow variance was detected, flow stabilization starts and completes for ML 8 during
              the minute processing of 10:12 to 10:13.
            * Lastly, low flow variance detected during this minute's processing will be acted on during 10:13 to
              10:14 and the zones listed above should receive their 3rd strike and error out.
                + When this happens, the other watering components (WS,PC,ML,FM,MV,PM) go into a "waiting" state
                  respectively. In order to get FM's to get into their waiting state, we must update FM 8's
                  rates to 0 GPM because there are no other zones running on their respective ML's, which causes their
                  status to be "Ok" while the rest of the Zones finish soaking. (Setting FM was moved to 10:13-10:14
                  (step 20) so that we could get the zones above to error out)

        --
        ML 2:
        --
            * Complete final Flow stabilization (3/3 minutes completed) minute and read FM 2 rate
            * Flow variance will be acted on from 10:13 to 10:14 min processing (expecting no variance event).

        --
        ML 3,4:
        --
            * Watering should continue, even with expected flow != actual flow, variance is set to 0%, thus no
              action should be taken by controller. No messages should be posted
            * Verify ML 4 has no message posted for low variance detected (ML 5 has shutdown enabled and wouldn't have
              a msg yet)

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:12 - 10:13 AM                                       #
            #                                                        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 1,3,4 HIGH FLOW VARIANCE WITH SHUTDOWN            #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # For each flow component with addresses 1, 5 and 7 verify:
            for address in [1, 5, 7]:

                # Programs
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_running()

                # Flow Sources
                self.config.BaseStation3200[1].water_sources[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].points_of_control[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].mainlines[address].statuses.verify_status_is_running()

                # Flow Devices
                self.config.BaseStation3200[1].master_valves[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].pumps[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_running()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()

            # Mainline 5 Zones
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_soaking()

            # Mainline 7 Zones
            self.config.BaseStation3200[1].zones[19].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[20].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[21].statuses.verify_status_is_soaking()

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 1 LOW FLOW VARIANCE WITHOUT SHUTDOWN              #
            #                                                        #
            #   + Mainlines have 3 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # Programs
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[2].statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_watering()

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 3 LOW FLOW VARIANCE WITHOUT SHUTDOWN              #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # Programs
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].water_sources[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[6].statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].master_valves[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()

            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_watering()

            # ---
            # NOTE:
            #
            # At this point, no Low Flow Variance detected message should be posted because our flow was adjusted to
            # not cause a variance.
            #
            #   -> Thus, verify no message posted
            # ---

            self.config.BaseStation3200[1].mainlines[6].messages.check_for_low_flow_variance_detected_message_not_present()
            
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 4 LOW FLOW VARIANCE WITH SHUTDOWN                 #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # Programs
            self.config.BaseStation3200[1].programs[8].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].water_sources[8].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[8].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[8].statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].master_valves[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[8].statuses.verify_status_is_running()

            # Mainline 8 Zones
            self.config.BaseStation3200[1].zones[22].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[23].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[24].statuses.verify_status_is_soaking()

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 2 HIGH/LOW FLOW VARIANCE WITH/WITHOUT SHUTDOWN    #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #   + ML 3 = High Flow Variance With Shutdown            #
            #   + ML 4 = Low Flow Variance Without Shutdown          #
            #                                                        #
            # ------------------------------------------------------ #

            # For each flow component with addresses 3 and 4 verify:
            for address in [3, 4]:

                # Programs
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_running()

                # Flow Sources
                self.config.BaseStation3200[1].water_sources[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].points_of_control[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].mainlines[address].statuses.verify_status_is_running()

                # Flow Devices
                self.config.BaseStation3200[1].master_valves[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].pumps[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_running()

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_watering()

            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_watering()

            # ---
            # NOTE:
            #
            # At this point, a Low Flow Variance would be detected on ML 4 if variance threshold was not 0%. The spec
            # says no variance event should be acted on in this case.
            #
            #   -> Thus, verify no message posted
            # ---

            self.config.BaseStation3200[1].mainlines[4].messages.check_for_low_flow_variance_detected_message_not_present()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ################################
    def step_20(self):
        """
        ############################
        Increment clock from 10:13 - 10:14 AM
        ############################

        --
        ML 1,5,7:
        --
            * Watering should continue for remaining zones on respective ML's 1, 5 and 7
            * This step is the verifier for the high flow variance shut-down on a Mainline feature. This is the point
              where we are expecting zones to be shutdown but watering continues.
            * Verify high flow variance shutdown on MLs 1, 5 and 7
                + Because flow became stable during the 10:12-10:13 min processing. This allows the controller to act
                  on the flow readings during that min processing to trigger flow variance
                + The following zones should be shutdown:
                    - ZN 2 (on ML 1)
                    - ZN 14 (on ML 5)
                    - ZN 20 (on ML 7)
                + Verify the zones above have error status with message posted.
                    -> The next step we will verify their status is set to Done when the message is cleared.

        --
        ML 6:
        --
            * Watering continues as expected.

        --
        ML 8:
        --
            * Watering should continue for remaining zones on respective ML 8
            * This step is the verifier for the low flow variance shut-down on a Mainline feature. This is the point
              where we are expecting zones to be shutdown but watering continues.
            * Verify low flow variance shutdown on MLs 8
                + Because flow became stable during the 10:12-10:13 min processing. This allows the controller to act
                  on the flow readings during that min processing to trigger flow variance
                + The following zones should be shutdown:
                    - ZN 23 (on ML 8)
                + Verify the zones above have error status with message posted.
                    -> The next step we will verify their status is set to Done when the message is cleared.

        --
        ML 2:
        --
            * Act on flow variance detected from 10:12 to 10:13 min processing
                + Verify no message

        --
        ML 3,4:
        --
            * Watering should continue, even with expected flow != actual flow, variance is set to 0%, thus no
              action should be taken by controller. No messages should be posted
            * Verify ML 4 has no message posted for low variance detected (ML 5 has shutdown enabled and wouldn't have
              a msg yet)

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
        
            # ------------------------------------------------------ #
            #                                                        #
            # 10:13 - 10:14 AM                                       #
            #                                                        #
            # ------------------------------------------------------ #
        
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
        
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 1,3,4 HIGH FLOW VARIANCE WITH SHUTDOWN            #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #
        
            # For each flow component with addresses 1, 5 and 7 verify:
            for address in [1, 5, 7]:
    
                # Programs
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_waiting_to_run()
    
                # Flow Sources
                self.config.BaseStation3200[1].water_sources[address].statuses.verify_status_is_ok()
                self.config.BaseStation3200[1].points_of_control[address].statuses.verify_status_is_off()
                self.config.BaseStation3200[1].mainlines[address].statuses.verify_status_is_off()
    
                # Flow Devices
                self.config.BaseStation3200[1].master_valves[address].statuses.verify_status_is_off()
                self.config.BaseStation3200[1].pumps[address].statuses.verify_status_is_off()
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_running()
        
            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_error()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
        
            # Mainline 5 Zones
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_error()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_soaking()
        
            # Mainline 7 Zones
            self.config.BaseStation3200[1].zones[19].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[20].statuses.verify_status_is_error()
            self.config.BaseStation3200[1].zones[21].statuses.verify_status_is_soaking()
            
            # Verify and clear shutdown messages
            self.config.BaseStation3200[1].programs[1].zone_programs[2].messages.verify_shutdown_on_high_flow_variance_message()
            self.config.BaseStation3200[1].programs[1].zone_programs[2].messages.clear_shutdown_on_high_flow_variance_message()
            
            self.config.BaseStation3200[1].programs[5].zone_programs[14].messages.verify_shutdown_on_high_flow_variance_message()
            self.config.BaseStation3200[1].programs[5].zone_programs[14].messages.clear_shutdown_on_high_flow_variance_message()
            
            self.config.BaseStation3200[1].programs[7].zone_programs[20].messages.verify_shutdown_on_high_flow_variance_message()
            self.config.BaseStation3200[1].programs[7].zone_programs[20].messages.clear_shutdown_on_high_flow_variance_message()

            # ---
            # NOTE:
            #
            # At this point, ZNs 2, 14 and 20 will error out when the controller increments time from 10:14 - 10:20 and
            # the rest of the other watering components (WS,PC,ML,FM,MV,PM) go into a "waiting" state.
            #
            #   -> This means that we need to set FM 1, 5 and 7's rate to 0 GPM to reflect actual flow vs expected flow
            #      and to get them to go into their "waiting" state of "Ok".
            # ---

            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _expected_gpm=0)

            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[5],
                _expected_gpm=0)

            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[7],
                _expected_gpm=0)

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 1 LOW FLOW VARIANCE WITHOUT SHUTDOWN              #
            #                                                        #
            #   + Mainlines have 3 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #
        
            # Programs
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()
        
            # Flow Sources
            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[2].statuses.verify_status_is_running()
        
            # Flow Devices
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()
        
            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_watering()

            # ---
            # NOTE:
            #
            # At this point, no Low Flow Variance detected message should be posted because our flow was adjusted to
            # not cause a variance.
            #
            #   -> Thus, verify no message posted
            # ---

            self.config.BaseStation3200[1].mainlines[2].messages.check_for_low_flow_variance_detected_message_not_present()
        
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 3 LOW FLOW VARIANCE WITHOUT SHUTDOWN              #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #
            
            # Programs
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()
        
            # Flow Sources
            self.config.BaseStation3200[1].water_sources[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[6].statuses.verify_status_is_running()
        
            # Flow Devices
            self.config.BaseStation3200[1].master_valves[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()
        
            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_watering()
        
            # ---
            # NOTE:
            #
            # At this point, no Low Flow Variance detected message should be posted because our flow was adjusted to
            # not cause a variance.
            #
            #   -> Thus, verify no message posted
            # ---
        
            self.config.BaseStation3200[1].mainlines[6].messages.check_for_low_flow_variance_detected_message_not_present()
            
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 4 LOW FLOW VARIANCE WITH SHUTDOWN                 #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # Programs
            self.config.BaseStation3200[1].programs[8].statuses.verify_status_is_waiting_to_run()

            # Flow Sources
            self.config.BaseStation3200[1].water_sources[8].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].points_of_control[8].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[8].statuses.verify_status_is_off()

            # Flow Devices
            self.config.BaseStation3200[1].master_valves[8].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[8].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].flow_meters[8].statuses.verify_status_is_running()

            # Mainline 8 Zones
            self.config.BaseStation3200[1].zones[22].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[23].statuses.verify_status_is_error()
            self.config.BaseStation3200[1].zones[24].statuses.verify_status_is_soaking()
            
            # Verify and clear shutdown messages
            self.config.BaseStation3200[1].programs[8].zone_programs[23].messages.verify_shutdown_on_low_flow_variance_message()
            self.config.BaseStation3200[1].programs[8].zone_programs[23].messages.clear_shutdown_on_low_flow_variance_message()
            
            # ---
            # NOTE:
            #
            # At this point, ZN 23 will error out when the controller increments time from 10:14 - 10:20 and
            # the rest of the other watering components (WS,PC,ML,FM,MV,PM) go into a "waiting" state.
            #
            #   -> This means that we need to set FM 8's rate to 0 GPM to reflect actual flow vs expected flow
            #      and to get them to go into their "waiting" state of "Ok".
            # ---

            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[8],
                _expected_gpm=0)
        
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 2 HIGH/LOW FLOW VARIANCE WITH/WITHOUT SHUTDOWN    #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #   + ML 3 = High Flow Variance With Shutdown            #
            #   + ML 4 = Low Flow Variance Without Shutdown          #
            #                                                        #
            # ------------------------------------------------------ #
        
            # For each flow component with addresses 3 and 4 verify:
            for address in [3, 4]:
            
                # Programs
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_running()
            
                # Flow Sources
                self.config.BaseStation3200[1].water_sources[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].points_of_control[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].mainlines[address].statuses.verify_status_is_running()
            
                # Flow Devices
                self.config.BaseStation3200[1].master_valves[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].pumps[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_running()
        
            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_watering()
        
            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_watering()
        
            # ---
            # NOTE:
            #
            # At this point, a Low Flow Variance would be detected on ML 4 if variance threshold was not 0%. The spec
            # says no variance event should be acted on in this case.
            #
            #   -> Thus, verify no message posted
            # ---
        
            self.config.BaseStation3200[1].mainlines[4].messages.check_for_low_flow_variance_detected_message_not_present()
    
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ################################
    def step_21(self):
        """
        ############################
        Increment clock from 10:14 - 10:20 AM
        ############################

        --
        ML 1,5,7:
        --
            * Watering should continue for remaining zones on respective ML's 1, 5 and 7
            * Verify that the following zones are set to done after clearing their high flow shutdown messages the
              previous step.
                + ZN 2 (on ML 1)
                + ZN 14 (on ML 5)
                + ZN 20 (on ML 7)
            * Verify all non-zone flow components are in their "IDLE" state (Off, Ok, Done, Waiting)
                + This is because the only running zone was shut down
                + Other zones aren't done with their soak cycle

            * Whats the controller doing:
                * Remaining ZNs are finishing up their soak cycles (1 more minute left of soaking)

        --
        ML 6:
        --
            * Watering continues as expected.
            * Update FMs 6's flow rates (actual flow) to be equal to the expected flow (sum of the next group of
              zones to run)

            * Whats the controller doing:
                + Finishing up last minute of current zone group's watering cycle before transitioning to soaking.
                + Queueing up next group of zones to start watering
                  (Next step, the zone's status' will change because we are processing the top of the minute)

        --
        ML 8:
        --
            * Watering should continue for remaining zones on respective ML 8
            * Verify that the following zones are set to done after clearing their low flow shutdown messages the
              previous step.
                + ZN 23 (on ML 8)
            * Verify all non-zone flow components are in their "IDLE" state (Off, Ok, Done, Waiting)
                + This is because the only running zone was shut down
                + Other zones aren't done with their soak cycle

            * Whats the controller doing:
                * Remaining ZNs are finishing up their soak cycles (1 more minute left of soaking)

        --
        ML 2:
        --
            * Finishing up watering cycle for current group of zones.
            * Since we are at the top of the minute, status' shouldn't change to reflect new zones turning on until
              minute processing 10:20 - 10:21.
            * Set FM 2's rate here because we want the FM readings for pipe stabilization done from 10:20 to 10:23 so
              that we trigger a variance on the next zones watering.

        --
        ML 3,4:
        --
            * Watering should continue, even with expected flow != actual flow, variance is set to 0%, thus no
              action should be taken by controller. No messages should be posted
            * Verify ML 4 has no message posted for low variance detected (ML 5 has shutdown enabled and wouldn't have
              a msg yet)

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
        
            # ------------------------------------------------------ #
            #                                                        #
            # 10:14 - 10:20 AM                                       #
            #                                                        #
            # ------------------------------------------------------ #
        
            self.config.BaseStation3200[1].do_increment_clock(minutes=6)
        
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 1,3,4 HIGH FLOW VARIANCE WITH SHUTDOWN            #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #
        
            # For each flow component with addresses 1, 5 and 7 verify:
            for address in [1, 5, 7]:

                # Programs
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_waiting_to_run()

                # Flow Sources
                self.config.BaseStation3200[1].water_sources[address].statuses.verify_status_is_ok()
                self.config.BaseStation3200[1].points_of_control[address].statuses.verify_status_is_off()
                self.config.BaseStation3200[1].mainlines[address].statuses.verify_status_is_off()

                # Flow Devices
                self.config.BaseStation3200[1].master_valves[address].statuses.verify_status_is_off()
                self.config.BaseStation3200[1].pumps[address].statuses.verify_status_is_off()
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_ok()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
        
            # Mainline 5 Zones
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_soaking()
        
            # Mainline 7 Zones
            self.config.BaseStation3200[1].zones[19].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[20].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[21].statuses.verify_status_is_soaking()
        
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 1 LOW FLOW VARIANCE WITHOUT SHUTDOWN              #
            #                                                        #
            #   + Mainlines have 3 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #
        
            # Programs
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()
        
            # Flow Sources
            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[2].statuses.verify_status_is_running()
        
            # Flow Devices
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()
        
            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_watering()

            # ---
            # NOTE:
            #
            # At this point, we are just simulating through the rest of the programming, to verify expected ending state
            #
            #   -> In order to not trigger messages on each minute, set FM 2's rate equal to their expected rate. The
            #      values being set here will be read in next minute processing (10:20 - 10:21)
            #
            #       This will allow us to:
            #           - verify additional low flow variance messages aren't posted when the variance isn't reached
            # ---

            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[2],
                _expected_gpm=self.zn_4_df + self.zn_5_df)

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 3 LOW FLOW VARIANCE WITHOUT SHUTDOWN              #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #
            
            # Programs
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()
        
            # Flow Sources
            self.config.BaseStation3200[1].water_sources[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[6].statuses.verify_status_is_running()
        
            # Flow Devices
            self.config.BaseStation3200[1].master_valves[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()
        
            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_watering()
        
            # ---
            # NOTE:
            #
            # At this point, we are just simulating through the rest of the programming, to verify expected ending state
            #
            #   -> In order to not trigger messages on each minute, set FM 6's rate equal to their expected
            #      rate. The values being set here will be read in next minute processing (10:20 - 10:21)
            #
            #       This will allow us to:
            #           - verify additional low flow variance messages aren't posted when the variance isn't reached
            # ---

            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[6],
                _expected_gpm=self.zn_16_df + self.zn_17_df)

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 4 LOW FLOW VARIANCE WITH SHUTDOWN                 #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # Programs
            self.config.BaseStation3200[1].programs[8].statuses.verify_status_is_waiting_to_run()

            # Flow Sources
            self.config.BaseStation3200[1].water_sources[8].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].points_of_control[8].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[8].statuses.verify_status_is_off()

            # Flow Devices
            self.config.BaseStation3200[1].master_valves[8].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[8].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].flow_meters[8].statuses.verify_status_is_ok()

            # Mainline 8 Zones
            self.config.BaseStation3200[1].zones[22].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[23].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[24].statuses.verify_status_is_soaking()

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 2 HIGH/LOW FLOW VARIANCE WITH/WITHOUT SHUTDOWN    #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #   + ML 3 = High Flow Variance With Shutdown            #
            #   + ML 4 = Low Flow Variance Without Shutdown          #
            #                                                        #
            # ------------------------------------------------------ #
        
            # For each flow component with addresses 3 and 4 verify:
            for address in [3, 4]:
            
                # Programs
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_running()
            
                # Flow Sources
                self.config.BaseStation3200[1].water_sources[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].points_of_control[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].mainlines[address].statuses.verify_status_is_running()
            
                # Flow Devices
                self.config.BaseStation3200[1].master_valves[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].pumps[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_running()
        
            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_watering()
        
            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_watering()
        
            # ---
            # NOTE:
            #
            # At this point, a Low Flow Variance would be detected on ML 4 if variance threshold was not 0%. The spec
            # says no variance event should be acted on in this case.
            #
            #   -> Thus, verify no message posted
            # ---
        
            self.config.BaseStation3200[1].mainlines[4].messages.check_for_low_flow_variance_detected_message_not_present()
    
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ################################
    def step_22(self):
        """
        ############################
        Increment clock from 10:20 - 10:21 AM
        ############################

        --
        ML 1,5,7:
        --
            * Watering should continue for remaining zones on respective ML's 1, 5 and 7
            * Verify that the following zones are set to done after clearing their high flow shutdown messages the
              previous step.
                + ZN 2 (on ML 1)
                + ZN 14 (on ML 5)
                + ZN 20 (on ML 7)
            * Verify all non-zone flow components are in their "IDLE" state (Off, Ok, Done, Waiting)
                + This is because the only running zone was shut down
                + Other zones aren't done with their soak cycle
            * Update FMs 1, 5 and 7 flow rates (actual flow) to be equal to the expected flow (sum of the next group of
              zones to run)

            * Whats the controller doing:
                * Remaining ZNs are finishing up their soak cycles (0 more minute left of soaking)
                    -> Will start watering when incrementing clock from 10:21 - 10:22

        --
        ML 6:
        --
            * Watering continues as expected.
            * Verify next group of zones start watering

            * Whats the controller doing:
                + Waits for 1 min pipe stabilization period to finish as well as takes FM reading to determine if
                  there is any flow variance.
                + Will act on variance (if found) during 10:21 - 10:22 min processing
                    -> in this case, we are not expecting variance

        --
        ML 8:
        --
            * Watering should continue for remaining zones on respective ML 8
            * Verify that the following zones are set to done after clearing their low flow shutdown messages the
              previous step.
                + ZN 23 (on ML 8)
            * Verify all non-zone flow components are in their "IDLE" state (Off, Ok, Done, Waiting)
                + This is because the only running zone was shut down
                + Other zones aren't done with their soak cycle
            * Update FM 8 flow rates (actual flow) to be equal to the expected flow (sum of the next group of
              zones to run)

            * Whats the controller doing:
                * Remaining ZNs are finishing up their soak cycles (0 more minute left of soaking)
                    -> Will start watering when incrementing clock from 10:21 - 10:22

        --
        ML 2:
        --
            * Zones that were watering are now soaking
            * Verify next group of zones start watering

            * Whats the controller doing:
                + Waits for 1 min pipe stabilization period to finish as well as takes FM reading to determine if
                  there is any flow variance.
                + Will act on variance (if found) during 10:21 - 10:22 min processing
                    -> in this case, we are not expecting variance

        --
        ML 3,4:
        --
            * Zones that were watering are now soaking
            * Verify next group of zones start watering
            * Watering should continue, even with expected flow != actual flow, variance is set to 0%, thus no
              action should be taken by controller. No messages should be posted
            * Verify ML 4 has no message posted for low variance detected even with expected != actual flow

            * Whats the controller doing:
                + Waits for 1 min pipe stabilization period to finish as well as takes FM reading to determine if
                  there is any flow variance.
                + Will act on variance (if found) during 10:21 - 10:22 min processing
                    -> in this case, our variance thresholds were 0% and thus we are not expecting a variance detection

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:20 - 10:21 AM                                       #
            #                                                        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 1,3,4 HIGH FLOW VARIANCE WITH SHUTDOWN            #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # For each flow component with addresses 1, 5 and 7 verify:
            for address in [1, 5, 7]:

                # Programs
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_waiting_to_run()

                # Flow Sources
                self.config.BaseStation3200[1].water_sources[address].statuses.verify_status_is_ok()
                self.config.BaseStation3200[1].points_of_control[address].statuses.verify_status_is_off()
                self.config.BaseStation3200[1].mainlines[address].statuses.verify_status_is_off()

                # Flow Devices
                self.config.BaseStation3200[1].master_valves[address].statuses.verify_status_is_off()
                self.config.BaseStation3200[1].pumps[address].statuses.verify_status_is_off()
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_ok()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()

            # Mainline 5 Zones
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_soaking()

            # Mainline 7 Zones
            self.config.BaseStation3200[1].zones[19].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[20].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[21].statuses.verify_status_is_soaking()

            # ---
            # NOTE:
            #
            # At this point, we are just simulating through the rest of the programming, to verify expected ending state
            #
            #   -> In order to not trigger messages on each minute, set FM's 1, 5 and 7's rate equal to their expected
            #      rate. The values being set here will be read in next minute processing (10:21 - 10:22)
            #
            # ---

            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _expected_gpm=self.zn_1_df + self.zn_3_df)

            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[5],
                _expected_gpm=self.zn_13_df + self.zn_15_df)

            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[7],
                _expected_gpm=self.zn_19_df + self.zn_21_df)

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 1 LOW FLOW VARIANCE WITHOUT SHUTDOWN              #
            #                                                        #
            #   + Mainlines have 3 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # Programs
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[2].statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_soaking()

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 3 LOW FLOW VARIANCE WITHOUT SHUTDOWN              #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # Programs
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].water_sources[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[6].statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].master_valves[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()

            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_soaking()

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 4 LOW FLOW VARIANCE WITH SHUTDOWN                 #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # Programs
            self.config.BaseStation3200[1].programs[8].statuses.verify_status_is_waiting_to_run()

            # Flow Sources
            self.config.BaseStation3200[1].water_sources[8].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].points_of_control[8].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[8].statuses.verify_status_is_off()

            # Flow Devices
            self.config.BaseStation3200[1].master_valves[8].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[8].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].flow_meters[8].statuses.verify_status_is_ok()

            # Mainline 8 Zones
            self.config.BaseStation3200[1].zones[22].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[23].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[24].statuses.verify_status_is_soaking()

            # ---
            # NOTE:
            #
            # At this point, we are just simulating through the rest of the programming, to verify expected ending state
            #
            #   -> In order to not trigger messages on each minute, set FM 8's rate equal to their expected
            #      rate. The values being set here will be read in next minute processing (10:21 - 10:22)
            # ---

            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[8],
                _expected_gpm=self.zn_22_df + self.zn_24_df)

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 2 HIGH/LOW FLOW VARIANCE WITH/WITHOUT SHUTDOWN    #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #   + ML 3 = High Flow Variance With Shutdown            #
            #   + ML 4 = Low Flow Variance Without Shutdown          #
            #                                                        #
            # ------------------------------------------------------ #

            # For each flow component with addresses 3 and 4 verify:
            for address in [3, 4]:

                # Programs
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_running()

                # Flow Sources
                self.config.BaseStation3200[1].water_sources[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].points_of_control[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].mainlines[address].statuses.verify_status_is_running()

                # Flow Devices
                self.config.BaseStation3200[1].master_valves[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].pumps[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_running()

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_soaking()

            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_soaking()

            # ---
            # NOTE:
            #
            # At this point, a Low Flow Variance would be detected on ML 4 if variance threshold was not 0%. The spec
            # says no variance event should be acted on in this case.
            #
            #   -> Thus, verify no message posted
            # ---

            self.config.BaseStation3200[1].mainlines[4].messages.check_for_low_flow_variance_detected_message_not_present()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ################################
    def step_23(self):
        """
        ############################
        Increment clock from 10:21 - 10:30 AM
        ############################

        --
        ML 1,5,7:
        --
            * Watering should continue for remaining zones on respective ML's 1, 5 and 7
            * Verify that the following zones are set to done after clearing their high flow shutdown messages the
              previous step.
                + ZN 2 (on ML 1)
                + ZN 14 (on ML 5)
                + ZN 20 (on ML 7)
            * Watering continues as previously, 1 minute cycle time left before remaining zones complete their total
              runtime and go to done.

            * Whats the controller doing:
                + Continuing to monitor flow variance, since we set the actual flow = expected flow, no variance
                  detected.

        --
        ML 6:
        --
            * Watering continues as expected.
            * First group of zones finish up last minute of their total runtime.
                -> Will transition to "Done" status from 10:30 - 10:31 AM minute processing
            * Second group of zones finish soak cycle and gets ready to water.
                -> Will transition to "Watering" status from 10:30 - 10:31 AM minute processing

            * Whats the controller doing:
                + Continuing to monitor flow variance, since we set the actual flow = expected flow, no variance
                  detected.

        --
        ML 8:
        --
            * Watering should continue for remaining zones on respective ML 8
            * Verify that the following zones are set to done after clearing their high flow shutdown messages the
              previous step.
                + ZN 23 (on ML 8)
            * Watering continues as previously, 1 minute cycle time left before remaining zones complete their total
              runtime and go to done.

            * Whats the controller doing:
                + Continuing to monitor flow variance, since we set the actual flow = expected flow, no variance
                  detected.

        --
        ML 2:
        --
            * First group of zones finish up last minute of their total runtime.
                -> Will transition to "Done" status from 10:30 - 10:31 AM minute processing
            * Second group of zones finish soak cycle and gets ready to water.
                -> Will transition to "Watering" status from 10:30 - 10:31 AM minute processing

            * Whats the controller doing:
                + Continuing to monitor flow variance, since we set the actual flow = expected flow, no variance
                  detected.

        --
        ML 3,4:
        --
            * Watering should continue, even with expected flow != actual flow, variance is set to 0%
            * First group of zones finish up last minute of their total runtime.
                -> Will transition to "Done" status from 10:30 - 10:31 AM minute processing
            * Second group of zones finish soak cycle and gets ready to water.
                -> Will transition to "Watering" status from 10:30 - 10:31 AM minute processing

            * Whats the controller doing:
                + Continuing to monitor flow variance, since we set the actual flow = expected flow, no variance
                  detected.
            * Verify ML 4 has no message posted for low variance detected even with expected != actual flow

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:21 - 10:30 AM                                       #
            #                                                        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].do_increment_clock(minutes=9)

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 1,3,4 HIGH FLOW VARIANCE WITH SHUTDOWN            #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # For each flow component with addresses 1, 5 and 7 verify:
            for address in [1, 5, 7]:

                # Programs
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_running()

                # Flow Sources
                self.config.BaseStation3200[1].water_sources[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].points_of_control[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].mainlines[address].statuses.verify_status_is_running()

                # Flow Devices
                self.config.BaseStation3200[1].master_valves[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].pumps[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_running()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()

            # Mainline 5 Zones
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_watering()

            # Mainline 7 Zones
            self.config.BaseStation3200[1].zones[19].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[20].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[21].statuses.verify_status_is_watering()

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 1 LOW FLOW VARIANCE WITHOUT SHUTDOWN              #
            #                                                        #
            #   + Mainlines have 3 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # Programs
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[2].statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_soaking()

            # ---
            # NOTE:
            #
            # At this point, current zone group is finishing up watering and the next zone group will start next step.
            #
            #   -> In order to not trigger messages on each minute, set FM 2's rate equal to next zone group's expected
            #      rate.
            # ---

            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[2],
                _expected_gpm=self.zn_6_df)

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 3 LOW FLOW VARIANCE WITHOUT SHUTDOWN              #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # Programs
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].water_sources[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[6].statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].master_valves[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()

            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_soaking()

            # ---
            # NOTE:
            #
            # At this point, the next group of zones will be turned on during the next min's processing. The next group
            # of zones will not have a variance event triggered.
            #
            #   -> Thus, set FM's 6 rate equal to the actual flow to not trigger a variance event.
            # ---

            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[6],
                _expected_gpm=self.zn_18_df)
            
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 4 LOW FLOW VARIANCE WITH SHUTDOWN                 #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # Programs
            self.config.BaseStation3200[1].programs[8].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].water_sources[8].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[8].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[8].statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].master_valves[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[8].statuses.verify_status_is_running()

            # Mainline 8 Zones
            self.config.BaseStation3200[1].zones[22].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[23].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[24].statuses.verify_status_is_watering()

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 2 HIGH/LOW FLOW VARIANCE WITH/WITHOUT SHUTDOWN    #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #   + ML 3 = High Flow Variance With Shutdown            #
            #   + ML 4 = Low Flow Variance Without Shutdown          #
            #                                                        #
            # ------------------------------------------------------ #

            # For each flow component with addresses 3 and 4 verify:
            for address in [3, 4]:

                # Programs
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_running()

                # Flow Sources
                self.config.BaseStation3200[1].water_sources[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].points_of_control[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].mainlines[address].statuses.verify_status_is_running()

                # Flow Devices
                self.config.BaseStation3200[1].master_valves[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].pumps[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_running()

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_soaking()

            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_soaking()

            # ---
            # NOTE:
            #
            # At this point, a Low Flow Variance would be detected on ML 4 if variance threshold was not 0%. The spec
            # says no variance event should be acted on in this case.
            #
            #   -> Thus, verify no message posted
            # ---

            self.config.BaseStation3200[1].mainlines[4].messages.check_for_low_flow_variance_detected_message_not_present()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ################################
    def step_24(self):
        """
        ############################
        Increment clock from 10:30 - 10:31 AM
        ############################

        --
        ML 1,5,7:
        --
            * Watering should continue for remaining zones on respective ML's 1, 5 and 7
            * Verify that the following zones are set to done after clearing their high flow shutdown messages the
              previous step.
                + ZN 2 (on ML 1)
                + ZN 14 (on ML 5)
                + ZN 20 (on ML 7)
            * Watering continues as previously, zones complete their total runtime, will go to "Done" from
              10:31 - 10:32 min processing.
            * Verify all flow components are done with no errors
            * Update FMs 1, 5 and 7's flow rate to 0 GPM so that the FMs status changes from running to "Ok" with the
              rest of the system

            * Whats the controller doing:
                + Continuing to monitor flow variance, since we set the actual flow = expected flow, no variance
                  detected.

        --
        ML 6:
        --
            * Watering continues as expected.
            * First group of zones transition to "Done" status
            * Second group of zones transition to "Watering"

            * Whats the controller doing:
                + Waits for 1 min pipe stabilization period to finish as well as takes FM reading to determine if
                  there is any flow variance.
                + Will act on variance (if found) during 10:31 - 10:32 min processing
                    -> in this case, we are not expecting variance

        --
        ML 8:
        --
            * Watering should continue for remaining zones on respective ML 8
            * Verify that the following zones are set to done after clearing their high flow shutdown messages the
              previous step.
                + ZN 23 (on ML 8)
            * Watering continues as previously, zones complete their total runtime, will go to "Done" from
              10:31 - 10:32 min processing.
            * Verify all flow components are done with no errors
            * Update FMs 8's flow rate to 0 GPM so that the FMs status changes from running to "Ok" with the
              rest of the system

            * Whats the controller doing:
                + Continuing to monitor flow variance, since we set the actual flow = expected flow, no variance
                  detected.

        --
        ML 2:
        --
            * First group of zones transition to "Done" status
            * Second group of zones transition to "Watering"

            * Whats the controller doing:
                + Start waiting for 1/3 min pipe stabilization period to finish as well as takes FM reading to determine
                  if there is any flow variance.
                + Will act on variance (if found) during 10:33 - 10:34 min processing (skipped verification at that
                  specific minute, but verified at the end that no message was present)
                    -> in this case, we are not expecting variance

        --
        ML 3,4:
        --
            * Watering should continue, even with expected flow != actual flow, variance is set to 0%
            * First group of zones transition to "Done" status
            * Second group of zones transition to "Watering"

            * Whats the controller doing:
                + Waits for 1 min pipe stabilization period to finish as well as takes FM reading to determine if
                  there is any flow variance.
                + Will act on variance (if found) during 10:31 - 10:32 min processing
                    -> in this case, we are not expecting variance
            * Verify ML 4 has no message posted for low variance detected even with expected != actual flow

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:30 - 10:31 AM                                       #
            #                                                        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 1,3,4 HIGH FLOW VARIANCE WITH SHUTDOWN            #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # For each flow component with addresses 1, 5 and 7 verify:
            for address in [1, 5, 7]:

                # Programs
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_running()

                # Flow Sources
                self.config.BaseStation3200[1].water_sources[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].points_of_control[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].mainlines[address].statuses.verify_status_is_running()

                # Flow Devices
                self.config.BaseStation3200[1].master_valves[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].pumps[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_running()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()

            # Mainline 5 Zones
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_watering()

            # Mainline 7 Zones
            self.config.BaseStation3200[1].zones[19].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[20].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[21].statuses.verify_status_is_watering()

            # # ---
            # # NOTE:
            # #
            # # At this point, Update FMs 1, 5 and 7's flow rate to 0 GPM so that the FMs status changes from running to
            # # "Ok" with the rest of the system
            # #
            # #   -> This means that we need to set FM 1, 5 and 7's rate to 0 GPM to reflect actual flow vs expected flow
            # #      and to get them to go into their "Ok" status.
            # # ---
            #
            # helper_methods.update_flow_to_not_trigger_variance(
            #     _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
            #     _expected_gpm=0)
            #
            # helper_methods.update_flow_to_not_trigger_variance(
            #     _flow_meter=self.config.BaseStation3200[1].flow_meters[5],
            #     _expected_gpm=0)
            #
            # helper_methods.update_flow_to_not_trigger_variance(
            #     _flow_meter=self.config.BaseStation3200[1].flow_meters[7],
            #     _expected_gpm=0)

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 1 LOW FLOW VARIANCE WITHOUT SHUTDOWN              #
            #                                                        #
            #   + Mainlines have 3 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # Programs
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[2].statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_watering()

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 3 LOW FLOW VARIANCE WITHOUT SHUTDOWN              #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # Programs
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].water_sources[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[6].statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].master_valves[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()

            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_watering()

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 4 LOW FLOW VARIANCE WITH SHUTDOWN                 #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # Programs
            self.config.BaseStation3200[1].programs[8].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].water_sources[8].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[8].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[8].statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].master_valves[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[8].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[8].statuses.verify_status_is_running()

            # Mainline 8 Zones
            self.config.BaseStation3200[1].zones[22].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[23].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[24].statuses.verify_status_is_watering()
            
            # # ---
            # # NOTE:
            # #
            # # At this point, Update FMs 8's flow rate to 0 GPM so that the FMs status changes from running to
            # # "Ok" with the rest of the system
            # #
            # #   -> This means that we need to set FM 8's rate to 0 GPM to reflect actual flow vs expected flow
            # #      and to get them to go into their "Ok" status.
            # # ---
            #
            # helper_methods.update_flow_to_not_trigger_variance(
            #     _flow_meter=self.config.BaseStation3200[1].flow_meters[8],
            #     _expected_gpm=0)

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 2 HIGH/LOW FLOW VARIANCE WITH/WITHOUT SHUTDOWN    #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #   + ML 3 = High Flow Variance With Shutdown            #
            #   + ML 4 = Low Flow Variance Without Shutdown          #
            #                                                        #
            # ------------------------------------------------------ #

            # For each flow component with addresses 3 and 4 verify:
            for address in [3, 4]:

                # Programs
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_running()

                # Flow Sources
                self.config.BaseStation3200[1].water_sources[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].points_of_control[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].mainlines[address].statuses.verify_status_is_running()

                # Flow Devices
                self.config.BaseStation3200[1].master_valves[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].pumps[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_running()

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_watering()

            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_watering()

            # ---
            # NOTE:
            #
            # At this point, a Low Flow Variance would be detected on ML 4 if variance threshold was not 0%. The spec
            # says no variance event should be acted on in this case.
            #
            #   -> Thus, verify no message posted
            # ---

            self.config.BaseStation3200[1].mainlines[4].messages.check_for_low_flow_variance_detected_message_not_present()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ################################
    def step_25(self):
        """
        ############################
        Increment clock from 10:31 - 10:32 AM
        ############################

        --
        ML 1,5,7:
        --
            * Watering should complete for these MLs
            * Verify that the following zones are set to done after clearing their high flow shutdown messages the
              previous step.
                + ZN 2 (on ML 1)
                + ZN 14 (on ML 5)
                + ZN 20 (on ML 7)
            * Verify all flow components are done with no errors

            * Whats the controller doing:
                + Program complete

        --
        ML 6:
        --
            * Watering continues as expected.
            * First group of zones remain in "Done" status
            * Second group of zones continue to water for remaining 8 minutes of runtime

            * Whats the controller doing:
                + No variance detected, water as normal

        --
        ML 8:
        --
            * Watering should complete for these MLs
            * Verify that the following zones are set to done after clearing their low flow shutdown messages the
              previous step.
                + ZN 23 (on ML 8)
            * Verify all flow components are done with no errors

            * Whats the controller doing:
                + Program complete

        --
        ML 2:
        --
            * First group of zones transition to "Done" status
            * Second group of zones transition to "Watering"

            * Whats the controller doing:
                + Start waiting for 2/3 min pipe stabilization period to finish as well as takes FM reading to determine
                  if there is any flow variance.
                + Will act on variance (if found) during 10:33 - 10:34 min processing (skipped verification at that
                  specific minute, but verified at the end that no message was present)
                    -> in this case, we are not expecting variance

        --
        ML 3,4:
        --
            * Watering should continue, even with expected flow != actual flow, variance is set to 0%
            * First group of zones remain in "Done" status
            * Second group of zones continue to water for remaining 8 minutes of runtime

            * Whats the controller doing:
                + No variance detected, water as normal
            * Verify ML 4 has no message posted for low variance detected even with expected != actual flow

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:31 - 10:32 AM                                       #
            #                                                        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 1,3,4 HIGH FLOW VARIANCE WITH SHUTDOWN            #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # ---
            # NOTE:
            #
            # At this point, Update FMs 1, 5 and 7's flow rate to 0 GPM so that the FMs status changes from running to
            # "Ok" with the rest of the system
            #
            #   -> This means that we need to set FM 1, 5 and 7's rate to 0 GPM to reflect actual flow vs expected flow
            #      and to get them to go into their "Ok" status.
            # ---

            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[1],
                _expected_gpm=0)

            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[5],
                _expected_gpm=0)

            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[7],
                _expected_gpm=0)

            # For each flow component with addresses 1, 5 and 7 verify:
            for address in [1, 5, 7]:

                # Programs
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_done()

                # Flow Sources
                self.config.BaseStation3200[1].water_sources[address].statuses.verify_status_is_ok()
                self.config.BaseStation3200[1].points_of_control[address].statuses.verify_status_is_off()
                self.config.BaseStation3200[1].mainlines[address].statuses.verify_status_is_off()

                # Flow Devices
                self.config.BaseStation3200[1].master_valves[address].statuses.verify_status_is_off()
                self.config.BaseStation3200[1].pumps[address].statuses.verify_status_is_off()
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_ok()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()

            # Mainline 5 Zones
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_done()

            # Mainline 7 Zones
            self.config.BaseStation3200[1].zones[19].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[20].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[21].statuses.verify_status_is_done()

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 1 LOW FLOW VARIANCE WITHOUT SHUTDOWN              #
            #                                                        #
            #   + Mainlines have 3 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # Programs
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[2].statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_watering()

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 3 LOW FLOW VARIANCE WITHOUT SHUTDOWN              #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # Programs
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].water_sources[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[6].statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].master_valves[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()

            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_watering()

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 4 LOW FLOW VARIANCE WITH SHUTDOWN                 #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # ---
            # NOTE:
            #
            # At this point, Update FMs 8's flow rate to 0 GPM so that the FMs status changes from running to
            # "Ok" with the rest of the system
            #
            #   -> This means that we need to set FM 8's rate to 0 GPM to reflect actual flow vs expected flow
            #      and to get them to go into their "Ok" status.
            # ---

            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[8],
                _expected_gpm=0)

            # Programs
            self.config.BaseStation3200[1].programs[8].statuses.verify_status_is_done()

            # Flow Sources
            self.config.BaseStation3200[1].water_sources[8].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].points_of_control[8].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[8].statuses.verify_status_is_off()

            # Flow Devices
            self.config.BaseStation3200[1].master_valves[8].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[8].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].flow_meters[8].statuses.verify_status_is_ok()

            # Mainline 8 Zones
            self.config.BaseStation3200[1].zones[22].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[23].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[24].statuses.verify_status_is_done()

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 2 HIGH/LOW FLOW VARIANCE WITH/WITHOUT SHUTDOWN    #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #   + ML 3 = High Flow Variance With Shutdown            #
            #   + ML 4 = Low Flow Variance Without Shutdown          #
            #                                                        #
            # ------------------------------------------------------ #

            # For each flow component with addresses 3 and 4 verify:
            for address in [3, 4]:

                # Programs
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_running()

                # Flow Sources
                self.config.BaseStation3200[1].water_sources[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].points_of_control[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].mainlines[address].statuses.verify_status_is_running()

                # Flow Devices
                self.config.BaseStation3200[1].master_valves[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].pumps[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_running()

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_watering()

            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_watering()

            # ---
            # NOTE:
            #
            # At this point, a Low Flow Variance would be detected on ML 4 if variance threshold was not 0%. The spec
            # says no variance event should be acted on in this case.
            #
            #   -> Thus, verify no message posted
            # ---

            self.config.BaseStation3200[1].mainlines[4].messages.check_for_low_flow_variance_detected_message_not_present()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ################################
    def step_26(self):
        """
        ############################
        Increment clock from 10:32 - 10:40 AM
        ############################

        --
        ML 1,5,7:
        --
            * Watering should complete for these MLs
            * Verify that the following zones are set to done after clearing their high flow shutdown messages the
              previous step.
                + ZN 2 (on ML 1)
                + ZN 14 (on ML 5)
                + ZN 20 (on ML 7)
            * Verify all flow components are done with no errors

            * Whats the controller doing:
                + Program complete

        --
        ML 6:
        --
            * Watering continues as expected.
            * First group of zones remain in "Done" status
            * Second group of zones finish remaining 8 minutes of runtime
            * Update FMs 6 and 8's flow rate to 0 GPM so that the FMs status changes from running to "Ok" with the
              rest of the system

            * Whats the controller doing:
                + No variance detected, water as normal

        --
        ML 8:
        --
            * Watering should complete for these MLs
            * Verify that the following zones are set to done after clearing their low flow shutdown messages the
              previous step.
                + ZN 23 (on ML 8)
            * Verify all flow components are done with no errors

            * Whats the controller doing:
                + Program complete

        --
        ML 2:
        --
            * First group of zones transition to "Done" status
            * Second group of zones finish remaining 8 minutes of runtime
            * Update FMs 2's flow rate to 0 GPM so that the FMs status changes from running to "Ok" with the
              rest of the system

            * Whats the controller doing:
                + Completed waiting for 3/3 min pipe stabilization period
                + Will act on variance (if found) during 10:33 - 10:34 min processing (skipped verification at that
                  specific minute, but verified at the end that no message was present)
                    -> in this case, we are not expecting variance

        --
        ML 3,4:
        --
            * Watering should continue, even with expected flow != actual flow, variance is set to 0%
            * First group of zones remain in "Done" status
            * Second group of zones finish remaining 8 minutes of runtime
            * Update FMs 3 and 4's flow rate to 0 GPM so that the FMs status changes from running to "Ok" with the
              rest of the system

            * Whats the controller doing:
                + No variance detected, water as normal
            * Verify ML 4 has no message posted for low variance detected even with expected != actual flow

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:32 - 10:40 AM                                       #
            #                                                        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].do_increment_clock(minutes=8)

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 1,3,4 HIGH FLOW VARIANCE WITH SHUTDOWN            #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # For each flow component with addresses 1, 5 and 7 verify:
            for address in [1, 5, 7]:

                # Programs
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_done()

                # Flow Sources
                self.config.BaseStation3200[1].water_sources[address].statuses.verify_status_is_ok()
                self.config.BaseStation3200[1].points_of_control[address].statuses.verify_status_is_off()
                self.config.BaseStation3200[1].mainlines[address].statuses.verify_status_is_off()

                # Flow Devices
                self.config.BaseStation3200[1].master_valves[address].statuses.verify_status_is_off()
                self.config.BaseStation3200[1].pumps[address].statuses.verify_status_is_off()
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_ok()

            # Mainline 1 Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()

            # Mainline 5 Zones
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_done()

            # Mainline 7 Zones
            self.config.BaseStation3200[1].zones[19].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[20].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[21].statuses.verify_status_is_done()

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 1 LOW FLOW VARIANCE WITHOUT SHUTDOWN              #
            #                                                        #
            #   + Mainlines have 3 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # Programs
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[2].statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()

            # Mainline 2 Zones
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_watering()

            # ---
            # NOTE:
            #
            # At this point, Update FMs 2's flow rate to 0 GPM so that the FMs status changes from running to "Ok" with 
            # the rest of the system
            #
            #   -> This means that we need to set FM 1's rate to 0 GPM to reflect actual flow vs expected flow and to 
            #      get them to go into their "Ok" status.
            # ---

            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[2],
                _expected_gpm=0)

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 3 LOW FLOW VARIANCE WITHOUT SHUTDOWN              #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # Programs
            self.config.BaseStation3200[1].programs[6].statuses.verify_status_is_running()

            # Flow Sources
            self.config.BaseStation3200[1].water_sources[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[6].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[6].statuses.verify_status_is_running()

            # Flow Devices
            self.config.BaseStation3200[1].master_valves[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].flow_meters[6].statuses.verify_status_is_running()

            # Mainline 6 Zones
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_watering()

            # ---
            # NOTE:
            #
            # At this point, Update FMs 6's flow rate to 0 GPM so that the FMs status changes from running to "Ok" 
            # with the rest of the system
            #
            #   -> This means that we need to set FM 6's rate to 0 GPM to reflect actual flow vs expected flow and 
            #      to get them to go into their "Ok" status.
            # ---

            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[6],
                _expected_gpm=0)
            
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 4 LOW FLOW VARIANCE WITH SHUTDOWN                 #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #                                                        #
            # ------------------------------------------------------ #

            # Programs
            self.config.BaseStation3200[1].programs[8].statuses.verify_status_is_done()

            # Flow Sources
            self.config.BaseStation3200[1].water_sources[8].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].points_of_control[8].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[8].statuses.verify_status_is_off()

            # Flow Devices
            self.config.BaseStation3200[1].master_valves[8].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[8].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].flow_meters[8].statuses.verify_status_is_ok()

            # Mainline 8 Zones
            self.config.BaseStation3200[1].zones[22].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[23].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[24].statuses.verify_status_is_done()

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 2 HIGH/LOW FLOW VARIANCE WITH/WITHOUT SHUTDOWN    #
            #                                                        #
            #   + Mainlines have 1 min pipe stabilization times      #
            #   + ML 3 = High Flow Variance With Shutdown            #
            #   + ML 4 = Low Flow Variance Without Shutdown          #
            #                                                        #
            # ------------------------------------------------------ #

            # For each flow component with addresses 3 and 4 verify:
            for address in [3, 4]:

                # Programs
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_running()

                # Flow Sources
                self.config.BaseStation3200[1].water_sources[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].points_of_control[address].statuses.verify_status_is_running()
                self.config.BaseStation3200[1].mainlines[address].statuses.verify_status_is_running()

                # Flow Devices
                self.config.BaseStation3200[1].master_valves[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].pumps[address].statuses.verify_status_is_watering()
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_running()

            # Mainline 3 Zones
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_watering()

            # Mainline 4 Zones
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_watering()

            # ---
            # NOTE:
            #
            # At this point, a Low Flow Variance would be detected on ML 4 if variance threshold was not 0%. The spec
            # says no variance event should be acted on in this case.
            #
            #   -> Thus, verify no message posted
            # ---

            self.config.BaseStation3200[1].mainlines[4].messages.check_for_low_flow_variance_detected_message_not_present()

            # ---
            # NOTE:
            #
            # At this point, Update FMs 3 and 4's flow rate to 0 GPM so that the FMs status changes from running to "Ok" 
            # with the rest of the system
            #
            #   -> This means that we need to set FM 3 and 4's rate to 0 GPM to reflect actual flow vs expected flow and 
            #      to get them to go into their "Ok" status.
            # ---

            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[3],
                _expected_gpm=0)

            helper_methods.update_flow_to_not_trigger_variance(
                _flow_meter=self.config.BaseStation3200[1].flow_meters[4],
                _expected_gpm=0)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ################################
    def step_27(self):
        """
        ############################
        Increment clock from 10:40 - 10:41 AM
        ############################
        
        --
        ML 1,3,5,7:
        --
            * Verify all flow components are done with no errors
            * Verify no high flow variance shutdown messages exist on controller

            * Whats the controller doing:
                + Program complete
                
        --
        ML 2,4,6:
        --
            * Verify all flow components are done with no errors
            * Verify no low flow variance detection messages exist on controller

            * Whats the controller doing:
                + Program complete
                
        --
        ML 8:
        --
            * Verify all flow components are done with no errors
            * Verify no low flow variance shutdown messages exist on controller

            * Whats the controller doing:
                + Program complete

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ------------------------------------------------------ #
            #                                                        #
            # 10:40 - 10:41 AM                                       #
            #                                                        #
            # ------------------------------------------------------ #

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 1,2,3,4 HIGH FLOW VARIANCE WITH SHUTDOWN          #
            #                                                        #
            # ------------------------------------------------------ #

            # For each flow component with addresses 1, 3, 5 and 7 verify:
            for address in [1, 3, 5, 7]:

                # Programs
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_done()

                # Flow Sources
                self.config.BaseStation3200[1].water_sources[address].statuses.verify_status_is_ok()
                self.config.BaseStation3200[1].points_of_control[address].statuses.verify_status_is_off()
                self.config.BaseStation3200[1].mainlines[address].statuses.verify_status_is_off()

                # Flow Devices
                self.config.BaseStation3200[1].master_valves[address].statuses.verify_status_is_off()
                self.config.BaseStation3200[1].pumps[address].statuses.verify_status_is_off()
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_ok()

            # Verify Mainline 1 Zones are "Done"
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()

            # Verify Mainline 3 Zones are "Done"
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[9].statuses.verify_status_is_done()

            # Verify Mainline 5 Zones are "Done"
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[15].statuses.verify_status_is_done()

            # Verify Mainline 7 Zones are "Done"
            self.config.BaseStation3200[1].zones[19].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[20].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[21].statuses.verify_status_is_done()
            
            # Verify no high variance shutdown messages exist
            self.config.BaseStation3200[1].programs[1].zone_programs[1].messages.check_for_shutdown_on_high_flow_variance_message_not_present()
            self.config.BaseStation3200[1].programs[1].zone_programs[2].messages.check_for_shutdown_on_high_flow_variance_message_not_present()
            self.config.BaseStation3200[1].programs[1].zone_programs[3].messages.check_for_shutdown_on_high_flow_variance_message_not_present()
            self.config.BaseStation3200[1].programs[3].zone_programs[7].messages.check_for_shutdown_on_high_flow_variance_message_not_present()
            self.config.BaseStation3200[1].programs[3].zone_programs[8].messages.check_for_shutdown_on_high_flow_variance_message_not_present()
            self.config.BaseStation3200[1].programs[3].zone_programs[9].messages.check_for_shutdown_on_high_flow_variance_message_not_present()
            self.config.BaseStation3200[1].programs[5].zone_programs[13].messages.check_for_shutdown_on_high_flow_variance_message_not_present()
            self.config.BaseStation3200[1].programs[5].zone_programs[14].messages.check_for_shutdown_on_high_flow_variance_message_not_present()
            self.config.BaseStation3200[1].programs[5].zone_programs[15].messages.check_for_shutdown_on_high_flow_variance_message_not_present()
            self.config.BaseStation3200[1].programs[7].zone_programs[19].messages.check_for_shutdown_on_high_flow_variance_message_not_present()
            self.config.BaseStation3200[1].programs[7].zone_programs[20].messages.check_for_shutdown_on_high_flow_variance_message_not_present()
            self.config.BaseStation3200[1].programs[7].zone_programs[21].messages.check_for_shutdown_on_high_flow_variance_message_not_present()

            # ------------------------------------------------------ #
            #                                                        #
            # TIER 1,2,3 LOW FLOW VARIANCE WITHOUT SHUTDOWN          #
            #                                                        #
            # ------------------------------------------------------ #

            # For each flow component with addresses 2, 4, 6 and 8 verify:
            for address in [2, 4, 6]:

                # Programs
                self.config.BaseStation3200[1].programs[address].statuses.verify_status_is_done()

                # Flow Sources
                self.config.BaseStation3200[1].water_sources[address].statuses.verify_status_is_ok()
                self.config.BaseStation3200[1].points_of_control[address].statuses.verify_status_is_off()
                self.config.BaseStation3200[1].mainlines[address].statuses.verify_status_is_off()

                # Flow Devices
                self.config.BaseStation3200[1].master_valves[address].statuses.verify_status_is_off()
                self.config.BaseStation3200[1].pumps[address].statuses.verify_status_is_off()
                self.config.BaseStation3200[1].flow_meters[address].statuses.verify_status_is_ok()

            # Verify Mainline 2 Zones are "Done"
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_done()

            # Verify Mainline 4 Zones are "Done"
            self.config.BaseStation3200[1].zones[10].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_done()

            # Verify Mainline 6 Zones are "Done"
            self.config.BaseStation3200[1].zones[16].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[17].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[18].statuses.verify_status_is_done()

            # Verify no low variance detection messages exist on the mainlines
            self.config.BaseStation3200[1].mainlines[2].messages.check_for_low_flow_variance_detected_message_not_present()
            self.config.BaseStation3200[1].mainlines[4].messages.check_for_low_flow_variance_detected_message_not_present()
            self.config.BaseStation3200[1].mainlines[6].messages.check_for_low_flow_variance_detected_message_not_present()
            
            # ------------------------------------------------------ #
            #                                                        #
            # TIER 4 LOW FLOW VARIANCE WITH SHUTDOWN                 #
            #                                                        #
            # ------------------------------------------------------ #

            # Programs
            self.config.BaseStation3200[1].programs[8].statuses.verify_status_is_done()

            # Flow Sources
            self.config.BaseStation3200[1].water_sources[8].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].points_of_control[8].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[8].statuses.verify_status_is_off()

            # Flow Devices
            self.config.BaseStation3200[1].master_valves[8].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[8].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].flow_meters[8].statuses.verify_status_is_ok()

            # Mainline 8 Zones
            self.config.BaseStation3200[1].zones[22].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[23].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[24].statuses.verify_status_is_done()
            
            # Verify no high variance shutdown messages exist
            self.config.BaseStation3200[1].programs[8].zone_programs[22].messages.check_for_shutdown_on_low_flow_variance_message_not_present()
            self.config.BaseStation3200[1].programs[8].zone_programs[23].messages.check_for_shutdown_on_low_flow_variance_message_not_present()
            self.config.BaseStation3200[1].programs[8].zone_programs[24].messages.check_for_shutdown_on_low_flow_variance_message_not_present()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
