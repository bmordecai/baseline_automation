import sys
from time import sleep
from datetime import timedelta, datetime
from decimal import Decimal

from common.configuration import Configuration
from common import object_factory
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# Bicoder imports
from common.objects.bicoders.valve_bicoder import ValveBicoder
from common.objects.bicoders.flow_bicoder import FlowBicoder
from common.objects.bicoders.pump_bicoder import PumpBicoder
from common.objects.bicoders.switch_bicoder import SwitchBicoder
from common.objects.bicoders.moisture_bicoder import MoistureBicoder
from common.objects.bicoders.temp_bicoder import TempBicoder
from common.objects.bicoders.analog_bicoder import AnalogBicoder

# Device imports
from common.objects.devices.zn import Zone
from common.objects.devices.mv import MasterValve
from common.objects.devices.pm import Pump
from common.objects.devices.ms import MoistureSensor
from common.objects.devices.fm import FlowMeter
from common.objects.devices.sw import EventSwitch
from common.objects.devices.ts import TemperatureSensor
from common.objects.devices.ps import PressureSensor

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

__author__ = "Eldin"


class ControllerUseCase52(object):
    """
    Test name: \n
        Search Assign Test Real-Devices \n
        
    Purpose: \n
        Purpose of this use case is to test basic search/assign/test functionality on BaseStation 3200 using
        real devices. \n
        
        This use case can be taken one step further to include verifying manual run operations.\n
    
    Coverage area: \n
        1. For each device:
            a. Search for device type \n
            b. Verify device found on two-wire search \n
            c. Can assign device an address \n
            d. Verify device assigned to address \n
            e. Un-assigns device \n
            f. Verify device un-assigned to address \n
            g. Can assign device an address \n
            h. Verify device assigned to address \n
            i. Can run a self test on device \n
            j. Can verify device test results for device \n
            
    Not Covered: \n
        1. Pressure Sensors
        2. Coach's Button
        

    Real-Devices to be configured by default for test: \n
        - Zones \n
            - TSD0001 (1) \n
            - TSE0011 (100) \n
            - TSQ0011 (200) \n

        - Master Valves \n
            - TSD0007 (1) \n
            - TSD0008 (8) \n

        - Temp Sensors \n
            - TAT0001 (1) \n
            - TAT0002 (8) \n

        - Moisture Sensors \n
            - SB01250 (1) \n
            - SB06300 (25) \n

        - Flow Meters \n
            - TWF0001 (1) \n
            - TWF0004 (8) \n

        - Pumps \n
            - TPR0011 (1) \n
            - TPR0012 (8) \n

        - Pressure Sensors (unable to do) \n

        - Event Switches \n
            - TPD0001 (1) \n
            - TPD0004 (8) \n
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)
            
    #################################
    def step_1(self):
        """
        ############################
        Configure Controller
        ############################
        
        Configure the controller to use real devices in real-time mode.
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseStation3200[1].turn_off_faux_io()
            self.config.BaseStation3200[1].set_sim_mode_to_off()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        Configure Zones
        ############################

        1. Create Zones
            - For each Zone:
                1) Create bicoder
                2) Create Zone
                3) Update bicoder attributes (with real values returned by controller) for verifying against
        2. Search and address Zones

        --------------------------------------------------
        | Address | Serial  | 2-Wire | Voltage | Current |
        --------------------------------------------------
        |    1    | TSD0001 | 0.9    | 28.2    | 0.20    |
        |   100   | TSE0011 | 1.2    | 28.6    | 0.21    |
        |   200   | TSQ0011 | 1.2    | 27.9    | 0.20    |
        --------------------------------------------------
        """
        helper_methods.print_method_name()
        try:
            # TODO - Configure Zones for test:
            #
            #   Steps:
            #       (1) Update test zone 1, 100 and 200's serial numbers to use
            #           (i.e., zone_1_serial_number, zone_100_serial_number, zone_200_serial_number)

            # Tim's values
            # zone_1_serial_number = 'D020260'
            # zone_100_serial_number = 'D020261'
            # zone_200_serial_number = 'D020262'
            zone_1_serial_number = 'TSD0001'
            zone_100_serial_number = 'TSE0011'
            zone_200_serial_number = 'TSQ0011'

            # ----------------
            # ZONE 1
            # ----------------

            zone_1_bicoder = ValveBicoder(
                _sn=zone_1_serial_number,
                _controller=self.config.BaseStation3200[1],
                _id=opcodes.zone,
                _decoder_type=opcodes.single_valve_decoder)
            zone_1 = Zone(
                _controller=self.config.BaseStation3200[1],
                _address=1,
                _valve_bicoder=zone_1_bicoder)

            self.config.BaseStation3200[1].valve_bicoders[zone_1_serial_number] = zone_1_bicoder
            self.config.BaseStation3200[1].zones[1] = zone_1

            # Update attributes with "real values" from the controller to verify against
            zone_1_bicoder.va = 0.20
            zone_1_bicoder.vv = 28.2
            zone_1_bicoder.vt = 0.9

            # Search for Zones
            self.config.BaseStation3200[1].do_search_for_zones()

            # Verify valve biCoder found on Two-Wire (device address = 0 expected)
            self.config.BaseStation3200[1].valve_bicoders[zone_1_serial_number].verify_device_address()

            # Address biCoder as Zone 1
            self.config.BaseStation3200[1].zones[1].set_address(_ad=1)

            # Verify address assigned to Zone 1
            self.config.BaseStation3200[1].zones[1].bicoder.verify_device_address()

            # Un-assign Zone 1
            self.config.BaseStation3200[1].zones[1].set_address(_ad=0)

            # Verify able to un-assign valve bicoder from an address
            self.config.BaseStation3200[1].valve_bicoders[zone_1_serial_number].verify_device_address()

            # Re-address biCoder as Zone 1
            self.config.BaseStation3200[1].zones[1].set_address(_ad=1)

            # Verify address assigned to Zone 1
            self.config.BaseStation3200[1].zones[1].bicoder.verify_device_address()

            # Do self test on Zone 1
            self.config.BaseStation3200[1].zones[1].bicoder.do_self_test()

            # Verify Zone 1 self test results
            zone_1_data = self.config.BaseStation3200[1].zones[1].bicoder.get_data()
            self.config.BaseStation3200[1].zones[1].bicoder.verify_solenoid_current(_data=zone_1_data)
            self.config.BaseStation3200[1].zones[1].bicoder.verify_solenoid_voltage(_data=zone_1_data)
            self.config.BaseStation3200[1].zones[1].bicoder.verify_two_wire_drop_value(_data=zone_1_data)

            # ----------------
            # ZONE 100
            # ----------------

            zone_100_bicoder = ValveBicoder(
                _sn=zone_100_serial_number,
                _controller=self.config.BaseStation3200[1],
                _id=opcodes.zone,
                _decoder_type=opcodes.single_valve_decoder)
            zone_100 = Zone(
                _controller=self.config.BaseStation3200[1],
                _address=100,
                _valve_bicoder=zone_100_bicoder)

            self.config.BaseStation3200[1].valve_bicoders[zone_100_serial_number] = zone_100_bicoder
            self.config.BaseStation3200[1].zones[100] = zone_100

            # Update attributes with "real values" from the controller to verify against
            zone_100_bicoder.va = 0.21
            zone_100_bicoder.vv = 28.6
            zone_100_bicoder.vt = 1.2

            # Search for Zones
            self.config.BaseStation3200[1].do_search_for_zones()

            # Verify valve biCoder found on Two-Wire (device address = 0 expected)
            self.config.BaseStation3200[1].valve_bicoders[zone_100_serial_number].verify_device_address()

            # Address biCoder as Zone 100
            self.config.BaseStation3200[1].zones[100].set_address(_ad=100)

            # Verify address assigned to Zone 100
            self.config.BaseStation3200[1].zones[100].bicoder.verify_device_address()

            # Un-assign Zone 100
            self.config.BaseStation3200[1].zones[100].set_address(_ad=0)

            # Verify able to un-assign valve bicoder from an address
            self.config.BaseStation3200[1].valve_bicoders[zone_100_serial_number].verify_device_address()

            # Re-address biCoder as Zone 100
            self.config.BaseStation3200[1].zones[100].set_address(_ad=100)

            # Verify address assigned to Zone 100
            self.config.BaseStation3200[1].zones[100].bicoder.verify_device_address()

            # Do self test on Zone 100
            self.config.BaseStation3200[1].zones[100].bicoder.do_self_test()

            # Verify Zone 100 self test results
            zone_100_data = self.config.BaseStation3200[1].zones[100].bicoder.get_data()
            self.config.BaseStation3200[1].zones[100].bicoder.verify_solenoid_current(_data=zone_100_data)
            self.config.BaseStation3200[1].zones[100].bicoder.verify_solenoid_voltage(_data=zone_100_data)
            self.config.BaseStation3200[1].zones[100].bicoder.verify_two_wire_drop_value(_data=zone_100_data)

            # ----------------
            # ZONE 200
            # ----------------

            zone_200_bicoder = ValveBicoder(
                _sn=zone_200_serial_number,
                _controller=self.config.BaseStation3200[1],
                _id=opcodes.zone,
                _decoder_type=opcodes.single_valve_decoder)
            zone_200 = Zone(
                _controller=self.config.BaseStation3200[1],
                _address=200,
                _valve_bicoder=zone_200_bicoder)

            self.config.BaseStation3200[1].valve_bicoders[zone_200_serial_number] = zone_200_bicoder
            self.config.BaseStation3200[1].zones[200] = zone_200

            # Update attributes with "real values" from the controller to verify against
            zone_200_bicoder.va = 0.20
            zone_200_bicoder.vv = 27.9
            zone_200_bicoder.vt = 1.2

            # Search for Zones
            self.config.BaseStation3200[1].do_search_for_zones()

            # Verify valve biCoder found on Two-Wire (device address = 0 expected)
            self.config.BaseStation3200[1].valve_bicoders[zone_200_serial_number].verify_device_address()

            # Address biCoder as Zone 200
            self.config.BaseStation3200[1].zones[200].set_address(_ad=200)

            # Verify address assigned to Zone 200
            self.config.BaseStation3200[1].zones[200].bicoder.verify_device_address()

            # Un-assign Zone 200
            self.config.BaseStation3200[1].zones[200].set_address(_ad=0)

            # Verify able to un-assign valve bicoder from an address
            self.config.BaseStation3200[1].valve_bicoders[zone_200_serial_number].verify_device_address()

            # Re-address biCoder as Zone 200
            self.config.BaseStation3200[1].zones[200].set_address(_ad=200)

            # Verify address assigned to Zone 200
            self.config.BaseStation3200[1].zones[200].bicoder.verify_device_address()

            # Do self test on Zone 200
            self.config.BaseStation3200[1].zones[200].bicoder.do_self_test()

            # Verify Zone 200 self test results
            zone_200_data = self.config.BaseStation3200[1].zones[200].bicoder.get_data()
            self.config.BaseStation3200[1].zones[200].bicoder.verify_solenoid_current(_data=zone_200_data)
            self.config.BaseStation3200[1].zones[200].bicoder.verify_solenoid_voltage(_data=zone_200_data)
            self.config.BaseStation3200[1].zones[200].bicoder.verify_two_wire_drop_value(_data=zone_200_data)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        Configure Master Valves
        ############################

        1. Create Master Valves
            - For each Master Valve:
                1) Create bicoder
                2) Create Master Valve
                3) Update bicoder attributes (with real values returned by controller) for verifying against
        2. Search and address Master Valves

        --------------------------------------------------
        | Address | Serial  | 2-Wire | Voltage | Current |
        --------------------------------------------------
        |    1    | TSD0007 | 1.2    | 28.8    | 0.21    |
        |    8    | TSD0008 | 1.2    | 28.5    | 0.21    |
        --------------------------------------------------
        """
        helper_methods.print_method_name()
        try:
            # TODO - Configure Master Valves for test:
            #
            #   Steps:
            #       (1) Update test master valve 1 & 8's serial numbers to use
            #           (i.e., mv_1_serial_number, mv_8_serial_number)

            # Tim's values
            # mv_1_serial_number = 'D004048'
            # mv_8_serial_number = 'D010039'
            mv_1_serial_number = 'TSD0007'
            mv_8_serial_number = 'TSD0008'

            # ----------------
            # MASTER VALVE 1
            # ----------------

            mv_1_bicoder = ValveBicoder(
                _sn=mv_1_serial_number,
                _controller=self.config.BaseStation3200[1],
                _id=opcodes.master_valve,
                _decoder_type=opcodes.single_valve_decoder)
            mv_1 = MasterValve(
                _controller=self.config.BaseStation3200[1],
                _address=1,
                _valve_bicoder=mv_1_bicoder)

            self.config.BaseStation3200[1].valve_bicoders[mv_1_serial_number] = mv_1_bicoder
            self.config.BaseStation3200[1].master_valves[1] = mv_1

            # Update attributes with "real values" from the controller to verify against
            mv_1_bicoder.va = 0.21
            mv_1_bicoder.vv = 28.8
            mv_1_bicoder.vt = 1.2

            # Search for Master Valves
            self.config.BaseStation3200[1].do_search_for_master_valves()

            # Verify valve biCoder found on Two-Wire (device address = 0 expected)
            self.config.BaseStation3200[1].valve_bicoders[mv_1_serial_number].verify_device_address()

            # Address biCoder as Master Valve 1
            self.config.BaseStation3200[1].master_valves[1].set_address(_ad=1)

            # Verify address assigned to Master Valve 1
            self.config.BaseStation3200[1].master_valves[1].bicoder.verify_device_address()

            # Un-assign Master Valve 1
            self.config.BaseStation3200[1].master_valves[1].set_address(_ad=0)

            # Verify able to un-assign valve bicoder from an address
            self.config.BaseStation3200[1].valve_bicoders[mv_1_serial_number].verify_device_address()

            # Re-address biCoder as Master Valve 1
            self.config.BaseStation3200[1].master_valves[1].set_address(_ad=1)

            # Verify address assigned to Master Valve 1
            self.config.BaseStation3200[1].master_valves[1].bicoder.verify_device_address()

            # Do self test on Master Valve 1
            self.config.BaseStation3200[1].master_valves[1].bicoder.do_self_test()

            # Verify Master Valve 1 self test results
            mv_1_data = self.config.BaseStation3200[1].master_valves[1].bicoder.get_data()
            self.config.BaseStation3200[1].master_valves[1].bicoder.verify_solenoid_current(_data=mv_1_data)
            self.config.BaseStation3200[1].master_valves[1].bicoder.verify_solenoid_voltage(_data=mv_1_data)
            self.config.BaseStation3200[1].master_valves[1].bicoder.verify_two_wire_drop_value(_data=mv_1_data)

            # ----------------
            # MASTER VALVE 8
            # ----------------

            mv_8_bicoder = ValveBicoder(
                _sn=mv_8_serial_number,
                _controller=self.config.BaseStation3200[1],
                _id=opcodes.master_valve,
                _decoder_type=opcodes.single_valve_decoder)
            mv_8 = MasterValve(
                _controller=self.config.BaseStation3200[1],
                _address=8,
                _valve_bicoder=mv_8_bicoder)

            self.config.BaseStation3200[1].valve_bicoders[mv_8_serial_number] = mv_8_bicoder
            self.config.BaseStation3200[1].master_valves[8] = mv_8

            # Update attributes with "real values" from the controller to verify against
            mv_8_bicoder.va = 0.21
            mv_8_bicoder.vv = 28.5
            mv_8_bicoder.vt = 1.2

            # Search for Master Valves
            self.config.BaseStation3200[1].do_search_for_master_valves()

            # Verify valve biCoder found on Two-Wire (device address = 0 expected)
            self.config.BaseStation3200[1].valve_bicoders[mv_8_serial_number].verify_device_address()

            # Address biCoder as Master Valve 8
            self.config.BaseStation3200[1].master_valves[8].set_address(_ad=8)

            # Verify address assigned to Master Valve 8
            self.config.BaseStation3200[1].master_valves[8].bicoder.verify_device_address()

            # Un-assign Master Valve 8
            self.config.BaseStation3200[1].master_valves[8].set_address(_ad=0)

            # Verify able to un-assign valve bicoder from an address
            self.config.BaseStation3200[1].valve_bicoders[mv_8_serial_number].verify_device_address()

            # Re-address biCoder as Master Valve 8
            self.config.BaseStation3200[1].master_valves[8].set_address(_ad=8)

            # Verify address assigned to Master Valve 8
            self.config.BaseStation3200[1].master_valves[8].bicoder.verify_device_address()

            # Do self test on Master Valve 8
            self.config.BaseStation3200[1].master_valves[8].bicoder.do_self_test()

            # Verify Master Valve 8 self test results
            mv_8_data = self.config.BaseStation3200[1].master_valves[8].bicoder.get_data()
            self.config.BaseStation3200[1].master_valves[8].bicoder.verify_solenoid_current(_data=mv_8_data)
            self.config.BaseStation3200[1].master_valves[8].bicoder.verify_solenoid_voltage(_data=mv_8_data)
            self.config.BaseStation3200[1].master_valves[8].bicoder.verify_two_wire_drop_value(_data=mv_8_data)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        Configure Temperature Sensors
        ############################

        1. Create Temperature Sensors
            - For each Temperature Sensor:
                1) Create bicoder
                2) Create Temperature Sensor
                3) Update bicoder attributes (with real values returned by controller) for verifying against
        2. Search and address Temperature Sensors

        -----------------------------------------------
        | Address | Serial  | 2-Wire | Temp (degrees) |
        -----------------------------------------------
        |    1    | TAT0001 | 0.9    | 74.0           |
        |    8    | TAT0002 | 0.9    | 73.8           |
        -----------------------------------------------
        """
        helper_methods.print_method_name()
        try:
            # TODO - Configure Temperature Sensors for test:
            #
            #   Steps:
            #       (1) Update test temp sensor 1 & 8's serial numbers to use
            #           (i.e., ts_1_serial_number, ts_8_serial_number)

            # Tim's values
            # ts_1_serial_number = '00T1053'
            # ts_8_serial_number = 'AT01093'
            ts_1_serial_number = 'TAT0001'
            ts_8_serial_number = 'TAT0002'

            # ----------------
            # TEMP SENSOR 1
            # ----------------

            ts_1_bicoder = TempBicoder(
                _sn=ts_1_serial_number,
                _controller=self.config.BaseStation3200[1])
            ts_1 = TemperatureSensor(
                _controller=self.config.BaseStation3200[1],
                _address=1,
                _temp_bicoder=ts_1_bicoder)

            self.config.BaseStation3200[1].temperature_bicoders[ts_1_serial_number] = ts_1_bicoder
            self.config.BaseStation3200[1].temperature_sensors[1] = ts_1

            # Update attributes with "real values" from the controller to verify against
            ts_1_bicoder.vd = 74.0
            ts_1_bicoder.vt = 0.9

            # Search for Temperature Sensors
            self.config.BaseStation3200[1].do_search_for_temperature_sensor()

            # Verify temp biCoder found on Two-Wire (device address = 0 expected)
            self.config.BaseStation3200[1].temperature_bicoders[ts_1_serial_number].verify_device_address()

            # Address biCoder as Temperature Sensor 1
            self.config.BaseStation3200[1].temperature_sensors[1].set_address(_ad=1)

            # Verify address assigned to Temperature Sensor 1
            self.config.BaseStation3200[1].temperature_sensors[1].bicoder.verify_device_address()

            # Un-assign Temperature Sensor 1
            self.config.BaseStation3200[1].temperature_sensors[1].set_address(_ad=0)

            # Verify able to un-assign temp bicoder from an address
            self.config.BaseStation3200[1].temperature_bicoders[ts_1_serial_number].verify_device_address()

            # Re-address biCoder as Temperature Sensor 1
            self.config.BaseStation3200[1].temperature_sensors[1].set_address(_ad=1)

            # Verify address assigned to Temperature Sensor 1
            self.config.BaseStation3200[1].temperature_sensors[1].bicoder.verify_device_address()

            # Do self test on Temperature Sensor 1
            self.config.BaseStation3200[1].temperature_sensors[1].bicoder.do_self_test()

            # Verify Temperature Sensor 1 self test results
            ts_1_data = self.config.BaseStation3200[1].temperature_sensors[1].bicoder.get_data()
            self.config.BaseStation3200[1].temperature_sensors[1].bicoder.verify_temperature_reading(_data=ts_1_data)
            self.config.BaseStation3200[1].temperature_sensors[1].bicoder.verify_two_wire_drop_value(_data=ts_1_data)

            # ----------------
            # TEMP SENSOR 8
            # ----------------

            ts_8_bicoder = TempBicoder(
                _sn=ts_8_serial_number,
                _controller=self.config.BaseStation3200[1])
            ts_8 = TemperatureSensor(
                _controller=self.config.BaseStation3200[1],
                _address=8,
                _temp_bicoder=ts_8_bicoder)

            self.config.BaseStation3200[1].temperature_bicoders[ts_8_serial_number] = ts_8_bicoder
            self.config.BaseStation3200[1].temperature_sensors[8] = ts_8

            # Update attributes with "real values" from the controller to verify against
            ts_8_bicoder.vd = 73.8
            ts_8_bicoder.vt = 0.9

            # Search for Temperature Sensors
            self.config.BaseStation3200[1].do_search_for_temperature_sensor()

            # Verify temp biCoder found on Two-Wire (device address = 0 expected)
            self.config.BaseStation3200[1].temperature_bicoders[ts_8_serial_number].verify_device_address()

            # Address biCoder as Temperature Sensor 8
            self.config.BaseStation3200[1].temperature_sensors[8].set_address(_ad=8)

            # Verify address assigned to Temperature Sensor 8
            self.config.BaseStation3200[1].temperature_sensors[8].bicoder.verify_device_address()

            # Un-assign Temperature Sensor 8
            self.config.BaseStation3200[1].temperature_sensors[8].set_address(_ad=0)

            # Verify able to un-assign temp bicoder from an address
            self.config.BaseStation3200[1].temperature_bicoders[ts_8_serial_number].verify_device_address()

            # Re-address biCoder as Temperature Sensor 8
            self.config.BaseStation3200[1].temperature_sensors[8].set_address(_ad=8)

            # Verify address assigned to Temperature Sensor 8
            self.config.BaseStation3200[1].temperature_sensors[8].bicoder.verify_device_address()

            # Do self test on Temperature Sensor 8
            self.config.BaseStation3200[1].temperature_sensors[8].bicoder.do_self_test()

            # Verify Temperature Sensor 8 self test results
            ts_8_data = self.config.BaseStation3200[1].temperature_sensors[8].bicoder.get_data()
            self.config.BaseStation3200[1].temperature_sensors[8].bicoder.verify_temperature_reading(_data=ts_8_data)
            self.config.BaseStation3200[1].temperature_sensors[8].bicoder.verify_two_wire_drop_value(_data=ts_8_data)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        Configure Moisture Sensors
        ############################

        1. Create Moisture Sensors
            - For each Moisture Sensor:
                1) Create bicoder
                2) Create Moisture Sensor
                3) Update bicoder attributes (with real values returned by controller) for verifying against
        2. Search and address Moisture Sensors

        -------------------------------------------------------------------
        | Address | Serial  | 2-Wire | Moisture % | Temperature (degrees) |
        -------------------------------------------------------------------
        |    1    | SB01250 | 0.9    | 0.0        | 75.2                  |
        |   25    | SB06300 | 1.2    | 2.4        | 75.2                  |
        -------------------------------------------------------------------
        """
        helper_methods.print_method_name()
        try:
            # TODO - Configure Moisture Sensors for test:
            #
            #   Steps:
            #       (1) Update test moisture sensor 1 & 25's serial numbers to use
            #           (i.e., ms_1_serial_number, ms_25_serial_number)

            # Tim's values
            # ms_1_serial_number = 'SB12080'
            # ms_25_serial_number = 'ST00002'
            ms_1_serial_number = 'SB01250'
            ms_25_serial_number = 'SB06300'

            # ----------------
            # MOISTURE SENSOR 1
            # ----------------

            ms_1_bicoder = MoistureBicoder(
                _sn=ms_1_serial_number,
                _controller=self.config.BaseStation3200[1])
            ms_1 = MoistureSensor(
                _controller=self.config.BaseStation3200[1],
                _address=1,
                _moisture_bicoder=ms_1_bicoder)

            self.config.BaseStation3200[1].moisture_bicoders[ms_1_serial_number] = ms_1_bicoder
            self.config.BaseStation3200[1].moisture_sensors[1] = ms_1

            # Update attributes with "real values" from the controller to verify against
            ms_1_bicoder.vp = 0.0
            ms_1_bicoder.vd = 75.2
            ms_1_bicoder.vt = 0.9

            # Search for Moisture Sensors
            self.config.BaseStation3200[1].do_search_for_moisture_sensor()

            # Verify moisture biCoder found on Two-Wire (device address = 0 expected)
            self.config.BaseStation3200[1].moisture_bicoders[ms_1_serial_number].verify_device_address()

            # Address biCoder as Moisture Sensor 1
            self.config.BaseStation3200[1].moisture_sensors[1].set_address(_ad=1)

            # Verify address assigned to Moisture Sensor 1
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.verify_device_address()

            # Un-assign Moisture Sensor 1
            self.config.BaseStation3200[1].moisture_sensors[1].set_address(_ad=0)

            # Verify able to un-assign moisture bicoder from an address
            self.config.BaseStation3200[1].moisture_bicoders[ms_1_serial_number].verify_device_address()

            # Re-address biCoder as Moisture Sensor 1
            self.config.BaseStation3200[1].moisture_sensors[1].set_address(_ad=1)

            # Verify address assigned to Moisture Sensor 1
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.verify_device_address()

            # Do self test on Moisture Sensor 1
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.do_self_test()

            # Verify Moisture Sensor 1 self test results
            ms_1_data = self.config.BaseStation3200[1].moisture_sensors[1].bicoder.get_data()
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.verify_moisture_percent(_data=ms_1_data)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.verify_temperature_reading(_data=ms_1_data)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.verify_two_wire_drop_value(_data=ms_1_data)

            # ----------------
            # MOISTURE SENSOR 25
            # ----------------

            ms_25_bicoder = MoistureBicoder(
                _sn=ms_25_serial_number,
                _controller=self.config.BaseStation3200[1])
            ms_25 = MoistureSensor(
                _controller=self.config.BaseStation3200[1],
                _address=25,
                _moisture_bicoder=ms_25_bicoder)

            self.config.BaseStation3200[1].moisture_bicoders[ms_25_serial_number] = ms_25_bicoder
            self.config.BaseStation3200[1].moisture_sensors[25] = ms_25

            # Update attributes with "real values" from the controller to verify against
            ms_25_bicoder.vp = 2.4
            ms_25_bicoder.vd = 75.2
            ms_25_bicoder.vt = 1.2

            # Search for Moisture Sensors
            self.config.BaseStation3200[1].do_search_for_moisture_sensor()

            # Verify moisture biCoder found on Two-Wire (device address = 0 expected)
            self.config.BaseStation3200[1].moisture_bicoders[ms_25_serial_number].verify_device_address()

            # Address biCoder as Moisture Sensor 25
            self.config.BaseStation3200[1].moisture_sensors[25].set_address(_ad=25)

            # Verify address assigned to Moisture Sensor 25
            self.config.BaseStation3200[1].moisture_sensors[25].bicoder.verify_device_address()

            # Un-assign Moisture Sensor 25
            self.config.BaseStation3200[1].moisture_sensors[25].set_address(_ad=0)

            # Verify able to un-assign moisture bicoder from an address
            self.config.BaseStation3200[1].moisture_bicoders[ms_25_serial_number].verify_device_address()

            # Re-address biCoder as Moisture Sensor 25
            self.config.BaseStation3200[1].moisture_sensors[25].set_address(_ad=25)

            # Verify address assigned to Moisture Sensor 25
            self.config.BaseStation3200[1].moisture_sensors[25].bicoder.verify_device_address()

            # Do self test on Moisture Sensor 25
            self.config.BaseStation3200[1].moisture_sensors[25].bicoder.do_self_test()

            # Verify Moisture Sensor 25 self test results
            ms_25_data = self.config.BaseStation3200[1].moisture_sensors[25].bicoder.get_data()
            self.config.BaseStation3200[1].moisture_sensors[25].bicoder.verify_moisture_percent(_data=ms_25_data)
            self.config.BaseStation3200[1].moisture_sensors[25].bicoder.verify_temperature_reading(_data=ms_25_data)
            self.config.BaseStation3200[1].moisture_sensors[25].bicoder.verify_two_wire_drop_value(_data=ms_25_data)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ############################
        Configure Flow Meters
        ############################

        1. Create Flow Meters
            - For each Flow Meter:
                1) Create bicoder
                2) Create Flow Meter
                3) Update bicoder attributes (with real values returned by controller) for verifying against
        2. Search and address Flow Meters

        ---------------------------------------------
        | Address | Serial  | 2-Wire | Rate | Usage |
        ---------------------------------------------
        |    1    | TWF0001 | 0.8    | 0.2  | ~~~   |
        |    8    | TWF0004 | 0.9    | 0.0  | ~~~   |
        ---------------------------------------------
        """
        helper_methods.print_method_name()
        try:
            # TODO - Configure Flow Meters for test:
            #
            #   Steps:
            #       (1) Update test flow meter 1 & 8's serial numbers to use (fm_1_serial_number, fm_8_serial_number)
            #       (2) Update test flow meter 1 flow simulator settings:
            #           (2.a) If flow meter is using flow simulator with a dial (i.e., positions 1-9), set dial
            #                 position to 3 (simulates ~20.0 GPM flow)
            #           (2.b) If flow meter is using a digital flow simulator (i.e., a digital number is visible),
            #                 set to 24 (simulates ~20.0 GPM flow)
            #       (3) Update test flow meter 8 flow simulator settings:
            #           (3.a) If flow meter is using flow simulator with a dial (i.e., positions 1-9), set dial
            #                 position to 5 (simulates ~100.0 GPM flow)
            #           (3.b) If flow meter is using a digital flow simulator (i.e., a digital number is visible),
            #                 set to 71 (simulates ~60.0 GPM flow)

            # Tim's values
            # fm_1_serial_number = 'WMV0705'
            # fm_8_serial_number = 'WF00388'
            fm_1_serial_number = 'TWF0001'
            fm_8_serial_number = 'TWF0004'

            # ----------------
            # FLOW METER 1
            # ----------------

            fm_1_bicoder = FlowBicoder(
                _sn=fm_1_serial_number,
                _controller=self.config.BaseStation3200[1])
            fm_1 = FlowMeter(
                _controller=self.config.BaseStation3200[1],
                _address=1,
                _flow_bicoder=fm_1_bicoder)

            self.config.BaseStation3200[1].flow_bicoders[fm_1_serial_number] = fm_1_bicoder
            self.config.BaseStation3200[1].flow_meters[1] = fm_1

            # Update attributes with "real values" from the controller to verify against
            fm_1_bicoder.vr = 20.0
            fm_1_bicoder.vg = 1.0
            fm_1_bicoder.vt = 0.8

            # Search for Flow Meters
            self.config.BaseStation3200[1].do_search_for_flow_meters()

            # Verify flow biCoder found on Two-Wire (device address = 0 expected)
            self.config.BaseStation3200[1].flow_bicoders[fm_1_serial_number].verify_device_address()

            # Address biCoder as Flow Meter 1
            self.config.BaseStation3200[1].flow_meters[1].set_address(_ad=1)

            # Verify address assigned to Flow Meter 1
            self.config.BaseStation3200[1].flow_meters[1].bicoder.verify_device_address()

            # Un-assign Flow Meter 1
            self.config.BaseStation3200[1].flow_meters[1].set_address(_ad=0)

            # Verify able to un-assign flow bicoder from an address
            self.config.BaseStation3200[1].flow_bicoders[fm_1_serial_number].verify_device_address()

            # Re-address biCoder as Flow Meter 1
            self.config.BaseStation3200[1].flow_meters[1].set_address(_ad=1)

            # Verify address assigned to Flow Meter 1
            self.config.BaseStation3200[1].flow_meters[1].bicoder.verify_device_address()
            time.sleep(65)

            # Do self test on Flow Meter 1
            self.config.BaseStation3200[1].flow_meters[1].bicoder.do_self_test()

            # Verify Flow Meter 1 self test results
            fm_1_data = self.config.BaseStation3200[1].flow_meters[1].bicoder.get_data()

            self.config.BaseStation3200[1].flow_meters[1].bicoder.verify_flow_rate(_data=fm_1_data)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.verify_water_usage(_data=fm_1_data)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.verify_two_wire_drop_value(_data=fm_1_data)
        
            # ----------------
            # FLOW METER 8
            # ----------------
            
            fm_8_bicoder = FlowBicoder(
                _sn=fm_8_serial_number,
                _controller=self.config.BaseStation3200[1])
            fm_8 = FlowMeter(
                _controller=self.config.BaseStation3200[1],
                _address=8,
                _flow_bicoder=fm_8_bicoder)

            self.config.BaseStation3200[1].flow_bicoders[fm_8_serial_number] = fm_8_bicoder
            self.config.BaseStation3200[1].flow_meters[8] = fm_8

            # Update attributes with "real values" from the controller to verify against
            fm_8_bicoder.vr = 60.0
            fm_8_bicoder.vg = 1.0
            fm_8_bicoder.vt = 0.9

            # Search for Flow Meters
            self.config.BaseStation3200[1].do_search_for_flow_meters()

            # Verify flow biCoder found on Two-Wire (device address = 0 expected)
            self.config.BaseStation3200[1].flow_bicoders[fm_8_serial_number].verify_device_address()

            # Address biCoder as Flow Meter 8
            self.config.BaseStation3200[1].flow_meters[8].set_address(_ad=8)

            # Verify address assigned to Flow Meter 8
            self.config.BaseStation3200[1].flow_meters[8].bicoder.verify_device_address()

            # Un-assign Flow Meter 8
            self.config.BaseStation3200[1].flow_meters[8].set_address(_ad=0)

            # Verify able to un-assign flow bicoder from an address
            self.config.BaseStation3200[1].flow_bicoders[fm_8_serial_number].verify_device_address()

            # Re-address biCoder as Flow Meter 8
            self.config.BaseStation3200[1].flow_meters[8].set_address(_ad=8)

            # Verify address assigned to Flow Meter 8
            self.config.BaseStation3200[1].flow_meters[8].bicoder.verify_device_address()
            time.sleep(65)
            # Do self test on Flow Meter 8
            self.config.BaseStation3200[1].flow_meters[8].bicoder.do_self_test()

            # Verify Flow Meter 1 self test results
            fm_8_data = self.config.BaseStation3200[1].flow_meters[8].bicoder.get_data()

            self.config.BaseStation3200[1].flow_meters[8].bicoder.verify_flow_rate(_data=fm_8_data)
            self.config.BaseStation3200[1].flow_meters[8].bicoder.verify_water_usage(_data=fm_8_data)
            self.config.BaseStation3200[1].flow_meters[8].bicoder.verify_two_wire_drop_value(_data=fm_8_data)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ############################
        Configure Pumps
        ############################

        1. Create Pumps
            - For each Pump:
                1) Create bicoder
                2) Create Pump
                3) Update bicoder attributes (with real values returned by controller) for verifying against
        2. Search and address Pumps

        --------------------------------------------------
        | Address | Serial  | 2-Wire | Voltage | Current |
        --------------------------------------------------
        |    1    | TPR0011 | 1.2    | 28.9    | 0.21    |
        |    8    | TPR0012 | 0.9    | 28.6    | 0.14    |
        --------------------------------------------------
        """
        helper_methods.print_method_name()
        try:
            # TODO - Configure Pumps for test:
            #
            #   Steps:
            #       (1) Update test pump 1 & 8's serial numbers to use
            #           (i.e., pump_1_serial_number, pump_8_serial_number)

            # Tim's values
            # pump_1_serial_number = 'E007631'
            # pump_8_serial_number = 'E007632'
            pump_1_serial_number = 'TPR0011'
            pump_8_serial_number = 'TPR0012'

            # ----------------
            # PUMP 1
            # ----------------

            pump_1_bicoder = ValveBicoder(
                _sn=pump_1_serial_number,
                _controller=self.config.BaseStation3200[1],
                _id=opcodes.pump,
                _decoder_type=opcodes.single_valve_decoder)
            pump_1 = Pump(
                _controller=self.config.BaseStation3200[1],
                _address=1,
                _pump_bicoder=pump_1_bicoder)

            self.config.BaseStation3200[1].valve_bicoders[pump_1_serial_number] = pump_1_bicoder
            self.config.BaseStation3200[1].pumps[1] = pump_1

            # Update attributes with "real values" from the controller to verify against
            pump_1_bicoder.va = 0.21
            pump_1_bicoder.vv = 28.9
            pump_1_bicoder.vt = 1.2

            # Search for Pumps
            self.config.BaseStation3200[1].do_search_for_pumps()

            # Verify valve biCoder found on Two-Wire (device address = 0 expected)
            self.config.BaseStation3200[1].valve_bicoders[pump_1_serial_number].verify_device_address()

            # Address biCoder as Pump 1
            self.config.BaseStation3200[1].pumps[1].set_address(_ad=1)

            # Verify address assigned to Pump 1
            self.config.BaseStation3200[1].pumps[1].bicoder.verify_device_address()

            # Un-assign Pump 1
            self.config.BaseStation3200[1].pumps[1].set_address(_ad=0)

            # Verify able to un-assign valve bicoder from an address
            self.config.BaseStation3200[1].valve_bicoders[pump_1_serial_number].verify_device_address()

            # Re-address biCoder as Pump 1
            self.config.BaseStation3200[1].pumps[1].set_address(_ad=1)

            # Verify address assigned to Pump 1
            self.config.BaseStation3200[1].pumps[1].bicoder.verify_device_address()

            # Do self test on Pump 1
            self.config.BaseStation3200[1].pumps[1].bicoder.do_self_test()

            # Verify Pump 1 self test results
            pump_1_data = self.config.BaseStation3200[1].pumps[1].bicoder.get_data()
            self.config.BaseStation3200[1].pumps[1].bicoder.verify_solenoid_current(_data=pump_1_data)
            self.config.BaseStation3200[1].pumps[1].bicoder.verify_solenoid_voltage(_data=pump_1_data)
            self.config.BaseStation3200[1].pumps[1].bicoder.verify_two_wire_drop_value(_data=pump_1_data)

            # ----------------
            # PUMP 8
            # ----------------

            pump_8_bicoder = ValveBicoder(
                _sn=pump_8_serial_number,
                _controller=self.config.BaseStation3200[1],
                _id=opcodes.pump,
                _decoder_type=opcodes.single_valve_decoder)
            pump_8 = Pump(
                _controller=self.config.BaseStation3200[1],
                _address=8,
                _pump_bicoder=pump_8_bicoder)

            self.config.BaseStation3200[1].valve_bicoders[pump_8_serial_number] = pump_8_bicoder
            self.config.BaseStation3200[1].pumps[8] = pump_8

            # Update attributes with "real values" from the controller to verify against
            pump_8_bicoder.va = 0.14
            pump_8_bicoder.vv = 28.6
            pump_8_bicoder.vt = 0.9

            # Search for Pumps
            self.config.BaseStation3200[1].do_search_for_pumps()

            # Verify valve biCoder found on Two-Wire (device address = 0 expected)
            self.config.BaseStation3200[1].valve_bicoders[pump_8_serial_number].verify_device_address()

            # Address biCoder as Pump 8
            self.config.BaseStation3200[1].pumps[8].set_address(_ad=8)

            # Verify address assigned to Pump 8
            self.config.BaseStation3200[1].pumps[8].bicoder.verify_device_address()

            # Un-assign Pump 8
            self.config.BaseStation3200[1].pumps[8].set_address(_ad=0)

            # Verify able to un-assign valve bicoder from an address
            self.config.BaseStation3200[1].valve_bicoders[pump_8_serial_number].verify_device_address()

            # Re-address biCoder as Pump 8
            self.config.BaseStation3200[1].pumps[8].set_address(_ad=8)

            # Verify address assigned to Pump 8
            self.config.BaseStation3200[1].pumps[8].bicoder.verify_device_address()

            # Do self test on Pump 8
            self.config.BaseStation3200[1].pumps[8].bicoder.do_self_test()

            # Verify Pump 8 self test results
            pump_8_data = self.config.BaseStation3200[1].pumps[8].bicoder.get_data()
            self.config.BaseStation3200[1].pumps[8].bicoder.verify_solenoid_current(_data=pump_8_data)
            self.config.BaseStation3200[1].pumps[8].bicoder.verify_solenoid_voltage(_data=pump_8_data)
            self.config.BaseStation3200[1].pumps[8].bicoder.verify_two_wire_drop_value(_data=pump_8_data)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ############################
        Configure Event Switches
        ############################

        1. Create Event Switches
            - For each Event Switch:
                1) Create bicoder
                2) Create Event Switch
                3) Update bicoder attributes (with real values returned by controller) for verifying against
        2. Search and address Event Switches

        ----------------------------------------------
        | Address | Serial  | 2-Wire | Contact State |
        ----------------------------------------------
        |    1    | TPD0001 |   0.9  | Closed        |
        |    8    | TPD0004 |   0.9  | Closed        |
        ----------------------------------------------

        TODO: Test won't work with current "Coach's Button" implementation in 3200. Testing this device will be
        TODO: done at a later date.
        """
        helper_methods.print_method_name()
        try:
            # TODO - Configure Event Switches for test:
            #
            #   Steps:
            #       (1) Update test switch 1 & 8's serial numbers to use (i.e., sw_1_serial_number, sw_8_serial_number)
            #       (2) If possible, use available "Coaches Button" (BL-5401 biCoder) for Event Switch 8
            #       (3) If a "coaches button" is being configured for Event Switch 8:
            #           (3.a) Change: `sw_8_bicoder.vc = opcodes.contacts_closed`
            #                 To: `sw_8_bicoder.vc = opcodes.contacts_open`
            #           (3.b) Set breakpoint at line invoking the self test, 1371
            #           (3.c) When executing test and breakpoint is hit, walk to the "coaches button" and press the
            #                 button to change it's contact state to open (verify LED starts blinking)
            #           (3.d) Then with a brisk walk, get back to your computer and hit the continue button to continue
            #                 the test.

            # Tim's values
            # sw_1_serial_number = '00P0001'
            # sw_8_serial_number = 'RP00371'
            sw_1_serial_number = 'TPD0001'
            sw_8_serial_number = 'TPD0004'

            # ----------------
            # EVENT SWITCH 1
            # ----------------

            sw_1_bicoder = SwitchBicoder(
                _sn=sw_1_serial_number,
                _controller=self.config.BaseStation3200[1])
            sw_1 = EventSwitch(
                _controller=self.config.BaseStation3200[1],
                _address=1,
                _switch_bicoder=sw_1_bicoder)

            self.config.BaseStation3200[1].switch_bicoders[sw_1_serial_number] = sw_1_bicoder
            self.config.BaseStation3200[1].event_switches[1] = sw_1

            # Update attributes with "real values" from the controller to verify against
            sw_1_bicoder.vc = opcodes.contacts_closed
            sw_1_bicoder.vt = 0.9

            # Search for Event Switches
            self.config.BaseStation3200[1].do_search_for_event_switches()

            # Verify switch biCoder found on Two-Wire (device address = 0 expected)
            self.config.BaseStation3200[1].switch_bicoders[sw_1_serial_number].verify_device_address()

            # Address biCoder as Event Switch 1
            self.config.BaseStation3200[1].event_switches[1].set_address(_ad=1)

            # Verify address assigned to Event Switch 1
            self.config.BaseStation3200[1].event_switches[1].bicoder.verify_device_address()

            # Un-assign Event Switch 1
            self.config.BaseStation3200[1].event_switches[1].set_address(_ad=0)

            # Verify able to un-assign switch bicoder from an address
            self.config.BaseStation3200[1].switch_bicoders[sw_1_serial_number].verify_device_address()

            # Re-address biCoder as Event Switch 1
            self.config.BaseStation3200[1].event_switches[1].set_address(_ad=1)

            # Verify address assigned to Event Switch 1
            self.config.BaseStation3200[1].event_switches[1].bicoder.verify_device_address()

            # Do self test on Event Switch 1
            self.config.BaseStation3200[1].event_switches[1].bicoder.do_self_test()

            # Verify Event Switch 1 self test results
            sw_1_data = self.config.BaseStation3200[1].event_switches[1].bicoder.get_data()
            self.config.BaseStation3200[1].event_switches[1].bicoder.verify_contact_state(_data=sw_1_data)
            self.config.BaseStation3200[1].event_switches[1].bicoder.verify_two_wire_drop_value(_data=sw_1_data)

            # ----------------
            # EVENT SWITCH 8
            # ----------------

            sw_8_bicoder = SwitchBicoder(
                _sn=sw_8_serial_number,
                _controller=self.config.BaseStation3200[1])
            sw_8 = EventSwitch(
                _controller=self.config.BaseStation3200[1],
                _address=8,
                _switch_bicoder=sw_8_bicoder)

            self.config.BaseStation3200[1].switch_bicoders[sw_8_serial_number] = sw_8_bicoder
            self.config.BaseStation3200[1].event_switches[8] = sw_8

            # Update attributes with "real values" from the controller to verify against
            sw_8_bicoder.vc = opcodes.contacts_closed
            sw_8_bicoder.vt = 0.9

            # Search for Event Switches
            self.config.BaseStation3200[1].do_search_for_event_switches()

            # Verify switch biCoder found on Two-Wire (device address = 0 expected)
            self.config.BaseStation3200[1].switch_bicoders[sw_8_serial_number].verify_device_address()

            # Address biCoder as Event Switch 8
            self.config.BaseStation3200[1].event_switches[8].set_address(_ad=8)

            # Verify address assigned to Event Switch 8
            self.config.BaseStation3200[1].event_switches[8].bicoder.verify_device_address()

            # Un-assign Event Switch 8
            self.config.BaseStation3200[1].event_switches[8].set_address(_ad=0)

            # Verify able to un-assign switch bicoder from an address
            self.config.BaseStation3200[1].switch_bicoders[sw_8_serial_number].verify_device_address()

            # Re-address biCoder as Event Switch 8
            self.config.BaseStation3200[1].event_switches[8].set_address(_ad=8)

            # Verify address assigned to Event Switch 8
            self.config.BaseStation3200[1].event_switches[8].bicoder.verify_device_address()

            # Do self test on Event Switch 8
            self.config.BaseStation3200[1].event_switches[8].bicoder.do_self_test()

            # Verify Event Switch 8 self test results
            sw_8_data = self.config.BaseStation3200[1].event_switches[8].bicoder.get_data()
            self.config.BaseStation3200[1].event_switches[8].bicoder.verify_contact_state(_data=sw_8_data)
            self.config.BaseStation3200[1].event_switches[8].bicoder.verify_two_wire_drop_value(_data=sw_8_data)

            # If user modified Switch 8 to be a "coaches button", continue into if statement to handle the coaches
            # button reseting it's state back to "closed"
            if sw_8_bicoder.vc == opcodes.contacts_open:

                # Reset switch 8 back to closed state
                sw_8_bicoder.vc = opcodes.contacts_closed

                # Do self test on Event Switch 8 (to get new "closed" state)
                self.config.BaseStation3200[1].event_switches[8].bicoder.do_self_test()

                # Verify Event Switch 8 self test results (verifying the switch state reset back to closed)
                sw_8_data = self.config.BaseStation3200[1].event_switches[8].bicoder.get_data()
                self.config.BaseStation3200[1].event_switches[8].bicoder.verify_contact_state(_data=sw_8_data)
                self.config.BaseStation3200[1].event_switches[8].bicoder.verify_two_wire_drop_value(_data=sw_8_data)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
