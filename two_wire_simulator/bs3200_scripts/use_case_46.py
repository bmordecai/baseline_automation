import sys
from time import sleep
from datetime import time, timedelta, datetime, date
from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods


__author__ = 'Tige'


class ControllerUseCase46(object):
    """
    Test name:
        - CN UseCase46 assigning devices
    purpose:
        - This test addressing the devices on the 3200  and verify the 3200 forgets them and new devices can be found
    Coverage Area: \n
        - load a first set of all biCoder types on the 3200 and the SubStation
            - Do a search from the 3200
            - verify all biCoders are not assigned to an address
        - add or Address all the new biCoders to devices
            - verify all biCoders attributes through the 3200
            - verify all biCoders are not assigned to an address
            - verify all biCoders are addressed on the 3200
        - add all devices to programs
            - verify all programing
        - remove or un-address all the new devices
            - verify all biCoder are un-assigned or set to zero address
            - verify all programing has been lost
        -
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str or None \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                     # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)


    #################################
    def step_2(self):
        """
        load devices on the 3200 will not be using json for this test
        Controller devices loaded:

            BiCoder Type        Serial Number on 3200
            -----------------------------------------------------------------------
            - Single Valve
                                "TSD0001"
            -----------------------------------------------------------------------
            - Dual Valve
                                "TSE0011"
            -----------------------------------------------------------------------
            - Quad Valve
                                "TSQ0011"
            -----------------------------------------------------------------------
            - Twelve Valve
                                "TVA1001"
            -----------------------------------------------------------------------
            - Master Valves
                                "TMV0001"
                                "TMV0011"
            -----------------------------------------------------------------------
            - Pump
                                "PMV0001"
                                "PMV0011"
            -----------------------------------------------------------------------
            - Flow
                                "TWF0001"
            -----------------------------------------------------------------------
            - Moisture
                                "SB00001"
            -----------------------------------------------------------------------
            - Temperature
                                "TAT0001"
            -----------------------------------------------------------------------
            - Switch
                                "TPD0001"
            -----------------------------------------------------------------------
            - Analog
                                "PSD0001"
            -----------------------------------------------------------------------
        :return:
        """
        try:
            # load devices on 3200
            self.config.BaseStation3200[1].load_all_dv(d1_list=["TSD0001"],
                                                       mv_d1_list=["TMV0001"],
                                                       pm_d1_list=["PMV0001"],
                                                       d2_list=["TSE0011"],
                                                       mv_d2_list=["TMV0011"],
                                                       pm_d2_list=["PMV0011"],
                                                       d4_list=["TSQ0011"],
                                                       dd_list=["TVA1001"],
                                                       ms_list=["SB00001"],
                                                       fm_list=["TWF0001"],
                                                       ts_list=["TAT0001"],
                                                       sw_list=["TPD0001"],
                                                       an_list=["PSD0001"])

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        Do a search for all biCoders
        verify that all devices show up in the 3200 but they all have an address of zero
        """
        try:
            # one search will populate all devices
            self.config.BaseStation3200[1].do_search_for_zones()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        address Devices on the controller:

        Device Type     biCoder type    Serial Number   Address
        -----------------------------------------------------------------------
        - Zones
                        Single          "TSD0001"       0

                        Dual            "TSE0011"       0
                                        "TSE0012"       0

                        Quad            "TSQ0011"       0
                                        "TSQ0012"       0
                                        "TSQ0013"       0
                                        "TSQ0014"       0

                        Twelve          "TVA1001"       0
                                        "TVA1002"       0
                                        "TVA1003"       0

        -----------------------------------------------------------------------
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # verify single biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSD0001"].verify_device_address()


            # verify dual biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSE0011"].verify_device_address()

            # verify single biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSE0012"].verify_device_address()

            # verify quad biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSQ0012"].verify_device_address()

            # verify quad biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSQ0013"].verify_device_address()

            # verify quad biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSQ0014"].verify_device_address()

            # verify twelve valve biCoder values on SubStation and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TVA1001"].verify_device_address()

            # verify twelve biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TVA1002"].verify_device_address()

            # verify twelve biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TVA1003"].verify_device_address()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        address Devices on the controller:

        Device Type         biCoder type    Serial Number   Address
        -----------------------------------------------------------------------
        - Pumps
                        Single          "PMV0001"       0

                        Dual            "PMV0011"       0
                                        "PMV0011"       0

        -----------------------------------------------------------------------
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # verify pump biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["PMV0001"].verify_device_address()

            # verify pump biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["PMV0011"].verify_device_address()

            # verify pump biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["PMV0012"].verify_device_address()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        address Devices on the controller:

        Device Type         biCoder type    Serial Number   Address
        -----------------------------------------------------------------------
        - Master Valves     Single          "TMV0001"       0

                            Dual            "TMV0011"       0
                                            "TMV0012"       0

        -----------------------------------------------------------------------
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # verify master_valve biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TMV0001"].verify_device_address()


            # verify master_valve biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TMV0011"].verify_device_address()

            # verify master_valve biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TMV0012"].verify_device_address()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        address Devices on the controller:

        Device Type     biCoder type    Serial Number   Address
        -----------------------------------------------------------------------
        - Flow Meters   Flow            "TWF0001"       0
          -----------------------------------------------------------------------
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # verify flow biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].flow_bicoders["TWF0001"].verify_device_address()


        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        address Devices on the controller:

        Device Type         biCoder type    Serial Number   Address
        -----------------------------------------------------------------------
        - Moisture Sensors  Moisture        "SB00001"       0
        -----------------------------------------------------------------------
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # verify moisture biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].moisture_bicoders["SB00001"].verify_device_address()


        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        address Devices on the controller:

        Device Type             biCoder type    Serial Number   Address
        -----------------------------------------------------------------------
        - Temperature Sensor    Temperature    "TAT0001"       0

        -----------------------------------------------------------------------
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # verify temperature biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].temperature_bicoders["TAT0001"].verify_device_address()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        address Devices on the controller:

        Device Type         biCoder type    Serial Number   Address
        -----------------------------------------------------------------------
        - Event Switches    Switch          "TPD0001"       0

        -----------------------------------------------------------------------
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # verify switch biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].switch_bicoders["TPD0001"].verify_device_address()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        address Devices on the controller:

        Device Type         biCoder type    Serial Number   Address
        -----------------------------------------------------------------------
        - Pressure Sensors  Analog          "PSD0001"       0

        -----------------------------------------------------------------------
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # verify analog biCoder values and address 0 on the 3200
            self.config.BaseStation3200[1].analog_bicoders["PSD0001"].verify_device_address()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]    #################################

    #################################
    def step_12(self):
        """
        address Devices on the controller:

        Device Type         biCoder type    Serial Number   Address
        -----------------------------------------------------------------------
        - Pressure Sensors  Analog          "PSD0001"       0

        -----------------------------------------------------------------------
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # have to do a clear device to remove the faux devices
            self.config.BaseStation3200[1].clear_all_devices()
            # self.config.BaseStation3200[1].clear_all_programming()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]  #################################

    #################################
    def step_13(self):
        """
        load devices on the 3200 will not be using json for this test
        Controller devices loaded:

            BiCoder Type        Serial Number on 3200
            -----------------------------------------------------------------------
            - Single Valve
                                "TSD0003"
            -----------------------------------------------------------------------
            - Dual Valve
                                "TSE0031"
            -----------------------------------------------------------------------
            - Quad Valve
                                "TSQ0031"
            -----------------------------------------------------------------------
            - Twelve Valve
                                "TVA3001"
            -----------------------------------------------------------------------
            - Master Valves
                                "TMV0003"
                                "TMV0031"
            -----------------------------------------------------------------------
            - Pump
                                "PMV0003"
                                "PMV0031"
            -----------------------------------------------------------------------
            - Flow
                                "TWF0003"
            -----------------------------------------------------------------------
            - Moisture
                                "SB00003"
            -----------------------------------------------------------------------
            - Temperature
                                "TAT0003"
            -----------------------------------------------------------------------
            - Switch
                                "TPD0003"
            -----------------------------------------------------------------------
            - Analog
                                "PSD0003"
            -----------------------------------------------------------------------
        :return:
        """
        try:
            # load devices on 3200
            self.config.BaseStation3200[1].load_all_dv(d1_list=["TSD0003"],
                                                       mv_d1_list=["TMV0003"],
                                                       pm_d1_list=["PMV0003"],
                                                       d2_list=["TSE0031"],
                                                       mv_d2_list=["TMV0031"],
                                                       pm_d2_list=["PMV0031"],
                                                       d4_list=["TSQ0031"],
                                                       dd_list=["TVA3001"],
                                                       ms_list=["SB00003", "SB00004"],
                                                       fm_list=["TWF0003", "TWF0004"],
                                                       ts_list=["TAT0003", "TAT0004"],
                                                       sw_list=["TPD0003", "TPD0004"],
                                                       an_list=["PSD0003", "PSD0004"])

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_14(self):
        """
        Do a search for all biCoders
        verify that all devices show up in the 3200 but they all have an address of zero
        """
        try:

            # one search will populate all devices
            self.config.BaseStation3200[1].do_search_for_zones()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_15(self):
        """
        address Devices on the controller:

        Device Type     biCoder type    Serial Number   Address
        -----------------------------------------------------------------------
        - Zones
                        Single          "TSD0003"       1

                        Dual            "TSE0031"       3
                                        "TSE0032"       4

                        Quad            "TSQ0031"       7
                                        "TSQ0031"       8
                                        "TSQ0031"       9
                                        "TSQ0031"       10

                        Twelve          "TVA3001"       15
                                        "TVA3002"       16
                                        "TVA3003"       17

        -----------------------------------------------------------------------
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # - Zones
            # add single valve biCoder to zone address 1 on the 3200
            self.config.BaseStation3200[1].add_zone_to_controller(_address=1, _serial_number='TSD0003')
            self.config.BaseStation3200[1].valve_bicoders["TSD0003"].verify_device_address()

            # add dual valve biCoder to zone addresses 3, 4 on the 3200
            self.config.BaseStation3200[1].add_zone_to_controller(_address=3, _serial_number='TSE0031')
            self.config.BaseStation3200[1].valve_bicoders["TSE0031"].verify_device_address()

            self.config.BaseStation3200[1].add_zone_to_controller(_address=4, _serial_number='TSE0032')
            self.config.BaseStation3200[1].valve_bicoders["TSE0032"].verify_device_address()

            # add quad valve biCoder to zone addresses 7,8,9,10 on the 3200
            self.config.BaseStation3200[1].add_zone_to_controller(_address=7, _serial_number='TSQ0031')
            self.config.BaseStation3200[1].valve_bicoders["TSQ0031"].verify_device_address()

            self.config.BaseStation3200[1].add_zone_to_controller(_address=8, _serial_number='TSQ0032')
            self.config.BaseStation3200[1].valve_bicoders["TSQ0032"].verify_device_address()

            self.config.BaseStation3200[1].add_zone_to_controller(_address=9, _serial_number='TSQ0033')
            self.config.BaseStation3200[1].valve_bicoders["TSQ0033"].verify_device_address()

            self.config.BaseStation3200[1].add_zone_to_controller(_address=10, _serial_number='TSQ0034')
            self.config.BaseStation3200[1].valve_bicoders["TSQ0034"].verify_device_address()

            # add Twelve valve biCoder to zone addresses 15,16,17 on the 3200
            self.config.BaseStation3200[1].add_zone_to_controller(_address=15, _serial_number='TVA3001')
            self.config.BaseStation3200[1].valve_bicoders["TVA3001"].verify_device_address()

            self.config.BaseStation3200[1].add_zone_to_controller(_address=16, _serial_number='TVA3002')
            self.config.BaseStation3200[1].valve_bicoders["TVA3003"].verify_device_address()

            self.config.BaseStation3200[1].add_zone_to_controller(_address=17, _serial_number='TVA3003')
            self.config.BaseStation3200[1].valve_bicoders["TVA3003"].verify_device_address()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_16(self):
        """
        address Devices on the controller:

        Device Type         biCoder type    Serial Number   Address
        -----------------------------------------------------------------------
        - Pumps
                        Single          "PMV0003"       1

                        Dual            "PMV0031"       3
                                        "PMV0041"       8

        -----------------------------------------------------------------------
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # add single valve biCoder to pump addresses 1 on the 3200
            self.config.BaseStation3200[1].add_pump_to_controller(_address=1, _serial_number='PMV0003')
            self.config.BaseStation3200[1].valve_bicoders["PMV0003"].verify_device_address()

            # add dual valve biCoder to pump addresses 3,4 on the 3200
            self.config.BaseStation3200[1].add_pump_to_controller(_address=3, _serial_number='PMV0031')
            self.config.BaseStation3200[1].valve_bicoders["PMV0031"].verify_device_address()

            self.config.BaseStation3200[1].add_pump_to_controller(_address=8, _serial_number='PMV0032')
            self.config.BaseStation3200[1].valve_bicoders["PMV0032"].verify_device_address()


        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_17(self):
        """
        address Devices on the controller:

        Device Type         biCoder type    Serial Number   Address
        -----------------------------------------------------------------------
        - Master Valves     Single          "TMV0003"       1

                            Dual            "TMV0031"       3
                                            "TMV0042"       8

        -----------------------------------------------------------------------

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # add single valve biCoder to master valve addresses 1 on the 3200
            self.config.BaseStation3200[1].add_master_valve_to_controller(_address=1, _serial_number='TMV0003')
            self.config.BaseStation3200[1].valve_bicoders["TMV0003"].verify_device_address()


            # add dual valve biCoder to master valve addresses 3,4 on the 3200
            self.config.BaseStation3200[1].add_master_valve_to_controller(_address=3, _serial_number='TMV0031')
            self.config.BaseStation3200[1].valve_bicoders["TMV0031"].verify_device_address()

            self.config.BaseStation3200[1].add_master_valve_to_controller(_address=8, _serial_number='TMV0032')
            self.config.BaseStation3200[1].valve_bicoders["TMV0032"].verify_device_address()


        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_18(self):
        """
        address Devices on the controller:

        Device Type     biCoder type    Serial Number   Address
        -----------------------------------------------------------------------
        - Flow Meters   Flow            "TWF0003"       1
                        Flow            "TWF0004"       8
        -----------------------------------------------------------------------


        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # add flow biCoder to flow meter addresses a on the 3200
            self.config.BaseStation3200[1].add_flow_meter_to_controller(_address=1, _serial_number='TWF0003')
            self.config.BaseStation3200[1].flow_bicoders["TWF0003"].verify_device_address()

            # add flow biCoder to flow meter addresses a on the 3200
            self.config.BaseStation3200[1].add_flow_meter_to_controller(_address=8, _serial_number='TWF0004')
            self.config.BaseStation3200[1].flow_bicoders["TWF0004"].verify_device_address()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_19(self):
        """
        address Devices on the controller:

        Device Type         biCoder type    Serial Number   Address
        -----------------------------------------------------------------------
        - Moisture Sensors  Moisture        "SB00003"       1
                            Moisture        "SB00004"       8
        -----------------------------------------------------------------------


        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # add moisture biCoder to moisture sensor addresses a on the 3200
            self.config.BaseStation3200[1].add_moisture_sensor_to_controller(_address=1, _serial_number='SB00003')
            self.config.BaseStation3200[1].moisture_bicoders["SB00003"].verify_device_address()

            # add moisture biCoder to moisture sensor addresses a on the 3200
            self.config.BaseStation3200[1].add_moisture_sensor_to_controller(_address=8, _serial_number='SB00004')
            self.config.BaseStation3200[1].moisture_bicoders["SB00004"].verify_device_address()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_20(self):
        """
        address Devices on the controller:

        Device Type             biCoder type    Serial Number   Address
        -----------------------------------------------------------------------
        - Temperature Sensor    Temperature    "TAT0003"       1
                                Temperature    "TAT0004"       8
        -----------------------------------------------------------------------

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # add temperature biCoder to temperature sensor addresses a on the 3200
            self.config.BaseStation3200[1].add_temperature_sensor_to_controller(_address=1, _serial_number='TAT0003')
            self.config.BaseStation3200[1].temperature_bicoders["TAT0003"].verify_device_address()

            # add temperature biCoder to temperature sensor addresses a on the 3200
            self.config.BaseStation3200[1].add_temperature_sensor_to_controller(_address=8, _serial_number='TAT0004')
            self.config.BaseStation3200[1].temperature_bicoders["TAT0004"].verify_device_address()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_21(self):
        """
        address Devices on the controller:

        Device Type         biCoder type    Serial Number   Address
        -----------------------------------------------------------------------
        - Event Switches    Switch          "TPD0003"       1
                            Switch          "TPD0004"       2
        -----------------------------------------------------------------------

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # add event biCoder to event switch addresses a on the 3200
            self.config.BaseStation3200[1].add_event_switch_to_controller(_address=1, _serial_number='TPD0003')
            self.config.BaseStation3200[1].switch_bicoders["TPD0003"].verify_device_address()

            # add event biCoder to event switch addresses a on the 3200
            self.config.BaseStation3200[1].add_event_switch_to_controller(_address=8, _serial_number='TPD0004')
            self.config.BaseStation3200[1].switch_bicoders["TPD0004"].verify_device_address()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_22(self):
        """
        address Devices on the controller:

        Device Type         biCoder type    Serial Number   Address
        -----------------------------------------------------------------------
        - Pressure Sensors  Analog          "PSD0003"       1
                            Analog          "PSD0004"       8
        -----------------------------------------------------------------------

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # add analog biCoder to pressure sensor addresses a on the 3200
            self.config.BaseStation3200[1].add_pressure_sensor_to_controller(_address=1, _serial_number='PSD0003')
            self.config.BaseStation3200[1].analog_bicoders["PSD0003"].verify_device_address()

            # add analog biCoder to pressure sensor addresses a on the 3200
            self.config.BaseStation3200[1].add_pressure_sensor_to_controller(_address=8, _serial_number='PSD0004')
            self.config.BaseStation3200[1].analog_bicoders["PSD0004"].verify_device_address()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_23(self):
        """
        Verify fist valve biCoders are not present:

        Device Type     biCoder type    Serial Number
        -----------------------------------------------------------------------
        - Zones
                        Single          "TSD0001"

                        Dual            "TSE0011"
                                        "TSE0012"

                        Quad            "TSQ0011"
                                        "TSQ0012"
                                        "TSQ0013"
                                        "TSQ0014"

                        Twelve          "TVA1001"
                                        "TVA1002"
                                        "TVA1003"
        -----------------------------------------------------------------------
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # verify single biCoder not present on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSD0001"].verify_bicoder_not_present()


            # verify dual valve biCoder not present on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSE0011"].verify_bicoder_not_present()

            # verify dual valve biCoder not present on  the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSE0012"].verify_bicoder_not_present()

            # verify quad valve biCoder not present on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSQ0012"].verify_bicoder_not_present()

            # verify quad biCoder not present on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSQ0013"].verify_bicoder_not_present()

            # verify quad biCoder not present on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TSQ0014"].verify_bicoder_not_present()

            # verify twelve valve biCoder not present on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TVA1001"].verify_bicoder_not_present()

            # verify twelve biCoder not present on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TVA1002"].verify_bicoder_not_present()

            # verify twelve biCoder not present on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TVA1003"].verify_bicoder_not_present()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_24(self):
        """
        Verify fist pump biCoders are not present:

        Device Type         biCoder type    Serial Number
        -----------------------------------------------------------------------
        - Pumps
                        Single          "PMV0001"

                        Dual            "PMV0011"
                                        "PMV0011"

        -----------------------------------------------------------------------
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # verify pump biCoder not present on the 3200
            self.config.BaseStation3200[1].valve_bicoders["PMV0001"].verify_bicoder_not_present()

            # verify pump biCoder not present on the 3200
            self.config.BaseStation3200[1].valve_bicoders["PMV0011"].verify_bicoder_not_present()

            # verify pump biCoder not present on the 3200
            self.config.BaseStation3200[1].valve_bicoders["PMV0012"].verify_bicoder_not_present()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_25(self):
        """
        Verfiy fist master valve biCoders are not present:

        Device Type         biCoder type    Serial Number
        -----------------------------------------------------------------------
        - Master Valves     Single          "TMV0001"

                            Dual            "TMV0011"
                                            "TMV0012"
        -----------------------------------------------------------------------
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # verify master_valve biCoder not present on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TMV0001"].verify_bicoder_not_present()

            # verify master_valve biCoder not present on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TMV0011"].verify_bicoder_not_present()

            # verify master_valve biCoder not present on the 3200
            self.config.BaseStation3200[1].valve_bicoders["TMV0012"].verify_bicoder_not_present()


        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_26(self):
        """
        Verify fist flow biCoders are not present:

        Device Type     biCoder type    Serial Number
        -----------------------------------------------------------------------
        - Flow Meters   Flow            "TWF0001"
        -----------------------------------------------------------------------
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # verify flow biCoder values not present on the 3200
            self.config.BaseStation3200[1].flow_bicoders["TWF0001"].verify_bicoder_not_present()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_27(self):
        """
        aVerify fist moisture biCoders are not present:

        Device Type         biCoder type    Serial Number
        -----------------------------------------------------------------------
        - Moisture Sensors  Moisture        "SB00001"
        -----------------------------------------------------------------------
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # verify moisture biCoder not present on the 3200
            self.config.BaseStation3200[1].moisture_bicoders["SB00001"].verify_bicoder_not_present()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_28(self):
        """
        Verify fist temperature biCoders are not present:

        Device Type             biCoder type    Serial Number
        -----------------------------------------------------------------------
        - Temperature Sensor    Temperature    "TAT0001"
        -----------------------------------------------------------------------
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # verify temperature biCoder not present on the 3200
            self.config.BaseStation3200[1].temperature_bicoders["TAT0001"].verify_bicoder_not_present()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_29(self):
        """
        Verify fist switch biCoders are not present:

        Device Type         biCoder type    Serial Number
        -----------------------------------------------------------------------
        - Event Switches    Switch          "TPD0001"
        -----------------------------------------------------------------------
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # verify switch biCoder not present on the 3200
            self.config.BaseStation3200[1].switch_bicoders["TPD0001"].verify_bicoder_not_present()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_30(self):
        """
        Verify fist analog biCoders are not present:

        Device Type         biCoder type    Serial Number
        -----------------------------------------------------------------------
        - Pressure Sensors  Analog          "PSD0001"
        -----------------------------------------------------------------------
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method

        try:
            # verify analog biCoder not present on the 3200
            self.config.BaseStation3200[1].analog_bicoders["PSD0001"].verify_bicoder_not_present()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]    #################################

    #################################
    def step_31(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller agaist information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_32(self):
        """
        do a self test on all devices \n
        increment the clock by 1 minute \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for zone in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone].bicoder.self_test_and_update_object_attributes()
            for moisture_sensors in sorted(self.config.BaseStation3200[1].moisture_sensors.keys()):
                self.config.BaseStation3200[1].moisture_sensors[moisture_sensors].\
                    bicoder.self_test_and_update_object_attributes()
            for master_valves in sorted(self.config.BaseStation3200[1].master_valves.keys()):
                self.config.BaseStation3200[1].master_valves[master_valves].\
                    bicoder.self_test_and_update_object_attributes()
            for flow_meters in sorted(self.config.BaseStation3200[1].flow_meters.keys()):
                self.config.BaseStation3200[1].flow_meters[flow_meters].bicoder.self_test_and_update_object_attributes()
            for event_switches in sorted(self.config.BaseStation3200[1].event_switches.keys()):
                self.config.BaseStation3200[1].event_switches[event_switches].\
                    bicoder.self_test_and_update_object_attributes()
            for temperature_sensors in sorted(self.config.BaseStation3200[1].temperature_sensors.keys()):
                self.config.BaseStation3200[1].temperature_sensors[temperature_sensors].\
                    bicoder.self_test_and_update_object_attributes()
            for pump in sorted(self.config.BaseStation3200[1].pumps.keys()):
                self.config.BaseStation3200[1].pumps[pump].bicoder.self_test_and_update_object_attributes()
            for pressure_sensor in sorted(self.config.BaseStation3200[1].pressure_sensors.keys()):
                self.config.BaseStation3200[1].pressure_sensors[pressure_sensor].\
                    bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_33(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller agaist information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
