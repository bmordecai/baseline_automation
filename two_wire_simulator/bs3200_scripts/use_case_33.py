import sys
from time import sleep
from datetime import datetime, timedelta
from distutils.version import LooseVersion

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

__author__ = 'Tige'


class ControllerUseCase33(object):
    """
    Coverage area: \n
    1. able to search for valve decoders \n # NOMV+Flow has been discontinued \n
    2. able to assign and un-assign decoders to any MV number \n
    3. able to assign MV to POC \n
    4. enable a master valve as a booster pump \n
    5. enable a booster pump on a program \n
    6. able to assign POC to Mainlines \n
    7. able to assign Programs to Mainlines \n
    8. able to assign MV as booster pump to program
    9. able to manually start Programs and verify MVs
    10. able to assign a mv as normally open or normally closed \n
    11. verification for bug ZZ-432 released in coder 12.21
    12. erased program 4 and add a program 99
    13. changed master valve 4 from normally open to normally closed
    14. in exercise 1 added a check to verify master valve is working correctly
    15. in exercise 4 added a check to verify master valve is working correctly
    """
    # TODO need to add verifying if master valve are running
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        setup programs
        ############################

        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            program_start_time_8am = [480]

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_start_time_8am)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(
                _sun=True, _mon=True, _tues=True, _wed=True, _thurs=True, _fri=True, _sat=True
            )

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation3200[1].programs[2].set_enabled()
            self.config.BaseStation3200[1].programs[2].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[2].set_start_times(_st_list=program_start_time_8am)
            self.config.BaseStation3200[1].programs[2].set_watering_intervals_to_selected_days_of_the_week(
                _sun=True, _mon=True, _tues=True, _wed=True, _thurs=True, _fri=True, _sat=True
            )

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=3)
            self.config.BaseStation3200[1].programs[3].set_enabled()
            self.config.BaseStation3200[1].programs[3].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[3].set_start_times(_st_list=program_start_time_8am)
            self.config.BaseStation3200[1].programs[3].set_watering_intervals_to_selected_days_of_the_week(
                _sun=True, _mon=True, _tues=True, _wed=True, _thurs=True, _fri=True, _sat=True
            )

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=4)
            self.config.BaseStation3200[1].programs[4].set_enabled()
            self.config.BaseStation3200[1].programs[4].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[4].set_start_times(_st_list=program_start_time_8am)
            self.config.BaseStation3200[1].programs[4].set_watering_intervals_to_selected_days_of_the_week(
                _sun=True, _mon=True, _tues=True, _wed=True, _thurs=True, _fri=True, _sat=True
            )
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        Setup Zones on programs
        ############################

        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ratio \n

        - Set up Zone Programs \n
            - Zone 1 Program 1 ( Timed Zone) \n
                - Run Time: 2700 \n
                - Cycle Time: None \n
                - Soak Time: None \n
            - Zone 2 Program 1 (Timed Zone) \n
                - Run Time: 2700 \n
                - Cycle Time: None \n
                - Soak Time: None \n
            - Zone 3 Program 2 (Timed Zone) \n
                - Run Time: 2700 \n
                - Cycle Time: None \n
                - Soak Time: None \n
            - Zone 4 Program 2 (Timed Zone) \n
                - Run Time: 2700 \n
                - Cycle Time: None \n
                - Soak Time: None \n
            - Zone 5 Program 3 (Timed Zone) \n
                - Run Time: 2700 \n
                - Cycle Time: None \n
                - Soak Time: None \n
            - Zone 5 Program 3 (Timed Zone) \n
                - Run Time: 2700 \n
                - Cycle Time: None \n
                - Soak Time: None \n
            - Zone 6 Program 3 (Timed Zone) \n
                - Run Time: 2700 \n
                - Cycle Time: None \n
                - Soak Time: None \n
            - Zone 7 Program 99 (Timed Zone) \n
                - Run Time: 2700 \n
                - Cycle Time: None \n
                - Soak Time: None \n
            - Zone 8 Program 99 (Timed Zone) \n
                - Run Time: 2700 \n
                - Cycle Time: None \n
                - Soak Time: None \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:


            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=45)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_run_time(_minutes=45)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[2].zone_programs[3].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[2].zone_programs[3].set_run_time(_minutes=45)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[2].zone_programs[4].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[2].zone_programs[4].set_run_time(_minutes=45)

            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=5)
            self.config.BaseStation3200[1].programs[3].zone_programs[5].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[3].zone_programs[5].set_run_time(_minutes=45)

            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=6)
            self.config.BaseStation3200[1].programs[3].zone_programs[6].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[3].zone_programs[6].set_run_time(_minutes=45)

            self.config.BaseStation3200[1].programs[4].add_zone_to_program(_zone_address=7)
            self.config.BaseStation3200[1].programs[4].zone_programs[7].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[4].zone_programs[7].set_run_time(_minutes=45)

            self.config.BaseStation3200[1].programs[4].add_zone_to_program(_zone_address=8)
            self.config.BaseStation3200[1].programs[4].zone_programs[8].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[4].zone_programs[8].set_run_time(_minutes=45)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        Setup water sources
        ############################

        Add water sources -----> to controlLer
            - set up water source  Attributes \n
                - set enable state \n
                - set priority \n
                - set water budget \n
                - set water rationing state \n
                - set high pressure limit with shut down state \n
                - set low pressure limit with shut down state \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # Water Source 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].set_priority(_priority_for_water_source=2)

            # Water Source 2
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=2)
            self.config.BaseStation3200[1].water_sources[2].set_enabled()
            self.config.BaseStation3200[1].water_sources[2].set_priority(_priority_for_water_source=2)


            # Water Source 3
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=3)
            self.config.BaseStation3200[1].water_sources[3].set_enabled()
            self.config.BaseStation3200[1].water_sources[3].set_priority(_priority_for_water_source=2)

            # Water Source 4
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=4)
            self.config.BaseStation3200[1].water_sources[4].set_enabled()
            self.config.BaseStation3200[1].water_sources[4].set_priority(_priority_for_water_source=2)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        Setup points of control
        ############################

        Add Points of Control -----> to controlLer
            - set up points of control Attributes \n
                - set enable state \n
                - set target flow \n
                - set high flow limit with shut down state \n
                - set unscheduled flow limit with shut down state \n
                - set high pressure limit with shut down state \n
                - set low pressure limit with shut down state \n
        Add flow meters ---> to point of control \n
        Add pump ---> to point of control \n
        Add master valve  ---> to point of control \n
        Add pressure sensor  ---> to point of control \n
        Add Points of Control -----> To Water Source
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # Add & Configure POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].points_of_control[1].set_high_flow_limit(_limit=60,
                                                                                    with_shutdown_enabled=True)
            self.config.BaseStation3200[1].points_of_control[1].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=True)
            # Add MV 1 to POC 1
            self.config.BaseStation3200[1].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=1)
            # Add PM 1 to POC 1
            self.config.BaseStation3200[1].points_of_control[1].add_pump_to_point_of_control(_pump_address=1)
            # Add POC 1 to Water Source 1
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=1)

            # Add & Configure POC 2
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=2)
            self.config.BaseStation3200[1].points_of_control[2].set_enabled()
            self.config.BaseStation3200[1].points_of_control[2].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].points_of_control[2].set_high_flow_limit(_limit=60,
                                                                                    with_shutdown_enabled=True)
            self.config.BaseStation3200[1].points_of_control[2].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=True)
            # Add MV 2 to POC 2
            self.config.BaseStation3200[1].points_of_control[2].add_master_valve_to_point_of_control(
                _master_valve_address=2)
            # Add PM 2 to POC 1
            self.config.BaseStation3200[1].points_of_control[2].add_pump_to_point_of_control(_pump_address=2)
            # Add POC 2 to Water Source 2
            self.config.BaseStation3200[1].water_sources[2].add_point_of_control_to_water_source(
                _point_of_control_address=2)


            # Add & Configure POC 3
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=3)
            self.config.BaseStation3200[1].points_of_control[3].set_enabled()
            self.config.BaseStation3200[1].points_of_control[3].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].points_of_control[3].set_high_flow_limit(_limit=60,
                                                                                    with_shutdown_enabled=True)
            self.config.BaseStation3200[1].points_of_control[3].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=True)
            # Add MV 3 to POC 3
            self.config.BaseStation3200[1].points_of_control[3].add_master_valve_to_point_of_control(
                _master_valve_address=3)
            self.config.BaseStation3200[1].water_sources[3].add_point_of_control_to_water_source(
                _point_of_control_address=3)

            # Add & Configure POC 4
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=4)
            self.config.BaseStation3200[1].points_of_control[4].set_enabled()
            self.config.BaseStation3200[1].points_of_control[4].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].points_of_control[4].set_high_flow_limit(_limit=60,
                                                                                    with_shutdown_enabled=True)
            self.config.BaseStation3200[1].points_of_control[4].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=True)
            # Add PM 4 to POC 1
            self.config.BaseStation3200[1].points_of_control[4].add_pump_to_point_of_control(_pump_address=4)
            # Add POC 4 to Water Source 4
            self.config.BaseStation3200[1].water_sources[4].add_point_of_control_to_water_source(
                _point_of_control_address=4)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        Setup mainlines
        ############################

        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
        Add Mainline ---> to point of control
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # Add & Configure ML 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_false()
            self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=3)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=40)
            # Add ML 1 to POC 1
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)

            # Add & Configure ML 2
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=2)
            self.config.BaseStation3200[1].mainlines[2].set_limit_zones_by_flow_to_false()
            self.config.BaseStation3200[1].mainlines[2].set_pipe_stabilization_time(_minutes=3)
            self.config.BaseStation3200[1].mainlines[2].set_target_flow(_gpm=40)
            # Add ML 2 to POC 2
            self.config.BaseStation3200[1].points_of_control[2].add_mainline_to_point_of_control(_mainline_address=2)

            # Add & Configure ML 3
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=3)
            self.config.BaseStation3200[1].mainlines[3].set_limit_zones_by_flow_to_false()
            self.config.BaseStation3200[1].mainlines[3].set_pipe_stabilization_time(_minutes=3)
            self.config.BaseStation3200[1].mainlines[3].set_target_flow(_gpm=40)
            # Add ML 3 to POC 3
            self.config.BaseStation3200[1].points_of_control[3].add_mainline_to_point_of_control(_mainline_address=3)

            # Add & Configure ML 4
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=4)
            self.config.BaseStation3200[1].mainlines[4].set_limit_zones_by_flow_to_false()
            self.config.BaseStation3200[1].mainlines[4].set_pipe_stabilization_time(_minutes=3)
            self.config.BaseStation3200[1].mainlines[4].set_target_flow(_gpm=40)
            # Add ML 4 to POC 4
            self.config.BaseStation3200[1].points_of_control[4].add_mainline_to_point_of_control(_mainline_address=4)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ############################
        Add zones to mainlines
        ############################

        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Mainline 1
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=1)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=2)
            # Mainline 2
            self.config.BaseStation3200[1].mainlines[2].add_zone_to_mainline(_zone_address=3)
            self.config.BaseStation3200[1].mainlines[2].add_zone_to_mainline(_zone_address=4)
            # Mainline 3
            self.config.BaseStation3200[1].mainlines[3].add_zone_to_mainline(_zone_address=5)
            self.config.BaseStation3200[1].mainlines[3].add_zone_to_mainline(_zone_address=6)
            # Mainline 4
            self.config.BaseStation3200[1].mainlines[4].add_zone_to_mainline(_zone_address=7)
            self.config.BaseStation3200[1].mainlines[4].add_zone_to_mainline(_zone_address=8)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ###############################
        verify the entire configuration
        ###############################

        - Increment clock to save configuration
        - Get information for each object from controller
        - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        Modify programming
        ###############################

        - Disable program 4
        - Create a new program 99
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].programs[4].set_disabled()

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=99)
            self.config.BaseStation3200[1].programs[99].set_enabled()
            self.config.BaseStation3200[1].programs[99].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[99].set_start_times(_st_list=[480])
            self.config.BaseStation3200[1].programs[99].set_watering_intervals_to_selected_days_of_the_week(
                _sun=True, _mon=True, _tues=True, _wed=True, _thurs=True, _fri=True, _sat=True
            )

            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=7)
            self.config.BaseStation3200[1].programs[99].zone_programs[7].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[99].zone_programs[7].set_run_time(_minutes=45)

            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=8)
            self.config.BaseStation3200[1].programs[99].zone_programs[8].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[99].zone_programs[8].set_run_time(_minutes=45)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        ###############################
        verify the entire configuration
        ###############################

        setup master valve
          - "TSD0001" address 1 set to normally closed
          - "TVE3301" address 2 set to normally open
          - "TSE0012" address 3 set to normally closed
          - "TMV0004" address 4 set to normally closed

            - set TMV0004 address 4 to be a Booster Pump on Program 2
            - set TMV0004 address 4 to be a Booster Pump on Program 3
            -  set TSE0022 address 3 to be a booster pump on program 99
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].master_valves[1].set_normally_open_state(_normally_open=opcodes.false)
            self.config.BaseStation3200[1].master_valves[2].set_normally_open_state(_normally_open=opcodes.true)
            self.config.BaseStation3200[1].master_valves[3].set_normally_open_state(_normally_open=opcodes.false)
            self.config.BaseStation3200[1].master_valves[4].set_normally_open_state(_normally_open=opcodes.false)

            self.config.BaseStation3200[1].master_valves[4].set_booster_enabled()
            self.config.BaseStation3200[1].programs[2].add_booster_pump(_mv_address=4)
            self.config.BaseStation3200[1].programs[3].add_booster_pump(_mv_address=4)
            self.config.BaseStation3200[1].pumps[3].set_booster_enabled()
            self.config.BaseStation3200[1].programs[99].add_booster_pump(_pm_address=3)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        ###############################
        verify the entire configuration
        ###############################

        - Get information for each object from controller
        - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]



    #################################
    def step_11(self):
        """
        set the date and time of the controller so that there is a known days of the week and days of the month \n
        - verify all devices are working by doing a verification before starting the test \n

        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_date_and_time(_date='04/08/2014', _time='07:00:00')
            # verify water sources
            for water_source in [1, 2, 3, 4]:
                self.config.BaseStation3200[1].water_sources[water_source].statuses.verify_status_is_ok()
            # verify points of control
            for point_of_control in [1, 2, 3, 4]:
                self.config.BaseStation3200[1].points_of_control[point_of_control].statuses.verify_status_is_off()
            # verify devices on points of control
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            # Master Valve 2 in normally open therefore it is watering
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].master_valves[4].statuses.verify_status_is_off()

            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[3].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[4].statuses.verify_status_is_off()
            # verify Mainlines
            for mainlines in [1, 2, 3, 4]:
                self.config.BaseStation3200[1].mainlines[mainlines].statuses.verify_status_is_off()
            # verify zones on mainlines
            for zone in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            # verify programs
            for program_ad in [1, 2, 3, 99]:
                self.config.BaseStation3200[1].programs[program_ad].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_disabled()


        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        Start program 1 \n
        increment clock 1 minute so that the program will start and status will be updated \n


        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].programs[1].set_program_to_start()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # verify water sources
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].water_sources[3].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].water_sources[4].statuses.verify_status_is_ok()

            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].points_of_control[3].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].points_of_control[4].statuses.verify_status_is_off()
            # verify devices on points of control
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # Master Valve 2 in normally open therefore it is watering
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].master_valves[4].statuses.verify_status_is_off()

            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[3].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[4].statuses.verify_status_is_off()

            # verify Mainlines
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[2].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[3].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[4].statuses.verify_status_is_off()

            # verify zones on mainlines
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_done()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        Stop program 1 \n
        Start program 2 \n
        increment clock 1 minute so that the program will start and status will be updated \n

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[2].set_program_to_start()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            # verify water sources
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].water_sources[3].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].water_sources[4].statuses.verify_status_is_ok()

            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[3].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].points_of_control[4].statuses.verify_status_is_off()
            # verify devices on points of control
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            # Master Valve 2 in normally open therefore it is watering
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_off()
            # This is running because its a booster of program 2
            self.config.BaseStation3200[1].master_valves[4].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[3].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[4].statuses.verify_status_is_off()

            # verify Mainlines
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[3].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[4].statuses.verify_status_is_off()

            # verify zones on mainlines
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_done()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_14(self):
        """
        Stop program 2 \n
        Start program 3 \n
        increment clock 1 minute so that the program will start and status will be updated \n


        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()
            self.config.BaseStation3200[1].programs[3].set_program_to_start()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            # verify water sources
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].water_sources[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].water_sources[4].statuses.verify_status_is_ok()

            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].points_of_control[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[4].statuses.verify_status_is_off()
            # verify devices on points of control
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            # Master Valve 2 in normally open therefore it is watering
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_watering()
            # This is running because its a booster of program 2
            self.config.BaseStation3200[1].master_valves[4].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[3].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[4].statuses.verify_status_is_off()

            # verify Mainlines
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[2].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[4].statuses.verify_status_is_off()

            # verify zones on mainlines
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_done()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_15(self):
        """
        Stop program 3 \n
        Start program 4 \n
        increment clock 1 minute so that the program will start and status will be updated \n



        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].programs[3].set_program_to_stop()
            self.config.BaseStation3200[1].programs[4].set_program_to_start()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            # verify water sources
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].water_sources[3].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].water_sources[4].statuses.verify_status_is_ok()

            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].points_of_control[3].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].points_of_control[4].statuses.verify_status_is_off()
            # verify devices on points of control
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            # Master Valve 2 in normally open therefore it is watering
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_off()
            # This is running because its a booster of program 2
            self.config.BaseStation3200[1].master_valves[4].statuses.verify_status_is_off()

            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[3].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[4].statuses.verify_status_is_off()

            # verify Mainlines
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[2].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[3].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[4].statuses.verify_status_is_off()

            # verify zones on mainlines
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_done()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_16(self):
        """
        Stop program 4 \n
        Start program 99 \n
        increment clock 1 minute so that the program will start and status will be updated \n



        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].programs[4].set_program_to_stop()
            self.config.BaseStation3200[1].programs[99].set_program_to_start()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            # verify water sources
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].water_sources[3].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].water_sources[4].statuses.verify_status_is_running()

            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].points_of_control[3].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].points_of_control[4].statuses.verify_status_is_running()
            # verify devices on points of control
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            # Master Valve 2 in normally open therefore it is watering
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_off()
            # This is running because its a booster of program 2
            self.config.BaseStation3200[1].master_valves[4].statuses.verify_status_is_off()

            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[4].statuses.verify_status_is_watering()

            # verify Mainlines
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[2].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[3].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[4].statuses.verify_status_is_running()

            # verify zones on mainlines
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_waiting_to_water()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_running()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_17(self):
        """
        set the max zone concurrency for the controller to 2
        Because each each program is limited to 1 zone but we set the controller to run 2 than two Programs will \n
        run at one time \n
        Stop program 99 \n
        Start program 1 \n
        Start program 2 \n
        increment clock 1 minute so that the program will start and status will be updated \n

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # change max concurrent zones on controller
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=3)

            self.config.BaseStation3200[1].programs[99].set_program_to_stop()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            self.config.BaseStation3200[1].programs[1].set_program_to_start()
            self.config.BaseStation3200[1].programs[2].set_program_to_start()
            self.config.BaseStation3200[1].programs[99].set_program_to_start()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            # verify water sources
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].water_sources[3].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].water_sources[4].statuses.verify_status_is_ok()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[3].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].points_of_control[4].statuses.verify_status_is_off()
            # verify devices on points of control
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # Master Valve 2 in normally open therefore it is watering
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_off()
            # This is running because its a booster of program 2
            self.config.BaseStation3200[1].master_valves[4].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[3].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[4].statuses.verify_status_is_off()

            # verify Mainlines
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[3].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[4].statuses.verify_status_is_off()

            # verify zones on mainlines
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_waiting_to_water()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[4].statuses.verify_status_is_disabled()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_waiting_to_run()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
