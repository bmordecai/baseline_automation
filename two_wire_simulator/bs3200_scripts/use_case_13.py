import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler


__author__ = 'Tige'


class ControllerUseCase13(object):
    """
    Test name:
        - CN UseCase13 learn flow
    purpose:
        - set up a full configuration on the controller
            - check that a controller can learn flow accross on multple main lines at the same time
                - verify success learn flow
                - verify fail learn flow
    Coverage area: \n
        1. Setting up the controller \n
        2. Searching and assigning: \n
            - Zones \n
            - Master Valves \n
            - Flow Meters \n
        3. Assign each zone a design flow \n
        4. Setting up water sources \n
        5. Set up the Programs \n
        6. Assign each zone to a program \n
        7. Set up the zone concurrency for the controller and Programs \n
        7. learn flow failure with 0 flow
        8. learn flow success and zone designg flows are updated
        8. Verify the full configuration
        """
        # TODO Bug the controller will allow two zones to learn flow if they are on different mainline but total
        # zone concurance is set to 1 there for not both program should be learning flow
        # create another set to verify zone concurrency during a learn flow

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                     # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        program_start_times = [600] # 10:00am start time
        program_water_windows = ['111111111111111111111111']
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:          
            # Add and configure Program 1
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_water_window(_ww=program_water_windows)
            self.config.BaseStation3200[1].programs[1].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[1].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(_sun=True,
                                                                                                           _mon=True,
                                                                                                           _tues=True,
                                                                                                           _wed=True,
                                                                                                           _thurs=True,
                                                                                                           _fri=True,
                                                                                                           _sat=True)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_start_times)
            
            # Add and configure Program 99
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=99)
            self.config.BaseStation3200[1].programs[99].set_enabled()
            self.config.BaseStation3200[1].programs[99].set_water_window(_ww=program_water_windows)
            self.config.BaseStation3200[1].programs[99].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[99].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[99].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[99].set_watering_intervals_to_selected_days_of_the_week(_sun=True,
                                                                                                            _mon=True,
                                                                                                            _tues=True,
                                                                                                            _wed=True,
                                                                                                            _thurs=True,
                                                                                                            _fri=True,
                                                                                                            _sat=True)
            self.config.BaseStation3200[1].programs[99].set_start_times(_st_list=program_start_times)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_3(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n

        Program Zone 1
            - Program: 1
            - Zone: 1
            - Mode: Primary
            - Runtime: 16 min
            - Cycle Time: 5 min
            - Soak time: 5 min

        Program Zone 2:
            - Program: 1
            - Zone: 2
            - Mode: Linked-1
            - Runtime: 16 min
            - Cycle Time: 5 min
            - Soak time: 5 min
            
        Program Zone 3:
            - Program: 1
            - Zone: 3
            - Mode: Linked-1
            - Runtime: 16 min
            - Cycle Time: 5 min
            - Soak time: 5 min
            
        Program Zone 4:
            - Program: 1
            - Zone: 4
            - Mode: Linked-1
            - Runtime: 16 min
            - Cycle Time: 5 min
            - Soak time: 5 min
            
        Program Zone 5:
            - Program: 1
            - Zone: 5
            - Mode: Linked-1
            - Runtime: 16 min
            - Cycle Time: 5 min
            - Soak time: 5 min

        Program Zone 6:
            - Program: 99
            - Zone: 6
            - Mode: Timed
            - Runtime: 5 min
            - Cycle Time: 2 min
            - Soak time: 2 min

        Program Zone 7:
            - Program: 99
            - Zone: 7
            - Mode: Timed
            - Runtime: 5 min
            - Cycle Time: 2 min
            - Soak time: 2 min

        Program Zone 8:
            - Program: 99
            - Zone: 8
            - Mode: Timed
            - Runtime: 5 min
            - Cycle Time: 2 min
            - Soak time: 2 min

        Program Zone 9:
            - Program: 99
            - Zone: 9
            - Mode: Timed
            - Runtime: 5 min
            - Cycle Time: 2 min
            - Soak time: 2 min

        Program Zone 10:
            - Program: 99
            - Zone: 10
            - Mode: Timed
            - Runtime: 5 min
            - Cycle Time: 2 min
            - Soak time: 2 min
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure Program Zone 1
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=16)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=5)

            # Add & Configure Program Zone 2
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1)

            # Add & Configure Program Zone 3
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_as_linked_zone(_primary_zone=1)

            # Add & Configure Program Zone 4
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[1].zone_programs[4].set_as_linked_zone(_primary_zone=1)

            # Add & Configure Program Zone 5
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[5].set_as_linked_zone(_primary_zone=1)

            # Add & Configure Program Zone 6
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=6)
            self.config.BaseStation3200[1].programs[99].zone_programs[6].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[99].zone_programs[6].set_run_time(_minutes=5)
            self.config.BaseStation3200[1].programs[99].zone_programs[6].set_cycle_time(_minutes=2)
            self.config.BaseStation3200[1].programs[99].zone_programs[6].set_soak_time(_minutes=2)

            # Add & Configure Program Zone 7
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=7)
            self.config.BaseStation3200[1].programs[99].zone_programs[7].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[99].zone_programs[7].set_run_time(_minutes=5)
            self.config.BaseStation3200[1].programs[99].zone_programs[7].set_cycle_time(_minutes=2)
            self.config.BaseStation3200[1].programs[99].zone_programs[7].set_soak_time(_minutes=2)

            # Add & Configure Program Zone 8
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=8)
            self.config.BaseStation3200[1].programs[99].zone_programs[8].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[99].zone_programs[8].set_run_time(_minutes=5)
            self.config.BaseStation3200[1].programs[99].zone_programs[8].set_cycle_time(_minutes=2)
            self.config.BaseStation3200[1].programs[99].zone_programs[8].set_soak_time(_minutes=2)

            # Add & Configure Program Zone 9
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=9)
            self.config.BaseStation3200[1].programs[99].zone_programs[9].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[99].zone_programs[9].set_run_time(_minutes=5)
            self.config.BaseStation3200[1].programs[99].zone_programs[9].set_cycle_time(_minutes=2)
            self.config.BaseStation3200[1].programs[99].zone_programs[9].set_soak_time(_minutes=2)

            # Add & Configure Program Zone 10
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=10)
            self.config.BaseStation3200[1].programs[99].zone_programs[10].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[99].zone_programs[10].set_run_time(_minutes=5)
            self.config.BaseStation3200[1].programs[99].zone_programs[10].set_cycle_time(_minutes=2)
            self.config.BaseStation3200[1].programs[99].zone_programs[10].set_soak_time(_minutes=2)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n

        set up WS 1 \n
            - enabled
            - target flow: 500 \n
            - priority: 2 \n
            - water budget: 100000
            - water budget shutdown: enabled \n
            - water rationing: enabled \n
            - empty conditions:
                - none

        set up WS 8 \n
            - enabled
            - target flow: 500 \n
            - priority: 2 \n
            - water budget: 100000
            - water budget shutdown: enabled \n
            - water rationing: enabled \n
            - empty conditions:
                - none
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Water Source 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[1].set_monthly_watering_budget(_budget=100000,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].water_sources[1].set_water_rationing_to_enabled()

            # Water Source 8
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=8)
            self.config.BaseStation3200[1].water_sources[8].set_enabled()
            self.config.BaseStation3200[1].water_sources[8].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[8].set_monthly_watering_budget(_budget=100000,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].water_sources[8].set_water_rationing_to_enabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                    self.config.test_name,
                    sys._getframe().f_code.co_name,
                    date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                    str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n
        set up POC 1 \n
            enable POC 1 \n
            assign master valve TMV0003 and flow meter TWF0003 to POC 1 \n
            assign POC 1 a target flow of 500 \n
            assign POC 1 to main line 1 \n
            set POC priority to 2-medium \n
            set high flow limit to 550 and enable high flow shut down \n
            set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            set water budget to 100000 and enable the water budget shut down \n
            enable water rationing \n

        set up POC 2 \n
            enable POC 2 \n
            assign master valve TMV0004 and flow meter TWF0004 to POC 2 \n
            assign POC 2 a target flow of 500 \n
            assign POC 2 to mainline 8 \n
            set POC priority to 2-medium \n
            set high flow limit to 550 and enable high flow shut down \n
            set unscheduled flow limit to 10 and enable unscheduled flow shut down \n
            set water budget to 100000 and enable the water budget shut down \n
            enable water rationing \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_flow_meter_to_point_of_control(
                _flow_meter_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].points_of_control[1].set_high_flow_limit(_limit=550,
                                                                                    with_shutdown_enabled=True)
            self.config.BaseStation3200[1].points_of_control[1].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=True)
            # Add POC 1 to Water Source 1
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=1)

            # Add & Configure POC 8
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=8)
            self.config.BaseStation3200[1].points_of_control[8].set_enabled()
            self.config.BaseStation3200[1].points_of_control[8].add_master_valve_to_point_of_control(
                _master_valve_address=2)
            self.config.BaseStation3200[1].points_of_control[8].add_flow_meter_to_point_of_control(_flow_meter_address=2)
            self.config.BaseStation3200[1].points_of_control[8].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].points_of_control[8].set_high_flow_limit(_limit=550,
                                                                                    with_shutdown_enabled=True)
            self.config.BaseStation3200[1].points_of_control[8].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=True)
            # Add POC 8 to Water Source 8
            self.config.BaseStation3200[1].water_sources[8].add_point_of_control_to_water_source(
                _point_of_control_address=8)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                    self.config.test_name,
                    sys._getframe().f_code.co_name,
                    date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                    str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control

        - set up main line 1 \n
            - set limit zones by flow to true \n
            - set the pipe fill time to 4 minutes \n
            - set the target flow to 500 \n
            - set the high variance limit to 5% and enable the high variance shut down \n
            - set the low variance limit to 20% and enable the low variance shut down \n

        - set up main line 8 \n
            - set limit zones by flow to true \n
            - set the pipe fill time to 1 minutes \n
            - set the target flow to 500 \n
            - set the high variance limit to 5% and enable the high variance shut down \n
            - set the low variance limit to 20% and enable the low variance shut down \n
        """

        # here we can either execute the following uncommented lines in procedural fashion, or we could re-init the
        # object, would have to import Mainline at the top, effectively accomplishing the same thing by:
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure ML 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=4)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=20)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[1].set_high_flow_variance_tier_one(_percent=5,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].mainlines[1].set_low_flow_variance_tier_one(_percent=20,
                                                                                       _with_shutdown_enabled=True)
            # Add ML 1 to POC 1
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)

            # Add & Configure ML 8
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=8)
            self.config.BaseStation3200[1].mainlines[8].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[8].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].mainlines[8].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[8].set_high_flow_variance_tier_one(_percent=10,
                                                                                        _with_shutdown_enabled=False)
            self.config.BaseStation3200[1].mainlines[8].set_low_flow_variance_tier_one(_percent=5,
                                                                                       _with_shutdown_enabled=False)
            # Add ML 8 to POC 8
            self.config.BaseStation3200[1].points_of_control[8].add_mainline_to_point_of_control(_mainline_address=8)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                    self.config.test_name,
                    sys._getframe().f_code.co_name,
                    date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                    str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone
        Program | Mainline  | Zones
        ---------------------------------------------
        1       | 1         | 1,2,3,4,5
        99      | 8         | 6,7,8,9,10
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Add ZN 1,2,3,4,5 to ML 1
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=1)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=2)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=3)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=4)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=5)

            # Add ZN 6,7,8,9,10 to ML 8
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=6)
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=7)
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=8)
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=9)
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=10)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_8(self):
        """
        Give each zone a design flow
            - zones 1 -5 20 gpm
            - zones 6 - 10 50 gpm
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Assign a design flow value to each zone so that they have a default setting
            self.config.BaseStation3200[1].zones[1].set_design_flow(_gallons_per_minute=2)
            self.config.BaseStation3200[1].zones[2].set_design_flow(_gallons_per_minute=2)
            self.config.BaseStation3200[1].zones[3].set_design_flow(_gallons_per_minute=2)
            self.config.BaseStation3200[1].zones[4].set_design_flow(_gallons_per_minute=2)
            self.config.BaseStation3200[1].zones[5].set_design_flow(_gallons_per_minute=2)
            self.config.BaseStation3200[1].zones[6].set_design_flow(_gallons_per_minute=5)
            self.config.BaseStation3200[1].zones[7].set_design_flow(_gallons_per_minute=5)
            self.config.BaseStation3200[1].zones[8].set_design_flow(_gallons_per_minute=5)
            self.config.BaseStation3200[1].zones[9].set_design_flow(_gallons_per_minute=5)
            self.config.BaseStation3200[1].zones[10].set_design_flow(_gallons_per_minute=5)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_9(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_10(self):
        """
        - set flow meter values
            - set flow meter 1 to 0 gpm
            - set flow meter 2 to 0 GPM
            - do a learn flow verify all 10 zones fail and give the failed learn flow message as well as checking the
            design to verify that after a failed learn flow the design flow value is not changed

        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=0.0)
            self.config.BaseStation3200[1].flow_meters[2].bicoder.set_flow_rate(_gallons_per_minute=0.0)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_11(self):
        """
        learn flow failure because of the program being manually set to stop
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            date_mngr.set_current_date_to_match_computer()
            self.config.BaseStation3200[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                               _time='07:59:00')

            self.config.BaseStation3200[1].programs[1].set_learn_flow_to_start()
            self.config.BaseStation3200[1].programs[99].set_learn_flow_to_start()

            self.config.BaseStation3200[1].do_increment_clock(minutes=2)

            # this is looking at zone concurrency because the controller can only run one zone at a time
            # only 1 zone should be learning flow
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_learning_flow()

            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_learning_flow()

            # Verify Program 1 zones 1 Which is on mainline 1 status is learning flow'
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_learning_flow()
            # Verify Program 1 zones 2-5 status'
            for zone in range(2, 6):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_waiting_to_water()

            # Verify Program 99 zones 7-10 status'
            for zone in range(7, 11):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].do_increment_clock(minutes=2)

            # stop both Programs and reset zone concurrency to 2 on the controller
            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[99].set_program_to_stop()

            # these message get generated because we stop the program therefore the message is zones didn't learn flow
            # we need to clear theme to continue with the test

            for zone in range(1, 6):
                self.config.BaseStation3200[1].zones[zone]\
                    .messages.verify_learn_flow_complete_error_message()
                self.config.BaseStation3200[1].zones[zone]\
                    .messages.clear_learn_flow_complete_error_message()

            for zone in range(6, 11):
                self.config.BaseStation3200[1].zones[zone]\
                    .messages.verify_learn_flow_complete_error_message()

                self.config.BaseStation3200[1].zones[zone]\
                    .messages.clear_learn_flow_complete_error_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_12(self):
        """
        learn flow failure because the flow meters are reading zero
        :return:
        :rtype:
        """
        try:
            # change controller to have two have a total of two concurrent zones this way both mainlines will learn flow
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=2)

            # increment clock to clear statuses
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)

            date_mngr.set_current_date_to_match_computer()
            self.config.BaseStation3200[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                               _time='07:59:00')
            # restart learn flow
            self.config.BaseStation3200[1].programs[1].set_learn_flow_to_start()
            self.config.BaseStation3200[1].programs[99].set_learn_flow_to_start()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_learning_flow()

            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_learning_flow()

             # Verify Program 1 zone 1 is learning flow concurrently with Program 99's zone 6
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_learning_flow()

            # Verify Program 1's other zones are waiting
            for zone in range(2, 6):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_waiting_to_water()

            # Verify Program 99 zones 6 Which is on mainline 8 status is learning flow'
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_learning_flow()

            # Verify Program 99's other zones are waiting
            for zone in range(7, 11):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_waiting_to_water()

            not_done = True
            while not_done:

                zones_still_running = False
                # ------------------------------------------------------------------------------------------------------
                # for each zone:
                #   1. get current data from controller (for getting updated status)
                #   2. get current status
                #   3. set flag only if any zones are still running, we want to remain in the while loop checking status

                for zone in sorted(self.config.BaseStation3200[1].zones.keys()):
                    self.config.BaseStation3200[1].zones[zone].get_data()
                    _zone_status = self.config.BaseStation3200[1].zones[zone]\
                        .data.get_value_string_by_key(opcodes.status_code)

                    # for each zone get the ss per zone
                    #   update the attribute in the object
                    #       (this is so we can store it for what happened during the last run time)
                    if _zone_status == opcodes.learn_flow_active:
                        # using 60 so that we are in seconds
                        self.config.BaseStation3200[1].zones[zone].seconds_zone_ran += 60

                    # set flag not all zones are done
                    if _zone_status != opcodes.done_watering and _zone_status != opcodes.error:
                        zones_still_running = True

                        # flag to true until all zone are done:
                # ----- END FOR ----------------------------------------------------------------------------------------

                # this could say if zones_still_running:
                if zones_still_running:
                    not_done = True
                    self.config.BaseStation3200[1].do_increment_clock(minutes=1)
                    self.config.BaseStation3200[1].verify_date_and_time()
                else:
                    not_done = False

            # TODO have zones run until all are set to done than read messages
            # Verify and clear Program 1 zone program learn flow error messages
            for zone in sorted(self.config.BaseStation3200[1].programs[1].zone_programs.keys()):
                self.config.BaseStation3200[1].zones[zone]\
                    .messages.verify_learn_flow_complete_error_message()
                self.config.BaseStation3200[1].zones[zone]\
                    .messages.clear_learn_flow_complete_error_message()

            # Verify and clear Program 99 zone program learn flow error messages
            for zone in sorted(self.config.BaseStation3200[1].programs[99].zone_programs.keys()):
                self.config.BaseStation3200[1].zones[zone]\
                    .messages.verify_learn_flow_complete_error_message()
                self.config.BaseStation3200[1].zones[zone]\
                    .messages.clear_learn_flow_complete_error_message()

            # Verify and clear Program learn flow error messages
            for program in sorted(self.config.BaseStation3200[1].programs.keys()):
                self.config.BaseStation3200[1].programs[program].messages.verify_learn_flow_complete_errors_message()
                self.config.BaseStation3200[1].programs[program].messages.clear_learn_flow_complete_errors_message()

            # check to see if the time each zone took to learn flow is correct
            seconds_learning_flow = (self.config.BaseStation3200[1].mainlines[1].ft * 60) + 60
            for zone in range(1, 6):
                if seconds_learning_flow != self.config.BaseStation3200[1].zones[zone].seconds_zone_ran:
                    print ("Zone " + str(zone) + " took " + str(self.config.BaseStation3200[1].zones[zone]
                                                                .seconds_zone_ran) +
                           " seconds to learn flow and it should have taken " + str(seconds_learning_flow) + " seconds")
                else:
                    print ("Zone " + str(zone) + " took " + str(self.config.BaseStation3200[1].zones[zone]
                                                                .seconds_zone_ran) + " seconds to learn flow")

            seconds_learning_flow = (self.config.BaseStation3200[1].mainlines[8].ft * 60) + 60
            for zone in range(6, 11):
                if seconds_learning_flow != self.config.BaseStation3200[1].zones[zone].seconds_zone_ran:
                    print ("Zone " + str(zone) + " took " + str(self.config.BaseStation3200[1].zones[zone]
                                                                .seconds_zone_ran) +
                           " seconds to learn flow and it should have taken " + str(seconds_learning_flow))
                else:
                    print ("Zone " + str(zone) + " took " + str(self.config.BaseStation3200[1].zones[zone]
                                                                .seconds_zone_ran) + " seconds to learn flow")
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_13(self):
        """
        - set flow meter values
            - set flow meter 1 to 50 gpm
            - set flow meter 2 to 20 GPM
            - do a learn flow verify all 10 zones fail and give the failed learn flow message as well as checking the
            design to verify that after a failed learn flow the design flow value is not changed

        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=50.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].flow_meters[2].bicoder.set_flow_rate(_gallons_per_minute=20.0)
            self.config.BaseStation3200[1].flow_meters[2].bicoder.self_test_and_update_object_attributes()


        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_14(self):
        """
        learn flow success
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            date_mngr.set_current_date_to_match_computer()
            self.config.BaseStation3200[1].set_date_and_time(_date=date_mngr.curr_day.date_string_for_controller(),
                                                               _time='07:59:00')
            self.config.BaseStation3200[1].programs[1].set_learn_flow_to_start()
            self.config.BaseStation3200[1].programs[99].set_learn_flow_to_start()
            # TODO read how long each zone should run and than calculate against runtime plus pipe file time
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            not_done = True
            while not_done:

                zones_still_running = False
                # ------------------------------------------------------------------------------------------------------
                # for each zone:
                #   1. get current data from controller (for getting updated status)
                #   2. get current status
                #   3. set flag only if any zones are still running, we want to remain in the while loop checking status
                for zone in sorted(self.config.BaseStation3200[1].zones.keys()):
                    self.config.BaseStation3200[1].zones[zone].get_data()
                    _zone_status = self.config.BaseStation3200[1].zones[zone]\
                        .data.get_value_string_by_key(opcodes.status_code)

                    # for each zone get the ss per zone
                    #   update the attribute in the object
                    #       (this is so we can store it for what happened during the last run time)
                    if _zone_status == opcodes.watering or _zone_status == opcodes.learn_flow_active:
                        # using 60 so that we are in seconds
                        self.config.BaseStation3200[1].zones[zone].seconds_zone_ran += 60

                    # set flag not all zones are done
                    if _zone_status != opcodes.done_watering and _zone_status != opcodes.error:
                        zones_still_running = True

                        # flag to true until all zone are done:
                # ----- END FOR ----------------------------------------------------------------------------------------

                # this could say if zones_still_running:
                if zones_still_running:
                    not_done = True
                    self.config.BaseStation3200[1].do_increment_clock(minutes=1)
                    self.config.BaseStation3200[1].verify_date_and_time()
                else:
                    not_done = False

            # Verify and clear Program 1 zone program learn flow error messages
            for zone in range(1, 6):
                self.config.BaseStation3200[1].zones[zone].update_design_flow_value_after_learn_flow(
                    _flow_meter_address=1)
            for zone in range(6, 11):
                self.config.BaseStation3200[1].zones[zone].update_design_flow_value_after_learn_flow(
                    _flow_meter_address=2)

            for zone in sorted(self.config.BaseStation3200[1].programs[1].zone_programs.keys()):
                self.config.BaseStation3200[1].zones[zone]\
                    .messages.verify_learn_flow_complete_success_message()
                self.config.BaseStation3200[1].zones[zone]\
                    .messages.clear_learn_flow_complete_success_message()

            # Verify and clear Program 99 zone program learn flow error messages
            for zone in sorted(self.config.BaseStation3200[1].programs[99].zone_programs.keys()):
                self.config.BaseStation3200[1].zones[zone]\
                    .messages.verify_learn_flow_complete_success_message()
                self.config.BaseStation3200[1].zones[zone]\
                    .messages.clear_learn_flow_complete_success_message()

            # Verify and clear Program learn flow error messages
            for program in sorted(self.config.BaseStation3200[1].programs.keys()):
                self.config.BaseStation3200[1].programs[program]\
                    .messages.verify_learn_flow_complete_success_message()
                self.config.BaseStation3200[1].programs[program]\
                    .messages.clear_learn_flow_complete_success_message()
            # check to see if the time each zone took to learn flow is correct
            seconds_learning_flow = (self.config.BaseStation3200[1].mainlines[1].ft * 60) + 60
            for zone in range(1, 6):
                if seconds_learning_flow != self.config.BaseStation3200[1].zones[zone].seconds_zone_ran:
                    print ("Zone " + str(zone) + " took " +
                           str(self.config.BaseStation3200[1].zones[zone]
                               .seconds_zone_ran) + " seconds to learn flow and it should have taken "
                           + str(seconds_learning_flow) + " seconds")
                else:
                    print ("Zone " + str(zone) + " took " +
                           str(self.config.BaseStation3200[1].zones[zone].seconds_zone_ran) +" seconds to learn flow")

            seconds_learning_flow = (self.config.BaseStation3200[1].mainlines[8].ft * 60) + 60
            for zone in range(6, 11):
                if seconds_learning_flow != self.config.BaseStation3200[1].zones[zone].seconds_zone_ran:
                    print ("Zone " + str(zone) + " took " +
                           str(self.config.BaseStation3200[1].zones[zone].seconds_zone_ran) +
                           " seconds to learn flow and it should have taken " + str(seconds_learning_flow) + " seconds")
                else:
                    print ("Zone " + str(zone) + " took " +
                           str(self.config.BaseStation3200[1].zones[zone].seconds_zone_ran) + " seconds to learn flow")
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_15(self):
        """
        verify all zones are done watering
        Verify master valve 1 is off
        Verify master valve 2 is off
        verify and clear unscheduled flow  \n
            - because the first set of zones got done before the second set and there is still water flowing to poc 8
            - a unscheduled flow is triggered on POC 8
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for zone in range(1, 11):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()

            self.config.BaseStation3200[1].water_sources[8].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].points_of_control[8].statuses.verify_status_is_flow_fault()
            self.config.BaseStation3200[1].mainlines[8].statuses.verify_status_is_flow_fault()
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_off()

            self.config.BaseStation3200[1].points_of_control[8].messages.verify_unscheduled_flow_shutdown_message()
            self.config.BaseStation3200[1].points_of_control[8].messages.clear_unscheduled_flow_shutdown_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_17(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
