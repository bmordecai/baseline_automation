import sys
from datetime import timedelta, datetime

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

# from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

import requests

__author__ = 'kent'


class ControllerUseCase43_1(object):
    """
    Test name:
        - Test Data Group Packets
    purpose:

    Coverage area: \n

    Date References:
        - configuration for script is located common\configuration_files\EPA_test_configuration.json
        - the devices and addresses range is read from the .json file

    """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        This will verify data packets on the 3200 controller
        This only verifies that the structure of the packet matches what is in the ERS. It does not test whether or
        not the data group values can be set.

        TODO: A TS packet for DG131 will not set the k value. This is a bug that has been logged, BM-1912.

        A use case that will be written in the future will test setting of values using a TS packet. This test
        sets values using a TS packet, but it sets the default values, which are already set using the test engine.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # sleep to wait for the controller to finish sending up data to BaseManager from the initial configuration

            minutes = 5.0
            print "Waiting " + str((minutes)) + " minutes for controller to send up all data from use case initialization"
            self.config.BaseStation3200[1].data_group_packets.resume_packet_logging()
            time.sleep(minutes * 60.0)
            self.config.BaseStation3200[1].data_group_packets.stop_packet_logging()

            # # verify packet 302
            self.config.BaseStation3200[1].moisture_sensors[1].packets.set_moisture_sensor_dg302()
            self.config.BaseStation3200[1].moisture_sensors[1].packets.verify_moisture_sensor_dg302()

            # # verify packet 101
            self.config.BaseStation3200[1].zones[1].packets.set_zone_dg101()
            self.config.BaseStation3200[1].zones[1].packets.verify_zone_dg101()

            # verify packet 111
            self.config.BaseStation3200[1].master_valves[1].packets.set_master_valve_dg111()
            self.config.BaseStation3200[1].master_valves[1].packets.verify_master_valve_dg111()

            # verify packet 131
            self.config.BaseStation3200[1].flow_meters[1].packets.set_flow_meter_dg131()
            self.config.BaseStation3200[1].flow_meters[1].packets.verify_flow_meter_dg131()

            # close logging connections
            self.config.BaseStation3200[1].data_group_packets.close_logging_connections()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))

            if self.config.BaseStation3200[1] is not None:
                if self.config.BaseStation3200[1].ser2.serial_conn.isOpen():
                    self.config.BaseStation3200[1].ser2.close_connection()

            raise Exception(e_msg)
