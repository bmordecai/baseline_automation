import sys
from time import sleep

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods
__author__ = "Eldin"


class ControllerUseCase23(object):
    """
    Test name: \n
        BaseManager Disconnect and Reconnect continuously. \n
    Purpose: \n

    Coverage area: \n
        - Disconnecting a 3200 from BaseManager, and then making sure it automatically reconnects. \n
            - Do this while we are using both static and dynamic IP addressing
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    #################################
    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)
                    # we have to set a long timeout on the serial port because the test runs for long periods of time
                    self.config.BaseStation3200[1].set_serial_port_timeout(timeout=5040)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        Wait 5 minutes in real time. \n
        Disconnect the controller from BaseManager. \n
        Wait another 5 minutes in real time. \n
        Connect the controller to BaseManager. \n
        Once we go through that process for 4 hours (240 minutes), change the way we connect to BaseManager.
            - If we used a dynamic IP address, change it to static.
            - If we used a static IP address, change it to dynamic.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            total_time = 0
            seconds_to_wait = 60
            minutes_until_switching_ip_type = 10

            # UPDATE - Ben - 11/2/17
            #       Added these two variables to help exit out of the while loop after we have disconnected/reconnected
            #       from BaseManager.
            #
            #       Minutes to run was set to 20 because it was switching IP types every 10 minutes. Thus, by running
            #       for 20 minutes, we have effectively tested reconnection for both static and dynamic IP connections.
            total_time_ran = 0
            minutes_to_run = 20

            while True:
                # Wait 5 minutes
                print "Waiting {0} seconds...".format(seconds_to_wait)
                time.sleep(seconds_to_wait)
                total_time += (seconds_to_wait / 60)

                # Disconnect the controller from BaseManager and then reconnect it
                self.config.BaseStation3200[1].basemanager_connection[1].disconnect_cn_from_bm()
                self.config.BaseStation3200[1].do_increment_clock(minutes=1)
                self.config.BaseStation3200[1].do_increment_clock(minutes=1)
                self.config.BaseStation3200[1].basemanager_connection[1].connect_cn_to_bm()
                self.config.BaseStation3200[1].do_increment_clock(minutes=1)

                # Wait 5 minutes
                print "Waiting {0} seconds...".format(seconds_to_wait)
                time.sleep(seconds_to_wait)
                total_time += (seconds_to_wait / 60)

                # Verify we are connected to BaseManager, if not, throw an error
                if not self.config.BaseStation3200[1].basemanager_connection[1].verify_cn_connected_to_bm():
                    e_msg = "Controller was not connected to BaseManager after we disconnected it, and then waited " \
                            "for it to reconnect."
                    raise Exception(e_msg)

                total_time_ran += total_time

                # Once our time passes the amount of time we wanted to wait before changing IP types, change the IP
                if total_time > minutes_until_switching_ip_type and total_time_ran < minutes_to_run:
                    # Reset the total time
                    total_time = 0
                    # Need to get the current controller BM configuration for verification.
                    controller_reply = self.config.BaseStation3200[1].ser.get_and_wait_for_reply("GET,BM")
                    dynamic_ip_boolean = controller_reply.get_value_string_by_key('UA')

                    if dynamic_ip_boolean == opcodes.false:
                        print "############## Changing from Dynamic IP addressing to Static IP addressing #############"
                        self.config.BaseStation3200[1].ser.send_and_wait_for_reply("SET,BM,UA=TR")
                        self.config.BaseStation3200[1].ser.send_and_wait_for_reply("SET,BM,AI=104.130.246.18")

                    else:
                        print "############## Changing from Static IP addressing to Dynamic IP addressing #############"
                        self.config.BaseStation3200[1].ser.send_and_wait_for_reply("SET,BM,UA=FA")

                # Else if we have run the full length of the test, exit
                elif total_time_ran == minutes_to_run:
                    # break out of while loop to finish test.
                    break
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
