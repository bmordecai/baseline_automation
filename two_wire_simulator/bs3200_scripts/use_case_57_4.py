import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration

# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr

from common import helper_methods

__author__ = 'Tim'


class ControllerUseCase57_4(object):
    """
    Test name:
        - CN UseCase57_3 Program Ignore Global Conditions: jumpers

    Purpose:
        - Set up a full configuration on the controller
        - Jumpers stop is ignored by program with ignore global set

    Coverage area:
        - jumper
        - ignore global conditions

    Areas not covered:
        - stop/pause conditions
        - event dates
        - rain delay days

    Test Configuration setup: \n
        1. zones 1-5 on program 1
        2. zones 6 to 10 on program 2
        3. program 1:
            - 8 am start time
            - concurrent zones = 5
        4. program 2:
            - 8 am start time
            - concurrent zones = 5
        5. controller:
            - concurrent zones = 15

    Test Steps - two cases to be tested:
        1. Test 1
            - set time and run through start time
            - verify all zones/programs watering
            - open flow jumper
            - run time
            - verify zones/programs stopped
        2. Test 2
            - set program 1 to ignore global conditions
            - set time and run through start time
            - verify all zones/programs watering
            - open flow jumper
            - run time
            - verify zones/programs running/stopped
        3. Test 3
            - set time and run through start time
            - verify all zones/programs watering
            - open rain jumper
            - run time
            - verify zones/programs stopped
        4. Test 4
            - set program 1 to ignore global conditions
            - set time and run through start time
            - verify all zones/programs watering
            - open rain jumper
            - run time
            - verify zones/programs running/stopped
        5. Test 5
            - set time and run through start time
            - verify all zones/programs watering
            - open pause jumper
            - run time
            - verify zones/programs paused
        6. Test 6
            - set program 1 to ignore global conditions
            - set time and run through start time
            - verify all zones/programs watering
            - open pause jumper
            - run time
            - verify zones/programs running/paused

    """

    ###############################
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase44' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    ###############################
    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break

                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        setup controller
        ############################
        - current zones
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=15)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_2(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=5)

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation3200[1].programs[2].set_enabled()
            self.config.BaseStation3200[1].programs[2].set_max_concurrent_zones(_number_of_zones=5)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_3(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add zone programs to Program 1
            for zone_ad in range(1, 6):
                self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=zone_ad)
                self.config.BaseStation3200[1].programs[1].zone_programs[zone_ad].set_run_time(_minutes=10)
                self.config.BaseStation3200[1].programs[1].zone_programs[zone_ad].set_cycle_time(_minutes=10)
                self.config.BaseStation3200[1].programs[1].zone_programs[zone_ad].set_soak_time(_minutes=10)

            # Add zone programs to Program 2
            for zone_ad in range(6, 11):
                self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=zone_ad)
                self.config.BaseStation3200[1].programs[2].zone_programs[zone_ad].set_run_time(_minutes=10)
                self.config.BaseStation3200[1].programs[2].zone_programs[zone_ad].set_cycle_time(_minutes=10)
                self.config.BaseStation3200[1].programs[2].zone_programs[zone_ad].set_soak_time(_minutes=10)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ###############################
        Test 1 -  start program verify zones stopped on flow jumper
        ###############################

        1. Test 1
            - set time and run through start time
            - verify all zones/programs watering
            - open flow jumper
            - run time
            - verify zones/programs stopped

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set time to 7:59 am
            self.config.BaseStation3200[1].set_date_and_time(_date='1/31/2018', _time='7:59:00')

            # clear to ignore global conditions
            self.config.BaseStation3200[1].programs[1].set_to_obey_global_conditions()

            # start both programs = everything running
            self.config.BaseStation3200[1].do_increment_clock(minutes=3)

            # verify zones
            for zone in range(1, 6):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(6, 11):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # flow jumper open
            self.config.BaseStation3200[1].set_flow_jumper_to_open()

            # advance to cause conditions to execute
            self.config.BaseStation3200[1].do_increment_clock(minutes=3)

            # verify zones
            for zone in range(1, 6):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(6, 11):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()

            # verify message
            self.config.BaseStation3200[1].messages.verify_flow_jumper_stopped_message()

            # clean up after this step, ready for next
            self.config.BaseStation3200[1].set_flow_jumper_to_closed()

            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            self.config.BaseStation3200[1].messages.clear_flow_jumper_stopped_message()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ###############################
        Test 2 -  start program verify zones running if ignore flow jumper
        ###############################

        2. Test 2
            - set program 1 to ignore global conditions
            - set time and run through start time
            - verify all zones/programs watering
            - open flow jumper
            - run time
            - verify:
                - Zones 1-5 are still watering because program 1 was set to ignore global conditions.
                - Zones 6-10 are not watering because program 2 does not ignore global conditions, and the flow jumper
                  was opened.

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set time to 7:59 am
            self.config.BaseStation3200[1].set_date_and_time(_date='1/31/2018', _time='7:59:00')

            # set to ignore global conditions
            self.config.BaseStation3200[1].programs[1].set_ignore_global_conditions()

            # start both programs = everything running
            self.config.BaseStation3200[1].do_increment_clock(minutes=3)

            # verify zones
            for zone in range(1, 6):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(6, 11):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # flow jumper open
            self.config.BaseStation3200[1].set_flow_jumper_to_open()

            # advance clock to check new state
            self.config.BaseStation3200[1].do_increment_clock(minutes=3)

            # verify zones
            for zone in range(1, 6):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(6, 11):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()

            # verify message
            self.config.BaseStation3200[1].messages.verify_flow_jumper_stopped_message()

            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()

            self.config.BaseStation3200[1].set_flow_jumper_to_closed()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            self.config.BaseStation3200[1].messages.clear_flow_jumper_stopped_message()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ###############################
        Test 3 -  start program verify zones stopped on rain jumper
        ###############################

        3. Test 3
            - set time and run through start time
            - verify all zones/programs watering
            - open rain jumper
            - run time
            - verify zones/programs stopped

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set time to 7:59 am
            self.config.BaseStation3200[1].set_date_and_time(_date='1/31/2018', _time='7:59:00')

            # clear to ignore global conditions
            self.config.BaseStation3200[1].programs[1].set_to_obey_global_conditions()

            # start both programs = everything running
            self.config.BaseStation3200[1].do_increment_clock(minutes=3)

            # verify zones
            for zone in range(1, 6):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(6, 11):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # flow jumper open
            self.config.BaseStation3200[1].set_rain_jumper_to_open()

            # advance to cause conditions to execute
            self.config.BaseStation3200[1].do_increment_clock(minutes=3)

            # verify zones
            for zone in range(1, 6):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(6, 11):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()

            # verify message
            self.config.BaseStation3200[1].messages.verify_rain_stop_jumper_message()

            # clean up after this step, ready for next
            self.config.BaseStation3200[1].set_rain_jumper_to_closed()

            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ###############################
        Test 7 -  start program verify zones running if ignore rain jumper
        ###############################

        4. Test 4
            - set program 1 to ignore global conditions
            - set time and run through start time
            - verify all zones/programs watering
            - open rain jumper
            - run time
            - verify:
                - Zones 1-5 are still watering because program 1 was set to ignore global conditions.
                - Zones 6-10 are not watering because program 2 does not ignore global conditions, and rain jumper was
                  opened.

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set time to 7:59 am
            self.config.BaseStation3200[1].set_date_and_time(_date='1/31/2018', _time='7:59:00')

            # set to ignore global conditions
            self.config.BaseStation3200[1].programs[1].set_ignore_global_conditions()

            # start both programs = everything running
            self.config.BaseStation3200[1].do_increment_clock(minutes=3)

            # verify zones
            for zone in range(1, 6):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(6, 11):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # flow jumper open
            self.config.BaseStation3200[1].set_rain_jumper_to_open()

            # advance clock to check new state
            self.config.BaseStation3200[1].do_increment_clock(minutes=3)

            # verify zones
            for zone in range(1, 6):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(6, 11):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()

            # clean up after this step, ready for next
            self.config.BaseStation3200[1].set_rain_jumper_to_closed()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        Test 5 -  start program verify zones pause on pause jumper
        ###############################

        5. Test 5
            - set time and run through start time
            - verify all zones/programs watering
            - open pause jumper
            - run time
            - verify zones/programs paused

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set time to 7:59 am
            self.config.BaseStation3200[1].set_date_and_time(_date='1/31/2018', _time='7:59:00')

            # clear to ignore global conditions
            self.config.BaseStation3200[1].programs[1].set_to_obey_global_conditions()

            # start both programs = everything running
            self.config.BaseStation3200[1].do_increment_clock(minutes=3)

            # verify zones
            for zone in range(1, 6):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(6, 11):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # flow jumper open
            self.config.BaseStation3200[1].set_pause_jumper_to_open()

            # advance to cause conditions to execute
            self.config.BaseStation3200[1].do_increment_clock(minutes=3)

            # verify zones
            for zone in range(1, 6):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_paused()
            for zone in range(6, 11):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_paused()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_paused()

            # verify message
            self.config.BaseStation3200[1].messages.verify_pause_jumper_message()

            # clean up after this step, ready for next
            self.config.BaseStation3200[1].set_pause_jumper_to_closed()

            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            self.config.BaseStation3200[1].messages.clear_pause_jumper_message()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        ###############################
        Test 6 -  start program verify zones running if ignore pause jumper
        ###############################

        6. Test 6
            - set program 1 to ignore global conditions
            - set time and run through start time
            - verify all zones/programs watering
            - open pause jumper
            - run time
            - verify:
                - Zones 1-5 are still watering because program 1 was set to ignore global conditions.
                - Zones 6-10 are paused because program 2 does not ignore global conditions, and the pause jumper was
                  opened.

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set time to 7:59 am
            self.config.BaseStation3200[1].set_date_and_time(_date='1/31/2018', _time='7:59:00')

            # set to ignore global conditions
            self.config.BaseStation3200[1].programs[1].set_ignore_global_conditions()

            # start both programs = everything running
            self.config.BaseStation3200[1].do_increment_clock(minutes=3)

            # verify zones
            for zone in range(1, 6):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(6, 11):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            # flow jumper open
            self.config.BaseStation3200[1].set_pause_jumper_to_open()

            # advance clock to check new state
            self.config.BaseStation3200[1].do_increment_clock(minutes=3)

            # verify zones
            for zone in range(1, 6):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(6, 11):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_paused()

            # verify programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_paused()

            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()

            self.config.BaseStation3200[1].set_pause_jumper_to_closed()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            self.config.BaseStation3200[1].messages.clear_pause_jumper_message()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
