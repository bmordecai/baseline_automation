import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration

# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr

__author__ = 'Tige'


class ControllerUseCase5_3(object):
    """
    Test name:
        - Using multiple water source budgets to a single point mainline with priorities

    User Story: \n
        1)  The user wants to set a monthly amount of water that can be used for irrigation. The user wants to be able\n
            to chose weather irrigation is terminated or not when the budge is met. The user also wants to be\n
            notified with messages and statuses when the monthly allotted water usage has been reached \n
        2)  The user wants to have to different water supply delivering water to there property with separate \n
            amounts of allocated water  for each supply. If either source reaches its allocated amount of water \n
            they want it to stop supplying water. But is the other supplies still have water to give than they want\n
            the system to maintain watering
        3)  As a user I want to use my least expensive water first. if they run out of that water they want to be \n
            able to have the system switch the water source for them. In some cases they want the water source with \n
            the least expensive water to shut off and in other cases they want that water source to shut off and the \n
            next water source to turn on. They also want to be able to decide when the cheep water runs out based on a\n
            known used amount or or water tier pricing is met that the system will notify them and switch over the \n
            more expensive water \n

    Coverage area of feature: \n
        This test covers 2nd point in the user story:
            Two water sources with with set water budgets and priorities \

    Test explanation:
        Two water sources will be setup to verify the behavior \n
        First water source will be set to have shutdown enabled \n
        Second water source with have the shutdown disabled \n
        Each water source will be assigned a single poc \n
        The two water sources will share a single mainline \n
        Three zones will be assigned to the mainline with one being a primary and the other two being linked \n
        Only one program will be used \n

        First Scenario:
            The program will be told to start manually
            - Second water source will have a higher priority than the first
                -  the water source will hit the budget value with shutdown is enabled
                - water source will no longer supply water
                - message will be generated
                - operate status will be displayed
                - the first water source will now supply water until it run to it budget and than it will turn off
        Second Scenario:
            - Second water source will have a higher priority than the first
                - the water source will hit the budget value with shutdown is disabled
                - water source will continue to supply water
                - message will be generated
                - operate status will be displayed
                - the first water source will not supply water even though the priority says to change water sources

    Date References:
        - configuration for script is located common\configuration_files\using_real_time_flow.json
        - the devices and addresses range is read from the .json file


    """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                     # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """

        # TODO need to have concurrent zones per program added

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        program_6am_start_time = [360]
        full_open_water_windows = ['111111111111111111111111']

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_water_window(_ww=full_open_water_windows)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_6am_start_time)
            self.config.BaseStation3200[1].programs[1].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[1].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(_sun=True,
                                                                                                           _mon=True,
                                                                                                           _tues=True,
                                                                                                           _wed=True,
                                                                                                           _thurs=True,
                                                                                                           _fri=True,
                                                                                                           _sat=True)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=5)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(
                _primary_zone=1,_tracking_ratio=100)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(
                _primary_zone=1,_tracking_ratio=100)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_3(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        set up water sources \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Priority 2 (lower priority) water source
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[1].set_water_rationing_to_enabled()

            # Priority 1 (highest priority) water source
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=2)
            self.config.BaseStation3200[1].water_sources[2].set_enabled()
            self.config.BaseStation3200[1].water_sources[2].set_priority(_priority_for_water_source=1)
            self.config.BaseStation3200[1].water_sources[2].set_water_rationing_to_enabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_pump_to_point_of_control(_pump_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_flow_meter_to_point_of_control(
                _flow_meter_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=500)

            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=2)
            self.config.BaseStation3200[1].water_sources[2].add_point_of_control_to_water_source(
                _point_of_control_address=2)
            self.config.BaseStation3200[1].points_of_control[2].set_enabled()
            self.config.BaseStation3200[1].points_of_control[2].add_master_valve_to_point_of_control(
                _master_valve_address=2)
            self.config.BaseStation3200[1].points_of_control[2].add_pump_to_point_of_control(_pump_address=2)
            self.config.BaseStation3200[1].points_of_control[2].add_flow_meter_to_point_of_control(
                _flow_meter_address=2)
            self.config.BaseStation3200[1].points_of_control[2].set_target_flow(_gpm=500)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)
            self.config.BaseStation3200[1].points_of_control[2].add_mainline_to_point_of_control(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_enabled()
            self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=4)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_true()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=zone_address)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
            #################################

    def step_9(self):
        """
        set the initial water budget for each Water source to used in budget \n
        Set the flow meter to zero usage this gives us a starting point \n
        set the shutdowns to be True \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].water_sources[1].set_monthly_watering_budget(_budget=1000,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_water_usage(_water_usage=0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].water_sources[2].set_monthly_watering_budget(_budget=2400,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].flow_meters[2].bicoder.set_water_usage(_water_usage=0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                    self.config.test_name,
                    sys._getframe().f_code.co_name,
                    date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                    str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        - Run first Scenario
           - set each flow meter to have a set GPM
           - calculate how much time it take to reach the budget point for each water source
           - verify that when the first budget is met that all devices shut down because shut down is enabled
           - also verify message is triggered
           - continue to run the second water source until its budget is met
           - verify that when the second budget is met that all devices do not shut down because shut down is disabled
           - verify message is triggered
        \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # set an initial time on the controller
            self.config.BaseStation3200[1].set_date_and_time(_date='08/27/2014', _time='23:45:00')
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)
            self.config.BaseStation3200[1].verify_date_and_time()

            # set the flow meter GPM
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=0)
            self.config.BaseStation3200[1].flow_meters[2].bicoder.set_flow_rate(_gallons_per_minute=400)

            # this gets the water source budget value
            second_ws_budget = int(self.config.BaseStation3200[1].water_sources[2].wb)
            # this gets the value the flow meter was set to
            second_flow_meter_rate = int(self.config.BaseStation3200[1].flow_meters[2].bicoder.vr)
            # this calculates how many minutes the water source has to run in order to reach the budget Value
            total_minutes_for_second_water_budget = second_ws_budget / second_flow_meter_rate

            self.config.BaseStation3200[1].flow_meters[1].bicoder.verify_water_usage()
            self.config.BaseStation3200[1].flow_meters[2].bicoder.verify_water_usage()

            # manually start program
            self.config.BaseStation3200[1].programs[1].set_program_to_start()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            # verify first water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()

            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_off()

            # Verify first minute status.  WS2 is higher priority than WS1, so it should be supplying the water
            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Run the clock until we expect the second water source to be used up
            # (we've already run one minute, so run the rest)
            self.config.BaseStation3200[1].do_increment_clock(minutes=total_minutes_for_second_water_budget-1)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].flow_meters[2].bicoder.set_flow_rate(_gallons_per_minute=0.0)
            self.config.BaseStation3200[1].flow_meters[2].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            # set the water source object to have the correct water used value so that the message can be compared
            self.config.BaseStation3200[1].flow_meters[2].bicoder.vg = second_ws_budget
            self.config.BaseStation3200[1].flow_meters[2].bicoder.verify_water_usage()
            # verify that WS2 has tossed an over budget message, and that its downstream POC has shut down
            self.config.BaseStation3200[1].water_sources[2].messages.\
                verify_exceed_monthly_budget_with_shutdown_message()

            # Verify water source 2 shuts down.  WS2 is higher priority than WS1, so it should be supplying the water
            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_over_budget()
            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_off()

            # WS1 starts flowing, WS2 stops
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=200)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()

            # verify first water source starts running
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()

            # this gets the water source budget value
            first_ws_budget = int(self.config.BaseStation3200[1].water_sources[1].wb)
            # this gets the value the flow meter was set to
            first_flow_meter_rate = int(self.config.BaseStation3200[1].flow_meters[1].bicoder.vr)
            # this calculates how many minutes the water source has to run in order to reach the budget Value
            total_minutes_for_first_water_budget = first_ws_budget / first_flow_meter_rate

            # run to use all water budget'
            self.config.BaseStation3200[1].do_increment_clock(minutes=total_minutes_for_first_water_budget)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=0.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()

            # next minute to post message
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            # set the water source object to have the correct water used value so that the message can be compared
            self.config.BaseStation3200[1].flow_meters[1].bicoder.vg = (total_minutes_for_first_water_budget) * first_flow_meter_rate
            self.config.BaseStation3200[1].flow_meters[1].bicoder.verify_water_usage()

            # verify that WS1 has tossed an over budget message
            self.config.BaseStation3200[1].water_sources[1].messages.verify_exceed_monthly_budget_with_shutdown_message()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()

            # verify first water source, etc
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_over_budget()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_off()

            # verify second water source, etc
            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_over_budget()
            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_off()

            # clear both messages
            self.config.BaseStation3200[1].water_sources[1].messages.clear_exceed_monthly_budget_with_shutdown_message()
            self.config.BaseStation3200[1].water_sources[2].messages.clear_exceed_monthly_budget_with_shutdown_message()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        Reset the initial water budget for each Water source to used in budget \n
        Set the flow meter to zero usage this gives us a starting point \n
        set the shutdowns to be false \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].water_sources[1].set_monthly_watering_budget(_budget=1000,
                                                                                        _with_shutdown_enabled=False)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_water_usage(_water_usage=0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].water_sources[2].set_monthly_watering_budget(_budget=2400,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].flow_meters[2].bicoder.set_water_usage(_water_usage=0)
            self.config.BaseStation3200[1].flow_meters[2].bicoder.self_test_and_update_object_attributes()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                    self.config.test_name,
                    sys._getframe().f_code.co_name,
                    date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                    str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        - Run Second Scenario
           - set each flow meter to have a set GPM
           - calculate how much time it take to reach the budget point for each water source
           - verify that when the first budget is met that all devices shut down because shut down is enabled
           - also verify message is triggered
           - continue to run the second water source until its budget is met
           - verify that when the second budget is met that all devices do not shut down because shut down is disabled
           - verify message is triggered
        \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # set the flow meter GPM
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=0)
            self.config.BaseStation3200[1].flow_meters[2].bicoder.set_flow_rate(_gallons_per_minute=400)

            # this gets the water source budget value
            second_ws_budget = int(self.config.BaseStation3200[1].water_sources[2].wb)
            # this gets the value the flow meter was set to
            second_flow_meter_rate = int(self.config.BaseStation3200[1].flow_meters[2].bicoder.vr)
            # this calculates how many minutes the water source has to run in order to reach the budget Value
            total_minutes_for_second_water_budget = second_ws_budget / second_flow_meter_rate

            self.config.BaseStation3200[1].flow_meters[1].bicoder.verify_water_usage()
            self.config.BaseStation3200[1].flow_meters[2].bicoder.verify_water_usage()

            # manually start program
            self.config.BaseStation3200[1].programs[1].set_program_to_start()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            # verify first water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()

            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_off()

            # Verify first minute status.  WS2 is higher priority than WS1, so it should be supplying the water
            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Run the clock until we expect the second water source to be used up
            # (we've already run one minute, so run the rest)
            self.config.BaseStation3200[1].do_increment_clock(minutes=total_minutes_for_second_water_budget - 1)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].flow_meters[2].bicoder.set_flow_rate(_gallons_per_minute=0.0)
            self.config.BaseStation3200[1].flow_meters[2].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            # set the water source object to have the correct water used value so that the message can be compared
            self.config.BaseStation3200[1].flow_meters[2].bicoder.vg = second_ws_budget
            self.config.BaseStation3200[1].flow_meters[2].bicoder.verify_water_usage()
            # verify that WS2 has tossed an over budget message, and that its downstream POC has shut down
            self.config.BaseStation3200[1].water_sources[2].messages. \
                verify_exceed_monthly_budget_with_shutdown_message()

            # Verify water source 2 shuts down.  WS2 is higher priority than WS1, so it should be supplying the water
            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_over_budget()
            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].flow_meters[2].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_off()

            # WS1 starts flowing, WS2 stops
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=200)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()

            # verify first water source starts running
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].flow_meters[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()

            # this gets the water source budget value
            first_ws_budget = int(self.config.BaseStation3200[1].water_sources[1].wb)
            # this gets the value the flow meter was set to
            first_flow_meter_rate = int(self.config.BaseStation3200[1].flow_meters[1].bicoder.vr)
            # this calculates how many minutes the water source has to run in order to reach the budget Value
            total_minutes_for_first_water_budget = first_ws_budget / first_flow_meter_rate

            # run to use all water budget'
            self.config.BaseStation3200[1].do_increment_clock(minutes=total_minutes_for_first_water_budget)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=0.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()

            # next minute to post message
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            # set the water source object to have the correct water used value so that the message can be compared
            self.config.BaseStation3200[1].flow_meters[1].bicoder.vg = (total_minutes_for_first_water_budget) * first_flow_meter_rate
            self.config.BaseStation3200[1].flow_meters[1].bicoder.verify_water_usage()

            # verify that WS1 has tossed an over budget message
            self.config.BaseStation3200[1].water_sources[1].messages.verify_exceed_monthly_budget_message()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # verify first water source, etc
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()

            # verify second water source, etc
            self.config.BaseStation3200[1].water_sources[2].statuses.verify_status_is_over_budget()
            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[2].statuses.verify_status_is_off()

            # clear both messages
            self.config.BaseStation3200[1].water_sources[1].messages.clear_exceed_monthly_budget_message()
            self.config.BaseStation3200[1].water_sources[2].messages.clear_exceed_monthly_budget_with_shutdown_message()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
