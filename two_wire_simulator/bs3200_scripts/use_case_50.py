import sys
from time import sleep

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *
from datetime import datetime,timedelta
from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

__author__ = "Eldin"


class ControllerUseCase50(object):
    """
    Test name: \n
        Program Over Runs \n
    Purpose: \n
        To verify that program over runs work . \n
    Coverage area: \n
        1. Able to set up program with multiple start time\ \n
        2. Verify that Programs causes a program over run \n
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)
                    self.config.BaseStation3200[1].set_serial_port_timeout(7200)
                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                     # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        # These are set in the PG3200 object

        program_start_time_8am_9am = [480, 540]

        program_water_windows_weekly = ["011111111011111111111110"]
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_start_time_8am_9am)
            self.config.BaseStation3200[1].programs[1].set_water_window(_ww=program_water_windows_weekly)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(
                _sun=True, _mon=True, _tues=True, _wed=True, _thurs=True, _fri=True, _sat=True
            )

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=30)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_run_time(_minutes=30)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_run_time(_minutes=30)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

            #################################

    #################################
    def step_4(self):
        """
        - Get the data of each zone and program on the controller. \n
         - Program start time:
             - Program 1 start times 8:00pm and 9:00pm
         - Program 1 Water Windows:
        - Verify Statuses: \n
            - Program 1: Done Watering because its start time hasn't been triggered yet. \n
                - Zone 1: Done Watering because its start time hasn't been triggered yet. \n
                - Zone 2: Done Watering because its start time hasn't been triggered yet. \n
                - Zone 3: Done Watering because its start time hasn't been triggered yet. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set the controller's time to 7:59:00 am
            self.config.BaseStation3200[1].set_date_and_time(_date='5/13/2012', _time='7:59:00')
            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()

            # Verify all Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        - Increment the clock by 2 minutes \n
        - Set the date to 5/13/2012 (Sunday) and the time to 22:01:00, which triggers both program's run-times. \n
         - Program start time:
             - Program 1 start times 8:00AM and 9:00AM

        - Program 1 start at 8:00am and 9:00am
         - Verify Statuses: \n
            - Program 1: Running because it's runtime of 20:00 was triggered. \n
                - Water window is open
                - Zone 1: Watering because the program it is attached to was triggered. \n
                - Zone 2: Waiting because its program's runtime was triggered, but max concurrent zones is 1. \n
                - Zone 3: Waiting because its program's runtime was triggered, but max concurrent zones is 1. \n

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the controller by two minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)
            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            # Verify all Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        - Increment the clock by 30 minutes \n
        - because a second start time was hit on program \n

        - Verify restart
        - Verify Statuses: \n
            - Zone 1: Done because it ran for its runtime of 30 minutes. \n
            - Zone 2: Watering. \n
            - Zone 3: Waiting to water \n

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the controller by one hour
            self.config.BaseStation3200[1].do_increment_clock(minutes=30)
            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()


            # Verify all Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        - Increment the clock by 30 minutes \n
        - because a second start time was hit on program \n

        - Verify restart
        - Verify Statuses: \n watering soaking waiting
            - Zone 1: Done because it ran for its runtime of 30 minutes. \n
            - Zone 2: Done because it ran for its runtime of 30 minutes. \n
            - Zone 3: Paused because Program 1 was paused due to its water window. \n

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the controller by one hour
            self.config.BaseStation3200[1].do_increment_clock(minutes=30)
            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].messages.verify_program_overrun_message()
            self.config.BaseStation3200[1].programs[1].messages.verify_paused_water_window_message()
            # Verify all Programs
            # Increment the controller by one hour
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_paused()

            # Verify all Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_paused()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        - Increment the clock by 30 minutes \n
        - Increment the clock by 1 hour \n
        - because a second start time was hit on program \n
        - Verify restart
        - Verify Statuses: \n
            - Zone 1: Done because it ran for its runtime of 1 hour. \n
            - Zone 2: Watering
            - Zone 3: Waiting to water

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Increment the controller by one hour
            self.config.BaseStation3200[1].do_increment_clock(hours=1)
            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # Verify all Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]