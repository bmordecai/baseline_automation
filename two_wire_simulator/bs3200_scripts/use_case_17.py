import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

__author__ = 'Eldin'


class ControllerUseCase17(object):
    """
    Test name:
        - CN UseCase17 Soak Cycles
    purpose:
        - set up a zones on the controller and attach them to a program
            - Make two zones timed
            - Make one zone the primary zone
                - Make the other two into linked zones with different ratios
    Coverage Area: \n
        1. Able to disable zones \n
        2. Able to set zone concurrency \n
        3. Able to set up a soak cycle and verify that it runs properly \n
        4. able to set up primary zones and linked zones
        5. verify linked zones follow the primary zone ratios
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        date_mngr.set_date_to_default()
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                     # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # set controller to run 2 zones at a time one from each program
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=2)

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=[1200])

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation3200[1].programs[2].set_enabled()
            self.config.BaseStation3200[1].programs[2].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[2].set_start_times(_st_list=[1200])
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=60)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=25)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1, _tracking_ratio=200)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_as_linked_zone(_primary_zone=1, _tracking_ratio=50)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[2].zone_programs[4].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[2].zone_programs[4].set_run_time(_minutes=60)
            self.config.BaseStation3200[1].programs[2].zone_programs[4].set_cycle_time(_minutes=15)
            self.config.BaseStation3200[1].programs[2].zone_programs[4].set_soak_time(_minutes=15)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=5)
            self.config.BaseStation3200[1].programs[2].zone_programs[5].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[2].zone_programs[5].set_run_time(_minutes=12)
            self.config.BaseStation3200[1].programs[2].zone_programs[5].set_cycle_time(_minutes=2)
            self.config.BaseStation3200[1].programs[2].zone_programs[5].set_soak_time(_minutes=5)

            self.config.BaseStation3200[1].zones[5].set_disabled()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller agaist information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        Set the date and time to some arbitrary date/time \n
        Start Program 1, which has all 5 zones on it \n
        Loop through until every zone is done watering \n
            - In each loop check the status of the zone. \n
            - Since we are incrementing by a minute during each loop, whatever the status is, add 60 (seconds) to the
              variable that represents that certain status in the zone
            - We will later use these values to make sure the zone ran/soaked for the correct amount of time \n
            
        Zone Behavior:
            - Zone 1:
                > Waters for 10 minutes (600s), then soaks for 25 minutes (1500s) then repeats
            - Zone 2:
                > Waits for 10 minutes (600s), then waters for 20 minutes (1200s) then soaks for 25 minutes (1500s) then
                  repeats.
            - Zone 3:
                > Waits for 30 minutes (1800s), then waters for 5 minutes (300s) then soaks for 50 minutes (3000s) then
                  repeats.
            - Zone 4:
                > Waters for 15 minutes (900s), then soaks for 15 minutes (900s) then repeats until 4th time watering,
                  then it shuts off.
            - Zone 5:
                > Never runs, is disabled.
        """
        expected_status_dict = {
            "07:59:00": {
                1: "DN", 2: "DN", 3: "DN", 4: "DN", 5: "DS"
            },
            "08:00:00": {
                1: "WT", 2: "WA", 3: "WA", 4: "WT", 5: "DS"
            },
            "08:05:00": {
                1: "WT", 2: "WA", 3: "WA", 4: "WT", 5: "DS"
            },
            "08:10:00": {
                1: "SO", 2: "WT", 3: "WA", 4: "WT", 5: "DS"
            },
            "08:15:00": {
                1: "SO", 2: "WT", 3: "WA", 4: "SO", 5: "DS"
            },
            "08:20:00": {
                1: "SO", 2: "WT", 3: "WA", 4: "SO", 5: "DS"
            },
            "08:25:00": {
                1: "SO", 2: "WT", 3: "WA", 4: "SO", 5: "DS"
            },
            "08:30:00": {
                1: "SO", 2: "SO", 3: "WT", 4: "WT", 5: "DS"
            },
            "08:35:00": {
                1: "WT", 2: "SO", 3: "SO", 4: "WT", 5: "DS"
            },
            "08:40:00": {
                1: "WT", 2: "SO", 3: "SO", 4: "WT", 5: "DS"
            },
            "08:45:00": {
                1: "SO", 2: "SO", 3: "SO", 4: "SO", 5: "DS"
            },
            "08:50:00": {
                1: "SO", 2: "SO", 3: "SO", 4: "SO", 5: "DS"
            },
            "08:55:00": {
                1: "SO", 2: "WT", 3: "SO", 4: "SO", 5: "DS"
            },
            "09:00:00": {
                1: "SO", 2: "WT", 3: "SO", 4: "WT", 5: "DS"
            },
            "09:05:00": {
                1: "SO", 2: "WT", 3: "SO", 4: "WT", 5: "DS"
            },
            "09:10:00": {
                1: "SO", 2: "WT", 3: "SO", 4: "WT", 5: "DS"
            },
            "09:15:00": {
                1: "WT", 2: "SO", 3: "SO", 4: "SO", 5: "DS"
            },
            "09:20:00": {
                1: "WT", 2: "SO", 3: "SO", 4: "SO", 5: "DS"
            },
            "09:25:00": {
                1: "SO", 2: "SO", 3: "WT", 4: "SO", 5: "DS"
            },
            "09:30:00": {
                1: "SO", 2: "SO", 3: "SO", 4: "WT", 5: "DS"
            },
            "09:35:00": {
                1: "SO", 2: "SO", 3: "SO", 4: "WT", 5: "DS"
            },
            "09:40:00": {
                1: "SO", 2: "WT", 3: "SO", 4: "WT", 5: "DS"
            },
            "09:45:00": {
                1: "SO", 2: "WT", 3: "SO", 4: "DN", 5: "DS"
            },
            "09:50:00": {
                1: "SO", 2: "WT", 3: "SO", 4: "DN", 5: "DS"
            },
            "09:55:00": {
                1: "SO", 2: "WT", 3: "SO", 4: "DN", 5: "DS"
            },
            "10:00:00": {
                1: "WT", 2: "SO", 3: "SO", 4: "DN", 5: "DS"
            },
        }

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            date_mngr.set_controller_datetime(date="02/12/2017", time="7:59:00")

            self.config.BaseStation3200[1].set_date_and_time(
                _date=date_mngr.controller_datetime.date_string_for_controller(),
                _time=date_mngr.controller_datetime.time_string_for_controller()
            )

            self.config.BaseStation3200[1].verify_date_and_time()

            # -------------------------------------------------------------------------------------------------------- #
            # Verify initial starting status'
            # -------------------------------------------------------------------------------------------------------- #
            for zone_ad in sorted(self.config.BaseStation3200[1].zones.keys()):

                zone = self.config.BaseStation3200[1].zones[zone_ad]
                zone.get_data()
                zone_status = zone.data.get_value_string_by_key('SS')
                time_string = date_mngr.controller_datetime.time_string_for_controller()

                expected_status = expected_status_dict[time_string][zone_ad]

                if zone_status != expected_status:
                    e_msg = "{0}: Received unexpected status for Zone {1} before starting program. Received: {2}, " \
                            "Expected: {3}".format(
                                time_string,
                                zone_ad,
                                zone_status,
                                expected_status
                            )
                    raise AssertionError(e_msg)
                else:
                    print "{0}: Verified Zone {1}'s initial status: {2}".format(time_string, zone_ad, zone_status)

            # Start Programming
            self.config.BaseStation3200[1].programs[1].set_program_to_start()
            self.config.BaseStation3200[1].programs[2].set_program_to_start()

            # Increment clock for watering to start
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # -------------------------------------------------------------------------------------------------------- #
            # Verify zone status' after program has started.
            # -------------------------------------------------------------------------------------------------------- #
            for zone_ad in sorted(self.config.BaseStation3200[1].zones.keys()):

                zone = self.config.BaseStation3200[1].zones[zone_ad]
                zone.get_data()
                zone_status = zone.data.get_value_string_by_key('SS')
                time_string = date_mngr.controller_datetime.time_string_for_controller()

                expected_status = expected_status_dict[time_string][zone_ad]

                if zone_status != expected_status:
                    e_msg = "{0}: Received unexpected status for Zone {1} after starting program. Received: {2}, " \
                            "Expected: {3}".format(
                                time_string,
                                zone_ad,
                                zone_status,
                                expected_status
                            )
                    raise AssertionError(e_msg)
                else:
                    print "{0}: Verified Zone {1}'s status: {2}".format(time_string, zone_ad, zone_status)

            # -------------------------------------------------------------------------------------------------------- #
            # Run programming for two hours and verify status'
            # -------------------------------------------------------------------------------------------------------- #
            while date_mngr.controller_datetime.time_string_for_controller() != "10:00:00":

                # Run simulation time at 5 minute intervals until 10:00:00 is reached.
                self.config.BaseStation3200[1].do_increment_clock(minutes=5)

                # Verify zone status' after program has started.
                for zone_ad in sorted(self.config.BaseStation3200[1].zones.keys()):

                    zone = self.config.BaseStation3200[1].zones[zone_ad]
                    zone.get_data()
                    zone_status = zone.data.get_value_string_by_key('SS')
                    time_string = date_mngr.controller_datetime.time_string_for_controller()

                    expected_status = expected_status_dict[time_string][zone_ad]

                    if zone_status != expected_status:
                        e_msg = "{0}: Received unexpected status for Zone {1} after starting program. Received: {2}, " \
                                "Expected: {3}".format(
                                    time_string,
                                    zone_ad,
                                    zone_status,
                                    expected_status
                                )
                        raise AssertionError(e_msg)
                    else:
                        print "{0}: Verified Zone {1}'s status: {2}".format(time_string, zone_ad, zone_status)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
