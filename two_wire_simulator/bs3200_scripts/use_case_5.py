import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration

# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr

__author__ = 'Tige'


class ControllerUseCase5(object):
    """
    Test name:
        - Using empty conditions of pocs
    purpose:
        -test and verify that pocs empty condition work on empty open empty closed and moisture sensors
    Coverage area: \n
        This test covers usigning flow to water. It covers the use of both POC and Mainline \n
        -setting up devices:
            - Loading \n
            - Searching \n
            - Addressing \n
                - Setting:
                    - descriptions
                    - locations \n
        - setting Programs: \n
            - start times
            - watering days
            - water windows
            - zone concurrency
            - assign Programs to water sources \n
            - assign zones to Programs \n
            - set up primary linked zones \n
            - give each zone a run time of 1 hour and 30 minutes \n
            - assign moisture sensors to a primary zone 1 and set to lower limit watering

        - setting up mainline: \n
            set main line \n
                - limit zones by flow \n
                - pipe fill time\n
                - target flow\n
                - high variance limit
                - high variance shut down \n
                - low variance limit
                - low variance shut down \n
        - setting up poc's: \n
            - enable POC \n
            - assign:
                - master valve
                - flow meter \n
                - target flow\n
                - main line \n
                - priority
                    - low
                    - medium
                    - high\n
            - set high flow limit
            - high flow shut down \n
            - set unscheduled flow limit
            - enable unscheduled flow shut down \n
            - set water budget
            - enable the water budget shut down \n
            - enable water rationing \n
            - empty conditions
                - event switch\n
                - set switch empty condition to closed \n
                - set empty wait time\n
        Verifiers:
            - verify open or closing a switch triggers and empty condition
            - verify the empty condition time to wait
            - verify status on zone
            - verify message on zone
            - verify message on program
            - verify message on mainline
            - verify message on poc
            - set up three different poc with only the empty condition being defferent
            -  assign 3 poc's to 1 mainline
            -  assign 3 zones to one program
    Date References:
        - configuration for script is located common\configuration_files\using_real_time_flow.json
        - the devices and addresses range is read from the .json file


    """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                     # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """

        # TODO need to have concurrent zones per program added

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        program_6am_start_time = [360]
        every_day_watering_days = [1, 1, 1, 1, 1, 1, 1]  # run every day
        full_open_water_windows = ['111111111111111111111111']

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_description(_ds='EPA Test Program')
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_water_window(_ww=full_open_water_windows)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_6am_start_time)
            self.config.BaseStation3200[1].programs[1].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[1].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(_sun=True,
                                                                                                           _mon=True,
                                                                                                           _tues=True,
                                                                                                           _wed=True,
                                                                                                           _thurs=True,
                                                                                                           _fri=True,
                                                                                                           _sat=True)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=5)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1,_tracking_ratio=100)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1,_tracking_ratio=100)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_3(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        set up water sources \n
        set up WS 1 \n
            - enable WS 1 \n
            - assign WS 1 a target flow of 500 \n
            - set WS priority to 2-medium \n
            - set water budget to 100000 and enable the water budget shut down \n
            - enable water rationing \n
            - empty conditions:
                - switch
                - closed
                - wait time 6 minutes
        \n
        set up WS 2 \n
            - enable WS 2 \n
            - assign master valve TMV0002 and flow meter TWF0002 to POC 3 \n
            - assign WS 2 a target flow of 500 \n
            - set WS priority to 2-medium \n
            - set water budget to 100000 and enable the water budget shut down \n
            - enable water rationing \n
            - empty conditions:
                - switch
                - open
                - wait time 6 minutes
        \n
        set up WS 3 \n
            - enable WS 3 \n
            - assign master valve TMV0003 and flow meter TWF0003 to POC 5 \n
            - assign WS 3 a target flow of 500 \n
            - set WS priority to 2-medium \n
            - set water budget to 100000 and enable the water budget shut down \n
            - enable water rationing \n
            - empty conditions:
                - moisture sensor
                - below 15%
                - wait time 6 minutes
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[1].set_monthly_watering_budget(_budget=10000,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].water_sources[1].set_water_rationing_to_enabled()

            self.config.BaseStation3200[1].water_sources[1].add_switch_empty_condition(_event_switch_address=1)
            self.config.BaseStation3200[1].water_sources[1].switch_empty_conditions[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].switch_empty_conditions[1].set_switch_empty_condition_to_closed()
            self.config.BaseStation3200[1].water_sources[1].switch_empty_conditions[1].set_empty_wait_time(_minutes=6)

            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=2)
            self.config.BaseStation3200[1].water_sources[2].set_enabled()
            self.config.BaseStation3200[1].water_sources[2].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[2].set_monthly_watering_budget(_budget=10000,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].water_sources[2].set_water_rationing_to_enabled()

            self.config.BaseStation3200[1].water_sources[2].add_switch_empty_condition(_event_switch_address=2)
            self.config.BaseStation3200[1].water_sources[2].switch_empty_conditions[2].set_enabled()
            self.config.BaseStation3200[1].water_sources[2].switch_empty_conditions[2].set_switch_empty_condition_to_open()
            self.config.BaseStation3200[1].water_sources[2].switch_empty_conditions[2].set_empty_wait_time(_minutes=6)

            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=3)
            self.config.BaseStation3200[1].water_sources[3].set_enabled()
            self.config.BaseStation3200[1].water_sources[3].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[3].set_monthly_watering_budget(_budget=10000,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].water_sources[3].set_water_rationing_to_enabled()

            self.config.BaseStation3200[1].water_sources[3].add_moisture_empty_condition(_moisture_sensor_address=1)
            self.config.BaseStation3200[1].water_sources[3].moisture_empty_conditions[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[3].moisture_empty_conditions[1]\
                .set_moisture_empty_limit(_percent=15.0)
            self.config.BaseStation3200[1].water_sources[3].moisture_empty_conditions[1].set_empty_wait_time(_minutes=6)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n
        set_poc_3200
        set up POC 1 \n
            - enable POC 1 \n
            - assign master valve TMV0001 and flow meter TWF0001 to POC 1 \n
            - assign POC 1 a target flow of 500 \n
            - assign POC 1 to main line 1 \n
            - set POC priority to 2-medium \n
            - set high flow limit to 550 and enable high flow shut down \n
            - set unscheduled flow limit to 10 and enable unscheduled flow shut down \n

        \n
        set up POC 2 \n
            - enable POC 2 \n
            - assign master valve TMV0002 and flow meter TWF0002 to POC 3 \n
            - assign POC 2 a target flow of 500 \n
            - assign POC 2 to main line 1 \n
            - set POC priority to 2-medium \n
            - set high flow limit to 550 and enable high flow shut down \n
            - set unscheduled flow limit to 10 and enable unscheduled flow shut down \n

        \n
        set up POC 3 \n
            - enable POC 3 \n
            - assign master valve TMV0003 and flow meter TWF0003 to POC 5 \n
            - assign POC 3 a target flow of 500 \n
            - assign POC 3 to main line 1 \n
            - set POC priority to 2-medium \n
            - set high flow limit to 550 and enable high flow shut down \n
            - set unscheduled flow limit to 10 and enable unscheduled flow shut down \n

        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].add_master_valve_to_point_of_control(_master_valve_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_flow_meter_to_point_of_control(_flow_meter_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].points_of_control[1].set_high_flow_limit(_limit=550,
                                                                                    with_shutdown_enabled=True)
            self.config.BaseStation3200[1].points_of_control[1].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=True)

            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=2)
            self.config.BaseStation3200[1].water_sources[2].add_point_of_control_to_water_source(_point_of_control_address=2)
            self.config.BaseStation3200[1].points_of_control[2].set_enabled()
            self.config.BaseStation3200[1].points_of_control[2].add_master_valve_to_point_of_control(_master_valve_address=2)
            self.config.BaseStation3200[1].points_of_control[2].add_flow_meter_to_point_of_control(_flow_meter_address=2)
            self.config.BaseStation3200[1].points_of_control[2].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].points_of_control[2].set_high_flow_limit(_limit=550,
                                                                                    with_shutdown_enabled=True)
            self.config.BaseStation3200[1].points_of_control[2].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=True)

            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=3)
            self.config.BaseStation3200[1].water_sources[3].add_point_of_control_to_water_source(_point_of_control_address=3)
            self.config.BaseStation3200[1].points_of_control[3].set_enabled()
            self.config.BaseStation3200[1].points_of_control[3].add_master_valve_to_point_of_control(_master_valve_address=3)
            self.config.BaseStation3200[1].points_of_control[3].add_flow_meter_to_point_of_control(_flow_meter_address=3)
            self.config.BaseStation3200[1].points_of_control[3].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].points_of_control[3].set_high_flow_limit(_limit=550,
                                                                                    with_shutdown_enabled=True)
            self.config.BaseStation3200[1].points_of_control[3].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=True)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control

        set_mainlines_3200 \n
            - set up main line 1 \n
                - Add mainline 1 to point of control 1,2,3
                - set limit zones by flow to true \n
                - set the pipe fill time to 4 minutes \n
                - set the target flow to 500 \n
                - set the high variance limit to 5% and enable the high variance shut down \n
                - set the low variance limit to 20% and enable the low variance shut down \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)
            self.config.BaseStation3200[1].points_of_control[2].add_mainline_to_point_of_control(_mainline_address=1)
            self.config.BaseStation3200[1].points_of_control[3].add_mainline_to_point_of_control(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_enabled()
            self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=4)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[1].set_high_flow_variance_tier_one(_percent=5,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].mainlines[1].set_low_flow_variance_tier_one(_percent=20,
                                                                                       _with_shutdown_enabled=False)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=zone_address)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        set default values for all empty conditions so that all poc can go to a watering state
        \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_open()
            self.config.BaseStation3200[1].event_switches[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].event_switches[2].bicoder.set_contact_closed()
            self.config.BaseStation3200[1].event_switches[2].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=25.0)
            # there is a 6 minute wait time on the empty condition so when the event bicoder changes state
            # we need to run the clock for at least six minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=6)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        POC 1
            - has an empty condition of a switch
            - it triggers on a open even
            - once the switch closes it waits 6 minutes to turn back on the point of connection
        process:
            - Start by setting clock to a time away from the start time
            - Start program
            - verify all zone and master valve turn on correctly
            - close switch
            - increment clock 2 minutes
            - the master valve on poc 1 will be set to done because there is no water available do to the empty condition
            - verify a message is posted
            - the other two master valves on poc 2 and 3 will continue to water
            - re-open the switch to clear the empty condition
            - but do to the wait timer the master should not turn back on for 6 minutes
            - increment clock only 4 minutes this is to check to verify the wait timer is working
            - after the 4 minutes the master valve for poc 1 is still off
            - increment the clock other 3 minute and verify that the master on poc 1 gets set back to watering
            - Verify that the message was cleared
        \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_date_and_time(_date='08/27/2014', _time='23:45:00')
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)
            self.config.BaseStation3200[1].verify_date_and_time()

            ##################
            self.config.BaseStation3200[1].programs[1].set_program_to_start()
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)
            self.config.BaseStation3200[1].verify_date_and_time()

            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            # this should trigger water source one to be empty
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_closed()
            self.config.BaseStation3200[1].event_switches[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].do_increment_clock(minutes=2)
            self.config.BaseStation3200[1].verify_date_and_time()

            self.config.BaseStation3200[1].water_sources[1].switch_empty_conditions[1]\
                .messages.verify_empty_condition_with_event_switch_message()

            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_open()
            self.config.BaseStation3200[1].event_switches[1].bicoder.self_test_and_update_object_attributes()
            # there is a six minute wait time after contacts are opend
            self.config.BaseStation3200[1].do_increment_clock(minutes=6)
            self.config.BaseStation3200[1].verify_date_and_time()

            # message was cleared by the controller verify it gone
            self.config.BaseStation3200[1].water_sources[1].switch_empty_conditions[1]\
                .messages.check_for_empty_condition_with_event_switch_message_not_present()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_watering()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        POC 2
            - has an empty condition of a switch
            - it triggers on a closed even
            - once the switch closes it waits 6 minutes to turn back on the point of connection
        process:
            - the program is still running from step 9
            - verify all zone and master valve turn on correctly
            - open switch
            - increment clock 2 minutes
            - the master valve on poc 2 will be set to done because there is no water available do to the empty condition
            - verify a message is posted
            - the other two master valves on poc 1 and 3 will continue to water
            - re-close the switch to clear the empty condition
            - but do to the wait timer the master should not turn back on for 6 minutes
            - increment the clock other 6 minute and verify that the master on poc 2 gets set back to watering
            - Verify that the message was cleared
            - also verify that the zones continue to follow the correct run time, cycle time, and soak time
        \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # this will tigger an empty conditions on water source 2
            self.config.BaseStation3200[1].event_switches[2].bicoder.set_contact_open()
            self.config.BaseStation3200[1].event_switches[2].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].do_increment_clock(minutes=2)
            self.config.BaseStation3200[1].verify_date_and_time()

            # message was cleared by the controller verify it gone
            self.config.BaseStation3200[1].water_sources[2].switch_empty_conditions[2] \
                .messages.verify_empty_condition_with_event_switch_message()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].event_switches[2].bicoder.set_contact_closed()
            self.config.BaseStation3200[1].event_switches[2].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].do_increment_clock(minutes=7)
            self.config.BaseStation3200[1].verify_date_and_time()

            # message was cleared by the controller verify it gone
            self.config.BaseStation3200[1].water_sources[2].switch_empty_conditions[2]\
                .messages.check_for_empty_condition_with_event_switch_message_not_present()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_watering()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        POC 2
            - has an empty condition of a moisture sensor
            - it triggers when the threshold drops below 15
            - once the threshold return to above 15 it waits 6 minutes to turn back on the point of connection
        process:
            - the program is still running from step 9 and 10
            - verify all zone and master valve turn on correctly
            - set moisture sensor to 14% which is below the threshold of 15%
            - increment clock 2 minutes
            - the master valve on poc 3 will be set to done because there is no water available do to the empty condition
            - verify a message is posted
            - the other two master valves on poc 1 and 2 will continue to water
            - re-set the moisture sensor to 16% which is above the threshold
            - but do to the wait timer the master should not turn back on for 6 minutes
            - increment the clock other 6 minute and verify that the master on poc 3 gets set back to watering
            - Verify that the message was cleared
            - also verify that the zones continue to follow the correct run time, cycle time, and soak time
        \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=14.0)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].do_increment_clock(minutes=2)
            self.config.BaseStation3200[1].verify_date_and_time()

            # message was cleared by the controller verify it gone
            self.config.BaseStation3200[1].water_sources[3].moisture_empty_conditions[1].messages.verify_empty_condition_with_moisture_sensor_message()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_off()

            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.set_moisture_percent(_percent=16.0)
            self.config.BaseStation3200[1].moisture_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].do_increment_clock(minutes=7)
            self.config.BaseStation3200[1].verify_date_and_time()

            # message was cleared by the controller verify it gone
            self.config.BaseStation3200[1].water_sources[3].moisture_empty_conditions[1].messages.check_for_empty_condition_with_moisture_sensor_message_not_present()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].master_valves[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].master_valves[3].statuses.verify_status_is_watering()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]





