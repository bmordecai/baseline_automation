import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration

# import log_handler for logging functionality
from common.logging_handler import log_handler

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr

from common import helper_methods

__author__ = 'Dillon/Eldin'


class ControllerUseCase47_2(object):
    """
    Test name:
        - CN UseCase 47_2 Zone Infinite Watering Bug

    User Story:
        1 ) As a user I want to be able to assign mainlines to all of my POCs and still have my zones I setup to be able
            to water properly.

    Purpose:
        - To replicate a bug where if all POCs are assigned to mainlines any zones that aren't assigned to a mainline
          are on mainline 0.
        - Verify that the bug is no longer present on the newest versions of firmware.

    Coverage Area:
        - Update the controller's firmware to the most recent and verify that the bug has been fixed by running the
          same configuration that the bug was replicated on.
        - Zones assigned to mainline 0 (no mainline) don't finish watering.
            - This was first demonstrated on the Nike site.  This can be caused by assigning all POCs to mainlines.
              This results in no POCs on mainline 0 (where unassigned POCs show up).  The code that checks run timers
              (cycle and water times) gets skipped for mainlines without POCs, as normally this would signify a not used
              state and skipping timers would not effect watering.
            - For the bug report, look at JIRA #ZZ-1757
        - Run the zones that are attached to mainline 1 and the zones for mainline 0 for their full runtime and verify
          that both finish watering.
                
    Not Covered:
        - Start/Stop/Pause conditions
        - Learn Flow
        - v12 to v16 Update
        - Water budgets
        - Design Flows
    """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        ############################
        Set up controller
        ############################
        - Set controller concurrent zones.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=3)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        Set up programs
        ############################
        - Setup one program to hold all of the zones we are expecting to have.
            - Give it a start time so we can later trigger watering by moving over the start time.
            - Set max concurrent zones to 3, seems like a reasonable amount.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # this is set in the PG3200 object
            program_start_time_8am = [480]
            program_water_windows = ['111111111111111111111111']

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=12)
            self.config.BaseStation3200[1].programs[12].set_enabled()
            self.config.BaseStation3200[1].programs[12].set_max_concurrent_zones(_number_of_zones=3)
            self.config.BaseStation3200[1].programs[12].set_start_times(_st_list=[960])

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        Set up zones on programs
        ############################

        - We have 6 zones in total we want to run, a group of 3 zones will be ran concurrently
        - Half the zones will be on mainline 0 (Not assigned to a mainline) and half will be assigned to mainline 1
            - This means one group will be reproducing the bug, one will be running normally
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add zone programs to Program 1
            self.config.BaseStation3200[1].programs[12].add_zone_to_program(_zone_address=12)
            self.config.BaseStation3200[1].programs[12].zone_programs[12].set_run_time(_minutes=10)
            self.config.BaseStation3200[1].programs[12].zone_programs[12].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[12].zone_programs[12].set_soak_time(_minutes=5)
            self.config.BaseStation3200[1].programs[12].zone_programs[12].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[12].zone_programs[12].set_water_strategy_to_timed()

            self.config.BaseStation3200[1].programs[12].add_zone_to_program(_zone_address=27)
            self.config.BaseStation3200[1].programs[12].zone_programs[27].set_as_linked_zone(_primary_zone=12)

            self.config.BaseStation3200[1].programs[12].add_zone_to_program(_zone_address=36)
            self.config.BaseStation3200[1].programs[12].zone_programs[36].set_as_linked_zone(_primary_zone=12)

            self.config.BaseStation3200[1].programs[12].add_zone_to_program(_zone_address=52)
            self.config.BaseStation3200[1].programs[12].zone_programs[52].set_as_linked_zone(_primary_zone=12)

            self.config.BaseStation3200[1].programs[12].add_zone_to_program(_zone_address=55)
            self.config.BaseStation3200[1].programs[12].zone_programs[55].set_as_linked_zone(_primary_zone=12)

            self.config.BaseStation3200[1].programs[12].add_zone_to_program(_zone_address=71)
            self.config.BaseStation3200[1].programs[12].zone_programs[71].set_as_linked_zone(_primary_zone=12)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        setup WaterSources
        ############################

        - We only need one water source to verify this bug as we can attach all POCs to it
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Water Source 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].set_monthly_watering_budget(_budget=20000)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        setup Point of Controls
        ############################

        - Setup 8 POCs (Max on the controller) so that we can reproduce the bug
            - By creating 8 POCs, we can then later attach ML 1 to all of them, meaning no POCs will be pointing at ML
              0. When we put zones on ML 0 (no mainline), they will run infinitely.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add the points of control to the controller
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_flow_meter_to_point_of_control(
                _flow_meter_address=1)
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=1)

            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=2)
            self.config.BaseStation3200[1].points_of_control[2].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=2)

            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=3)
            self.config.BaseStation3200[1].points_of_control[3].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=3)

            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=4)
            self.config.BaseStation3200[1].points_of_control[4].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=4)

            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=5)
            self.config.BaseStation3200[1].points_of_control[5].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=5)

            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=6)
            self.config.BaseStation3200[1].points_of_control[6].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=6)

            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=7)
            self.config.BaseStation3200[1].points_of_control[7].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=7)

            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=8)
            self.config.BaseStation3200[1].points_of_control[8].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=8)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ##################
        setup mainlines
        ##################

        - Set up one mainline that will be downstream of all POCs.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure ML 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=50)

            # Add ML to every POC
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)
            self.config.BaseStation3200[1].points_of_control[2].add_mainline_to_point_of_control(_mainline_address=1)
            self.config.BaseStation3200[1].points_of_control[3].add_mainline_to_point_of_control(_mainline_address=1)
            self.config.BaseStation3200[1].points_of_control[4].add_mainline_to_point_of_control(_mainline_address=1)
            self.config.BaseStation3200[1].points_of_control[5].add_mainline_to_point_of_control(_mainline_address=1)
            self.config.BaseStation3200[1].points_of_control[6].add_mainline_to_point_of_control(_mainline_address=1)
            self.config.BaseStation3200[1].points_of_control[7].add_mainline_to_point_of_control(_mainline_address=1)
            self.config.BaseStation3200[1].points_of_control[8].add_mainline_to_point_of_control(_mainline_address=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ######################
        setup zones on mainlines
        ######################

        - Add zones 12, 27, 36 to mainline 1, they will be the normal watering group
        - Zones 52, 55, 72 will not be assigned to any mainline, so they will be the group that doesn't stop watering
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Add zones that we DON'T want to reproduce the bug to mainline 1
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=12)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=27)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=36)

            # Add zones that we DO want to reproduce the bug to mainline 0
            # Note: though this points to mainline 1, we are actually just setting every zone to mainline 0 by removing
            self.config.BaseStation3200[1].mainlines[1].remove_zone_from_mainline(_zone_address=52)
            self.config.BaseStation3200[1].mainlines[1].remove_zone_from_mainline(_zone_address=55)
            self.config.BaseStation3200[1].mainlines[1].remove_zone_from_mainline(_zone_address=71)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_9(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################

        - Get information for each object from controller
        - Verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_10(self):
        """
        - Scenario 1: Verifying the bug, following these steps:
            - Program 12 hits start time so all zone should be watering or waiting to water
            - Once the zones are running, they should alternate between running and soaking until they are done watering
                - This is because each zone has a 5 minute cycle and 5 minute soak time
            - Zones 12, 27, 36 are assigned to mainline 0, so they should run for their full runtime and be done
            - Zones 52, 55, 71 are assigned to no mainline (mainline 0), so they should keep running even after
              their runtime is up, since there is no POCs upstream of mainline 0 (all are upstream of mainline 1)
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_date_and_time(_date='05/08/2018', _time='15:59:00')
            self.config.BaseStation3200[1].verify_date_and_time()

            # ------------------------------------------------ 4:01PM ------------------------------------------------ #
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)

            for watering_zones in [12, 27, 36]:
                self.config.BaseStation3200[1].zones[watering_zones].statuses.verify_status_is_watering()

            for waiting_zones in [52, 55, 71]:
                self.config.BaseStation3200[1].zones[waiting_zones].statuses.verify_status_is_waiting_to_water()

            # ------------------------------------------------ 4:06PM ------------------------------------------------ #
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # 12, 27, 36 ran for their 5 minutes cycle time and are now soaking
            for watering_zones in [12, 27, 36]:
                self.config.BaseStation3200[1].zones[watering_zones].statuses.verify_status_is_soaking()

            for waiting_zones in [52, 55, 71]:
                self.config.BaseStation3200[1].zones[waiting_zones].statuses.verify_status_is_watering()

            # ------------------------------------------------ 4:11PM ------------------------------------------------ #
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Here is where the bug was originally found. Zones 52/55/71 should have been done with their cycle and
            # started soaking, but instead they keep running. This leaves the rest of the zones in a soaking state and
            # 52/55/71 stay in a watering state infinitely. A working version should not have this error and should run
            # through this verifier normally
            for watering_zones in [12, 27, 36]:
                self.config.BaseStation3200[1].zones[watering_zones].statuses.verify_status_is_watering()

            for waiting_zones in [52, 55, 71]:
                self.config.BaseStation3200[1].zones[waiting_zones].statuses.verify_status_is_soaking()

            # ------------------------------------------------ 4:16PM ------------------------------------------------ #
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # 12, 27, 36 ran for their 5 minutes cycle time, making a grand total of 10 minutes, which is their runtime,
            # so they are now done
            for watering_zones in [12, 27, 36]:
                self.config.BaseStation3200[1].zones[watering_zones].statuses.verify_status_is_done()

            for waiting_zones in [52, 55, 71]:
                self.config.BaseStation3200[1].zones[waiting_zones].statuses.verify_status_is_watering()

            # ------------------------------------------------ 4:21PM ------------------------------------------------ #
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # 52, 55, 71 ran for their 5 minutes cycle time, making a grand total of 10 minutes, which is their runtime,
            # so they are now done
            for watering_zones in [12, 27, 36]:
                self.config.BaseStation3200[1].zones[watering_zones].statuses.verify_status_is_done()

            for waiting_zones in [52, 55, 71]:
                self.config.BaseStation3200[1].zones[waiting_zones].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]