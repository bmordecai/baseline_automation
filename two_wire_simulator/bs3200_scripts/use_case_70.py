import sys
from time import sleep

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler
from common import helper_methods

__author__ = 'Dillon'


class ControllerUseCase70(object):
    """
    Test name:
        - CN UseCase70 Mainline Pressure Delays

    User Story:
        - As a user I want my zones to have a delay before running based on the pressure reading in my line.
        - As a user I want there to be a delay based on pressure before my first zone activates, to ensure the pipe
          is pressurized before running water.


    Coverage area:
        - Pressure delay before first zone
        - Pressure delay between zones
        - Pressure delay stop
        - Time delay between zones


    Explanation of Scenarios:
        - 1st scenario:
            - Setup basic configuration
            - Setup mainline pressure delay before 1st zone
            - Verify device states minute by minute

        - 2nd scenario:
            - Setup basic configuration
            - Change max concurrent zones to 2
            - Setup mainline pressure delay after each zone
            - Verify device states minute by minute

    """

    ###############################
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase44' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    ###############################
    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        setup controller concurrency.
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=15)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_2(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=5)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_3(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add zone programs to Program 1
            for zone_ad in range(1, 6):
                self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=zone_ad)
                self.config.BaseStation3200[1].programs[1].zone_programs[zone_ad].set_run_time(_minutes=10)
                self.config.BaseStation3200[1].programs[1].zone_programs[zone_ad].set_cycle_time(_minutes=4)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_4(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        set_water Source \n
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Water Source 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].set_priority(_priority_for_water_source=2)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n

        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=1)
            self.config.BaseStation3200[1].points_of_control[1].\
                add_pressure_sensor_to_point_of_control(_pressure_sensor_address=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control
        set_mainlines_3200 \n
            - set up main line 1 \n
                - set limit zones by flow to true \n
                - set the pipe fill time to 4 minutes \n
                - set the target flow to 500 \n
                - set the high variance limit to 5% and enable the high variance shut down \n
                - set the low variance limit to 20% and enable the low variance shut down \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Mainline 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_enabled()
            # Mainline 1 Time Delays
            self.config.BaseStation3200[1].mainlines[1].set_pressure_delay_before_first_zone(_psi=15.0)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add Zones 1-5 to Mainline 1
            for zone_address in range(1, 6):
                self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=zone_address)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        ###############################
        Program 1 - Verify initial statuses - 7:59am
        ###############################
        Scenario 1:
            - When the program starts the pressure sensor is only reading 10 psi, so the zones go into waiting.
            - Once the pressure has passed the delay limit of 15 psi the zones will run

        Timed Delays Table:

            Time  | WS 1 | PC 1 | MV 1 | ML 1 | ZN 1 | ZN 2 | ZN 3 | ZN 4 | ZN 5 | PG 1 |
            -----------------------------------------------------------------------------
            8:00a |  OK  |  OF  |  OF  |  OF  |  DN  |  DN  |  DN  |  DN  |  DN  |  DN  |
            8:01a |  RN  |  RN  |  WT  |  RN  |  WA  |  WA  |  WA  |  WA  |  WA  |  WA  |
            8:02a |  RN  |  RN  |  WT  |  RN  |  WA  |  WA  |  WA  |  WA  |  WA  |  WA  |
            8:03a |  RN  |  RN  |  WT  |  RN  |  WT  |  WT  |  WT  |  WT  |  WT  |  RN  |
            8:04a |  RN  |  RN  |  WT  |  RN  |  WT  |  WT  |  WT  |  WT  |  WT  |  RN  |
            8:05a |  RN  |  RN  |  WT  |  RN  |  WT  |  WT  |  WT  |  WT  |  WT  |  RN  |
            8:06a |  RN  |  RN  |  WT  |  RN  |  WT  |  WT  |  WT  |  WT  |  WT  |  RN  |
            8:07a |  OK  |  OF  |  OF  |  OF  |  SO  |  SO  |  SO  |  SO  |  SO  |  WA  |
            8:08a |  RN  |  RN  |  WT  |  OF  |  WT  |  WT  |  WT  |  WT  |  WT  |  RN  |
            8:09a |  RN  |  RN  |  WT  |  RN  |  WT  |  WT  |  WT  |  WT  |  WT  |  RN  |
            8:10a |  RN  |  RN  |  WT  |  RN  |  WT  |  WT  |  WT  |  WT  |  WT  |  RN  |
            8:11a |  RN  |  RN  |  WT  |  RN  |  WT  |  WT  |  WT  |  WT  |  WT  |  RN  |
            8:12a |  RN  |  RN  |  WT  |  RN  |  WT  |  WT  |  WT  |  WT  |  WT  |  RN  |
            8:13a |  RN  |  RN  |  WT  |  OF  |  WT  |  WT  |  WT  |  WT  |  WT  |  RN  |
            8:15a |  OK  |  OF  |  OF  |  OF  |  DN  |  DN  |  DN  |  DN  |  DN  |  DN  |

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set time to 7:59a
            self.config.BaseStation3200[1].set_date_and_time(_date='12/18/2017', _time='7:59:00')
            # this is needed until ERT-81 improvement has be finished, this ensures the correct test command
            # is built
            self.config.BaseStation3200[1].pressure_sensors[1].id = opcodes.pressure_sensor
            self.config.BaseStation3200[1].pressure_sensors[1].set_pressure_value_in_psi(_psi=10)
            self.config.BaseStation3200[1].pressure_sensors[1].self_test_and_update_object_attributes()

            # ----> Time 8:00a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_off()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()

            # ----> Time 8:01a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            #verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            #verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            # ----> Time 8:02a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            #verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            #verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            # ----> Time 8:03a <--------#
            self.config.BaseStation3200[1].pressure_sensors[1].set_pressure_value_in_psi(_psi=25)
            self.config.BaseStation3200[1].pressure_sensors[1].self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ----> Time 8:07a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_off()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_soaking()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            # ----> Time 8:08a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ----> Time 8:09a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ----> Time 8:13a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ----> Time 8:15a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_off()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        ###############################
        Program 1 - Verify initial statuses - 7:59am
        ###############################
        Scenario 2:
            - When program starts the pressure sensor will read 30 psi, which is 10 psi above the pressure delay limit
              so the zones will immediately try to lay down water
            - Once the pressure sensor sees a psi value below the delay limit the zones will go into waiting until
              the pressure sensor sees a value above hte 10 psi delay limit we set
            - Max concurrent zones was also changed to 2, this way it's easier to see how the pressure delay effects the
              program running

        Timed Delays Table:

            Time  | WS 1 | PC 1 | MV 1 | ML 1 | ZN 1 | ZN 2 | ZN 3 | ZN 4 | ZN 5 | PG 1 |
            -----------------------------------------------------------------------------
            8:00a |  OK  |  OF  |  OF  |  OF  |  DN  |  DN  |  DN  |  DN  |  DN  |  DN  |
            8:01a |  RN  |  RN  |  WT  |  RN  |  WA  |  WA  |  WA  |  WA  |  WA  |  WA  |
            8:02a |  RN  |  RN  |  WT  |  RN  |  WA  |  WA  |  WA  |  WA  |  WA  |  WA  |
            8:03a |  RN  |  RN  |  WT  |  RN  |  WA  |  WA  |  WA  |  WA  |  WA  |  WA  |
            8:04a |  RN  |  RN  |  WT  |  RN  |  WT  |  WT  |  WA  |  WA  |  WA  |  RN  |
            8:05a |  RN  |  RN  |  WT  |  RN  |  WT  |  WT  |  WA  |  WA  |  WA  |  RN  |
            8:06a |  RN  |  RN  |  WT  |  RN  |  WT  |  WT  |  WA  |  WA  |  WA  |  RN  |
            8:07a |  RN  |  RN  |  WT  |  RN  |  WT  |  WT  |  WA  |  WA  |  WA  |  RN  |
            8:08a |  RN  |  RN  |  WT  |  RN  |  SO  |  SO  |  WA  |  WA  |  WA  |  WA  |
            8:09a |  RN  |  RN  |  WT  |  RN  |  WT  |  WT  |  WA  |  WA  |  WA  |  RN  |
            8:10a |  RN  |  RN  |  WT  |  RN  |  WT  |  WT  |  WA  |  WA  |  WA  |  RN  |
            8:11a |  RN  |  RN  |  WT  |  RN  |  WT  |  WT  |  WA  |  WA  |  WA  |  RN  |
            8:12a |  RN  |  RN  |  WT  |  RN  |  WT  |  WT  |  WA  |  WA  |  WA  |  RN  |
            8:13a |  RN  |  RN  |  WT  |  RN  |  SO  |  SO  |  WA  |  WA  |  WA  |  WA  |
            8:14a |  RN  |  RN  |  WT  |  RN  |  WT  |  WT  |  WA  |  WA  |  WA  |  RN  |
            8:15a |  RN  |  RN  |  WT  |  RN  |  WT  |  WT  |  WA  |  WA  |  WA  |  RN  |
            8:16a |  RN  |  RN  |  WT  |  RN  |  DN  |  DN  |  WA  |  WA  |  WA  |  WA  |
            8:17a |  RN  |  RN  |  WT  |  RN  |  DN  |  DN  |  WT  |  WT  |  WA  |  RN  |
            8:18a |  RN  |  RN  |  WT  |  RN  |  DN  |  DN  |  WT  |  WT  |  WA  |  RN  |
            8:19a |  RN  |  RN  |  WT  |  RN  |  DN  |  DN  |  WT  |  WT  |  WA  |  RN  |
            8:20a |  RN  |  RN  |  WT  |  RN  |  DN  |  DN  |  WT  |  WT  |  WA  |  RN  |
            8:21a |  RN  |  RN  |  WT  |  RN  |  DN  |  DN  |  SO  |  SO  |  WA  |  WA  |
            8:22a |  RN  |  RN  |  WT  |  RN  |  DN  |  DN  |  WT  |  WT  |  WA  |  RN  |
            8:23a |  RN  |  RN  |  WT  |  RN  |  DN  |  DN  |  WT  |  WT  |  WA  |  RN  |
            8:24a |  RN  |  RN  |  WT  |  RN  |  DN  |  DN  |  WT  |  WT  |  WA  |  RN  |
            8:25a |  RN  |  RN  |  WT  |  RN  |  DN  |  DN  |  WT  |  WT  |  WA  |  RN  |
            8:26a |  RN  |  RN  |  WT  |  RN  |  DN  |  DN  |  SO  |  SO  |  WA  |  WA  |
            8:27a |  RN  |  RN  |  WT  |  RN  |  DN  |  DN  |  WT  |  WT  |  WA  |  RN  |
            8:28a |  RN  |  RN  |  WT  |  RN  |  DN  |  DN  |  WT  |  WT  |  WA  |  RN  |
            8:29a |  RN  |  RN  |  WT  |  RN  |  DN  |  DN  |  DN  |  DN  |  WA  |  WA  |
            8:30a |  RN  |  RN  |  WT  |  RN  |  DN  |  DN  |  DN  |  DN  |  WT  |  RN  |
            8:31a |  RN  |  RN  |  WT  |  RN  |  DN  |  DN  |  DN  |  DN  |  WT  |  RN  |
            8:32a |  RN  |  RN  |  WT  |  RN  |  DN  |  DN  |  DN  |  DN  |  WT  |  RN  |
            8:33a |  RN  |  RN  |  WT  |  RN  |  DN  |  DN  |  DN  |  DN  |  WT  |  RN  |
            8:34a |  RN  |  RN  |  WT  |  RN  |  DN  |  DN  |  DN  |  DN  |  SO  |  WA  |
            8:35a |  RN  |  RN  |  WT  |  RN  |  DN  |  DN  |  DN  |  DN  |  WT  |  RN  |
            8:36a |  RN  |  RN  |  WT  |  RN  |  DN  |  DN  |  DN  |  DN  |  WT  |  RN  |
            8:36a |  RN  |  RN  |  WT  |  RN  |  DN  |  DN  |  DN  |  DN  |  WT  |  RN  |
            8:37a |  RN  |  RN  |  WT  |  RN  |  DN  |  DN  |  DN  |  DN  |  WT  |  RN  |
            8:38a |  RN  |  RN  |  WT  |  RN  |  DN  |  DN  |  DN  |  DN  |  WT  |  RN  |
            8:39a |  RN  |  RN  |  WT  |  RN  |  DN  |  DN  |  DN  |  DN  |  SO  |  WA  |
            8:40a |  RN  |  RN  |  WT  |  RN  |  DN  |  DN  |  DN  |  DN  |  WT  |  RN  |
            8:41a |  RN  |  RN  |  WT  |  RN  |  DN  |  DN  |  DN  |  DN  |  WT  |  RN  |
            8:42a |  RN  |  RN  |  WT  |  RN  |  DN  |  DN  |  DN  |  DN  |  DN  |  DN  |
            8:42a |  OK  |  OF  |  OF  |  RN  |  DN  |  DN  |  DN  |  DN  |  DN  |  DN  |

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].mainlines[1].set_pressure_delay_after_zone(_psi=20)
            # This will make only two zones run at a time
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=2)

            # Set time to 7:59a
            self.config.BaseStation3200[1].set_date_and_time(_date='12/18/2017', _time='7:59:00')
            # this is needed until ERT-81 improvement has be finished, this ensures the correct test command
            # is built
            self.config.BaseStation3200[1].pressure_sensors[1].id = opcodes.pressure_sensor
            self.config.BaseStation3200[1].pressure_sensors[1].set_pressure_value_in_psi(_psi=30)
            self.config.BaseStation3200[1].pressure_sensors[1].self_test_and_update_object_attributes()

            # ----> Time 8:00a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_off()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()

            # ----> Time 8:01a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            #verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            #verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation3200[1].pressure_sensors[1].set_pressure_value_in_psi(_psi=5)
            self.config.BaseStation3200[1].pressure_sensors[1].self_test_and_update_object_attributes()

            # ----> Time 8:02a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            #verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            #verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            # ----> Time 8:03a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation3200[1].pressure_sensors[1].set_pressure_value_in_psi(_psi=30)
            self.config.BaseStation3200[1].pressure_sensors[1].self_test_and_update_object_attributes()

            # ----> Time 8:04a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ----> Time 8:05a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ----> Time 8:06a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ----> Time 8:07a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ----> Time 8:08a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            # ----> Time 8:09a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ----> Time 8:13a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            # ----> Time 8:15a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ----> Time 8:16a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            # ----> Time 8:17a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ----> Time 8:18a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ----> Time 8:19a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ----> Time 8:20a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ----> Time 8:21a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            # ----> Time 8:22a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ----> Time 8:23a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ----> Time 8:24a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verify water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ----> Time 8:25a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verify water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ----> Time 8:26a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verify water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            # ----> Time 8:27a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verify water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ----> Time 8:28a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verify water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ----> Time 8:29a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verify water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            # ----> Time 8:30a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verify water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ----> Time 8:31a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verify water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ----> Time 8:32a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verify water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ----> Time 8:33a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verify water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ----> Time 8:34a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verify water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_soaking()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            # ----> Time 8:35a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verify water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ----> Time 8:36a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verify water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ----> Time 8:37a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verify water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ----> Time 8:38a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verify water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ----> Time 8:39a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verify water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_soaking()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            # ----> Time 8:40a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verify water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ----> Time 8:41a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verify water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ----> Time 8:42a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verify water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()

            # ----> Time 8:43a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verify water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_off()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

