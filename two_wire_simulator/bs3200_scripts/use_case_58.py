import sys
from time import sleep
from datetime import timedelta

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
from common.objects.base_classes.web_driver import *

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

__author__ = 'Eldin'


class ControllerUseCase58(object):
    """
    Test name:
        - Multiple Start Stop Pause on Pressure sensor
    purpose:
        - confirming that one pressure sensor can have a start, stop, condition on multiple Programs
        - one SSP condition can:
            - stop multiple Programs at once
            - start a program
    Coverage area: \n
        This test covers using a pressure sensor to set and activate SSP conditions on multiple Programs at once \n
        -setting up devices:
            - Loading \n
            - Searching \n
            - Addressing \n
                - Setting:
                    - descriptions
                    - locations \n
        - setting Programs: \n
            - start times

        - attaching zones to program \n

        - setting up mainline: \n
            set main line \n
                - limit zones by flow \n
                - pipe fill time\n
                - target flow \n
                - high variance limit \n
                - high variance shut down \n
                - low variance limit \n
                - low variance shut down \n
                - adding a zone to it \n

        - setting up poc's: \n
            - enable POC \n
            - assign:
                - master valve \n
                - flow meter \n
                - target flow\n
                - main line \n

        - turning off multiple Programs at once
        - starting a program with the same pressure sensor that stopped another program
        - checking when Programs turn on they obey zone concurrency
    """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize use case instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        program_no_start_time = []

        try:
            self.config.BaseStation3200[1].set_serial_port_timeout(timeout=1200)
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=5)

            # Add & Configure Program 1
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_no_start_time)

            # Add & Configure Program 96
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=96)
            self.config.BaseStation3200[1].programs[96].set_enabled()
            self.config.BaseStation3200[1].programs[96].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation3200[1].programs[96].set_start_times(_st_list=program_no_start_time)

            # Add & Configure Program 97
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=97)
            self.config.BaseStation3200[1].programs[97].set_enabled()
            self.config.BaseStation3200[1].programs[97].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation3200[1].programs[97].set_start_times(_st_list=program_no_start_time)

            # Add & Configure Program 98
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=98)
            self.config.BaseStation3200[1].programs[98].set_enabled()
            self.config.BaseStation3200[1].programs[98].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation3200[1].programs[98].set_start_times(_st_list=program_no_start_time)

            # Add & Configure Program 99
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=99)
            self.config.BaseStation3200[1].programs[99].set_enabled()
            self.config.BaseStation3200[1].programs[99].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation3200[1].programs[99].set_start_times(_st_list=program_no_start_time)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        setup start/stop/pause conditions: \n
            - Each one is tested with an upper and lower limit. \n
            - make Programs 1
                - start by (PS[1])
                - pause by (PS[2]),
                - stop by (PS[3])
            - make program 96
                - start by (PS[2])
                - pause by (PS[3]),
                - stop by (PS[4])
            - make program 97
                - start by (PS[3])
                - pause by (PS[4]),
                - stop by (PS[5])
            - make program 98
                - start by (PS[4])
                - pause by (PS[5]),
                - stop by (PS[1])
            - make program 99
                - start by (PS[5])
                - pause by (PS[1]),
                - stop by (PS[2])
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # ----------------------------------------------------------------------------------------------------------
            # PRESSURE START CONDITIONS GROUP 1 - UPPER LIMIT
            # ----------------------------------------------------------------------------------------------------------
            self.config.BaseStation3200[1].programs[1].add_pressure_start_condition(_pressure_sensor_address=1)
            self.config.BaseStation3200[1].programs[1].pressure_start_conditions[1].set_pressure_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[1].pressure_start_conditions[1].set_pressure_limit(_limit=60)

            self.config.BaseStation3200[1].programs[96].add_pressure_start_condition(_pressure_sensor_address=2)
            self.config.BaseStation3200[1].programs[96].pressure_start_conditions[2].set_pressure_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[96].pressure_start_conditions[2].set_pressure_limit(_limit=60)

            self.config.BaseStation3200[1].programs[97].add_pressure_start_condition(_pressure_sensor_address=3)
            self.config.BaseStation3200[1].programs[97].pressure_start_conditions[3].set_pressure_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[97].pressure_start_conditions[3].set_pressure_limit(_limit=60)

            self.config.BaseStation3200[1].programs[98].add_pressure_start_condition(_pressure_sensor_address=4)
            self.config.BaseStation3200[1].programs[98].pressure_start_conditions[4].set_pressure_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[98].pressure_start_conditions[4].set_pressure_limit(_limit=60)

            self.config.BaseStation3200[1].programs[99].add_pressure_start_condition(_pressure_sensor_address=5)
            self.config.BaseStation3200[1].programs[99].pressure_start_conditions[5].set_pressure_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[99].pressure_start_conditions[5].set_pressure_limit(_limit=60)

            # ----------------------------------------------------------------------------------------------------------
            # PRESSURE PAUSE CONDITIONS GROUP 1 - UPPER LIMIT
            # ----------------------------------------------------------------------------------------------------------
            self.config.BaseStation3200[1].programs[1].add_pressure_pause_condition(_pressure_sensor_address=2)
            self.config.BaseStation3200[1].programs[1].pressure_pause_conditions[2].set_pressure_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[1].pressure_pause_conditions[2].set_pressure_limit(_limit=60)
            self.config.BaseStation3200[1].programs[1].pressure_pause_conditions[2].set_pressure_pause_time(_minutes=2)

            self.config.BaseStation3200[1].programs[96].add_pressure_pause_condition(_pressure_sensor_address=3)
            self.config.BaseStation3200[1].programs[96].pressure_pause_conditions[3].set_pressure_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[96].pressure_pause_conditions[3].set_pressure_limit(_limit=60)
            self.config.BaseStation3200[1].programs[96].pressure_pause_conditions[3].set_pressure_pause_time(_minutes=2)

            self.config.BaseStation3200[1].programs[97].add_pressure_pause_condition(_pressure_sensor_address=4)
            self.config.BaseStation3200[1].programs[97].pressure_pause_conditions[4].set_pressure_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[97].pressure_pause_conditions[4].set_pressure_limit(_limit=60)
            self.config.BaseStation3200[1].programs[97].pressure_pause_conditions[4].set_pressure_pause_time(_minutes=2)

            self.config.BaseStation3200[1].programs[98].add_pressure_pause_condition(_pressure_sensor_address=5)
            self.config.BaseStation3200[1].programs[98].pressure_pause_conditions[5].set_pressure_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[98].pressure_pause_conditions[5].set_pressure_limit(_limit=60)
            self.config.BaseStation3200[1].programs[98].pressure_pause_conditions[5].set_pressure_pause_time(_minutes=2)

            self.config.BaseStation3200[1].programs[99].add_pressure_pause_condition(_pressure_sensor_address=1)
            self.config.BaseStation3200[1].programs[99].pressure_pause_conditions[1].set_pressure_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[99].pressure_pause_conditions[1].set_pressure_limit(_limit=60)
            self.config.BaseStation3200[1].programs[99].pressure_pause_conditions[1].set_pressure_pause_time(_minutes=2)

            # ----------------------------------------------------------------------------------------------------------
            # PRESSURE STOP CONDITIONS GROUP 1 - UPPER LIMIT
            # ----------------------------------------------------------------------------------------------------------
            self.config.BaseStation3200[1].programs[1].add_pressure_stop_condition(_pressure_sensor_address=3)
            self.config.BaseStation3200[1].programs[1].pressure_stop_conditions[3].set_pressure_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[1].pressure_stop_conditions[3].set_pressure_limit(_limit=60)

            self.config.BaseStation3200[1].programs[96].add_pressure_stop_condition(_pressure_sensor_address=4)
            self.config.BaseStation3200[1].programs[96].pressure_stop_conditions[4].set_pressure_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[96].pressure_stop_conditions[4].set_pressure_limit(_limit=60)

            self.config.BaseStation3200[1].programs[97].add_pressure_stop_condition(_pressure_sensor_address=5)
            self.config.BaseStation3200[1].programs[97].pressure_stop_conditions[5].set_pressure_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[97].pressure_stop_conditions[5].set_pressure_limit(_limit=60)

            self.config.BaseStation3200[1].programs[98].add_pressure_stop_condition(_pressure_sensor_address=1)
            self.config.BaseStation3200[1].programs[98].pressure_stop_conditions[1].set_pressure_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[98].pressure_stop_conditions[1].set_pressure_limit(_limit=60)

            self.config.BaseStation3200[1].programs[99].add_pressure_stop_condition(_pressure_sensor_address=2)
            self.config.BaseStation3200[1].programs[99].pressure_stop_conditions[2].set_pressure_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[99].pressure_stop_conditions[2].set_pressure_limit(_limit=60)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method

        try:
            # Programs 1 has 8 zones
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[1].zone_programs[4].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[5].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=6)
            self.config.BaseStation3200[1].programs[1].zone_programs[6].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=7)
            self.config.BaseStation3200[1].programs[1].zone_programs[7].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=8)
            self.config.BaseStation3200[1].programs[1].zone_programs[8].set_run_time(_minutes=15)

            # Program 96 has 4 zones (81-84)
            self.config.BaseStation3200[1].programs[96].add_zone_to_program(_zone_address=81)
            self.config.BaseStation3200[1].programs[96].zone_programs[81].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[96].add_zone_to_program(_zone_address=82)
            self.config.BaseStation3200[1].programs[96].zone_programs[82].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[96].add_zone_to_program(_zone_address=83)
            self.config.BaseStation3200[1].programs[96].zone_programs[83].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[96].add_zone_to_program(_zone_address=84)
            self.config.BaseStation3200[1].programs[96].zone_programs[84].set_run_time(_minutes=15)

            # Program 97 has 4 zones (85-88)
            self.config.BaseStation3200[1].programs[97].add_zone_to_program(_zone_address=85)
            self.config.BaseStation3200[1].programs[97].zone_programs[85].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[97].add_zone_to_program(_zone_address=86)
            self.config.BaseStation3200[1].programs[97].zone_programs[86].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[97].add_zone_to_program(_zone_address=87)
            self.config.BaseStation3200[1].programs[97].zone_programs[87].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[97].add_zone_to_program(_zone_address=88)
            self.config.BaseStation3200[1].programs[97].zone_programs[88].set_run_time(_minutes=15)

            # Program 98 has 4 zones (89-92)
            self.config.BaseStation3200[1].programs[98].add_zone_to_program(_zone_address=89)
            self.config.BaseStation3200[1].programs[98].zone_programs[89].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[98].add_zone_to_program(_zone_address=90)
            self.config.BaseStation3200[1].programs[98].zone_programs[90].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[98].add_zone_to_program(_zone_address=91)
            self.config.BaseStation3200[1].programs[98].zone_programs[91].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[98].add_zone_to_program(_zone_address=92)
            self.config.BaseStation3200[1].programs[98].zone_programs[92].set_run_time(_minutes=15)

            # Program 99 has 4 zones (93-96)
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=93)
            self.config.BaseStation3200[1].programs[99].zone_programs[93].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=94)
            self.config.BaseStation3200[1].programs[99].zone_programs[94].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=95)
            self.config.BaseStation3200[1].programs[99].zone_programs[95].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=96)
            self.config.BaseStation3200[1].programs[99].zone_programs[96].set_run_time(_minutes=15)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].add_master_valve_to_point_of_control(_master_valve_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_flow_meter_to_point_of_control(_flow_meter_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=500)

            # Add POC 1 to Water Source 1
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(_point_of_control_address=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add & Configure ML 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=500)

            # Add ML 1 to POC 1
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        Set the time to 7:53:00 \n
        Increment the clock by 6 minutes to 7:59:00 \n
        Verify the status for each program and zone on the controller \n
        - Program status: \n
            - program 1 done \n
            - program 96 done \n
            - program 97 done \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - all zones done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_date_and_time(_date='08/28/2014', _time='7:53:00')
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].do_increment_clock(minutes=6)
            self.config.BaseStation3200[1].verify_date_and_time()

            for zone in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        - set ps[1] to 65, which is above program 1's upper limit start trigger of 60 \n
        - start program 1 \n
        - pause program 99 \n
        - stop program 98 \n
        - set the time to 8:10:00 \n
        - Program status: \n
            - program 1 running \n
            - program 96 done \n
            - program 97 done \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-4 watering \n
            - zones 5-8 waiting \n
            - zones 81-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=11)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.set_pressure_value_in_psi(_psi=65)
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 5):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(5, 9):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_waiting_to_water()
            for zone in range(81, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.set_pressure_value_in_psi(_psi=45)
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        - set ps[2] to 65, which is above program 96's upper limit start trigger of 60 \n
        - start program 96 \n
        - pause program 1 \n
        - stop program 99 \n
        - set the time to 8:15:00 \n
        - Program status: \n
            - program 1 paused \n
            - program 96 running \n
            - program 97 done \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-8 paused \n
            - zones 81-84 watering \n
            - zones 85-88 done \n
            - zones 89-92 done \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].pressure_sensors[2].bicoder.set_pressure_value_in_psi(_psi=65)
            self.config.BaseStation3200[1].pressure_sensors[2].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 9):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_paused()
            for zone in range(81, 85):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(85, 89):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(89, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].pressure_sensors[2].bicoder.set_pressure_value_in_psi(_psi=45)
            self.config.BaseStation3200[1].pressure_sensors[2].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        - set ps[3] to 65, which is above program 97's upper limit start trigger of 60 \n
        - start program 97 \n
        - pause program 96 \n
        - stop program 1 \n
        - set the time to 8:20:00 \n
        - Program status: \n
            - program 1 done \n
            - program 96 paused \n
            - program 97 running \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-8 done \n
            - zones 81-84 paused \n
            - zones 85-88 watering \n
            - zones 89-92 done \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].pressure_sensors[3].bicoder.set_pressure_value_in_psi(_psi=65)
            self.config.BaseStation3200[1].pressure_sensors[3].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 9):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(81, 85):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_paused()
            for zone in range(85, 89):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(89, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].pressure_sensors[3].bicoder.set_pressure_value_in_psi(_psi=45)
            self.config.BaseStation3200[1].pressure_sensors[3].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        - set ps[4] to 65, which is above program 98's upper limit start trigger of 60 \n
        - start program 98 \n
        - pause program 97 \n
        - stop program 96 \n
        - set the time to 8:25:00 \n
        - Program status: \n
            - program 1 done \n
            - program 96 done \n
            - program 97 paused \n
            - program 98 running \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-8 done \n
            - zones 81-84 done \n
            - zones 85-88 paused \n
            - zones 89-92 watering \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].pressure_sensors[4].bicoder.set_pressure_value_in_psi(_psi=65)
            self.config.BaseStation3200[1].pressure_sensors[4].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 9):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(81, 85):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(85, 89):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_paused()
            for zone in range(89, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].pressure_sensors[4].bicoder.set_pressure_value_in_psi(_psi=45)
            self.config.BaseStation3200[1].pressure_sensors[4].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        - set ps[5] to 65, which is above program 99's upper limit start trigger of 60 \n
        - start program 99 \n
        - pause program 98 \n
        - stop program 97 \n
        - set the time to 8:30:00 \n
        - Program status: \n
            - program 1 done \n
            - program 96 done \n
            - program 97 done \n
            - program 98 paused \n
            - program 99 running \n
        - Zone status: \n
            - zones 1-8 done \n
            - zones 81-84 done \n
            - zones 85-88 done \n
            - zones 89-92 paused \n
            - zones 93-96 watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].pressure_sensors[5].bicoder.set_pressure_value_in_psi(_psi=65)
            self.config.BaseStation3200[1].pressure_sensors[5].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 9):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(81, 85):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(85, 89):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(89, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_paused()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].pressure_sensors[5].bicoder.set_pressure_value_in_psi(_psi=45)
            self.config.BaseStation3200[1].pressure_sensors[5].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    # FROM HERE ON OUT WE ARE TRIGGERING LOWER LIMITS

    #################################
    def step_14(self):
        """
        setup start/stop/pause conditions: \n
            - LOWER LIMIT CONDITIONS \n
            - make Programs 1
                - start by (PS[1])
                - pause by (PS[2]),
                - stop by (PS[3])
            - make program 96
                - start by (PS[2])
                - pause by (PS[3]),
                - stop by (PS[4])
            - make program 97
                - start by (PS[3])
                - pause by (PS[4]),
                - stop by (PS[5])
            - make program 98
                - start by (PS[4])
                - pause by (PS[5]),
                - stop by (PS[1])
            - make program 99
                - start by (PS[5])
                - pause by (PS[1]),
                - stop by (PS[2])
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # ----------------------------------------------------------------------------------------------------------
            # PRESSURE START CONDITIONS GROUP 2 - LOWER LIMIT
            # ----------------------------------------------------------------------------------------------------------
            self.config.BaseStation3200[1].programs[1].add_pressure_start_condition(_pressure_sensor_address=1)
            self.config.BaseStation3200[1].programs[1].pressure_start_conditions[1].set_pressure_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[1].pressure_start_conditions[1].set_pressure_limit(_limit=30)

            self.config.BaseStation3200[1].programs[96].add_pressure_start_condition(_pressure_sensor_address=2)
            self.config.BaseStation3200[1].programs[96].pressure_start_conditions[2].set_pressure_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[96].pressure_start_conditions[2].set_pressure_limit(_limit=30)

            self.config.BaseStation3200[1].programs[97].add_pressure_start_condition(_pressure_sensor_address=3)
            self.config.BaseStation3200[1].programs[97].pressure_start_conditions[3].set_pressure_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[97].pressure_start_conditions[3].set_pressure_limit(_limit=30)

            self.config.BaseStation3200[1].programs[98].add_pressure_start_condition(_pressure_sensor_address=4)
            self.config.BaseStation3200[1].programs[98].pressure_start_conditions[4].set_pressure_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[98].pressure_start_conditions[4].set_pressure_limit(_limit=30)

            self.config.BaseStation3200[1].programs[99].add_pressure_start_condition(_pressure_sensor_address=5)
            self.config.BaseStation3200[1].programs[99].pressure_start_conditions[5].set_pressure_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[99].pressure_start_conditions[5].set_pressure_limit(_limit=30)

            # ----------------------------------------------------------------------------------------------------------
            # PRESSURE PAUSE CONDITIONS GROUP 2 - LOWER LIMIT
            # ----------------------------------------------------------------------------------------------------------
            self.config.BaseStation3200[1].programs[1].add_pressure_pause_condition(_pressure_sensor_address=2)
            self.config.BaseStation3200[1].programs[1].pressure_pause_conditions[2].set_pressure_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[1].pressure_pause_conditions[2].set_pressure_limit(_limit=30)
            self.config.BaseStation3200[1].programs[1].pressure_pause_conditions[2].set_pressure_pause_time(_minutes=2)

            self.config.BaseStation3200[1].programs[96].add_pressure_pause_condition(_pressure_sensor_address=3)
            self.config.BaseStation3200[1].programs[96].pressure_pause_conditions[3].set_pressure_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[96].pressure_pause_conditions[3].set_pressure_limit(_limit=30)
            self.config.BaseStation3200[1].programs[96].pressure_pause_conditions[3].set_pressure_pause_time(_minutes=2)

            self.config.BaseStation3200[1].programs[97].add_pressure_pause_condition(_pressure_sensor_address=4)
            self.config.BaseStation3200[1].programs[97].pressure_pause_conditions[4].set_pressure_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[97].pressure_pause_conditions[4].set_pressure_limit(_limit=30)
            self.config.BaseStation3200[1].programs[97].pressure_pause_conditions[4].set_pressure_pause_time(_minutes=2)

            self.config.BaseStation3200[1].programs[98].add_pressure_pause_condition(_pressure_sensor_address=5)
            self.config.BaseStation3200[1].programs[98].pressure_pause_conditions[5].set_pressure_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[98].pressure_pause_conditions[5].set_pressure_limit(_limit=30)
            self.config.BaseStation3200[1].programs[98].pressure_pause_conditions[5].set_pressure_pause_time(_minutes=2)

            self.config.BaseStation3200[1].programs[99].add_pressure_pause_condition(_pressure_sensor_address=1)
            self.config.BaseStation3200[1].programs[99].pressure_pause_conditions[1].set_pressure_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[99].pressure_pause_conditions[1].set_pressure_limit(_limit=30)
            self.config.BaseStation3200[1].programs[99].pressure_pause_conditions[1].set_pressure_pause_time(_minutes=2)

            # ----------------------------------------------------------------------------------------------------------
            # PRESSURE STOP CONDITIONS GROUP 2 - LOWER LIMIT
            # ----------------------------------------------------------------------------------------------------------
            self.config.BaseStation3200[1].programs[1].add_pressure_stop_condition(_pressure_sensor_address=3)
            self.config.BaseStation3200[1].programs[1].pressure_stop_conditions[3].set_pressure_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[1].pressure_stop_conditions[3].set_pressure_limit(_limit=30)

            self.config.BaseStation3200[1].programs[96].add_pressure_stop_condition(_pressure_sensor_address=4)
            self.config.BaseStation3200[1].programs[96].pressure_stop_conditions[4].set_pressure_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[96].pressure_stop_conditions[4].set_pressure_limit(_limit=30)

            self.config.BaseStation3200[1].programs[97].add_pressure_stop_condition(_pressure_sensor_address=5)
            self.config.BaseStation3200[1].programs[97].pressure_stop_conditions[5].set_pressure_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[97].pressure_stop_conditions[5].set_pressure_limit(_limit=30)

            self.config.BaseStation3200[1].programs[98].add_pressure_stop_condition(_pressure_sensor_address=1)
            self.config.BaseStation3200[1].programs[98].pressure_stop_conditions[1].set_pressure_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[98].pressure_stop_conditions[1].set_pressure_limit(_limit=30)

            self.config.BaseStation3200[1].programs[99].add_pressure_stop_condition(_pressure_sensor_address=2)
            self.config.BaseStation3200[1].programs[99].pressure_stop_conditions[2].set_pressure_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[99].pressure_stop_conditions[2].set_pressure_limit(_limit=30)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_15(self):
        """
        - set ps[1] to 25, which is above program 1's lower limit start trigger of 30 \n
        - start program 1 \n
        - pause program 99 \n
        - stop program 98 \n
        - set the time to 8:35:00 \n
        - Program status: \n
            - program 1 running \n
            - program 96 done \n
            - program 97 done \n
            - program 98 done \n
            - program 99 paused \n
        - Zone status: \n
            - zones 1-4 watering \n
            - zones 1-8 waiting \n
            - zones 81-84 done \n
            - zones 85-88 done \n
            - zones 89-92 done \n
            - zones 93-96 paused \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.set_pressure_value_in_psi(_psi=25)
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 5):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(5, 9):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_waiting_to_water()
            for zone in range(81, 85):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(85, 89):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(89, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_paused()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_paused()

            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.set_pressure_value_in_psi(_psi=45)
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_16(self):
        """
        - set ps[2] to 25, which is above program 96's lower limit start trigger of 30 \n
        - start program 96 \n
        - pause program 1 \n
        - stop program 99 \n
        - set the time to 8:40:00 \n
        - Program status: \n
            - program 1 paused \n
            - program 96 running \n
            - program 97 done \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-8 paused \n
            - zones 81-84 watering \n
            - zones 85-88 done \n
            - zones 89-92 done \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].pressure_sensors[2].bicoder.set_pressure_value_in_psi(_psi=25)
            self.config.BaseStation3200[1].pressure_sensors[2].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 9):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_paused()
            for zone in range(81, 85):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(85, 89):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(89, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].pressure_sensors[2].bicoder.set_pressure_value_in_psi(_psi=45)
            self.config.BaseStation3200[1].pressure_sensors[2].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_17(self):
        """
        - set ps[3] to 25, which is above program 97's lower limit start trigger of 30 \n
        - start program 97 \n
        - pause program 96 \n
        - stop program 1 \n
        - set the time to 8:45:00 \n
        - Program status: \n
            - program 1 done \n
            - program 96 paused \n
            - program 97 running \n
            - program 98 done \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-8 done \n
            - zones 81-84 paused \n
            - zones 85-88 watering \n
            - zones 89-92 done \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].pressure_sensors[3].bicoder.set_pressure_value_in_psi(_psi=25)
            self.config.BaseStation3200[1].pressure_sensors[3].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 9):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(81, 85):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_paused()
            for zone in range(85, 89):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(89, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].pressure_sensors[3].bicoder.set_pressure_value_in_psi(_psi=45)
            self.config.BaseStation3200[1].pressure_sensors[3].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_18(self):
        """
        - set ps[4] to 25, which is above program 98's lower limit start trigger of 30 \n
        - start program 98 \n
        - pause program 97 \n
        - stop program 96 \n
        - set the time to 8:50:00 \n
        - Program status: \n
            - program 1 done \n
            - program 96 done \n
            - program 97 paused \n
            - program 98 running \n
            - program 99 done \n
        - Zone status: \n
            - zones 1-8 done \n
            - zones 81-84 done \n
            - zones 85-88 paused \n
            - zones 89-92 watering \n
            - zones 93-96 done \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].pressure_sensors[4].bicoder.set_pressure_value_in_psi(_psi=25)
            self.config.BaseStation3200[1].pressure_sensors[4].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 9):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(81, 85):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(85, 89):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_paused()
            for zone in range(89, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].pressure_sensors[4].bicoder.set_pressure_value_in_psi(_psi=45)
            self.config.BaseStation3200[1].pressure_sensors[4].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_19(self):
        """
        - set ps[5] to 25, which is above program 99's lower limit start trigger of 30 \n
        - start program 99 \n
        - pause program 98 \n
        - stop program 97 \n
        - set the time to 8:30:00 \n
        - Program status: \n
            - program 1 done \n
            - program 96 done \n
            - program 97 done \n
            - program 98 paused \n
            - program 99 running \n
        - Zone status: \n
            - zones 1-8 done \n
            - zones 81-84 done \n
            - zones 85-88 done \n
            - zones 89-92 paused \n
            - zones 93-96 watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].pressure_sensors[5].bicoder.set_pressure_value_in_psi(_psi=25)
            self.config.BaseStation3200[1].pressure_sensors[5].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in range(1, 9):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(81, 85):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(85, 89):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_done()
            for zone in range(89, 93):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_paused()
            for zone in range(93, 97):
                self.config.BaseStation3200[1].zones[zone].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[96].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[97].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[98].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].programs[99].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].pressure_sensors[5].bicoder.set_pressure_value_in_psi(_psi=45)
            self.config.BaseStation3200[1].pressure_sensors[5].bicoder.self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
