import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

__author__ = 'Tige'


class ControllerUseCase34(object):
    """
    This test cover basic Learn flow operations: \n
        First it clears the controller so we have a base point to start from, then it loads some device so that we can\n
        configure them into the controller (4 zones 1 POC 1 mainline, 1 program) \n
    Coverage area: \n
    1. searching and assigning zones \n
    2. searching and assigning master valve \n
    3. searching and assigning a flow meter \n
    4. setting the k value for a given flow meter \n
    5. setting a POC \n
    6. assign a program to a mainline \n
    7. learn flow of a single program that is assign to a single mainline which is assign to a single POC \n
    8. learn flow of a multiple zones one at a time \n
    9. design flow of a zone being over written by a learn flow after a learn flow is complete \n
    10. changing flow rate while learning flow of a zone causing learn flow failures \n
    11.	Changing flow rate during pipe fill time verify learn flow of a zone is still successful \n
    12.	changing flow rate while learning flow of a zone causing learn flow failures \n
    13.	Changing flow rate during pipe fill time verify learn flow of a program is still successful \n
    14.	changing flow rate while learning flow of a zone causing learn flow failures \n
    15.	activating a master valve when learn flows are started \n
    16.	exercise four verifies that soak cycles work correctly with flow operation while using flow \n

    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        # These are set in the PG3200 object
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=10)

            program_start_time_7am = [420]

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_start_time_7am)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(
                _sun=True, _mon=True, _tues=True, _wed=True, _thurs=True, _fri=True, _sat=True
            )
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        Setup Zones on programs
        ############################

        Add zone -----> to program
            - set up zone program  Attributes \n
                - set zone type |Timed, Primary, Linked | \n
                - set runtime\n
                - set cycle time \n
                - set soak time \n
            - if zone type |Linked| \n
                - set tracking ratio \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=60)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=4)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[1].zone_programs[4].set_as_linked_zone(_primary_zone=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        Setup water sources
        ############################

        Add water sources -----> to controlLer
            - set up water source  Attributes \n
                - set enable state \n
                - set priority \n
                - set water budget \n
                - set water rationing state \n
                - set high pressure limit with shut down state \n
                - set low pressure limit with shut down state \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:

            # Water Source 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].set_priority(_priority_for_water_source=2)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        Setup points of control
        ############################

        Add Points of Control -----> to controlLer
            - set up points of control Attributes \n
                - set enable state \n
                - set target flow \n
                - set high flow limit with shut down state \n
                - set unscheduled flow limit with shut down state \n
                - set high pressure limit with shut down state \n
                - set low pressure limit with shut down state \n
        Add flow meters ---> to point of control \n
        Add pump ---> to point of control \n
        Add master valve  ---> to point of control \n
        Add pressure sensor  ---> to point of control \n
        Add Points of Control -----> To Water Source
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:

            # Add & Configure POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=50)

            # Add MV 1 to POC 1
            self.config.BaseStation3200[1].points_of_control[1].\
                add_master_valve_to_point_of_control(_master_valve_address=1)

            # Add FM 1 to POC 1
            self.config.BaseStation3200[1].points_of_control[1].\
                add_flow_meter_to_point_of_control(_flow_meter_address=1)

            # Add POC 1 to Water Source 1
            self.config.BaseStation3200[1].water_sources[1].\
                add_point_of_control_to_water_source(_point_of_control_address=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        Setup mainlines
        ############################

        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
        Add Mainline ---> to point of control
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # Add & Configure ML 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=3)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=40)
            # Add ML 1 to POC 1
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ############################
        Add zones to mainlines
        ############################

        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Mainline 1
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=1)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=2)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=3)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=4)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ###############################
        verify the entire configuration
        ###############################

        - Increment clock to save configuration
        - Get information for each object from controller
        - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        Verify a successful program learn flow
        ###############################
        Flow Setup
        WS 1 ---> PC 1 ---> ML 1
        WS 2 ---> PC 2 ---> ML 1
        ML 1 ---> Pipe fill time 3 min

        - The initial pipe fill time is set to 3 minute pipe fill time
        #### This is how a learn flow works###
        - The time will pass for Pipe fill than two flow reading  will be taken one minute apart \n
        -  While doing a learn flow, if the first reading after the flow stable time is zero the zone will fail
            immediately:  a message will be posted and the next zone will be run.
        - 	After the stable flow time/pressure, two readings will be taken one minute apart.
            a.	The flow difference percent will be compared to 25% to determine success.
            b.	 If the difference percent ( this is equal to the difference over the average) is less than 25%,
                then the average is set on the zone and this zone is marked as done.
            c.	If the flow difference is larger than 25%, then a third reading is taken.
            d.	The flow difference percent of the two closest readings are compared to 25%.
                If less than 25% then success (set the learn flow value and move on) or failure
                (set an error message and move on).
        Set the flow meter reading
            - Flow meter set to 15 GPM \n
        - Set the date and time of the controller so that there is a known days of the week and days of the month \n
        First verify that the design flow for zone 1 through zone 4  is set to zero because they have not learned \n
        flow yet \n
        Step 1
        verify if a value of zero from flow meter is set zones fail immediately
        step 2


        - Perform a learn flow on program 1 at 15 GPM \n

        - Because there are 4 zones and the pipe file time is set to 3 minutes it should run 5 minutes for each zone

        - the clock needs to Increment time by 20 minutes to finish the learn flow all 4 zone should have a value \n
            of 15.0 gpm \n
        -  verify zones and program learn flow and message populate correctly

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            number_of_zones = 4
            pipe_fill_time = self.config.BaseStation3200[1].mainlines[1].ft
            fist_flow_reading_minute = 1
            second_flow_reading_minute = 1
            time_to_increment = 1 + number_of_zones*(pipe_fill_time+fist_flow_reading_minute+second_flow_reading_minute)

            self.config.BaseStation3200[1].date_mngr.set_current_date_to_match_computer()

            # the first minute all flow values of zones should be zero
            for zone in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone].verify_learn_flow(_expected_flow_value=0.0)

            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=15)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()

            # start lean flow of program 1 here
            self.config.BaseStation3200[1].programs[1].set_learn_flow_to_start_standard()

            self.config.BaseStation3200[1].do_increment_clock(minutes=time_to_increment)

            # increment one extra minute to populate the messages
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone].verify_learn_flow(_expected_flow_value=15.0)

            for zone in sorted(self.config.BaseStation3200[1].programs[1].zone_programs.keys()):
                self.config.BaseStation3200[1].zones[zone]\
                    .messages.verify_learn_flow_complete_success_message()
                self.config.BaseStation3200[1].zones[zone]\
                    .messages.clear_learn_flow_complete_success_message()
            # also verify the program display a message as well as the zones
            self.config.BaseStation3200[1].programs[1].messages.verify_learn_flow_complete_success_message()
            self.config.BaseStation3200[1].programs[1].messages.clear_learn_flow_complete_success_message()

            # shut down flow meter here so that an unscheduled flow doesn't occur
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=0.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        ##################
        Re-configure mainlines
        ##################

        - Change pipe file time to see if it takes a shorter time to learn flow of each zone \n

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        ###############################
        Verify a successful program learn flow
        ###############################
        ## This verifies a second learn flow can happen and zone updates ##
        - Set flow meter value to 25 GPM \n
        - Perform a learn flow on program 1 at 25 GPM \n
        - Increment time by 1 minute \n
        - Verify that the flow readings for zones 1 through 4 are 15 at the beginning of the section to check that
          nothing has changed \n
        - Increment time by 13 minutes in 2 sections of seven minutes and six minutes in order to give the learn flow
          time to run \n
        - Verify that the flow readings for zones 1 through 4 are 25 after the learn flow to check that each zone
          successfully learned flow \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # This is set it step 9
            number_of_zones = 4
            pipe_fill_time = self.config.BaseStation3200[1].mainlines[1].ft
            fist_flow_reading_minute = 1
            second_flow_reading_minute = 1
            time_to_increment = 1 + number_of_zones*(pipe_fill_time+fist_flow_reading_minute+second_flow_reading_minute)

            # the first minute all flow values of zones should be zero
            # set flow meter value to 25gpm
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=25.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()

            # Start program learn flow
            self.config.BaseStation3200[1].programs[1].set_learn_flow_to_start_standard()

            # Increment clock for learn flow to start
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # verify that the flow value for each zone hasn't changed
            for zone in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone].verify_learn_flow(_expected_flow_value=15.0)

            # subtract 1 minute because the program already ran 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=time_to_increment-1)

            # Increment clock to trigger an unscheduled flow condition
            self.config.BaseStation3200[1].do_increment_clock(minutes=3)

            # verify new flow value for each zone has change to new flow value of 25 gpm
            for zone in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone].verify_learn_flow(_expected_flow_value=25.0)

            for zone in sorted(self.config.BaseStation3200[1].programs[1].zone_programs.keys()):
                self.config.BaseStation3200[1].zones[zone] \
                    .messages.verify_learn_flow_complete_success_message()
                self.config.BaseStation3200[1].zones[zone] \
                    .messages.clear_learn_flow_complete_success_message()

            self.config.BaseStation3200[1].programs[1].messages.verify_learn_flow_complete_success_message()
            self.config.BaseStation3200[1].programs[1].messages.clear_learn_flow_complete_success_message()
            # shut down flow meter
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=0.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        ##################
        Re-configure mainlines

        ##################

        - set Mainline pipe fill set back to 3 minutes \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
             self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=3)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        ##################
        Verifies learn flow behavior when modifying flow rate of a flow meter
        ##################
        - Set the flow meter value to 50 GPM \n
        - Perform a learn flow on zones 3 and 4 at 50 GPM \n
        - Increment time by 1 minute \n
        - Verify that the flow readings for zones 1-4 are 25 GPM before the test is started \n
        - Increment time by 9 minutes to allow the learn flow to run \n
        - Verify that the flow reading for zones 3 and 4 are 50 GPM after the learn flow to verify that the learn flow
          succeeded \n
        - Verify that the flow readings for zones 1 and 2 are 25 GPM after the learn flow to verify that nothing changed\n
          change flow limit to 50
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # This is set it step 11
            number_of_zones = 2
            pipe_fill_time = self.config.BaseStation3200[1].mainlines[1].ft
            fist_flow_reading_minute = 1
            second_flow_reading_minute = 1
            time_to_increment = 1 + number_of_zones*(pipe_fill_time+fist_flow_reading_minute+second_flow_reading_minute)

            # the first minute all flow values of zones should be zero
            # set flow meter value to 50 gpm
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=50.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()

            # start learn flow on zones 3 and 4
            self.config.BaseStation3200[1].zones[3].set_learn_flow_to_start_standard()
            self.config.BaseStation3200[1].zones[4].set_learn_flow_to_start_standard()
            
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            for zone in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone].verify_learn_flow(_expected_flow_value=25.0)
                
            # subtract 1 minute because the program already ran 1 minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=time_to_increment-1)

            self.config.BaseStation3200[1].zones[1].verify_learn_flow(_expected_flow_value=25.0)
            self.config.BaseStation3200[1].zones[2].verify_learn_flow(_expected_flow_value=25.0)
            self.config.BaseStation3200[1].zones[3].verify_learn_flow(_expected_flow_value=50.0)
            self.config.BaseStation3200[1].zones[4].verify_learn_flow(_expected_flow_value=50.0)

            self.config.BaseStation3200[1].zones[3] \
                .messages.clear_learn_flow_complete_success_message()
            self.config.BaseStation3200[1].zones[4] \
                .messages.clear_learn_flow_complete_success_message()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # advance clock to trigger unscheduled flow
            self.config.BaseStation3200[1].do_increment_clock(minutes=3)

            # shut down flow meter
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=0.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        ##################
        Verifies after learn flow that the correct zone will run based on new design flows
        ##################
        
        - Change max concurrent number of zones on program 1 to 3 because zone concurrance should be based on flow not
        on the zone count \n
        - Update mainline 1's target flow to 50gpm
            - This will allow two zones with a flow of 25gpm to turn on at one time
            - This also verifies that watering will still occur when a POC has a flow of 0
        - Start program 1 let each zone run through one cycle \n
        - Increment the time by 4 minutes zones 1 and 2 will have finished and should go into soaking \n
        - Verify zone 3 turns on since its design flow is 50 \n
        - Verify zones 1 and 2 go into soaking and only zone 3 turns on because the design flow for mainline is set to 
          50 to allow 50 gpm to run \n
        - Verify that the flow readings for zones 3 through 4 are 50 \n
        - Increment the clock 4 minutes \n
        - Because zones 1 and 2 have a soak cycle of 5 minute after zone 3 has completed its cycle zones 1 and 2 should
          turn resume watering \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=3)

            # set flow meter value to 50 gpm
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=50.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=50)

            self.config.BaseStation3200[1].programs[1].set_program_to_start()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            self.config.BaseStation3200[1].zones[1].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].verify_status_is_waiting_to_water()

            # run simulation 4 minutes to to allow zones 1 and 2 to finish first cycle verify zone 3 turns on
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)
            self.config.BaseStation3200[1].zones[1].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            self.config.BaseStation3200[1].zones[1].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].verify_status_is_waiting_to_water()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_14(self):
        """
        ##################
        Verifies a program learn flow fails when changing the flow value
        ##################

        verify that changing the flow value during a program learn flow will fail the learn flow \n

        - Verify a learn flow failure: \n
            - Cause a learn flow failure by varying the flow meter reading by more then
              25% each minute. This will invoke the three strike rule and we will get a learn flow with errors.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:

            # start the learn flow for program 1
            self.config.BaseStation3200[1].programs[1].set_learn_flow_to_start_standard()

            # first zone will start - clock past mainline fill time to first reading
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=5.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            # change flow and advance to next reading
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=50.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            # change flow and advance to next reading
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=35.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # post message and move to zone two
            # change flow and advance to next reading
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=5.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            # change flow and advance to next reading
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=50.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            # change flow and advance to next reading
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=35.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # post message and move to zone three
            # change flow and advance to next reading
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=5.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            # change flow and advance to next reading
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=50.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            # change flow and advance to next reading
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=35.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # post message and move to zone four
            # change flow and advance to next reading
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=5.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            # change flow and advance to next reading
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=50.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            # change flow and advance to next reading
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=35.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # one more minute for final reading and to post message
            self.config.BaseStation3200[1].flow_meters[1].bicoder.set_flow_rate(_gallons_per_minute=5.0)
            self.config.BaseStation3200[1].flow_meters[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # verify messages
            self.config.BaseStation3200[1].zones[1] \
                .messages.verify_learn_flow_complete_error_message()
            self.config.BaseStation3200[1].zones[1] \
                .messages.clear_learn_flow_complete_error_message()
            self.config.BaseStation3200[1].zones[2] \
                .messages.verify_learn_flow_complete_error_message()
            self.config.BaseStation3200[1].zones[2] \
                .messages.clear_learn_flow_complete_error_message()
            self.config.BaseStation3200[1].zones[3] \
                .messages.verify_learn_flow_complete_error_message()
            self.config.BaseStation3200[1].zones[3] \
                .messages.clear_learn_flow_complete_error_message()
            self.config.BaseStation3200[1].zones[4] \
                .messages.verify_learn_flow_complete_error_message()
            self.config.BaseStation3200[1].zones[4] \
                .messages.clear_learn_flow_complete_error_message()

            self.config.BaseStation3200[1].programs[1].messages.verify_learn_flow_complete_errors_message()
            self.config.BaseStation3200[1].programs[1].messages.clear_learn_flow_complete_errors_message()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]