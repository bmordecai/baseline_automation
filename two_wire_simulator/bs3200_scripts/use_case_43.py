import sys
from datetime import timedelta, datetime

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

# from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

from common.object_factory import *


import requests

__author__ = 'kent'


class ControllerUseCase43(object):
    """
    Test name:
        - Test Data Group Packets
    purpose:

    Coverage area: \n

    Date References:
        - configuration for script is located common\configuration_files\EPA_test_configuration.json
        - the devices and addresses range is read from the .json file

    """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        This will verify data packets on the 3200 controller
        """
        controller = None
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            controller = self.config.BaseStation3200[1]
            # test_user_name = "BenAutoTests"
            # test_password = "90ff636a01f8a626c1042ebf87ad9a84"
            # controller.old_data_group_packets = DataGroupPackets(controller, test_user_name, test_password)
            # For ser2 need this value in JSON file: 'logging_ethernet_to_serial_connection'
            self.config.BaseStation3200[1].old_data_group_packets.start_packet_logging()

            minutes = 5.0
            print "Waiting " + str((minutes)) + " minutes for controller to send up all data from use case initialization\n"
            self.config.BaseStation3200[1].old_data_group_packets.resume_packet_logging()
            time.sleep(minutes * 60.0)
            self.config.BaseStation3200[1].old_data_group_packets.stop_packet_logging()

            #####################
            # verify packet 302 #
            #####################
            data_group = '302'
            # Step 1: Get an existing Moisture sensor and set the attributes to new ones
            # Get a current Moisture address 1
            moisture_sensor = self.config.BaseStation3200[1].moisture_sensors[1]

            # Set the attributes
            moisture_sensor.ds = 'test table programming'
            moisture_sensor.en = opcodes.false
            moisture_sensor.la = 45.607999
            moisture_sensor.lg = -119.2356

            # Step 2: Create a TS packet using the packet builder
            ts_packet_packet_302 = self.config.BaseStation3200[1].old_data_group_packets.build_ts_packet_dg_302_single_moisture_sensor(
                                                                                                     moisture_sensor)

            # Step 3: Create a TQ packet using the packet builder. This gets a single moisture sensor
            tq_packet_packet_302 = self.config.BaseStation3200[1].old_data_group_packets.build_tq_packet_dg_302_get_single_moisture_sensor(
                                                                                                         moisture_sensor)
            # wait for the controller to send up all the data from test case set up

            # # Step 4: Start the thread to begin recording data
            self.config.BaseStation3200[1].old_data_group_packets.resume_packet_logging()

            # Step 5: Send the TS packet to the controller using a http call
            self.config.BaseStation3200[1].old_data_group_packets.send_ts_packet(packet=ts_packet_packet_302)
            time.sleep(15)
            # Step 6: Send the TQ packet to the controller using an http call
            self.config.BaseStation3200[1].old_data_group_packets.send_tq_packet(packet=tq_packet_packet_302)
            time.sleep(5)
            # Step 7: Shut down the thread so we can verify what was recorded in the logs.
            self.config.BaseStation3200[1].old_data_group_packets.stop_packet_logging()

            # Step 8: Parse the Log File. If there is a packet 302, the packet structure and keys will be verified in
            # this method
            self.config.BaseStation3200[1].old_data_group_packets.parse_packet_log_file()

            try:
                # Step 9: Get the parsed 302 data packet
                data_packet = self.config.BaseStation3200[1].old_data_group_packets.te_packets[data_group]
            except KeyError:
                e_msg = "There is no data group packet for DG" + str(data_group)
                raise Exception(e_msg)
            else:
                # Step 10: Verify that the attributes on the controller match the attributes of the test engine object
                #  using the key values from the Data Group Packet
                self.config.BaseStation3200[1].old_data_group_packets.verify_packet_dg_302_key_values(parsed_packet=data_packet,
                                                                              test_engine_moisture_sensor=moisture_sensor)
                # Step 11: Use the serial port to verify the test engine object
                moisture_sensor.verify_who_i_am()

            #####################
            # verify packet 402 #
            #####################
            data_group = '402'

            # setup a water source on the controller
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)

            # Step 1: Get an existing water source and set the attributes to new ones
            water_source = self.config.BaseStation3200[1].water_sources[1]

            # Set the attributes
            water_source.ds = 'test water source'
            water_source.en = opcodes.false
            water_source.pr = 3
            water_source.wb = 5000
            water_source.wr = opcodes.false
            water_source.pc = 0
            water_source.sh = opcodes.true
            water_source.ws = opcodes.false

            # Step 2: Create a TS packet using the packet builder
            ts_packet_402 = self.config.BaseStation3200[1].old_data_group_packets.build_ts_packet_dg_402_single_water_source(
                                                                                            water_source_obj=water_source)

            # Step 3: Create a TQ packet using the packet builder. This gets a single water source
            tq_packet_402 = self.config.BaseStation3200[1].old_data_group_packets.build_tq_packet_dg_402_get_single_water_source(
                                                                                                    water_source=water_source)

            # Step 4: Tell the thread to start recording data again
            self.config.BaseStation3200[1].old_data_group_packets.resume_packet_logging()

            # Step 5: Send the TS packet to the controller using a http call
            self.config.BaseStation3200[1].old_data_group_packets.send_ts_packet(packet=ts_packet_402)
            time.sleep(10)
            # Step 6: Send the TQ packet to the controller using a http call
            self.config.BaseStation3200[1].old_data_group_packets.send_tq_packet(packet=tq_packet_402)
            time.sleep(10)
            # Step 7: Shut down the thread so we can verify what was recorded in the logs.
            self.config.BaseStation3200[1].old_data_group_packets.stop_packet_logging()

            # Step 8: Parse the Log File. If there is a packet 402, the packet structure and keys will be verified in
            # this method
            self.config.BaseStation3200[1].old_data_group_packets.parse_packet_log_file()

            # Step 9: Get the parsed 402 data packet
            try:
                data_packet = self.config.BaseStation3200[1].old_data_group_packets.te_packets[data_group]

            except KeyError:
                e_msg = "There is no data group packet for DG" + str(data_group)
                raise Exception(e_msg)
            else:
                # Step 10: Verify that the attributes on the controller match the attributes of the test_engine object
                #  using the key values from the Data Group Packet
                self.config.BaseStation3200[1].old_data_group_packets.verify_packet_dg_402_key_values(parsed_packet=data_packet,
                                                                              test_engine_water_source=water_source)

                # Step 11: Use the serial port to verify the test engine object
                water_source.verify_who_i_am()

            #####################
            # verify packet 412 #
            #####################
            data_group = '412'
            point_of_control_address = 1
            mainline_address = 1

            # setup a point of control on the controller
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=point_of_control_address)

            # setup a mainline on the controller
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=mainline_address)

            # Step 1: Get an existing point of control and set the attributes to new ones
            poc = self.config.BaseStation3200[1].points_of_control[point_of_control_address]

            # Set the attributes
            poc.ds = 'test point of control'
            poc.en = opcodes.false
            poc.fl = 250
            poc.fm = 1
            poc.pp = 1
            poc.hf = 500.5
            poc.hs = opcodes.false
            poc.mv = 1
            poc.uf = 600
            poc.us = opcodes.true
            poc.pr = 3
            poc.wb = 700
            poc.wr = opcodes.true
            poc.pc = 1
            poc.sh = opcodes.true
            poc.ws = opcodes.true
            poc.ps = 1
            poc.hp = 800
            poc.he = opcodes.true
            poc.lp = 900
            poc.le = opcodes.true
            poc.hf = 1000
            # poc.sh = opcodes.false
            # poc.ml = mainline_address

            # Step 2: Create a TS packet using the packet builder
            ts_packet_412 = self.config.BaseStation3200[1].old_data_group_packets.build_ts_packet_dg_412_point_of_control(
                point_of_control_obj=poc)

            # Step 3: Create a TQ packet using the packet builder. This gets a single water source
            tq_packet_412 = self.config.BaseStation3200[1].old_data_group_packets.build_tq_packet_dg_412_get_single_point_of_control(
                point_of_control=poc)

            # Step 4: Tell the thread to start recording data again
            self.config.BaseStation3200[1].old_data_group_packets.resume_packet_logging()

            # Step 5: Send the TS packet to the controller using a http call
            self.config.BaseStation3200[1].old_data_group_packets.send_ts_packet(packet=ts_packet_412)
            time.sleep(10)
            # Step 6: Send the TQ packet to the controller using a http call
            self.config.BaseStation3200[1].old_data_group_packets.send_tq_packet(packet=tq_packet_412)
            time.sleep(10)
            # Step 7: Shut down the thread so we can verify what was recorded in the logs.
            self.config.BaseStation3200[1].old_data_group_packets.stop_packet_logging()

            # Step 8: Parse the Log File. If there is a packet 402, the packet structure and keys will be verified in
            # this method
            self.config.BaseStation3200[1].old_data_group_packets.parse_packet_log_file()

            # Step 9: Get the parsed 402 data packet
            try:
                data_packet = self.config.BaseStation3200[1].old_data_group_packets.te_packets[data_group]

            except KeyError:
                e_msg = "There is no data group packet for DG" + str(data_group)
                raise Exception(e_msg)
            else:
                # Step 10: Verify that the attributes on the controller match the attributes of the test_engine object
                #  using the key values from the Data Group Packet
                self.config.BaseStation3200[1].old_data_group_packets.verify_packet_dg_412_key_values(parsed_packet=data_packet,
                                                                              test_engine_poc=poc)

                # Step 11: Use the serial port to verify the test engine object
                poc.verify_who_i_am()

            #####################
            # verify packet 422 #
            #####################
            data_group = '422'
            mainline_number = 6

            # setup a mainline on the controller
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=mainline_number)

            # Step 1: Get an existing mainline and set the attributes to new ones
            mainline = self.config.BaseStation3200[1].mainlines[mainline_number]

            # Set the attributes
            # Address
            mainline.ad = mainline_number
            # Description
            mainline.ds = 'test mainline'
            # Enabled
            mainline.en = opcodes.false
            # Target flow / design flow value
            mainline.fl = 999
            # Pipe fill time
            mainline.ft = 2
            # Stable flow pressure value
            mainline.sp = 10
            # Use pressure for stable flow
            mainline.up = opcodes.false
            # Use time for stable flow
            mainline.ut = opcodes.true
            # Dynamic flow allocation (Flowstation only)
            mainline.fp = opcodes.false
            # Standard variance shutdown enabled
            mainline.fw = opcodes.false
            # Standard variance limit
            mainline.fv = 0
            # Mainline variance limit
            mainline.mf = 0
            # Mainline variance shutdown enabled
            mainline.mw = opcodes.false
            # Priority (Flowstation only)
            mainline.pr = 3
            # Water out to these mainlines
            mainline.ml = 1
            # Water out to these POCs
            mainline.pc = 1
            # Time delay between MVs and zone
            mainline.tm = 0
            # Time delay between zones
            mainline.tz = 0
            # Share with Flow Station
            mainline.sh = opcodes.false
            # Managed by flowstation (3200 only)
            mainline.fs = opcodes.false
            # Time delay after last zone off to MV off
            mainline.tl = 0
            # Number of zones to delay
            mainline.zc = 0
            # Limit concurrent zones by flow
            mainline.lc = opcodes.false
            # Use advanced flow
            mainline.af = opcodes.false
            # High variance limit
            mainline.ah = 0
            # Low variance limit
            mainline.al = 0
            # High variance limit
            mainline.bh = 0
            # Low variance limit
            mainline.bl = 0
            # High variance limit
            mainline.ch = 0
            # Low variance limit
            mainline.cl = 0
            # High variance limit
            mainline.dh = 0
            # Low variance limit
            mainline.dl = 0
            # Zone high variance detection & shutdown
            mainline.zh = opcodes.false
            # Zone low variance detection & shutdown
            mainline.zl = opcodes.false
            # Delay Units (PS = Pressure, TM = Time)
            mainline.un = opcodes.time

            # Step 2: Create a TS packet using the packet builder
            ts_packet_422 = self.config.BaseStation3200[1].old_data_group_packets.build_ts_packet_mainline_dg_422_single_mainline(
                                                                                            mainline_obj=mainline)

            # Step 3: Create a TQ packet using the packet builder. This gets a single mainline
            tq_packet_422 = self.config.BaseStation3200[1].old_data_group_packets.build_tq_packet_mainline_dg_422_get_single_mainline(
                                                                                                    mainline_address=mainline_number)

            # Step 4: Tell the thread to start recording data again
            self.config.BaseStation3200[1].old_data_group_packets.resume_packet_logging()

            # Step 5: Send the TS packet to the controller using a http call
            self.config.BaseStation3200[1].old_data_group_packets.send_ts_packet(packet=ts_packet_422)
            time.sleep(10)

            # Step 6: Send the TQ packet to the controller using a http call
            self.config.BaseStation3200[1].old_data_group_packets.send_tq_packet(packet=tq_packet_422)
            time.sleep(10)

            # Step 7: Shut down the thread so we can verify what was recorded in the logs.
            self.config.BaseStation3200[1].old_data_group_packets.stop_packet_logging()

            # Step 8: Parse the Log File. If there is a packet 422, the packet structure and keys will be verified
            self.config.BaseStation3200[1].old_data_group_packets.parse_packet_log_file()

            # Step 9: Get the parsed 422 data packet
            try:
                data_packet = self.config.BaseStation3200[1].old_data_group_packets.te_packets[data_group]

            except KeyError:
                e_msg = "There is no data group packet for DG" + str(data_group)
                raise Exception(e_msg)
            else:
                # Step 10: Verify that the attributes on the controller match the attributes of the test_engine object
                #  using the key values from the Data Group Packet
                self.config.BaseStation3200[1].old_data_group_packets.verify_packet_422_key_values(
                                                                            parsed_packet=data_packet,
                                                                            mainline_address=mainline_number,
                                                                            test_engine_mainline=mainline)

                # Step 11: Use the serial port to verify the test engine object
                mainline.verify_who_i_am()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))

            if controller is not None:
                if controller.old_data_group_packets is not None:
                    if controller.old_data_group_packets.log_file is not None:
                        if not controller.old_data_group_packets.log_file.closed:
                            controller.old_data_group_packets.log_file.flush()
                            controller.old_data_group_packets.log_file.close()
                    if controller.old_data_group_packets.read_data_thread is not None:
                        controller.old_data_group_packets.read_data_thread.join(0)
                if controller.ser2.serial_conn.isOpen():
                    controller.ser2.close_connection()

            raise Exception(e_msg)

    ###############################
    def step_2(self):
        """
        This step verifies data groups used by the Flow Station. It requires a controller to be
        attached to a Flow Station. Objects will be shared between the Flow Station and the
        Controller using data group packets. This step will verify the objects are correctly shared.
        """
        controller = None
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Initialize variables for step 2
            controller_3200 = self.config.BaseStation3200[1]
            controller_fs = self.config.FlowStations[1]

            #####################
            # verify packet 332 #
            #####################
            data_group = '332'
            flow_station_number = 1
            flow_station_enabled = opcodes.false
            flow_station_description = 'test flow station'

            # Step 1: Create a TS packet using the packet builder
            ts_packet_332 = self.config.BaseStation3200[1].old_data_group_packets.build_ts_packet_dg_332_flowstation_settings(
                                                                            flow_station=self.config.FlowStations[1],
                                                                            flowstation_address=flow_station_number,
                                                                            enabled=flow_station_enabled,
                                                                            description=flow_station_description)

            # Step 3: Create a TQ packet using the packet builder. This gets a singe flow station setting
            tq_packet_332 = self.config.BaseStation3200[1].old_data_group_packets.build_tq_packet_dg_332_get_all_flowstation_settings()

            # Step 4: Tell the thread to start recording data
            self.config.BaseStation3200[1].old_data_group_packets.resume_packet_logging()
            time.sleep(5)

            # Step 5: Send the TS packet to the self.config.BaseStation3200[1] using a http call
            self.config.BaseStation3200[1].old_data_group_packets.send_ts_packet(packet=ts_packet_332)
            time.sleep(10)
            # Step 6: Send the TQ packet to the self.config.BaseStation3200[1] using a http call
            self.config.BaseStation3200[1].old_data_group_packets.send_tq_packet(packet=tq_packet_332)

            # wait for the controller to send up a CR packet and get the 332 TE response to the TQ packet
            time.sleep(5)

            # Step 7: Shut down the thread so we can verify what was recorded in the logs.
            self.config.BaseStation3200[1].old_data_group_packets.stop_packet_logging()

            # Step 8: Parse the Log File. If there is a packet 332, the packet structure and keys will be verified in
            # this method
            self.config.BaseStation3200[1].old_data_group_packets.parse_packet_log_file()

            # Step 9: Get the parsed 332 data packet
            try:
                parsed_data_packet_332 = self.config.BaseStation3200[1].old_data_group_packets.te_packets[data_group]

            except KeyError:
                e_msg = "There is no data group packet for DG" + str(data_group)
                raise Exception(e_msg)
            else:
                # Step 10: Verify that the attributes on the controller match the attributes of the test_engine object
                #  using the key values from the Data Group Packet
                self.config.BaseStation3200[1].old_data_group_packets.verify_packet_dg_332_key_values(parsed_packet=parsed_data_packet_332,
                                                                                   flow_station=self.config.FlowStations[1],
                                                                                   flowstation_address=flow_station_number,
                                                                                   enabled=flow_station_enabled,
                                                                                   description=flow_station_description)
            #####################
            # verify packet 500 #
            #####################
            data_group = '500'
            flow_station_longitude = 44.45781
            flow_station_latitude = -110.579015
            flow_station_description = '9876543210987654321098765432109'  # A 31-character (max length) description
            self.config.FlowStations[1].ds = flow_station_description
            self.config.FlowStations[1].lg = flow_station_longitude
            self.config.FlowStations[1].la = flow_station_latitude

            # Step 1: Create a TS packet using the packet builder
            ts_packet_500 = self.config.FlowStations[1].old_data_group_packets.build_ts_dg_500_flowstation_settings(
                flowstation=self.config.FlowStations[1])

            # Step 3: Create a TQ packet using the packet builder. This gets a singe flow station setting
            tq_packet_500 = self.config.FlowStations[1].old_data_group_packets.build_tq_dg_500_get_flowstation_settings()

            # Step 4: Tell the thread to start recording data
            self.config.FlowStations[1].old_data_group_packets.start_packet_logging()
            time.sleep(5)

            # Step 5: Send the TS packet to the self.config.FlowStations[1] using a http call
            self.config.FlowStations[1].old_data_group_packets.send_ts_packet(packet=ts_packet_500)
            time.sleep(10)
            self.config.FlowStations[1].save_programming_to_flow_station()
            time.sleep(5)

            # Step 6: Send the TQ packet to the self.config.FlowStations[1] using a http call
            self.config.FlowStations[1].old_data_group_packets.send_tq_packet(packet=tq_packet_500)

            # wait for the controller to send up a CR packet and get the 500 TE response to the TQ packet
            time.sleep(5)

            # Step 7: Shut down the thread so we can verify what was recorded in the logs.
            self.config.FlowStations[1].old_data_group_packets.stop_packet_logging()

            # Step 8: Parse the Log File. If there is a packet 500, the packet structure and keys will be verified in
            # this method
            self.config.FlowStations[1].old_data_group_packets.parse_packet_log_file()

            # Step 9: Get the parsed 500 data packet
            try:
                parsed_data_packet_500 = self.config.FlowStations[1].old_data_group_packets.te_packets[data_group]

            except KeyError:
                e_msg = "There is no data group packet for DG" + str(data_group)
                raise Exception(e_msg)
            else:
                # Step 10: Verify that the attributes on the controller match the attributes of the test_engine object
                #  using the key values from the Data Group Packet
                self.config.FlowStations[1].old_data_group_packets.verify_packet_500_key_values(parsed_packet=parsed_data_packet_500,
                                                                                  flowstation=self.config.FlowStations[1])

            #####################
            # verify packet 570 #
            #####################
            data_group = '570'
            controller_slot_in_flowstation = 1

            # Step 1: Create a TS packet using the packet builder
            ts_packet_570 = self.config.FlowStations[1].old_data_group_packets.build_ts_packet_dg_570_controller_assignment(
                controller=self.config.BaseStation3200[1],
                controller_slot=controller_slot_in_flowstation)

            # Step 3: Create a TQ packet using the packet builder. This gets a singe flow station setting
            tq_packet_570 = self.config.FlowStations[1].old_data_group_packets.build_tq_packet_dg_570_get_all_controller_assignments()

            # Step 4: Tell the thread to start recording data
            self.config.FlowStations[1].old_data_group_packets.resume_packet_logging()
            time.sleep(5)

            # Step 5: Send the TS packet to the self.config.FlowStations[1] using a http call
            self.config.FlowStations[1].old_data_group_packets.send_ts_packet(packet=ts_packet_570)
            time.sleep(10)
            self.config.FlowStations[1].save_programming_to_flow_station()
            time.sleep(5)

            # Step 6: Send the TQ packet to the self.config.FlowStations[1] using a http call
            self.config.FlowStations[1].old_data_group_packets.send_tq_packet(packet=tq_packet_570)

            # wait for the controller to send up data for packet
            time.sleep(10)

            # Step 7: Shut down the thread so we can verify what was recorded in the logs.
            self.config.FlowStations[1].old_data_group_packets.stop_packet_logging()

            # Step 8: Parse the Log File. If there is a packet 570, the packet structure and keys will be verified in
            # this method
            self.config.FlowStations[1].old_data_group_packets.parse_packet_log_file()

            # Step 9: Get the parsed 570 data packet
            try:
                parsed_data_packet_570 = self.config.FlowStations[1].old_data_group_packets.te_packets[data_group]

            except KeyError:
                e_msg = "There is no data group packet for DG" + str(data_group)
                raise Exception(e_msg)
            else:
                # Step 10: Verify that the attributes on the controller match the attributes of the test_engine object
                #  using the key values from the Data Group Packet
                self.config.FlowStations[1].old_data_group_packets.verify_packet_570_key_values(parsed_packet=parsed_data_packet_570,
                                                                                  controller=self.config.BaseStation3200[1],
                                                                                  controller_slot_in_flowstation=controller_slot_in_flowstation)

            #####################
            # verify packet 571 #
            #####################
            # NOTE: Before a packet 571 (Controller Settings) will "take" on a flowstation, the controller for
            # which the settings are being made (or retrieved) must be assigned (using a packet 570, a manual
            # assignment on the flowstation hardware, or the equivalent test engine command).   In short, don't
            # try to run this test in complete isolation!

            data_group = '571'
            enable_controller_in_flowstation = True

            # Step 1: Create a TS packet using the packet builder
            ts_packet_571 = self.config.FlowStations[1].old_data_group_packets.build_ts_dg_571_controller_settings(
                controller=self.config.BaseStation3200[1],
                enabled=enable_controller_in_flowstation)

            # Step 3: Create a TQ packet using the packet builder. This gets a singe flow station setting
            tq_packet_571 = self.config.FlowStations[1].old_data_group_packets.build_tq_dg_571_get_all_controller_settings()

            # Step 4: Tell the thread to start recording data
            self.config.FlowStations[1].old_data_group_packets.resume_packet_logging()   # resume
            time.sleep(5)

            # Step 5: Send the TS packet to the self.config.FlowStations[1] using a http call
            self.config.FlowStations[1].old_data_group_packets.send_ts_packet(packet=ts_packet_571)
            time.sleep(10)
            self.config.FlowStations[1].save_programming_to_flow_station()
            time.sleep(5)

            # Step 6: Send the TQ packet to the self.config.FlowStations[1] using a http call
            self.config.FlowStations[1].old_data_group_packets.send_tq_packet(packet=tq_packet_571)
            time.sleep(5)

            # wait for the controller to send up packets
            time.sleep(10)

            # Step 7: Shut down the thread so we can verify what was recorded in the logs.
            self.config.FlowStations[1].old_data_group_packets.stop_packet_logging()

            # Step 8: Parse the Log File. If there is a packet 571, the packet structure and keys will be verified in
            # this method
            self.config.FlowStations[1].old_data_group_packets.parse_packet_log_file()

            # Step 9: Get the parsed 571 data packet
            try:
                parsed_data_packet_571 = self.config.FlowStations[1].old_data_group_packets.te_packets[data_group]

            except KeyError:
                e_msg = "There is no data group packet for DG" + str(data_group)
                raise Exception(e_msg)
            else:
                # Step 10: Verify that the attributes on the controller match the attributes of the test_engine object
                #  using the key values from the Data Group Packet
                self.config.FlowStations[1].old_data_group_packets.verify_packet_571_key_values(parsed_packet=parsed_data_packet_571,
                                                                                  controller=self.config.BaseStation3200[1],
                                                                                  enabled=enable_controller_in_flowstation)

            # Share the 3200 with the FlowStation

            # ts_packet_570 = self.config.FlowStations[1].old_data_group_packets.build_ts_packet_dg_570_controller_assignment(
            #                                                                 controller=self.config.BaseStation3200[1],
            #                                                                 controller_slot=1)
            # # ts_packet_570 = "TS^DG:570/ID=448/SQ=0^MC:000CC67BC752/CN=1/SN=3X00001^SL:TR^DN:DN"
            # self.config.FlowStations[1].old_data_group_packets.send_ts_packet(packet=ts_packet_570)
            #
            # # Build packet 332 with enabled set to true. Send packet 332 to the Controller.
            # ts_packet_332 = self.config.BaseStation3200[1].old_data_group_packets.build_ts_packet_dg_332_flowstation_settings(
            #     flow_station=self.config.FlowStations[1],
            #     flowstation_address=flow_station_number,
            #     description='Flowstation 1',
            #     enabled=opcodes.true)

            # self.config.BaseStation3200[1].old_data_group_packets.send_ts_packet(packet=ts_packet_332)

            # Add the controller to the FlowStation test engine object and verify that the two are connected using
            # the Serial Port
            self.config.FlowStations[1].add_controller_to_flow_station(1, flow_station_number)

            #########################
            # verify packet 402/501 #
            # ########################################################
            # Share Water Source and verify packet 402 on FlowStation#
            ##########################################################
            data_group_1 = '501'
            data_group_2 = '402'
            fs_water_source_slot_number = 1
            controller_water_source_slot_number = 1

            # Step 1: Add a water source to the controller, get a reference to the new water source
            self.config.BaseStation3200[1].add_water_source_to_controller(controller_water_source_slot_number)
            water_source = self.config.BaseStation3200[1].water_sources[controller_water_source_slot_number]

            # build dg402 request packet
            tq_packet_402 = self.config.FlowStations[1].old_data_group_packets.build_tq_packet_dg_402_get_single_water_source(
                water_source)

            # Step 2: Create TQ and TS packets for DG501
            tq_packet_501 = self.config.FlowStations[1].old_data_group_packets.build_tq_packet_dg_501_get_all_water_source_assignments()

            ts_packet_501 = self.config.FlowStations[1].old_data_group_packets.build_ts_packet_dg_501_water_source_assignment(
                controller=self.config.BaseStation3200[1], flow_station_slot=fs_water_source_slot_number,
                controller_slot=controller_water_source_slot_number)

            # Step 3: Tell the threads to start recording data
            self.config.FlowStations[1].old_data_group_packets.resume_packet_logging()
            self.config.BaseStation3200[1].old_data_group_packets.resume_packet_logging()

            # Step 4: Send the TS packet 501 to the controller using a http call
            self.config.FlowStations[1].old_data_group_packets.send_ts_packet(packet=ts_packet_501)
            time.sleep(10)
            self.config.FlowStations[1].save_programming_to_flow_station()
            time.sleep(5)

            # Step 5: Send the TQ packets to the controller and FlowStation to get packets 402 and 501 using a http call
            self.config.FlowStations[1].old_data_group_packets.send_tq_packet(packet=tq_packet_501)

            # Set new values on the water_source
            water_source.ds = 'test shared fs water source'
            water_source.en = opcodes.true
            water_source.pr = 4
            water_source.wb = 6000
            water_source.wr = opcodes.true
            water_source.pc = 0
            water_source.sh = opcodes.true
            water_source.ws = opcodes.true

            # build a packet 402 with the new values
            ts_packet_402 = self.config.FlowStations[1].old_data_group_packets.build_ts_packet_dg_402_single_water_source(
                                                                                         water_source_obj=water_source)

            # Send the 402 TS packet to the FlowStation
            self.config.FlowStations[1].old_data_group_packets.send_ts_packet(packet=ts_packet_402)
            time.sleep(10)
            self.config.FlowStations[1].save_programming_to_flow_station()
            time.sleep(5)

            # Send the 402 TQ packet to the FlowStation
            self.config.FlowStations[1].old_data_group_packets.send_tq_packet(packet=tq_packet_402)
            time.sleep(5)

            # # Send the 402 TQ packet to the 3200
            # self.config.BaseStation3200[1].old_data_group_packets.send_tq_packet(packet=tq_packet_402)

            # Wait for the controller to process the previous three packets before stopping the logging process
            time.sleep(10)

            # Step 6: Shut down the thread so we can verify what was recorded in the logs.
            self.config.FlowStations[1].old_data_group_packets.stop_packet_logging()
            self.config.BaseStation3200[1].old_data_group_packets.stop_packet_logging()

            # Step 7: Parse the Log File. If there is a packet 402/501, the packet structure and keys will be verified
            # in this method
            self.config.FlowStations[1].old_data_group_packets.parse_packet_log_file()
            self.config.BaseStation3200[1].old_data_group_packets.parse_packet_log_file()

            # Step 9: Get the parsed packet data

            # Get a valid te packets from the FlowStation for DG501
            try:
                data_packet_501 = self.config.FlowStations[1].old_data_group_packets.te_packets[data_group_1]
            except KeyError:
                e_msg = "There is no data group packet for DG" + str(data_group_1)
                raise Exception(e_msg)

            # Get a valid te packets from the FlowStation for DG402
            try:
                data_packet_402_fs = self.config.FlowStations[1].old_data_group_packets.te_packets[data_group_2]
            except KeyError:
                e_msg = "There is no data group packet for DG" + str(data_group_2)
                raise Exception(e_msg)
            # Get a valid te packets from the Controller for DG402
            # try:
            #     data_packet_402_controller = self.config.FlowStations[1].old_data_group_packets.te_packets[data_group_2]
            # except KeyError:
            #     e_msg = "There is no data group packet for DG" + str(data_group_2)
            #     raise Exception(e_msg)

            # Step 8: Verify that the attributes on the controller match the attributes of the test_engine object
            # using the key values from the Data Group Packets. Packet 501 should have an assignment and packet 402
            # should have the same attributes on the Flow Station as it does on the controller
            self.config.FlowStations[1].old_data_group_packets.verify_packet_dg_501_key_values(parsed_packet=data_packet_501,
                                                                             controller=self.config.BaseStation3200[1],
                                                                             fs_slot_number= fs_water_source_slot_number,
                                                                             controller_slot_number= controller_water_source_slot_number)

            # verify that the updated water source source attributes are correct on the FlowStation
            self.config.FlowStations[1].old_data_group_packets.verify_packet_dg_402_key_values(parsed_packet=data_packet_402_fs,
                                                                             test_engine_water_source=water_source)

            #########################
            # verify packet 412/511 #
            #########################
            data_group_1 = '511'
            data_group_2 = '412'
            fs_point_of_control_slot_number = 1
            controller_point_of_control_slot_number = 1

            # Step 1: Add a point of control to the controller, get a reference to the new point of control, and build
            # dg412 request packet
            self.config.BaseStation3200[1].add_point_of_control_to_controller(controller_point_of_control_slot_number)

            poc = self.config.BaseStation3200[1].points_of_control[controller_point_of_control_slot_number]

            tq_packet_412 = self.config.FlowStations[1].old_data_group_packets.build_tq_packet_dg_412_get_single_point_of_control(
                point_of_control=poc)

            # Step 2: Create TQ and TS packets for DG511
            tq_packet_511 = self.config.FlowStations[1].old_data_group_packets.build_tq_packet_dg_511_get_all_point_of_control_assignments()

            ts_packet_511 = self.config.FlowStations[1].old_data_group_packets.build_ts_packet_dg_511_point_of_control_assignment(
                controller=self.config.BaseStation3200[1],
                flow_station_slot=fs_point_of_control_slot_number,
                controller_slot=controller_point_of_control_slot_number)

            # Step 3: Tell the threads to start recording data
            self.config.FlowStations[1].old_data_group_packets.resume_packet_logging()
            self.config.BaseStation3200[1].old_data_group_packets.resume_packet_logging()

            # Step 4: Send the TS packet to the controller using a http call
            self.config.FlowStations[1].old_data_group_packets.send_ts_packet(packet=ts_packet_511)
            time.sleep(10)
            self.config.FlowStations[1].save_programming_to_flow_station()
            time.sleep(5)

            # Step 5: Send the TQ packets to the controller and FlowStation to get packets 412 and 511 using a http call
            self.config.FlowStations[1].old_data_group_packets.send_tq_packet(packet=tq_packet_511)
            time.sleep(5)

            # Set new values on the water_source
            poc.ds = 'test point of control'
            poc.en = opcodes.true
            poc.fl = 200.0
            poc.fm = 1
            poc.pp = 1
            poc.hf = 600.6
            poc.hs = opcodes.true
            poc.mv = 1
            poc.uf = 200
            poc.us = opcodes.false
            poc.pr = 5
            poc.wb = 6000
            poc.wr = opcodes.false
            poc.pc = 1
            poc.sh = opcodes.false
            poc.ws = opcodes.false
            poc.ps = 1
            poc.hp = 200
            poc.he = opcodes.true
            poc.lp = 300
            poc.le = opcodes.true
            poc.hf = 400
            poc.sh = opcodes.true
            poc.ml = 2
            poc.gr = 5
            poc.mf = 8

            # build TS packet 412 with the new values
            ts_packet_412 = self.config.FlowStations[1].old_data_group_packets.build_ts_packet_dg_412_point_of_control(
                point_of_control_obj=poc)

            # Send the 412 TS packet to the FlowStation
            self.config.FlowStations[1].old_data_group_packets.send_ts_packet(packet=ts_packet_412)
            time.sleep(10)
            self.config.FlowStations[1].save_programming_to_flow_station()
            time.sleep(5)

            # Send the 412 TQ packet to the FlowStation
            self.config.FlowStations[1].old_data_group_packets.send_tq_packet(packet=tq_packet_412)
            time.sleep(5)

            # # Send the 412 TQ packet to the 3200
            # self.config.BaseStation3200[1].old_data_group_packets.send_tq_packet(packet=tq_packet_412)

            # Wait for the controller to process the previous three packets before stopping the logging process
            time.sleep(10)

            # Step 6: Shut down the thread so we can verify what was recorded in the logs.
            self.config.FlowStations[1].old_data_group_packets.stop_packet_logging()
            self.config.BaseStation3200[1].old_data_group_packets.stop_packet_logging()

            # Step 7: Parse the Log File. If there is a packet 412/511, the packet structure and keys will be verified
            # in this method
            self.config.FlowStations[1].old_data_group_packets.parse_packet_log_file()
            self.config.BaseStation3200[1].old_data_group_packets.parse_packet_log_file()

            # Step 8: Get the parsed packet data

            # Get a valid te packets from the FlowStation for DG511
            try:
                data_packet_511 = self.config.FlowStations[1].old_data_group_packets.te_packets[data_group_1]
            except KeyError:
                e_msg = "There is no data group packet for DG" + str(data_group_1)
                raise Exception(e_msg)

            # Get a valid te packets from the FlowStation for DG412
            try:
                data_packet_412_fs = self.config.FlowStations[1].old_data_group_packets.te_packets[data_group_2]
            except KeyError:
                e_msg = "There is no data group packet for DG" + str(data_group_2)
                raise Exception(e_msg)

            # Step 9: Verify that the attributes on the controller match the attributes of the test_engine object
            # using the key values from the Data Group Packets. Packet 511 should have an assignment and packet 412
            # should have the same attributes on the Flow Station as it does on the controller
            self.config.FlowStations[1].old_data_group_packets.verify_packet_dg_511_key_values(parsed_packet=data_packet_511,
                                                                             controller=self.config.BaseStation3200[1],
                                                                             fs_slot_number= fs_point_of_control_slot_number,
                                                                             controller_slot_number= controller_point_of_control_slot_number)

            # verify that the updated point of control attributes are correct on the FlowStation
            self.config.FlowStations[1].old_data_group_packets.verify_packet_dg_412_key_values(parsed_packet=data_packet_412_fs,
                                                                             test_engine_poc=poc)

            #########################
            # verify packet 422/521 #
            #########################
            data_group_1 = '521'
            data_group_2 = '422'
            fs_mainline_slot_number = 4
            controller_mainline_slot_number = 6

            # Step 1: Add a mainline to the controller, get a reference to the new mainline, and build
            # dg422 request packet
            self.config.BaseStation3200[1].add_mainline_to_controller(controller_mainline_slot_number)
            ml = self.config.BaseStation3200[1].mainlines[controller_mainline_slot_number]

            tq_flowstation_packet_422 = self.config.FlowStations[1].old_data_group_packets.build_tq_packet_mainline_dg_422_get_single_mainline(
                mainline_address=fs_mainline_slot_number)

            # tq_controller_packet_422 = self.config.FlowStations[1].old_data_group_packets.build_tq_packet_mainline_dg_422_get_single_mainline(
            #     mainline_address=controller_mainline_slot_number)

            # Step 2: Create TQ and TS packets for DG521
            tq_packet_521 = self.config.FlowStations[1].old_data_group_packets.build_tq_dg_521_get_all_mainline_assignments()

            ts_packet_521 = self.config.FlowStations[1].old_data_group_packets.build_ts_dg_521_mainline_assignment(
                controller=self.config.BaseStation3200[1],
                flow_station_slot=fs_mainline_slot_number,
                controller_slot=controller_mainline_slot_number)

            # Step 3: Tell the threads to start recording data
            self.config.FlowStations[1].old_data_group_packets.resume_packet_logging()
            self.config.BaseStation3200[1].old_data_group_packets.resume_packet_logging()

            # Step 4: Send the TS packet to the controller using a http call
            self.config.FlowStations[1].old_data_group_packets.send_ts_packet(packet=ts_packet_521)
            time.sleep(10)

            # Step 5: Send the TQ packets to the controller and FlowStation to get packets 422 and 521 using a http call
            self.config.FlowStations[1].old_data_group_packets.send_tq_packet(packet=tq_packet_521)

            # Wait for the FlowStation/controller to send up an updated DG422 packet
            time.sleep(10)
            # Set the attributes
            ml.ad = fs_mainline_slot_number
            # Description
            ml.ds = 'test mainline'
            # Enabled
            ml.en = opcodes.false
            # Target flow / design flow value
            ml.fl = 999   # As of 2/5/2018, the 3200 test engine screws up for values > 1000.  Keep under for now
            # Pipe fill time
            ml.ft = 2
            # Stable flow pressure value
            ml.sp = 10
            # Use pressure for stable flow
            ml.up = opcodes.false
            # Use time for stable flow
            ml.ut = opcodes.true
            # Dynamic flow allocation (Flowstation only)
            ml.fp = opcodes.false
            # Standard variance shutdown enabled
            ml.fw = opcodes.false
            # Standard variance limit
            ml.fv = 0
            # Mainline variance limit
            ml.mf = 0
            # Mainline variance shutdown enabled
            ml.mw = opcodes.false
            # Priority (Flowstation only)
            ml.pr = 3
            # Water out to these mainlines
            ml.ml = 1
            # Water out to these POCs
            ml.pc = 1
            # Time delay between MVs and zone
            ml.tm = 0
            # Time delay between zones
            ml.tz = 0
            # Managed by flowstation (3200 only)
            ml.sh = opcodes.true
            # Time delay after last zone off to MV off
            ml.tl = 0
            # Number of zones to delay
            ml.zc = 0
            # Limit concurrent zones by flow
            ml.lc = opcodes.false
            # Use advanced flow
            ml.af = opcodes.false
            # High variance limit
            ml.ah = 0
            # Low variance limit
            ml.al = 0
            # High variance limit
            ml.bh = 0
            # Low variance limit
            ml.bl = 0
            # High variance limit
            ml.ch = 0
            # Low variance limit
            ml.cl = 0
            # High variance limit
            ml.dh = 0
            # Low variance limit
            ml.dl = 0
            # Zone high variance detection & shutdown
            ml.zh = opcodes.false
            # Zone low variance detection & shutdown
            ml.zl = opcodes.false
            # Delay Units (PS = Pressure, TM = Time)
            ml.un = opcodes.time

            # build TS packet 422 with the new values
            ts_packet_422 = self.config.FlowStations[1].old_data_group_packets.build_ts_packet_mainline_dg_422_single_mainline(
                mainline_obj=ml)

            # Send the 422 TS packet to the FlowStation
            self.config.FlowStations[1].old_data_group_packets.send_ts_packet(packet=ts_packet_422)
            time.sleep(10)
            self.config.FlowStations[1].save_programming_to_flow_station()
            time.sleep(5)

            # Send the 422 TQ packet to the FlowStation
            self.config.FlowStations[1].old_data_group_packets.send_tq_packet(packet=tq_flowstation_packet_422)
            time.sleep(5)

            # # Send the 422 TQ packet to the 3200
            # self.config.BaseStation3200[1].old_data_group_packets.send_tq_packet(packet=tq_controller_packet_422)

            # # Wait for the controller to process the previous three packets before stopping the logging process
            # time.sleep(20)

            # Step 6: Shut down the thread so we can verify what was recorded in the logs.
            self.config.FlowStations[1].old_data_group_packets.stop_packet_logging()
            self.config.BaseStation3200[1].old_data_group_packets.stop_packet_logging()

            # Step 7: Parse the Log File. If there is a packet 422/521, the packet structure and keys will be verified
            # in this method
            self.config.FlowStations[1].old_data_group_packets.parse_packet_log_file()
            # self.config.BaseStation3200[1].old_data_group_packets.parse_packet_log_file()

            # Step 8: Get the parsed packet data

            # Get a valid te packets from the FlowStation for DG521
            try:
                data_packet_521 = self.config.FlowStations[1].old_data_group_packets.te_packets[data_group_1]
            except KeyError:
                e_msg = "There is no data group packet for DG" + str(data_group_1)
                raise Exception(e_msg)

            # Get a valid te packets from the FlowStation for DG422
            try:
                data_packet_422_fs = self.config.FlowStations[1].old_data_group_packets.te_packets[data_group_2]
            except KeyError:
                e_msg = "There is no data group packet for DG" + str(data_group_2)
                raise Exception(e_msg)

            # Step 9: Verify that the attributes on the controller match the attributes of the test_engine object
            # using the key values from the Data Group Packets. Packet 521 should have an assignment and packet 422
            # should have the same attributes on the Flow Station as it does on the controller
            self.config.FlowStations[1].old_data_group_packets.verify_packet_521_key_values(parsed_packet=data_packet_521,
                                                                          controller=self.config.BaseStation3200[1],
                                                                          fs_slot_number= fs_mainline_slot_number,
                                                                          controller_slot_number= controller_mainline_slot_number)

            # verify that the updated mainline attributes are correct on the FlowStation
            self.config.FlowStations[1].old_data_group_packets.verify_packet_422_key_values(parsed_packet=data_packet_422_fs,
                                                                          mainline_address=fs_mainline_slot_number,
                                                                          test_engine_mainline=ml)

            # shut down the logging thread and close the file
            self.config.FlowStations[1].old_data_group_packets.close_logging_connections()
            self.config.BaseStation3200[1].old_data_group_packets.close_logging_connections()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))

            if controller is not None:
                if controller.old_data_group_packets is not None:
                    if controller.old_data_group_packets.log_file is not None:
                        if not controller.old_data_group_packets.log_file.closed:
                            controller.old_data_group_packets.log_file.flush()
                            controller.old_data_group_packets.log_file.close()
                    if controller.old_data_group_packets.read_data_thread is not None:
                        controller.old_data_group_packets.read_data_thread.join(0)
                if controller.ser2.serial_conn.isOpen():
                    controller.ser2.close_connection()

            raise Exception(e_msg)