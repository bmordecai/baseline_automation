__author__ = 'bens'

import common.objects.basemanager.dv_tab as dt


class LoadDevicesSetDescriptionsUseCase(object):

    def __init__(self, test_config):
        self.config = test_config
        # Resets objects to a known state, creates serial connections, creates all objects.
        self.config.initialize_for_test()
        self.config.objects["CN"].initialize_for_test()
        # self.config.objects["CN"].set_sim_mode_to_off()
        self.config.objects["CN"].verify_ip_address_state(url=self.config.user_conf.url)
        self.config.objects["CN"].load_all_dv_to_cn(d1_list=self.config.d1,
                                                    d2_list=self.config.d2,
                                                    d4_list=self.config.d4,
                                                    dd_list=self.config.dd,
                                                    ms_list=self.config.ms,
                                                    fm_list=self.config.fm,
                                                    ts_list=self.config.ts,
                                                    sw_list=self.config.sw)

        self.config.objects["CN"].do_search_for_dv(dv_type="ZN")

        self.config.objects["CN"].set_address_and_default_values_for_zn(zn_dict=self.config.objects["ZN"],
                                                                        zn_ad_range=self.config.zn_ad_range)
        self.config.objects["TS"][1].set_lat_on_cn(12)
        self.config.objects["TS"][1].set_lng_on_cn()

    def run_load_devices_set_descriptions_use_case(self):
        """

        :return:
        """
        self.config.browser.open_and_login()
        self.config.browser.select_site(site_name=self.config.user_conf.site_name)
        self.config.browser.select_a_controller(mac_address=self.config.objects['CN'].mac)

        # Zone
        self.config.browser.select_a_menu_tab(menu_name_in="Devices", sub_menu_in="ZN")
        dt.assign_bicoder(dv_ty="ZN", browser_instance=self.config.browser)
        self.config.objects['ZN'][1].set_ds_on_bm()
        dt.save_bicoder_assignment(dv_type="ZN", browser_instance=self.config.browser)

        # Moisture Sensors
        self.config.browser.select_a_menu_tab(menu_name_in="Devices", sub_menu_in="MS")
        dt.assign_bicoder(dv_ty="MS", browser_instance=self.config.browser)
        self.config.objects['MS'][1].set_ds_on_bm()
        dt.save_bicoder_assignment(dv_type="MS", browser_instance=self.config.browser)

        print "All done."
        self.config.browser.teardown_driver()


