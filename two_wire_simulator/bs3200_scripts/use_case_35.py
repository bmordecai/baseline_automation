import sys
from time import sleep
from datetime import timedelta

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

__author__ = 'Tige'


class ControllerUseCase35(object):
    """
    Test name:
        - CN UseCase35 Large Configuration Programming
    purpose:
        - set up a full configuration on the controller
            - with 240 address
            - 200 Zones
            - 99 Programs
            - 7 Moisture sensor
            - 7 Temperature sensor
            - 7 Flow sensor
            - 7 Master Valves
            - 7 POCS
            - 7 Mainlines
        - reboot the controller
            - verify programming
    Coverage Area: \n
        1. Load devices \n
        2. Search for devices \n
        3. Set descriptions and locations \n
        4. Can assign devices to addresses \n
        5. Can assign zones to Programs \n
        6. Can set up Programs with; start times, watering days, water windows, zone concurrency, and assign Programs to
           water sources \n
        7. Can set up moisture sensors and give them values \n
        8. Can set up temperature sensors and give them values \n
        9. Can set up event switches and give them values \n
        10. Can set up master valves \n
        11. Can set up flow meters and give them values \n
        12. Can set up water sources \n
        13. Can set up booster pumps \n
        14. Can set up water sources \n
        15. Can test all devices \n
            put in for to verify bug ZZ-435 \n
        16. added a program 4 that has one zone and disabled the program this test for a bug that was introduced in \n
            12.20 id a program was disabled and you rebooted the controller all programming would be lost 4/29/2015 \n

        7/29/2014 reboot command before the get command to verify that programing is saved correctly \n
        7/31/2014 added a test temperature sensor method
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """

        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file,
                                    serial_port_time_out=6000)

        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)
                    self.config.BaseStation3200[1].set_serial_port_timeout(timeout=6000)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        ############################
        Set up controller concurrency
        ############################

        - Set controller concurrent zones.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=15)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        Set up programs
        ############################
        
        Add program -----> to controller
            - set up program  Attributes \n
                - set enabled state  \n
                - set water window\n
                - set start times \n
                - set priority \n
                - set seasonal adjust \n
                - set watering intervals \n
                
        If not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
            
        If using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
            
        give each program a start time of 8:00 A.M./9:00 A.M./10:00 A.M./11:00 A.M. \n
        set up program watering days \n
        set up water windows for Programs \n
        set Programs to have a concurrent zone of 4 \n
        disable program 9 \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # this is set in the PG3200 object
            program_start_time_8am_9am_10am_11am = [480, 540, 600, 660]
            program_start_time_2pm = [840]  # 2:00pm start time
            program_watering_days_mwf = [0, 1, 0, 1, 0, 1, 1]  # runs monday/wednesday/friday
            program_water_windows = ['111111111111111111111111']
            monthly_water_windows = ['011111100000111111111110', '011111100001111111111111', '011111100001111111111110',
                                     '011111100001111111111110', '011111100001111111111110', '011111100001111111111110',
                                     '011111100001111111111111']

            #
            # Add and configure Programs 1 - 8
            #
            for program_ad in range(1, 4):

                # UPDATE - Ben - 11/27/17
                # ~~> Commented this code out (mainline for loop) because v16 doesn't support mainlines on programs.
                self.config.BaseStation3200[1].add_program_to_controller(_program_address=program_ad)
                self.config.BaseStation3200[1].programs[program_ad].set_enabled()
                self.config.BaseStation3200[1].programs[program_ad].set_max_concurrent_zones(_number_of_zones=4)
                self.config.BaseStation3200[1].programs[program_ad].set_water_window(_ww=program_water_windows)
                self.config.BaseStation3200[1].programs[program_ad].set_start_times(_st_list=program_start_time_8am_9am_10am_11am)
                self.config.BaseStation3200[1].programs[program_ad].set_watering_intervals_to_selected_days_of_the_week(
                    _sun=False, _mon=True, _tues=False, _wed=True, _thurs=False, _fri=True, _sat=False
                )

            # Add and configure Program 9
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=4)
            self.config.BaseStation3200[1].programs[4].set_disabled()
            self.config.BaseStation3200[1].programs[4].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[4].set_water_window(_ww=program_water_windows)
            self.config.BaseStation3200[1].programs[4].set_start_times(_st_list=program_start_time_8am_9am_10am_11am)
            self.config.BaseStation3200[1].programs[4].set_watering_intervals_to_odd_days()

            # Add and configure Program 99

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=99)
            self.config.BaseStation3200[1].programs[99].set_disabled()
            self.config.BaseStation3200[1].programs[99].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation3200[1].programs[99].set_water_window(_ww=program_water_windows)
            self.config.BaseStation3200[1].programs[99].set_start_times(_st_list=program_start_time_8am_9am_10am_11am)
            self.config.BaseStation3200[1].programs[99].set_watering_intervals_to_selected_days_of_the_week(
                _sun=False, _mon=True, _tues=False, _wed=True, _thurs=False, _fri=True, _sat=False
            )
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        Set up zones on programs
        ############################

        Add zone -----> to program
            - set up zone program  Attributes \n
                - set zone type |Timed, Primary, Linked | \n
                - set runtime\n
                - set cycle time \n
                - set soak time \n
            - if zone type |Linked| \n
                - set tracking ratio \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # ----------------------------------------------#
            # Add and configure zone programs for program 1 #
            # ----------------------------------------------#

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=16)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=8)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].add_moisture_sensor_to_primary_zone(_moisture_sensor_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_water_strategy_to_lower_limit()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_lower_limit_threshold(_percent=24.0)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_one_time_calibration_cycle()

            # -------------------------------------------------------------#
            # Add linked zone programs to program 1 primary zone program 1 #
            # -------------------------------------------------------------#

            for zone_ad in range(2, 150):
                self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=zone_ad)
                self.config.BaseStation3200[1].programs[1].zone_programs[zone_ad].set_as_linked_zone(_primary_zone=1)
            # Add zone programs to program 2 #
            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=49)
            self.config.BaseStation3200[1].programs[2].zone_programs[49].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[2].zone_programs[49].set_water_strategy_to_timed()
            self.config.BaseStation3200[1].programs[2].zone_programs[49].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[2].zone_programs[49].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[2].zone_programs[49].set_soak_time(_minutes=60)

            # Add zone programs to program 3 #
            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=49)
            self.config.BaseStation3200[1].programs[3].zone_programs[49].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[3].zone_programs[49].set_water_strategy_to_timed()
            self.config.BaseStation3200[1].programs[3].zone_programs[49].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[3].zone_programs[49].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[3].zone_programs[49].set_soak_time(_minutes=60)

            self.config.BaseStation3200[1].programs[3].add_zone_to_program(_zone_address=50)
            self.config.BaseStation3200[1].programs[3].zone_programs[50].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[3].zone_programs[50].set_water_strategy_to_timed()
            self.config.BaseStation3200[1].programs[3].zone_programs[50].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[3].zone_programs[50].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[3].zone_programs[50].set_soak_time(_minutes=60)
            # Add zone programs to program 4 #
            self.config.BaseStation3200[1].programs[4].add_zone_to_program(_zone_address=50)
            self.config.BaseStation3200[1].programs[4].zone_programs[50].set_as_timed_zone()
            self.config.BaseStation3200[1].programs[4].zone_programs[50].set_water_strategy_to_timed()
            self.config.BaseStation3200[1].programs[4].zone_programs[50].set_run_time(_minutes=20)
            self.config.BaseStation3200[1].programs[4].zone_programs[50].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[4].zone_programs[50].set_soak_time(_minutes=60)
            # Add zone programs to program 99 #
            self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=200)
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_run_time(_minutes=33)
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_cycle_time(_minutes=3)
            self.config.BaseStation3200[1].programs[99].zone_programs[200].set_soak_time(_minutes=13)
            # Add linked zone programs to program 99 primary zone program 200 #
            for zone_ad in range(153, 200):
                self.config.BaseStation3200[1].programs[99].add_zone_to_program(_zone_address=zone_ad)
                self.config.BaseStation3200[1].programs[99].zone_programs[zone_ad].set_as_linked_zone(_primary_zone=200,
                                                                                                      _tracking_ratio=50)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        Set up initial device values
        ############################

        Set up Moisture Sensors \n
            - MS 1: SB05308 \n
                - Moisture value: 10.0 \n
                - Two wire drop: 1.6 \n
            - MS 2: SB07258 \n
                - Moisture value: 26.0 \n
                - Two wire drop: 1.8 \n

        Set up Temperature Sensors \n
            - TS 1: TAT0001 \n
                - Temperature value: 26.0 \n
                - Two wire drop: 1.8 \n

        Set up Event Switches \n
            - SW 1: TPD0001 \n
                - Value: closed \n
                - Two wire drop: 1.8 \n

        Set up Master Valves \n
            - MV 1: TSD0001 \n
                - Booster Pump: true \n
                - Normally Open: closed \n
            - MV 2 - 7: TMV0003 \n
                - Normally Open: closed \n
            - MV 8: TMV0004 \n
                - Normally Open: open \n

        Set up Flow Meters \n
            - FM 1: TWF0003 \n
                - Enabled: True \n
                - K-value: 3.10 \n
                - Flow GPM: 25 \n
            - FM 2: TWF0004 \n
                - Enabled: True \n
                - K-value: 5.01 \n
                - Flow GPM: 50 \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for ms_sensor in sorted(self.config.BaseStation3200[1].moisture_sensors.keys()):
                self.config.BaseStation3200[1].moisture_sensors[ms_sensor].bicoder.set_moisture_percent(_percent=10.0)
                self.config.BaseStation3200[1].moisture_sensors[ms_sensor].bicoder.set_two_wire_drop_value(_value=1.6)

            for temp_sensor in sorted(self.config.BaseStation3200[1].temperature_sensors.keys()):
                self.config.BaseStation3200[1].temperature_sensors[temp_sensor]\
                    .bicoder.set_temperature_reading(_degrees=26.0)
                self.config.BaseStation3200[1].temperature_sensors[temp_sensor]\
                    .bicoder.set_two_wire_drop_value(_value=1.8)

            self.config.BaseStation3200[1].event_switches[1].set_disabled()
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_contact_closed()
            self.config.BaseStation3200[1].event_switches[1].bicoder.set_two_wire_drop_value(_value=1.8)

            for event_switch in range(2, 6):
                self.config.BaseStation3200[1].event_switches[event_switch].set_enabled()
                self.config.BaseStation3200[1].event_switches[event_switch].bicoder.set_contact_closed()
                self.config.BaseStation3200[1].event_switches[event_switch].bicoder.set_two_wire_drop_value(_value=1.8)

            # --------------#
            # Master Valves #
            # --------------#
            
            self.config.BaseStation3200[1].master_valves[1].set_booster_enabled()
            self.config.BaseStation3200[1].master_valves[1].set_normally_open_state(_normally_open=opcodes.false)

            for master_valve in range(2, 6):
                self.config.BaseStation3200[1].master_valves[master_valve].\
                    set_normally_open_state(_normally_open=opcodes.false)

            self.config.BaseStation3200[1].master_valves[8].set_disabled()
            self.config.BaseStation3200[1].master_valves[8].set_booster_enabled()
            self.config.BaseStation3200[1].master_valves[8].set_normally_open_state(_normally_open=opcodes.false)

            for flow_meter in range(1, 5):
                self.config.BaseStation3200[1].flow_meters[flow_meter].set_enabled()
                self.config.BaseStation3200[1].flow_meters[flow_meter].bicoder.set_k_value(_value=3.10)
                self.config.BaseStation3200[1].flow_meters[flow_meter].bicoder.set_flow_rate(_gallons_per_minute=25)

            self.config.BaseStation3200[1].flow_meters[5].set_disabled()
            self.config.BaseStation3200[1].flow_meters[5].bicoder.set_k_value(_value=5.01)
            self.config.BaseStation3200[1].flow_meters[5].bicoder.set_flow_rate(_gallons_per_minute=50)

            for flow_meter in range(7, 9):
                self.config.BaseStation3200[1].flow_meters[flow_meter].set_enabled()
                self.config.BaseStation3200[1].flow_meters[flow_meter].bicoder.set_k_value(_value=5.01)
                self.config.BaseStation3200[1].flow_meters[flow_meter].bicoder.set_flow_rate(_gallons_per_minute=50)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        Set up start/stop/pause conditions on programs
        ############################
        
        Create Start/stop/pause conditions \n
        Start PG 99 when MS1 gets above 38 \n
        Pause PG 99 when SW1 gets opened \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            self.config.BaseStation3200[1].programs[99].add_moisture_start_condition(_moisture_sensor_address=1)
            self.config.BaseStation3200[1].programs[99].moisture_start_conditions[1].set_moisture_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[99].moisture_start_conditions[1].set_moisture_threshold(_percent=38)

            self.config.BaseStation3200[1].programs[99].add_switch_pause_condition(_event_switch_address=1)
            self.config.BaseStation3200[1].programs[99].event_switch_pause_conditions[1].set_switch_mode_to_open()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ############################
        Set up water sources
        ############################
        
        Add water sources -----> to controlLer
            - set up water source  Attributes \n
                - set enable state \n
                - set priority \n
                - set water budget \n
                - set water rationing state \n
                - set high pressure limit with shut down state \n
                - set low pressure limit with shut down state \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for i in range(1, 6):
                self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=i)
                self.config.BaseStation3200[1].water_sources[i].set_enabled()
                self.config.BaseStation3200[1].water_sources[i].set_priority(_priority_for_water_source=2)
                self.config.BaseStation3200[1].water_sources[i].set_monthly_watering_budget(_budget=100000, 
                                                                                            _with_shutdown_enabled=True)
                self.config.BaseStation3200[1].water_sources[i].set_water_rationing_to_enabled()

            for i in range(7, 9):
                self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=i)
                self.config.BaseStation3200[1].water_sources[i].set_enabled()
                self.config.BaseStation3200[1].water_sources[i].set_priority(_priority_for_water_source=3)
                self.config.BaseStation3200[1].water_sources[i]\
                    .set_monthly_watering_budget(_budget=100000,
                                                 _with_shutdown_enabled=False)
                self.config.BaseStation3200[1].water_sources[i].set_water_rationing_to_disabled()
        
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=8)
            self.config.BaseStation3200[1].water_sources[8].set_disabled()
            self.config.BaseStation3200[1].water_sources[8].set_priority(_priority_for_water_source=1)
            self.config.BaseStation3200[1].water_sources[8].set_monthly_watering_budget(_budget=100000,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].water_sources[8].set_water_rationing_to_enabled()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        
    #################################
    def step_7(self):
        """
        ############################
        Setup points of control
        ############################

        Add Points of Control -----> to controlLer
            - set up points of control Attributes \n
                - set enable state \n
                - set target flow \n
                - set high flow limit with shut down state \n
                - set unscheduled flow limit with shut down state \n
                - set high pressure limit with shut down state \n
                - set low pressure limit with shut down state \n
        Add flow meters ---> to point of control \n
        Add pump ---> to point of control \n
        Add master valve  ---> to point of control \n
        Add pressure sensor  ---> to point of control \n
        Add Points of Control -----> To Water Source
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for i in range(1, 6):

                self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=i)
                self.config.BaseStation3200[1].points_of_control[i].set_enabled()
                self.config.BaseStation3200[1].points_of_control[i].set_target_flow(_gpm=500)
                self.config.BaseStation3200[1].points_of_control[i].set_high_flow_limit(_limit=550,
                                                                                        with_shutdown_enabled=True)
                self.config.BaseStation3200[1].points_of_control[i]\
                    .set_unscheduled_flow_limit(_gallons=10,
                                                with_shutdown_enabled=True)
                # Add MV i to POC i
                self.config.BaseStation3200[1].points_of_control[i].add_master_valve_to_point_of_control(
                    _master_valve_address=i)
                # Add FM i to POC i
                self.config.BaseStation3200[1].points_of_control[i].add_flow_meter_to_point_of_control(
                    _flow_meter_address=i)
                # Add POC i to Water Source i
                self.config.BaseStation3200[1].water_sources[i].add_point_of_control_to_water_source(
                    _point_of_control_address=i)

            for i in range(7, 9):

                self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=i)
                self.config.BaseStation3200[1].points_of_control[i].set_enabled()
                self.config.BaseStation3200[1].points_of_control[i].set_target_flow(_gpm=500)
                self.config.BaseStation3200[1].points_of_control[i].set_high_flow_limit(_limit=550,
                                                                                        with_shutdown_enabled=False)
                self.config.BaseStation3200[1].points_of_control[i]\
                    .set_unscheduled_flow_limit(_gallons=10,
                                                with_shutdown_enabled=False)
                # Add PM i to POC i
                self.config.BaseStation3200[1].points_of_control[i].add_pump_to_point_of_control(
                    _pump_address=i)
                # Add MV i to POC i
                self.config.BaseStation3200[1].points_of_control[i].add_master_valve_to_point_of_control(
                    _master_valve_address=i)
                # Add FM i to POC i
                self.config.BaseStation3200[1].points_of_control[i].add_flow_meter_to_point_of_control(
                    _flow_meter_address=i)
                # Add PS i to POC i
                self.config.BaseStation3200[1].points_of_control[i].add_pressure_sensor_to_point_of_control(
                    _pressure_sensor_address=i)
                # Add POC i to Water Source i
                self.config.BaseStation3200[1].water_sources[i].add_point_of_control_to_water_source(
                    _point_of_control_address=i)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ############################
        Set up mainlines
        ############################

        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
        Add Mainline ---> to point of control
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for i in range(1, 6):

                self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=i)
                self.config.BaseStation3200[1].mainlines[i].set_limit_zones_by_flow_to_true()
                self.config.BaseStation3200[1].mainlines[i].set_pipe_stabilization_time(_minutes=4)
                self.config.BaseStation3200[1].mainlines[i].set_target_flow(_gpm=500)
                self.config.BaseStation3200[1].mainlines[i].set_high_flow_variance_tier_one(_percent=5,
                                                                                            _with_shutdown_enabled=True)
                self.config.BaseStation3200[1].mainlines[i].set_low_flow_variance_tier_one(_percent=20,
                                                                                           _with_shutdown_enabled=True)
                # Add ML i to POC i
                self.config.BaseStation3200[1].points_of_control[i].add_mainline_to_point_of_control(_mainline_address=i)

            for i in range(7, 9):

                self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=i)
                self.config.BaseStation3200[1].mainlines[i].set_limit_zones_by_flow_to_true()
                self.config.BaseStation3200[1].mainlines[i].set_pipe_stabilization_time(_minutes=1)
                self.config.BaseStation3200[1].mainlines[i].set_target_flow(_gpm=50)
                self.config.BaseStation3200[1].mainlines[i].set_high_flow_variance_tier_one(_percent=20,
                                                                                            _with_shutdown_enabled=False)
                self.config.BaseStation3200[1].mainlines[i].set_low_flow_variance_tier_one(_percent=5,
                                                                                           _with_shutdown_enabled=False)
                # Add ML i to POC i
                self.config.BaseStation3200[1].points_of_control[i].add_mainline_to_point_of_control(_mainline_address=i)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        ############################
        Add zones to mainlines
        ############################

        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # ---------------------------------#
            # Add Zones 1 - 24 to Mainline 1   #
            # ---------------------------------#
            for zone_ad in range(1, 25):
                self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=zone_ad)

            # ---------------------------------#
            # Add Zones 25 - 49 to Mainline 2  #
            # ---------------------------------#
            for zone_ad in range(25, 50):
                self.config.BaseStation3200[1].mainlines[2].add_zone_to_mainline(_zone_address=zone_ad)
            # ---------------------------------#
            # Add Zones 50 - 74 to Mainline 3  #
            # ---------------------------------#
            for zone_ad in range(50, 75):
                self.config.BaseStation3200[1].mainlines[3].add_zone_to_mainline(_zone_address=zone_ad)

            # ---------------------------------#
            # Add Zones 75 - 124 to Mainline 4  #
            # ---------------------------------#
            for zone_ad in range(75, 125):
                self.config.BaseStation3200[1].mainlines[4].add_zone_to_mainline(_zone_address=zone_ad)

            # --------------------------------#
            # Add Zones 125-149 to Mainline 6 #
            # --------------------------------#
            for zone_ad in range(125, 149):
                self.config.BaseStation3200[1].mainlines[5].add_zone_to_mainline(_zone_address=zone_ad)

            # --------------------------------#
            # Add Zones 150-174 to Mainline 7 #
            # --------------------------------#
            for zone_ad in range(150, 175):
                self.config.BaseStation3200[1].mainlines[7].add_zone_to_mainline(_zone_address=zone_ad)

            # --------------------------------#
            # Add Zones 175-200 to Mainline 8 #
            # --------------------------------#
            for zone_ad in range(175, 201):
                self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=zone_ad)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        ###############################
        Verify the entire configuration before reboot
        ###############################

        - Increment clock to save configuration
        - Get information for each object from controller
        - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        ###############################
        Reboot controller
        ###############################

        - Reboot the controller \n
        - Set sim mode to off, wait 10 seconds, then turn it back on \n
        - Stop the clock \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_reboot_controller()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        ############################
        Update two-wire attributes of devices
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            for zone in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone].bicoder. \
                    self_test_and_update_object_attributes()
            for moisture_sensors in sorted(self.config.BaseStation3200[1].moisture_sensors.keys()):
                self.config.BaseStation3200[1].moisture_sensors[moisture_sensors].bicoder. \
                    self_test_and_update_object_attributes()
            for master_valves in sorted(self.config.BaseStation3200[1].master_valves.keys()):
                self.config.BaseStation3200[1].master_valves[master_valves].bicoder. \
                    self_test_and_update_object_attributes()
            for flow_meters in sorted(self.config.BaseStation3200[1].flow_meters.keys()):
                self.config.BaseStation3200[1].flow_meters[flow_meters].bicoder. \
                    self_test_and_update_object_attributes()
            for event_switches in sorted(self.config.BaseStation3200[1].event_switches.keys()):
                self.config.BaseStation3200[1].event_switches[event_switches].bicoder. \
                    self_test_and_update_object_attributes()
            for temperature_sensors in sorted(self.config.BaseStation3200[1].temperature_sensors.keys()):
                self.config.BaseStation3200[1].temperature_sensors[temperature_sensors].bicoder. \
                    self_test_and_update_object_attributes()
            for pump in sorted(self.config.BaseStation3200[1].pumps.keys()):
                self.config.BaseStation3200[1].pumps[pump].bicoder. \
                    self_test_and_update_object_attributes()
            for pressure_sensor in sorted(self.config.BaseStation3200[1].pressure_sensors.keys()):
                self.config.BaseStation3200[1].pressure_sensors[pressure_sensor].bicoder. \
                    self_test_and_update_object_attributes()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        ############################
        Verify the entire configuration after reboot
        ############################

        - Verify the entire configuration again \n
        - Verify that BaseManager is still connected \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
            self.config.BaseStation3200[1].basemanager_connection[1].wait_for_bm_connection()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
