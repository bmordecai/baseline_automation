import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

__author__ = "Eldin"


class ControllerUseCase30(object):
    """
    Test name: \n
        Temperature Decoders \n
    Purpose: \n
        To set the start/stop/pause condition on Programs using temperature sensor and verifying that the program and
        its zones have the proper status when conditions are triggered. \n
    Coverage area: \n
        1.	Able to search and find Temperature decoders \n
        2.	Verify both a Temp sensor and a Moisture sensor acting as a temp sensor work correctly \n
        3.	Set a Temperature decoders to pause all Programs on the controller using Lower Limit  \n
        4.	Set a Temperature decoders to stop all program on the controller using Upper Limit \n
        5.	Set a Temperature decoder to pause a single program using Lower Limit \n
        6.	Set a Temperature decoder to pause multiple Programs using Upper Limit\n
        7.	Set a Temperature decoder to start a single program using Lower Limit \n
        8.	Set a Temperature decoder to start multiple Programs using Lower Limit \n
        9.	Set a Temperature decoder to stop a single program using Upper Limit \n
        10.	Set a Temperature decoder to stop multiple Programs using Upper Limit \n
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtyp
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        ############################
        - Set up the controller settings \n
            - Set max concurrency on controller to 4 we want 4 zone max to run at a time \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=4)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set concurrent zones \n
            - set start times \n
            - set priority \n
            - set watering intervals \n
        """
        # These are set in the PG3200 object
        program_start_time_8am = [480]

        program_week_day_every_day = [1, 1, 1, 1, 1, 1, 1]

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=2)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_start_time_8am)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(_sun=True,
                                                                                                           _mon=True,
                                                                                                           _tues=True,
                                                                                                           _wed=True,
                                                                                                           _thurs=True,
                                                                                                           _fri=True,
                                                                                                           _sat=True)

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation3200[1].programs[2].set_enabled()
            self.config.BaseStation3200[1].programs[2].set_max_concurrent_zones(_number_of_zones=2)
            self.config.BaseStation3200[1].programs[2].set_start_times(_st_list=program_start_time_8am)
            self.config.BaseStation3200[1].programs[2].set_watering_intervals_to_selected_days_of_the_week(_sun=True,
                                                                                                           _mon=True,
                                                                                                           _tues=True,
                                                                                                           _wed=True,
                                                                                                           _thurs=True,
                                                                                                           _fri=True,
                                                                                                           _sat=True)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=45)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=18)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=4)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[1].zone_programs[3].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[1].zone_programs[4].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=5)
            self.config.BaseStation3200[1].programs[2].zone_programs[5].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[2].zone_programs[5].set_run_time(_minutes=45)
            self.config.BaseStation3200[1].programs[2].zone_programs[5].set_cycle_time(_minutes=18)
            self.config.BaseStation3200[1].programs[2].zone_programs[5].set_soak_time(_minutes=4)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=6)
            self.config.BaseStation3200[1].programs[2].zone_programs[6].set_as_linked_zone(_primary_zone=5)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=7)
            self.config.BaseStation3200[1].programs[2].zone_programs[7].set_as_linked_zone(_primary_zone=5)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=8)
            self.config.BaseStation3200[1].programs[2].zone_programs[8].set_as_linked_zone(_primary_zone=5)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ###############################
        Setup Program Conditions
        ###############################
        - Set temp sensor 1 TAT0001 to pause all programming with below a limit \n
        - Set temp sensor SB05308 to stop program 2 when below limit \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # This will have a wait time
            self.config.BaseStation3200[1].add_temperature_pause_condition(_temperature_sensor_address=1)
            self.config.BaseStation3200[1].temperature_pause_conditions[1].set_temperature_mode_to_lower_limit()
            self.config.BaseStation3200[1].temperature_pause_conditions[1].set_temperature_threshold(_degrees=32.0)
            self.config.BaseStation3200[1].temperature_pause_conditions[1].set_temperature_pause_time(_minutes=2)

            #   Added a second temperature sensor to the test, (serial: TAT0002, address: 2) because v16 3200 no longer
            #   supports using moisture sensors as temp sensors.
            self.config.BaseStation3200[1].programs[2].add_temperature_stop_condition(_temperature_sensor_address=2)
            self.config.BaseStation3200[1].programs[2].temperature_stop_conditions[2]\
                .set_temperature_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[2].temperature_stop_conditions[2]\
                .set_temperature_threshold(_degrees=60)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ###############################
        verify the entire configuration
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ###############################
        verify initial programming statuses 
        ###############################
        
        - Set the temperature reading to a value that doesn't trigger any conditions on Programs. \n
        - Set the date and time on the controller to be 04/08/2014 07:00:00. \n
        - The time is now 07:00:00. \n
        - Get the data of each zone and program on the controller. \n
        - Verify Statuses: \n
            - Program 1: Done Watering because the time hasn't been incremented to trigger a start condition. \n
                - Zone 1: Done Watering because the time hasn't been incremented to trigger a start condition. \n
                - Zone 2: Done Watering because the time hasn't been incremented to trigger a start condition. \n
                - Zone 3: Done Watering because the time hasn't been incremented to trigger a start condition. \n
                - Zone 4: Done Watering because the time hasn't been incremented to trigger a start condition. \n
            - Program 2: Done Watering because the time hasn't been incremented to trigger a start condition. \n
                - Zone 5: Done Watering because the time hasn't been incremented to trigger a start condition. \n
                - Zone 6: Done Watering because the time hasn't been incremented to trigger a start condition. \n
                - Zone 7: Done Watering because the time hasn't been incremented to trigger a start condition. \n
                - Zone 8: Done Watering because the time hasn't been incremented to trigger a start condition. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set the temperature sensors values
            self.config.BaseStation3200[1].temperature_sensors[1].bicoder.set_temperature_reading(_degrees=33.0)
            self.config.BaseStation3200[1].temperature_sensors[2].bicoder.set_temperature_reading(_degrees=65.0)

            self.config.BaseStation3200[1].temperature_sensors[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].temperature_sensors[2].bicoder.self_test_and_update_object_attributes()

            # Set the date and time
            self.config.BaseStation3200[1].set_date_and_time(_date="04/08/2014", _time="07:00:00")

            # Verify all Programs
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()

            # Verify all Zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ###############################
        start programs and verify status 
        ###############################
        - Manually start both Programs that both Programs will be running. \n
        - Increment the clock by 1 minute. \n
        - The time is now 07:01:00. \n
        - Get the data of each zone and program on the controller. \n
        - Verify Statuses: \n
            - Program 1: Running because the program was manually started. \n
                - Zone 1: Watering because the program is running. Program has max concurrent of 2, and cn has 4. \n
                - Zone 2: Watering because the program is running. Program has max concurrent of 2, and cn has 4. \n
                - Zone 3: Waiting to water because max concurrent zones on this program is 2, and 4 on controller. \n
                - Zone 4: Waiting to water because max concurrent zones on this program is 2, and 4 on controller. \n
            - Program 2: Running because the program was manually started. \n
                - Zone 5: Watering because the program is running. Program has max concurrent of 2, and cn has 4. \n
                - Zone 6: Watering because the program is running. Program has max concurrent of 2, and cn has 4. \n
                - Zone 7: Waiting to water because max concurrent zones on this program is 2, and 4 on controller. \n
                - Zone 8: Waiting to water because max concurrent zones on this program is 2, and 4 on controller. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Start both Programs manually
            self.config.BaseStation3200[1].programs[1].set_program_to_start_like_controller_manual_start()
            self.config.BaseStation3200[1].programs[2].set_program_to_start_like_controller_manual_start()

            # Increment the time by one minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all Programs and their zones
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        - Lower the value of  temp sensor TAT0001 to pause all programing \n
        - Lower the value of  moisture sensor SB05308 to stop program 2 \n
        - Because the TAT0001 has paused all programing the change to SB05305 shouldn't impact zone status until pause
          condition goes away \n
        - Increment the clock by 1 minute. \n
        - The time is now 07:02:00. \n
        - Get the data of each zone and program on the controller. \n
        - Verify Statuses: \n
            - Program 1: Paused because Temp Sensor 1 went below the lower threshold, pausing all Programs. \n
                - Zone 1: Paused because the program it is on had its pause condition triggered. \n
                - Zone 2: Paused because the program it is on had its pause condition triggered. \n
                - Zone 3: Paused because the program it is on had its pause condition triggered. \n
                - Zone 4: Paused because the program it is on had its pause condition triggered. \n
            - Program 2: Paused because Temp Sensor 1 went below the lower threshold, pausing all Programs. \n
                - Zone 5: Paused because the program it is on had its pause condition triggered. \n
                - Zone 6: Paused because the program it is on had its pause condition triggered. \n
                - Zone 7: Paused because the program it is on had its pause condition triggered. \n
                - Zone 8: Paused because the program it is on had its pause condition triggered. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set the temperature sensors values
            self.config.BaseStation3200[1].temperature_sensors[1].bicoder.set_temperature_reading(_degrees=31.0)
            self.config.BaseStation3200[1].temperature_sensors[2].bicoder.set_temperature_reading(_degrees=58.0)

            self.config.BaseStation3200[1].temperature_sensors[1].bicoder.self_test_and_update_object_attributes()
            self.config.BaseStation3200[1].temperature_sensors[2].bicoder.self_test_and_update_object_attributes()

            # Increment the time by one minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            # self.config.BaseStation3200[1].do_increment_clock(minutes=2)

            # Verify all Programs and their zones
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_paused()

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        - Raise value of TAT0000 to un-pause all Programs. \n
        - Because the TAT0001 has paused all programing the change to SB05305 shouldn't impact zone status until pause
          condition goes away \n
        - Increment the clock by 3 minutes. \n
        - The time is now 07:05:00. \n
        - Get the data of each zone and program on the controller. \n
        - Verify Statuses: \n
            - Program 1: Running because the temperature sensor's value was changed to un-pause all Programs. \n
                - Zone 1: Watering because the program is running. Program has max concurrent of 2, and cn has 4. \n
                - Zone 2: Watering because the program is running. Program has max concurrent of 2, and cn has 4. \n
                - Zone 3: Waiting to water because max concurrent zones on this program is 2, and 4 on controller. \n
                - Zone 4: Waiting to water because max concurrent zones on this program is 2, and 4 on controller. \n
            - Program 2: Done because it was un-paused, but another stop condition triggered causing this to not run. \n
                - Zone 5: Done because the program had a stop condition trigger. \n
                - Zone 6: Done because the program had a stop condition trigger. \n
                - Zone 7: Done because the program had a stop condition trigger. \n
                - Zone 8: Done because the program had a stop condition trigger. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set the temperature sensors values
            self.config.BaseStation3200[1].temperature_sensors[1].bicoder.set_temperature_reading(_degrees=41.0)
            self.config.BaseStation3200[1].temperature_sensors[1].bicoder.self_test_and_update_object_attributes()

            # Increment the time by three minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=3)

            # Verify all Programs and their zones
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[2].temperature_stop_conditions[2]\
                .messages.verify_stop_condition_with_temperature_sensor_message()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        - Stop both Programs manually so we can 'reset' the test. \n
        - Set two temp sensors to start at stop two simultaneous Programs at one time using above and below limits \n
        - Set temp sensor TAT0001 to start program 1 when below limit \n
        - Set temp sensor SB05308 to start program 2 when below limit \n
        - Set temp sensor TAT001 to stop program 1 when above limit \n
        - Set temp sensor SB05308 to stop program 2 when Above limit \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Stop both Programs manually
            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()

            self.config.BaseStation3200[1].temperature_pause_conditions[1].set_temperature_mode_off()

            self.config.BaseStation3200[1].programs[2].temperature_stop_conditions[2].set_temperature_mode_off()

            self.config.BaseStation3200[1].programs[1].add_temperature_start_condition(_temperature_sensor_address=1)
            self.config.BaseStation3200[1].programs[1].temperature_start_conditions[1].\
                set_temperature_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[1].temperature_start_conditions[1].\
                set_temperature_threshold(_degrees=32.0)

            self.config.BaseStation3200[1].programs[2].add_temperature_start_condition(_temperature_sensor_address=2)
            self.config.BaseStation3200[1].programs[2].temperature_start_conditions[2].\
                set_temperature_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[2].temperature_start_conditions[2].\
                set_temperature_threshold(_degrees=60.0)

            self.config.BaseStation3200[1].programs[1].add_temperature_stop_condition(_temperature_sensor_address=1)
            self.config.BaseStation3200[1].programs[1].temperature_stop_conditions[1].\
                set_temperature_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[1].temperature_stop_conditions[1].\
                set_temperature_threshold(_degrees=50.0)

            #   Over writing Program 2's temp stop condition 2 with new upper limit mode and threshold, since we are 
            #   addressing stop contitions by their device's address.
            self.config.BaseStation3200[1].programs[2].temperature_stop_conditions[2].\
                set_temperature_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[2].temperature_stop_conditions[2].\
                set_temperature_threshold(_degrees=70.0)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        - Set values of temp sensor value of TAT0001 to to trigger program 1 to start. \n
        - Set values of temp sensor value of SB05308 to to trigger program 2 to start. \n
        - Increment the clock by 3 minutes. \n
        - The time is now 07:08:00. \n
        - Get the data of each zone and program on the controller. \n
        - Verify Statuses: \n
            - Program 1: Running because the program's start condition was triggered by temp sensor 1. \n
                - Zone 1: Watering because the program is running. Program has max concurrent of 2, and cn has 4. \n
                - Zone 2: Watering because the program is running. Program has max concurrent of 2, and cn has 4. \n
                - Zone 3: Waiting to water because max concurrent zones on this program is 2, and 4 on controller. \n
                - Zone 4: Waiting to water because max concurrent zones on this program is 2, and 4 on controller. \n
            - Program 2: Running because the program's start condition was triggered by moisture sensor 1. \n
                - Zone 5: Watering because the program is running. Program has max concurrent of 2, and cn has 4. \n
                - Zone 6: Watering because the program is running. Program has max concurrent of 2, and cn has 4. \n
                - Zone 7: Waiting to water because max concurrent zones on this program is 2, and 4 on controller. \n
                - Zone 8: Waiting to water because max concurrent zones on this program is 2, and 4 on controller. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set the temperature sensors values
            self.config.BaseStation3200[1].temperature_sensors[1].bicoder.set_temperature_reading(_degrees=30.0)
            self.config.BaseStation3200[1].temperature_sensors[2].bicoder.set_temperature_reading(_degrees=40.0)

            # Increment the time by three minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=3)

            # Verify all Programs and their zones
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_12(self):
        """
        - Raise values of temp sensor TAT0001 to verify program 1 doesnt stop \n
        - Raise values of temp sensor SB05308 to verify program 2 doesnt stop \n
        - Increment the clock by 2 minutes. \n
        - The time is now 07:10:00. \n
        - Get the data of each zone and program on the controller. \n
        - Verify Statuses: \n
            - Program 1: Still running because the program's start condition was triggered by temp sensor 1. \n
                - Zone 1: Watering because the program is running. Program has max concurrent of 2, and cn has 4. \n
                - Zone 2: Watering because the program is running. Program has max concurrent of 2, and cn has 4. \n
                - Zone 3: Waiting to water because max concurrent zones on this program is 2, and 4 on controller. \n
                - Zone 4: Waiting to water because max concurrent zones on this program is 2, and 4 on controller. \n
            - Program 2: Still running because the program's start condition was triggered by moisture sensor 1. \n
                - Zone 5: Watering because the program is running. Program has max concurrent of 2, and cn has 4. \n
                - Zone 6: Watering because the program is running. Program has max concurrent of 2, and cn has 4. \n
                - Zone 7: Waiting to water because max concurrent zones on this program is 2, and 4 on controller. \n
                - Zone 8: Waiting to water because max concurrent zones on this program is 2, and 4 on controller. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set the temperature sensors values
            self.config.BaseStation3200[1].temperature_sensors[1].bicoder.set_temperature_reading(_degrees=35.0)
            self.config.BaseStation3200[1].temperature_sensors[2].bicoder.set_temperature_reading(_degrees=65.0)

            # Increment the time by two minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)

            # Verify all Programs and their zones
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_13(self):
        """
        - Raise the value of temp sensor SB05308 to stop program 2. \n
        - Increment the clock by 4 minutes. \n
        - The time is now 07:14:00. \n
        - Get the data of each zone and program on the controller. \n
        - Verify Statuses: \n
            - Program 1: Still running because it hasn't hit any stop conditions and its zones are still watering. \n
                - Zone 1: Watering because the program is running. Program has max concurrent of 2, and cn has 4. \n
                - Zone 2: Watering because the program is running. Program has max concurrent of 2, and cn has 4. \n
                - Zone 3: Waiting to water because max concurrent zones on this program is 2, and 4 on controller. \n
                - Zone 4: Waiting to water because max concurrent zones on this program is 2, and 4 on controller. \n
            - Program 2: Done because the moisture sensor hit its upper limit, triggering a stop condition on this
                         program. \n
                - Zone 5: Done because the program had a stop condition trigger. \n
                - Zone 6: Done because the program had a stop condition trigger. \n
                - Zone 7: Done because the program had a stop condition trigger. \n
                - Zone 8: Done because the program had a stop condition trigger. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set the temperature sensors values
            self.config.BaseStation3200[1].temperature_sensors[2].bicoder.set_temperature_reading(_degrees=75.0)

            # Increment the time by four minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)

            # Verify all Programs and their zones
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_14(self):
        """
        - Clear the previous start/stop/pause conditions \n
        - Set one temp sensor to pause all controller watering using above limits \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].programs[1].temperature_start_conditions[1].set_temperature_mode_off()

            self.config.BaseStation3200[1].programs[2].temperature_start_conditions[2].set_temperature_mode_off()

            self.config.BaseStation3200[1].programs[1].temperature_stop_conditions[1].set_temperature_mode_off()

            self.config.BaseStation3200[1].programs[2].temperature_stop_conditions[2].set_temperature_mode_off()

            self.config.BaseStation3200[1].add_temperature_stop_condition(_temperature_sensor_address=2)
            self.config.BaseStation3200[1].temperature_stop_conditions[2].set_temperature_mode_to_lower_limit()
            self.config.BaseStation3200[1].temperature_stop_conditions[2].set_temperature_threshold(_degrees=32.0)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_15(self):
        """
        - Increment the clock by 1 minute. \n
        - Manually start both Programs that both Programs will be running. \n
        - Increment the clock by 1 minute. \n
        - The time is now 07:15:00. \n
        - Get the data of each zone and program on the controller. \n
        - Verify Statuses: \n
            - Program 1: Running because the program was manually started. \n
                - Zone 1: Watering because the program is running. Program has max concurrent of 2, and cn has 4. \n
                - Zone 2: Watering because the program is running. Program has max concurrent of 2, and cn has 4. \n
                - Zone 3: Soaking because of a program overrun caused by running while this was already waiting. \n
                - Zone 4: Soaking because of a program overrun caused by running while this was already waiting. \n
            - Program 2: Running because the program was manually started. \n
                - Zone 5: Watering because the program is running. Program has max concurrent of 2, and cn has 4. \n
                - Zone 6: Watering because the program is running. Program has max concurrent of 2, and cn has 4. \n
                - Zone 7: Waiting to water because max concurrent zones on this program is 2, and 4 on controller. \n
                - Zone 8: Waiting to water because max concurrent zones on this program is 2, and 4 on controller. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Start both Programs manually
            self.config.BaseStation3200[1].programs[1].set_program_to_start_like_controller_manual_start()
            self.config.BaseStation3200[1].programs[2].set_program_to_start_like_controller_manual_start()

            # Increment the time by one minute
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)

            # Verify all Programs and their zones
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_16(self):
        """
        - Set moisture sensor 1's temperature reading to 31, which will trigger the condition to stop all Programs. \n
        - Increment the clock by 4 minutes. \n
        - The time is now 07:19:00. \n
        - Get the data of each zone and program on the controller. \n
        - Verify Statuses: \n
            - Program 1: Done because a stop condition was triggered by moisture sensor 1. \n
                - Zone 1: Done because the program had a stop condition trigger. \n
                - Zone 2: Done because the program had a stop condition trigger. \n
                - Zone 3: Done because the program had a stop condition trigger. \n
                - Zone 4: Done because the program had a stop condition trigger. \n
            - Program 2: Done because a stop condition was triggered by moisture sensor 1. \n
                - Zone 5: Done because the program had a stop condition trigger. \n
                - Zone 6: Done because the program had a stop condition trigger. \n
                - Zone 7: Done because the program had a stop condition trigger. \n
                - Zone 8: Done because the program had a stop condition trigger. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set the temperature sensors values
            self.config.BaseStation3200[1].temperature_sensors[2].bicoder.set_temperature_reading(_degrees=31.0)

            # Increment the time by four minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)

            # Verify all Programs and their zones
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_17(self):
        """
        - Clear the previous start/stop/pause conditions \n
        - Set two temp sensors to pause two simultaneous Programs at one time \n
        - Set temp sensor TAT0001 to pause program 1 when below limit \n
        - Set temp sensor SB05308 to pause program 2 when below limit \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # We are hard coding this value in for now because the start/stop/pause module doesn't support the 'AL'
            #
            # TODO: Ben - 11/20/17
            #   Updated the sensor serial number from SB05308 to TAT0002 (the temp sensor replaced for this test)
            # self.config.BaseStation3200[1].ser.send_and_wait_for_reply(tosend="SET,PP=AL,TS=TAT0002,TM=OF")
            self.config.BaseStation3200[1].temperature_stop_conditions[2].set_temperature_mode_off()

            self.config.BaseStation3200[1].programs[1].add_temperature_pause_condition(_temperature_sensor_address=1)
            self.config.BaseStation3200[1].programs[1].temperature_pause_conditions[1].\
                set_temperature_mode_to_lower_limit()
            self.config.BaseStation3200[1].programs[1].temperature_pause_conditions[1].\
                set_temperature_threshold(_degrees=32.0)
            self.config.BaseStation3200[1].programs[1].temperature_pause_conditions[1].\
                set_temperature_pause_time(_minutes=4)

            self.config.BaseStation3200[1].programs[2].add_temperature_pause_condition(_temperature_sensor_address=2)
            self.config.BaseStation3200[1].programs[2].temperature_pause_conditions[2].\
                set_temperature_mode_to_upper_limit()
            self.config.BaseStation3200[1].programs[2].temperature_pause_conditions[2].\
                set_temperature_threshold(_degrees=50.0)
            self.config.BaseStation3200[1].programs[2].temperature_pause_conditions[2].\
                set_temperature_pause_time(_minutes=4)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_18(self):
        """
        - Set temp sensor values to allow Programs to start watering. \n
        - Increment the clock by 2 minutes. \n
        - The time is now 07:21:00. \n
        - Get the data of each zone and program on the controller. \n
        - Verify Statuses: \n
            - Program 1: Running because the program was manually started. \n
                - Zone 1: Watering because the program is running. Program has max concurrent of 2, and cn has 4. \n
                - Zone 2: Watering because the program is running. Program has max concurrent of 2, and cn has 4. \n
                - Zone 3: Waiting to water because max concurrent zones on this program is 2, and 4 on controller. \n
                - Zone 4: Waiting to water because max concurrent zones on this program is 2, and 4 on controller. \n
            - Program 2: Running because the program was manually started. \n
                - Zone 5: Watering because the program is running. Program has max concurrent of 2, and cn has 4. \n
                - Zone 6: Watering because the program is running. Program has max concurrent of 2, and cn has 4. \n
                - Zone 7: Waiting to water because max concurrent zones on this program is 2, and 4 on controller. \n
                - Zone 8: Waiting to water because max concurrent zones on this program is 2, and 4 on controller. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set the temperature sensors values
            self.config.BaseStation3200[1].temperature_sensors[1].bicoder.set_temperature_reading(_degrees=35.0)

            self.config.BaseStation3200[1].temperature_sensors[2].bicoder.set_temperature_reading(_degrees=45.0)

            # Start both Programs manually
            self.config.BaseStation3200[1].programs[1].set_program_to_start_like_controller_manual_start()
            self.config.BaseStation3200[1].programs[2].set_program_to_start_like_controller_manual_start()

            # Increment the time by two minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)

            # Verify all Programs and their zones
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_waiting_to_water()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_19(self):
        """
        - Lower value of temp sensor TAT0001 to pause program 1 \n
        - Raise value of temp sensor SB05308 to pause program 2 \n
        - Increment the clock by 4 minutes. \n
        - The time is now 07:25:00. \n
        - Get the data of each zone and program on the controller. \n
        - Verify Statuses: \n
            - Program 1: Paused because Temp Sensor 1 went below the lower threshold, pausing this program. \n
                - Zone 1: Paused because the program it is on had its pause condition triggered. \n
                - Zone 2: Paused because the program it is on had its pause condition triggered. \n
                - Zone 3: Paused because the program it is on had its pause condition triggered. \n
                - Zone 4: Paused because the program it is on had its pause condition triggered. \n
            - Program 2: Paused because Moisture Sensor 1 went above the upper threshold, pausing this program. \n
                - Zone 5: Paused because the program it is on had its pause condition triggered. \n
                - Zone 6: Paused because the program it is on had its pause condition triggered. \n
                - Zone 7: Paused because the program it is on had its pause condition triggered. \n
                - Zone 8: Paused because the program it is on had its pause condition triggered. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set the temperature sensors values
            self.config.BaseStation3200[1].temperature_sensors[1].bicoder.set_temperature_reading(_degrees=31.0)

            self.config.BaseStation3200[1].temperature_sensors[2].bicoder.set_temperature_reading(_degrees=58.0)

            # Increment the time by four minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)

            # Verify all Programs and their zones
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_paused()

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_paused()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_20(self):
        """
        - Raise value of temp sensor TAT0001 to un-pause program 1 \n
        - Increment the clock by 2 minutes. \n
        - The time is now 07:27:00. \n
        - Get the data of each zone and program on the controller. \n
        - Verify Statuses: \n
            - Program 1: Running because the temperature reading was was raised to un-pause the program. \n
                - Zone 1: Watering because the program is running. Program has max concurrent of 2, and cn has 4. \n
                - Zone 2: Watering because the program is running. Program has max concurrent of 2, and cn has 4. \n
                - Zone 3: Waiting to water because max concurrent zones on this program is 2, and 4 on controller. \n
                - Zone 4: Waiting to water because max concurrent zones on this program is 2, and 4 on controller. \n
            - Program 2: Paused because Moisture Sensor 1 went above the upper threshold, pausing this program. \n
                - Zone 5: Paused because the program it is on had its pause condition triggered. \n
                - Zone 6: Paused because the program it is on had its pause condition triggered. \n
                - Zone 7: Paused because the program it is on had its pause condition triggered. \n
                - Zone 8: Paused because the program it is on had its pause condition triggered. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set the temperature sensors values
            self.config.BaseStation3200[1].temperature_sensors[1].bicoder.set_temperature_reading(_degrees=41.0)

            # Increment the time by four minutes
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            # Verify all Programs and their zones
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()

            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].zones[6].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].zones[7].statuses.verify_status_is_paused()
            self.config.BaseStation3200[1].zones[8].statuses.verify_status_is_paused()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
