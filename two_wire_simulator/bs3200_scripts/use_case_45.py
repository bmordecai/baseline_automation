import sys
import logging
from datetime import datetime, timedelta

# this import allows us to directly use the date_mngr
from datetime import time, timedelta, datetime, date

# import common.product as helper_methods
from common.configuration import Configuration

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr

# import log_handler for logging functionality
from common.logging_handler import log_handler

# allows us to create objects as would be done in the configuration
from common.objects.bicoders.valve_bicoder import ValveBicoder

# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, csv
from common import helper_methods

__author__ = 'Eldin'


class ControllerUseCase45(object):
    """
    Test name:
        - DC Latch Serial Numbers
    purpose:
        - set up a full configuration on the controller
            - Load valve bicoders that have a 0 as the last digit on the serial number
                - Dual valve bicoders
            - Test serial numbers of DC latching decoders

    Coverage area: \n
        - Load valve bicoders with serial numbers ending with 0
        - Address these bicoders
    Date References:
        - configuration for script is located common\configuration_files\dc_latching_decoder_serial_numbers.json
        - the devices and addresses range is read from the .json file

    """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
            - Load the single and dual valve bicoders onto the controller. \n
                - We have to hard code the dual valve bicoder load to escape from our error checking that would prevent us
                  from setting a dual valve bicoder with a serial number that ends in 0. \n
            - Create the valve bicoder objects for the dual valve bicoders we had to load manually. \n
            - Search for zones. \n
            - Create Zone objects to represent the objects on the controller. \n
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        Setup Zones
        ############################
        Load the single and dual valve bicoders onto the controller. \n
            - We have to hard code the dual valve bicoder load to escape from our error checking that would prevent us
              from setting a dual valve bicoder with a serial number that ends in 0. \n
        Create the valve bicoder objects for the dual valve bicoders we had to load manually. \n
        Search for zones. \n
        Create Zone objects to represent the objects on the controller. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            single_valve_serial_number_1 = "K000000"
            single_valve_serial_number_2 = "K000001"
            dual_valve_serial_number_1 = "K000010"
            dual_valve_serial_number_1_incremented = "K000011"
            dual_valve_serial_number_2 = "K000020"
            dual_valve_serial_number_2_incremented = "K000021"
            # We should be able to load and assign serial numbers on single valves that start with a K and end with a 0
            self.config.BaseStation3200[1].load_dv(dv_type=opcodes.single_valve_decoder,
                                                   list_of_decoder_serial_nums=[single_valve_serial_number_1,
                                                                                single_valve_serial_number_2])

            # We send the command to load this serial number onto the controller
            self.config.BaseStation3200[1].ser.send_and_wait_for_reply("DEV,D2={0}".format(dual_valve_serial_number_1))
            # Create two valve bicoders that will represent the serial number we just loaded onto the controller
            self.config.BaseStation3200[1].valve_bicoders[dual_valve_serial_number_1] = ValveBicoder(
                _sn=dual_valve_serial_number_1,
                _controller=self.config.BaseStation3200[1],
                _id=opcodes.zone)
            self.config.BaseStation3200[1].valve_bicoders[dual_valve_serial_number_1_incremented] = ValveBicoder(
                _sn=dual_valve_serial_number_1_incremented,
                _controller=self.config.BaseStation3200[1],
                _id=opcodes.zone)

            # We send the command to load this serial number onto the controller
            self.config.BaseStation3200[1].ser.send_and_wait_for_reply("DEV,D2={0}".format(dual_valve_serial_number_2))
            # Create two valve bicoders that will represent the serial number we just loaded onto the controller
            self.config.BaseStation3200[1].valve_bicoders[dual_valve_serial_number_2] = ValveBicoder(
                _sn=dual_valve_serial_number_2,
                _controller=self.config.BaseStation3200[1],
                _id=opcodes.zone)
            self.config.BaseStation3200[1].valve_bicoders[dual_valve_serial_number_2_incremented] = ValveBicoder(
                _sn=dual_valve_serial_number_2_incremented,
                _controller=self.config.BaseStation3200[1],
                _id=opcodes.zone)

            # Search for the zones we just loaded onto the controller
            self.config.BaseStation3200[1].do_search_for_zones()

            # Create the object on the controller
            self.config.zn_ad_range = [1, 6]
            self.config.d1 = [single_valve_serial_number_1, single_valve_serial_number_2]
            self.config.d2 = [dual_valve_serial_number_1, dual_valve_serial_number_2]
            self.config.create_bl_3200_devices_objects(_controller=self.config.BaseStation3200[1])
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        Verify addressing behavior
        ############################
        - Verify that single valve bicoders with serial numbers ending in 0 can be assigned. \n
        - Verify that single valve bicoders with serial numbers not ending in 0 can be assigned. \n
        - Verify that each dual valve bicoder with a serial number that ends in 0 returns a BC when we try to assign it
          an address. (They are DC latch decoders) \n
        - Verify that each dual valve bicoder with a serial number that ends in 1 or 2 can still be assigned even
          though they were loaded with a serial number that started with a 0. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # The first two zones we try to address should be fine
            self.config.BaseStation3200[1].zones[1].set_address(_ad=1)
            self.config.BaseStation3200[1].zones[1].set_default_values()
            self.config.BaseStation3200[1].zones[2].set_address(_ad=2)
            self.config.BaseStation3200[1].zones[2].set_default_values()

            # These should all give back a BC when we try to address them on the controller because dual valve bicoders
            # can't be addressed with serial numbers that end in 0.
            try:
                self.config.BaseStation3200[1].zones[3].set_address(_ad=3)
            except Exception:
                print "The controller replied back with a BC when we tried to address a dual valve bicoder with the" \
                      "serial number: K000010. This is expected as it should not be allowed."

            # This should work because the controller changes K000010 to be K000011 and K000012
            self.config.BaseStation3200[1].zones[4].set_address(_ad=4)
            self.config.BaseStation3200[1].zones[4].set_default_values()

            # These should all give back a BC when we try to address them on the controller because dual valve bicoders
            # can't be addressed with serial numbers that end in 0.
            try:
                self.config.BaseStation3200[1].zones[5].set_address(_ad=5)
            except Exception:
                print "The controller replied back with a BC when we tried to address a dual valve bicoder with the" \
                      "serial number: K000020. This is expected as it should not be allowed."

            # This should work because the controller changes K000010 to be K000011 and K000012
            self.config.BaseStation3200[1].zones[6].set_address(_ad=6)
            self.config.BaseStation3200[1].zones[6].set_default_values()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
