import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration
from old_32_10_sb_objects_dec_29_2017.common.configuration import Configuration as OldConfiguration
from common.helper_methods import test_elapsed_time, end_controller_test
from common.objects.base_classes.ser import Ser
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr

# Objects
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.poc_3200 import POC3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.pg_3200 import PG3200
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.zp import ZoneProgram
from old_32_10_sb_objects_dec_29_2017.common.objects.base_classes.web_driver import *
from old_32_10_sb_objects_dec_29_2017.common.objects.controller.ml import Mainline
from old_32_10_sb_objects_dec_29_2017.common.imports import opcodes
# from common.objects.base_classes.web_driver import *

from common.imports import opcodes

# import log_handler for logging functionality
from common.logging_handler import log_handler

# Import new v16 controller object
from common.objects.controllers.bl_32 import BaseStation3200
# Import new v16 device objects
from common.objects.devices.zn import Zone
from common.objects.devices.fm import FlowMeter
from common.objects.devices.ms import MoistureSensor
from common.objects.devices.mv import MasterValve
from common.objects.devices.pm import Pump
from common.objects.devices.ps import PressureSensor
from common.objects.devices.sw import EventSwitch
from common.objects.devices.ts import TemperatureSensor
# Import new v16 bicoder objects
from common.objects.bicoders.analog_bicoder import AnalogBicoder
from common.objects.bicoders.flow_bicoder import FlowBicoder
from common.objects.bicoders.moisture_bicoder import MoistureBicoder
from common.objects.bicoders.pump_bicoder import PumpBicoder
from common.objects.bicoders.switch_bicoder import SwitchBicoder
from common.objects.bicoders.temp_bicoder import TempBicoder
from common.objects.bicoders.valve_bicoder import ValveBicoder
# Import new v16 programming objects
from common.objects.programming.ml import Mainline as v16Mainline
from common.objects.programming.pg_3200 import PG3200 as v16PG3200
from common.objects.programming.point_of_control import PointOfControl as v16PointOfControl
from common.objects.programming.ws import WaterSource
from common.objects.programming.zp import ZoneProgram as v16ZoneProgram

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr

# this import allows us to directly use the date_mngr
from datetime import time

__author__ = "Eldin"


class ControllerUseCase22(object):
    """
    Test name: \n
        Program to Zone Mainline after v16 Update \n
    Purpose: \n
        Test that after an update to v16 code, Programs no longer have mainlines, and instead is given to the zone. \n
    Coverage area: \n
        1. Test that we can set up a program with a mainline and zones (This is how it works in V12 code) \n
            - (Master Valve 1) -> (POC 1) -> (Mainline 1) -> (Program 1) -> (Zone 1) (Primary)
                                                                         -> (Zone 2) (Linked)
                                                                         -> (Zone 3) (Timed)
                                                                         -> (Zone 4) (Timed)
            - (Master Valve 2) -> (POC 2) -> (Mainline 2) -> (Program 2) -> (Zone 3) (Timed)
                                                                         -> (Zone 4) (Timed)
                                                                         -> (Zone 5) (Primary)
                                                                         -> (Zone 6) (Linked)
        2. Test that we can update from v12 code to v16 \n
        3. Verify that the mainlines on the Programs get transferred to zones (This is how it works in V16 code) \n
            - (Master Valve 1) -> (POC 1) -> (Mainline 1) -> (Zone 1) (Primary) -> (Program 1)
                                                          -> (Zone 2) (Linked) -> (Program 1)
                                                          -> (Zone 3) (Timed) -> (Program 1 & 2)
                                                          -> (Zone 4) (Timed) -> (Program 1 & 2)
            - (Master Valve 2) -> (POC 2) -> (Mainline 2) -> (Zone 5) (Primary) -> (Program 2)
                                                          -> (Zone 6) (Linked) -> (Program 2)
    """

    # Current "Latest" v16 firmware versions in DB (update as more are released and added to P2!)
    BaseMgr_20_Firmware_Update_DB_ID = 153     # 153: v16.0.588 (BaseMgr_20)
    BaseMgr_35_Firmware_Update_DB_ID = 159     # 154: v16.0.592 (BaseMgr_35)

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        # For this test only, set test timing here so that we can get a true start to finish. Without setting it here,
        # we can't time how long it takes to downgrade and upgrade the controller case.

        # Without having to down-grade controller via local directory to 12.34, takes about 00:08:30 minutes.
        # With having to down-grade controller via local directory to 12.34, takes about 00:15:00 minutes.

        # If controller is currently v16, downgrade it via local directory to v12.34
        self.check_controller_version(test_name=test_name, user_configuration_instance=user_configuration_instance)

        # Init "OLD" configuration.py object for creating and initializing v12 3200 objects
        self.config = OldConfiguration(cn_type="32",
                                       test_name=test_name,
                                       user_conf_file=user_configuration_instance,
                                       data_json_file=json_configuration_file)

        # Init "NEW" configuration.py object for when we verify v16 controller upgrade
        self.v16_config = Configuration(test_name=test_name,
                                        user_conf_file=user_configuration_instance,
                                        data_json_file=json_configuration_file)

        self.run_use_case()

    def run_use_case(self):
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test()
                    # we have to set a long timeout on the serial port because the test runs for long periods of time
                    self.config.controllers[1].set_serial_port_timeout(timeout=5040)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def check_controller_version(self, test_name, user_configuration_instance):
        """
        Check if the controller is operating on v16 firmware, if it is, do a firmware update from the local directory
        to 12.34 firmware. If it isn't, do nothing.
        """
        try:
            # Create the configuration object and initialize it so that we can communicate through the serial connection
            self.config = Configuration(test_name=test_name,
                                        user_conf_file=user_configuration_instance,
                                        data_json_file="empty_configuration.json")
            self.config.initialize_for_test(connect_to_basemanager=True)

            # Get data from the controller and check it's firmware version
            self.config.BaseStation3200[1].get_data()
            firmware_version = self.config.BaseStation3200[1].data.get_value_string_by_key(opcodes.firmware_version)
            if firmware_version >= opcodes.firmware_version_V16:
                # Updates the firmware to 12.34
                self.config.BaseStation3200[1].do_firmware_update(were_from=opcodes.local_directory,
                                                                  directory='common' + os.sep + 'firmware_update_files',
                                                                  file_name='v12.34_special')
        except Exception as e:
            helper_methods.print_test_failed(test_name=self.config.test_name)
            # If logging is enabled, do not bomb out on the Exception. Continue with the use case
            if log_handler.is_enabled():
                log_handler.exception(message=e.message)
            else:
                raise
        finally:
            # Clean up the old configuration, does not end the test, simply closes the serial port that was opened
            end_controller_test(config_object=self.config)

    #################################
    def convert_v12_objects_to_v16_objects(self):
        """
        HERE WE GO
        """
        # Create v16 3200 Controller object
        new_ser_object = Ser(self.config.controllers[1].ser.c_port, self.config.controllers[1].ser.s_port)
        new_ser_object.serial_conn = self.config.controllers[1].ser.serial_conn
        self.config.controllers[1].ser = new_ser_object
        self.v16_config.BaseStation3200[1] = BaseStation3200(_description=self.config.controllers[1].ds,
                                                             _serial_port=self.config.controllers[1].ser,
                                                             _serial_number=self.config.controllers[1].sn,
                                                             _firmware_version=self.config.controllers[1].vr,
                                                             _mac=self.config.controllers[1].mac,
                                                             _port_address=None,
                                                             _socket_port=None,
                                                             _ip_address=None)

        try:
            # Convert zones
            for zone_ad in sorted(self.config.zones.keys()):
                bicoder = ValveBicoder(
                    _sn=self.config.zones[zone_ad].sn,
                    _controller=self.v16_config.BaseStation3200[1],
                    _id=opcodes.zone,
                    _address=zone_ad
                )
                self.v16_config.BaseStation3200[1].zones[zone_ad] = Zone(
                    _controller=self.v16_config.BaseStation3200[1],
                    _address=zone_ad,
                    _valve_bicoder=bicoder
                )
                self.v16_config.BaseStation3200[1].zones[zone_ad].bicoder.va = self.config.zones[zone_ad].va
                self.v16_config.BaseStation3200[1].zones[zone_ad].bicoder.vv = self.config.zones[zone_ad].vv
                self.v16_config.BaseStation3200[1].zones[zone_ad].bicoder.vt = self.config.zones[zone_ad].vt
                self.v16_config.BaseStation3200[1].zones[zone_ad].ds = self.config.zones[zone_ad].ds
                self.v16_config.BaseStation3200[1].zones[zone_ad].la = self.config.zones[zone_ad].la
                self.v16_config.BaseStation3200[1].zones[zone_ad].lg = self.config.zones[zone_ad].lg
                self.v16_config.BaseStation3200[1].zones[zone_ad].en = self.config.zones[zone_ad].en
                self.v16_config.BaseStation3200[1].zones[zone_ad].df = self.config.zones[zone_ad].df
                self.v16_config.BaseStation3200[1].zones[zone_ad].kc = self.config.zones[zone_ad].kc
                self.v16_config.BaseStation3200[1].zones[zone_ad].pr = self.config.zones[zone_ad].pr
                self.v16_config.BaseStation3200[1].zones[zone_ad].rz = self.config.zones[zone_ad].rz
        except Exception as e:
            e_msg = "Exception thrown converting v12 Zones to v16 Zones.\nException: {0}".format(e.message)
            raise Exception(e_msg)

        try:
            # Convert Flow Meters
            for fm_ad in sorted(self.config.flow_meters.keys()):
                bicoder = FlowBicoder(
                    _sn=self.config.flow_meters[fm_ad].sn,
                    _controller=self.v16_config.BaseStation3200[1],
                    _address=fm_ad
                )
                self.v16_config.BaseStation3200[1].flow_meters[fm_ad] = FlowMeter(
                    _controller=self.v16_config.BaseStation3200[1],
                    _address=fm_ad,
                    _flow_bicoder=bicoder
                )
                self.v16_config.BaseStation3200[1].flow_meters[fm_ad].bicoder.vr = self.config.flow_meters[fm_ad].vr
                self.v16_config.BaseStation3200[1].flow_meters[fm_ad].bicoder.vg = self.config.flow_meters[fm_ad].vg
                self.v16_config.BaseStation3200[1].flow_meters[fm_ad].bicoder.vt = self.config.flow_meters[fm_ad].vt
                self.v16_config.BaseStation3200[1].flow_meters[fm_ad].ds = self.config.flow_meters[fm_ad].ds
                self.v16_config.BaseStation3200[1].flow_meters[fm_ad].la = self.config.flow_meters[fm_ad].la
                self.v16_config.BaseStation3200[1].flow_meters[fm_ad].lg = self.config.flow_meters[fm_ad].lg
                self.v16_config.BaseStation3200[1].flow_meters[fm_ad].en = self.config.flow_meters[fm_ad].en
        except Exception as e:
            e_msg = "Exception thrown converting v12 Flow Meters to v16 Flow Meters.\nException: {0}".format(e.message)
            raise Exception(e_msg)

        try:
            # Convert Moisture Sensors
            for ms_ad in sorted(self.config.moisture_sensors.keys()):
                bicoder = MoistureBicoder(
                    _sn=self.config.moisture_sensors[ms_ad].sn,
                    _controller=self.v16_config.BaseStation3200[1],
                    _address=ms_ad
                )
                self.v16_config.BaseStation3200[1].moisture_sensors[ms_ad] = MoistureSensor(
                    _controller=self.v16_config.BaseStation3200[1],
                    _address=ms_ad,
                    _moisture_bicoder=bicoder
                )
                self.v16_config.BaseStation3200[1].moisture_sensors[ms_ad].bicoder.vp = \
                    self.config.moisture_sensors[ms_ad].vp
                self.v16_config.BaseStation3200[1].moisture_sensors[ms_ad].bicoder.vd = \
                    self.config.moisture_sensors[ms_ad].vd
                self.v16_config.BaseStation3200[1].moisture_sensors[ms_ad].bicoder.vt = \
                    self.config.moisture_sensors[ms_ad].vt
                self.v16_config.BaseStation3200[1].moisture_sensors[ms_ad].ds = self.config.moisture_sensors[ms_ad].ds
                self.v16_config.BaseStation3200[1].moisture_sensors[ms_ad].la = self.config.moisture_sensors[ms_ad].la
                self.v16_config.BaseStation3200[1].moisture_sensors[ms_ad].lg = self.config.moisture_sensors[ms_ad].lg
        except Exception as e:
            e_msg = "Exception thrown converting v12 Moisture Sensors to v16 Moisture Sensors.\n" \
                    "Exception: " "{0}".format(e.message)
            raise Exception(e_msg)

        try:
            # Convert Master Valve
            for mv_ad in sorted(self.config.master_valves.keys()):
                bicoder = ValveBicoder(
                    _sn=self.config.master_valves[mv_ad].sn,
                    _controller=self.v16_config.BaseStation3200[1],
                    _id=opcodes.master_valve,
                    _address=mv_ad
                )
                self.v16_config.BaseStation3200[1].master_valves[mv_ad] = MasterValve(
                    _controller=self.v16_config.BaseStation3200[1],
                    _address=mv_ad,
                    _valve_bicoder=bicoder
                )
                self.v16_config.BaseStation3200[1].master_valves[mv_ad].bicoder.vt = self.config.master_valves[mv_ad].vt
                self.v16_config.BaseStation3200[1].master_valves[mv_ad].ds = self.config.master_valves[mv_ad].ds
                self.v16_config.BaseStation3200[1].master_valves[mv_ad].la = self.config.master_valves[mv_ad].la
                self.v16_config.BaseStation3200[1].master_valves[mv_ad].lg = self.config.master_valves[mv_ad].lg
                self.v16_config.BaseStation3200[1].master_valves[mv_ad].en = self.config.master_valves[mv_ad].en
                self.v16_config.BaseStation3200[1].master_valves[mv_ad].no = self.config.master_valves[mv_ad].no
                self.v16_config.BaseStation3200[1].master_valves[mv_ad].bp = self.config.master_valves[mv_ad].bp
        except Exception as e:
            e_msg = "Exception thrown converting v12 Master Valves to v16 Master Valves.\n" \
                    "Exception: {0}".format(e.message)
            raise Exception(e_msg)

        try:
            # Convert Event Switches
            for sw_ad in sorted(self.config.event_switches.keys()):
                bicoder = SwitchBicoder(
                    _sn=self.config.event_switches[sw_ad].sn,
                    _controller=self.v16_config.BaseStation3200[1],
                    _address=sw_ad
                )
                self.v16_config.BaseStation3200[1].event_switches[sw_ad] = EventSwitch(
                    _controller=self.v16_config.BaseStation3200[1],
                    _address=sw_ad,
                    _switch_bicoder=bicoder
                )
                self.v16_config.BaseStation3200[1].event_switches[sw_ad].bicoder.vc = \
                    self.config.event_switches[sw_ad].vc
                self.v16_config.BaseStation3200[1].event_switches[sw_ad].bicoder.vt = \
                    self.config.event_switches[sw_ad].vt
                self.v16_config.BaseStation3200[1].event_switches[sw_ad].ds = self.config.event_switches[sw_ad].ds
                self.v16_config.BaseStation3200[1].event_switches[sw_ad].la = self.config.event_switches[sw_ad].la
                self.v16_config.BaseStation3200[1].event_switches[sw_ad].lg = self.config.event_switches[sw_ad].lg
                self.v16_config.BaseStation3200[1].event_switches[sw_ad].en = self.config.event_switches[sw_ad].en
        except Exception as e:
            e_msg = "Exception thrown converting v12 Event Switches to v16 Event Switches.\n" \
                    "Exception: {0}".format(e.message)
            raise Exception(e_msg)

        try:
            # Convert Temperature Sensors
            for ts_ad in sorted(self.config.temperature_sensors.keys()):
                bicoder = TempBicoder(
                    _sn=self.config.temperature_sensors[ts_ad].sn,
                    _controller=self.v16_config.BaseStation3200[1],
                    _address=ts_ad
                )
                self.v16_config.BaseStation3200[1].temperature_sensors[ts_ad] = TemperatureSensor(
                    _controller=self.v16_config.BaseStation3200[1],
                    _address=ts_ad,
                    _temp_bicoder=bicoder
                )
                self.v16_config.BaseStation3200[1].temperature_sensors[ts_ad].bicoder.vd = \
                    self.config.temperature_sensors[ts_ad].vd
                self.v16_config.BaseStation3200[1].temperature_sensors[ts_ad].bicoder.vt = \
                    self.config.temperature_sensors[ts_ad].vt
                self.v16_config.BaseStation3200[1].temperature_sensors[ts_ad].ds = \
                    self.config.temperature_sensors[ts_ad].ds
                self.v16_config.BaseStation3200[1].temperature_sensors[ts_ad].la = \
                    self.config.temperature_sensors[ts_ad].la
                self.v16_config.BaseStation3200[1].temperature_sensors[ts_ad].lg = \
                    self.config.temperature_sensors[ts_ad].lg
        except Exception as e:
            e_msg = "Exception thrown converting v12 Temperature Sensors to v16 Temperature Sensors.\n" \
                    "Exception: {0}".format(e.message)
            raise Exception(e_msg)

        try:
            # Convert Programs
            for pg_ad in sorted(self.config.programs.keys()):
                self.v16_config.BaseStation3200[1].programs[pg_ad] = v16PG3200(
                    _controller=self.v16_config.BaseStation3200[1],
                    _ad=pg_ad,
                )
                self.v16_config.BaseStation3200[1].programs[pg_ad].ds = self.config.programs[pg_ad].ds
                self.v16_config.BaseStation3200[1].programs[pg_ad].en = self.config.programs[pg_ad].en
                self.v16_config.BaseStation3200[1].programs[pg_ad].ww = self.config.programs[pg_ad].ww
                self.v16_config.BaseStation3200[1].programs[pg_ad].mc = self.config.programs[pg_ad].mc
                self.v16_config.BaseStation3200[1].programs[pg_ad].sa = self.config.programs[pg_ad].sa
                self.v16_config.BaseStation3200[1].programs[pg_ad].ci = self.config.programs[pg_ad].ci
                self.v16_config.BaseStation3200[1].programs[pg_ad].di = self.config.programs[pg_ad].di
                self.v16_config.BaseStation3200[1].programs[pg_ad].wd = self.config.programs[pg_ad].wd
                self.v16_config.BaseStation3200[1].programs[pg_ad].sm = self.config.programs[pg_ad].sm
                self.v16_config.BaseStation3200[1].programs[pg_ad].st = self.config.programs[pg_ad].st
                self.v16_config.BaseStation3200[1].programs[pg_ad].pr = self.config.programs[pg_ad].pr
                self.v16_config.BaseStation3200[1].programs[pg_ad].bp = self.config.programs[pg_ad].bp
        except Exception as e:
            e_msg = "Exception thrown converting v12 Programs to v16 Programs.\nException: {0}".format(e.message)
            raise Exception(e_msg)

        try:
            # Convert Zone Programs
            for zone_program in sorted(self.config.zone_programs.values()):
                self.v16_config.BaseStation3200[1].programs[zone_program.program.ad].zone_programs[zone_program.zone.ad] = v16ZoneProgram(
                    _controller=self.v16_config.BaseStation3200[1],
                    zone_ad=zone_program.zone.ad,
                    prog_ad=zone_program.program.ad
                )

                self.v16_config.BaseStation3200[1].programs[zone_program.program.ad].zone_programs[zone_program.zone.ad]\
                    .rt = self.config.zone_programs[zone_program.zone.ad].rt
                self.v16_config.BaseStation3200[1].programs[zone_program.program.ad].zone_programs[zone_program.zone.ad]\
                    .ra = self.config.zone_programs[zone_program.zone.ad].ra
                self.v16_config.BaseStation3200[1].programs[zone_program.program.ad].zone_programs[zone_program.zone.ad]\
                    .ct = self.config.zone_programs[zone_program.zone.ad].ct
                self.v16_config.BaseStation3200[1].programs[zone_program.program.ad].zone_programs[zone_program.zone.ad]\
                    .so = self.config.zone_programs[zone_program.zone.ad].so
                self.v16_config.BaseStation3200[1].programs[zone_program.program.ad].zone_programs[zone_program.zone.ad]\
                    .pz = self.config.zone_programs[zone_program.zone.ad].pz
                self.v16_config.BaseStation3200[1].programs[zone_program.program.ad].zone_programs[zone_program.zone.ad]\
                    .ws = self.config.zone_programs[zone_program.zone.ad].ws
                self.v16_config.BaseStation3200[1].programs[zone_program.program.ad].zone_programs[zone_program.zone.ad]\
                    .ms = self.config.zone_programs[zone_program.zone.ad].ms
                self.v16_config.BaseStation3200[1].programs[zone_program.program.ad].zone_programs[zone_program.zone.ad]\
                    .ll = self.config.zone_programs[zone_program.zone.ad].ll
                self.v16_config.BaseStation3200[1].programs[zone_program.program.ad].zone_programs[zone_program.zone.ad]\
                    .ul = self.config.zone_programs[zone_program.zone.ad].ul
                self.v16_config.BaseStation3200[1].programs[zone_program.program.ad].zone_programs[zone_program.zone.ad]\
                    .cc = self.config.zone_programs[zone_program.zone.ad].cc
                self.v16_config.BaseStation3200[1].programs[zone_program.program.ad].zone_programs[zone_program.zone.ad]\
                    .ee = self.config.zone_programs[zone_program.zone.ad].ee
        except Exception as e:
            e_msg = "Exception thrown converting v12 Zone Programs to v16 Zone Programs.\nException: {0}".format(e.message)
            raise Exception(e_msg)

        # TODO Program Start conditions
        # try:
        #     pass
        # except Exception as e:
        #     e_msg = "Exception thrown converting v12 Program Start Conditions to v16 Program Start Conditions.\n" \
        #             "Exception: {0}".format(e.message)
        #     raise Exception(e_msg)

        # TODO Program Stop conditions
        # try:
        #     pass
        # except Exception as e:
        #     e_msg = "Exception thrown converting v12 Program Stop Conditions to v16 Program Stop Conditions.\n" \
        #             "Exception: {0}".format(e.message)
        #     raise Exception(e_msg)

        # TODO Program Pause conditions
        # try:
        #     pass
        # except Exception as e:
        #     e_msg = "Exception thrown converting v12 Program Pause Conditions to v16 Program Pause Conditions.\n" \
        #             "Exception: {0}".format(e.message)
        #     raise Exception(e_msg)

        # TODO fill in the rest of the mainline attributes if they are needed (default on the controller)
        try:
            # Convert Mainlines
            for ml_ad in sorted(self.config.mainlines.keys()):
                self.v16_config.BaseStation3200[1].mainlines[ml_ad] = v16Mainline(
                    _controller=self.v16_config.BaseStation3200[1],
                    _ad=ml_ad,
                )
                self.v16_config.BaseStation3200[1].mainlines[ml_ad].ds = self.config.mainlines[ml_ad].ds
                self.v16_config.BaseStation3200[1].mainlines[ml_ad].ft = self.config.mainlines[ml_ad].ft
                self.v16_config.BaseStation3200[1].mainlines[ml_ad].fl = self.config.mainlines[ml_ad].fl
                self.v16_config.BaseStation3200[1].mainlines[ml_ad].lc = self.config.mainlines[ml_ad].lc

                # Convert v12 ML high flow variance to v16 adv. high flow variance tiers 1-4
                self.v16_config.BaseStation3200[1].mainlines[ml_ad].ah = self.config.mainlines[ml_ad].hv
                self.v16_config.BaseStation3200[1].mainlines[ml_ad].bh = self.config.mainlines[ml_ad].hv
                self.v16_config.BaseStation3200[1].mainlines[ml_ad].ch = self.config.mainlines[ml_ad].hv
                self.v16_config.BaseStation3200[1].mainlines[ml_ad].dh = self.config.mainlines[ml_ad].hv
                self.v16_config.BaseStation3200[1].mainlines[ml_ad].zh = self.config.mainlines[ml_ad].hs

                # Convert v12 ML low flow variance to v16 adv. low flow variance tiers 1-4
                self.v16_config.BaseStation3200[1].mainlines[ml_ad].al = self.config.mainlines[ml_ad].lv
                self.v16_config.BaseStation3200[1].mainlines[ml_ad].bl = self.config.mainlines[ml_ad].lv
                self.v16_config.BaseStation3200[1].mainlines[ml_ad].cl = self.config.mainlines[ml_ad].lv
                self.v16_config.BaseStation3200[1].mainlines[ml_ad].dl = self.config.mainlines[ml_ad].lv
                self.v16_config.BaseStation3200[1].mainlines[ml_ad].zl = self.config.mainlines[ml_ad].ls

                # Enable advanced flow variance
                self.v16_config.BaseStation3200[1].mainlines[ml_ad].af = opcodes.true

        except Exception as e:
            e_msg = "Exception thrown converting v12 Mainlines to v16 Mainlines.\nException: {0}".format(e.message)
            raise Exception(e_msg)

        # TODO: Need to check v12 poc for an empty condition, if it has one, we need to create it for the WS
        try:
            # Convert POINTS OF CONNECTION -> WATER SOURCES + POINTS OF CONTROL
            for poc_ad in sorted(self.config.poc.keys()):

                # We initialize the point of control first with its half of the old point of connection's values
                self.v16_config.BaseStation3200[1].points_of_control[poc_ad] = v16PointOfControl(
                    _controller=self.v16_config.BaseStation3200[1],
                    _ad=poc_ad,
                )
                self.v16_config.BaseStation3200[1].points_of_control[poc_ad].ds = self.config.poc[poc_ad].ds
                self.v16_config.BaseStation3200[1].points_of_control[poc_ad].en = self.config.poc[poc_ad].en
                self.v16_config.BaseStation3200[1].points_of_control[poc_ad].fl = self.config.poc[poc_ad].fl
                self.v16_config.BaseStation3200[1].points_of_control[poc_ad].hf = self.config.poc[poc_ad].hf
                self.v16_config.BaseStation3200[1].points_of_control[poc_ad].hs = self.config.poc[poc_ad].hs
                self.v16_config.BaseStation3200[1].points_of_control[poc_ad].uf = self.config.poc[poc_ad].uf
                self.v16_config.BaseStation3200[1].points_of_control[poc_ad].us = self.config.poc[poc_ad].us
                self.v16_config.BaseStation3200[1].points_of_control[poc_ad].fm = self.config.poc[poc_ad].fm
                self.v16_config.BaseStation3200[1].points_of_control[poc_ad].mv = self.config.poc[poc_ad].mv
                self.v16_config.BaseStation3200[1].points_of_control[poc_ad].us = self.config.poc[poc_ad].us
                self.v16_config.BaseStation3200[1].points_of_control[poc_ad].ml = self.config.poc[poc_ad].ml
                self.v16_config.BaseStation3200[1].points_of_control[poc_ad].controller_mainline =\
                    self.config.poc[poc_ad].ml

                # Now we initialize the water source with its half of the old point of connection's values
                self.v16_config.BaseStation3200[1].water_sources[poc_ad] = WaterSource(
                    _controller=self.v16_config.BaseStation3200[1],
                    _ad=poc_ad,
                )
                self.v16_config.BaseStation3200[1].water_sources[poc_ad].ds = 'WS of ' + self.config.poc[poc_ad].ds
                self.v16_config.BaseStation3200[1].water_sources[poc_ad].en = self.config.poc[poc_ad].en
                self.v16_config.BaseStation3200[1].water_sources[poc_ad].pr = self.config.poc[poc_ad].pr
                self.v16_config.BaseStation3200[1].water_sources[poc_ad].wb = self.config.poc[poc_ad].wb
                self.v16_config.BaseStation3200[1].water_sources[poc_ad].ws = self.config.poc[poc_ad].ws
                self.v16_config.BaseStation3200[1].water_sources[poc_ad].wr = self.config.poc[poc_ad].wr
                self.v16_config.BaseStation3200[1].water_sources[poc_ad].pc = poc_ad
                self.v16_config.BaseStation3200[1].water_sources[poc_ad].controller_point_of_control = poc_ad
        except Exception as e:
            e_msg = "Exception thrown converting v12 Points of Control to v16 Points of Control.\n" \
                    "Exception: {0}".format(e.message)
            raise Exception(e_msg)

        # ------------------------- #
        # Assign Zones to Mainlines #
        # ------------------------- #

        try:

            # For each program object
            for program in self.v16_config.BaseStation3200[1].programs.values():

                # For each zone program object in the program check if a zone is attached to a given program as a
                # "zone program" and convert the mainline location
                for zone_program in program.zone_programs.values():

                    # Get Mainline number assigned to v12 Program
                    v12_pg_mainline_number = self.config.programs[program.ad].ml

                    # Get v16 Zone and Mainline object reference
                    v16_zone_obj = self.v16_config.BaseStation3200[1].zones[zone_program.zone.ad]
                    v16_mainline_obj = self.v16_config.BaseStation3200[1].mainlines[v12_pg_mainline_number]

                    # Set the v16 Zone mainline attribute to the v12 program assigned mainline number
                    v16_zone_obj.ml = v12_pg_mainline_number

                    # Add v16 Zone to v16 mainline zone list
                    v16_mainline_obj.zones[v16_zone_obj.ad] = v16_zone_obj

        except Exception as e:
            e_msg = "Exception thrown converting v12 Program to Mainline Assignments to v16 Zone to Mainline " \
                    "assignments.\nException: {0}".format(e.message)
            raise Exception(e_msg)

    #################################
    def step_1(self):
        """
        - The controller version is V12 - this is done in the "init" \n
        - Sets up the controller \n
            - setup controller \n
            - Stop clock \n
            - enable faux IO \n
            - set max concurrency on controller to 4 we only want 4 zones to run at a time \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.controllers[1].init_cn()
            self.config.controllers[1].load_all_dv_to_cn(d1_list=self.config.d1,
                                                         mv_d1_list=self.config.mv_d1,
                                                         d2_list=self.config.d2,
                                                         mv_d2_list=self.config.mv_d2,
                                                         d4_list=self.config.d4,
                                                         dd_list=self.config.dd,
                                                         ms_list=self.config.ms,
                                                         fm_list=self.config.fm,
                                                         ts_list=self.config.ts,
                                                         sw_list=self.config.sw)

            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.zone)
            # assign zones an address between 1-200
            self.config.controllers[1].set_address_and_default_values_for_zn(zn_object_dict=self.config.zones,
                                                                             zn_ad_range=self.config.zn_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.master_valve)
            self.config.controllers[1].set_address_and_default_values_for_mv(mv_object_dict=self.config.master_valves,
                                                                             mv_ad_range=self.config.mv_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.moisture_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ms(
                ms_object_dict=self.config.moisture_sensors,
                ms_ad_range=self.config.ms_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.temperature_sensor)
            self.config.controllers[1].set_address_and_default_values_for_ts(
                ts_object_dict=self.config.temperature_sensors,
                ts_ad_range=self.config.ts_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.event_switch)
            self.config.controllers[1].set_address_and_default_values_for_sw(sw_object_dict=self.config.event_switches,
                                                                             sw_ad_range=self.config.sw_ad_range)
            self.config.controllers[1].do_search_for_dv(dv_type=opcodes.flow_meter)
            self.config.controllers[1].set_address_and_default_values_for_fm(fm_object_dict=self.config.flow_meters,
                                                                             fm_ad_range=self.config.fm_ad_range)
            self.config.create_mainline_objects()
            self.config.create_3200_poc_objects()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        - Setup Programs: \n
            - Program 1: \n
                - Enabled is True \n
                - Max Concurrent Zones to 4 \n
                - Mainline 1 \n
            - Program 2: \n
                - Enabled is True \n
                - Max Concurrent Zones to 4 \n
                - Mainline 2 \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # program 1
            program_number_1_start_times = [480]
            self.config.programs[1] = PG3200(_ad=1,
                                             _en=opcodes.true,
                                             _pr=1,
                                             _mc=4,
                                             _sa=100,
                                             _ci=opcodes.week_days,
                                             _di=None,
                                             _sm=[],
                                             _st=program_number_1_start_times,
                                             _ml=1,
                                             _bp='')
            # program 2
            program_number_2_start_times = [480]
            self.config.programs[2] = PG3200(_ad=2,
                                             _en=opcodes.true,
                                             _pr=1,
                                             _mc=4,
                                             _sa=100,
                                             _ci=opcodes.week_days,
                                             _di=None,
                                             _sm=[],
                                             _st=program_number_2_start_times,
                                             _ml=2,
                                             _bp='')

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        - Set up zones on Programs so that both Programs (1 & 2) each have 1 primary zone and 1 linked zone. \n
        - Set up zones on Programs so that both Programs (1 & 2) each have 2 shared timed zones. \n
        - Set up Zone Programs:
            - Program 1:
                - Zone 1 (Primary Zone 1):
                - Zone 2 (Linked to Zone 1):
                - Zone 3 (Timed Zone):
                - Zone 4 (Timed Zone):
            - Program 2:
                - Zone 3 (Timed Zone):
                - Zone 4 (Timed Zone):
                - Zone 5 (Primary Zone 5):
                - Zone 6 (Linked to Zone 5):
        :return:
        :rtype:
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Program 1 Zone 1 Programs
            self.config.zone_programs[1] = ZoneProgram(zone_obj=self.config.zones[1],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=900,
                                                       _ct=300,
                                                       _so=300,
                                                       _pz=1)
            # Program 1 Zone 2 Programs
            self.config.zone_programs[2] = ZoneProgram(zone_obj=self.config.zones[2],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=900,
                                                       _ct=300,
                                                       _so=300,
                                                       _pz=1)
            # Program 1 Zone 3 Programs
            self.config.zone_programs[3] = ZoneProgram(zone_obj=self.config.zones[3],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=900,
                                                       _ct=300,
                                                       _so=300,
                                                       _ws=opcodes.timed)
            # Program 1 Zone 4 Programs
            self.config.zone_programs[4] = ZoneProgram(zone_obj=self.config.zones[4],
                                                       prog_obj=self.config.programs[1],
                                                       _rt=900,
                                                       _ct=300,
                                                       _so=300,
                                                       _ws=opcodes.timed)

            # Program 2 Zone 3 Programs
            self.config.zone_programs[5] = ZoneProgram(zone_obj=self.config.zones[3],
                                                       prog_obj=self.config.programs[2],
                                                       _rt=900,
                                                       _ct=300,
                                                       _so=300,
                                                        _ws=opcodes.timed)
            # Program 2 Zone 4 Programs
            self.config.zone_programs[6] = ZoneProgram(zone_obj=self.config.zones[4],
                                                       prog_obj=self.config.programs[2],
                                                       _rt=900,
                                                       _ct=300,
                                                       _so=300,
                                                        _ws=opcodes.timed)
            # Program 2 Zone 5 Programs
            self.config.zone_programs[7] = ZoneProgram(zone_obj=self.config.zones[5],
                                                       prog_obj=self.config.programs[2],
                                                       _rt=900,
                                                       _ct=300,
                                                       _so=300,
                                                       _pz=5)
            # Program 2 Zone 6 Programs
            self.config.zone_programs[8] = ZoneProgram(zone_obj=self.config.zones[6],
                                                       prog_obj=self.config.programs[2],
                                                       _rt=900,
                                                       _ct=300,
                                                       _so=300,
                                                       _pz=5)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        set up POC 1 \n
            enable POC 1 \n
            assign master valve 1 and flowmeter 1 to POC 1 \n
            assign POC 1 to main line 1 \n
        \n
        set up POC 2 \n
            enable POC 2 \n
            assign master valve 2 and flowmeter 2 to POC 2 \n
            assign POC 2 to main line 2 \n
        \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.poc[1] = POC3200(
                _ad=1,
                _en=opcodes.true,
                _mv=1,
                _fm=1,
                _ml=1
            )

            self.config.poc[2] = POC3200(
                _ad=2,
                _en=opcodes.true,
                _mv=2,
                _fm=2,
                _ml=2
            )

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        - Sets up all mainlines
        - Set up Mainline 1 \n
        - Set up Mainline 2 \n
        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        # here we can either execute the following uncommented lines in procedural fashion, or we could re-init the
        # object, would have to import Mainline at the top, effectively accomplishing the same thing by:
        try:
            # mainline 1
            self.config.mainlines[1] = Mainline(_ad=1,
                                                _ft=1,
                                                _fl=500)
            self.config.programs[1].set_mainline_on_cn(_ml_num=1)

            # mainline 2
            self.config.mainlines[2] = Mainline(_ad=2,
                                                _ft=2,
                                                _fl=50)
            self.config.programs[2].set_mainline_on_cn(_ml_num=2)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        We update the code version to v16 from v12 and increment the clock. \n
        We then call our method that converts the old test objects to the new ones. \n
        What converts:
            - We convert
            - Mainlines go from residing on programs to residing on zones. If a zone is attached to a program, we give
              that programs mainline to the zone after the update.
            - We split up every point of connection into a point of control and water source
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Connect to BM before incrementing clock forward so that the BM configuration is saved in the controller
            # backup
            self.config.basemanager_connection[1].set_ai_for_cn('104.130.246.18')  # Address of p2
            self.config.basemanager_connection[1].verify_cn_connected_to_bm()

            # Increment the clock so the controller has time to save all of its programming
            self.config.controllers[1].do_increment_clock(minutes=5)

            # # First, try using firmware image for BaseMgr_20 3200's (older hardware)
            # print("\n--\nTrying to update to v16 firmware using BaseMgr_20 ID: {0}\n--\n".format(
            #     self.BaseMgr_20_Firmware_Update_DB_ID
            # ))
            #
            # # Start clock to allow for the download
            # self.config.controllers[1].do_firmware_update(were_from=opcodes.basemanager,
            #                                               bm_id_number=self.BaseMgr_20_Firmware_Update_DB_ID)

            # After the update make sure we are back into sim mode with clock stopped (if the update failed, controller
            # comes back in sim mode but with the clock not stopped. The code below just helps us maintain a known
            # predictable state, even if the lines of code aren't needed)
            self.config.controllers[1].set_sim_mode_to_on()
            self.config.controllers[1].stop_clock()

            # If firmware update attempt failed (controller's firmware is still v12) then try the other firmware image
            # for "newer" hardware (BaseMgr_35)
            # if self.config.controllers[1].vr < opcodes.firmware_version_V16:
            #
            #     print("\n--\nFirst try failed, now trying to update to v16 firmware using BaseMgr_35 "
            #           "ID: {0}\n--\n".format(opcodes.latest_3200_basemanager_firmware_id))

                # Make sure controller is connected to BM (sometimes, at the moment in time after the FW update failed,
                # BaseManager connection is lost and re-established. This causes below to fail unless
                # self.config.basemanager_connection[1].set_ai_for_cn('104.130.246.18')  # Address of p2
                #                 # self.config.basemanager_connection[1].verify_cn_connected_to_bm()

                # Start clock to allow for the download
                # self.config.controllers[1].do_firmware_update(were_from=opcodes.basemanager,
                #                                               bm_id_number=opcodes.latest_3200_basemanager_firmware_id)

                # After the update make sure we are back into sim mode with clock stopped (this is to re-inforce keeping
                # the controller in a known state)
                # self.config.controllers[1].set_sim_mode_to_on()
                # self.config.controllers[1].stop_clock()

            # Convert v12 objects to v16 (including controllers)
            self.config.controllers[1].wait_for_controller_after_reboot()
            self.convert_v12_objects_to_v16_objects()

            self.v16_config.BaseStation3200[1].do_increment_clock(minutes=5)

            # If the firmware update still didn't work, bomb out instead of trying to convert objects
            self.config.controllers[1].get_data()
            self.config.controllers[1].vr = self.config.controllers[1].data.get_value_string_by_key('VR')
            if self.config.controllers[1].vr < opcodes.firmware_version_V16:
                e_msg = "Unable to update controller firmware from {0} to latest v16 firmware. Tried both BaseMgr_20 " \
                        "and BaseMgr_35 firmware images on P2.".format(self.config.controllers[1].vr)
                raise Exception(e_msg)

            # Now we can use `self.v16_config.BaseStation3200[1]` because we've converted the v12 controller to a v16
            self.v16_config.BaseStation3200[1].do_increment_clock(minutes=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        - Set the new mainlines in our objects, these will be our 'expected' results to compare against. \n
        - Our zones should have the mainlines of the program that they were first attached to. \n
        - If a zone was on two Programs, it should take the mainline of the first program that it was 'assigned' to. \n
            - Zone 1 should have mainline 1 from Program 1. \n
            - Zone 2 should have mainline 1 from Program 1. \n
            - Zone 3 should have mainline 1 from Program 1. \n
            - Zone 4 should have mainline 1 from Program 1. \n
            - Zone 5 should have mainline 2 from Program 2. \n
            - Zone 6 should have mainline 2 from Program 2. \n

        - Update the firmware version on all zone and program objects to be the same as the controller. \n
            - We do this so that our zones and Programs will have the correct functionality because they function
              differently depending on if the controller they reside on has a firmware version above V16. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            self.v16_config.BaseStation3200[1].get_data()
            self.v16_config.BaseStation3200[1].vr = self.v16_config.BaseStation3200[1].data.get_value_string_by_key('VR')

            # Update the zone mainline attribute to be the old program mainline attribute. We don't send anything to cn
            self.v16_config.BaseStation3200[1].zones[1].ml = self.config.programs[1].ml
            self.v16_config.BaseStation3200[1].zones[2].ml = self.config.programs[1].ml
            self.v16_config.BaseStation3200[1].zones[3].ml = self.config.programs[1].ml
            self.v16_config.BaseStation3200[1].zones[4].ml = self.config.programs[1].ml
            self.v16_config.BaseStation3200[1].zones[5].ml = self.config.programs[2].ml
            self.v16_config.BaseStation3200[1].zones[6].ml = self.config.programs[2].ml

            # Update the firmware in our objects so we can have the correct behavior decided by V16
            for zone in sorted(self.v16_config.BaseStation3200[1].zones.keys()):
                self.v16_config.BaseStation3200[1].zones[zone].vr = self.v16_config.BaseStation3200[1].vr

            self.v16_config.BaseStation3200[1].programs[1].cn_firmware_version = self.v16_config.BaseStation3200[1].vr
            self.v16_config.BaseStation3200[1].programs[2].cn_firmware_version = self.v16_config.BaseStation3200[1].vr

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        - Verify the full configuration again to make sure that our mainlines have been successfully taken from our
          Programs and attached to our zones instead. \n
        - Since we also updated the firmware version in our objects, Programs will no longer try to attempt to verify
          a mainline. Instead, zones will try to verify mainlines, and they should be equal to the ones we set them to
          in the step before. \n
        - The main things we want to verify are: \n
            - Programs still have the correct zones assigned to them. \n
                - Program 1: Zones 1, 2, 3, 4 \n
                - Program 2: Zones 3, 4, 5, 6 \n
            - Zones now have mainlines from the Programs they were first attached to. \n
                - Zones 1, 2, 3, 4: Mainline 1. \n
                - Zones 5, 6: Mainline 2. \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            # check everything
            # v16 code disables the mainline that are not being used during the conversion
            # we no longer disable the ml or pocs
            for index in range(3, 9):
                # disable the en in the object so we can check to see in the 3200 disabled the mainlines
                self.v16_config.BaseStation3200[1].mainlines[index].en = opcodes.false
            self.v16_config.BaseStation3200[1].verify_full_configuration()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        - Verify the messages don't show up for point of control and mainlines
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ####################\n"
        print method
        try:
            for index in range(1, 9):
                self.v16_config.BaseStation3200[1].points_of_control[index].messages.\
                    check_for_configuration_error_message_not_present()
            for index in range(1, 9):
                self.v16_config.BaseStation3200[1].mainlines[index].messages. \
                    check_for_configuration_error_message_not_present()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

