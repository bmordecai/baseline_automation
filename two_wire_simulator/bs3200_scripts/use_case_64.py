import sys
from time import sleep
from datetime import datetime, timedelta
from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.logging_handler import log_handler

from common import helper_methods

__author__ = 'Tige'


class ControllerUseCase64(object):
    """
    Test name:
        - CN UseCase64 Running zones and programs across mainlines
    User Story:
        - using a single controller

        - As a user I want to use moisture sensors on my property and maximize my hydraulics at the same time so that
        all of my water can be utilized effectivity. By maximizing my hydraulics I can also reduce the amount of time
        that the system waters because I will be running the maximum zone count per minute. But I also want to be able
        to conserve water when the moisture content of the soil dictates that I should not be watering. I also want to
        be able to know where the water is being distributed across the property

        - as a user i want to be able to put any zone on any program regardless of the mainline assignment so that I
        can make my programming model easier

    Coverage area of feature: \n
        This test covers primary and link zones across mainlines :
            primary zones and linked zone are not on the same mainline
            mainline wont to run in order

    Use Case explanation:


        First Scenario:
            WS 1                    WS 8
              |                       |
            CP 1                    CP 8
              |                       |
            ML 1                    ML 8
              |                       |
            PG 2                    PG 1
              |                       |
        ZN 11 = PR 11           ZN 1 = PR 1
        ZN 2 LK to PR 1         ZN 12 LK to PR 11
        ZN 3 LK to PR 1         ZN 13 LK to PR 11
        ZN 4 LK to PR 1         ZN 14 LK to PR 11

    """
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                     # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # this is set in the PG3200 object
            # TODO need to have concurrent zones per program added

            program_start_time = [480]

            program_water_window = ['111111111111111111111111']
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=10)
            # setup Program 1 on controller
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_water_window(_ww=program_water_window)
            self.config.BaseStation3200[1].programs[1].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[1].set_start_times(_st_list=program_start_time)
            self.config.BaseStation3200[1].programs[1].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=5)
            self.config.BaseStation3200[1].programs[1].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(_sun=True,
                                                                                                           _mon=True,
                                                                                                           _tues=True,
                                                                                                           _wed=True,
                                                                                                           _thurs=True,
                                                                                                           _fri=True,
                                                                                                           _sat=True)

            self.config.BaseStation3200[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation3200[1].programs[2].set_enabled()
            self.config.BaseStation3200[1].programs[2].set_water_window(_ww=program_water_window)
            self.config.BaseStation3200[1].programs[2].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[2].set_start_times(_st_list=program_start_time)
            self.config.BaseStation3200[1].programs[2].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[2].set_max_concurrent_zones(_number_of_zones=5)
            self.config.BaseStation3200[1].programs[2].set_seasonal_adjust(_percent=100)
            self.config.BaseStation3200[1].programs[2].set_watering_intervals_to_selected_days_of_the_week(_sun=True,
                                                                                                           _mon=True,
                                                                                                           _tues=True,
                                                                                                           _wed=True,
                                                                                                           _thurs=True,
                                                                                                           _fri=True,
                                                                                                           _sat=True)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # -------------------------------------------------------------------------------------------------------- #
            # Program 1 Zone Programs
            # -------------------------------------------------------------------------------------------------------- #
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=60)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=10)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=12)
            self.config.BaseStation3200[1].programs[1].zone_programs[12].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=13)
            self.config.BaseStation3200[1].programs[1].zone_programs[13].set_as_linked_zone(_primary_zone=1)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=14)
            self.config.BaseStation3200[1].programs[1].zone_programs[14].set_as_linked_zone(_primary_zone=1)

            # -------------------------------------------------------------------------------------------------------- #
            # Program 2 Zone Programs
            # -------------------------------------------------------------------------------------------------------- #
            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=11)
            self.config.BaseStation3200[1].programs[2].zone_programs[11].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[2].zone_programs[11].set_run_time(_minutes=60)
            self.config.BaseStation3200[1].programs[2].zone_programs[11].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[2].zone_programs[11].set_soak_time(_minutes=10)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[2].zone_programs[2].set_as_linked_zone(_primary_zone=11)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[2].zone_programs[3].set_as_linked_zone(_primary_zone=11)

            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[2].zone_programs[4].set_as_linked_zone(_primary_zone=11)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        ############################
        setup WaterSources Empty Conditions
        ############################
        Add empty condition with device ---> to water source \n
        - set up empty condition  Attributes \n
            - set enabled state \n
            - set device state \n
            - set wait time  \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # Water Source 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].set_priority(_priority_for_water_source=2)

            # Water Source 8
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=8)
            self.config.BaseStation3200[1].water_sources[8].set_enabled()
            self.config.BaseStation3200[1].water_sources[8].set_priority(_priority_for_water_source=2)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # Add & Configure POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_flow_meter_to_point_of_control(
                _flow_meter_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=40)
            self.config.BaseStation3200[1].points_of_control[1].set_high_flow_limit(_limit=500,
                                                                                    with_shutdown_enabled=False)

            # Add POC 1 to Water Source 1
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=1)

            # Add & Configure POC 8
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=8)
            self.config.BaseStation3200[1].points_of_control[8].set_enabled()
            self.config.BaseStation3200[1].points_of_control[8].add_master_valve_to_point_of_control(
                _master_valve_address=8)
            self.config.BaseStation3200[1].points_of_control[8].add_flow_meter_to_point_of_control(
                _flow_meter_address=8)
            self.config.BaseStation3200[1].points_of_control[8].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].points_of_control[8].set_high_flow_limit(_limit=500,
                                                                                    with_shutdown_enabled=False)

            # Add POC 8 to Water Source 8
            self.config.BaseStation3200[1].water_sources[8].add_point_of_control_to_water_source(
                _point_of_control_address=8)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control
        """

        # here we can either execute the following uncommented lines in procedural fashion, or we could re-init the
        # object, would have to import Mainline at the top, effectively accomplishing the same thing by:
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=40)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_true()

            # Add ML 1 to POC 1
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(
                _mainline_address=1)

            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=8)
            self.config.BaseStation3200[1].mainlines[8].set_pipe_stabilization_time(_minutes=1)
            self.config.BaseStation3200[1].mainlines[8].set_target_flow(_gpm=50)
            self.config.BaseStation3200[1].mainlines[8].set_limit_zones_by_flow_to_true()

            # Add ML 8 to POC 8
            self.config.BaseStation3200[1].points_of_control[8].add_mainline_to_point_of_control(
                _mainline_address=8)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ######################
        setup zones on mainlines
        ######################

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=1)
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=12)
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=13)
            self.config.BaseStation3200[1].mainlines[8].add_zone_to_mainline(_zone_address=14)

            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=11)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=2)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=3)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=4)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ######################
        Set design flow for each zone
        ######################
        - set up zone  Attributes associated with main lines \n
            - set design flow on zone
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].zones[1].set_design_flow(_gallons_per_minute=20)
            self.config.BaseStation3200[1].zones[12].set_design_flow(_gallons_per_minute=10)
            self.config.BaseStation3200[1].zones[13].set_design_flow(_gallons_per_minute=10)
            self.config.BaseStation3200[1].zones[14].set_design_flow(_gallons_per_minute=10)

            self.config.BaseStation3200[1].zones[11].set_design_flow(_gallons_per_minute=20)
            self.config.BaseStation3200[1].zones[2].set_design_flow(_gallons_per_minute=10)
            self.config.BaseStation3200[1].zones[3].set_design_flow(_gallons_per_minute=10)
            self.config.BaseStation3200[1].zones[4].set_design_flow(_gallons_per_minute=20)


        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        Verify that everything is in a done state before we start the test
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_date_and_time(_date='02/12/2017', _time='7:59:00')
            self.config.BaseStation3200[1].verify_date_and_time()
            
            for zone_address in sorted(self.config.BaseStation3200[1].zones.keys()):
                self.config.BaseStation3200[1].zones[zone_address].statuses.verify_status_is_done()
            
            for program in range(1, 3):
                self.config.BaseStation3200[1].programs[program].statuses.verify_status_is_done()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_10(self):
        """
        Mainline 1 can provide 40 GM
        Mainline 8 can provide 50 GM
        Mainline 1 will start first
        which will allow program 2 to start
        program 2 has primary zone 11 so it will run first
        next it will turn on the linked zone from primary zone 11 which is zone 2 and 3  which together use 20 GPM
        Primary zone 1 started which is using 20 gpm all water on mainline 1 is used
        now Mainline 8 has primary zone 1 on and linked zone 12, 13, 14

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # This should trigger the Programs to start watering
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)

            self.config.BaseStation3200[1].water_sources[8].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[8].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[8].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_watering()

            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_11(self):
        """
        increment the clock to past the first cycle times and verify transition of zones
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # This should trigger the Programs to start watering
            self.config.BaseStation3200[1].do_increment_clock(minutes=5)

            self.config.BaseStation3200[1].water_sources[8].statuses.verify_status_is_ok()
            self.config.BaseStation3200[1].points_of_control[8].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].mainlines[8].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[12].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[13].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[14].statuses.verify_status_is_soaking()

            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()

            self.config.BaseStation3200[1].zones[11].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]