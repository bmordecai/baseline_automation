import sys
from time import sleep
from datetime import timedelta, datetime
from decimal import Decimal

from common.configuration import Configuration
from common import object_factory
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# Bicoder imports
from common.objects.bicoders.valve_bicoder import ValveBicoder
from common.objects.bicoders.flow_bicoder import FlowBicoder
from common.objects.bicoders.pump_bicoder import PumpBicoder
from common.objects.bicoders.switch_bicoder import SwitchBicoder
from common.objects.bicoders.moisture_bicoder import MoistureBicoder
from common.objects.bicoders.temp_bicoder import TempBicoder
from common.objects.bicoders.analog_bicoder import AnalogBicoder

# Device imports
from common.objects.devices.zn import Zone
from common.objects.devices.mv import MasterValve
from common.objects.devices.pm import Pump
from common.objects.devices.ms import MoistureSensor
from common.objects.devices.fm import FlowMeter
from common.objects.devices.sw import EventSwitch
from common.objects.devices.ts import TemperatureSensor
from common.objects.devices.ps import PressureSensor

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

__author__ = "Eldin/Time"


class ControllerUseCase62(object):
    """
    Test name:
        Patterned after use case 52
        Ignore global conditions - real devices
        
    Purpose:
        Purpose of this use case is verify valve operation for ignore global conditions.  The valves are getting
        turned off even though the zone status indicates watering.

    User Story:
        1.  As a user I want to have some programs ignore global pause and stop conditions.

        2.  When I set a global pause condition, programs that have ignore global conditions set to true should
            continue to run their zones, and keep their valves on, through out the pause condition.

        3.  When I set a global stop condition, programs that have ignore global conditions set to true should
            continue to run their zones, and keep their valves on, through out the stop condition.

        4.  On all global conditions, when programs are ignoring the condition, the master valve on the POC should
            to be on to supply water - it should not turn off for 15 seconds and then back on.

    Coverage area of feature:
        1.  Event switch used as global stop condition
            
    Not Covered: \n
        1.  Pressure Sensors
        2.  Moisture sensors
        3.  Coach's Button
        4.  Event days
        5.  Temperature sensor
        6.  Global Pause condition

    Real-Devices to be configured by default for test: \n
        - Zones \n
            - TSD0001 (1) \n
            - TSD0002 (2) \n
            - TSD0003 (3) \n
            - TSD0004 (4) \n

        - Master Valves
            - TSD0007 (1)

        - Event Switches \n
            - TPD0001 (1) \n

    Use case explanation:
        0:  General settings
            a)  controller zone concurrent count = 8
            b)  Default SW1-->POC1-->ML1

        1.  Scenario 1 - event switch stop condition
            a)  search and assign zones, master valve, and event switch
            b)  four zones on ML1, 30 minute run time
            c)  two programs, each with two zones, zone concurrent count = 2
            d)  event switch set as global stop condition when OPEN
            e)  set event switch to CLOSED
            f)  start both programs and run for one minute
            g)  verify zones and master valve on
            h)  set switch to OPEN and run one minute
            i)  verify two zones set to done, two zones and master valve running
            j)  run three minutes.
            k)  verify two zones set to done, two zones and master valve running
            l)  stop program and clear message

        2.  Scenario 2 - event switch pause condition
            a)  continue with previous programming
            b)  remove event switch as global STOP condition
            d)  set event switch as global PAUSE condition when OPEN, with two minute delay
            e)  set event switch to CLOSED
            f)  start both programs and run for one minute
            g)  verify zones and master valve on
            h)  set switch to OPEN and run one minute
            i)  verify two zones set to paused, two zones and master valve running
            j)  run three minutes.
            k)  verify all zones and master valve running
            l)  stop program

    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)
            
    #################################
    def step_1(self):
        """
        ############################
        Configure Controller
        ############################
        
        Configure the controller to use real devices in real-time mode.
        """
        helper_methods.print_method_name()
        try:
            self.config.BaseStation3200[1].turn_off_faux_io()
            self.config.BaseStation3200[1].set_sim_mode_to_off()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        Configure Zones
        ############################

        1. Create Zones
            - For each Zone:
                1) Create bicoder
                2) Create Zone
                3) Update bicoder attributes (with real values returned by controller) for verifying against
        2. Search and address Zones

        --------------------------------------------------
        | Address | Serial  | 2-Wire | Voltage | Current |
        --------------------------------------------------
        |    1    | TSD0001 | 0.9    | 28.2    | 0.20    |
        |    2    | TSD0002 | 0.9    | 28.2    | 0.20    |
        |    3    | TSD0003 | 0.9    | 28.2    | 0.20    |
        |    4    | TSD0004 | 0.9    | 28.2    | 0.20    |
        --------------------------------------------------
        """
        helper_methods.print_method_name()
        try:
            # TODO - Configure Zones for test:
            #
            #   Steps:
            #       (1) Update test zone 1, 100 and 200's serial numbers to use
            #           (i.e., zone_1_serial_number, zone_100_serial_number, zone_200_serial_number)

            # Tim's values
            # zone_1_serial_number = 'D020259'
            # zone_2_serial_number = 'D020260'
            # zone_3_serial_number = 'D020261'
            # zone_4_serial_number = 'D020262'
            # Values at Eldin's controller
            zone_1_serial_number = 'TSQ0011'
            zone_2_serial_number = 'TSQ0012'
            zone_3_serial_number = 'TSQ0013'
            zone_4_serial_number = 'TSQ0014'

            # ----------------
            # Search for Devices
            # ----------------
            self.config.BaseStation3200[1].do_search_for_zones()

            # ----------------
            # ZONE 1
            # ----------------

            zone_1_bicoder = ValveBicoder(
                _sn=zone_1_serial_number,
                _controller=self.config.BaseStation3200[1],
                _id=opcodes.zone,
                _decoder_type=opcodes.single_valve_decoder)
            zone_1 = Zone(
                _controller=self.config.BaseStation3200[1],
                _address=1,
                _valve_bicoder=zone_1_bicoder)

            self.config.BaseStation3200[1].valve_bicoders[zone_1_serial_number] = zone_1_bicoder
            self.config.BaseStation3200[1].zones[1] = zone_1

            # Verify valve biCoder found on Two-Wire (device address = 0 expected)
            self.config.BaseStation3200[1].valve_bicoders[zone_1_serial_number].verify_device_address()

            # Address biCoder as Zone 1
            self.config.BaseStation3200[1].zones[1].set_address(_ad=1)

            # Verify address assigned to Zone 1
            self.config.BaseStation3200[1].zones[1].bicoder.verify_device_address()

            # Do self test on Zone 1
            self.config.BaseStation3200[1].zones[1].bicoder.do_self_test()

            # ----------------
            # ZONE 2
            # ----------------

            zone_2_bicoder = ValveBicoder(
                _sn=zone_2_serial_number,
                _controller=self.config.BaseStation3200[1],
                _id=opcodes.zone,
                _decoder_type=opcodes.single_valve_decoder)
            zone_2 = Zone(
                _controller=self.config.BaseStation3200[1],
                _address=2,
                _valve_bicoder=zone_2_bicoder)

            self.config.BaseStation3200[1].valve_bicoders[zone_2_serial_number] = zone_2_bicoder
            self.config.BaseStation3200[1].zones[2] = zone_2

            # Verify valve biCoder found on Two-Wire (device address = 0 expected)
            self.config.BaseStation3200[1].valve_bicoders[zone_2_serial_number].verify_device_address()

            # Address biCoder as Zone 2
            self.config.BaseStation3200[1].zones[2].set_address(_ad=2)

            # Verify address assigned to Zone 2
            self.config.BaseStation3200[1].zones[2].bicoder.verify_device_address()

            # Do self test on Zone 2
            self.config.BaseStation3200[1].zones[2].bicoder.do_self_test()

            # ----------------
            # ZONE 3
            # ----------------

            zone_3_bicoder = ValveBicoder(
                _sn=zone_3_serial_number,
                _controller=self.config.BaseStation3200[1],
                _id=opcodes.zone,
                _decoder_type=opcodes.single_valve_decoder)
            zone_3 = Zone(
                _controller=self.config.BaseStation3200[1],
                _address=3,
                _valve_bicoder=zone_3_bicoder)

            self.config.BaseStation3200[1].valve_bicoders[zone_3_serial_number] = zone_3_bicoder
            self.config.BaseStation3200[1].zones[3] = zone_3

            # Verify valve biCoder found on Two-Wire (device address = 0 expected)
            self.config.BaseStation3200[1].valve_bicoders[zone_3_serial_number].verify_device_address()

            # Address biCoder as Zone 3
            self.config.BaseStation3200[1].zones[3].set_address(_ad=3)

            # Verify address assigned to Zone 3
            self.config.BaseStation3200[1].zones[3].bicoder.verify_device_address()

            # Do self test on Zone 3
            self.config.BaseStation3200[1].zones[3].bicoder.do_self_test()

            # ----------------
            # ZONE 4
            # ----------------

            zone_4_bicoder = ValveBicoder(
                _sn=zone_4_serial_number,
                _controller=self.config.BaseStation3200[1],
                _id=opcodes.zone,
                _decoder_type=opcodes.single_valve_decoder)
            zone_4 = Zone(
                _controller=self.config.BaseStation3200[1],
                _address=3,
                _valve_bicoder=zone_4_bicoder)

            self.config.BaseStation3200[1].valve_bicoders[zone_4_serial_number] = zone_4_bicoder
            self.config.BaseStation3200[1].zones[4] = zone_4

            # Verify valve biCoder found on Two-Wire (device address = 0 expected)
            self.config.BaseStation3200[1].valve_bicoders[zone_4_serial_number].verify_device_address()

            # Address biCoder as Zone 4
            self.config.BaseStation3200[1].zones[4].set_address(_ad=4)

            # Verify address assigned to Zone 4
            self.config.BaseStation3200[1].zones[4].bicoder.verify_device_address()

            # Do self test on Zone 4
            self.config.BaseStation3200[1].zones[4].bicoder.do_self_test()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        Configure Master Valves
        ############################

        1. Create Master Valves
            - For each Master Valve:
                1) Create bicoder
                2) Create Master Valve
                3) Update bicoder attributes (with real values returned by controller) for verifying against
        2. Search and address Master Valves

        --------------------------------------------------
        | Address | Serial  | 2-Wire | Voltage | Current |
        --------------------------------------------------
        |    1    | D004048 | 1.2    | 28.8    | 0.21    |
        --------------------------------------------------
        """
        helper_methods.print_method_name()
        try:
            # TODO - Configure Master Valves for test:
            #
            #   Steps:
            #       (1) Update test master valve 1 & 8's serial numbers to use
            #           (i.e., mv_1_serial_number, mv_8_serial_number)

            # Tim's values
            # mv_1_serial_number = 'D004048'
            # Values at Eldin's controller
            mv_1_serial_number = 'TMV0004'

            # ----------------
            # MASTER VALVE 1
            # ----------------

            mv_1_bicoder = ValveBicoder(
                _sn=mv_1_serial_number,
                _controller=self.config.BaseStation3200[1],
                _id=opcodes.master_valve,
                _decoder_type=opcodes.single_valve_decoder)
            mv_1 = MasterValve(
                _controller=self.config.BaseStation3200[1],
                _address=1,
                _valve_bicoder=mv_1_bicoder)

            self.config.BaseStation3200[1].valve_bicoders[mv_1_serial_number] = mv_1_bicoder
            self.config.BaseStation3200[1].master_valves[1] = mv_1

            # Verify valve biCoder found on Two-Wire (device address = 0 expected)
            self.config.BaseStation3200[1].valve_bicoders[mv_1_serial_number].verify_device_address()

            # Address biCoder as Master Valve 1
            self.config.BaseStation3200[1].master_valves[1].set_address(_ad=1)

            # Verify address assigned to Master Valve 1
            self.config.BaseStation3200[1].master_valves[1].bicoder.verify_device_address()

            # Do self test on Master Valve 1
            self.config.BaseStation3200[1].master_valves[1].bicoder.do_self_test()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        Configure Event Switches
        ############################

        1. Create Event Switches
            - For each Event Switch:
                1) Create bicoder
                2) Create Event Switch
                3) Update bicoder attributes (with real values returned by controller) for verifying against
        2. Search and address Event Switches

        ----------------------------------------------
        | Address | Serial  | 2-Wire | Contact State |
        ----------------------------------------------
        |    1    | RP00371 |   0.9  | Closed        |
        ----------------------------------------------

        """
        helper_methods.print_method_name()
        try:
            # Tim's values
            # sw_1_serial_number = 'RP00371'
            # Values at Eldin's controller
            sw_1_serial_number = 'TPD0001'

            # ----------------
            # EVENT SWITCH 1
            # ----------------

            sw_1_bicoder = SwitchBicoder(
                _sn=sw_1_serial_number,
                _controller=self.config.BaseStation3200[1])
            sw_1 = EventSwitch(
                _controller=self.config.BaseStation3200[1],
                _address=1,
                _switch_bicoder=sw_1_bicoder)

            self.config.BaseStation3200[1].switch_bicoders[sw_1_serial_number] = sw_1_bicoder
            self.config.BaseStation3200[1].event_switches[1] = sw_1

            # Verify switch biCoder found on Two-Wire (device address = 0 expected)
            self.config.BaseStation3200[1].switch_bicoders[sw_1_serial_number].verify_device_address()

            # Address biCoder as Event Switch 1
            self.config.BaseStation3200[1].event_switches[1].set_address(_ad=1)

            # Verify address assigned to Event Switch 1
            self.config.BaseStation3200[1].event_switches[1].bicoder.verify_device_address()

            # Do self test on Event Switch 1
            self.config.BaseStation3200[1].event_switches[1].bicoder.do_self_test()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        Configure Controller
        ############################

        1.  Global Stop condition
            a)  set event switch as stop on OPEN

        2.  Concurrent zone count = 8

        """
        helper_methods.print_method_name()
        try:
            # concurrent zone count
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=8)

            # global stop on event switch open
            self.config.BaseStation3200[1].add_switch_stop_condition(_event_switch_address=1)
            self.config.BaseStation3200[1].event_switch_stop_conditions[1].set_switch_mode_to_open()

            # sim time enabled
            self.config.BaseStation3200[1].set_sim_mode_to_on()
            self.config.BaseStation3200[1].stop_clock()
            self.config.BaseStation3200[1].set_date_and_time(_date='1/1/2018', _time='10:00:00')

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ############################
        Configure Programs
        ############################

        1.  Program 1:
            a)  Zones 1 and 2 with 30 minute run times
            b)  Concurrent zone count = 4
            c)  Ignore global conditions = TRUE

        2.  Program 2:
            a)  Zones 3 and 4 with 30 minute run times
            b)  Concurrent zone count = 4
            c)  Ignore global conditions = FALSE

        """
        helper_methods.print_method_name()
        try:
            # Add and configure Program 1
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation3200[1].programs[1].set_ignore_global_conditions()
            # Setup zones on the program
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=30)
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_run_time(_minutes=30)

            # Add and configure Program 2
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation3200[1].programs[2].set_enabled()
            self.config.BaseStation3200[1].programs[2].set_max_concurrent_zones(_number_of_zones=4)
            self.config.BaseStation3200[1].programs[2].set_to_obey_global_conditions()
            # Setup Zones on the program
            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=3)
            self.config.BaseStation3200[1].programs[2].zone_programs[3].set_run_time(_minutes=30)
            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=4)
            self.config.BaseStation3200[1].programs[2].zone_programs[4].set_run_time(_minutes=30)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ############################
        Start programs
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # start both programs
            self.config.BaseStation3200[1].programs[1].set_program_to_start()
            self.config.BaseStation3200[1].programs[2].set_program_to_start()

            # verify starting
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ############################
        Advance minutes with break points and verify operation
        ############################

        Rules to follow:
            1.  Set break points at indicated lines before running
            2.  Manually change devices as needed for each step
            3.  Continue running for each step until complete

        Steps:
            1.  verify running zones and master valve after first minute
            2.  open event switch to make a global stop condition
            3.  run one minute
                a.  program 2 is shutdown
                b.  zones 3 and 4 are done
                c.  master valve decocder is off - ERROR

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            ########## BREAKPOINT ##########
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)
            ####################################################
            # TODO manual action:
            # TODO  - CLOSE event switch
            ####################################################

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].statuses.verify_status_is_running()

            ########## BREAKPOINT ##########

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            ####################################################
            # TODO manual action:
            # TODO   - OPEN event switch
            ####################################################

            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            # self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()     # needs to be ON
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()     # needs to be ON
            self.config.BaseStation3200[1].statuses.verify_status_is_running()

            ########## BREAKPOINT ##########
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            ####################################################
            # TODO manual action:
            # TODO   - OPEN event switch
            ####################################################

            # NOTE:  the sleeps are to let the decoders time out (3+ minutes) which turns them off.
            sleep(60)
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            sleep(60)
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            sleep(60)
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            sleep(60)
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].statuses.verify_status_is_running()

            ########## BREAKPOINT ##########
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            ####################################################
            # TODO manual action:
            # TODO   - verify zone 1 & 2 decoders are ON
            # TODO   - master valve decoder is ON
            # TODO   - CLOSE event switch
            ####################################################

            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[1].set_program_to_stop()

            self.config.BaseStation3200[1].set_sim_mode_to_off()


        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

