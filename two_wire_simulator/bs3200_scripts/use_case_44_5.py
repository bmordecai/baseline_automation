import sys
from time import sleep

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods

__author__ = 'Tige'


class ControllerUseCase44_5(object):
    """
    Test name:
        - CN UseCase44_5 Mainline pressure delay before starting zones

    Purpose:
        - Set up a full configuration on the controller
            - Check for correct watering behavior using timed delays on a mainline.
                - Verify messages (if any)
                - Verify expected statuses

    Coverage area:
        - master valve turns on before zone do
        - all zones than water as expected


    ########################
    START OF v16 3200 SW ERS FOR Mainline-Zone DELAYS:
    ########################

    Zone Activation Process:

    1. At the beginning of each minute, all zones in the system are processed.  They are set to done, watering,
       soaking, and other states based on their state for the previous minute combined with the controller and
       program state changes.

    2. All zones that are watering will be kept in a watering state as long as the following conditions are met:

        a. The controller is not is a paused state (dial moved from RUN)
        b. The total number of concurrent valves does not exceed the maximum number for the controller
        c. The total number of program concurrent valves is not exceeded
        d. The program that started the zone is not in a paused state
        e. The zone is not in a flow fault state
        f. The mainline is enabled - if not enabled, then the zone will not run and will just stay in a waiting state?
        g. If the zone-mainline is local, then
            - The POCs supplying water to the mainline are not all stopped (flow faults or budget)
            - The mainline has water
        h. If the zone-mainline is assigned to a FlowStation, then
            - The water allocation is not exceeded by this zone
            - The water allocation interval is still valid
            - Or if fallback is active, the fallback water allocation is not exceeded by this zone

    3. Mainline based zone time and pressure delays:

        a. The user can now specify the following:
            - Time or pressure delay between POC and first zone(s)
                1. If zero, then no delay between POC turn on and first zone(s) turn on
            - The maximum number of zones to be turned on at a time.
                1. If zero, then defaults to controller maximum
                2. This zone count may be reduced by any of the following:
                    a. Controller maximum concurrent zone count
                    b. Program maximum concurrent zone count
                    c. R-series decoder maximum on count
                    d. Zone design flow
                    e. POC and ML design flow values
                    f. FlowStation allocation
            - Time or pressure delay between zone(s):  this is the minimum between turning zones on and the next turn
              zones on operation, or turning zones off and the next turn zones on operation.  This controls how fast
              zones get turned on, not how fast they get turned off.
                1. If zero, then no delay to next group of zones
                2. Examples:
                    a. The water and programming is set for five zones at once, with two zones at a time.  The first two
                       zones turn on and then there is a delay before the next two zones turn on.  Then there is a
                       delay before the 5th zone turns on.
                    b. There are five zones running and five zones waiting to run.  Three zones complete their cycle
                       time and are shut off.  There is a delay before the next two zones turn on.  Then another delay
                       before the 5th zone is turned on.
                    c. There are five zones running and they all finish running at the same time.  They are all turned
                       off (and the POC continues for the last delay).
            - Time or pressure delay between last zone(s) off and POC off.
                1. If zero, then there is no delay from the last zone turn off and the POC turn off.

        b. These delays will happen on 60 second boundaries.  The pressure is read before the top of minute and is then
           acted on at the top of minute:
            - When this is enabled, the following table defines the behavior:
                1. Program Starts
                    a. MV set to ON
                    b. Pump set to ON
                    c. Timed Delay:
                        - If 0, then zone(s) on immediately.
                        - Wait the number of minutes and then turn zone(s) on.
                    d. Pressure Delay:
                        - If 0, then zone(s) on immediately.
                        - Wait until the pressure is reached on ALL available  POCs, checking on minute boundaries, then
                          turn zone(s) on.
                        - If FlowStation, then get pressure from it (lowest of all POCs connected to mainline).
                2. Zone(s) turned on and more zone(s) are waiting to run (flow and concurrent count OK)
                    a. Timed Delay:
                        - Wait the "between zones" delay time, then turn on zone(s)
                    b. Pressure Delay:
                        - Check the pressure on ALL available POCs each minute and turn zone(s) on when the "between
                          zones" pressure is reached.
                3. Zone(s) turned off and more zone(s) are waiting to run (flow and concurrent count OK)
                    a. Timed Delay:
                        - Wait the "between zones" delay time when turn on zone(s).
                    b. Pressure Delay:
                        - Check the pressure on ALL available POCs each minute and turn zone(s) on when the "between
                          zones" pressure is reached.
                4. All zones are turned off with no zones waiting to water.
                    a. Timed Delay:
                        - Wait the final delay time and turn POC off (normally closed MV and Pump).
                    b. Pressure Delay:
                        - Check the pressure on ALL available POCs each minute and turn POC off when the pressure is
                          reached.

        c.	Watering will be mainline centric (was program centric), happy path operation:
            - There are the following key lists in each mainline:
                1.	Zones:      all zones assigned to the mainline.
                2.	Waiting:    zones waiting to water.
                3.	Active:     zones actively watering.
                4.	Soaking:    zones soaking that have not finished their soak cycle time.
                5.	Paused:     zones that are paused
                6.	Ready:      zones that have finished their soak times and are ready to go again
            - Zone management lists:
                1.	{done} -> {waiting}	    program start
                2.	{waiting} -> {active}	zone count, water available
                3.	{active} -> {soaking}	cycle time done
                4.	{soaking} -> {ready}	soak complete
                5.	{ready} -> {active}	    zone count, water availability
                6.	{active} -> {paused}	pause condition, manual run, learn flow, priority
                7.	{paused} -> {ready}	    pause condition off
                8.	{active} -> {done}		run time complete, stop condition
            - When a program starts its zones will be put into a mainline list of zones waiting to water.
            - The POCs will be activated and any booster pumps for the program will be activated.
            - After the first delay time, some number of waiting zones will be moved to the active zones list, and they
              will be turned on.  This will also start a next delay timer.
            - After the next delay, additional zones may also be started.
            - When active zones complete their cycle time, they are moved to the soaking zones list.  The next timer
              is started and the zones are turned off.
            - When soaking zones complete their soak times, they are moved to the zones ready to water list.
            - If a pause condition happens, zones are moved from the active list to the paused list.
            - When the pause condition expires, zones are moved from the paused list to the ready list.
            - When an active zone completes it run time, it is moved back to the general zones list.

    ########################
    END OF v16 3200 SW ERS FOR TIMED DELAYS:
    ########################

    Test Configuration setup: \n
        1. Zone On Delay: 2 minutes
        2. Between Zone Delay: 1 minute
        3. Zone off Delay: 2 minutes
        4. Number of Zones to turn on at once: 2
        5. Controller concurrent zones: 15
        5. Configuration:
            - WS 1 ---> POC 1 ---> ML 1 \n
                - POC 1
                    - FM 1
                    - MV 1
                    - PM 1
                - ML 1 (Timed Delays)
                    - Zone On Delay: 2 minutes
                    - ZN 1
                    - ZN 2
                    - ZN 3
                    - ZN 4
                    - ZN 5
                - PG 1
                    - Concurrent Zones: 5

    """

    ###############################
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase44' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    ###############################
    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """
        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()
                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        setup controller concurrency.
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=15)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_2(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=5)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_3(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add zone programs to Program 1
            for zone_ad in range(1, 6):
                self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=zone_ad)
                self.config.BaseStation3200[1].programs[1].zone_programs[zone_ad].set_run_time(_minutes=8)
                self.config.BaseStation3200[1].programs[1].zone_programs[zone_ad].set_cycle_time(_minutes=4)
                self.config.BaseStation3200[1].programs[1].zone_programs[zone_ad].set_soak_time(_minutes=2)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_4(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        set_water Source \n
         """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Water Source 1
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[1].set_monthly_watering_budget(_budget=10000,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].water_sources[1].set_water_rationing_to_enabled()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n

        """

        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_pump_to_point_of_control(
                _pump_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_flow_meter_to_point_of_control(
                _flow_meter_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_pressure_sensor_to_point_of_control(
                _pressure_sensor_address=1)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control
        set_mainlines_3200 \n
            - set up main line 1 \n
                - set limit zones by flow to true \n
                - set the pipe fill time to 4 minutes \n
                - set the target flow to 500 \n
                - set the high variance limit to 5% and enable the high variance shut down \n
                - set the low variance limit to 20% and enable the low variance shut down \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Mainline 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_enabled()
            # Mainline 2 Pressure Delays
            self.config.BaseStation3200[1].mainlines[1].set_pressure_delay_before_first_zone(_psi=75.0)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Add Zones 1-5 to Mainline 1
            for zone_address in range(1, 6):
                self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=zone_address)
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_8(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_9(self):
        """
        ###############################
        Program 1 - Verify initial statuses - 7:59am
        ###############################

        Timed Delays Table:

            Time  | WS 1 | PC 1 | MV 1 | PM 1 | ML 1 | ZN 1 | ZN 2 | ZN 3 | ZN 4 | ZN 5 | PG 1 |
            ------------------------------------------------------------------------------------
            7:59a |  OF  |  OF  |  OF  |  OF  |  OF  |  DN  |  DN  |  DN  |  DN  |  DN  |  DN  |
            8:00a |  OF  |  OF  |  OF  |  OF  |  OF  |  DN  |  DN  |  DN  |  DN  |  DN  |  DN  |
            8:01a |  RN  |  RN  |  WT  |  WT  |  RN  |  WA  |  WA  |  WA  |  WA  |  WA  |  WA  |
            8:02a |  RN  |  RN  |  WT  |  WT  |  RN  |  WA  |  WA  |  WA  |  WA  |  WA  |  WA  |
            8:03a |  RN  |  RN  |  WT  |  WT  |  RN  |  WT  |  WT  |  WT  |  WT  |  WT  |  RN  |
            8:04a |  RN  |  RN  |  WT  |  WT  |  RN  |  WT  |  WT  |  WT  |  WT  |  WT  |  RN  |
            8:05a |  RN  |  RN  |  WT  |  WT  |  RN  |  WT  |  WT  |  WT  |  WT  |  WT  |  RN  |
            8:06a |  RN  |  RN  |  WT  |  WT  |  RN  |  WT  |  WT  |  WT  |  WT  |  WT  |  RN  |
            8:07a |  OF  |  OF  |  OF  |  OF  |  OF  |  SO  |  SO  |  SO  |  SO  |  SO  |  WA  |
            8:08a |  OF  |  OF  |  OF  |  OF  |  OF  |  SO  |  SO  |  SO  |  SO  |  SO  |  WA  |
            8:09a |  RN  |  RN  |  WT  |  WT  |  RN  |  WT  |  WT  |  WT  |  WT  |  WT  |  RN  |
            8:10a |  RN  |  RN  |  WT  |  WT  |  RN  |  WT  |  WT  |  WT  |  WT  |  WT  |  RN  |
            8:11a |  RN  |  RN  |  WT  |  WT  |  RN  |  WT  |  WT  |  WT  |  WT  |  WT  |  RN  |
            8:12a |  RN  |  RN  |  WT  |  WT  |  RN  |  WT  |  WT  |  WT  |  WT  |  WT  |  RN  |
            8:13a |  OF  |  OF  |  OF  |  OF  |  OF  |  DN  |  DN  |  DN  |  DN  |  DN  |  DN  |

        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set time to 7:59a
            self.config.BaseStation3200[1].set_date_and_time(_date='12/18/2017', _time='7:59:00')

            # ----> Time 8:00a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.set_pressure_value_in_psi(_psi=25)
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.self_test_and_update_object_attributes()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_off()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()


            # ----> Time 8:01a <--------#
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.set_pressure_value_in_psi(_psi=50)
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.self_test_and_update_object_attributes()

            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            # ----> Time 8:02a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()

            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.set_pressure_value_in_psi(_psi=76)
            self.config.BaseStation3200[1].pressure_sensors[1].bicoder.self_test_and_update_object_attributes()

            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_waiting_to_water()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_waiting_to_water()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            # ----> Time 8:03a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            #verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            #verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ----> Time 8:07a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_off()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_soaking()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            # ----> Time 8:08a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_off()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_soaking()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_soaking()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_waiting_to_run()

            # ----> Time 8:09a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=1)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_running()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_watering()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_running()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_watering()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()

            # ----> Time 8:013a <--------#
            self.config.BaseStation3200[1].do_increment_clock(minutes=4)
            self.config.BaseStation3200[1].verify_date_and_time()
            # verfiy water source
            self.config.BaseStation3200[1].water_sources[1].statuses.verify_status_is_ok()
            # verify points of control
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].pumps[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].master_valves[1].statuses.verify_status_is_off()
            # verify mainline
            self.config.BaseStation3200[1].mainlines[1].statuses.verify_status_is_off()
            # verify zones
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[3].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[4].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[5].statuses.verify_status_is_done()
            # verify Program
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                     self.config.test_name,
                     sys._getframe().f_code.co_name,
                     date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                     str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

