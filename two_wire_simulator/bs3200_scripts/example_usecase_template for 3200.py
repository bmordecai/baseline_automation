import sys
from datetime import timedelta, datetime, date
import time

# import common.product as helper_methods
from common.configuration import Configuration

# import log_handler for logging functionality
from common.logging_handler import log_handler

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
from common.imports import opcodes, csv
from common import helper_methods

__author__ = 'bens'


class UseCaseExample(object):
    """
    Test name:
        - 

    Multi-Controller Setup Guide:
    
    Purpose:
        - 
    
    Coverage:
        After establishing connection between 3200 and Substation initially,
        
        1. 
        2.
        ...
            
    IMPORTANT NOTES FOR CURRENT RESULTS FOR THIS TEST:
        - 
        
    Controller Programming:
        - Zones:                0   (0-local, 0-shared) 
        - Master Valves:        0   (0-local, 0-shared)
        - Event Switches:       0   (0-local, 0-shared)
        - Temp Sensors:         0   (0-local, 0-shared)
        - Moisture Sensors:     0   (0-local, 0-shared)
        - Flow Meters:          0   (0-local, 0-shared)
        - Programs:             0
        - Zone Programs:        0
        - Mainlines:            0
        - Pocs:                 0
        
    Substation Programming:
        - Valve BiCoders:       0
        - Moisture BiCoders:    0   
        - Temperature BiCoders: 0
        - Switch BiCoders:      0 
        - Flow BiCoders:        0
        - Pump BiCoders:        0

    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        """
        try:
            # Resets objects to a known state, creates serial connections, creates all objects.
            self.config.initialize_for_test(connect_to_basemanager=True)

            # get list of all the steps by function name in the use case
            method_list = [func for func in dir(self) if
                           callable(getattr(self, func)) and func.startswith('step')]
            # sort list in numerical order of numbers in steps step names must be 'step_X'
            sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
            # run each step_1,2,3 esc.
            for method in sorted_new_list:
                getattr(self, method)()
            helper_methods.print_test_passed(test_name=self.config.test_name)
        except Exception as e:
            helper_methods.print_test_failed(test_name=self.config.test_name)
            # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
            # to the next use case in the list
            if log_handler.is_enabled():
                log_handler.exception(message=e.message)
            else:
                raise

        # Handle successful test
        else:
            helper_methods.print_test_passed(test_name=self.config.test_name)

        # Cleanup after Pass or Fail
        finally:
            helper_methods.end_multiple_controller_test(config_object=self.config)

            ###############################

    def step_1(self):
        """
        ############################
        setup programs
        ############################
        Add program -----> to controller
        - set up program  Attributes \n
            - set enabled state  \n
            - set water window\n
            - set start times \n
            - set priority \n
            - set seasonal adjust \n
            - set watering intervals \n
        if not going to set the zones to use flow for concurrency \n
            - Set max concurrent zones for the program \n
        if using a booster bump \n
            - set a master device to be a booster pump  \n
            - Add master device -----> to program \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # this is set in the PG3200 object
            # TODO need to have concurrent zones per program added

            program_number_1_start_times = [480, 540, 600, 660]

            program_number_1_water_windows = ['011111100001111111111110']
            program_number_99_water_windows = ['011111100000111111111110',
                                               '011111100001111111111111',
                                               '011111100001111111111110',
                                               '011111100001111111111110',
                                               '011111100001111111111110',
                                               '011111100001111111111110',
                                               '011111100001111111111111']
            # setup Program 1 on controller
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_water_window(_ww=program_number_1_water_windows)
            self.config.BaseStation3200[1].programs[1].set_start_times_on_cn(_st_list=program_number_1_start_times)
            self.config.BaseStation3200[1].programs[1].set_priority_level(_pr_level=1)
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=1)
            self.config.BaseStation3200[1].programs[1].set_seasonal_adjust_on_cn(_percent=100)
            self.config.BaseStation3200[1].programs[1].set_watering_intervals_to_selected_days_of_the_week(_sun=False,
                                                                                                           _mon=True,
                                                                                                           _tues=False,
                                                                                                           _wed=True,
                                                                                                           _thurs=False,
                                                                                                           _fri=True,
                                                                                                           _sat=False)

            # make master valve two a booster so it can be assigned to a program
            self.config.BaseStation3200[1].master_valves[2].set_booster_enable_state(booster_state=opcodes.true)
            # assign the booster to the program
            self.config.BaseStation3200[1].programs[99].add_booster_pump(_mv_address=2)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

            ###############################

    def step_2(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Zone Programs
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_as_primary_zone()
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=15)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=5)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=5)

            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[1].zone_programs[2].set_as_linked_zone(_primary_zone=1,
                                                                                           _tracking_ratio=50)


        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

            ###############################

    def step_3(self):
        """
        ############################
        setup WaterSources
        ############################
        Add water sources -----> to controlLer
        - set up water source  Attributes \n
            - set enable state \n
            - set priority \n
            - set water budget \n
            - set water rationing state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].water_sources[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].set_priority(_priority_for_water_source=2)
            self.config.BaseStation3200[1].water_sources[1].set_monthly_watering_budget(_budget=10000,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].water_sources[1].set_water_rationing_to_enabled()

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

            ###############################

    def step_4(self):
        """
        ############################
        setup WaterSources Empty Conditions
        ############################
        Add empty condition with device ---> to water source \n
        - set up empty condition  Attributes \n
            - set enabled state \n
            - set device state \n
            - set wait time  \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:

            # add empty condition to the water source
            self.config.BaseStation3200[1].water_sources[1].add_switch_empty_condition(_event_switch_address=1)
            self.config.BaseStation3200[1].water_sources[1].switch_empty_conditions[1].set_enabled()
            self.config.BaseStation3200[1].water_sources[1].switch_empty_conditions[1] \
                .set_switch_empty_condition_to_closed()
            self.config.BaseStation3200[1].water_sources[1].switch_empty_conditions[1].set_empty_wait_time(
                _minutes=2)


        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

            ###############################

    def step_5(self):
        """
        ############################
        setup Point of Controls
        ############################
        Add Points of Control -----> to controlLer
        Add Points of Control -----> To Water Source
        - set up points of control Attributes \n
            - set enable state \n
            - set target flow \n
            - set high flow limit with shut down state \n
            - set unscheduled flow limit with shut down state \n
            - set high pressure limit with shut down state \n
            - set low pressure limit with shut down state \n
        - Add flow meters ---> to point of control \n
        - Add pump ---> to point of control \n
        - Add master valve  ---> to point of control \n
        - Add pressure sensor  ---> to point of control \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            # add point of control to water source
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(
                _point_of_control_address=1)

            self.config.BaseStation3200[1].points_of_control[1].set_enabled()
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].points_of_control[1].set_high_flow_limit(_limit=550,
                                                                                    with_shutdown_enabled=True)
            self.config.BaseStation3200[1].points_of_control[1].set_unscheduled_flow_limit(_gallons=10,
                                                                                           with_shutdown_enabled=True)
            self.config.BaseStation3200[1].points_of_control[1].set_high_pressure_limit(_limit=240,
                                                                                        with_shutdown_enabled=True)
            self.config.BaseStation3200[1].points_of_control[1].set_low_pressure_limit(_limit=100,
                                                                                       with_shutdown_enabled=True)
            # add devices to point of control
            self.config.BaseStation3200[1].points_of_control[1].add_flow_meter_to_point_of_control(
                _flow_meter_address=3)
            self.config.BaseStation3200[1].points_of_control[1].add_pump_to_point_of_control(_pump_address=1)
            self.config.BaseStation3200[1].points_of_control[1].add_master_valve_to_point_of_control(
                _master_valve_address=2)
            self.config.BaseStation3200[1].points_of_control[1].add_pressure_sensor_to_point_of_control(
                _pressure_sensor_address=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    def step_6(self):
        """
        ##################
        setup mainlines
        ##################
        Add mainlines ----> to controller
            - set up main line Attributes \n
                - set enabled State
                - set limit zones by flow \n
                - set the pipe fill stabilization\n
                - set the target flow\n
                - set the high variance limit with shut down state \n
                - set the low variance limit with shut down state \n
            - Add Mainline ---> to point of control
        """

        # here we can either execute the following uncommented lines in procedural fashion, or we could re-init the
        # object, would have to import Mainline at the top, effectively accomplishing the same thing by:
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_enabled()
            self.config.BaseStation3200[1].mainlines[1].set_pipe_stabilization_time(_minutes=4)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=500)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[1].set_high_flow_variance_tier_one(_percent=5,
                                                                                        _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].mainlines[1].set_low_flow_variance_tier_one(_percent=20,
                                                                                       _with_shutdown_enabled=True)
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

            ###############################

    def step_7(self):
        """
        ######################
        setup zones on mainlines
        ######################
        Add zones to Mainlines
            - set up zone  Attributes associated with main lines \n
                - set design flow on zone
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Mainline 1
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=1)
            self.config.BaseStation3200[1].zones[1].set_design_flow(_gallons_per_minute=25)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

            #################################

    def step_8(self):
        """
        ###############################
        verify the entire configuration  \n
        ###############################
            - Get information for each object from controller
            - verify information returned from controller against information stored in the objects
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.verify_full_configuration()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]


