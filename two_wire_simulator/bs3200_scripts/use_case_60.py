import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration

# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr

from common import helper_methods

__author__ = 'Tim'


class ControllerUseCase60(object):
    """
    Test name:
        - CN UseCase60 POC dynamic flow allocation

    Purpose:
        - Set up a full configuration on the controller
        - Set mainlines with and without dynamic allocation set
        - Run programs using these mainlines and verify the correct POCs activate

    Coverage area:
        - All POCs run when dynamic flow allocation is off on a mainline
        - With dynamic flow allocation is on POCs are turned on only when needed as flow demand increases on a mainline

    Areas not covered:
        -

    Test Configuration setup: \n
        1. controller:
            - concurrent zones = 10

        2. poc 1:
            - 20gpm

        2. poc 2:
            - 100gpm

        2. mainline 1:
            - 200gpm
            - concurrent zones = yes

        2. program 1 & 2:
            - 8 am start time
            - concurrent zones = 1

        1. zones 1 on program 1
            - 10 min runtime
            - 15pgm design flow
            - assigned to mainline 1

        1. zones 2 on program 2
            - 10 min runtime
            - 95pgm design flow
            - assigned to mainline 1

    Test Steps - one case to test:
        1. Test 1 without dynamic flow
            - start program 1
                - verify only zone 1 is running
                - verify POC 1 and 2 are running
            - stop program 1
            - start program 2
                - verify only zone 2 is running
                - verify POC 1 and 2 are running
            - stop program 2
            - start program 1 and 2
                - verify zone 1 and 2 are running
                - verify POC 1 and 2 are running
            - stop both programs
        2. Test 2 with dynamic flow
            - start program 1
                - verify only zone 1 is running
                - verify only POC 1 is running
            - stop program 1
            - start program 2
                - verify only zone 2 is running
                - verify only POC 2 is running
            - start program 1
                - verify zone 1 and 2 are running
                - verify POC 1 and 2 are running
            - stop both programs

    """

    ###############################
    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase44' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    ###############################
    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break

                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries + 1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    ###############################
    def step_1(self):
        """
        ############################
        setup controller
        ############################
        - current zones
        - stop condition = moisture sensor above 25.0%
        - pause condition = event switch open
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation3200[1].set_max_concurrent_zones(_max_zones=10)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_2(self):
        """
        ############################
        setup programs
        ############################
        1. controller:
            - concurrent zones = 10

        2. water sources 1 and 2:
            - assigned to POC 1 and 2 respectively

        3. poc 1:
            - 20gpm

        4. poc 2:
            - 100gpm

        5. mainline 1:
            - 200gpm
            - concurrent zones = yes

        6. program 1 & 2:
            - 8 am start time
            - concurrent zones = 1
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Program 1
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=1)
            self.config.BaseStation3200[1].programs[1].set_enabled()
            self.config.BaseStation3200[1].programs[1].set_max_concurrent_zones(_number_of_zones=1)

            # Program 2
            self.config.BaseStation3200[1].add_program_to_controller(_program_address=2)
            self.config.BaseStation3200[1].programs[2].set_enabled()
            self.config.BaseStation3200[1].programs[2].set_max_concurrent_zones(_number_of_zones=1)

            # Water Sources
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=1)
            self.config.BaseStation3200[1].add_water_source_to_controller(_water_source_address=2)

            # POC 1
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=1)
            self.config.BaseStation3200[1].points_of_control[1].set_target_flow(_gpm=20)

            # POC 2
            self.config.BaseStation3200[1].add_point_of_control_to_controller(_point_of_control_address=2)
            self.config.BaseStation3200[1].points_of_control[2].set_target_flow(_gpm=100)

            # Add the points of control the their respective water sources
            self.config.BaseStation3200[1].water_sources[1].add_point_of_control_to_water_source(_point_of_control_address=1)
            self.config.BaseStation3200[1].water_sources[2].add_point_of_control_to_water_source(_point_of_control_address=2)

            # Mainline 1
            self.config.BaseStation3200[1].add_mainline_to_controller(_mainline_address=1)
            self.config.BaseStation3200[1].mainlines[1].set_target_flow(_gpm=200)
            self.config.BaseStation3200[1].mainlines[1].set_limit_zones_by_flow_to_true()
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=1)
            self.config.BaseStation3200[1].mainlines[1].add_zone_to_mainline(_zone_address=2)
            self.config.BaseStation3200[1].points_of_control[1].add_mainline_to_point_of_control(_mainline_address=1)
            self.config.BaseStation3200[1].points_of_control[2].add_mainline_to_point_of_control(_mainline_address=1)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    ###############################
    def step_3(self):
        """
        ############################
        setup Zones on programs
        ############################
        Add zone -----> to program
        - set up zone program  Attributes \n
            - set zone type |Timed, Primary, Linked | \n
            - set runtime\n
            - set cycle time \n
            - set soak time \n
            if zone type |Linked| \n
            - set tracking ration \n
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Setup zone 1
            self.config.BaseStation3200[1].zones[1].set_design_flow(15)
            self.config.BaseStation3200[1].programs[1].add_zone_to_program(_zone_address=1)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_run_time(_minutes=10)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[1].zone_programs[1].set_soak_time(_minutes=10)

            # Setup zone 2
            self.config.BaseStation3200[1].zones[2].set_design_flow(95)
            self.config.BaseStation3200[1].programs[2].add_zone_to_program(_zone_address=2)
            self.config.BaseStation3200[1].programs[2].zone_programs[2].set_run_time(_minutes=10)
            self.config.BaseStation3200[1].programs[2].zone_programs[2].set_cycle_time(_minutes=10)
            self.config.BaseStation3200[1].programs[2].zone_programs[2].set_soak_time(_minutes=10)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ###############################
        Test 1 without dynamic flow
        ###############################

        1. Test 1 without dynamic flow
            - start program 1
                - verify only zone 1 is running
                - verify POC 1 and 2 are running
            - stop program 1
            - start program 2
                - verify only zone 2 is running
                - verify POC 1 and 2 are running
            - stop program 2
            - start program 1 and 2
                - verify zone 1 and 2 are running
                - verify POC 1 and 2 are running
            - stop both programs
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set time to 1:00 am
            self.config.BaseStation3200[1].set_date_and_time(_date='1/31/2018', _time='1:00:00')

            #=====================================================================
            # start program 1
            self.config.BaseStation3200[1].programs[1].set_program_to_start()
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)

            # verify status
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_running()

            # stop program 1
            self.config.BaseStation3200[1].programs[1].set_program_to_stop()

            #=====================================================================
            # start program 2
            self.config.BaseStation3200[1].programs[2].set_program_to_start()
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)

            # verify status
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_running()

            #=====================================================================
            # start program 1
            self.config.BaseStation3200[1].programs[1].set_program_to_start()
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)

            # verify status
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_running()

            # clean up
            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)

        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ###############################
        Test 2 -  Test 2 with dynamic flow
        ###############################

        2. Test 2 with dynamic flow
            - start program 1
                - verify only zone 1 is running
                - verify only POC 1 is running
            - stop program 1
            - start program 2
                - verify only zone 2 is running
                - verify only POC 2 is running
            - start program 1
                - verify zone 1 and 2 are running
                - verify POC 1 and 2 are running
            - stop both programs
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            # Set time to 1:00 am
            self.config.BaseStation3200[1].set_date_and_time(_date='1/31/2018', _time='1:00:00')

            # Set dynamic flow allocation to true on mainline 1
            self.config.BaseStation3200[1].mainlines[1].set_dynamic_flow_allocation_to_true()

            #=====================================================================
            # start program 1
            self.config.BaseStation3200[1].programs[1].set_program_to_start()
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)

            # verify status
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_off()

            # stop program 1
            self.config.BaseStation3200[1].programs[1].set_program_to_stop()

            #=====================================================================
            # start program 2
            self.config.BaseStation3200[1].programs[2].set_program_to_start()
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)

            # verify status
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_done()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_off()
            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_running()

            #=====================================================================
            # start program 1
            self.config.BaseStation3200[1].programs[1].set_program_to_start()
            self.config.BaseStation3200[1].do_increment_clock(minutes=2)

            # verify status
            self.config.BaseStation3200[1].programs[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].programs[2].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].zones[1].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].zones[2].statuses.verify_status_is_watering()
            self.config.BaseStation3200[1].points_of_control[1].statuses.verify_status_is_running()
            self.config.BaseStation3200[1].points_of_control[2].statuses.verify_status_is_running()

            # clean up
            self.config.BaseStation3200[1].programs[1].set_program_to_stop()
            self.config.BaseStation3200[1].programs[2].set_program_to_stop()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                self.config.test_name,
                sys._getframe().f_code.co_name,
                date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
