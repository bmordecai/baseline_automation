import sys
from time import sleep
from datetime import timedelta, datetime

from common.configuration import Configuration
# this import allows us to directly use the date_mngr
from common.date_package.date_resource import date_mngr
# Objects
from common.objects.base_classes.web_driver import *

from common.imports import opcodes, types

# import log_handler for logging functionality
from common.logging_handler import log_handler

from common import helper_methods


__author__ = 'Eldin'


class ControllerUseCase1(object):
    """
    Test name:
        - TW UseCase1 Editing BiCoder Values

    User Story:
        - As a user I want an item that will simulate two wire communications.

    Coverage area:
        - Valve biCoders (Zones, Master Valves, Pumps) return voltage/current
            - We have more zones in the configuration than we will actually use, we have a lot just to show that we
              are able to load a lot of them.
        - Moisture biCoders (Moisture sensors) return readings
        - Temperature biCoders (Temperature sensors) return readings
        - Analog biCoders (Pressure sensors) return readings
        - Event biCoders (Event Switch) return state
        - Flow biCoders (Flow Meter) return readings

    Explanation of Scenarios:
        - 1st scenario:
            - Load up all required devices.
            - Change Zone/Master Valve/Pump biCoder values and verify controller got the change through the two-wire.
            - Change Moisture biCoder values and verify controller got the change through the two-wire.
            - Change Temperature biCoder values and verify controller got the change through the two-wire.
            - Change Analog biCoder values and verify controller got the change through the two-wire.
            - Change Event biCoder values and verify controller got the change through the two-wire.
            - Change Flow biCoder values and verify controller got the change through the two-wire.
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize 'UseCase1' instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str \n

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      UserConfiguration \n

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str \n
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)
        self.run_use_case()

    def run_use_case(self):
        """
        Initialize for Test:
            - reset all objects
            - create controller objects
            - connect to BaseManager  | bool True or False
        Run all Steps in use case:
            - Run all steps in order that art in the use case
            - retry is setup so that you can rerun the same test
        :return:
        :rtype:
        """

        try:
            number_of_retries = 0
            retries = 0
            while True:
                # executes a "retry"
                try:
                    # Resets objects to a known state, creates serial connections, creates all objects.
                    self.config.initialize_for_test(connect_to_basemanager=False, real_devices=True)

                    # get list of all the steps by function name in the use case
                    method_list = [func for func in dir(self) if
                                   callable(getattr(self, func)) and func.startswith('step')]
                    # sort list in numerical order of numbers in steps step names must be 'step_X'
                    sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                    # run each step_1,2,3 esc.
                    for method in sorted_new_list:
                        getattr(self, method)()

                    helper_methods.print_test_passed(test_name=self.config.test_name)
                    break
                except Exception as e:
                    if hasattr(e, 'msg'):
                        error_txt = e.msg
                    else:
                        error_txt = e.message
                    # getting a none I cant explain
                    print "Exception received: " + str(error_txt)
                    print "Retrying " + self.config.test_name + " for the " + str(retries+1) + " time"
                    self.config.resource_handler.restart_connections()
                    retries += 1

                    # added more sleep time to let controller processes finish whatever they are doing.
                    time.sleep(10)
                    if retries >= number_of_retries:
                        helper_methods.print_test_failed(test_name=self.config.test_name)
                        # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
                        # to the next use case in the list
                        if log_handler.is_enabled():
                            log_handler.exception(message=e.message)
                            break
                        else:
                            raise
        finally:
            helper_methods.end_controller_test(config_object=self.config)

    #################################
    def step_1(self):
        """
        ############################
        Connect BaseStation to TwoWireSimulator
        ############################
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.BaseStation1000[1].set_connection_to_two_wire_simulator(
                two_wire_simulator=self.config.TWSimulator[1])
            self.config.BaseStation1000[1].search_for_simulated_bicoders()

            # Assign zones
            self.config.BaseStation1000[1].add_zone_to_controller(_address=1, _serial_number="HA00001")
            self.config.BaseStation1000[1].add_zone_to_controller(_address=2, _serial_number="HA00002")
            self.config.BaseStation1000[1].add_zone_to_controller(_address=3, _serial_number="HA00003")
            self.config.BaseStation1000[1].add_zone_to_controller(_address=4, _serial_number="HA00004")
            self.config.BaseStation1000[1].add_zone_to_controller(_address=5, _serial_number="HA00005")
            self.config.BaseStation1000[1].add_zone_to_controller(_address=6, _serial_number="HA00006")
            self.config.BaseStation1000[1].add_zone_to_controller(_address=7, _serial_number="HA00007")
            self.config.BaseStation1000[1].add_zone_to_controller(_address=8, _serial_number="HA00008")
            self.config.BaseStation1000[1].add_zone_to_controller(_address=9, _serial_number="HA00009")
            self.config.BaseStation1000[1].add_zone_to_controller(_address=10, _serial_number="HA00010")
            self.config.BaseStation1000[1].add_zone_to_controller(_address=11, _serial_number="HA00011")
            self.config.BaseStation1000[1].add_zone_to_controller(_address=12, _serial_number="HA00012")
            self.config.BaseStation1000[1].add_zone_to_controller(_address=13, _serial_number="HA00021")
            self.config.BaseStation1000[1].add_zone_to_controller(_address=14, _serial_number="HA00022")
            self.config.BaseStation1000[1].add_zone_to_controller(_address=15, _serial_number="HA00023")
            self.config.BaseStation1000[1].add_zone_to_controller(_address=16, _serial_number="HA00024")
            self.config.BaseStation1000[1].add_zone_to_controller(_address=17, _serial_number="HA00025")
            self.config.BaseStation1000[1].add_zone_to_controller(_address=18, _serial_number="HA00026")
            self.config.BaseStation1000[1].add_zone_to_controller(_address=19, _serial_number="HA00027")
            self.config.BaseStation1000[1].add_zone_to_controller(_address=20, _serial_number="HA00028")
            self.config.BaseStation1000[1].add_zone_to_controller(_address=96, _serial_number="HA00029")

            # Assign master valve
            self.config.BaseStation1000[1].add_master_valve_to_controller(_address=1, _serial_number="HM00001")
            self.config.BaseStation1000[1].add_master_valve_to_controller(_address=2, _serial_number="HM00002")
            self.config.BaseStation1000[1].add_master_valve_to_controller(_address=7, _serial_number="HM00003")

            # Assign moisture sensors
            self.config.BaseStation1000[1].add_moisture_sensor_to_controller(_address=1, _serial_number="HY00001")
            self.config.BaseStation1000[1].add_moisture_sensor_to_controller(_address=2, _serial_number="HY00002")
            self.config.BaseStation1000[1].add_moisture_sensor_to_controller(_address=8, _serial_number="HY00003")

            # Assign temperature sensors
            self.config.BaseStation1000[1].add_temperature_sensor_to_controller(_address=1, _serial_number="HT00001")
            self.config.BaseStation1000[1].add_temperature_sensor_to_controller(_address=2, _serial_number="HT00002")
            self.config.BaseStation1000[1].add_temperature_sensor_to_controller(_address=3, _serial_number="HT00003")

            # Assign event switches
            self.config.BaseStation1000[1].add_event_switch_to_controller(_address=1, _serial_number="HE00001")
            self.config.BaseStation1000[1].add_event_switch_to_controller(_address=2, _serial_number="HE00002")
            self.config.BaseStation1000[1].add_event_switch_to_controller(_address=3, _serial_number="HE00003")

            # Assign flow meter
            self.config.BaseStation1000[1].add_flow_meter_to_controller(_address=1, _serial_number="HF00001")
            self.config.BaseStation1000[1].add_flow_meter_to_controller(_address=2, _serial_number="HF00002")
            self.config.BaseStation1000[1].add_flow_meter_to_controller(_address=3, _serial_number="HF00003")
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_2(self):
        """
        ############################
        Zone BiCoders
        ############################
        - Change and verify the two-wire solenoid current and solenoid voltage.
            - Set the value through the serial port that is talking to the executable that is running on your computer.
            - Tell the 1000 controller you are connected to do a self test so that it goes out and reads the current
              and voltage values we just set on the simulator.
            - Our test engine objects were set when we wrote the values, so now we need to compare our values in the
              object to the ones that the 1000 got after doing a self test to retrieve the values from the two-wire.
            - These values should, if not exactly the same, be very close. Some values are off a little due to the
              calculations that must be done. We give a small tolerance for those values.
        NOTE: R-Boards (12 valves) are set at a CONSTANT 24 volts for now, so our voltage will always be 24
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.TWSimulator[1].get_simulated_valve_bicoders("HA00001").write_solenoid_current(_solenoid_current=0.3)
            self.config.TWSimulator[1].get_simulated_valve_bicoders("HA00001").write_solenoid_voltage(_solenoid_voltage=24)
            self.config.BaseStation1000[1].zones[1].bicoder.do_self_test()
            self.config.BaseStation1000[1].zones[1].get_bicoder().verify_solenoid_current()
            self.config.BaseStation1000[1].zones[1].get_bicoder().verify_solenoid_voltage()

            self.config.TWSimulator[1].get_simulated_valve_bicoders("HA00002").write_solenoid_current(_solenoid_current=0.75)
            self.config.TWSimulator[1].get_simulated_valve_bicoders("HA00002").write_solenoid_voltage(_solenoid_voltage=24)
            self.config.BaseStation1000[1].zones[2].get_bicoder().do_self_test()
            self.config.BaseStation1000[1].zones[2].get_bicoder().verify_solenoid_current()
            self.config.BaseStation1000[1].zones[2].get_bicoder().verify_solenoid_voltage()

            self.config.TWSimulator[1].get_simulated_valve_bicoders("HA00029").write_solenoid_current(_solenoid_current=1.5)
            self.config.TWSimulator[1].get_simulated_valve_bicoders("HA00029").write_solenoid_voltage(_solenoid_voltage=24)
            self.config.BaseStation1000[1].zones[96].get_bicoder().do_self_test()
            self.config.BaseStation1000[1].zones[96].get_bicoder().verify_solenoid_current()
            self.config.BaseStation1000[1].zones[96].get_bicoder().verify_solenoid_voltage()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_3(self):
        """
        ############################
        Master Valve BiCoders
        ############################
        - Change and verify the two-wire solenoid current and solenoid voltage.
            - Set the value through the serial port that is talking to the executable that is running on your computer.
            - Tell the 1000 controller you are connected to do a self test so that it goes out and reads the current
              and voltage values we just set on the simulator.
            - Our test engine objects were set when we wrote the values, so now we need to compare our values in the
              object to the ones that the 1000 got after doing a self test to retrieve the values from the two-wire.
            - These values should, if not exactly the same, be very close. Some values are off a little due to the
              calculations that must be done. We give a small tolerance for those values.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.TWSimulator[1].get_simulated_valve_bicoders("HM00001").write_solenoid_current(_solenoid_current=0.3)
            self.config.TWSimulator[1].get_simulated_valve_bicoders("HM00001").write_solenoid_voltage(_solenoid_voltage=24)
            self.config.BaseStation1000[1].master_valves[1].get_bicoder().do_self_test()
            self.config.BaseStation1000[1].master_valves[1].get_bicoder().verify_solenoid_current()
            self.config.BaseStation1000[1].master_valves[1].get_bicoder().verify_solenoid_voltage()

            self.config.TWSimulator[1].get_simulated_valve_bicoders("HM00002").write_solenoid_current(_solenoid_current=0.75)
            self.config.TWSimulator[1].get_simulated_valve_bicoders("HM00002").write_solenoid_voltage(_solenoid_voltage=24)
            self.config.BaseStation1000[1].master_valves[2].get_bicoder().do_self_test()
            self.config.BaseStation1000[1].master_valves[2].get_bicoder().verify_solenoid_current()
            self.config.BaseStation1000[1].master_valves[2].get_bicoder().verify_solenoid_voltage()

            self.config.TWSimulator[1].get_simulated_valve_bicoders("HM00003").write_solenoid_current(_solenoid_current=1.5)
            self.config.TWSimulator[1].get_simulated_valve_bicoders("HM00003").write_solenoid_voltage(_solenoid_voltage=24)
            self.config.BaseStation1000[1].master_valves[7].get_bicoder().do_self_test()
            self.config.BaseStation1000[1].master_valves[7].get_bicoder().verify_solenoid_current()
            self.config.BaseStation1000[1].master_valves[7].get_bicoder().verify_solenoid_voltage()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_4(self):
        """
        ############################
        Moisture Sensor BiCoders
        ############################
        - Change and verify the two-wire moisture percent and temperature reading.
            - Set the value through the serial port that is talking to the executable that is running on your computer.
            - Tell the 1000 controller you are connected to do a self test so that it goes out and reads the current
              and voltage values we just set on the simulator.
            - Our test engine objects were set when we wrote the values, so now we need to compare our values in the
              object to the ones that the 1000 got after doing a self test to retrieve the values from the two-wire.
            - These values should, if not exactly the same, be very close. Some values are off a little due to the
              calculations that must be done. We give a small tolerance for those values.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.TWSimulator[1].get_simulated_moisture_bicoders("HY00001").write_moisture_percent(_percent=29.5)
            self.config.TWSimulator[1].get_simulated_moisture_bicoders("HY00001").write_temperature_reading(_temp=88)
            self.config.BaseStation1000[1].moisture_sensors[1].get_bicoder().do_self_test()
            self.config.BaseStation1000[1].moisture_sensors[1].get_bicoder().verify_moisture_percent()
            self.config.BaseStation1000[1].moisture_sensors[1].get_bicoder().verify_temperature_reading()

            self.config.TWSimulator[1].get_simulated_moisture_bicoders("HY00002").write_moisture_percent(_percent=25)
            self.config.TWSimulator[1].get_simulated_moisture_bicoders("HY00002").write_temperature_reading(_temp=83)
            self.config.BaseStation1000[1].moisture_sensors[2].get_bicoder().do_self_test()
            self.config.BaseStation1000[1].moisture_sensors[2].get_bicoder().verify_moisture_percent()
            self.config.BaseStation1000[1].moisture_sensors[2].get_bicoder().verify_temperature_reading()

            self.config.TWSimulator[1].get_simulated_moisture_bicoders("HY00003").write_moisture_percent(_percent=15.5)
            self.config.TWSimulator[1].get_simulated_moisture_bicoders("HY00003").write_temperature_reading(_temp=75)
            self.config.BaseStation1000[1].moisture_sensors[8].get_bicoder().do_self_test()
            self.config.BaseStation1000[1].moisture_sensors[8].get_bicoder().verify_moisture_percent()
            self.config.BaseStation1000[1].moisture_sensors[8].get_bicoder().verify_temperature_reading()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_5(self):
        """
        ############################
        Temperature Sensor BiCoders
        ############################
        - Change and verify the two-wire temperature reading.
            - Set the value through the serial port that is talking to the executable that is running on your computer.
            - Tell the 1000 controller you are connected to do a self test so that it goes out and reads the current
              and voltage values we just set on the simulator.
            - Our test engine objects were set when we wrote the values, so now we need to compare our values in the
              object to the ones that the 1000 got after doing a self test to retrieve the values from the two-wire.
            - These values should, if not exactly the same, be very close. Some values are off a little due to the
              calculations that must be done. We give a small tolerance for those values.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.TWSimulator[1].get_simulated_temperature_bicoders("HT00001").write_temperature_reading(_degrees=87)
            self.config.BaseStation1000[1].temperature_sensors[1].get_bicoder().do_self_test()
            self.config.BaseStation1000[1].temperature_sensors[1].get_bicoder().verify_temperature_reading()

            self.config.TWSimulator[1].get_simulated_temperature_bicoders("HT00002").write_temperature_reading(_degrees=82)
            self.config.BaseStation1000[1].temperature_sensors[2].get_bicoder().do_self_test()
            self.config.BaseStation1000[1].temperature_sensors[2].get_bicoder().verify_temperature_reading()

            self.config.TWSimulator[1].get_simulated_temperature_bicoders("HT00003").write_temperature_reading(_degrees=75)
            self.config.BaseStation1000[1].temperature_sensors[3].get_bicoder().do_self_test()
            self.config.BaseStation1000[1].temperature_sensors[3].get_bicoder().verify_temperature_reading()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_6(self):
        """
        ############################
        Switch BiCoders
        ############################
        - Change and verify the two-wire contact state
            - Set the value through the serial port that is talking to the executable that is running on your computer.
            - Tell the 1000 controller you are connected to do a self test so that it goes out and reads the current
              and voltage values we just set on the simulator.
            - Our test engine objects were set when we wrote the values, so now we need to compare our values in the
              object to the ones that the 1000 got after doing a self test to retrieve the values from the two-wire.
            - These values should, if not exactly the same, be very close. Some values are off a little due to the
              calculations that must be done. We give a small tolerance for those values.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.TWSimulator[1].get_simulated_switch_bicoders("HE00001").write_contact_open()
            self.config.BaseStation1000[1].event_switches[1].get_bicoder().do_self_test()
            self.config.BaseStation1000[1].event_switches[1].get_bicoder().verify_contact_state()

            self.config.TWSimulator[1].get_simulated_switch_bicoders("HE00002").write_contact_open()
            self.config.BaseStation1000[1].event_switches[2].get_bicoder().do_self_test()
            self.config.BaseStation1000[1].event_switches[2].get_bicoder().verify_contact_state()

            self.config.TWSimulator[1].get_simulated_switch_bicoders("HE00003").write_contact_closed()
            self.config.BaseStation1000[1].event_switches[3].get_bicoder().do_self_test()
            self.config.BaseStation1000[1].event_switches[3].get_bicoder().verify_contact_state()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]

    #################################
    def step_7(self):
        """
        ############################
        Flow BiCoders
        ############################
        - Change and verify the two-wire flow percent and water usage.
            - Set the value through the serial port that is talking to the executable that is running on your computer.
            - Tell the 1000 controller you are connected to do a self test so that it goes out and reads the current
              and voltage values we just set on the simulator.
            - Our test engine objects were set when we wrote the values, so now we need to compare our values in the
              object to the ones that the 1000 got after doing a self test to retrieve the values from the two-wire.
            - These values should, if not exactly the same, be very close. Some values are off a little due to the
              calculations that must be done. We give a small tolerance for those values.
        """
        method = "\n ########################     Running " + sys._getframe().f_code.co_name + " ######################"
        print method
        try:
            self.config.TWSimulator[1].get_simulated_flow_bicoders("HF00001").write_flow_rate(_gallons_per_minute=50.0)
            self.config.TWSimulator[1].get_simulated_flow_bicoders("HF00001").write_water_usage(_water_usage=10000)
            self.config.BaseStation1000[1].flow_meters[1].get_bicoder().do_self_test()
            self.config.BaseStation1000[1].flow_meters[1].get_bicoder().verify_flow_rate()
            self.config.BaseStation1000[1].flow_meters[1].get_bicoder().verify_water_usage()

            self.config.TWSimulator[1].get_simulated_flow_bicoders("HF00002").write_flow_rate(_gallons_per_minute=40.0)
            self.config.TWSimulator[1].get_simulated_flow_bicoders("HF00002").write_water_usage(_water_usage=5000)
            self.config.BaseStation1000[1].flow_meters[2].get_bicoder().do_self_test()
            self.config.BaseStation1000[1].flow_meters[2].get_bicoder().verify_flow_rate()
            self.config.BaseStation1000[1].flow_meters[2].get_bicoder().verify_water_usage()

            self.config.TWSimulator[1].get_simulated_flow_bicoders("HF00003").write_flow_rate(_gallons_per_minute=25.0)
            self.config.TWSimulator[1].get_simulated_flow_bicoders("HF00003").write_water_usage(_water_usage=1000)
            self.config.BaseStation1000[1].flow_meters[3].get_bicoder().do_self_test()
            self.config.BaseStation1000[1].flow_meters[3].get_bicoder().verify_flow_rate()
            self.config.BaseStation1000[1].flow_meters[3].get_bicoder().verify_water_usage()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Controller Date and time was currently set to {2}\n" \
                    "\tThe Exception thrown was {3}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        date_mngr.controller_datetime.formatted_date_time_string("%m-%d-%Y", "%H:%M:%S"),
                        str(e.message))
            raise Exception, Exception(e_msg), sys.exc_info()[2]
