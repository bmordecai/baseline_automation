import os

import common.user_configuration as user_conf_module

# bmw use cases
import bmw_bs3200_v16_scripts.uc1_login_test as uc1_module
import bmw_bs3200_v16_scripts.uc2_schedules_tab_test as uc2_module
import bmw_bs3200_v16_scripts.uc3_menu_tab_test as uc3_module
import bmw_bs3200_v16_scripts.uc4_address_device_test as uc4_module
import bmw_bs3200_v16_scripts.uc5_basemanager_devices_tab as uc5_module
import bmw_bs3200_v16_scripts.uc6_verify_ws_ui as uc6_module
import bmw_bs3200_v16_scripts.uc7_verify_poc_ui as uc7_module
import bmw_bs3200_v16_scripts.uc9_verify_ml_ui as uc9_module

_author__ = 'ben'


def run_use_cases_basemanager_v16_3200(user_configuration_file_name, auto_update_fw, passing_tests, failing_tests,
                                       manual_tests, specific_tests):
    """
    This is where we will run all of our BaseManager + v16 3200 use cases. \n

    :param user_configuration_file_name:    The name of the configuration file we will be using for the tests. \n
    :type user_configuration_file_name:     str

    :param auto_update_fw:    If you want to automatically update firmware versions of controllers to "latest" version.
    :type auto_update_fw:     bool

    :param passing_tests:   If you want to run passing tests. True for yes, False for no. \n
    :type passing_tests:    bool

    :param failing_tests:   If you want to run failing tests. True for yes, False for no. \n
    :type failing_tests:    bool

    :param manual_tests:    If you want to run manual tests. True for yes, False for no. \n
    :type manual_tests:     bool

    :param specific_tests:  Pick which tests you want to run individually. Will run all if the list is empty. \n
    :type specific_tests:   list[int]
    """
    
    # Load in user configured items from text file (necessary for serial connection)
    user_conf = user_conf_module.UserConfiguration(
        os.path.join('common/user_credentials', user_configuration_file_name))
    
    # ---------------------------------------------------------------------------------------------------------------- #
    #                                                                                                                  #
    #                                           BaseManager + v16 3200 Tests                                           #
    #                                                                                                                  #
    # ---------------------------------------------------------------------------------------------------------------- #
    # ---------------------------------------------------------------------------------------------------------------- #
    # ##########################################------Passing Tests------############################################# #
    # ---------------------------------------------------------------------------------------------------------------- #
    if passing_tests:
        if 2 in specific_tests or len(specific_tests) == 0:
            uc2_module.BaseManagerUseCase2(test_name="BM-UseCase2-SchedulesTabTest-v16-3200",
                                           user_configuration_instance=user_conf,
                                           json_configuration_file='devices_for_programs.json')
        if 3 in specific_tests or len(specific_tests) == 0:
            uc3_module.BaseManagerUseCase3(test_name="BM-UseCase3-MenuTabTest-v16-3200",
                                           user_configuration_instance=user_conf,
                                           json_configuration_file='update_cn.json')
        if 4 in specific_tests or len(specific_tests) == 0:
            uc4_module.BaseManagerUseCase4(test_name="BM-UseCase4-AddressDevTest-v16-3200",
                                           user_configuration_instance=user_conf,
                                           json_configuration_file='update_cn.json')
        if 5 in specific_tests or len(specific_tests) == 0:
            uc5_module.BaseManagerUseCase5(test_name="BM-UC5-baseManager_devices_tab-v16-3200",
                                           user_configuration_instance=user_conf,
                                           json_configuration_file='two_of_each_device.json')
        if 6 in specific_tests or len(specific_tests) == 0:
            uc6_module.BaseManagerUseCase6(test_name="BM-UseCase6-VerifyWsUI-v16-3200",
                                           user_configuration_instance=user_conf,
                                           json_configuration_file='devices_for_empty_conditions.json')
        if 7 in specific_tests or len(specific_tests) == 0:
            uc7_module.BaseManagerUseCase7(test_name="BM-UseCase7-VerifyPocUI-v16-3200",
                                           user_configuration_instance=user_conf,
                                           json_configuration_file='update_cn.json')
        if 9 in specific_tests or len(specific_tests) == 0:
            uc9_module.BaseManagerUseCase9(test_name="BM-UseCase9-VerifyMlUI-v16-3200",
                                           user_configuration_instance=user_conf,
                                           json_configuration_file='update_cn.json')
        
        print("################### ----FINISHED RUNNING PASSING BASEMANAGER + V16 3200 TESTS---- #####################")
    # ---------------------------------------------------------------------------------------------------------------- #
    # ##########################################------Manual Tests------############################################## #
    # ---------------------------------------------------------------------------------------------------------------- #
    # ---------------------------------------------------------------------------------------------------------------- #
    #                                                                                                                  #
    #      (Requires Manual Device/Use-Case Modification)                                                              #
    #                                                                                                                  #
    #    - THESE USE CASES ARE NOT APART OF NIGHTLY BUILD                                                              #
    #    - These use cases require setting breakpoints at specific locations in the test in order to configure         #
    #      a real-device to be in an expected state for the test.                                                      #
    #    - Above each use case is a list of break points to set inside the use case. The breakpoints should point      #
    #      to a "To Do" item which explains what the user needs to do.                                                 #
    #                                                                                                                  #
    # ---------------------------------------------------------------------------------------------------------------- #
    if manual_tests:
        print("################### ----FINISHED RUNNING MANUAL BASEMANAGER + V12 3200 TESTS---- #####################")
    
    # ---------------------------------------------------------------------------------------------------------------- #
    # ##########################################------Failing Tests------############################################# #
    # ---------------------------------------------------------------------------------------------------------------- #
    if failing_tests:
        
        if 1 in specific_tests or len(specific_tests) == 0:
            uc1_module.BaseManagerUseCase1(test_name="BM-UseCase1-LoginTest-v16-3200",
                                           user_configuration_instance=user_conf,
                                           json_configuration_file='update_cn.json')
        
        print("################### ----FINISHED RUNNING FAILING BASEMANAGER + V12 3200 TESTS---- #####################")
    
    # ---------------------------------------------------------------------------------------------------------------- #
    # #######################################------Future Needed Tests------########################################## #
    # ---------------------------------------------------------------------------------------------------------------- #
    
    print("######################### ----YOU ARE A WINNER WINNER CHICKEN DINNER---- ##################################")


if __name__ == "__main__":
    run_use_cases_basemanager_v16_3200(user_configuration_file_name="user_credentials_tige_bm.json",
                                       passing_tests=False,
                                       failing_tests=False,
                                       manual_tests=False,
                                       specific_tests=[],
                                       auto_update_fw=False)
    exit()

