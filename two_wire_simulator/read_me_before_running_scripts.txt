The scripts are a combination of all the test that can be ran against the specified product.

# ------------------------------------------------------------------- #
Ideas:
    Wording:
        1.  "Package"
            -   This is a collection of python modules and classes considered
                relative to each other, thus are grouped in a package. This is 
                very similar to directories.

        2.  "Module"
            -   This is a python file containing methods and anything else
                considered relative in grouping into the same file

        3.  "Class" ("Object")
            -   A python class is an 'Object' contained in a 'Module'. Classes
                contain an '__init__' method which allows for setting instance
                based variables to certain starting values or values passed in.

                -   These instance based variables can only be seen by itself,
                    (the instance)

            -   Initializing a class would look like:
                my_class = MyClass(parameter_1, parameter_2)

            -   Class variables are inherited by all instances of that class.

        4.  "Inheritance"
            -   This is when a child class inherits methods and functionality
                from it's base class. This is accomplished by importing the 
                base class and passing the base class name into the parenthesis.
                i.e: from package.subPackage.parentClass import ParentClass
                    class ChildClass(ParentClass):

            -   Next in order to implement the parent's class functionality it
                needs to be initialized in the child's class '__init__' method.
                i.e: class ChildClass(ParentClass):
                        def __init__(self, param_1, param_2):
                            ParentClass.__init__(self, any other parameters)

    Base Classes:
        1.  ser.py
            -   Contains serial object and all serial helper methods that
                can be referenced through the instance.
            -   Once initialized, the 'ser' instance is passed to objects
                and anything else requiring communication over serial port.
            -   Is initialized in the 'configuration.py' module.

    Dictionaries:
        1.  We use dictionaries to store objects. Each object can be 
            referenced by it's address number (integer) in the dictionary.
        2.  Each object is addressed using an integer, this allows us to 
            iterate over the dictionary in a sorted fashion if need be.
            ie: "for blah in sorted(some_dictionary.keys()):"

# ------------------------------------------------------------------- #

Naming convention:
What who what
do = multi command task example: do_ms_sr "search for moisture sensors"
set = single command task example: set_ms_description  "set moisture sensor descriptions"
ver = verify or compare a set and get method results example ver_ms_description
this a set or a get and verifies against some known value
get = single command task example: get_ms_description
execute = run: example run_device_tab_test
run = run: example do_exercise
cn = controller
fm = flow meter
ms = moisture sensor
mv = master valve
ts = temperature sensor
zn = zone

File structure
automated_testing_2015:
    -> bmw_scripts
    -> bs1000_scripts
    -> bs3200_scripts
    -> common
        - Contains common files used throughout test scripts for the 1000, 3200 and BaseManager.
        
        --> basemanager
            -   Contains modules implementing browser.py object that objects can use to
                "do something" on basemanager.

        --> configuration_files
            -   Contains .json files containing configuration data to be used on a script
                basis.

        --> objects
            -   Object files used throughout testing structure. 
                i.e: ser.py, browser.py, devices.py, cn.py, zn.py

        --> unit_tests
            -   Unit tests surrounding module methods

        --> user_credentials
            -   Contains user, developer, tester configuration settings as far as login
                credentials, Ethernet ports, controller mac addresses, ect.

        --> variables
            -   Contains variables common to the specific controller type as well as browser 
                specific variables common throughout testing structure.

Browser:
   - common
   - maps
   - quickview
   - program
   - liveview
   - watersource_point_of_connetion
   - watersource_mainline
   - device_moisture
   - device_flow
   - device_zones
   - device_master_valves
   - device_temperature
   - device_event_switches
Controller:
   - common
   - cn_3200
   - cn_1000
   - device_moisture
   - device_flow
   - device_zones
   - device_master_valves
   - device_temperature
   - device_event_switches

First:
- Verify that you user credentials are correct and that it is pointed at the correct Json for the current user.
- In the user credentials file verify that all file path are pointed at your .exe files.
- Also if the port number is set to "None", the socket type will be used to establish a serial connection. If a port
number is enter then the port number will be used.

Second:
- In order to run the scripts double click or open the appropriate file "acceptance_test_"".py.
- Uncomment out which ever test you want to run and select the debug in your to run in your idea.

You can also run in command line just point at the appropriate file. "acceptance_test_"".py.

------------------------------------------------------------------------------------------------------------------------
1000 Tests Usages:

-

------------------------------------------------------------------------------------------------------------------------
3200 Tests Usages:

- Setup Programs:
    Prior to running test, verify the following are as desired: (edit any temporary changes as needed on your local
    machine.)
    1. Verify in the init method:
        a.) Correct range of programs to be created is specified
        b.) Correct range of zones to be added to each program
        c.) Correct range of primary zones to be added to each program
    2. Verify correct device configuration is being loaded to controller ('small' or 'large')
