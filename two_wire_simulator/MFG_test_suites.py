__author__ = 'Tige'
import sys
import os

from common.user_configuration import UserConfiguration
from Hardware_and_Manufacturing_scripts.bu_test import BUUseCase1
from Hardware_and_Manufacturing_scripts.bu_use_case_1_prototype import BUUseCase1ProtoType
from tcp_handler import TcpHandler

from common.objects.baseunit import bu_imports, bu_helpers, bu_mfg_factory


def test():
    # Load in user configured items from text file (necessary for serial connection)
    user_conf = UserConfiguration(os.path.join('common/user_credentials', 'user_credentials_ben_mfg.json'))
    tcp = TcpHandler(55111)

    # uc1 = BUUseCase1(conf=user_conf)
    # uc1.run_use_case()

    test1 = BUUseCase1ProtoType(_conf=user_conf,
                                _test_mode=True,
                                _verbose=True,
                                _company=bu_imports.CompanyNames.BASELINE,
                                _type=bu_imports.DeviceTypes.TWENTY_FOUR_VALVE_STR,
                                _tcp=tcp)
    test1.run_use_case()

    print("################################### ----YOU ARE A WINNER---- ##############################################")


def main(argv):
    """
    # global ser
    # comport = input('Enter COM Port:')
    :Select comport
    ControllerUseCase1:
        - Coverage Area:
            -Reboot controller
            -Update Firmware
            #TODO - verify configuration
            #TODO -Replace Device
    BaseManagerUseCase1:
        - Coverage Area:
            -Login to url
    """

    # Load in user configured items from text file (necessary for serial connection)
    user_conf = UserConfiguration(os.path.join('common/user_credentials', 'user_credentials_ben_mfg.json'))
    tcp = TcpHandler(55111)

    # uc1 = BUUseCase1(conf=user_conf)
    # uc1.run_use_case()

    test1 = BUUseCase1ProtoType(_conf=user_conf,
                                _test_mode=False,
                                _verbose=True,
                                _company=bu_imports.CompanyNames.BASELINE,
                                _type=bu_imports.DeviceTypes.SINGLE_VALVE_STR,
                                _tcp=tcp)
    test1.run_use_case()

    print("################################### ----YOU ARE A WINNER---- ##############################################")
    # ########################### Test Below line are not finished ###################################################

    exit()

if __name__ == "__main__":
    main(sys.argv[1:])
