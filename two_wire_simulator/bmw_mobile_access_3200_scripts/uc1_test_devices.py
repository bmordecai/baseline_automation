import sys
from time import sleep

# import common.product as helper_methods
from common import helper_methods
from common.configuration import Configuration

# import log_handler for logging functionality
from common.logging_handler import log_handler


__author__ = 'Eldin'


class MobileAccessTestDevices(object):
    """
    Test name:
        - Mobile Access Test Devices

    User Story: \n
        1)  As a user I want to be able to use my phone/tablet to check on the states of my decoders, and what values
            they have. I want these results to be consistent across all platforms.

    Coverage and Objectives:
        1.  When a user goes to Mobile Access and tests a device, we want to verify all of their devices display, with
            correct statuses/descriptions, and they get results back for:
                - Zones
                - Master Valves
                - Moisture Sensors
                - Temperature Sensors
                - Pressure Sensors
                - Event Switches
                - Flow Meters
        2.  For all of these devices, we want to verify the 3200 has the same results as the ones that are displayed in
            Mobile Access.

    Not Covered:
        1.	Actual packets flying up and down. We verify this indirectly by verifying the values but we don't capture
            any packets.
        2.  No database storage is verified.

    Test Overview:
        - Setup all devices we want to verify in the 'test device' tab in Mobile Access:
            - Zones
            - Master Valves
            - Moisture Sensors
            - Temperature Sensors
            - Pressure Sensors
            - Event Switches
            - Flow Meters
        - Log into Mobile Access
        - Browse to the controller I am using
        - Go to the test devices menu
        - Test each device and then verify that the value we are seeing reflects the value actually on the controller

    Test Configuration setup: \n
        - Zones
            - 1 (TSD0001)
            - 200 (TSD0002)
        - Master Valves
            - 1 (TMV0001)
            - 8 (TMV0002)
        - Pumps
            - 1 (TPR0001)
            - 8 (TPR0002)
        - Moisture Sensors
            - 1 (SB00001)
            - 8 (SB00002)
        - Temperature Sensors
            - 1 (TAT0001)
            - 8 (TAT0002)
        - Pressure Sensors
            - 1 (PSF0001)
            - 8 (PSF0002)
        - Event Switches
            - 1 (TPD0001)
            - 8 (TPD0002)
        - Flow Meters
            - 1 (TWF0001)
            - 8 (TWF0002)
    """

    def __init__(self, test_name, user_configuration_instance, json_configuration_file):
        """
        Initialize Use Case instance with the specified parameters. \n

        :param test_name:                       Name of the test to name the controller. \n
        :type test_name:                        str

        :param user_configuration_instance:     User configuration instance created in 'product_assessments.py' \n
        :type user_configuration_instance:      common.user_configuration.UserConfiguration

        :param json_configuration_file:         Name of the json file to use to configure objects for test. \n
        :type json_configuration_file:          str
        """
        self.config = Configuration(test_name=test_name,
                                    user_conf_file=user_configuration_instance,
                                    data_json_file=json_configuration_file)

        self.run_use_case()

    def run_use_case(self):
        """
        This method initializes the test and runs the steps. \n
        """
        try:
            for run_number, browser in enumerate(["chrome"]):  # Firefox taken out for now
                # Resets objects to a known state, creates serial connections, creates all objects.
                self.config.initialize_for_test(connect_to_basemanager=True, web_tests=True)

                # get list of all the steps by function name in the use case
                method_list = [func for func in dir(self) if
                               callable(getattr(self, func)) and func.startswith('step')]

                # sort list in numerical order of numbers in steps step names must be 'step_X'
                sorted_new_list = sorted(method_list, key=lambda x: int(x.split("_")[1]))
                # run each step_1,2,3 esc.
                for method in sorted_new_list:
                    getattr(self, method)()

                # Close the first webdriver to start the second web driver
                if run_number == 0:
                    self.config.resource_handler.web_driver.web_driver.quit()

        except Exception as e:
            helper_methods.print_test_failed(test_name=self.config.test_name)
            # If logging is enabled, do not bomb out on the Exception. Instead, log the error and move on
            # to the next use case in the list
            if log_handler.is_enabled():
                log_handler.exception(message=e.message)
            else:
                raise
        else:
            helper_methods.print_test_passed(self.config.test_name)

        finally:
            # We close the first webdriver and not the second because of this line, which closes the webdriver
            helper_methods.end_controller_test(self.config)

    #################################
    def step_1(self):
        """
        Login to Mobile Access
        """
        helper_methods.print_method_name()
        try:
            self.config.MobileAccess.login_page.go_from_desktop_login_to_mobile_login()
            self.config.MobileAccess.login_page.verify_open()
            self.config.MobileAccess.login_page.enter_login_info()
            self.config.MobileAccess.login_page.click_login_button()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Logged into Mobile Access"
            )

    #################################
    def step_2(self):
        """
        Navigate to your controller and get to the 'Controller Operations' menu, then to the 'Test Device' page
            - Verify your controller status
            - Verify the 'Test Device' menu has popped up
        """
        helper_methods.print_method_name()
        try:
            self.config.MobileAccess.main_page.select_site()
            self.config.MobileAccess.main_page.select_a_controller()
            self.config.MobileAccess.main_page.select_operation_test_device()
            self.config.MobileAccess.test_device_page.verify_open()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Selected the controller we wanted and navigated to the 'Test Device' menu"
            )

    #################################
    def step_3(self):
        """
        Verify Zones
        - Verify we can self test the device
        - Verify the test results that are displayed are consistent with the values returned from the controller
        """
        helper_methods.print_method_name()
        try:
            # Change some values so we aren't just verifying defaults
            self.config.BaseStation3200[1].zones[200].bicoder.set_solenoid_current(_solenoid_current=0.78)
            self.config.BaseStation3200[1].zones[200].bicoder.set_solenoid_voltage(_solenoid_voltage=35.1)
            self.config.BaseStation3200[1].zones[200].bicoder.set_two_wire_drop_value(_value=2.2)
            self.config.BaseStation3200[1].zones[200].bicoder.do_self_test()

            # Zone 1
            self.config.MobileAccess.test_device_page.toggle_zones()
            self.config.MobileAccess.test_device_page.select_zone(_address=1)
            self.config.MobileAccess.test_device_page.do_self_test()
            self.config.MobileAccess.test_device_page.verify_test_result_values_for_zone(_address=1)

            # Zone 200
            self.config.MobileAccess.test_device_page.next_device()
            self.config.MobileAccess.test_device_page.do_self_test()
            self.config.MobileAccess.test_device_page.verify_test_result_values_for_zone(_address=200)

            # Press the back button to reset the menu so we can choose our next device
            self.config.MobileAccess.back_button()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Verified Zones Self Test Attributes"
            )

    #################################
    def step_4(self):
        """
        Verify Master Valves
        - Verify we can self test the device
        - Verify the test results that are displayed are consistent with the values returned from the controller
        """
        helper_methods.print_method_name()
        try:
            # Change some values so we aren't just verifying defaults
            self.config.BaseStation3200[1].master_valves[8].bicoder.set_solenoid_current(_solenoid_current=0.78)
            self.config.BaseStation3200[1].master_valves[8].bicoder.set_solenoid_voltage(_solenoid_voltage=35.1)
            self.config.BaseStation3200[1].master_valves[8].bicoder.set_two_wire_drop_value(_value=2.2)
            self.config.BaseStation3200[1].master_valves[8].bicoder.do_self_test()

            # Master Valve 1
            self.config.MobileAccess.test_device_page.toggle_master_valves()
            self.config.MobileAccess.test_device_page.select_master_valve(_address=1)
            self.config.MobileAccess.test_device_page.do_self_test()
            self.config.MobileAccess.test_device_page.verify_test_result_values_for_master_valve(_address=1)

            # Master Valve 8
            self.config.MobileAccess.test_device_page.next_device()
            self.config.MobileAccess.test_device_page.do_self_test()
            self.config.MobileAccess.test_device_page.verify_test_result_values_for_master_valve(_address=8)

            # Press the back button to reset the menu so we can choose our next device
            self.config.MobileAccess.back_button()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Verified Master Valve Self Test Attributes"
            )

    #################################
    def step_5(self):
        """
        Verify Moisture Sensors
        - Verify we can self test the device
        - Verify the test results that are displayed are consistent with the values returned from the controller
        """
        helper_methods.print_method_name()
        try:
            # Change some values so we aren't just verifying defaults
            self.config.BaseStation3200[1].moisture_sensors[8].bicoder.set_moisture_percent(_percent=28.2)
            self.config.BaseStation3200[1].moisture_sensors[8].bicoder.set_temperature_reading(_temp=89.3)
            self.config.BaseStation3200[1].moisture_sensors[8].bicoder.set_two_wire_drop_value(_value=2.2)
            self.config.BaseStation3200[1].moisture_sensors[8].bicoder.do_self_test()

            # Moisture Sensor 1
            self.config.MobileAccess.test_device_page.toggle_moisture_sensors()
            self.config.MobileAccess.test_device_page.select_moisture_sensor(_address=1)
            self.config.MobileAccess.test_device_page.do_self_test()
            self.config.MobileAccess.test_device_page.verify_test_result_values_for_moisture_sensor(_address=1)

            # Moisture Sensor 8
            self.config.MobileAccess.test_device_page.next_device()
            self.config.MobileAccess.test_device_page.do_self_test()
            self.config.MobileAccess.test_device_page.verify_test_result_values_for_moisture_sensor(_address=8)

            # Press the back button to reset the menu so we can choose our next device
            self.config.MobileAccess.back_button()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Verified Moisture Sensor Self Test Attributes"
            )

    #################################
    def step_6(self):
        """
        Verify Flow Meters
        - Verify we can self test the device
        - Verify the test results that are displayed are consistent with the values returned from the controller
        """
        helper_methods.print_method_name()
        try:
            # Change some values so we aren't just verifying defaults
            self.config.BaseStation3200[1].flow_meters[8].bicoder.set_water_usage(_water_usage=9101)
            self.config.BaseStation3200[1].flow_meters[8].bicoder.set_two_wire_drop_value(_value=2.2)
            self.config.BaseStation3200[1].flow_meters[8].bicoder.do_self_test()

            # Flow Meter 1
            self.config.MobileAccess.test_device_page.toggle_flow_meters()
            self.config.MobileAccess.test_device_page.select_flow_meter(_address=1)
            self.config.MobileAccess.test_device_page.do_self_test()
            self.config.MobileAccess.test_device_page.verify_test_result_values_for_flow_meter(_address=1)

            # Flow Meter 8
            self.config.MobileAccess.test_device_page.next_device()
            self.config.MobileAccess.test_device_page.do_self_test()
            self.config.MobileAccess.test_device_page.verify_test_result_values_for_flow_meter(_address=8)

            # VERIFY JIRA BUG HAS BEEN FIXED
            jira_bug_ref = 'https://baseline.atlassian.net/browse/MM-42'

            try:
                # We need to sleep after the last self test to wait for the packet that triggers the bug to be sent up
                sleep(10)
                # Verify the next device is not a Flow Meter
                self.config.MobileAccess.test_device_page.next_device()
                self.config.MobileAccess.test_device_page.do_self_test()
                self.config.MobileAccess.test_device_page.verify_test_result_values_for_event_switch(_address=1)

                print "Successfully verified that Flow Meters don't duplicate after a self test.\n->\tThis verifies " \
                      "JIRA bug: {0}".format(jira_bug_ref)
            except Exception:
                e_msg = "\nUnable to verify that Flow Meters don't duplicate after a self test.\n->\tThis Verifies " \
                        "JIRA bug: {0}".format(jira_bug_ref)
                raise AssertionError(e_msg)

            # Press the back button to reset the menu so we can choose our next device
            self.config.MobileAccess.back_button()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Verified Flow Meter Self Test Attributes"
            )

    #################################
    def step_7(self):
        """
        Verify Event Switches
        - Verify we can self test the device
        - Verify the test results that are displayed are consistent with the values returned from the controller
        """
        helper_methods.print_method_name()
        try:
            # Change some values so we aren't just verifying defaults
            self.config.BaseStation3200[1].event_switches[8].bicoder.set_contact_open()
            self.config.BaseStation3200[1].event_switches[8].bicoder.set_two_wire_drop_value(_value=2.2)
            self.config.BaseStation3200[1].event_switches[8].bicoder.do_self_test()

            # Event Switch 1
            self.config.MobileAccess.test_device_page.toggle_event_switches()
            self.config.MobileAccess.test_device_page.select_event_switch(_address=1)
            self.config.MobileAccess.test_device_page.do_self_test()
            self.config.MobileAccess.test_device_page.verify_test_result_values_for_event_switch(_address=1)

            # Event Switch 8
            self.config.MobileAccess.test_device_page.next_device()
            self.config.MobileAccess.test_device_page.do_self_test()
            self.config.MobileAccess.test_device_page.verify_test_result_values_for_event_switch(_address=8)

            # Press the back button to reset the menu so we can choose our next device
            self.config.MobileAccess.back_button()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Verified Event Switch Self Test Attributes"
            )

    #################################
    def step_8(self):
        """
        Verify Temperature Sensors
        - Verify we can self test the device
        - Verify the test results that are displayed are consistent with the values returned from the controller
        """
        helper_methods.print_method_name()
        try:
            # Change some values so we aren't just verifying defaults
            self.config.BaseStation3200[1].temperature_sensors[8].bicoder.set_temperature_reading(_degrees=81.9)
            self.config.BaseStation3200[1].temperature_sensors[8].bicoder.set_two_wire_drop_value(_value=2.2)
            self.config.BaseStation3200[1].temperature_sensors[8].bicoder.do_self_test()

            # Temperature Sensor 1
            self.config.MobileAccess.test_device_page.toggle_temperature_sensors()
            self.config.MobileAccess.test_device_page.select_temperature_sensor(_address=1)
            self.config.MobileAccess.test_device_page.do_self_test()
            self.config.MobileAccess.test_device_page.verify_test_result_values_for_temperature_sensor(_address=1)

            # Temperature Sensor 8
            self.config.MobileAccess.test_device_page.next_device()
            self.config.MobileAccess.test_device_page.do_self_test()
            self.config.MobileAccess.test_device_page.verify_test_result_values_for_temperature_sensor(_address=8)

            # Press the back button to reset the menu so we can choose our next device
            self.config.MobileAccess.back_button()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Verified Temperature Sensor Self Test Attributes"
            )

    #################################
    def step_9(self):
        """
        Verify Pumps
        - Verify we can self test the device
        - Verify the test results that are displayed are consistent with the values returned from the controller
        """
        helper_methods.print_method_name()
        try:
            # Change some values so we aren't just verifying defaults
            self.config.BaseStation3200[1].pumps[8].bicoder.set_solenoid_current(_solenoid_current=0.78)
            self.config.BaseStation3200[1].pumps[8].bicoder.set_solenoid_voltage(_solenoid_voltage=35.1)
            self.config.BaseStation3200[1].pumps[8].bicoder.set_two_wire_drop_value(_value=2.2)
            self.config.BaseStation3200[1].pumps[8].bicoder.do_self_test()

            # Pump 1
            self.config.MobileAccess.test_device_page.toggle_pumps()
            self.config.MobileAccess.test_device_page.select_pump(_address=1)
            self.config.MobileAccess.test_device_page.do_self_test()
            self.config.MobileAccess.test_device_page.verify_test_result_values_for_pump(_address=1)

            # Pump 8
            self.config.MobileAccess.test_device_page.next_device()
            self.config.MobileAccess.test_device_page.do_self_test()
            self.config.MobileAccess.test_device_page.verify_test_result_values_for_pump(_address=8)

            # Press the back button to reset the menu so we can choose our next device
            self.config.MobileAccess.back_button()
        except Exception as e:
            e_msg = "\n\tUse Case {0} failed running on {1}.\n" \
                    "\tThe Exception thrown was {2}".format(
                        self.config.test_name,
                        sys._getframe().f_code.co_name,
                        e.msg if hasattr(e, 'msg') and e.msg != "" else e.message)
            raise Exception, Exception(e_msg), sys.exc_info()[2]
        else:
            helper_methods.print_step_success(
                test_name=self.config.test_name,
                msg="Verified Pump Self Test Attributes"
            )


