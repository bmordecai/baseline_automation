import os
from common.logging_handler import LoggingHandler
from common.logging_handler import log_handler
from product_assessments_basestation_3200 import run_use_cases_3200
from product_assessments_basestation_3200_with_substation import run_use_cases_3200_with_substations
from product_assessments_flowstation import run_use_cases_flowstation
# from product_assessments_bacnet import run_use_cases_bacnet
from product_assessments_basestation_1000_ import run_use_cases_1000
from product_assessments_jade import run_use_cases_jade
from product_assessments_basemanager_v1_1000_ import run_use_cases_basemanager_v1_1000
from product_assessments_basemanager_v12_3200 import run_use_cases_basemanager_v12_3200
from product_assessments_basemanager_v16_3200 import run_use_cases_basemanager_v16_3200
from product_assessments_twsimulator import run_use_cases_tw_simulator

########################################################################################################################
#                                                USER CONFIGURATION                                                    #
########################################################################################################################
"""
This section currently contains:
    - A reference to the file path that contains:
        - Your username and password to BaseManager, as well as the url, site name, and company.
        - MAC Address and socket port of your controllers and substations.
        - Path of your browser drivers for selenium testing.
    - One user can have multiple configuration files for different product tests
"""
# Load in user configured items from text file (necessary for serial connection)
user = 'andy'  # This is your folder name under /common/user_credentials
user_configuration_file_name_for_3200 = user + os.sep + 'user_credentials_andy.json'
user_configuration_file_name_for_substation = user + os.sep + 'user_credentials_andy.json'
user_configuration_file_name_for_flowstation = user + os.sep + 'user_credentials_andy.json'
user_configuration_file_name_for_bacnet = user + os.sep +'user_credentials_andy.json'
user_configuration_file_name_for_1000 = user + os.sep +'user_credentials_andy.json'
user_configuration_file_name_for_jade = user + os.sep +'user_credentials_andy.json'
user_configuration_file_name_for_basemanager_1000 = user + os.sep +'user_credentials_andy.json'
user_configuration_file_name_for_basemanager_v12_3200 = user + os.sep +'user_credentials_andy.json'
user_configuration_file_name_for_basemanager_v16_3200 = user + os.sep +'user_credentials_andy.json'
user_configuration_file_name_for_tw_simulator = user + os.sep + 'tw_simulator_and_controller.json'

# This changes the current working directory to be the base (root) of our project
os.chdir('..')

########################################################################################################################
#                                                CONTROL VARIABLES                                                     #
########################################################################################################################
"""
This section currently contains:
    - The numbers of the use cases you would like to run.
        Ex: 'tests_to_run_3200 = [1, 2, 3]' will run 3200 use cases 1, 2, and 3.
        Ex: 'tests_to_run_3200 = opcodes.run_all_use_cases' will run all 3200 use cases.
        EX: 'tests_to_run_3200 = []' will not run any 3200 use cases.
    - A method that allows you to specify if you want the program to terminate on errors, or if you want to log them.
    - The location/name of your log file.
"""

# True - Run through your use cases without stopping on errors. We will log any errors along with starts/passes/fails
# False - Run through your use cases and crash on the first error you encounter.
LoggingHandler.enable_continuous_run_and_logging_for_use_cases(enabled=True)

# The location of the log file if you enable logging above this
log_file_name = "test_suites/_andy_run_automated_test_suite_log.txt"
log_handler.start_logging(logger_name='AutomatedTestSuiteLogger', filename=log_file_name)

# True - Log Serial Port Socket Address and Commands Sent and Data Received
# False - Exclude that data
LoggingHandler.enable_serial_port_logging(enabled=True)

# The location of the serial IO log file
serial_port_io_log_file_name = 'test_suites/_andy_run_automated_test_suite_serial_io_log.txt'
log_handler.start_logging(logger_name='AutomatedTestSuiteSerialPortIOLogger', filename=serial_port_io_log_file_name)

# True - Log incoming data strings received from controller/flow station get_data() calls.
# False - Exclude that data
LoggingHandler.enable_incoming_get_data_logging(enabled=False)

# The location of the get_data() csv log file
get_data_log_file_name = 'test_suites/_andy_run_automated_test_suite_get_data_log.txt'
log_handler.start_logging(logger_name='AutomatedTestSuiteGetDataLogger', filename=get_data_log_file_name)

########################################################################################################################
#                                                                                                                      #
#                                                USE CASE SELECTION                                                    #
#                                                                                                                      #
########################################################################################################################
"""
user_configuration_file_name: The user configuration variable specified at the top of this module
passing_tests: True or False, True if you want to run the passing use cases
failing_tests: True or False, True if you want to run the failing use cases
manual_tests: True or False, True if you want to run the manual use cases
specific_tests: Specify a specific test to run or leave as '[]' to run all test.
    - ex: [1, 2, 3, 4.1] will run tests 1, 2, 3 and 4_1
    - ex: [] will run all tests since you didn't specify a specific one
"""

# Pass in all the variables to run the use cases you selected
########################################################################################################################
#                                                        3200                                                          #
########################################################################################################################
run_use_cases_3200(
    user_configuration_file_name=user_configuration_file_name_for_3200,
    auto_update_fw=False,
    passing_tests=False,
    failing_tests=False,
    manual_tests=False,
    specific_tests=[])
########################################################################################################################
#                                                     SubStation                                                       #
########################################################################################################################
run_use_cases_3200_with_substations(
    user_configuration_file_name=user_configuration_file_name_for_substation,
    auto_update_fw=False,
    passing_tests=False,
    failing_tests=False,
    manual_tests=False,
    specific_tests=[])
########################################################################################################################
#                                                     FlowStation                                                      #
########################################################################################################################
run_use_cases_flowstation(
    user_configuration_file_name=user_configuration_file_name_for_flowstation,
    auto_update_fw=False,
    passing_tests=False,
    failing_tests=True,
    manual_tests=False,
    specific_tests=[155.1])
########################################################################################################################
#                                                      Bacnet                                                          #
########################################################################################################################
# run_use_cases_bacnet(
#     user_configuration_file_name=user_configuration_file_name_for_bacnet,
#     passing_tests=False,
#     failing_tests=False,
#     manual_tests=False,
#     specific_tests=[])
########################################################################################################################
#                                                    1000 TESTS                                                        #
########################################################################################################################
run_use_cases_1000(
    user_configuration_file_name=user_configuration_file_name_for_1000,
    passing_tests=False,
    failing_tests=False,
    manual_tests=False,
    specific_tests=[])
########################################################################################################################
#                                                    JADE TESTS                                                        #
########################################################################################################################
run_use_cases_jade(
    user_configuration_file_name=user_configuration_file_name_for_jade,
    auto_update_fw=False,
    passing_tests=False,
    failing_tests=False,
    manual_tests=False,
    specific_tests=[])
########################################################################################################################
#                                             BASEMANAGER + 1000 TESTS                                                 #
########################################################################################################################
run_use_cases_basemanager_v1_1000(
    user_configuration_file_name=user_configuration_file_name_for_basemanager_1000,
    passing_tests=False,
    failing_tests=False,
    manual_tests=False,
    specific_tests=[])
########################################################################################################################
#                                           BASEMANAGER + V12 3200 TESTS                                               #
########################################################################################################################
# If you want to run these you need to setup your user credentials in the 'old' object directory
# run_use_cases_basemanager_v12_3200(
#     user_configuration_file_name=user_configuration_file_name_for_basemanager_v12_3200,
#     passing_tests=False,
#     failing_tests=False,
#     manual_tests=False,
#     specific_tests=[])
########################################################################################################################
#                                           BASEMANAGER + V16 3200 TESTS                                               #
########################################################################################################################
run_use_cases_basemanager_v16_3200(
    user_configuration_file_name=user_configuration_file_name_for_basemanager_v16_3200,
    auto_update_fw=False,
    passing_tests=False,
    failing_tests=False,
    manual_tests=False,
    specific_tests=[])
########################################################################################################################
#                                           Two-Wire Simulator TESTS                                                   #
########################################################################################################################
run_use_cases_tw_simulator(
    user_configuration_file_name=user_configuration_file_name_for_tw_simulator,
    passing_tests=False,
    failing_tests=False,
    manual_tests=False,
    specific_tests=[])
