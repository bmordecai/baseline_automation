import os
from common.logging_handler import LoggingHandler
from common.logging_handler import log_handler
from common.helper_methods import send_email_from_baseline_auto_tests
from product_assessments_basestation_3200 import run_use_cases_3200

########################################################################################################################
#                                                USER CONFIGURATION                                                    #
########################################################################################################################
"""
This section currently contains:
    - A reference to the file path that contains:
        - Your username and password to BaseManager, as well as the url, site name, and company.
        - MAC Address and socket port of your controllers and substations.
        - Path of your browser drivers for selenium testing.
    - One user can have multiple configuration files for different product tests
"""
# Load in user configured items from text file (necessary for serial connection)
user = 'nightly_build'
user_configuration_file_name_for_3200 = user + os.sep + 'user_credentials_nightly_build_3200.json'

# This changes the current working directory to be the base (root) of our project
os.chdir('../..')

########################################################################################################################
#                                                CONTROL VARIABLES                                                     #
########################################################################################################################
"""
This section currently contains:
    - The numbers of the use cases you would like to run.
        Ex: 'tests_to_run_3200 = [1, 2, 3]' will run 3200 use cases 1, 2, and 3.
        Ex: 'tests_to_run_3200 = opcodes.run_all_use_cases' will run all 3200 use cases.
        EX: 'tests_to_run_3200 = []' will not run any 3200 use cases.
    - A method that allows you to specify if you want the program to terminate on errors, or if you want to log them.
    - The location/name of your log file.
"""

# True - Run through your use cases without stopping on errors. We will log any errors along with starts/passes/fails
# False - Run through your use cases and crash on the first error you encounter.
LoggingHandler.enable_continuous_run_and_logging_for_use_cases(enabled=True)

# The location of the log file if you enable logging above this
log_file_name = "test_suites/nightly_build/_nightly_build_3200_test_suite_log.txt"

log_handler.start_logging(logger_name='AutomatedTestSuiteLogger', filename=log_file_name)

# True - Log Serial Port Socket Address and Commands Sent and Data Received
# False - Exclude that data
LoggingHandler.enable_serial_port_logging(enabled=True)

# The location of the serial IO log file
serial_port_io_log_file_name = 'test_suites/nightly_build/_nightly_build_3200_test_suite_serial_io_log.txt'
log_handler.start_logging(logger_name='AutomatedTestSuiteSerialPortIOLogger', filename=serial_port_io_log_file_name)


########################################################################################################################
#                                                                                                                      #
#                                                USE CASE SELECTION                                                    #
#                                                                                                                      #
########################################################################################################################
"""
user_configuration_file_name: The user configuration variable specified at the top of this module
passing_tests: True or False, True if you want to run the passing use cases
failing_tests: True or False, True if you want to run the failing use cases
manual_tests: True or False, True if you want to run the manual use cases
specific_tests: Specify a specific test to run or leave as '[]' to run all test.
    - ex: [1, 2, 3, 4.1] will run tests 1, 2, 3 and 4_1
    - ex: [] will run all tests since you didn't specify a specific one
"""

# Pass in all the variables to run the use cases you selected
########################################################################################################################
#                                                        3200                                                          #
########################################################################################################################
run_use_cases_3200(
    user_configuration_file_name=user_configuration_file_name_for_3200,
    passing_tests=True,
    failing_tests=False,
    manual_tests=False,
    specific_tests=[])

subject_and_body = log_handler.return_email_subject_and_body(user="BL32v16.6.702 | 3200 Nightly Build Tests")

send_email_from_baseline_auto_tests(_to_list=["eldin@baselinesystems.com",
                                              "tige@baselinesystems.com",
                                              "ahill@baselinesystems.com",
                                              "kchristensen@baselinesystems.com",
                                              "chris@baselinesystems.com",
                                              "tim@baselinesystems.com",
                                              "jwallace@baselinesystems.com",
                                              "dharris@baselinesystems.com"],
                                    _subject=subject_and_body[0],
                                    _body=subject_and_body[1],
                                    _attachment_path=log_file_name)
