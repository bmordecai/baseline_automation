import sys
import os

import common.user_configuration as user_conf_module

import fs_flowstation_scripts.use_case_150 as cuc_flowstation_150_module
import fs_flowstation_scripts.use_case_151 as cuc_flowstation_151_module
import fs_flowstation_scripts.use_case_152 as cuc_flowstation_152_module
import fs_flowstation_scripts.use_case_152_1 as cuc_flowstation_152_1_module
import fs_flowstation_scripts.use_case_152_2 as cuc_flowstation_152_2_module
import fs_flowstation_scripts.use_case_152_4 as cuc_flowstation_152_4_module
import fs_flowstation_scripts.use_case_152_5 as cuc_flowstation_152_5_module
import fs_flowstation_scripts.use_case_153 as cuc_flowstation_153_module
import fs_flowstation_scripts.use_case_154 as cuc_flowstation_154_module
import fs_flowstation_scripts.use_case_154_1 as cuc_flowstation_154_1_module
import fs_flowstation_scripts.use_case_154_2 as cuc_flowstation_154_2_module
import fs_flowstation_scripts.use_case_154_3 as cuc_flowstation_154_3_module
import fs_flowstation_scripts.use_case_154_4 as cuc_flowstation_154_4_module
import fs_flowstation_scripts.use_case_155 as cuc_flowstation_155_module
import fs_flowstation_scripts.use_case_155_1 as cuc_flowstation_155_1_module
import fs_flowstation_scripts.use_case_155_2 as cuc_flowstation_155_2_module
import fs_flowstation_scripts.use_case_155_3 as cuc_flowstation_155_3_module
import fs_flowstation_scripts.use_case_156 as cuc_flowstation_156_module
import fs_flowstation_scripts.use_case_157 as cuc_flowstation_157_module
import fs_flowstation_scripts.use_case_157_1 as cuc_flowstation_157_1_module
import fs_flowstation_scripts.use_case_158 as cuc_flowstation_158_module
import fs_flowstation_scripts.use_case_159 as cuc_flowstation_159_module
import fs_flowstation_scripts.use_case_160 as cuc_flowstation_160_module
import fs_flowstation_scripts.use_case_161 as cuc_flowstation_161_module
import fs_flowstation_scripts.use_case_162_1 as cuc_flowstation_162_1_module
import fs_flowstation_scripts.use_case_162_2 as cuc_flowstation_162_2_module
import fs_flowstation_scripts.use_case_162_3 as cuc_flowstation_162_3_module
import fs_flowstation_scripts.use_case_163 as cuc_flowstation_163_module
import fs_flowstation_scripts.use_case_164 as cuc_flowstation_164_module
import fs_flowstation_scripts.use_case_165 as cuc_flowstation_165_module
import fs_flowstation_scripts.use_case_166 as cuc_flowstation_166_module
import fs_flowstation_scripts.use_case_167 as cuc_flowstation_167_module
import fs_flowstation_scripts.use_case_168 as cuc_flowstation_168_module
import fs_flowstation_scripts.use_case_169 as cuc_flowstation_169_module
import fs_flowstation_scripts.use_case_170 as cuc_flowstation_170_module
import fs_flowstation_scripts.use_case_171 as cuc_flowstation_171_module
import fs_flowstation_scripts.use_case_172 as cuc_flowstation_172_module
import fs_flowstation_scripts.use_case_173 as cuc_flowstation_173_module
import fs_flowstation_scripts.use_case_174 as cuc_flowstation_174_module

_author__ = 'Tige'


def run_use_cases_flowstation(user_configuration_file_name, passing_tests, failing_tests, manual_tests, specific_tests,
                              auto_update_fw=False):
    """
    This is where we will run all of our SubStation + 3200 use cases. \n

    :param user_configuration_file_name:    The name of the configuration file we will be using for the tests. \n
    :type user_configuration_file_name:     str
    
    :param auto_update_fw:    If you want to automatically update firmware versions of controllers to "latest" version.
    :type auto_update_fw:     bool

    :param passing_tests:   If you want to run passing tests. True for yes, False for no. \n
    :type passing_tests:    bool

    :param failing_tests:   If you want to run failing tests. True for yes, False for no. \n
    :type failing_tests:    bool

    :param manual_tests:    If you want to run manual tests. True for yes, False for no. \n
    :type manual_tests:     bool

    :param specific_tests:  Pick which tests you want to run individually. Will run all if the list is empty. \n
    :type specific_tests:   list[int | float]
    """

    # Load in user configured items from text file (necessary for serial connection)
    user_conf = user_conf_module.UserConfiguration(os.path.join('common/user_credentials', user_configuration_file_name))

    # ---------------------------------------------------------------------------------------------------------------- #
    #                                                                                                                  #
    #                                             FlowStation + 3200 TESTS                                             #
    #                                                                                                                  #
    # ---------------------------------------------------------------------------------------------------------------- #
    # ##########################################------Passing Tests------############################################# #
    # ---------------------------------------------------------------------------------------------------------------- #
    if passing_tests:
        if 150 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_150_module.ControllerUseCase150(test_name="FS-UseCase150-FlowStationBasicProgramming",
                                                            user_configuration_instance=user_conf,
                                                            json_configuration_file='FS_update_cn.json')
        if 151 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_151_module.ControllerUseCase151(test_name="FS-UseCase151-FlowStationCreateObjects",
                                                            user_configuration_instance=user_conf,
                                                            json_configuration_file='FS_share_with_flowstation.json')
        if 152 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_152_module.ControllerUseCase152(test_name="FS-UseCase152-LearnFlowMultipleMainlines",
                                                            user_configuration_instance=user_conf,
                                                            json_configuration_file='FS_UC_152_learn_flow_multiple_mainlines.json')
        if 152.1 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_152_1_module.ControllerUseCase152_1(test_name="FS-UC_152_1-LearnFlowMultipleMainlines",
                                                                user_configuration_instance=user_conf,
                                                                json_configuration_file='FS_learn_flow_multiple_mainlines.json')
        if 152.4 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_152_4_module.ControllerUseCase152_4(test_name="FS-UseCase152_4-LearnFlowMultipleMainlines",
                                                                user_configuration_instance=user_conf,
                                                                json_configuration_file='FS_UC_152_learn_flow_'
                                                                                        'multiple_mainlines.json')
        if 152.5 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_152_5_module.ControllerUseCase152_5(test_name="FS-UC152_5-LearnFlowMultipleUpstreamCP",
                                                                user_configuration_instance=user_conf,
                                                                json_configuration_file='FS_UC_152_learn_flow_'
                                                                                        'two_upstream_CPs.json')
        if 153 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_153_module.ControllerUseCase153(test_name="FS-UseCase153-MainlineZoneDelays",
                                                            user_configuration_instance=user_conf,
                                                            json_configuration_file='FS_mainline_zone_delays.json')
        if 154 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_154_module.ControllerUseCase154(test_name="FS-UseCase154-BasicDesignFlow",
                                                            user_configuration_instance=user_conf,
                                                            json_configuration_file='FS_basic_design_flow.json')
        if 154.1 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_154_1_module.ControllerUseCase154_1(test_name="FS-UseCase154_1-ZeroDesignFlow",
                                                                user_configuration_instance=user_conf,
                                                                json_configuration_file='FS_use_case_154_1.json')
        if 154.2 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_154_2_module.ControllerUseCase154_2(test_name="FS-UseCase154_2-MainlinePriorities",
                                                                user_configuration_instance=user_conf,
                                                                json_configuration_file='FS_use_case_154_2_multiple_controllers.json')
        if 154.3 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_154_3_module.ControllerUseCase154_3(test_name="FS-UseCase154_3-BasicDesignFlow",
                                                                user_configuration_instance=user_conf,
                                                                json_configuration_file='use_case_154_3_multiple_controllers.json')
        if 154.4 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_154_4_module.ControllerUseCase154_4(test_name="FS-UseCase154_4-Repro_Swoosh_Flood",
                                                                user_configuration_instance=user_conf,
                                                                json_configuration_file='use_case_154_4_multiple_controllers.json')
        if 155 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_155_module.ControllerUseCase155(test_name="FS-UseCase155-WS-EmptyConditions",
                                                            user_configuration_instance=user_conf,
                                                            json_configuration_file='FS_water_budget.json')
        if 155.1 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_155_1_module.ControllerUseCase155_1(test_name="FS-UseCase155_1-WaterSourceBudgets",
                                                                user_configuration_instance=user_conf,
                                                                json_configuration_file='FS_water_budget_1.json')
        if 155.2 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_155_2_module.ControllerUseCase155_2(test_name="FS-UseCase155_2-WaterSourceBudgets",
                                                                user_configuration_instance=user_conf,
                                                                json_configuration_file='FS_water_budget_2.json')
        if 155.3 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_155_3_module.ControllerUseCase155_3(test_name="FS-UseCase155_3-WaterSourceBudgets",
                                                                user_configuration_instance=user_conf,
                                                                json_configuration_file='FS_water_budget_3.json')
        if 156 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_156_module.ControllerUseCase156(test_name="FS-UC_156-ML-AdvHighLowVariance",
                                                            user_configuration_instance=user_conf,
                                                            json_configuration_file='FS_advanced_flow_variance.json')
        if 158 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_158_module.ControllerUseCase158(test_name="FS-UseCase158-HighFlowVariance",
                                                            user_configuration_instance=user_conf,
                                                            json_configuration_file='FS_flow_variance.json')
        if 159 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_159_module.ControllerUseCase159(test_name="FS-UseCase159-PrimaryLinkedSplitMainlines",
                                                            user_configuration_instance=user_conf,
                                                            json_configuration_file='FS_primary_linked_split_mainlines.json')
        if 160 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_160_module.ControllerUseCase160(test_name="FS-UseCase160-DisconnectConnectBaseManager",
                                                            user_configuration_instance=user_conf,
                                                            json_configuration_file='FS_empty_configuration.json')
        if 161 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_161_module.ControllerUseCase161(test_name="FS-UseCase161-MasterValvesAndPumps",
                                                            user_configuration_instance=user_conf,
                                                            json_configuration_file='FS_mv_pump_test.json')
        if 163 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_163_module.ControllerUseCase163(test_name="FS-UseCase163-POCGroups",
                                                            user_configuration_instance=user_conf,
                                                            json_configuration_file='FS_UC_163.json')
        if 164 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_164_module.ControllerUseCase164(test_name="FS-UseCase164-dynamic_flow",
                                                            user_configuration_instance=user_conf,
                                                            json_configuration_file='FS_basic_flow_test.json')
        if 165 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_165_module.ControllerUseCase165(test_name="FS-UseCase165-ManualRun",
                                                            user_configuration_instance=user_conf,
                                                            json_configuration_file='FS_manual_run.json')
        if 167 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_167_module.ControllerUseCase167(test_name="FS-UseCase167-HighFlowShutdown",
                                                            user_configuration_instance=user_conf,
                                                            json_configuration_file='FS_use_case_166.json')
        if 168 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_168_module.ControllerUseCase168(test_name="FS-UseCase168-TestProgramRunning",
                                                            user_configuration_instance=user_conf,
                                                            json_configuration_file='FS_use_case_166.json')
        if 169 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_169_module.ControllerUseCase169(test_name="FS-UseCase169-FlowVarianceStatusAndClear",
                                                            user_configuration_instance=user_conf,
                                                            json_configuration_file='FS_use_case_169.json')

        print("########################### ----FINISHED RUNNING PASSING FLOWSTATION TESTS---- ########################")

    # ---------------------------------------------------------------------------------------------------------------- #
    # ##########################################------Manual Tests------############################################## #
    # ---------------------------------------------------------------------------------------------------------------- #
    # ---------------------------------------------------------------------------------------------------------------- #
    #                                                                                                                  #
    #      (Requires Manual Device/Use-Case Modification)                                                              #
    #                                                                                                                  #
    #    - THESE USE CASES ARE NOT APART OF NIGHTLY BUILD                                                              #
    #    - These use cases require setting breakpoints at specific locations in the test in order to configure         #
    #      a real-device to be in an expected state for the test.                                                      #
    #    - Above each use case is a list of break points to set inside the use case. The breakpoints should point      #
    #      to a "To Do" item which explains what the user needs to do.                                                 #
    #                                                                                                                  #
    # ---------------------------------------------------------------------------------------------------------------- #
    if manual_tests:
        if 152.2 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_152_2_module.ControllerUseCase152_2(test_name="FS-UC_152_2-LearnFlowSwoosh",
                                                                user_configuration_instance=user_conf,
                                                                json_configuration_file='FS_swoosh_config.json')
        if 157 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_157_module.ControllerUseCase157(test_name="FS-UseCase157-BackupRestoreBaseManager",
                                                            user_configuration_instance=user_conf,
                                                            json_configuration_file='FS_back_restore_firmware_updates.json')
        if 157.1 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_157_1_module.ControllerUseCase157_1(test_name="FS-UseCase157_1-BackupRestoreUSB",
                                                                user_configuration_instance=user_conf,
                                                                json_configuration_file='FS_back_restore_firmware_updates.json')
        # Shouldn't be in the night build as of 08/28/2018, will be needed for v16.5 firmware
        if 172 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_172_module.ControllerUseCase172(test_name="FS-UseCase172-BugTest",
                                                            user_configuration_instance=user_conf,
                                                            json_configuration_file='FS_use_case_172.json')
        if 173 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_173_module.ControllerUseCase173(test_name="FS-UseCase173-WaterSourcePriority",
                                                            user_configuration_instance=user_conf,
                                                            json_configuration_file='FS_use_case_173.json')
        # Place breakpoints at the following line numbers
        # line 461, 468, 471, 475
        # follow the print statment instructions to complete the test
        if 174 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_174_module.ControllerUseCase174(test_name="FS-UseCase174-FS_32_Connection",
                                                            user_configuration_instance=user_conf,
                                                            json_configuration_file='FS_use_case_173.json')

        print("########################### ----FINISHED RUNNING MANUAL FLOWSTATION TESTS---- #########################")

    # ---------------------------------------------------------------------------------------------------------------- #
    # ##########################################------Failing Tests------############################################# #
    # ---------------------------------------------------------------------------------------------------------------- #

    if failing_tests:
        # TODO: FlowStation reboot crashes test
        if 162.1 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_162_1_module.ControllerUseCase162_1(test_name="FS-UseCase162_1-POCPressureShutdown",
                                                                user_configuration_instance=user_conf,
                                                                json_configuration_file='FS_UC_162_1.json')
        # TODO: FlowStation reboot crashes test
        if 162.2 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_162_2_module.ControllerUseCase162_2(test_name="FS-UseCase162_2-POCHighFlowShutdown",
                                                                user_configuration_instance=user_conf,
                                                                json_configuration_file='FS_high_flow_shutdown.json')
        # TODO: JIRA #FLOWSTN-321, unscheduled flows not working
        if 162.3 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_162_3_module.ControllerUseCase162_3(test_name="FS-UseCase162_3-POCUnscheduledFlowShutdown",
                                                                user_configuration_instance=user_conf,
                                                                json_configuration_file='FS_UC_162_3.json')
        # TODO: Stuck in near infinite loop and fails. LF isn't finishing when it should
        if 166 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_166_module.ControllerUseCase166(test_name="FS-UseCase166-OfflineControllerFlow",
                                                            user_configuration_instance=user_conf,
                                                            json_configuration_file='FS_use_case_166.json')
        if 170 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_170_module.ControllerUseCase170(test_name="CN-UseCase170-ZPs across ML",
                                                            user_configuration_instance=user_conf,
                                                            json_configuration_file='FS_across_mainlines.json')
        if 171 in specific_tests or len(specific_tests) == 0:
            cuc_flowstation_171_module.ControllerUseCase171(test_name="FS-UseCase171-LonePOC",
                                                            user_configuration_instance=user_conf,
                                                            json_configuration_file='FS_use_case_171.json')

        print("########################### ----FINISHED RUNNING FAILING FLOWSTATION TESTS---- ########################")

    # ---------------------------------------------------------------------------------------------------------------- #
    # #######################################------Future Needed Tests------########################################## #
    # ---------------------------------------------------------------------------------------------------------------- #
    # TODO tests needed
    # TODO test 1 program all WS,POC,ML through the 3200 Verify all of them at the FlowStation
    # TODO test 1 program all WS,POC,ML through the FlowStation Verify all of them at the 3200
    # TODO Do lean flows using FlowStation
    # TODO Do Manual Run using FlowStation
    # TODO Add Basic flow allocation through BaseManager

    print("######################### ----YOU ARE A WINNER WINNER CHICKEN DINNER---- ##################################")


if __name__ == "__main__":
    run_use_cases_flowstation(user_configuration_file_name="user_credentials_tim_FS_Plus2.json",
                              passing_tests=False,
                              failing_tests=True,
                              manual_tests=False,
                              auto_update_fw=False,
                              specific_tests=[152.4])
    exit()
