import os

import old_32_10_sb_objects_dec_29_2017.common.user_configuration as user_conf_module

# bmw use cases
import bmw_bs3200_v12_scripts.uc1_login_test as uc1_module
import bmw_bs3200_v12_scripts.uc2_subscriptions_test as uc2_module
import bmw_bs3200_v12_scripts.uc3_menu_tab_test as uc3_module
import bmw_bs3200_v12_scripts.uc4_address_devices_test as uc4_module
import bmw_bs3200_v12_scripts.uc5_edit_devices_test as uc5_module
import bmw_bs3200_v12_scripts.uc6_verify_poc_ui_test as uc6_module
import bmw_bs3200_v12_scripts.uc7_map_edit_menu_test as uc7_module
import bmw_bs3200_v12_scripts.uc8_verify_mainline_ui_test as uc8_module

_author__ = 'ben'


def run_use_cases_basemanager_v12_3200(user_configuration_file_name, passing_tests, failing_tests, manual_tests,
                                       specific_tests):
    """
    This is where we will run all of our BaseManager + v12 3200 use cases. \n

    :param user_configuration_file_name:    The name of the configuration file we will be using for the tests. \n
    :type user_configuration_file_name:     str

    :param passing_tests:   If you want to run passing tests. True for yes, False for no. \n
    :type passing_tests:    bool

    :param failing_tests:   If you want to run failing tests. True for yes, False for no. \n
    :type failing_tests:    bool

    :param manual_tests:    If you want to run manual tests. True for yes, False for no. \n
    :type manual_tests:     bool

    :param specific_tests:  Pick which tests you want to run individually. Will run all if the list is empty. \n
    :type specific_tests:   list[int]
    """

    # Load in user configured items from text file (necessary for serial connection)
    path = os.path.join('old_32_10_sb_objects_dec_29_2017/common/user_credentials', user_configuration_file_name)
    user_conf = user_conf_module.UserConfiguration(path)

    # ---------------------------------------------------------------------------------------------------------------- #
    #                                                                                                                  #
    #                                          BaseManager + v12 3200 Tests                                            #
    #                                                                                                                  #
    # ---------------------------------------------------------------------------------------------------------------- #
    # ---------------------------------------------------------------------------------------------------------------- #
    # ##########################################------Passing Tests------############################################# #
    # ---------------------------------------------------------------------------------------------------------------- #
    if passing_tests:
        if 2 in specific_tests or len(specific_tests) == 0:
            uc2_module.BaseManagerUseCase2(controller_type="32",
                                           test_name="BM-UC2-SubscriptionTest-v12-3200",
                                           user_configuration_instance=user_conf,
                                           json_configuration_file='update_cn.json')

        if 3 in specific_tests or len(specific_tests) == 0:
            uc3_module.BaseManagerUseCase3(controller_type="32",
                                           test_name="BM-UC3-MenuTabTest-v12-3200",
                                           user_configuration_instance=user_conf,
                                           json_configuration_file='update_cn.json')

        if 4 in specific_tests or len(specific_tests) == 0:
            uc4_module.BaseManagerUseCase4(controller_type="32",
                                           test_name="BM-UC4-AddressDevTest-v12-3200",
                                           user_configuration_instance=user_conf,
                                           json_configuration_file='update_cn.json')

        if 5 in specific_tests or len(specific_tests) == 0:
            uc5_module.BaseManagerUseCase5(controller_type="32",
                                           test_name="BM-UC5-EditDevicesTest-v12-3200",
                                           user_configuration_instance=user_conf,
                                           json_configuration_file='update_cn.json')

        if 6 in specific_tests or len(specific_tests) == 0:
            uc6_module.BaseManagerUseCase6(controller_type="32",
                                           test_name="BM-UC6-VerifyPocUiTest-v12-3200",
                                           user_configuration_instance=user_conf,
                                           json_configuration_file='bmw_poc_detail.json')

        if 7 in specific_tests or len(specific_tests) == 0:
            uc7_module.BaseManagerUseCase7(controller_type="32",
                                           test_name="BM-UC7-VerifyMapsTest-v12-3200",
                                           user_configuration_instance=user_conf,
                                           json_configuration_file='update_cn.json')

        if 8 in specific_tests or len(specific_tests) == 0:
            uc8_module.BaseManagerUseCase8(controller_type="32",
                                           test_name="BM-UC8-VerifyMainlineUiTest-v12-3200",
                                           user_configuration_instance=user_conf,
                                           json_configuration_file='bmw_ml_detail.json')

        print("################### ----FINISHED RUNNING PASSING BASEMANAGER + V12 3200 TESTS---- #####################")

    # ---------------------------------------------------------------------------------------------------------------- #
    # ##########################################------Manual Tests------############################################## #
    # ---------------------------------------------------------------------------------------------------------------- #
    # ---------------------------------------------------------------------------------------------------------------- #
    #                                                                                                                  #
    #      (Requires Manual Device/Use-Case Modification)                                                              #
    #                                                                                                                  #
    #    - THESE USE CASES ARE NOT APART OF NIGHTLY BUILD                                                              #
    #    - These use cases require setting breakpoints at specific locations in the test in order to configure         #
    #      a real-device to be in an expected state for the test.                                                      #
    #    - Above each use case is a list of break points to set inside the use case. The breakpoints should point      #
    #      to a "To Do" item which explains what the user needs to do.                                                 #
    #                                                                                                                  #
    # ---------------------------------------------------------------------------------------------------------------- #
    if manual_tests:

        print("################### ----FINISHED RUNNING MANUAL BASEMANAGER + V12 3200 TESTS---- ######################")

    # ---------------------------------------------------------------------------------------------------------------- #
    # ##########################################------Failing Tests------############################################# #
    # ---------------------------------------------------------------------------------------------------------------- #
    if failing_tests:
        # TODO: Failing due to JIRA BM-2199 (https://baseline.atlassian.net/browse/BM-2199)
        if 1 in specific_tests or len(specific_tests) == 0:
            uc1_module.BaseManagerUseCase1(controller_type="32",
                                           test_name="BM-UC1-LoginTest-v12-3200",
                                           user_configuration_instance=user_conf,
                                           json_configuration_file='update_cn.json')

        print("################### ----FINISHED RUNNING FAILING BASEMANAGER + V12 3200 TESTS---- #####################")

    # ---------------------------------------------------------------------------------------------------------------- #
    # #######################################------Future Needed Tests------########################################## #
    # ---------------------------------------------------------------------------------------------------------------- #

    print("######################### ----YOU ARE A WINNER WINNER CHICKEN DINNER---- ##################################")


if __name__ == "__main__":
    run_use_cases_basemanager_v12_3200(user_configuration_file_name="user_credentials_eldin.json",
                                       passing_tests=True,
                                       failing_tests=False,
                                       manual_tests=False,
                                       specific_tests=[])
    exit()
