import os

import common.user_configuration as user_conf_module

# bmw use cases
import bmw_mobile_access_3200_scripts.uc1_test_devices as uc1_module

_author__ = 'eldin'


def run_use_cases_mobile_access_3200(user_configuration_file_name, passing_tests, failing_tests, manual_tests,
                                     specific_tests):
    """
    This is where we will run all of our Mobile Access 3200 use cases. \n

    :param user_configuration_file_name:    The name of the configuration file we will be using for the tests. \n
    :type user_configuration_file_name:     str

    :param passing_tests:   If you want to run passing tests. True for yes, False for no. \n
    :type passing_tests:    bool

    :param failing_tests:   If you want to run failing tests. True for yes, False for no. \n
    :type failing_tests:    bool

    :param manual_tests:    If you want to run manual tests. True for yes, False for no. \n
    :type manual_tests:     bool

    :param specific_tests:  Pick which tests you want to run individually. Will run all if the list is empty. \n
    :type specific_tests:   list[int]
    """

    # Load in user configured items from text file (necessary for serial connection)
    user_conf = user_conf_module.UserConfiguration(os.path.join('common/user_credentials', user_configuration_file_name))

    # ---------------------------------------------------------------------------------------------------------------- #
    #                                                                                                                  #
    #                                           BaseManager + v16 3200 Tests                                           #
    #                                                                                                                  #
    # ---------------------------------------------------------------------------------------------------------------- #
    # ---------------------------------------------------------------------------------------------------------------- #
    # ##########################################------Passing Tests------############################################# #
    # ---------------------------------------------------------------------------------------------------------------- #
    if passing_tests:
        if 1 in specific_tests or len(specific_tests) == 0:
            uc1_module.MobileAccessTestDevices(test_name="MA-UseCase1-TestDevices-3200",
                                               user_configuration_instance=user_conf,
                                               json_configuration_file='two_of_each_device.json')

        print("##################### ----FINISHED RUNNING PASSING MOBILE ACCESS 3200 TESTS---- #######################")
    # ---------------------------------------------------------------------------------------------------------------- #
    # ##########################################------Manual Tests------############################################## #
    # ---------------------------------------------------------------------------------------------------------------- #
    # ---------------------------------------------------------------------------------------------------------------- #
    #                                                                                                                  #
    #      (Requires Manual Device/Use-Case Modification)                                                              #
    #                                                                                                                  #
    #    - THESE USE CASES ARE NOT APART OF NIGHTLY BUILD                                                              #
    #    - These use cases require setting breakpoints at specific locations in the test in order to configure         #
    #      a real-device to be in an expected state for the test.                                                      #
    #    - Above each use case is a list of break points to set inside the use case. The breakpoints should point      #
    #      to a "To Do" item which explains what the user needs to do.                                                 #
    #                                                                                                                  #
    # ---------------------------------------------------------------------------------------------------------------- #
    if manual_tests:

        print("################### ----FINISHED RUNNING MANUAL MOBILE ACCESS 3200 TESTS---- #####################")

    # ---------------------------------------------------------------------------------------------------------------- #
    # ##########################################------Failing Tests------############################################# #
    # ---------------------------------------------------------------------------------------------------------------- #
    if failing_tests:

        print("################### ----FINISHED RUNNING FAILING MOBILE ACCESS 3200 TESTS---- #####################")

    # ---------------------------------------------------------------------------------------------------------------- #
    # #######################################------Future Needed Tests------########################################## #
    # ---------------------------------------------------------------------------------------------------------------- #

    print("######################### ----YOU ARE A WINNER WINNER CHICKEN DINNER---- ##################################")


if __name__ == "__main__":
    run_use_cases_mobile_access_3200(user_configuration_file_name="user_credentials_eldin.json",
                                     passing_tests=True,
                                     failing_tests=False,
                                     manual_tests=False,
                                     specific_tests=[])
    exit()

