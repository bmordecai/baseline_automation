import os

import common.user_configuration as user_conf_module

# TWSimulator use cases
from weathertrak_tests import use_case_1
from weathertrak_tests import use_case_2

_author__ = 'Eldin'

user_credentials = "tige/tw_simulator_and_controller.json"


def run_use_cases_tw_simulator(user_configuration_file_name, passing_tests, failing_tests, manual_tests, specific_tests):
    """
    This is where we will run all of our TWSimulator only use cases. \n

    :param user_configuration_file_name:    The name of the configuration file we will be using for the tests. \n
    :type user_configuration_file_name:     str

    :param auto_update_fw:    If you want to automatically update firmware versions of controllers to "latest" version.
    :type auto_update_fw:     bool

    :param passing_tests:   If you want to run TWSimulator passing tests. True for yes, False for no. \n
    :type passing_tests:    bool

    :param failing_tests:   If you want to run TWSimulator failing tests. True for yes, False for no. \n
    :type failing_tests:    bool

    :param manual_tests:    If you want to run TWSimulator manual tests. True for yes, False for no. \n
    :type manual_tests:     bool

    :param specific_tests:  Pick which tests you want to run individually. Will run all if the list is empty. \n
    :type specific_tests:   list[int | float]
    """

    # Load in user configured items from text file (necessary for serial connection)
    user_conf = user_conf_module.UserConfiguration(os.path.join(
        'common/user_credentials/',
        user_configuration_file_name)
    )
    # ---------------------------------------------------------------------------------------------------------------- #
    #                                                                                                                  #
    #                                                 TWSimulator TESTS                                                #
    #                                                                                                                  #
    # ---------------------------------------------------------------------------------------------------------------- #
    # ##########################################------Passing Tests------############################################# #
    # ---------------------------------------------------------------------------------------------------------------- #
    if passing_tests:
        if 1 in specific_tests or len(specific_tests) == 0:
            use_case_1.ControllerUseCase1(test_name="TW-UseCase1-EditingBiCoderValues",
                                          user_configuration_instance=user_conf,
                                          json_configuration_file='100ZN_3MS_3TS_3PS_3FM_3SW.json')
        if 2 in specific_tests or len(specific_tests) == 0:
            use_case_2.ControllerUseCase2(test_name="TW-UseCase2-SettingValuesTWSimulator",
                                          user_configuration_instance=user_conf,
                                          json_configuration_file='100ZN_3MS_3TS_3PS_3FM_3SW.json')

        print("######################## ----FINISHED RUNNING PASSING TWSimulator TESTS---- ###########################")

    # ---------------------------------------------------------------------------------------------------------------- #
    # ##########################################------Manual Tests------############################################## #
    # ---------------------------------------------------------------------------------------------------------------- #
    # ---------------------------------------------------------------------------------------------------------------- #
    #                                                                                                                  #
    #     TWSimulator Tests (Requires Manual Device/Use-Case Modification)                                             #
    #                                                                                                                  #
    #    - THESE USE CASES ARE NOT APART OF NIGHTLY BUILD                                                              #
    #    - These use cases require setting breakpoints at specific locations in the test in order to configure         #
    #      a real-device to be in an expected state for the test.                                                      #
    #    - Above each use case is a list of break points to set inside the use case. The breakpoints should point      #
    #      to a "To Do" item which explains what the user needs to do.                                                 #
    #                                                                                                                  #
    # ---------------------------------------------------------------------------------------------------------------- #
    if manual_tests:

        print("######################## ----FINISHED RUNNING MANUAL TWSimulator TESTS---- ############################")

    # ---------------------------------------------------------------------------------------------------------------- #
    # ##########################################------Failing Tests------############################################# #
    # ---------------------------------------------------------------------------------------------------------------- #
    if failing_tests:

        print("######################## ----FINISHED RUNNING FAILING TWSimulator TESTS---- ###########################")

    # ---------------------------------------------------------------------------------------------------------------- #
    # #######################################------Future Needed Tests------########################################## #
    # ---------------------------------------------------------------------------------------------------------------- #

    print("######################### ----YOU ARE A WINNER WINNER CHICKEN DINNER---- ##################################")


if __name__ == "__main__":
    run_use_cases_tw_simulator(user_configuration_file_name=user_credentials,
                               passing_tests=True,
                               failing_tests=False,
                               manual_tests=False,
                               specific_tests=[])
    exit()
