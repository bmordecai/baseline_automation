\header
Program Setup
\footer
Press NEXT/PREV for more help, BACK to exit
\menu 0  
\page
PROGRAM START TIMES SETUP: Set up to 8
start times for a program

PROGRAM START DAYS SETUP: Select the
days when the program will run

PROGRAM WATER WINDOWS SETUP: Set 
times when watering will/will not
be allowed
\page
CONCURRENT ZONES SETUP: Set the number
of zones that can run at one time

ADVANCED PROGRAMS SETUP: Set the water 
priority for programs
\menu 1
\page
Program Start Times Setup

   1. Verify that the number displayed
      in the Program field matches the
      program that you want to set start
      times for. If you want to select a
      different program, press the + or
      - button to change the program 
      number.
\page
   2. Press the Next button to move to
      the start times grid.
   
   3. Press the + or - button to set the
      time in the first cell. 

   4. Press the Next button to move to
      the next cell in the start times
      grid, and then press the + or -
      button to set additional start
      times as needed.
\menu 2
\page
Program Start Days Setup

   1. Verify that the number displayed
      in the Program field matches the
      program that you want to set start
      days for. If you want to select a
      different program, press the + or
      - button to change the program 
      number.
\page
   2. Press the Next button to move to
      the Schedule Type field where you
      can select one of the following
      intervals:
      
      DAYS OF THE WEEK - After you
      select the Days of the Week
      option, press the Next button to
      set up the weekdays when you want
      the program to start. By default,
      the boxes for the days of the week
\page
      are marked with a Y (Yes), which
      indicates that the program will
      start every day. If you want to
      change a start day, press the Next
      button to select the day, and then
      press the + or - button to change
      the setting.

      INTERVAL DAYS - This option
      enables you to set up a custom
      interval for the program start
\page
      days. Press the Next button to
      move to the Watering Interval Days
      field, and then press the + or -
      button to change the setting. The
      next start date is calculated and
      entered in the Next Start field.
      If you want to change the next
      start date, press the Previous
      button to highlight the Next Start
      field, and then press the - button
      to change the date. 
\page
      EVEN DAYS - Select Even to start
      the program only on the even
      numbered days of the month.

      ODD DAYS - Select Odd to start the
      program only on the odd numbered
      days of the month.

      ODD SKIP 31 - Select Odd Skip 31
      to start the program only on the
      odd numbered days of the month,
\page

      but skip the 31st day in order to
      maintain an every-two-day schedule 
      when crossing to a new month.

      HISTORICAL CALENDAR - Set up
      custom start day intervals for the
      first half and second half of
      every month of the year. This
      watering schedule works best in
      regions where landscapes are
      irrigated all year. 
\menu 3
\page
Program Water Windows Setup

   1. Press the + or - button to
      select the program that you want
      to set up water windows for.

   2. Press the Next button to move to
      the Water Window Type field, and
      then press the + or - button to
      select either Weekly or Daily.
\page
   3. Press the Next button to move to
      the Day of the Week field. If you
      are using the �Weekly� Water
      Window Type, you cannot change the
      setting in this field, but if you
      are using �Daily,� press the + or
      - button to display the day of the
      week that you want to set up water
      windows for.

\page
   4. Press the Next button to move to
      the ON/OFF field.

      If the boxes in the grid are blue
      (allow watering), the field
      displays OFF. Press the Enter
      button to turn all the water
      windows off.  

\page
      If the boxes in the grid are white
      (do not allow watering), the field
      displays ON. Press the Enter
      button to turn all the water
      windows on. 

   5. To make changes to the individual
      boxes in the grid, press the Next
      button. The color of the number in
      the first box changes to red.
\page
      To change the water setting for
      the time/day represented by that
      grid, press the + button.

      To move to next box in the grid,
      press the Next button.

   6. When you have finished making 
      changes, turn the dial to the 
      RUN position.
\menu 4
\page
Concurrent Zones Setup

   1. In the Program column, press the
      + or - button to select the 
      program that you want to set up
      concurrent zones for. 

   2. Press the Next button to move to 
      the Concurrent Zones column, and
      then press the + or - button to 
      change the value in the field.
\page
Note: Consider electrical and water 
constraints when you are setting up 
concurrent zones.
 
   3. Press the Next button to move to
      the Total Allowed field, and 
      then press the + or - button 
      to change the value in the field.
      This field indicates the total 
      number of concurrent zones for
      the controller.
\page
   4. When you have finished making 
      changes, turn the dial to the 
      RUN position.
\menu 4
\page
Advanced Programs Setup

When a start event is reached, the
program with the highest priority
gets first call on the water. The
remaining water can then be used
by lower priority programs.

A higher priority program �preempts� 
lower priority programs and runs to 
completion before the lower ones.
\page
   1. In the Program field, press the
      + or - button to select the 
      program that you want to set 
      the priority for. 

   2. Press the Next button to move 
      to the Water Priority field, 
      and then press the + button 
      to select one of the following
      priorities:
\page
      High - The program that 
      you want to run first

      Medium - The program that
      you want to run after your high
      priority programs have finished
      running

      Low - The program that you 
      want to run after all other 
      programs have run
\page
   3. When you have finished making 
      changes, turn the dial to the
      RUN position.
\page
-----End of Help-----