﻿\header
RUN Screen
\footer
Press NEXT/PREV for more help, BACK to exit  
\page
When the controller dial is in the RUN
position, the main screen displays the 
Zone Status report. 

When there are messages, an indicator
displays in the top-left corner. 

The controller's current state 
displays in the top-right corner.
\page
On the right side of the screen, the
controller gives the following 
information:

   - The software version 
   - The part number and the 
     controller's serial number
   - The status of the BaseManager
     connection
   - The status of the two-wire
   - The total flow measured
\page
The left side of the screen shows the
controller's time and date.

The lower portion of the screen 
displays the status of master valves
and programs in groups of 50 zones.
\page
Colors display in the boxes of the 
grid that represents the master valves
and programs. The colors indicate the
current status of programs, master
valves, flow meters, and zones. 

Refer to the User Manual for an 
explanation of the status colors.
\page
-----End of Help-----