﻿\header
Water Sources Setup
\footer
Press NEXT/PREV for more help, BACK to exit  
\menu 0
\page
ASSIGN DECODERS TO PUMPS/MVS: Assign
installed devices to an address in the
controller

FLOW METERS SETUP: Search for and assign
flow devices with K-value and Offset

ASSIGN DEVICES TO WATER SOURCES: Assign
pump/master valve devices, and flow 
meters to the POCs
\page
WATER SOURCES (POCS) SETUP: Set limits
and budgets for each point of 
connection (POC)

POC EMPTY CONDITIONS: If one of your
water sources is a cistern or
reservoir, you can install a 
moisture sensor to monitor the water
level and stop using that water source
when the water drops below a specified
level.
\page
BOOSTER PUMPS PROGRAM SETUP: If you
have a decrease in water pressure in
a large irrigation system, you can 
install a booster pump to increase 
the pressure. The BaseStation 3200 
allows you to assign a master valve 
decoder as a booster pump. 
\menu 1
\page
Assign Decoders to Pumps/MVs

   1. The Search option should be
      highlighted in the box on the
      left. If it is not highlighted,
      press the + or - button to 
      highlight Search.
\page
   2. Press the Enter button. The 
      system lists the serial numbers
      of all pump/MV devices that it
      finds.

   3. Press the + or - button to
      highlight the serial number of
      the pump/MV device that you want
      to assign, and then press the 
      Enter button. The serial number
\page
      moves from the list on the left
      to an MV address row on the right.

   4. When you have finished making 
      changes, turn the dial to the 
      RUN position.
\menu 2
\page
Flow Meters Setup

   1. Press the + or - button to select
      Search in the Flow Meters column.

   2. Press the Enter button to search
      for flow meters. The system lists
      the serial numbers of all flow 
      meters that it finds.
\page
   3. Press the + or - button to select
      the flow meter that you want to 
      enable.

   4. Press the Next button to move to
      the Enabled field. Press the + 
	  or – button to toggle the value
	  in the field between YES and NO.
\page	  
   5. Press the Next button to move to
      the K-Value field.
	  
      - If the device is a Baseline 
	    flow biCoder, the correct 
		K-Value automatically 
		displays in the field. Press
		the + or – button to change
		the K-Value if necessary.
\page	
      - If the device is not a 
	    Baseline flow biCoder, you
		need to enter the K-Value 
		manually. Find the K-Value 
		for the device in the 
		manufacturer’s documentation,
		and then press the + or – 
		button to enter the number 
		in the K-Value field.
\page
   6. Press the Next button to move to
      the Offset field.
	  
      - If the device is a Baseline 
	    flow biCoder, the correct 
		offset automatically 
		displays in the field. Press
		the + or – button to change
		the offset if necessary.
\page		
      - If the device is not a 
	    Baseline flow biCoder, you
		need to enter the offset 
		manually. Find the offset 
		for the device in the 
		manufacturer’s documentation,
		and then press the + or – 
		button to enter the number 
		in the Offset field.
\page		
      - If the device does not have 
	    an offset value, you can 
		enter the pulses per gallon
		in the Pulses/GAL field to 
		provide the same compensation
		factor as the offset. 		

   7. When you have finished making 
      changes, turn the dial to the 
      RUN position.
\menu 3
\page
Assign Devices to Water Sources

   1. In the Point of Connection 
      field, press the + or - button
      to select the POC that you want
      to assign devices to.

   2. Press the Next button to move 
      to the Pump/MV field, and then
      press the + button to find the
      device that you want to assign.
\page
   3. Press the Next button to move 
      to the Pump/MV Type field, and 
      then press the + button to select 
      one of the following options:

      NORMALLY CLOSED - A traditional 
      valve decoder

      NORMALLY OPEN - A traditional
      valve decoder or part of a 
      Flow+NOMV decoder
\page
      PUMP - A traditional valve 
      decoder (or pump relay decoder)

   4. Press the Next button to move 
      to the Flow Meter field, and 
      then press the + button to find 
      the device that you want to 
      assign.
\page
   5. When you have finished making 
      changes, turn the dial to the 
      RUN position.
\menu 4
\page
Water Sources (POCs) Setup

   1. In the Water Source field, press
      the + button to select the POC
      that you want to set up.

   2. Press the Next button to move 
      to the POC Priority field, and
      then press the + or - button to
      set the priority as High, 
      Medium, or Low. The system will
\page
      use the high priority water 
      source first, and then switch 
      to medium, and finally to low.

      Note: When you have multiple 
      POCs on a mainline, the priority
      setting allows you to use the 
      POC with the lowest cost of 
      water first (high priority), 
      but you can only set priorities
\page
      when you have a normally closed
      master valve installed on the 
      POCs.  

   3. Press the Next button to move to
      the Design Flow GPM field, and 
      then press the + or - button to 
      enter the amount of flow in 
      gallons per minute (GPM) that
      is allowed through this POC.
\page
   4. Press the Next button to move to
      the High Flow Limit GPM field,
      and then press the + or - button
      to enter the maximum amount of
      flow in gallons per minute. 

   5. Press the Next button to move to
      the Shut Down field. 
      If you want programs using this
      POC to be stopped and
\page
      corresponding MVs shut off when
      the flow rate exceeds the limit,
      press the + button to enter a 
      Y (yes) in the field. 

      If you do not want excessive 
      flow to shut down the system, 
      enter an N (no) in the field.
\page
   6. Press the Next button to move to
      the Unscheduled Flow Limit GPM 
      field, and then press the + or 
      - button to enter the maximum 
      amount of unscheduled flow in 
      gallons per minute.

   7. Press the Next button to move to
      the Shut Down field.
\page      If you want the associated MVs 
      to turn off when an unscheduled
      flow exceeds this limit, press 
      the + button to enter a Y (yes)
      in the field. 

      If you do not want excessive 
      flow to shut down the system, 
      enter an N (no) in the field.
\page
   8. Press the Next button to move to
      the Monthly Budget Gal field, 
      and then press the + or - button
      to enter the maximum amount of 
      water that can be used per month
      in gallons.

   9. Press the Next button to move to
      the Shut Down field. 
\page
      If you want the associated MVs 
      to turn off when the amount of 
      water used exceeds this limit, 
      press the + button to enter a 
      Y (yes) in the field. 

      If you do not want excessive 
      water use to shut down the 
      system, enter an N (no) in the 
      field.
\page
   10.Press the Next button to move to
      the Water Rationing field. 

      To enable water rationing, 
      press the + button to display
      Enabled in the field. When 
      you enable water rationing,
      the system determines the 
      daily water ration by dividing
      the number of gallons in the 
\page
      Monthly Budget field by the
      number of days in the month.
      The system will use only the
      ration amount for daily watering. 
\menu 5
\page
POC Empty Conditions

If one of your water sources is a 
cistern or reservoir, you can install
a moisture sensor to monitor the water
level and stop using that water source
when the water drops below a specified
level.
\page
   1. In the POC column, press the + 
      or - button to select the POC 
      that you want to set up the 
      condition for. 

   2. Press the Next button to move to
      the Sensor column, and then 
      press the + or - button to 
      select the sensor that you want
      to monitor the condition.
\page
   3. Press the Next button to move to
      the Use Sensor field. To enable 
      the sensor to monitor the volume
      of water in the POC, press the 
      + button to display Enabled in
      the field.
\page
   4. Press the Next button to move to
      the Wait Time field, and then 
      press the + or - button to enter
      the amount of time that you want
      the system to wait before 
      readingthe sensor again. 

   5. When you have finished making 
      changes, turn the dial to the 
      RUN position.
\menu 6
\page
Booster Pumps Program Setup

If you have a decrease in water 
pressure in a large irrigation system,
you can install a booster pump to 
increase the pressure. The BaseStation
3200 allows you to assign a master
valve decoder as a booster pump. 
\page
   1. In the Decoder field, press the 
      + button to select the master 
      valve decoder that you want to 
      set up as a booster pump.

   2. Press the Next button to move to
      the Use As Booster Pump field, 
      and then press the + button 
      change the setting to Yes.
\page 
   3. Press the Next button to move to
      the Select Programs with Booster
      Pumps field, and then press the 
      + button to select the program 
      that you want to assign the 
      booster pump to.
 
   4. When you have finished making 
      changes, turn the dial to the 
      RUN position.
\page
-----End of Help-----