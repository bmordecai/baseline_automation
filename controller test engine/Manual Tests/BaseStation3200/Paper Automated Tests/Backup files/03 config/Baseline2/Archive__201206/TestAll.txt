-------------------------
Zone 1: VA00001
Status: OK
Valve Current: 0.18
Valve Voltage: 24.0
Two-wire Drop: NA
-------------------------
Zone 2: VA00002
Status: OK
Valve Current: 0.18
Valve Voltage: 24.0
Two-wire Drop: NA
-------------------------
Zone 3: VA00003
Status: OK
Valve Current: 0.18
Valve Voltage: 24.0
Two-wire Drop: NA
-------------------------
Zone 4: VA00006
Status: OK
Valve Current: 0.18
Valve Voltage: 24.0
Two-wire Drop: NA
-------------------------
Zone 5: VA00004
Status: OK
Valve Current: 0.18
Valve Voltage: 24.0
Two-wire Drop: NA
-------------------------
Zone 6: VE00001
Status: OK
Valve Current: 0.18
Valve Voltage: 24.0
Two-wire Drop: NA
-------------------------
Sensor: SB01183
Status: OK
Moisture: 0.0%
Soil Temperature: 77.9
Two-wire Drop: 0.9
-------------------------
Sensor: SB01429
Status: OK
Moisture: 0.0%
Soil Temperature: 76.1
Two-wire Drop: -
-------------------------
FL 2: WF00511
Status: OK
Flow GPM: NA
Used GAL: 3.77
Two-wire Drop: 0.9
-------------------------
FL 1: WMV0135
Status: OK
StatusMV: OK
Flow GPM: NA
Used GAL: 2.29
Valve Current: 0.11
Valve Voltage: 27.2
Two-wire Drop: 0.9
-------------------------
Event Decoder: RP00223
Status: OK
Contacts: Closed
Two-wire Drop: 1.2
-------------------------
Temperature biCoder: AT01098
Status: OK
Temperature: 76.8
Two-wire Drop: 0.9
