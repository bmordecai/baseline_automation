﻿\header
Manual Operations
\footer
Press NEXT/PREV for more help, BACK to exit
\menu 0
\page
RUN ZONE(S) FOR FIXED TIME: Use this
option to manually run one or more 
zones for a specified time.

RUN PUMP OR MV FOR FIXED TIME: Use 
this option to get water to a hose bib
that is attached to a master valve.

RUN PROGRAM(S) FOR FIXED TIME: Use 
this option to manually run one or 
more programs for a specified time.
\page
START OR STOP PROGRAM(S): Use this 
option to start a program and have the
zones water according to the program 
including run times, soak times, and 
sensor conditions.

START OR STOP PROGRAM ZONE(S): Use 
this option to manually run one or 
more zones of a program for their
programmed run times.
\menu 1
\page
Run Zone(s) for Fixed Time

   1. In the Zone column, press the + 
      button to highlight the zone 
      that you want to run.

   2. Press the Next button to move
      to the Run Time column, and 
      then press the + or – button
      to set the amount of time for
      the manual run. 
\page
   3. If you want to add another zone 
      to the manual run, press the 
      Previous button to return to 
      the Zone column, and then repeat
      steps 1 – 2 to set the time for 
      the next zone. 

   4. Press the Next button to move 
      to the Concurrent Zones field.
      If you want more than one zone
\page
      to run at one time during the 
      manual run, press the + button
      to change the number in this 
      field.

   5. When you have finished making 
      changes, turn the dial to the 
      RUN position. When the manual 
      run starts, the main screen 
      shows the details.
\page
   6. If you want to stop the manual 
      run while it is active, press 
      the Back button. 

Note: When the manual run is complete, 
the controller changes the Run Zone(s) 
for Fixed Time settings to Off.
\menu 2
\page
Run Pump or MV for Fixed Time

   1. In the Pump/MV column, press the
      + button to highlight the pump 
      or master valve that you want 
      to run.

   2. Press the Next button to move to
      the Run Time column, and then 
      press the + or – button to set 
\page
      the amount of time for the 
      manual run. 

   3. If you want to add another pump 
      or master valve to the manual 
      run, press the Previous button 
      to return to the Pump/MV column,
      and then repeat steps 1 – 2 to 
      set the time for the next pump 
      or master valve. 
\page
   4. Press the Next button to move to
      the Concurrent Zones field. If 
      you want more than one pump or 
      master valve to run at one time
      during the manual run, press the
      + button to change the number in
      this field.

   5. When you have finished making 
      changes, turn the dial to the 
\page
      RUN position. When the manual 
      run starts, the main screen 
      shows the details.

   6. If you want to stop the manual 
      run while it is active, press 
      the Back button. 

Note: When the manual run is complete,
the controller changes the Run Pump or
MV for Fixed Time settings to Off. 
\menu 3
\page
Run Program(s) for Fixed Time

   1. In the Prog column, press the 
      + button to highlight the 
      program that you want to run.

   2. Press the Next button to move to
     the Zone Time column, and then 
     press the + or – button to set 
     the amount of time for the manual
     run. 
\page
   3. If you want to add another 
      program to the manual run, press
      the Previous button to return to 
      the Prog column, and then repeat 
      steps 1 – 2 to set the time for 
      the next zone.

   4. Press the Next button to move to 
      the Concurrent Zones column. If 
      you want more than one zone to 
\page
      run at one time during the 
      manual run, press the + button 
      to change the number in this 
      field. 

   5. When you have finished making 
      changes, turn the dial to the 
      RUN position. When the manual 
      run starts, the main screen 
      shows the details.
\page
   6. If you want to stop the manual 
      run while it is active, press 
      the Back button. 

Note: When the manual run is complete,
the controller changes the Run 
Program(s) for Fixed Time settings 
to Off. 
\menu 4
\page
Start or Stop Program(s)

Manually Starting a Program

When you use this option to start
a program, the zones water according 
to the program including run times, 
soak times, and sensor conditions.
\page
   1. In the Program/Status column, 
      press the + button to highlight
      an idle or paused program that 
      you want to start. 

   2. Press the Next button to move to
      the Start/Stop column. The field
      in this column displays Start.
\page
   3. Press the + button. The field in
      the Start/Stop column displays 
      Stop, and the program’s status 
      changes to Watering.

   4. Turn the dial to the RUN position.
\page
Manually Stopping an Active Program

   1. In the Program/Status column, 
      press the + button to highlight
      a watering program that you want
      to stop.
\page
   2. Press the Next button to move to
      the Start/Stop column. The field
      in this column displays Stop.

   3. Press the + button. The field in
      the Start/Stop column displays 
      Start, and the program’s status 
      changes to Idle.

   4. Turn the dial to the RUN position.
\menu 5
\page
Start or Stop Program Zone(s)

   1. In the Prog column, press the 
      + button to highlight the 
      program that has the zones that
      you want to run.

   2. Press the Next button to move to
      the Zone/Status column.
\page
   3. Press the + button to highlight 
      the zone that you want to run.

   4. Press the Next button to move 
      to the Start/Stop column. The 
      field in this column displays 
      Start.

   5. Press the + button. The field in
      the Start/Stop column displays 
\page
      Stop, and the program’s status 
      changes to Watering.

   6. If you want to manually run 
      another zone in the program, 
      press the Previous button to 
      return to the Zone/Status 
      column. Repeat steps 4 – 6 to 
      start another zone.  

   7. Turn the dial to the RUN position.
\page
-----End of Help-----