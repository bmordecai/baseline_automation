﻿\header
Alarms/Messages
\footer
Press NEXT/PREV for more help, BACK to exit  
\page
Operator messages are used to indicate
programming errors and to provide 
assistance in fixing common 
installation and operation problems.

Messages can include status or warning
messages, or serious controller 
alarms. The message shows the time and
date of the alert. The message also 
includes suggestions for correcting 
the issue.
\page
1. If there are multiple messages, 
   press the + or - button to scroll
   through the messages.

2. To clear the message, press the 
   Next button to highlight the Delete
   field, and then press the Enter 
   button.
\page
-----End of Help-----