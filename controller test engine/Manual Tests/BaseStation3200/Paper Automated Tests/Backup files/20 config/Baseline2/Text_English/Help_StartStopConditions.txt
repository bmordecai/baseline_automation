﻿\header
Start/Stop Conditions
\footer
Press NEXT/PREV for more help, BACK to exit  
\menu 0
\page
EVENT SWITCHES SETUP: Find and 
configure any event switches that 
you have installed

MOISTURE CONTROL SETUP: Find and 
configure any soil moisture sensors 
that you want to use for start, stop,
pause conditions
\page
TEMPERATURE CONTROL SETUP: Find and
configure any temperature sensors that 
you have installed

EVENT DATES: The BaseStation 3200 
supports eight event days. An event 
day stops all watering and keeps 
all programs from starting on that
date. 
\menu 1
\page
Event Switches Setup

   1. All is highlighted in the 
      Program column. Perform one of
      the following:

      To associate the event switch 
      with the full controller, 
      continue to step 2. 
\page
      To associate the event switch
      with a specific program, press
      the + button to move to that 
      program number. Continue to 
      step 2.

   2. Press the Next button to 
      highlight Search.
\page
   3. Press the Enter button to search 
      for event switches. The serial 
      numbers of the decoders are shown
      in the Event Decoders column. 

   4. Press the + or - button to 
      highlight the serial number of 
      the event switch that you want 
      to assign.
\page
   5. Press the Next button to move to
      the Enable column of the Start 
      field. Press the + button to 
      toggle the setting between the
      following values:

      Off - Tells the controller to 
      ignore this condition
\page
      Equals - Start the program when
      the condition of the switch 
      matches the setting in the Value
      column

   6. Press the Next button to move to
      the Value column of the Start 
      field. Press the + button to 
      toggle the setting between Open
      and Closed.
\page
   7. Press the Next button to move to
      the Enable column of the Stop 
      field. Press the + button to 
      toggle the setting between Off 
      and Equals.

   8. Press the Next button to move to
      the Value column of the Stop 
      field. Press the + button to 
\page
      toggle the setting between Open
      and Closed.

   9. Press the Next button to move to
      the Enable column of the Pause 
      field. Press the + button to 
      toggle the setting between Off 
      and Equals.
\page
   10.Press the Next button to move to
      the Value column of the Pause 
      field. Press the + button to 
      toggle the setting between Open 
      and Closed.

   11.Press the Next button to move to
      the Pause Time field. Press the 
      + or - button to change the 
      value in the field. 
\page
   12.When you have finished making 
      changes, turn the dial to the 
      RUN position.
\menu 2
\page
Moisture Control Setup

   1. All is highlighted in the 
      Program column. Perform one of
      the following:

      To associate the moisture sensor 
      with the full controller, 
      continue to step 2. 
\page
      To associate the moisture sensor
      with a specific program, press
      the + button to move to that 
      program number. Continue to 
      step 2.

   2. Press the Next button to 
      highlight Search.
\page
   3. Press the Enter button to search 
      for moisture sensors. The serial 
      numbers of the sensors are shown
      in the Moisture Sensors column. 

   4. Press the + or - button to 
      highlight the serial number of 
      the moisture sensor that you want 
      to assign.
\page
   5. Press the Next button to move to
      the Enable column of the Start 
      field. Press the + button to 
      toggle the setting between the
      following values:

      Off - Tells the controller to 
      ignore this condition
\page
      Below - Start the program when 
      the soil moisture sensor detects
      a reading that is below the 
      setting in the Value column

      Above - Start the program when
      the soil moisture sensor detects
      a reading that is above the 
      setting in the Value column
\page
      Equals - Start the program when
      the soil moisture sensor detects
      a reading that is equal to the
      setting in the Value column

   6. Press the Next button to move to
      the Value column of the Start 
      field. Press the + button to 
      change the value in the field.
\page
   7. Press the Next button to move to
      the Enable column of the Stop 
      field. Press the + button to 
      toggle the setting between Off,
      Below, Above, and Equals.

   8. Press the Next button to move to
      the Value column of the Stop 
      field. Press the + button to 
      change the value in the field.
\page
   9. Press the Next button to move to
      the Enable column of the Pause
      field. Press the + button to 
      toggle the setting between Off,
      Below, Above, and Equals.

   10.Press the Next button to move to
      the Value column of the Pause 
      field. Press the + button to 
      change the value in the field.
\page
   11.Press the Next button to move to
      the Pause Time field. Press the 
      + or - button to change the value
      in the field.
 
   12.When you have finished making 
      changes, turn the dial to the 
      RUN position.
\menu 3
\page
Temperature Control Setup

   1. All is highlighted in the 
      Program column. Perform one of
      the following:

      To associate the temperature 
      sensor with the full controller, 
      continue to step 2. 
\page
      To associate the temperature
      sensor with a specific program,
      press the + button to move to
      that program number. Continue to 
      step 2.

   2. Press the Next button to 
      highlight Search.
\page
   3. Press the Enter button to search 
      for temperature sensors. The 
      serial numbers of the sensors 
      are shown in the Temperature 
      Sensors column. 

   4. Press the + or - button to 
      highlight the serial number of 
      the temperature sensor that you
      want to assign.
\page
   5. Press the Next button to move to
      the Enable column of the Start 
      field. Press the + button to 
      toggle the setting between the
      following values:

      Off - Tells the controller to 
      ignore this condition
\page
      Below - Start the program when
      the air temperature sensor 
      detects a reading that is below
      the setting in the Value column

      Above - Start the program when
      the air temperature sensor 
      detects a reading that is above
      the setting in the Value column
\page
      Equals - Start the program when
      the air temperature sensor 
      detects a reading that is equal
      to the setting in the Value 
      column

   6. Press the Next button to move to
      the Value column of the Start 
      field. Press the + button to 
      change the value in the field.
\page
   7. Press the Next button to move to
      the Enable column of the Stop 
      field. Press the + button to 
      toggle the setting between Off,
      Below, Above, and Equals.

   8. Press the Next button to move to
      the Value column of the Stop 
      field. Press the + button to 
      change the value in the field.
\page
   9. Press the Next button to move to
      the Enable column of the Pause 
      field. Press the + button to 
      toggle the setting between Off,
      Below, Above, and Equals.

   10.Press the Next button to move to
      the Value column of the Pause 
      field. Press the + button to 
      change the value in the field.
\page
   11.Press the Next button to move to
      the Pause Time field. Press the 
      + or - button to change the 
      value in the field. 

   12.When you have finished making 
      changes, turn the dial to the 
      RUN position.
\menu 4
\page
Event Dates

   1. In the Program column, press the
      + button to highlight the 
      program that you want to set up
      event dates for.

   2. Press the Next button to move to
      the Stop Watering Date column.
\page
   3. Press the + button to set the 
      desired date.

   4. If you want to set up another 
      event day, press the Enter 
      button to move to the next 
      line, and then repeat step 
      3.

   5. If you want to set up an event 
      day for another program, press
      the Previous button to return
      to the Program column, and then
      press the + button to highlight
      the program that you want to 
      set up event dates for.

   6. Repeat steps 3 and 4 to set up
      the date.
 
   7. When you have finished making
      changes, turn the dial to the
      RUN position.
\page
-----End of Help-----