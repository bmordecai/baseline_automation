﻿\header
OFF
\footer
Press NEXT/PREV for more help, BACK to exit  
\page
Turn the dial to the OFF position when
you want to halt all watering for an
indefinite period of time. No watering
cycles will be started. 

Use this dial position for seasonal shutdown. DO NOT power down the 
BaseStation.
\page
When set to OFF, the controller 
enforces the following conditions:

   - All current watering cycles will 
     be stopped and all zones will be
     set to Done.

   - It will power down the two-wire
     (even if it has been set to 
     always be powered up).
\page
-----End of Help-----