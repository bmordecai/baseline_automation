﻿\header
Zone Setup
\footer
Press NEXT/PREV for more help, BACK to exit  
\page
   1. In the Zone field, press the + or
      - button to change the number to
      the zone that you want to modify 
      the settings for.

   2. Press the Next button to move to
      the Zone Mode field. Press the + 
      or - button to change the value
      in the field to one of the 
      following settings:
\page
      Note: If you change the mode of 
      a zone to Primary or from 
      Primary to another mode, you 
      will potentially affect the 
      programming of other zones. 

      Primary - The zone within a 
      scheduling group that all other
      zones are linked to. Changing 
      the programming on this zone 
\page
      will adjust the programming on 
      all linked zones, resulting in
      saved time and consistency. 
      If you are using a biSensor, 
      the primary zone is the zone 
      that the sensor is connected to.

      Linked - Within a zone group, 
      there is one primary zone and 
      all other zones are then 
\page
      “linked” to the primary and 
      will get their programming 
      information from the primary
      (water time, program, schedule,
      etc.)

      Timed - Any zone programmed to 
      water on a time/day schedule, 
      rather than on a smart 
      irrigation schedule
\page
   3. Press the Next button to move to
      the Water Time field.
   
      If you want to change the value
      in the hours place, press the + 
      or - button.

      If you want to change the value 
      in the minutes place, press the
      Next button, and then press the 
      + or - button.
\page
      Note: When you change the water
      time setting for a linked zone,
      notice that a percentage 
      displays to the left of the 
      Water Time field. This 
      percentage compares the new 
      value with the value in the 
      Water Time field for the 
      Primary zone and establishes
      a tracking ratio. The ratio 
\page
      will be maintained when 
      changes are made to the 
      primary zone’s water time.

   4. Press the Next button to move to
      the Cycle Time field. If you are
      using soak cycles for this zone,
      the cycle time defines the 
      duration of each watering cycle
      interspersed with soak times.
\page
      Note: You cannot change the 
      cycle time setting for a linked
      zone.

   5. Press the Next button to move to
      the Soak Time field. If you are
      using soak cycles for this zone,
      the soak time defines the 
      duration of each soaking cycle
      interspersed with watering times.
\page
      Note: You cannot change the soak
      time setting for a linked zone.

   6. Press the Next button to move to
      the Flow field. If you know the 
      gallons per minute flow for this
      zone, you can press the + or - 
      button to enter it in this field.
      You can also perform a Learn 
\page
      Flow operation to update this 
      field. Refer to the User Manual 
      for information about learning 
      flow. 

   7. Press the Next button to move to
      the Enabled field. Press the + 
      or - button to toggle the value 
      in the field between YES and NO.
\page
	  Note: If you want to prevent a
	  zone from running, you can 
	  temporarily set the Enabled 
	  field to NO. When you want the
	  zone to run again, return to 
	  the Zones dial position and 
	  change this setting to YES. 
	  This setting disables the 
	  zone in all programs.
\page	  
   8. The number in the Program field
      in the upper-left corner of the
	  screen indicates what program 
	  these zone settings are for. A
	  zone can be removed from a 
	  program, assigned to a 
	  different program, or assigned
	  to more than one program. If 
	  you want to make a change to 
	  how the zone is assigned to a 
\page	  
	  program, perform one of the 
	  following actions; otherwise,
	  skip to step 9 of this 
	  procedure.
	  
      Note: To move to the Program
	  field, press the Next or 
	  Previous button.
\page	  
      - To remove the zone from
        the current program, 
		press the Previous 
		button to return to the
		Water Time field, and 
		then press the - button
		to remove the watering 
		time in the field. 
\page		
      - To assign the zone to a
        different program, 
		perform the previous 
		task to remove the zone
		from the current 
		program. Move to the 
		Program field, and 
		press the + or - 
		button to change 
		the number to the 
\page		
		new program that 
		you want the zone 
		to be assigned to. 
		
      - To assign the zone 
	    to an additional 
		program, move to the
		Program field, and 
		press the + or - 
		button to change the
\page		
		number to the 
		additional program 
		that you want the 
		zone to be assigned 
		to. Repeat steps 2
		through 6 to 
		configure the zone 
		settings in the new
		program.
\page		
    Note: The lower-left 
	corner of the screen 
	shows what programs the 
	zone is assigned to.

   9. When you have finished making 
      changes, turn the dial to the 
      RUN position.
\page
-----End of Help-----