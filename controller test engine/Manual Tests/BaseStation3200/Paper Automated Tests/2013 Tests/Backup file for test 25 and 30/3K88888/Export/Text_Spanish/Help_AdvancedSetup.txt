﻿\header
Configuración Avanzada
\footer
Presione NEXT/PREVIOUS para mas
ayuda, BACK para salir
\menu 0
\page
SELECT LANGUAGE FOR HELP DISPLAYS 
(SELECCIONE EL IDIOMA PARA LAS 
PANTALLAS DE AYUDA): Establezca el 
idioma Ingles o Español para la 
ayuda en línea

SECURITY SETUP (CONFIGURACION DE 
SEGURIDAD): Establezca 3 niveles de
seguridad para el controlador
\page
DATA MANAGEMENT (USB STORAGE)  
(ADMINISTRACION DE DATOS 
(ALMACENAMIENTO USB) ): Conecte una
memoria USB al controlador a fin de 
respaldar la programación, restaurar
la programación y exportar datos
\page
VALVE POWER SETUP (CONFIGURACION De
POTENCIA DE VALVULA): Ajuste el nivel
de potencia de la válvula de biCoders
que están unidos a las zonas 
asociadas con los solenoides que 
tienen diferentes requisitos de 
potencia
\page
TWO-WIRE SETTINGS (AJUSTES DE 
DOS-CABLES): Indique si la 
alimentación de dos-cables debe 
estar prendido siempre

UPDATE FIRMWARE (BASEMANAGER) 
(Actualización del Firmware): Si el 
controlador está conectado a BaseManager,
utilice esta opción para buscar e 
instalar firmware actualizaciones
\page
SYSTEM SETTINGS (AJUSTES DE SISTEMA):
Cambie el número de serie del 
controlador y actualice el software 
en el controlador
\menu 1
\page
Seleccione el Idioma para la Pantalla 
de Ayuda

   1. En el campo Help Language 
      (Ayuda de Idioma), presione el 
      botón + para cambiar el ajuste
      entre Ingles y Español.

   2. Si realiza un cambio en el 
      ajuste de idioma y quiere
      poner a prueba la Ayuda en 
\page
      Línea, presione el botón Next
      para seleccionar el campo Help 
      (Ayuda) y después presione el 
      botón Enter. La Ayuda en Línea
      se muestra en el idioma 
      seleccionado.

      Para regresar a la pantalla de 
      Help Language (Ayuda de Idioma), 
      presione el botón Back.
\menu 2
\page
Configuración de Seguridad

Puede configurar los siguientes 
niveles de acceso de seguridad:

Administrator (Administrador): Permite
el acceso a todas las funciones del 
controlador
\page
Programmer (Programador): Permite el 
acceso a todas las funciones del
controlador excepto a la función de 
seguridad

Operator (Operador): Permite el acceso
al menú de Run (Ejecución), el menú de 
Manual Run (Ejecución Manual), y el 
menú de Test (Prueba)
\page
Después de activar la seguridad y 
establecer las contraseñas, reinicie 
el controlador. Se les indicara a los
usuarios que introduzcan su contraseña
cuando presionen alguno de los botones
del menú en el controlador. Si el 
usuario no tiene acceso al menú 
el/ella vera el mensaje “Invalid PIN” 
("Contraseña invalida").
\page
Nota Importante! Cuando el usuario 
acceso al controlador con una 
contraseña, el controlador mantendrá 
ese nivel de acceso por una hora.
Cuando ese tiempo se ha terminado, 
cualquiera que trate de utilizar el 
controlador se le indicara que 
introduzca una contraseña.
\page
   1. En el campo Enable Security 
      Levels (Activar Niveles de 
      Seguridad), presione el botón +
      para cambiar entre Enabled 
      (Activar) y Disabled 
      (Desactivar).

   2. Presione el botón Next para 
      resaltar el primer digito del
      PIN de Administrador, y después
\page
      presione el botón Next para 
      resaltar.

   3. Presione el botón Next para 
      moverse al siguiente digito, 
      y después presione el botón + 
      o - para cambiar el número. 
      Repita este paso hasta que 
      haya configurado todos los 
      dígitos de la contraseña.
\page
   4. Presione el botón Next para 
      marcar la siguiente contraseña,
      y después continúe hasta que 
      haya configurado todas las 
      contraseña.

      Nota: Asegúrese de recordar que 
      contraseña esta asociada con 
      cada nivel de acceso de modo 
      que este seguro de dar la 
\page
      contraseña correcta al usuario
      correcto.

   5. Cuando haya terminado de hacer 
      cambios, cambie el marcador a 
      la posición RUN.
\menu 3
\page
Administración de Datos 
(Almacenamiento USB)

   1. Conecte una memoria USB al 
      controlador.

   2. Presione el botón Next para
      resaltar la opción que desea 
      realizar, y después presione
      el botón Enter.
\page
      IMPORTANTE! Nunca borre su 
      programación o los archivos 
      sin tener una copia de 
      seguridad actual disponible
      para restaurarla. 

Consulte el Manual de Usuario para
más información acerca de estas 
opciones.
\menu 4
\page
Configuración de Potencia de la 
Válvula

Ajuste el nivel de potencia de la
válvula de biCoders que están unidos
a las zonas asociadas con los 
solenoides que tienen diferentes 
requisitos de potencia.

   1. Presione el botón + o - para 
      seleccionar la zona y el biCoder
      que desea ajustar.
\page
   2. Presione el botón Next para 
      moverse al campo de Valve Drive
      Power (Potencia de la Válvula),
      y después presione el botón + 
      o - para cambiar el valor.

      Puede ajustar la potencia de la
      válvula para cualquier biCoder 
      de un mínimo de 1 a un máximo
\page
      de 3. Si un biCoder no es 
      compatible con esta 
      característica la palabra 
      “Fixed” ("Fijo") se muestra 
      en el campo de Valve Drive Power
      (Potencia de la Válvula).

   3. Cuando haya terminado de hacer
      cambios, cambie el marcador a 
      la posición RUN.
\menu 5
\page
Ajustes de Dos-Cables

En su condición predeterminada, el 
controlador apaga los dos-cables 
después de un minuto de inactividad.
Sin embargo, si normalmente tiene una
master valve abierta que no esta
conectada a una fuente de alimentación
separada o tiene un botón (COACH's 
BOTON= Botón entrenador) que puede 
\page
ser usado en cualquier momento, debe 
configurar el dos-cables para que 
siempre este encendido.

Además, si esta utilizando un 
dispositivo de prueba para solucionar 
un problema con los dos-cables, debe 
configurar el dos-cables para que 
siempre este encendido.
\page
En el campo de Two-wire Always On 
(Dos-cables Siempre Encendido),
presione el botón + o - para cambiar
entre YES (SI) y NO
\menu 6
\page
Actualización del Firmware 
(BaseManager)

Si hay una actualización disponible del
BaseManager, el controlador muestra la
información en la pantalla.

Nota: Si el controlador no esta 
conectado al Internet, puede actualizar
el Firmware utilizando una memoria USB.
\page
Vaya al sitio de internet del Baseline
(www.baselinesystems.com) para más 
información.

La opción de Update (Actualizar) esta 
resaltada en la esquina inferior 
derecha de la pantalla. Presione el 
botón Enter para iniciar la 
actualización del firmware. El 
controlador será bloqueado mientras
la actualización esta siendo aplicada.
\page
Cuando la actualización se ha 
completado, el controlador se reinicia
y brevemente muestra la versión del 
nuevo firmware.

Nota: Si el firmware no se actualizo 
como se esperaba, por favor llame al 
Baseline Support al numero 
866.294.5847.
\menu 7
\page
Ajustes de Sistema

Recomendamos que utilice la opción de 
System Setting (Ajustes del Sistema)
solo bajo la dirección de Soporte de 
Baseline.

Consulte el manual de usuario para 
más información o llame al soporte 
del Baseline al numero 866-294-5847
\page
-----End of Help-----