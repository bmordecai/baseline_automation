﻿\header
Configuración de las Fuentes de Agua
\footer
Presione NEXT/PREVIOUS para mas
ayuda, BACK para salir
\menu 0
\page
ASSIGN DECODERS TO PUMPS/MVs (ASIGNAR
DECODERS A LAS BOMBAS/MVs): Asigne 
dispositivos instalados a una 
dirección en el controlador

FLOW METERS SETUP (CONFIGURACION DE 
MEDIDORES DE FLUJO): Busque y asigne
dispositivos de flujo con un K-value 
y un offset
\page
ASSIGN DEVICES TO WATER SOURCES 
(ASIGNAR DISPOSITIVOS A FUENTES DE 
AGUA): Asigne bombas/master valves 
y medidores de flujo a los POCs 
(Puntos de Conneccion)

WATER SOURCES (POCS) SETUP 
(CONFIGURACION DE FUENTES DE AGUA
(Puntos de Conneccion = POCS) ): 
Establezca límites y presupuestos 
para cada punto de conneccion (POC)
\page
POC EMPTY CONDITIONS (CONDICIONES DE
VACIO DE PUNTOS DE CONECCION (POC) ): 
Si una de sus fuentes de agua es una
cisterna o un depósito, puede instalar
un sensor de humedad para monitorear 
el nivel del agua y dejar de usar esa
fuente de agua cuando cae por debajo 
del nivel especificado
\page
BOOSTER PUMPS PROGRAM SETUP 
(CONFIGURACION DE PROGRAMA DE BOMBAS 
DE REFUERZO): Si tiene una disminución
de la presión del agua en un sistema 
de riego de larga escala, puede 
instalar una bomba de refuerzo para 
aumentar la presión. La BaseStation 
3200 le permite asignar una master 
valve decoder como una bomba de 
refuerzo.
\menu 1
\page
Asignar Decoders (Decodificadores) a 
las Bombas/MVs

   1. La opción de Search (Búsqueda) 
      debe estar resaltada en la 
      casilla de la izquierda. Si no
      esta resaltada, presione el 
      botón + o - para resaltarla.
\page
   2. Presione el botón Enter. El 
      sistema lista los números de 
      serie de todas las Bombas/MVs
      que encuentra.

   3. Presione el botón + o - para
      resaltar el número de serie
      de la bomba/MV que desea 
      asignar, y después presione 
      el botón Enter. El numero de 
\page
      serie se mueve de la lista de
      en la izquierda a la fila de 
      direcciones de MV en la derecha.

   4. Cuando haya terminado de hacer
      cambios, cambie el marcador a 
      la posición RUN.
\menu 2
\page
Configuración de Medidores de Flujo

   1. Presione el botón + o - para 
      seleccionar la Search (Búsqueda)
      en la columna de Flow Meters 
      (Medidores de Flujo).

   2. Presione el botón Enter para 
      buscar por medidores de flujo.
      El sistema lista el numero de 
      serie de todos los medidores 
      de flujo que encuentra.
\page
   3. Presione el botón + o - para 
      seleccionar el medidor de flujo
      que desea activar.

   4. Presione el botón Next para 
      moverse al campo de Enabled 
      (Activar). Presione el 
      botón + o - para cambiar el 
      valor en el campo entre 
      Yes (SI) y NO.
\page
   5. Presione el botón Next para 
      moverse al campo K-Value 
      (Valor K).

      Si el dispositivo es un biCoder 
      de flujo Baseline, el K-value 
      aparece automáticamente en el 
      campo. Presione el botón + o 
      - para cambiar el K-Value 
      (valor K) si es necesario.
\page
      Si es dispositivo no es un 
      biCoder de flujo Baseline, 
      necesita introducir el K-Value
      (valor K) manualmente. Encuentre
      el K-Value (valor K) del 
      dispositivo en la documentación
      del fabricante, y después 
      presione el botón + o - para 
      introducir el numero en el 
      campo K-Value (Valor K).
\page
   6. Presione el botón Next para 
      moverse al campo Offset.

      Si el dispositivo es un biCoder
      de flujo Baseline el offset 
      correcto aparece automáticamente
      en el campo. Presione el botón +
      o - para cambiar el offset si es
      necesario.
\page
      Si es dispositivo no es un 
      biCoder de flujo Baseline, 
      necesita introducir el offset 
      manualmente. Encuentre el 
      offset del dispositivo en la 
      documentación del fabricante,
      y después presione el botón 
      + o - para introducir el 
      numero en el campo Offset.
\page
      Si el dispositivo no tiene un 
      valor de Offset, puede 
      introducir los pulsos por 
      galón en el campo Pulses/GAL
      (Pulsos/GAL) para proveer el 
      mismo factor de compensación
      como el offset.

   7. Cuando haya terminado de hacer
      cambios, cambie el marcador a 
      la posición RUN.
\menu 3
\page
Asignar dispositivos a las Fuentes de 
Agua

   1. En el campo Point of Connection
      (Punto de Conneccion), presione 
      el botón + o - para seleccionar
      el POC al cual desea asignar 
      los dispositivos.
\page
   2. Presione el botón Next para 
      moverse al campo de Pump/MV 
      (Bomba/MV), y después presione
      el botón + para encontrar el
      dispositivo que desea asignar.

   3. Presione el botón Next para 
      moverse al campo de Pump/MV 
      Type (Tipo de Bomba/MV), y 
      después presione el botón +
      para seleccionar una de las 
      siguientes opciones:
\page
      NORMALLY CLOSED (NORMALMENTE
      CERRADO) - Un decodificador 
      de válvula tradicional. "Un 
      decoder de válvula tradicional"

      NORMALLY OPEN (NORMALMENTE 
      ABIERTO) - Un decodificador 
      de válvula tradicional. "Un 
      decoder de válvula tradicional" 
      o parte de un decodificador de
      Flujo+NOMV.
\page
      PUMP (BOMBA) - Un decodificador
      de válvula tradicional (o 
      decodificador "relé" "relevador"
      de bomba)

   4. Presione el botón Next para 
      moverse a campo Flow Meter 
      (Medidor de Flujo), y después 
      presione el botón + para 
      encontrar el dispositivo que 
      desea asignar.
\page
   5. Cuando haya terminado de hacer 
      cambios, cambie el marcador a 
      la posición RUN.
\menu 4
\page
Configuración de Fuentes de Agua (POCs)

   1. En el campo Water Source (Fuente
      de Agua), presione el botón + 
      para seleccionar el POC (punto 
      de conneccion) que desea 
      configurar.

   2. Presione el botón Next para 
      moverse al campo POC Priority
      (Prioridad de POC), y después 
      presione el botón + o - para 
\page
      establecer la prioridad como 
      High (Alta), Medium (Media), 
      o Low (Baja). El sistema 
      utilizara la fuente de agua 
      con alta prioridad y después
      cambiara a media y finalmente
      a baja.

      Nota: Cuando tiene múltiples 
      POCs en una line principal, la
      configuración de prioridad le
\page
      permite utilizar el POC con el
      menor costo de agua primero 
      (alta prioridad), pero solo 
      puede establecer prioridades
      cuando tiene una válvula
      normalmente cerrada instalada
      en los POCs.

   3. Presione el botón Next para 
      moverse al campo de Design Flow
      GPM (Diseño de Flujo GPM), y 
\page
      después presione el botón + o -
      para introducir la cantidad de
      flujo en galones por minuto 
      (GPM) que están permitidos 
      atravez de este POC.

   4. Presione el botón Next para 
      moverse al campo de High Flow 
      Limit GPM (Limite de Alto Flujo
      GMP), y después presione el 
\page
      botón + o - para introducir la
      cantidad máxima de flujo en 
      galones por minuto.

   5. Presione el botón Next para 
      moverse al campo Shut Down 
      (Apagar). Si desea que los 
      programes que están utilizando 
      este POC se detengan y las 
      correspondientes MVs se apaguen
\page
      cuando el limite de flujo es 
      excedido, presione el botón + 
      para introducir a Y (SI) en el
      campo.

      Si no desea que el flujo 
      excesivo apague el sistema,
      introduzca una N (NO) en el 
      campo.
\page
   6. Presione el botón Next para 
      moverse al campo de Unscheduled 
      Flow Limit GPM (Limite de Flujo
      No Programado GPM), y después 
      presione el botón + o - para 
      introducir la cantidad máxima 
      de flujo no programado en 
      galones por minuto.

   7. Presione el botón Next para 
      moverse al campo Shut Down 
      (Apagar).
\page
      Si desea que las MVs asociadas
      se apaguen cuando un flujo no 
      programado excede este limite,
      presione el botón + para 
      introducir una Y (SI) en el 
      campo.

      Si no desea que el flujo 
      excesivo apague el sistema,
      introduzca una N (NO) en el 
      campo.
\page
   8. Presione el botón Next para 
      moverse al campo Monthly Budget
      Gal (Presupuesto de Galones 
      Mensual), y después presione el 
      botón + o - para introducir la
      cantidad máxima de agua que 
      puede ser usada al mes en 
      galones.

   9. Presione el botón Next para 
      moverse al campo Shut Down 
      (Apagar).
\page
      Si desea que las MVs asociadas
      se apaguen cuando la cantidad 
      de agua usada excede este 
      limite, presione el botón +
      para introducir una Y (SI) en
      el campo.

      Si no desea que el excesivo 
      uso de agua apague el sistema,
      introduzca una N (NO) en el 
      campo.
\page
  10. Presione el botón Next para 
      moverse al campo de Water 
      Rationing (Racionamiento de
      Agua).

      Para activar el Racionamiento 
      de Agua, presione el botón +
      para mostrar Enabled (Activar)
      en el campo. Cuando activa el 
      racionamiento de agua, el 
\page
      sistema determina la ración
      diaria de agua dividiendo la 
      cantidad de galones en el campo
      de Monthly Budget (Presupuesto
      Mensual) entre la cantidad de 
      día en el mes. El sistema
      solamente utilizara la ración
      de riego diario.
\menu 5
\page
Condiciones de Vacío de POC (PUNTOS DE
CONECCION)

Si una de sus fuentes de agua es una 
cisterna o un depósito, puede instalar
un sensor de humedad para monitorear 
el nivel del agua y dejar de usar esa 
fuente de agua cuando cae por debajo 
del nivel especificado.
\page
   1. En la columna POC (Puntos de 
      Conneccion), presione el botón + 
      o - para seleccionar el POC en 
      el cual desea establecer
      condiciones.

   2. Presione el botón Next para 
      moverse a la columna de Sensor,
      y después presione el botón + 
      o - para seleccionar el sensor
      que desea que monitoree la
      condición.
\page
   3. Presione el botón Next para 
      moverse al campo de Use Sensor
      (Uso de Sensor). Para activar 
      el sensor para que monitoree
      el volumen de agua en el POC, 
      presione el botón + para 
      mostrar Enabled (Activar) en 
      el campo.
\page
   4. Presione el botón Next para 
      moverse al campo Wait Time 
      (Tiempo de Espera), y después
      presione el botón + o - para 
      introducir la cantidad de 
      tiempo que desea que el 
      sistema espere antes de leer 
      el sensor nuevamente.

   5. Cuando haya terminado de hacer
      cambios, cambie el marcador a 
      la posición RUN.
\menu 6
\page
Configuración de Programa de Bombas 
de Refuerzo

Si tiene una disminución de la presión
del agua en un sistema de riego de 
larga escala, puede instalar una bomba
de refuerzo para aumentar la presión.
La BaseStation 3200 le permite asignar
una master valve decoder como una 
bomba de refuerzo.
\page
   1. En el campo Decoder 
      (Decodificador), presione el 
      botón + para seleccionar el
      decodificador master valve 
      que desea establecer como la
      bomba de refuerzo.

   2. Presione el botón Next para 
      moverse al campo Use as Booster
      Pump (Usar Como Bomba de 
      Refuerzo), y después presione
      el botón + para cambiar el 
      ajuste a Yes (SI).
\page
   3. Presione el botón Next para 
      moverse al campo Select 
      Programs with Booster Pumps
      (Seleccionar Programas con 
      Bombas de Refuerzo), y después
      presione el botón + para 
      seleccionar el programa al 
      cual desea asignar la bomba 
      de refuerzo.
\page
   4. Cuando haya terminado de hacer
      cambios, cambie el marcador a 
      la posición RUN.
\page
-----End of Help-----