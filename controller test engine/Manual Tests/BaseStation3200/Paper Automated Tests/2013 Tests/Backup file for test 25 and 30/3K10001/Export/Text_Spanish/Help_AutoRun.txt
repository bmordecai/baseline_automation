﻿\header
Pantalla de RUN (Ejecución)
\footer
Presione NEXT/PREVIOUS para mas
ayuda, BACK para salir
\page
Cuando el marcador del controlador 
esta en la posición RUN (EJECUTAR),
la pantalla principal muestra el 
reporte de Zone Status (Estatus de 
Zona).

Cuando haya mensajes, un indicador 
aparece en la esquina superior 
izquierda.
\page
El estado actual del controlador 
aparece en la esquina superior derecha.

En la parte derecha de la pantalla, el
controlador da la siguiente 
información:

- La versión del Software
- El numero de parte el número de 
  serie del controlador
\page
- El estatus de la conneccion del 
  BaseManager
- El estatus de los dos-cables
- El flujo total medido

El lado izquierdo de la pantalla 
muestra el tiempo y fecha del 
controlador.
\page
La parte de abajo de la pantalla 
muestra el estatus de las master 
valves y los programas en grupos de 
50 zonas.

En las casillas de la cuadricula se 
muestran colores que representan las 
Master Valves y los programas. El 
color indica el estatus actual de los
programas, master valves, medidores 
de flujo y zonas.
\page
Consulte el manual de usuario para 
una explicación de estado de los 
colores.
\page
-----End of Help-----