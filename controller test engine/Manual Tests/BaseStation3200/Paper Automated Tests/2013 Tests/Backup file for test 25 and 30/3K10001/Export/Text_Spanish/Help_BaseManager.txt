﻿\header
Configuración del BaseManager
\footer
Presione NEXT/PREVIOUS para mas
ayuda, BACK para salir
\menu 0
\page
BASEMANAGER SERVER SETUP 
(CONFIGURACION DEL SERVIDOR DEL 
BASEMANAGER): Configure su controlador
para conectarse al servidor del 
BaseManager

ETHERNET SETUP (CONFIGURACION DE 
ETHERNET): Configure su controlador
para conectarse a una red con la 
conneccion de Ethernet incorporada
\menu 1
\page
Configuración del Servidor del 
BaseManager

BaseManager el control central de 
solución del Baseline, lo cual le da
acceso remoto a su BaseStation 3200.
Con el fin de utilizar el BaseManager,
su controlador debe estar conectado a
una red con un modulo de comunicación
tal como Ethernet interno, Wi-Fi o 
\page
Radio Ethernet.También necesita 
acceso a un servidor de BaseManager 
ya sea en un Servidor de Baseline
o en su servidor de organización.

Si va a usar el BaseManager en un 
servidor de Baseline, el BaseStation 
3200 esta preconfigurado para 
conectarse a ese servidor. Después de
conectar el controlador a la red 
usando un modulo de comunicación, 
\page
puede checar el estatus de su 
conneccion en la pantalla de RUN
(EJECUCION), y después revisar los
ajustes en la pantalla del Servidor 
del BaseManager.

Consulte el Manual de Usuario para 
instrucciones especificas.
\menu 2
\page
Configuración de Ethernet

Después de conectar el cable de 
Ethernet en el puerto de Ethernet en
el controlador y conectar el otro 
extremo al conector de Ethernet, el 
controlador intentara conectarse a 
la red con el Protocolo de 
Configuración Dinámica de Cliente 
(DHCP).

Consulte el Manual de Usuario.
\page
-----End of Help-----