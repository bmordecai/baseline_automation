//*****************************************************************************
//	Filename:		Spanish_Text.c
//	Author:			Chris Swenson 
//	Description:
//
//	Copyright (c) 2012 by Baseline Inc
//*****************************************************************************
#include "TCPIP Stack/TCPIP.h"
#include "MainDemo.h"
#include "Graphics.h"

// English text for display text.
// The enum MSG_TEXT under the header provides the index numbers.
ROM char * ROM MSG_ES[] =
{
	"ER", // this is displayed when a bad MSG_EN index is referenced

	// day
	"Domingo",
	"Lunes",
	"Martes",
	"Miercoles",
	"Jueves",
	"Viernes",
	"Sabado",

	// day short
	"Dom",
	"Lun",
	"Mar",
	"Mie",
	"Jue",
	"Vie",
	"Sab",

	// month
	"Enero",
	"Febrero,",
	"Marzo",
	"Abril",
	"Mayo",
	"Junio",
	"Julio",
	"Agosto",
	"Septiembre",
	"Octubre",
	"Noviembre",
	"Diciembre",

	// month short
	"Ene",
	"Feb",
	"Mar",
	"Apr",
	"May",
	"Jun",
	"Jul",
	"Ago",
	"Sep",
	"Oct",
	"Nov",
	"Dic",

///////////////////
// Common text
	"Disactivado",
	"Activado",
	UNCHECKED,
	CHECKED,
	"Apagar",
	"Encender", 	//abrir
	"Ninguno", 		 //nada
	"Todo",
	"Para?",      //stop
	"Empieza?",	//Comienza?

	"Zona",
	"MV/Bomba",
	"Sensor Humedad",		//removing "de" from lots of the following words to make them shorter
	"Sensor Flujo",
	"Interruptor E", 	//evento
	"Sensor Temp",
	"Programa",
	"Fuente Agua",

	"Zonas",
	"MVS / Bombas",
	"Sensor humedad",
	"Sensor Flujo",
	"Interruptores E", //eventos
	"Sensor Temp.",
	"Programas",
	"Fuentes Agua",  //de

	"2-Cables",
	"Estado",
	"Ninguna Asignada",
	"Ayuda",
	"BaseManager Mensaje", // message

/////////////////////////
// Auto run screen titles
	"Estado de Zona",
	"Estado del Prg",
	"Flujo/MV/Bomba",		//Flujo/MV/estado de la bomba
	"Zonas Activas",	//Las zonas en ejecuci�n
	"Ver Mensajes",
	"Ver Diagramas", // View Graphs
	"Demora lluvia", 	//remove "por"
	"D�as de eventos",
	"Estado del sistema",
	
	"Flujo Estado",
	"MV/Bomba Estado",
	"Tiempo",
	"No hay Zonas Activas", //no hay zonas que se ejecutan
	"Retraso", 
	"No hay Mensajes",
	" de ",
	"WS Estado",
	"Flujo Real",
	"Flujo Estimado",
	"D�as de Retraso",	//D�as para retrasar
	"Fija Evento", 		//ajusta el evento
	"Comenzar",		
	"Terminar",		//Final
	"Borra evento", 		//evento abierto

	"Comenzar",
	"Pausar", //pausa
	"Parar",  //estop

////////////////////////
// Event Causes
	"Desconocido",
	"Sistema",
	"Hecho",
	"Demora por Lluvia",	//Demora por lluvia
	"Rain Switch", // NEEDS TRANSLATING
	"Sistema",		//removed "Apagado del"
	"Sistema",		//removed "Pausa de"
	"Espacio para Regar", 	//ventana de aqua
	"Fecha Evento",		//removed "del"
	"D�a y Hora",
	"Humedad",
	"Interruptor E", 	//Evento Interruptor
	"Temperatura",
	"BaseManager",
	"Usuario",
	"Administraci�n",	//Administraci�n
	"Programador",
	"Operador",

/////////////////////////
// Assign Device Menu
	"Buscar y Asignaci�n",
	"Zona",
	"MV/Bomba",
	"Sensor Humedad",		//remove "de"
	"Sensor Flujo",		//removed "de"
	"E. Interruptor",		//Interruptor de evento
	"Sensor Temp.",			// removed "de"

	// setup options
	"Asigna el Aparato",		//Asignaci�n de dispositivo
	"Config del Aparato",		//Configuraci�n del dispositivo

	// device assign
	"Asignaci�n de Zona",		//Zona de Asignaci�n
	"Asign de MV/Bomba",		//MV/Bomba de asignaci�n
	"Asign Sensor Humedad",		//Asignaci�n de humedad
	"Asign Sensor Flujo",			//Asignaci�n de flujo
	"Asign E. Interruptor",			//Asignaci�n de Evento
	"Asign Sensor Temp",		//Asignaci�n Temperatura

	// device setup
	"Config Zonas",		//Config Zonas o estaciones
	"Config MV/Bomba",
	"Config Sensor Humedad",		//changed del to de
	"Config Sensor Flujo",
	"Config E. Interruptor ",		//Config Interruptor de Evento
	"Config Sensor de Temp",		//Config Sensor de temperatura

	// decoder search
	"Comprobando Direcciones",		//Comprobaci�n de direcciones
	"B�squeda de 2-Cables",
	"Aparato Encontrado",		//Se encuentran los dispositivos

	"Buscar",
	"Desasigna" LEFT_ARROW,
	"Asignar",
	"Acci�n",

	"Abierto Normal", // NEEDS TRANSLATING

////////////////////
// Self Test Menu
	"Prueba",
	"Prueba de la Zona",		//remove de and shortened sensor on all of the following
	"Prueba MV/Bomba",
	"P. Sensor de Humedad",
	"P. Sensor de Flujo",
	"Prueba E Interruptor",	//Prueba del interruptor de eventos
	"P. Sensor de Temp",
	"Prueba de 2-Cables",
	"Probar a Todos",

	"Probando ",		//Pruebas
	"Cancelando...",			//Cancelaci�n de ...
	"-Resultos de Prueba-",
	"Errores",
	"Error",

	"Voltaje Ca�da 2-Cables",		//2-Ca�da de tensi�n del cable
	"Versi�n de Aparato",		//Versi�n de Aparato
	"Corriente Solenoide",
	"Voltaje Solenoide",		//Tensi�n de maniobra
	"Cambiar Estado",
	"Tasa Flujo",
	"Uso Total",
	"Temperatura",
	"Voltaje 2-Cables",			//De dos hilos de tensi�n
	"Corriente 2-Cables",		//De dos hilos de corriente
	"Controlador Temp",

	"Cerrado",
	"Abierto",

/////////////////////////
// Flow Setup
	"Flujo Config", 			//Flujo de configuraci�n
	"Config Fuentes de Agua",		//Config de la fuente de agua
	"Aprender Flujo",			//Aprenda de flujo

	// POC setup selection
	"Fuente de Agua 1",
	"Fuente de Agua 2",
	"Fuente de Agua 3",
	"Fuente de Agua 4",
	"Fuente de Agua 5",
	"Fuente de Agua 6",
	"Fuente de Agua 7",
	"Fuente de Agua 8",

	// POC setup options
	"Sensores de flujo",
	"MVS/Bombas",
	"Admin de Flujo", 		//Administraci�n de Flujo
	"Activar/Desactivar",

	// POC management
	"L�mite Zonas Flujo",
	"L�mite de Alto Flujo",		
	"Flujo Inesperado",		//L�mite
	"Tiempo Llenar Tubo",		//Tubo de llenado Tiempo

	// learn flow options
	"Aprenda una Zona",
	"Aprenda el Programa",

	"L�m de Flujo",		//Flujo de L�mite
	"Cerrar Fuente",
	"Flujo",

	"Limite Concurrente",
	"Rellene Tiempo",
	"min", // short for minute

	"Valor K",
	"Pulsos por Gal�n",
	"FA-", //Short for Fuente de Agua
	"gpm",

	// learn flow status
	"Rellene Tiempo Restante",
	"Tiempo Transcurrido",		//Paso del tiempo
	"Zona Actual",
	"Zona �ltima Aprendida",
	"Flujo Apprendido",		//Aprenda el Ritmo de Flujo

	"Aprenda Todas Zonas",  		//omitted "las"
	"Aprenda Una Zona",
	"Aprender de Flujo",
	"Aprenda Flujo Completo",		//Aprenda Flujo Hecho

	// learn flow status
	"Estado",
	"Aprenda Flujo Completado",
	"Aprenda Flujo Terminado",
	"Aprenda Flujo Error",

/////////////////////////
// Manual Run Menu
	"Operacion Manual",
	"Encienda Zona(s)",		//Operacion manual de zona
	"Encienda Todo",		//Operacion manual-toda zonas
	"Encienda MV/Bomba",		//Operacion manual de MV/Bomba
	"Empezar/Apagar Prg",		//Inicio/apagar programas

	"Tiempos de Riego",  		//tiempos para operaction manual
	"Borrar Tiempos",		//Despejar Operacion Manual

	"Demora de Inicio",
	"Demora de Zona",

/////////////////////////
// Program Setup Menu
	"Config de Prg",		//Programa de Configuraci�n
	"Tiempos de Riego",		//Tiempos de caudal por zona
	"Config Prg Empezar",		//Configuraci�n Prg. Comenzar
	"Ajuste Estacional",
	"Ciclos de Remojo",		//Remoje los ciclos
	"Espacios de Riego",		//Agua de ventanas
	"Zonas a la Vez",
	"Activar/Desactivar",
	"Config Prg Pausa",
	"Config Prg Parar",
	"MV/Bomba",
	"Fuente de Agua",

	// start event options
	"D�a y Hora Inicio",		//omit "de" from following
	"Humedad Inicio",
	"E Interruptor Inicio",
	"Temp Inicio",		//Temperatura de Inicio

	// pause event options
	"Humedad Pausa",		//omit La and De
	"E Interruptor Pausa",
	"Temp Pausa",

	// stop event options
	"Parada Humedad",		//La humedad de parada
	"Parada E Interruptor",			//E Interruptor de parada
	"Parada Temp",		//Temperatura de parada

	// start day/time
	"Fija Inicio D�as",		//Establecer los D�as de comienzo
	"Fija Inicio Horas",		//Establecer las horas de inicio

	// day options
	"D�as de la Semana",		//"establecer dias de la semana"
	"Config Intervalo",
	"Config Intervalos ",

	"Requisito",		//Desencadenar
	"Parar",
	"Horas de Pausa",

	"Al Final del Ciclo",
	"Inmediatamente",

	// moisture options
	"Limitar",
	"Menor al L�mite",
	"Superior al L�mite",
	"S�lo en D�a y Hora",		//omit el
	"Calibraci�n",

	// switch options
	"Abierto",		//omit cuando add si
	"Cerrado",		//omit cuando add si

	"Prg ",		// menu program number
	"Mostrar Mensaje",

	// Day Interval
	"Modo",
	"Intervalo de D�as",
	"D�as hasta Inicio",		//D�as hasta el comienzo

	// Day Interval
	"D�a laborable",
	"Par",		//Incluso
	"Impar 31a Saltar",		//Odd 31a Saltar
	"Impar",		//Extra�o
	"Intervalo",
	"Int Inteligentes",

	// Zone Config
	"Habilitado",			
	"Tiempo de Riego",
	"Tipo",

	// Sensor Config
	"Humedad",

	// Soak Config
	"Ciclos de Remojo",		//Remoje los ciclos
	"Tiempo Remojo",		//Tiempo de Mesitas
	"Remojo Inteligente",		

	// concurrent zones
	"M�ximo de una Prg",		//Concurrente M�ximo
	"M�ximo Todas Prgs",		//Concurrente M�ximo en los programas

	// seasonal adjust
	"Ajuste Estacional",

	// Water window
	"Fija la Semana",
	"Fija Cada D�a",
	"Ajustar las horas",
	"Modo",
	"Hora",
	"Clave: " DONE "=On " DISABLED "=Off",

//////////////////
// Aux Menu
	"Config del sistema",		//abreviated lots 
	"Fecha y Hora Config",	
	"Config la Red",
	"Config Pantalla",
	"Config Seguridad",
	"Borrar Programaci�n",		//Programaci�n clara
	"Copiar y Restaurar",			//Backup & Restore
	"Actualizar Firmware",
	"Exportar Datos",

	// display setup
	"Contraste Pantalla",		//omit de la 
	"Brillo",
	"Tiempo de Espera",

	// ethernet setup
	"Config de Ethernet",
	"Info de Ethernet",
	"Servidor Web", 		// translated
	"Config BaseManager ",
	"Info BaseManager ",

	// Backup / restore
	"Copia de Seguridad USB",
	"USB de Restaurar",		//restauraci�n
	"BaseManager Copia de S",
	"BaseManager Restaurar",		//restauraci�n

	// firmware updates
	"Actualizar por USB",//		de
	"BaseManager Actualizar",

	"Escribiendo copia de FW",		//Escibir copia de FW
	"Aplica Actualizacion",		//Aplicacion de actualizacion

	// USB firmware updates status
	"Verificando",		//verificacion espere please
	"Inserte USB",		//inserte la unidad USB
	"1000-0.BIN no encontrado",
	"Actualizaci�n Incompatible",
	"Archivo est� Da�ado",		
	"Archivo est� Bien",

	// BaseManager firmware updates status		all Actualizaci�n changed to archivo for space
	"BaseManager Desconectado",
	"Verificando Archivo",		//Comprobaci�n
	"Archivo No Disponible",
	"Archivo Disponible",
	"Archivo de Corrupto",
	"Error en la descarga",
	"Descarga de Archivo",
	"Archivo Listo",		//actualizacion ready

	"Versi�n Controlador",		//remove del
	"USB Actualizaci�n Firmware",
	"BaseManager Actualizaci�n",
	"Actualizaci�n Versi�n",		//omit de la
	"Nota",
	"Pulse + para Aplicar",
	"Pulse + para Descargar",

	// export
	"Pulse + para Exportar",
	"Exportaci�n de Datos...",

	// clear all
	"Pulse + para Borrar Todo",		// pulse + para borrar todos los
	"Datos de Programaci�n",	//Los Datos de Programaci�n	
	"Borrando Todo...",		//Borrar toda la

	// backup & restore
	"Pulse + para Archivar",
	"Datos de Programaci�n",

	"Pulse + para Restaurar",
	"Escribir Prg...",		//Escribir copia de seguridad
	"Restaurar Prg...",		//""

	"Copia Completo",
	"Copia Restaurada",
	
	"No Copia V�lida Encontrado",

	"Inserte la USB",		//omit unidad
	"No Hay Archivo Backup.csv",
	"Copia Incompatibles",
	"Archivo est� Da�ado",		//archivo de seguridad
	"Archivo est� OK",		//archivo de copia

	// Unused dial
	"Esta Posici�n del Selector",
	"No se Utiliza",

	// Websocket Status
	"Conectando",		//Conexi�n
	"Registrando",		//Registro de
	"Conectado",
	"Desconectado",

	// Time/Date Setup
	"Tiempo",
	"Fecha",
	"D�a Laborable",
	"Tiempo Formato",		//Formato de hora
	"AM / PM",
	"24H",

	// Network Setup
	"Netbios",
	"DHCP Habilitado",
	"Direcci�n IP",
	"Mask",		//M�scara
	"Puerta",
	"DNS 1",
	"DNS 2",
	"MAC",
	"N�m de Serie",

	// Network Status
	"Comprobar el Cable",
	"Descubrimiento de DHCP",
	"Conectado",

	// Web Server Setup
	"Force SSL", // NEEDS TRANSLATING
	"Require PIN", // NEEDS TRANSLATING
	"PIN", // NEEDS TRANSLATING

	// BaseManager Setup
	"BaseManager Activado",
	"Usa IP Alternativa",		//El Uso de IP Alternativa
	"Direcci�n IP",
	"Servidor",
	"Reg. PIN", // NEEDS TRANSLATING

	// Security Setup
	"PIN de Admin",
	"Programador PIN",
	"Operador de PIN",
	"Introduzca el PIN",
	"PIN no V�lido",

///////////////////////
// Dial Posistions
	"Apagado",
	"Operacion Manual",
	"Prueba",
	"Config Sistema",		//Configuraci�n del sistema
	"Ejecutar",
	"Config Programa",		//configuraci�n
	"Config Flujo",		//configuraci�n del flujo
	"Buscar y Asignar",		//asignaci�n

///////////////
// Keys
	"M�s",
	"Menos",
	"Arriba",		//Hasta
	"Abajo",		//Hacia abajo
	"Izquierda",
	"Derecho",

	"OK",		//Aceptar
	"Programa",
	"Idioma",		//Lengua
	"Ayuda",
	"Atr�s",		//Espalda

////////////////////
// Machine status
	"INACTIVA",		//idle
	"ACTIVA",		//funcionamiento
	"DEMORA LLUVIA",
	"RAIN SWITCH", // NEEDS TRANSLATING
	"EVENTO DIA",		//OMIT DEL
	"OP. MANUAL",
	"FALLO FLUJO",		//FLUJO DE FALLO
	"APRENDER FLUJO",
	"AUTO TEST",
	"APAGADO",
	"MENSAJE",

	// poc status
	"Apagado",
	"Activa",		//Funcionimiento
	"Aprender Flujo",		//omit "de"
	"Aparato Error",		//Dispositivo de Error
	"Discapacitado",
	"Fallo de Flujo",		//Flujo de Fallo

	// zone/program status
	"Hecho",		
	"Espera",
	"Activa",		//Funcionamiento
	"Remojo",
	"Pausado",		//Pause
	"Discapacitado",
	"Sin Asignar",		

	// error messages
	"Alerta",
	"Ponga Fecha y Hora",
	"Falta Reloj",		//omit "La" for the next 4 lines
	"Falta Flash",		
	"Falta 2-Cables",
	"Falta Ethernet",
	"Falta Ossilator",		//Si no Ossilator
	"Alta Corriente 2-Cables",		//M�s de dos hilos de corriente

	"No Probado Todav�a",		//No Determinado Todav�a
	"OK",		//Aceptar
	"Error#",

	// base error
	"Suma Error",		//Suma Error Habla Soporte
	"No Responde",		//Basar Sin respuesta
	"Alta Corriente 2-Cables",		//M�s de dos hilos de corriente

	// decoder error
	"No Responde",		//Sin Respuesta
	"Comm Error",

	"Solenoide corto",
	"Solenoide corto",
	"Solenoide Abierto",
	"Voltaje Demasiado Baja",		//Tensi�n demasiado baja
	"N�m Serie no Coincide",

	"Alta Corriente 2-Cables",		//M�s de dos hilos de corriente

	"Falta Flujo Alto",		//De alto flujo
	"Falta Flujo Inesperado",

	"Aparato Error",		//Dispositivo de error
	"Aprenda Flujo Error",
	"Superaci�n de Programa",
	"Error de Calibraci�n",

//--------------------
// Help Text
//--------------------
//	// Time/Date Setup
//	"SET HOUR",
//	"SET MINUTE",
//	"SET MONTH",
//	"SET DAY",
//	"SET YEAR",
//	"SET DAY OF WEEK",
//	"SET TIME FORMAT",
//
//	// Zone Config
//	"SELECT ZONE #",
//	"ENABLE/DISABLE ZONE",
//	"SET RUNTIME (HH:MM:SS)",
//	"SET PROGRAM",
//	"SET ZONE TYPE",
//
//	// Program start times
//	"SELECT PROGRAM",
//	"ENABLE/DISABLE PROGRAM",
//	"SET START TIMES",
//
//	// Program soaks
//	"SET # OF CYCLES TO RUN",
//	"SET SOAK WAIT TIME (HH:MM:SS)",
//
//	// Program start days
//	"SET DAYS TO START",
//	"SET DAYS OF WEEK TO START",
//
//	"SET DAY INTERVAL TO START",
	"FIJA INTERVALO POR CADA 1/2 MEZ",
//
//	// Program start days
//	"SELECT DAY",
//	"SET HOURS TO ALLOW WATERING",
//
//	// Seasonal Adjust
//	"SET SEASONAL ADJUST % VALUE",

	// Main screen help					translate all of the following
	"PULSE BACK POR MAS INFO",
	"PULSE +/- PARA AVANZAR LAS ZONAS",
	"PULSE OK PARA BORRAR MENSAJE",
	"PULSE OK PARA BORRAR FALTA DE FLUJO",

	"PULSE BACK PARA CANCELAR",
	"VER RESULTOS EN RUN",

	"AJUSTA TIEMPO POR BASEMANAGER",

	"PULSE OK PARA CONECTAR",
	"PULSE OK PARA DESCONECTAR",

	"PULSE OK PARA EMPEZAR",
	"PULSE OK PARA BORRAR MENSAJE",


//####################
//1.0 Run Menu
//####################
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"boton para moverse a" NEW_LINE
"la opcion que desee" NEW_LINE
"utilizar y despues" NEW_LINE
"presione OK.",

//####################
//1.2.1 Zone Status Screen
//####################
"Clave del Estado de" NEW_LINE
"Zona" NEW_LINE
UNASSIGNED " = Sin asignar" NEW_LINE
DONE " = Terminado" NEW_LINE
WAITING " = En Espera" NEW_LINE
WATERING " = Regando" NEW_LINE
SOAKING " = Remojando" NEW_LINE
PAUSED " = Pausado" NEW_LINE
DISABLED " = Deshabilitado" NEW_LINE
MESSAGE " = Mensaje" NEW_LINE
NEW_LINE
"Presione los botones" NEW_LINE
"en el lado derecho de" NEW_LINE
"la caratula para" NEW_LINE
"poner el controlador" NEW_LINE
"en los diferentes" NEW_LINE
"modos de operacion." NEW_LINE
NEW_LINE
SELECTED "ENG/ESP" nSELECTED " Cambia" NEW_LINE
"entre la interface" NEW_LINE
"de Ingles y Espa�ol" NEW_LINE
NEW_LINE
SELECTED "?" nSELECTED " Mostrar ayuda para" NEW_LINE
"cualquier pantalla" NEW_LINE
NEW_LINE
SELECTED "-" nSELECTED " Disminuir un valor" NEW_LINE
"en un campo o cambia" NEW_LINE
"entre las opciones" NEW_LINE
"disponibles" NEW_LINE
NEW_LINE
SELECTED "+" nSELECTED " Incrementar un" NEW_LINE
"valor en un campo o" NEW_LINE
"conmutador entre las" NEW_LINE
"opciones disponibles" NEW_LINE
NEW_LINE
SELECTED "PRG" nSELECTED " Seleccionar el" NEW_LINE
"programa que" NEW_LINE
"desea modificar" NEW_LINE
NEW_LINE
SELECTED "BACK" nSELECTED " Regresar a una" NEW_LINE
"pantalla anterior o" NEW_LINE
"cancelar una accion" NEW_LINE
NEW_LINE
SELECTED "FLECHAS" nSELECTED " Mover" NEW_LINE
"dentro de una pantalla" NEW_LINE
NEW_LINE
SELECTED "OK" nSELECTED " Seleccionar una" NEW_LINE
"opcion o realizar" NEW_LINE
"una accion",

//--------------------
//1.2.2 Program Status Screen
//--------------------
"Clave del Estado del Programa" NEW_LINE
DONE " = Terminado" NEW_LINE
WAITING " = En Espera" NEW_LINE
WATERING " = Regando" NEW_LINE
SOAKING " = Remojando" NEW_LINE
PAUSED " = Pausado" NEW_LINE
DISABLED " = Deshabilitado" NEW_LINE
MESSAGE " = Mensaje",

//--------------------
//1.2.3 Flow & MV/Pump Status Screen
//--------------------
"Pantalla del Estado de" NEW_LINE
"Flujo y MV/Bomba" NEW_LINE
NEW_LINE
"Esta pantalla muestra" NEW_LINE
"el estado de su" NEW_LINE
"fuente de agua y" NEW_LINE
"la cantidad de agua" NEW_LINE
"de la fuente de agua" NEW_LINE
"en galones" NEW_LINE
"por minuto (GPM)" NEW_LINE
NEW_LINE
"Presione el " DOWN_ARROW " boton" NEW_LINE
"para ver el informe" NEW_LINE
"de la siguiente fuente" NEW_LINE
"de agua." NEW_LINE
NEW_LINE
"MV/Bomba Clave del" NEW_LINE
"Estado" NEW_LINE
UNASSIGNED " = Sin asignar" NEW_LINE
DONE " = Terminado" NEW_LINE
WATERING " = Regando" NEW_LINE
DISABLED " = Deshabilitado" NEW_LINE
MESSAGE " = Mensaje",

//--------------------
//1.2.4 Running Zones Screen
//--------------------
"Pantalla de Zonas en" NEW_LINE
"Funcionamiento" NEW_LINE
NEW_LINE
"Esta pantalla muestra" NEW_LINE
"las zonas que estan" NEW_LINE
"funcionando y cuenta" NEW_LINE
"el tiempo" NEW_LINE
"restante" NEW_LINE
"Presione el boton + o -" NEW_LINE
"para avanzar" NEW_LINE
"atravez de las zonas.",

//--------------------
//1.2.5 View Messages Screen
//--------------------
"Pantalla de Vista de" NEW_LINE
"Mensajes" NEW_LINE
NEW_LINE
"Esta pantalla muestra" NEW_LINE
"cualquier mensaje" NEW_LINE
"relacionado al estado" NEW_LINE
"del las zonas en el" NEW_LINE
"controlador.",

//--------------------
//1.2.5 View Graphs Screen
//--------------------
"View Graphs" NEW_LINE
NEW_LINE
"This screen displays" NEW_LINE
"graphs",

//--------------------
//1.2.6 Rain Delay Screen
//--------------------
"Pantalla de Retraso" NEW_LINE
"por LLuvia" NEW_LINE
NEW_LINE
"Cuando se espera" NEW_LINE
"lluvia, usted puede" NEW_LINE
"programar un retraso" NEW_LINE
"por lluvia que" NEW_LINE
"prevendra que" NEW_LINE
"el controlador riegue" NEW_LINE
"mientras el retraso" NEW_LINE
"esta activado. El" NEW_LINE
"retraso por lluvia" NEW_LINE
"detiene todos los" NEW_LINE
"programas y establece" NEW_LINE
"los estados de todos" NEW_LINE
"los eventos de regado" NEW_LINE
"a TERMINADO." NEW_LINE
NEW_LINE
"Presione el boton +" NEW_LINE
"o - para establecer" NEW_LINE
"el numero en los dias" NEW_LINE
"a retrasar en el campo" NEW_LINE
"de retraso por el" NEW_LINE
"numero de dias que" NEW_LINE
"desee que el sistema" NEW_LINE
"deje de regar" NEW_LINE
NEW_LINE
"En el controlador," NEW_LINE
"1 dia es desde el" NEW_LINE
"momento de" NEW_LINE
"configurar el" NEW_LINE
"retraso por lluvia" NEW_LINE
"hasta la media" NEW_LINE
"noche del mismo dia." NEW_LINE
"Eal siguinere dia" NEW_LINE
"comienza a las" NEW_LINE
"12:01 am y termina" NEW_LINE
"a la media noche." NEW_LINE
NEW_LINE
"Cuando termine" NEW_LINE
"de programar" NEW_LINE
"el retraso por lluvia" NEW_LINE
"presione el boton" NEW_LINE
"RUN. El estado" NEW_LINE
"de retraso por" NEW_LINE
"lluvia aparece en la" NEW_LINE
"esquina superior" NEW_LINE
"izquierda de la" NEW_LINE
"pantalla.",

//--------------------
//1.2.7 Event Days List Screen
//--------------------
"Pantalla de Lista de" NEW_LINE
"Dias de Eventos" NEW_LINE
NEW_LINE
"Puede configurar hasta" NEW_LINE
"8 eventos que pausaran" NEW_LINE
"el regado. Un dia de" NEW_LINE
"evento bloquea" NEW_LINE
"cualquier regado por" NEW_LINE
"comenzar y pausa" NEW_LINE
"cualquier regado que" NEW_LINE
"estaba en proceso" NEW_LINE
"cuando el dia del" NEW_LINE
"evento comienza." NEW_LINE
"Cuando el dia del" NEW_LINE
"evento termina," NEW_LINE
"el controlador" NEW_LINE
"automaticamente borra" NEW_LINE
"la configuracion" NEW_LINE
"del dia del evento y" NEW_LINE
"reanuda el riego." NEW_LINE
NEW_LINE
"Presione el " DOWN_ARROW " boton" NEW_LINE
"para moverse al dia" NEW_LINE
"del evento que desee" NEW_LINE
"configurar y presione" NEW_LINE
"el boton OK.",

//--------------------
//1.2.7.1 Event Days Set Up Screen
//--------------------
"Pantalla de" NEW_LINE
"Configuracion de Dias" NEW_LINE
"de Evento" NEW_LINE
NEW_LINE
"Presione el " DOWN_ARROW " boton" NEW_LINE
"para moverse al campo" NEW_LINE
"de inicio." NEW_LINE
"Informacion" NEW_LINE
"predeterminada del dia" NEW_LINE
"y del tiempo se" NEW_LINE
"muestra en los campos" NEW_LINE
"de inicio y fin." NEW_LINE
NEW_LINE
"Presione el " RIGHT_ARROW " boton" NEW_LINE
"para moverse a la" NEW_LINE
"fecha de inicio" NEW_LINE
"o el tiempo que desee" NEW_LINE
"cambiar y despues" NEW_LINE
"presione el + o -" NEW_LINE
"boton para cambiar" NEW_LINE
"los numeros." NEW_LINE
NEW_LINE
"Presione el " DOWN_ARROW " boton" NEW_LINE
"para moverse al" NEW_LINE
"campo de fin." NEW_LINE
NEW_LINE
"Presione el " RIGHT_ARROW " boton" NEW_LINE
"para moverse a la" NEW_LINE
"fecha o el tiempo" NEW_LINE
"de finalizado" NEW_LINE
"que desee cambiar" NEW_LINE
"y despues presione el" NEW_LINE
"+ o - boton para" NEW_LINE
"cambiar los numeros.",

//--------------------
//1.2.8 System Status Screen
//--------------------
"Pantalla del Estado" NEW_LINE
"del Sistema" NEW_LINE
NEW_LINE
"Esta pantalla muestra" NEW_LINE
"la corriente y el" NEW_LINE
"voltage del doble" NEW_LINE
"cableado y la" NEW_LINE
"temperatura interna" NEW_LINE
"del controlador.",

//--------------------
//2.0 Program Setup Menu
//--------------------
"Menu de" NEW_LINE
"Configuracion de" NEW_LINE
"Programa" NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"boton para moverse" NEW_LINE
"a la opcion que desee" NEW_LINE
"utilizar y  despues" NEW_LINE
"presione OK." NEW_LINE
NEW_LINE
"Verifique que el" NEW_LINE
"numero correcto" NEW_LINE
"del programa aparezca" NEW_LINE
"en la esquina superior" NEW_LINE
"izquierda. Presione" NEW_LINE
"PRG para cambiarlo" NEW_LINE
"si es necesario.",

//--------------------
//2.1 Zone Runtimes Screen
//--------------------
"Pantalla de Tiempo" NEW_LINE
"de ejecucion de la" NEW_LINE
"Zona" NEW_LINE
NEW_LINE
"El tiempo de" NEW_LINE
"ejecucion de la" NEW_LINE
"Zona es el" NEW_LINE
"tiempo total" NEW_LINE
"que la zona esta" NEW_LINE
"programada para regar." NEW_LINE
"El tiempo de" NEW_LINE
"ejecucion puede o no" NEW_LINE
"ocurrir todo a la vez" NEW_LINE
"dependiendo si tiene" NEW_LINE
"ciclos de remojo" NEW_LINE
"configurados." NEW_LINE
NEW_LINE
"Verifique que el" NEW_LINE
"numero correcto" NEW_LINE
"del programa aparezca" NEW_LINE
"en la esquina superior" NEW_LINE
"izquierda. Presione" NEW_LINE
"PRG para cambiarlo" NEW_LINE
"si es necesario." NEW_LINE
NEW_LINE
"El marcador de horas" NEW_LINE
"en el campo de la" NEW_LINE
"Zona 1 esta resaltado." NEW_LINE
NEW_LINE
"Para mover al" NEW_LINE
"marcador de minutos" NEW_LINE
"o segundos," NEW_LINE
"presione el " RIGHT_ARROW " boton." NEW_LINE
NEW_LINE
"Para cambiar el" NEW_LINE
"tiempo, presione el" NEW_LINE
"+ o - boton." NEW_LINE
NEW_LINE
"Para moverse a una" NEW_LINE
"diferente zona," NEW_LINE
"presione el " UP_ARROW " or " DOWN_ARROW NEW_LINE
"boton." NEW_LINE
NEW_LINE
"Si no desea que una" NEW_LINE
"zona funcione en el" NEW_LINE
"programa seleccionado," NEW_LINE
"asegurese de que" NEW_LINE
"el tiempo de ejecucion" NEW_LINE
"este configurado en" NEW_LINE
"cero.",

//--------------------
//2.2 Setup Prg. Start Screen
//--------------------
"Pantalla de" NEW_LINE
"Configuracion de" NEW_LINE
"PRG de Inicio" NEW_LINE
NEW_LINE
"Un programa no esta" NEW_LINE
"activo hasta que usted" NEW_LINE
"selecciona una" NEW_LINE
"condicion de inicio." NEW_LINE
NEW_LINE
"Inicio de dia y tiempo" NEW_LINE
"opera el" NEW_LINE
"controlador basado en" NEW_LINE
"la configuracion de" NEW_LINE
"dia y tiempo." NEW_LINE
NEW_LINE
"El inicio de" NEW_LINE
"humedecido opera el" NEW_LINE
"controlador basado en" NEW_LINE
"las lecturasde la " NEW_LINE
"humedad del suelo" NEW_LINE
"en el sensor" NEW_LINE
NEW_LINE
"El inicio de cambio" NEW_LINE
"opera el" NEW_LINE
"controlador basado" NEW_LINE
"sobre el estado de" NEW_LINE
"un dispositivo de" NEW_LINE
"cambio." NEW_LINE
NEW_LINE
"El inicio de" NEW_LINE
"Temperatura" NEW_LINE
"opera el" NEW_LINE
"controlador basado" NEW_LINE
"en las lecturas de" NEW_LINE
"la temperatura del" NEW_LINE
"aire en el sensor" NEW_LINE
NEW_LINE
"El mensaje de la" NEW_LINE
"pantalla" NEW_LINE
"Indica al controlador" NEW_LINE
"que muestre un mensaje" NEW_LINE
"siempre que un" NEW_LINE
"programa comienze." NEW_LINE
NEW_LINE
"Presione el " DOWN_ARROW " boton" NEW_LINE
"para moverse a la" NEW_LINE
"opcion de inicio" NEW_LINE
"que desee" NEW_LINE
"configurar y despues" NEW_LINE
"presione el + boton" NEW_LINE
"para seleccionarlo." NEW_LINE
NEW_LINE
"Si tiene los" NEW_LINE
"dispositivos" NEW_LINE
"requeridos" NEW_LINE
"conectados al" NEW_LINE
"controlador puede" NEW_LINE
"aplicar todas las" NEW_LINE
"condiciones de" NEW_LINE
"inicio a un" NEW_LINE
"programa.",

//--------------------
//2.3 Seasonal Adjust Screen
//--------------------
"Pantalla de Ajuste" NEW_LINE
"Estacional" NEW_LINE
NEW_LINE
"Use al Ajuste" NEW_LINE
"estacional para" NEW_LINE
"aumentar o disminuir" NEW_LINE
"la cantidad de agua" NEW_LINE
"suministrada por las" NEW_LINE
"zonas asociadas" NEW_LINE
"con un programa." NEW_LINE
NEW_LINE
"Verifique que el" NEW_LINE
"numero correcto" NEW_LINE
"del programa aparezca" NEW_LINE
"en la esquina superior" NEW_LINE
"izquierda. Presione" NEW_LINE
"PRG para cambiarlo" NEW_LINE
"si es necesario." NEW_LINE
NEW_LINE
"Presione + o -" NEW_LINE
"para cambiar" NEW_LINE
"el porcentage en el" NEW_LINE
"campo de Ajuste" NEW_LINE
"estacional." NEW_LINE
NEW_LINE
"Ingrese 100% al agua" NEW_LINE
"segun lo programado" NEW_LINE
NEW_LINE
"Ingrese 50% para" NEW_LINE
"disminuir el riego" NEW_LINE
"a la mitad." NEW_LINE
NEW_LINE
"Ingrese 200% para" NEW_LINE
"doblar el regado." NEW_LINE
NEW_LINE
"Ingrese 0% para evitar" NEW_LINE
"que el sitema" NEW_LINE
"riegue." NEW_LINE
NEW_LINE
"Cuando configura un" NEW_LINE
"factor del Ajuste" NEW_LINE
"Estacional este" NEW_LINE
"permanece activo hasta" NEW_LINE
"que se cambia el" NEW_LINE
"numero en este" NEW_LINE
"campo a 100 para" NEW_LINE
"indicar que el" NEW_LINE
"sistema debe regar" NEW_LINE
"segun lo programado." NEW_LINE
NEW_LINE
"Si experimenta" NEW_LINE
"un programa de riego" NEW_LINE
"inesperado," NEW_LINE
"consulte este campo" NEW_LINE
"para determinar" NEW_LINE
"si aun tiene un factor" NEW_LINE
"de ajuste estacional" NEW_LINE
"establecido.",

//--------------------
//2.4 Soak Cycles Screen
//--------------------
"Pantalla de Ciclos" NEW_LINE
"de Remojo" NEW_LINE
NEW_LINE
"Los ciclos de" NEW_LINE
"remojo dividen el" NEW_LINE
"tiempo de ejecucion" NEW_LINE
"del programa" NEW_LINE
"en periodos de" NEW_LINE
"regado y remojo" NEW_LINE
"para evitar" NEW_LINE
"escurrimiento" NEW_LINE
"evaporacion." NEW_LINE
NEW_LINE
"Verifique que el" NEW_LINE
"numero correcto" NEW_LINE
"del programa aparezca" NEW_LINE
"en la esquina superior" NEW_LINE
"izquierda. Presione" NEW_LINE
"PRG para cambiarlo" NEW_LINE
"si es necesario." NEW_LINE
NEW_LINE
"Presione + o -" NEW_LINE
"en el campo de los" NEW_LINE
"ciclos de Remojo para" NEW_LINE
"indicar cuantos" NEW_LINE
"tiempos de remojo" NEW_LINE
"ocurriran." NEW_LINE
NEW_LINE
"En el campo de Tiempo" NEW_LINE
"de Remojo cambie el" NEW_LINE
"tiempo para indicar" NEW_LINE
"que tanto tiempo" NEW_LINE
"desea que el agua" NEW_LINE
"remoje." NEW_LINE
NEW_LINE
"Presione OK para" NEW_LINE
"mostrar una marca" NEW_LINE
"en el campo de" NEW_LINE
"remojo inteligente." NEW_LINE
"Este ajuste" NEW_LINE
"permite que el" NEW_LINE
"controlador aplique" NEW_LINE
"ciclos en el orden" NEW_LINE
"optimo.",

//--------------------
//2.5 Water Windows Screen
//--------------------
"Pantalla de" NEW_LINE
"Ventanas de Regado" NEW_LINE
NEW_LINE
"Ventanas de regado " NEW_LINE
"Cuando el regado" NEW_LINE
"sera o no sera" NEW_LINE
"permitido." NEW_LINE
NEW_LINE
"Verifique que el" NEW_LINE
"numero correcto" NEW_LINE
"del programa aparezca" NEW_LINE
"en la esquina superior" NEW_LINE
"izquierda. Presione" NEW_LINE
"PRG para cambiarlo" NEW_LINE
"si es necesario." NEW_LINE
NEW_LINE
"Modo - Escoja" NEW_LINE
"Ajustar Semana" NEW_LINE
"para la misma" NEW_LINE
"configuracion en" NEW_LINE
"cada dia de la" NEW_LINE
"semana o ajuste" NEW_LINE
"cada dia con" NEW_LINE
"diferente" NEW_LINE
"configuracion." NEW_LINE
NEW_LINE
"Presione el " DOWN_ARROW " boton" NEW_LINE
"para moverse a la" NEW_LINE
"cuadricula." NEW_LINE
NEW_LINE
"Presione el " LEFT_ARROW " o " RIGHT_ARROW NEW_LINE
"para moverse a" NEW_LINE
"alguna casilla en" NEW_LINE
"la cuadricula." NEW_LINE
NEW_LINE
"Para activar una" NEW_LINE
"ventana de regado" NEW_LINE
"en una casilla" NEW_LINE
"presione +." NEW_LINE
NEW_LINE
"Para apagar una" NEW_LINE
"ventana de regado" NEW_LINE
"en una casilla" NEW_LINE
"presione -.",

//--------------------
//2.6 Zones at One Time Screen
//--------------------
"Pantalla Zonas a la" NEW_LINE
"Vez" NEW_LINE
NEW_LINE
"Dependiendo de" NEW_LINE
"cantidad de agua" NEW_LINE
"disponible en su" NEW_LINE
"fuente de agua" NEW_LINE
"y los limites" NEW_LINE
"electricos, su sistema" NEW_LINE
"podria ser capaz de" NEW_LINE
"operar mas de" NEW_LINE
"una zona a la vez." NEW_LINE
"Puede configurar" NEW_LINE
"la BaseStation 1000" NEW_LINE
"para ejecutar un" NEW_LINE
"numero especifico" NEW_LINE
"de zonas a la vez" NEW_LINE
"por cada programa." NEW_LINE
NEW_LINE
"Verifique que el" NEW_LINE
"numero correcto" NEW_LINE
"del programa aparezca" NEW_LINE
"en la esquina superior" NEW_LINE
"izquierda. Presione" NEW_LINE
"PRG para cambiarlo" NEW_LINE
"si es necesario." NEW_LINE
NEW_LINE
"Presione + o - para" NEW_LINE
"cambiar el numero" NEW_LINE
"en el campo" NEW_LINE
"concurrente Maximo" NEW_LINE
"para indicar el" NEW_LINE
"maximo numero de" NEW_LINE
"zonas que pueden" NEW_LINE
"ejecutarse al mismo" NEW_LINE
"tiempo en este" NEW_LINE
"programa." NEW_LINE
NEW_LINE
"Presione el " DOWN_ARROW " boton" NEW_LINE
"para moverse al campo" NEW_LINE
"Maximizar Todos los" NEW_LINE
"Programas." NEW_LINE
NEW_LINE
"Presione + o -" NEW_LINE
"para cambiar" NEW_LINE
"el numero in el" NEW_LINE
"campo Maximizar" NEW_LINE
"Todos los Programas" NEW_LINE
"para indicar el numero" NEW_LINE
"maximo de zonas que" NEW_LINE
"pueden ejecutarse al" NEW_LINE
"mismo tiempo cuando" NEW_LINE
"mas de un programa" NEW_LINE
"esta ejecutandose" NEW_LINE
"al mismo tiempo.",

//--------------------
//2.7 Enabled/Disabled Screen
//--------------------
"Pantalla" NEW_LINE
"Habilitado/" NEW_LINE
"Deshabilitado" NEW_LINE
NEW_LINE
"De forma" NEW_LINE
"predeterminada, todos" NEW_LINE
"los programas de la" NEW_LINE
"BaseStation 1000" NEW_LINE
"estan habilitados," NEW_LINE
"lo que significa" NEW_LINE
"que los programas" NEW_LINE
"se ejecutaran cuando" NEW_LINE
"los dispositivos" NEW_LINE
"estan conectados" NEW_LINE
"y configurados. Si" NEW_LINE
"desea que ciertos" NEW_LINE
"programas no se" NEW_LINE
"ejecuten, puede" NEW_LINE
"deshabilitarlos." NEW_LINE
NEW_LINE
"Verifique que el" NEW_LINE
"numero correcto" NEW_LINE
"del programa aparezca" NEW_LINE
"en la esquina superior" NEW_LINE
"izquierda. Presione" NEW_LINE
"PRG para cambiarlo" NEW_LINE
"si es necesario." NEW_LINE
NEW_LINE
"Presione + o -" NEW_LINE
"para mostrar" NEW_LINE
"o remover la marca" NEW_LINE
"en el campo" NEW_LINE
"de habilitado." NEW_LINE
"Cuando la marca de" NEW_LINE
"verificacion se" NEW_LINE
"muestra, el programa" NEW_LINE
"esta activo.",

//--------------------
//2.8 Setup Prg. Pause Screen
//--------------------
"Pantalla de Pausado" NEW_LINE
"de Configuracion de" NEW_LINE
"Programa" NEW_LINE
NEW_LINE
"Puede configurar una" NEW_LINE
"condicion que hara que" NEW_LINE
"un programa en" NEW_LINE
"ejecucion sea" NEW_LINE
"pausado cuando la" NEW_LINE
"condicion ocurra." NEW_LINE
NEW_LINE
"Pausa de Humedecido" NEW_LINE
"Pausa el controlador" NEW_LINE
"basandose en las" NEW_LINE
"lecturas del sensor" NEW_LINE
"de humedad en el" NEW_LINE
"suelo." NEW_LINE
NEW_LINE
"Pausa del Cambio de" NEW_LINE
"Evento - Pausa el" NEW_LINE
"controlador basandose" NEW_LINE
"en el estado de un" NEW_LINE
"cambio de evento." NEW_LINE
NEW_LINE
"Pausa de Temperatura" NEW_LINE
"Pausa el" NEW_LINE
"controlador basandose" NEW_LINE
"en las lecturas del" NEW_LINE
"sensor de la" NEW_LINE
"temperatura del aire." NEW_LINE
NEW_LINE
"Mensaje en Pantalla" NEW_LINE
"Indica al controlador" NEW_LINE
"mostrar un mensaje" NEW_LINE
"siempre que un" NEW_LINE
"programa esta pausado." NEW_LINE
NEW_LINE
"Presione el " DOWN_ARROW " boton" NEW_LINE
"para moverse a la" NEW_LINE
"condicion de" NEW_LINE
"pausado que desee" NEW_LINE
"ajustar, y despues" NEW_LINE
"presione + para" NEW_LINE
"seleccionarlo." NEW_LINE
NEW_LINE
"Si tiene los" NEW_LINE
"dispositivos" NEW_LINE
"requeridos" NEW_LINE
"conectados al" NEW_LINE
"controlador" NEW_LINE
"puede aplicar" NEW_LINE
"todas las condiciones" NEW_LINE
"de pausado a" NEW_LINE
"un programa.",

//--------------------
//2.9 Setup Prg. Stop Screen
//--------------------
"Pantalla de" NEW_LINE
"Configuracion de" NEW_LINE
"Detenido de Programa" NEW_LINE
NEW_LINE
"Puede configurar una" NEW_LINE
"condicion que causara" NEW_LINE
"que un programa en" NEW_LINE
"ejecucion se" NEW_LINE
"detenga cuando esa" NEW_LINE
"condicion ocurra." NEW_LINE
NEW_LINE
"Parar el Humedecido" NEW_LINE
"Detiene el controlador" NEW_LINE
"basado en las lecturas" NEW_LINE
"del sensor de" NEW_LINE
"humedad en el suelo." NEW_LINE
NEW_LINE
"Parar Cambio" NEW_LINE
"Detiene el controlador" NEW_LINE
"basado en el estado" NEW_LINE
"del dispositivo de" NEW_LINE
"cambio." NEW_LINE
NEW_LINE
"Parar Temperatura" NEW_LINE
"Detiene el controlador" NEW_LINE
"basado en las lecturas" NEW_LINE
"del sensor de" NEW_LINE
"temperatura del aire." NEW_LINE
NEW_LINE
"Mensaje de Pantalla" NEW_LINE
"Indica al controlador" NEW_LINE
"mostrar un mensaje" NEW_LINE
"siempre que un" NEW_LINE
"programa se detiene." NEW_LINE
NEW_LINE
"Presione el " DOWN_ARROW " boton" NEW_LINE
"para moverse a la" NEW_LINE
"condicion" NEW_LINE
"de detenido que" NEW_LINE
"desee establecer," NEW_LINE
"y despues presione" NEW_LINE
"+ para seleccionarlo" NEW_LINE
NEW_LINE
"Si tiene los" NEW_LINE
"dispositivos" NEW_LINE
"requeridos" NEW_LINE
"conectados al" NEW_LINE
"controlador" NEW_LINE
"puede aplicar" NEW_LINE
"todas las condiciones" NEW_LINE
"para parar a" NEW_LINE
"un programa.",

//--------------------
//2.10 MV/Pump Screen
//--------------------
"Pantalla de la" NEW_LINE
"Valvula Maestra/Bomba" NEW_LINE
NEW_LINE
"Use esta opcion" NEW_LINE
"para asociar" NEW_LINE
"una valvula maestra" NEW_LINE
"o bomba de inicio" NEW_LINE
"biCoder con un" NEW_LINE
"programa." NEW_LINE
"Antes de realizar" NEW_LINE
"este procedimiento," NEW_LINE
"debe asignar biCoder" NEW_LINE
"como una valvula" NEW_LINE
"maestra o bomba" NEW_LINE
"biCoder." NEW_LINE
NEW_LINE
"Verifique que el" NEW_LINE
"numero correcto" NEW_LINE
"del programa aparezca" NEW_LINE
"en la esquina superior" NEW_LINE
"izquierda. Presione" NEW_LINE
"PRG para cambiarlo" NEW_LINE
"si es necesario." NEW_LINE
NEW_LINE
"Los numeros de serie" NEW_LINE
"de los biCoders que" NEW_LINE
"asigno como valvula" NEW_LINE
"maestra o bomba de" NEW_LINE
"inicio biCoders se" NEW_LINE
"muestran en el lado" NEW_LINE
"izquierdo de la" NEW_LINE
"pantalla." NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para moverse al" NEW_LINE
"numero de serie de" NEW_LINE
"la valvula maestra" NEW_LINE
"o bomba de inicio" NEW_LINE
"biCoder que desee" NEW_LINE
"asociar con este" NEW_LINE
"programa, y despues" NEW_LINE
"presione + o -" NEW_LINE
"para seleccionar" NEW_LINE
"el numero de serie." NEW_LINE
"Una marca de" NEW_LINE
"verificacion" NEW_LINE
"aparece en el lado" NEW_LINE
"derecho de la" NEW_LINE
"pantalla.",

//--------------------
//2.11 Water Source Screen
//--------------------
"Pantalla de la" NEW_LINE
"Fuente de Agua" NEW_LINE
NEW_LINE
"El controlador puede" NEW_LINE
"manipular una" NEW_LINE
"fuente de agua" NEW_LINE
"completa (valvula" NEW_LINE
"maestra, sensor de" NEW_LINE
"flujo y bomba de" NEW_LINE
"inicio) y puede ser" NEW_LINE
"actualizada para" NEW_LINE
"manipular hasta" NEW_LINE
"4 fuentes de agua." NEW_LINE
NEW_LINE
"Si el controlador" NEW_LINE
"ha sido actualizado" NEW_LINE
"para manipular" NEW_LINE
"multiples fuentes" NEW_LINE
"de agua, usted" NEW_LINE
"puede asociar una" NEW_LINE
"fuente de agua" NEW_LINE
"especifica con un" NEW_LINE
"programa especifico" NEW_LINE
"o asociar multiples" NEW_LINE
"fuentes de agua con" NEW_LINE
"un programa." NEW_LINE
NEW_LINE
"Verifique que el" NEW_LINE
"numero correcto" NEW_LINE
"del programa aparezca" NEW_LINE
"en la esquina superior" NEW_LINE
"izquierda. Presione" NEW_LINE
"PRG para cambiarlo" NEW_LINE
"si es necesario." NEW_LINE
NEW_LINE
"Si la fuente de agua" NEW_LINE
"esta actualmente" NEW_LINE
"habilitada, una" NEW_LINE
"marca de" NEW_LINE
"verificacion se" NEW_LINE
"muestra en el campo" NEW_LINE
"de habilitacion." NEW_LINE
"Presione + o - para" NEW_LINE
"remover la marca de" NEW_LINE
"verificacion." NEW_LINE
NEW_LINE
"Si la fuente de agua" NEW_LINE
"esta actualmente" NEW_LINE
"deshabilitada," NEW_LINE
"no habra ninguna" NEW_LINE
"marca de verificacion" NEW_LINE
"en el campo de" NEW_LINE
"habilitacion." NEW_LINE
"presione + o - para" NEW_LINE
"reemplazar la marca" NEW_LINE
"de verificacion.",

//--------------------
//2.2.1 Day & Time Start Screen
//--------------------
"Pantalla de Dia y" NEW_LINE
"Tiempo de Inicio" NEW_LINE
NEW_LINE
"Presione la felcha" NEW_LINE
"hacia abajo para" NEW_LINE
"seleccionar una de" NEW_LINE
"las opciones y" NEW_LINE
"depues presione OK" NEW_LINE
"para ir a la" NEW_LINE
"pantalla de" NEW_LINE
"configuracion.",

//--------------------
//2.2.1.1 Set Start Days Screen
//--------------------
"Pantalla de" NEW_LINE
"Configuracion de" NEW_LINE
"Dias de Inicio" NEW_LINE
NEW_LINE
"Presione la felcha" NEW_LINE
"hacia abajo para" NEW_LINE
"seleccionar una de" NEW_LINE
"las opciones." NEW_LINE
"Presione OK para" NEW_LINE
"seleccionar la opcion" NEW_LINE
"y despues presione " RIGHT_ARROW NEW_LINE
"para ir a la" NEW_LINE
"pantalla de" NEW_LINE
"configuracion.",

//--------------------
//2.2.1.1.1 Set Weekdays Screen
//--------------------
"Pantalla de" NEW_LINE
"Configuracion de" NEW_LINE
"Dias de la Semana" NEW_LINE
NEW_LINE
"Verifique que el" NEW_LINE
"numero correcto" NEW_LINE
"del programa aparezca" NEW_LINE
"en la esquina" NEW_LINE
"superior" NEW_LINE
"izquierda." NEW_LINE
"Presione PRG para" NEW_LINE
"cambiarlo si es" NEW_LINE
"necesario." NEW_LINE
NEW_LINE
"Configure los dias" NEW_LINE
"de la semana" NEW_LINE
"cuando desee que" NEW_LINE
"el programa comienze." NEW_LINE
NEW_LINE
"De forma" NEW_LINE
"predeterminada, las" NEW_LINE
"casillas de los" NEW_LINE
"dias de la semana" NEW_LINE
"estan marcados, lo" NEW_LINE
"cual indica que el" NEW_LINE
"programa iniciara" NEW_LINE
"cada dia." NEW_LINE
NEW_LINE
"Presione el " RIGHT_ARROW " boton" NEW_LINE
"para ir al dia" NEW_LINE
"que desea cambiar," NEW_LINE
"y despues presione" NEW_LINE
"Ok para cambiar" NEW_LINE
"la configuracion.",

//--------------------
//2.2.1.1.2 Interval Screen
//--------------------
"Pantalla de Intervalo" NEW_LINE
NEW_LINE
"Configure un" NEW_LINE
"intervalo de" NEW_LINE
"dia personalizado." NEW_LINE
"Presione + o - para" NEW_LINE
"cambiar el numero en" NEW_LINE
"el campo de" NEW_LINE
"intervalo de dia.",

//--------------------
//2.2.1.1.3 Smart Interval Screen
//--------------------
"Pantalla de" NEW_LINE
"Intervalo Inteligente" NEW_LINE
NEW_LINE
"Configure" NEW_LINE
"los intervalos por" NEW_LINE
"cada medio mes. El" NEW_LINE
"calendario muestra" NEW_LINE
"casillas que" NEW_LINE
"representan" NEW_LINE
"los meses del a�o." NEW_LINE
"Los numeros en" NEW_LINE
"las casillas" NEW_LINE
"representan los" NEW_LINE
"intervalos del dia" NEW_LINE
"en primera y segunda" NEW_LINE
"mitad del mes." NEW_LINE
NEW_LINE
"Presione el " RIGHT_ARROW " boton" NEW_LINE
"para moverse a la" NEW_LINE
"casilla en donde" NEW_LINE
"desee cambiar el" NEW_LINE
"intervalo y presione" NEW_LINE
"+ o - para cambiar" NEW_LINE
"el numero en la" NEW_LINE
"casilla.",

//--------------------
//2.2.1.2 Set Start Times Screen
//--------------------
"Pantalla de" NEW_LINE
"Configuracion de" NEW_LINE
"Tiempos de Inicio" NEW_LINE
NEW_LINE
"Cada programa activo" NEW_LINE
"debe tener almenos" NEW_LINE
"un tiempo de inicio." NEW_LINE
"Programas que no" NEW_LINE
"tienen un tiempo" NEW_LINE
"de inicio no" NEW_LINE
"funcionaran." NEW_LINE
NEW_LINE
"Presione el " DOWN_ARROW " boton" NEW_LINE
"para seleccionar" NEW_LINE
"la casilla del" NEW_LINE
"tiempo de inicio que" NEW_LINE
"desee cambiar." NEW_LINE
NEW_LINE
"Presione el boton" NEW_LINE
"+ o - para cambiar" NEW_LINE
"el numero en la" NEW_LINE
"casilla.",

//--------------------
//2.2.2 Moisture Start Screen
//--------------------
"Pantalla de Inicio" NEW_LINE
"de Humedecido" NEW_LINE
NEW_LINE
"Debe tener uno o" NEW_LINE
"mas sensores de" NEW_LINE
"humedad del suelo" NEW_LINE
"intalados y" NEW_LINE
"asignados." NEW_LINE
NEW_LINE
"Si el campo de" NEW_LINE
"humedad muestra" NEW_LINE
"el numero de serie" NEW_LINE
"del sensor instalado" NEW_LINE
"en el area que" NEW_LINE
"este programa regara," NEW_LINE
"ningun cambio es" NEW_LINE
"requerido." NEW_LINE
NEW_LINE
"Si necesita" NEW_LINE
"asignar un diferente" NEW_LINE
"sensor de humedad" NEW_LINE
"del suelo al programa" NEW_LINE
"presione + o -" NEW_LINE
"para seleccionar" NEW_LINE
"un diferente numero" NEW_LINE
"de serie." NEW_LINE
NEW_LINE
"Consulte el manual" NEW_LINE
"del usuario para" NEW_LINE
"obtener informacion" NEW_LINE
"sobre la" NEW_LINE
"configuracion de" NEW_LINE
"los limites y" NEW_LINE
"realizacion de" NEW_LINE
"calibracion.",

//--------------------
//2.2.3 Switch Start Screen
//--------------------
"Pantalla Inicio" NEW_LINE
"del Interruptor de" NEW_LINE
"Evento" NEW_LINE
NEW_LINE
"Debe de tener uno" NEW_LINE
"o mas" NEW_LINE
"dipspositivos del" NEW_LINE
"interruptor de evento" NEW_LINE
"intalados y" NEW_LINE
"asiganados" NEW_LINE
NEW_LINE
"Si el campo del" NEW_LINE
"interruptor de" NEW_LINE
"evento muesra" NEW_LINE
"el numero de serie" NEW_LINE
"del Dispositivo de" NEW_LINE
"cambio instaldo en" NEW_LINE
"el area que este" NEW_LINE
"programa regara." NEW_LINE
"Ningun cambio es" NEW_LINE
"requerido." NEW_LINE
NEW_LINE
"Si necesita" NEW_LINE
"asignar un" NEW_LINE
"diferente" NEW_LINE
"dispositivo de" NEW_LINE
"cambio al programa," NEW_LINE
"presione + o -" NEW_LINE
"para seleccionar" NEW_LINE
"un diferente" NEW_LINE
"numero de serie." NEW_LINE
NEW_LINE
"Consulte el Manual" NEW_LINE
"del usuario para" NEW_LINE
"obtener" NEW_LINE
"informacion sobre" NEW_LINE
"la configuracion" NEW_LINE
"de activacion.",

//--------------------
//2.2.4 Temp Start Screen
//--------------------
"Pantalla de" NEW_LINE
"Temperatura de Inicio" NEW_LINE
NEW_LINE
"Debe de tener uno o" NEW_LINE
"mas sensores de" NEW_LINE
"temperatura de aire" NEW_LINE
"instalados y" NEW_LINE
"signados.",

//--------------------
//2.8.1 Moisture Pause Screen
//--------------------
"Pantalla de" NEW_LINE
"Pausado de Humedad" NEW_LINE
NEW_LINE
"Si tiene un" NEW_LINE
"sensor de humedad" NEW_LINE
"instalado, puede" NEW_LINE
"usar esta" NEW_LINE
"condicion de" NEW_LINE
"pausado para pausar" NEW_LINE
"un programa cuando" NEW_LINE
"un el limite de un" NEW_LINE
"sensor de humedad" NEW_LINE
"especificado ha" NEW_LINE
"llegado a su limite." NEW_LINE
"Cuando el sensor" NEW_LINE
"detecta que la" NEW_LINE
"humedad no cumple" NEW_LINE
"con las condiciones" NEW_LINE
"que ha establecido," NEW_LINE
"el regado" NEW_LINE
"comenzara nuevamente." NEW_LINE
NEW_LINE
"Si el campo de" NEW_LINE
"humedad" NEW_LINE
"muestra el numero" NEW_LINE
"de serie del" NEW_LINE
"sensor instalado" NEW_LINE
"en el area que este" NEW_LINE
"programa regara," NEW_LINE
"ningun cambio es" NEW_LINE
"requerido." NEW_LINE
NEW_LINE
"Si necesita asignar" NEW_LINE
"un diferente" NEW_LINE
"sensor de humedad" NEW_LINE
"al programa" NEW_LINE
"presione + o -" NEW_LINE
"para seleccionar un" NEW_LINE
"diferente numero" NEW_LINE
"de serie." NEW_LINE
NEW_LINE
"Consulte el Manual" NEW_LINE
"de Usuario para" NEW_LINE
"obtener" NEW_LINE
"informacion sobre" NEW_LINE
"la configuracion" NEW_LINE
"de los limites.",

//--------------------
//2.8.2 Switch Pause Screen
//--------------------
"Pantalla de" NEW_LINE
"Pausado Cambio" NEW_LINE
NEW_LINE
"Si tiene un" NEW_LINE
"disposiivo de" NEW_LINE
"cambio instalado," NEW_LINE
"puede usar esta" NEW_LINE
"condicion para" NEW_LINE
"pausar un programa" NEW_LINE
"cuando una condicion" NEW_LINE
"de cambio" NEW_LINE
"especificada ocurra." NEW_LINE
NEW_LINE
"Si el campo de" NEW_LINE
"Cambio de Evento" NEW_LINE
"muestra que el numero" NEW_LINE
"de serie del" NEW_LINE
"dispositivo de cambio" NEW_LINE
"instalado en el" NEW_LINE
"area que este" NEW_LINE
"programa regara," NEW_LINE
"ningun cambio es" NEW_LINE
"requerido." NEW_LINE
NEW_LINE
"Si necesita" NEW_LINE
"asignar un" NEW_LINE
"dispositivo de" NEW_LINE
"cambio diferente" NEW_LINE
"al programa" NEW_LINE
"presione + o -" NEW_LINE
"para seleccionar" NEW_LINE
"un diferente" NEW_LINE
"numero de serie." NEW_LINE
NEW_LINE
"En el campo del" NEW_LINE
"Activador," NEW_LINE
"presione +" NEW_LINE
"para cambiar" NEW_LINE
"entre Abierto y" NEW_LINE
"Cerrado" NEW_LINE
NEW_LINE
"En el campo de Horas" NEW_LINE
"de Pausado" NEW_LINE
"presione + o - para" NEW_LINE
"indicar cuantas" NEW_LINE
"horas el programa" NEW_LINE
"se pausara cuando" NEW_LINE
"la condicion de" NEW_LINE
"cambio ocurra.",

//--------------------
//2.8.3 Temperature Pause Screen
//--------------------
"Pantalla de" NEW_LINE
"Pausado de" NEW_LINE
"Temperatura" NEW_LINE
NEW_LINE
"Si tiene un sensor" NEW_LINE
"de temperatura de" NEW_LINE
"aire instalado," NEW_LINE
"puede usar esta" NEW_LINE
"condicion de pausado" NEW_LINE
"para pausar un" NEW_LINE
"programa cuando" NEW_LINE
"una temperatura" NEW_LINE
"especificada es" NEW_LINE
"alcanzada." NEW_LINE
NEW_LINE
"Si el campo del" NEW_LINE
"Sensor de Temperatura" NEW_LINE
"muestra el numero" NEW_LINE
"de serie de de un" NEW_LINE
"sensor de" NEW_LINE
"temperaura instalado" NEW_LINE
"en el area que este" NEW_LINE
"programa regara," NEW_LINE
"ningun cambio es" NEW_LINE
"requerido." NEW_LINE
NEW_LINE
"Si necesita" NEW_LINE
"asignar un diferente" NEW_LINE
"sensor de" NEW_LINE
"temperatura al" NEW_LINE
"programa" NEW_LINE
"presione + o -" NEW_LINE
"para seleccionar" NEW_LINE
"un diferente" NEW_LINE
"numero de serie" NEW_LINE
NEW_LINE
"En el campo de" NEW_LINE
"Activacion," NEW_LINE
"presione + o -" NEW_LINE
"para seleccionar" NEW_LINE
"una de las opciones" NEW_LINE
"de pausado.",

//--------------------
//2.9.1 Moisture Stop Screen
//--------------------
"Pantalla de" NEW_LINE
"Detenido de Humedad" NEW_LINE
NEW_LINE
"Si tiene un sensor" NEW_LINE
"de humedad instalado," NEW_LINE
"puede usar esta" NEW_LINE
"condicion de parado" NEW_LINE
"para que un" NEW_LINE
"programa se detenga" NEW_LINE
"cuando el limite" NEW_LINE
"especificado de un" NEW_LINE
"sensor de humedad" NEW_LINE
"ha sido alcanzado" NEW_LINE
"o cuando el nivel" NEW_LINE
"del agua en un" NEW_LINE
"estanque/cisterna" NEW_LINE
"baja mas de un" NEW_LINE
"nivel especifico." NEW_LINE
NEW_LINE
"Si el campo de" NEW_LINE
"Humedad muestra el" NEW_LINE
"numero de serie" NEW_LINE
"de un sensor" NEW_LINE
"instalado" NEW_LINE
"en el area que" NEW_LINE
"este programa" NEW_LINE
"regara, ningun cambio" NEW_LINE
"es requerido." NEW_LINE
NEW_LINE
"Si necesita" NEW_LINE
"asignar un diferente" NEW_LINE
"sensor de humedad" NEW_LINE
"del suelo al programa" NEW_LINE
"presione + o -" NEW_LINE
"para seleccionar" NEW_LINE
"un diferente" NEW_LINE
"numero de serie" NEW_LINE
NEW_LINE
"Consulte el manual" NEW_LINE
"del usuario para" NEW_LINE
"obtener informacion" NEW_LINE
"sobre la" NEW_LINE
"configuracion de" NEW_LINE
"los limites" NEW_LINE
"y realizacion de" NEW_LINE
"calibracion.",

//--------------------
//2.9.2 Switch Stop Screen
//--------------------
"Pantalla de" NEW_LINE
"Detenido de" NEW_LINE
"Interruptor" NEW_LINE
NEW_LINE
"Si tiene un" NEW_LINE
"dispositivo" NEW_LINE
"de cambio" NEW_LINE
"instalado, puede" NEW_LINE
"usar esta" NEW_LINE
"condicion de parado" NEW_LINE
"para que un" NEW_LINE
"programa se detenga" NEW_LINE
"cuando una" NEW_LINE
"condicion de cambio" NEW_LINE
"especificada ocurra" NEW_LINE
NEW_LINE
"Si el campo del" NEW_LINE
"evento de cambio" NEW_LINE
"muesra en numero" NEW_LINE
"de serie del" NEW_LINE
"dispositivo de" NEW_LINE
"cambio instalado" NEW_LINE
"en el area en que" NEW_LINE
"este programa" NEW_LINE
"regara, ningun" NEW_LINE
"cambio es requerido." NEW_LINE
NEW_LINE
"Si necesita" NEW_LINE
"asignar un diferente" NEW_LINE
"dispositivo de" NEW_LINE
"cambio al programa," NEW_LINE
"presione + o -" NEW_LINE
"para seleccionar" NEW_LINE
"un diferente" NEW_LINE
"numero de serie." NEW_LINE
NEW_LINE
"En el campo de" NEW_LINE
"Activado presione" NEW_LINE
"+ para cambiar" NEW_LINE
"entre  Al Abrir y " NEW_LINE
"Al Cerrar" NEW_LINE
NEW_LINE
"En el campo de" NEW_LINE
"Parado presione + o -" NEW_LINE
"para cambiar entre" NEW_LINE
"Al final del Ciclo" NEW_LINE
"e inmediatamente.",

//--------------------
//2.9.3 Temperature Stop Screen
//--------------------
"Pantalla de" NEW_LINE
"Detenido de" NEW_LINE
"Temperatura" NEW_LINE
NEW_LINE
"Si tiene un sensor" NEW_LINE
"de temperatura de" NEW_LINE
"aire instalado," NEW_LINE
"puede usar esta" NEW_LINE
"condicion de parado" NEW_LINE
"para detener un" NEW_LINE
"programa cuando" NEW_LINE
"una temperatura" NEW_LINE
"especificada es" NEW_LINE
"alcanzada." NEW_LINE
NEW_LINE
"Si el campo del" NEW_LINE
"Sensor de Temperatura" NEW_LINE
"muestra que el numero" NEW_LINE
"de serie del" NEW_LINE
"sensor de" NEW_LINE
"temperatura del aire" NEW_LINE
"instalado en el" NEW_LINE
"area que este" NEW_LINE
"programa regara," NEW_LINE
"ningun cambio es" NEW_LINE
"requerido." NEW_LINE
NEW_LINE
"Si necesita" NEW_LINE
"asignar un diferente" NEW_LINE
"sensor de" NEW_LINE
"temperatura  al" NEW_LINE
"programa" NEW_LINE
"presione + o -" NEW_LINE
"para seleccionar" NEW_LINE
"un diferente" NEW_LINE
"numero de serie" NEW_LINE
NEW_LINE
"En el campo de" NEW_LINE
"activado presione" NEW_LINE
"+ o - para" NEW_LINE
"seleccionar una" NEW_LINE
"de las opciones" NEW_LINE
"de pausado.",

//####################
//3.0 Flow Setup Menu
//####################
"Menu de" NEW_LINE
"Configuracion de Flujo" NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para moverse a la" NEW_LINE
"opcion que desee" NEW_LINE
"usar y despues" NEW_LINE
"presione OK.",

//--------------------
//3.1 Water Source Selection Menu
//--------------------
"Menu de Seleccion" NEW_LINE
"de Fuente de Agua" NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para moverse a la" NEW_LINE
"fuente de agua" NEW_LINE
"que desee ajustar" NEW_LINE
"y despues precione" NEW_LINE
"OK.",

//--------------------
//3.2 Learn Flow Menu
//--------------------
"Menu de" NEW_LINE
"Aprendizaje de Flujo" NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para moverse a la" NEW_LINE
"opcion que desea" NEW_LINE
"usar y despues" NEW_LINE
"presione OK.",

//--------------------
//3.1.1	Water Source # Setup Menu
//--------------------
"Menu de" NEW_LINE
"Configuracion de" NEW_LINE
"Fuente de Agua" NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para moverse a la" NEW_LINE
"opcion que desee" NEW_LINE
"usar y despues" NEW_LINE
"presione OK.",

//--------------------
//3.1.1.1 Flow Sensors Screen
//--------------------
"Pantalla de" NEW_LINE
"Sensores de Flujo" NEW_LINE
NEW_LINE
"Antes de poder" NEW_LINE
"asignar un sensor" NEW_LINE
"de flujo a una" NEW_LINE
"fuente de agua," NEW_LINE
"debe buscar y" NEW_LINE
"asignar el biCoder" NEW_LINE
"de flujo." NEW_LINE
NEW_LINE
"Verifique que la" NEW_LINE
"fuente de agua que" NEW_LINE
"aparece en la" NEW_LINE
"esquina superior" NEW_LINE
"izquierda de la" NEW_LINE
"pantalla es la que" NEW_LINE
"desea asignar al" NEW_LINE
"sensor de flujo." NEW_LINE
"Si no es asi," NEW_LINE
"presione el boton" NEW_LINE
"BACK hasta que" NEW_LINE
"pueda seleccionar" NEW_LINE
"otra fuente de" NEW_LINE
"agua, y despues" NEW_LINE
"presione OK para" NEW_LINE
"regresar a esta" NEW_LINE
"pantalla." NEW_LINE
NEW_LINE
"Presione el " DOWN_ARROW " boton" NEW_LINE
"para seleccionar" NEW_LINE
"el dispositivo de" NEW_LINE
"flujo que desea" NEW_LINE
"asignar a esta" NEW_LINE
"fuente de agua," NEW_LINE
"y despues presione" NEW_LINE
"OK.",

//--------------------
//3.1.1.2 MV/Pumps Screen
//--------------------
"Pantalla de la" NEW_LINE
"Valvula Maestra/Bomba" NEW_LINE
NEW_LINE
"Antes de poder" NEW_LINE
"asignar una" NEW_LINE
"Valvula" NEW_LINE
"Maestra/Bomba de" NEW_LINE
"Inicio biCoder" NEW_LINE
"a una fuente de" NEW_LINE
"agua, debe" NEW_LINE
"buscar y asignar" NEW_LINE
"el biCoder." NEW_LINE
NEW_LINE
"Verifique que la" NEW_LINE
"fuente de agua que" NEW_LINE
"aparece en la" NEW_LINE
"esquina superior" NEW_LINE
"izquierda de la" NEW_LINE
"pantalla es la que" NEW_LINE
"desea asignar" NEW_LINE
"al la valvula" NEW_LINE
"maestra/bomba" NEW_LINE
"biCoder." NEW_LINE
"Si no es asi," NEW_LINE
"presione el boton" NEW_LINE
"BACK hasta que" NEW_LINE
"pueda seleccionar" NEW_LINE
"otra fuente de" NEW_LINE
"agua, y despues" NEW_LINE
"presione OK para" NEW_LINE
"regresar a esta" NEW_LINE
"pantalla." NEW_LINE
NEW_LINE
"Presione el " DOWN_ARROW " boton" NEW_LINE
"para seleccionar" NEW_LINE
"la Valvula" NEW_LINE
"Maestra/bomba biCoder" NEW_LINE
"que desea asignar" NEW_LINE
"a esta fuente de" NEW_LINE
"agua, y despues" NEW_LINE
"presione OK.",

//--------------------
//3.1.1.3 Flow Management Menu
//--------------------
"Menu de Control de" NEW_LINE
"Flujo" NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para moverse a la" NEW_LINE
"opcion que desee" NEW_LINE
"usar y despues" NEW_LINE
"presione OK.",

//--------------------
//3.1.1.4 Enable/Disable Screen
//--------------------
"Pantalla de" NEW_LINE
"Habilitacion/" NEW_LINE
"Deshabilitacion" NEW_LINE
NEW_LINE
"De forma" NEW_LINE
"predeterminada," NEW_LINE
"el controlador" NEW_LINE
"habilita todas" NEW_LINE
"las fuentes de" NEW_LINE
"agua conectadas." NEW_LINE
"Puede deshabilitar" NEW_LINE
"una fuente de agua" NEW_LINE
"segun sea" NEW_LINE
"necesario y" NEW_LINE
"re-habilitarla en" NEW_LINE
"otra ocacion." NEW_LINE
NEW_LINE
"Si le fuente de" NEW_LINE
"agua esta" NEW_LINE
"actualmente" NEW_LINE
"habilitada" NEW_LINE
"una marca de" NEW_LINE
"verificacion se" NEW_LINE
"mostrara en el" NEW_LINE
"campo de habilitado." NEW_LINE
"Presione + o -" NEW_LINE
"para remover la" NEW_LINE
"marca de" NEW_LINE
"verificacion." NEW_LINE
NEW_LINE
"Si la fuente de" NEW_LINE
"agua esta" NEW_LINE
"actualmente" NEW_LINE
"deshabilitada," NEW_LINE
"no hay una marca" NEW_LINE
"de verificacion en el" NEW_LINE
"campo e habilitado." NEW_LINE
"Presione + o -" NEW_LINE
"para reemplazar la" NEW_LINE
"marca.",

//--------------------
//3.1.1.3.1 Limit Zones by Flow Screen
//--------------------
"Pantalla de Limite" NEW_LINE
"de Zonas por Flujo" NEW_LINE
NEW_LINE
"introduzca un" NEW_LINE
"objetivo de flujo" NEW_LINE
"en galones por minuto" NEW_LINE
"(GPM) para la" NEW_LINE
"fuente de agua que" NEW_LINE
"abastece agua" NEW_LINE
"atravez del" NEW_LINE
"dispositivo de" NEW_LINE
"flujo al resto del" NEW_LINE
"sistema de" NEW_LINE
"irrigacion." NEW_LINE
"El controlador usa" NEW_LINE
"este valor para" NEW_LINE
"controlar el numero" NEW_LINE
"de zonas que" NEW_LINE
"puede ejecutar al" NEW_LINE
"mismo tiempo." NEW_LINE
"Si establece este" NEW_LINE
"valor a cero," NEW_LINE
"no puede utilizar" NEW_LINE
"esta cantidad de GPM" NEW_LINE
"para controlar" NEW_LINE
"zonas concurrentes." NEW_LINE
NEW_LINE
"En el campo de" NEW_LINE
"Objetivo de Flujo," NEW_LINE
"presione + o -" NEW_LINE
"para cambiar" NEW_LINE
"el numero en el campo" NEW_LINE
NEW_LINE
"Presione el " DOWN_ARROW " boton" NEW_LINE
"para seleccionar" NEW_LINE
"el campo del" NEW_LINE
"Limite Concurrente," NEW_LINE
"despues presione +" NEW_LINE
"o - para" NEW_LINE
"mostrar la marca" NEW_LINE
"de verificacion en" NEW_LINE
"el campo." NEW_LINE
"La marca indica" NEW_LINE
"que el sistema" NEW_LINE
"limitara el numero" NEW_LINE
"de zonas que" NEW_LINE
"pueden ejecutarse" NEW_LINE
"al mismo tiempo" NEW_LINE
"por los galones" NEW_LINE
"por minuto en el" NEW_LINE
"campo de objetivo" NEW_LINE
"de flujo.",

//--------------------
//3.1.1.3.2 High Flow Limit Screen
//--------------------
"Limitar al Ejecutar" NEW_LINE
NEW_LINE
"Introduzca un" NEW_LINE
"limite de flujo para" NEW_LINE
"una fuente de agua" NEW_LINE
"en ejecucion" NEW_LINE
"y configure el" NEW_LINE
"sistema para" NEW_LINE
"apagar la fuente" NEW_LINE
"de agua si ese" NEW_LINE
"limite es excedido." NEW_LINE
"Si habilita el" NEW_LINE
"campo de apagado," NEW_LINE
"entonces los" NEW_LINE
"programas que" NEW_LINE
"estan usando" NEW_LINE
"esta fuente de" NEW_LINE
"agua seran detenidos" NEW_LINE
"y las valvulas" NEW_LINE
"maestras se" NEW_LINE
"apagaran cuando" NEW_LINE
"el ritmo de flujo" NEW_LINE
"excede el limite." NEW_LINE
NEW_LINE
"En el campo de" NEW_LINE
"Limite de Flujo," NEW_LINE
"presione + o - para" NEW_LINE
"cambiar el numero" NEW_LINE
"en el campo." NEW_LINE
NEW_LINE
"Presione el " DOWN_ARROW " boton" NEW_LINE
"para seleccionar" NEW_LINE
"el campo de" NEW_LINE
"Apagado, despues" NEW_LINE
"presione + o -" NEW_LINE
"para mostrar la" NEW_LINE
"marca de verificacion" NEW_LINE
"en el campo." NEW_LINE
"La marca indica que" NEW_LINE
"el sietema apagara" NEW_LINE
"la fuente de agua" NEW_LINE
"si los galones por" NEW_LINE
"minuto en el campo de" NEW_LINE
"Limite de Flujo es" NEW_LINE
"excedido.",

//--------------------
//3.1.1.3.3 Unexpected Flow Limit Screen
//--------------------
"Limitar cuando esta" NEW_LINE
"Apagado" NEW_LINE
NEW_LINE
"Puede introducir" NEW_LINE
"un limite de flujo" NEW_LINE
"para una fuente de" NEW_LINE
"agua que esta apagada" NEW_LINE
"y despues" NEW_LINE
"comfigurar el" NEW_LINE
"sistema para" NEW_LINE
"cerrar la fuente" NEW_LINE
"de agua si ese limite" NEW_LINE
"es excedido." NEW_LINE
"Cuando habilita" NEW_LINE
"esta caracteristica" NEW_LINE
"el sistema" NEW_LINE
"monitoreara el flujo" NEW_LINE
"aun cuando la" NEW_LINE
"fuente de agua" NEW_LINE
"esta apagada" NEW_LINE
"con el fin de" NEW_LINE
"evitar perdida de" NEW_LINE
"agua debido a una" NEW_LINE
"tuberia rota." NEW_LINE
NEW_LINE
"En el campo de" NEW_LINE
"Limite de Flujo," NEW_LINE
"presione + o -" NEW_LINE
"para cambiar el" NEW_LINE
"numero en el campo." NEW_LINE
NEW_LINE
"Presione el " DOWN_ARROW " boton" NEW_LINE
"para resaltar el" NEW_LINE
"campo de Apagado," NEW_LINE
"y despues presione" NEW_LINE
"+ o - para" NEW_LINE
"mostrar la marca" NEW_LINE
"de verificacion en" NEW_LINE
"el campo. La marca" NEW_LINE
"indica que el sistema apagara" NEW_LINE
"la fuente de agua" NEW_LINE
"si los galones por" NEW_LINE
"minuto en el campo" NEW_LINE
"de Limite de Flujo" NEW_LINE
"es Excedido.",

//--------------------
//3.1.1.3.4 Pipe Fill Time
//--------------------
"Tiempo de LLenado" NEW_LINE
"de Tubo" NEW_LINE
NEW_LINE
"Para calibrar el" NEW_LINE
"proceso de Flujo" NEW_LINE
"en el controlador," NEW_LINE
"debe introducir" NEW_LINE
"el tiempo de" NEW_LINE
"llenado del tubo" NEW_LINE
"antes de iniciar" NEW_LINE
"el proceso de flujo" NEW_LINE
NEW_LINE
"El tiempo de" NEW_LINE
"llenado del tubo" NEW_LINE
"es el numero de" NEW_LINE
"minutos que tarda" NEW_LINE
"una linea bacia" NEW_LINE
"en llenarse," NEW_LINE
"active la valvula" NEW_LINE
"y alcanse un" NEW_LINE
"estado de presion" NEW_LINE
"de operacion estable." NEW_LINE
NEW_LINE
"En el campo de" NEW_LINE
"Tiempo de Llenado," NEW_LINE
"presione + o - para" NEW_LINE
"cambiar en numero" NEW_LINE
"en el campo.",

//--------------------
//3.2.1	Learn One Zone Screen
//--------------------
"Pantalla de" NEW_LINE
"Aprendizaje de Una" NEW_LINE
"Zona" NEW_LINE
NEW_LINE
"Para cada zona," NEW_LINE
"puede correr un ciclo" NEW_LINE
"de aprendizaje de" NEW_LINE
"flujo para tener" NEW_LINE
"su dise�o de flujo" NEW_LINE
"establecido" NEW_LINE
"automaticamente." NEW_LINE
NEW_LINE
"Debe reconfigurar" NEW_LINE
"el dise�o de flujo" NEW_LINE
"para cada zona" NEW_LINE
"cada vez que" NEW_LINE
"cambia un aspersor o" NEW_LINE
"un emisor en una" NEW_LINE
"zona o cuando" NEW_LINE
"el ciclo de" NEW_LINE
"aprendizaje falla" NEW_LINE
"en una zona." NEW_LINE
NEW_LINE
"El ciclo de" NEW_LINE
"aprendizaje de flujo" NEW_LINE
"utiliza el tiempo" NEW_LINE
"de llenado de valvula" NEW_LINE
"configurado en la" NEW_LINE
"pantalla de" NEW_LINE
"llenado del tubo." NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para resaltar la" NEW_LINE
"zona de la cual" NEW_LINE
"desea aprender el" NEW_LINE
"flujo, y despues" NEW_LINE
"presione OK para" NEW_LINE
"comenzar el nuevo" NEW_LINE
"ciclo de aprendizaje." NEW_LINE
NEW_LINE
"Para cancelar el" NEW_LINE
"ciclo de aprendizaje" NEW_LINE
"cuando esta en" NEW_LINE
"progreso," NEW_LINE
"presione el boton" NEW_LINE
"BACK." NEW_LINE
NEW_LINE
"Cuando el ciclo de" NEW_LINE
"aprendizaje de flujo" NEW_LINE
"es finalizado," NEW_LINE
"los resultados se" NEW_LINE
"mostraran en la" NEW_LINE
"pantalla" NEW_LINE
"de Finalizado de" NEW_LINE
"Aprendizado de" NEW_LINE
"Flujo.",

//--------------------
//3.2.2	Learn All Zones Screen
//--------------------
"Pantalla de" NEW_LINE
"Aprendizaje de" NEW_LINE
"Todas las Zonas" NEW_LINE
NEW_LINE
"El ciclo de" NEW_LINE
"aprendisaje de flujo" NEW_LINE
"empieza tan pronto" NEW_LINE
"como sleccione" NEW_LINE
"la opcion de" NEW_LINE
"Aprendizaje de" NEW_LINE
"todas las Zonas" NEW_LINE
"Dependiendo" NEW_LINE
"cuantas zonas" NEW_LINE
"estan habilitadas" NEW_LINE
"en su sistema, el" NEW_LINE
"ciclo de" NEW_LINE
"aprendizaje de flujo" NEW_LINE
"puede tardar mas" NEW_LINE
"de una hora en" NEW_LINE
"completarse." NEW_LINE
NEW_LINE
"Si necesita" NEW_LINE
"cancelar el ciclo" NEW_LINE
"de aprendizaje de" NEW_LINE
"flujo mientras" NEW_LINE
"esta en progreso" NEW_LINE
"pesione el boton" NEW_LINE
"BACK." NEW_LINE
NEW_LINE
"El ciclo de" NEW_LINE
"aprendizaje de flujo" NEW_LINE
"utiliza el tiempo" NEW_LINE
"de llenado de valvula" NEW_LINE
"configurado en la" NEW_LINE
"pantalla de" NEW_LINE
"llenado del tubo." NEW_LINE
NEW_LINE
"Cuando el ciclo de" NEW_LINE
"aprendizaje de flujo" NEW_LINE
"es finalizado, los" NEW_LINE
"resultados se" NEW_LINE
"muestran en la" NEW_LINE
"pantalla de" NEW_LINE
"finalizado del" NEW_LINE
"Aprendizaje" NEW_LINE
"de Flujo.",

//####################
//4.0 Search & Assign Menu
//####################
"Menu de Busqueda y" NEW_LINE
"Asignado" NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para moverse a la" NEW_LINE
"opcion que desee" NEW_LINE
"utilizar y despues" NEW_LINE
"presione OK.",

//--------------------
//4.1 Zone Menu
//--------------------
"Menu de Zona" NEW_LINE
NEW_LINE
"Despues de haber" NEW_LINE
"terminado de" NEW_LINE
"conectar todos sus" NEW_LINE
"dispositivos," NEW_LINE
"necesita hacer que" NEW_LINE
"el controlador" NEW_LINE
"encuentre esos" NEW_LINE
"dispositivos" NEW_LINE
"para que pueda" NEW_LINE
"asignarlos." NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para moverse a la" NEW_LINE
"opcion que desee" NEW_LINE
"utilizar y despues" NEW_LINE
"presione OK.",

//--------------------
//4.2 MV/Pump Menu
//--------------------
"Menu de Valvula" NEW_LINE
"Maestra/ Bomba" NEW_LINE
NEW_LINE
"Despues de haber" NEW_LINE
"terminado de" NEW_LINE
"conectar todos sus" NEW_LINE
"dispositivos," NEW_LINE
"necesita hacer que" NEW_LINE
"el controlador" NEW_LINE
"encuentre esos" NEW_LINE
"dispositivos" NEW_LINE
"para que pueda" NEW_LINE
"asignarlos." NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para moverse a la" NEW_LINE
"opcion que desee" NEW_LINE
"utilizar y despues" NEW_LINE
"presione OK.",

//--------------------
//4.3 Moisture Sensor Menu
//--------------------
"Menu de Sensor de" NEW_LINE
"Humedad" NEW_LINE
NEW_LINE
"Despues de haber" NEW_LINE
"terminado de" NEW_LINE
"conectar todos sus" NEW_LINE
"dispositivos," NEW_LINE
"necesita hacer que" NEW_LINE
"el controlador" NEW_LINE
"encuentre esos" NEW_LINE
"dispositivos" NEW_LINE
"para que pueda" NEW_LINE
"asignarlos." NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para moverse a la" NEW_LINE
"opcion que desee" NEW_LINE
"utilizar y despues" NEW_LINE
"presione OK.",

//--------------------
//4.4 Flow Sensor Menu
//--------------------
"Menu de Sensor de Flujo" NEW_LINE
NEW_LINE
"Despues de haber" NEW_LINE
"terminado de" NEW_LINE
"conectar todos sus" NEW_LINE
"dispositivos," NEW_LINE
"necesita hacer que" NEW_LINE
"el controlador" NEW_LINE
"encuentre esos" NEW_LINE
"dispositivos" NEW_LINE
"para que pueda" NEW_LINE
"asignarlos." NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para moverse a la" NEW_LINE
"opcion que desee" NEW_LINE
"utilizar y despues" NEW_LINE
"presione OK.",

//--------------------
//4.5 Event Switch Menu
//--------------------
"Menu de" NEW_LINE
"Interruptor de Evento" NEW_LINE
NEW_LINE
"Despues de haber" NEW_LINE
"terminado de" NEW_LINE
"conectar todos sus" NEW_LINE
"dispositivos," NEW_LINE
"necesita hacer que" NEW_LINE
"el controlador" NEW_LINE
"encuentre esos" NEW_LINE
"dispositivos" NEW_LINE
"para que pueda" NEW_LINE
"asignarlos." NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para moverse a la" NEW_LINE
"opcion que desee" NEW_LINE
"utilizar y despues" NEW_LINE
"presione OK.",

//--------------------
//4.6 Temperature Sensor Menu
//--------------------
"Menu de Sensor de" NEW_LINE
"Temperatura" NEW_LINE
NEW_LINE
"Despues de haber" NEW_LINE
"terminado de" NEW_LINE
"conectar todos sus" NEW_LINE
"dispositivos," NEW_LINE
"necesita hacer que" NEW_LINE
"el controlador" NEW_LINE
"encuentre esos" NEW_LINE
"dispositivos" NEW_LINE
"para que pueda" NEW_LINE
"asignarlos." NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para moverse a la" NEW_LINE
"opcion que desee" NEW_LINE
"utilizar y despues" NEW_LINE
"presione OK.",

//--------------------
//4.1.1 Device Assignment (Zones)
//--------------------
"Asignacion de" NEW_LINE
"Dispositivos (Zonas)" NEW_LINE
NEW_LINE
"Cuando Buscar esta" NEW_LINE
"resaltado," NEW_LINE
"presione OK para" NEW_LINE
"buscar por biCoders." NEW_LINE
NEW_LINE
"El controlador" NEW_LINE
"encuantra" NEW_LINE
"dispositivos y" NEW_LINE
"muestra sus" NEW_LINE
"numeros de serie" NEW_LINE
"en la Columna de" NEW_LINE
"Accion." NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para resaltar el" NEW_LINE
"numero de serie" NEW_LINE
"del dispositivo" NEW_LINE
"que desea asignar" NEW_LINE
"a la zona." NEW_LINE
NEW_LINE
"Presione el " RIGHT_ARROW " boton" NEW_LINE
"para moverse a la" NEW_LINE
"columna de Zona." NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para resaltar la zona" NEW_LINE
"a la cual desea" NEW_LINE
"asignar el" NEW_LINE
"dispositivo." NEW_LINE
NEW_LINE
"Presione OK para" NEW_LINE
"seleccionar la zona" NEW_LINE
"resaltada. El" NEW_LINE
"numero de serie" NEW_LINE
"del dispositivo" NEW_LINE
"que selecciono en" NEW_LINE
"la columna de Accion" NEW_LINE
"ahora se muestra" NEW_LINE
"al lado del numero" NEW_LINE
"de zona en la" NEW_LINE
"columna de zona." NEW_LINE
NEW_LINE
"Continue hasta que" NEW_LINE
"haya asignado" NEW_LINE
"todos los biCoders" NEW_LINE
"a sus zonas.",

//--------------------
//4.1.2 Device Setup (Zones)
//--------------------
"Configuracion de" NEW_LINE
"Dispositivoz (Zonas)" NEW_LINE
NEW_LINE
"Despues de asignar" NEW_LINE
"un dispositivo" NEW_LINE
"para un uso" NEW_LINE
"especifico en el" NEW_LINE
"controlador, el" NEW_LINE
"dispositivo esta" NEW_LINE
"habilitado" NEW_LINE
"automaticamente." NEW_LINE
"Puede deshabilitar" NEW_LINE
"ese dispositivo si" NEW_LINE
"desea ponerlo" NEW_LINE
"fuera de servicio" NEW_LINE
"temporalmente y" NEW_LINE
"puede habilitarlo" NEW_LINE
"despues si es" NEW_LINE
"necesario." NEW_LINE
NEW_LINE
"Tambien puede" NEW_LINE
"establecer" NEW_LINE
"manualmente la" NEW_LINE
"velocidad de flujo" NEW_LINE
"para cada zona." NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para resaltar el" NEW_LINE
"numero de zona en" NEW_LINE
"la cual desea" NEW_LINE
"cambiar la" NEW_LINE
"configuracion y" NEW_LINE
"despues presione" NEW_LINE
"el " RIGHT_ARROW " boton" NEW_LINE
"para moverse" NEW_LINE
"al campo de" NEW_LINE
"habilitado." NEW_LINE
NEW_LINE
"Si el dispositivo" NEW_LINE
"esta actualmente" NEW_LINE
"habilitado, una" NEW_LINE
"marca de" NEW_LINE
"verificacion" NEW_LINE
"aparece en el" NEW_LINE
"campo de Habilitado." NEW_LINE
"Presione + o -" NEW_LINE
"para remover la" NEW_LINE
"marca de" NEW_LINE
"verificacion." NEW_LINE
NEW_LINE
"Si el dispositivo" NEW_LINE
"esta actualmente" NEW_LINE
"deshabilitado," NEW_LINE
"no aparece una" NEW_LINE
"marca de" NEW_LINE
"verificacion en el" NEW_LINE
"campo de Habilitado." NEW_LINE
"presione + o -" NEW_LINE
"para reemplazar" NEW_LINE
"la marca de" NEW_LINE
"verificacion" NEW_LINE
NEW_LINE
"Presione el " DOWN_ARROW " boton" NEW_LINE
"para moverse al" NEW_LINE
"campo de Velocidad" NEW_LINE
"de Flujo." NEW_LINE
NEW_LINE
"Presione + o -" NEW_LINE
"para cambiar los" NEW_LINE
"galones por minuto" NEW_LINE
"en la zona." NEW_LINE
NEW_LINE
"Continue hasta que" NEW_LINE
"haya configurado" NEW_LINE
"todos los ajustes" NEW_LINE
"para todas las" NEW_LINE
"zonas.",

//--------------------
//4.2.1 Device Assignment (MV/Pump)
//--------------------
"Asignacion de" NEW_LINE
"Dispositivo" NEW_LINE
"(MV/Bomba)" NEW_LINE
NEW_LINE
"Cuando Buscar esta" NEW_LINE
"resaltado," NEW_LINE
"presione OK para" NEW_LINE
"buscar por biCoders." NEW_LINE
NEW_LINE
"El controlador" NEW_LINE
"encuentra los" NEW_LINE
"dispositivos" NEW_LINE
"y muestra sus" NEW_LINE
"numeros de serie" NEW_LINE
"en la columna de" NEW_LINE
"Accion." NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para resaltar el" NEW_LINE
"numero de serie" NEW_LINE
"del dispositivo" NEW_LINE
"que desea asignar" NEW_LINE
"a una valvula maestra" NEW_LINE
"o bomba de inicio." NEW_LINE
NEW_LINE
"Presione el " RIGHT_ARROW " boton" NEW_LINE
"para moverse a la" NEW_LINE
"columna de MV/bomba." NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para resaltar en" NEW_LINE
"numero de la" NEW_LINE
"MV/bomba a la cual" NEW_LINE
"desea asignar el" NEW_LINE
"dispositivo." NEW_LINE
NEW_LINE
"Presione OK para" NEW_LINE
"seleccionar el" NEW_LINE
"numero de la" NEW_LINE
"MV/bomba" NEW_LINE
"resaltado." NEW_LINE
"El numero de serie" NEW_LINE
"del dispositivo" NEW_LINE
"que selecciono en" NEW_LINE
"la columna de" NEW_LINE
"accion ahora" NEW_LINE
"se muestra a lado" NEW_LINE
"del numero de" NEW_LINE
"MV/bomba en la" NEW_LINE
"columna MV/bomba." NEW_LINE
NEW_LINE
"Continue hasta que" NEW_LINE
"haya asignado" NEW_LINE
"todas las MV/bomba" NEW_LINE
"biCoders a sus" NEW_LINE
"numeros.",

//--------------------
//4.2.2 Device Setup (MV/Pump)
//--------------------
"Configuracion de" NEW_LINE
"Dispositivo" NEW_LINE
"(MV/Bomba)" NEW_LINE
NEW_LINE
"Despues de asignar" NEW_LINE
"un dispositivo" NEW_LINE
"para un uso" NEW_LINE
"especifico en el" NEW_LINE
"controlador, el" NEW_LINE
"dispositivo esta" NEW_LINE
"habilitado" NEW_LINE
"automaticamente." NEW_LINE
"Puede deshabilitar" NEW_LINE
"ese dispositivo si" NEW_LINE
"desea ponerlo" NEW_LINE
"fuera de servicio" NEW_LINE
"temporalmente y" NEW_LINE
"puede habilitarlo" NEW_LINE
"despues si es" NEW_LINE
"necesario." NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para resaltar el" NEW_LINE
"numero de la" NEW_LINE
"MV/Bomba biCoder" NEW_LINE
"en la cual" NEW_LINE
"desea cambiar la" NEW_LINE
"configuracion y" NEW_LINE
"despues presione" NEW_LINE
"el " RIGHT_ARROW " boton" NEW_LINE
"para moverse" NEW_LINE
"al campo de" NEW_LINE
"habilitado." NEW_LINE
NEW_LINE
"Si el dispositivo" NEW_LINE
"esta actualmente" NEW_LINE
"habilitado, una" NEW_LINE
"marca de" NEW_LINE
"verificacion" NEW_LINE
"aparece en el" NEW_LINE
"campo de Habilitado." NEW_LINE
"Presione + o -" NEW_LINE
"para remover la" NEW_LINE
"marca de" NEW_LINE
"verificacion." NEW_LINE
NEW_LINE
"Si el dispositivo" NEW_LINE
"esta actualmente" NEW_LINE
"deshabilitado," NEW_LINE
"no aparece una" NEW_LINE
"marca de" NEW_LINE
"verificacion en el" NEW_LINE
"campo de Habilitado." NEW_LINE
"presione + o -" NEW_LINE
"para reemplazar" NEW_LINE
"la marca de" NEW_LINE
"verificacion.",

//--------------------
//4.3.1 Device Assignment (Moisture Sensor)
//--------------------
"Asignacion de" NEW_LINE
"Dispositivo" NEW_LINE
"(Sensor de Humedad)" NEW_LINE
NEW_LINE
"Cuando Buscar esta" NEW_LINE
"resaltado," NEW_LINE
"presione OK para" NEW_LINE
"buscar sensores de" NEW_LINE
"humedad." NEW_LINE
NEW_LINE
"El controlador" NEW_LINE
"encuentra los" NEW_LINE
"dispositivos" NEW_LINE
"y muestra sus" NEW_LINE
"numeros de serie" NEW_LINE
"en la columna de" NEW_LINE
"Accion." NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para resaltar el" NEW_LINE
"numero de serie" NEW_LINE
"del dispositivo" NEW_LINE
"que desea asignar" NEW_LINE
"a un numero de" NEW_LINE
"sensor de" NEW_LINE
"humedad." NEW_LINE
NEW_LINE
"Presione el " RIGHT_ARROW " boton" NEW_LINE
"para moverse a la" NEW_LINE
"columna de Sensor" NEW_LINE
"de humedad." NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para resaltar en" NEW_LINE
"numero del sensor" NEW_LINE
"de humedad al cual" NEW_LINE
"desea asignar el" NEW_LINE
"dispositivo." NEW_LINE
NEW_LINE
"Presione OK para" NEW_LINE
"seleccionar el" NEW_LINE
"numero del sensor" NEW_LINE
"de humedad" NEW_LINE
"resaltado." NEW_LINE
"El numero de serie" NEW_LINE
"del dispositivo" NEW_LINE
"que selecciono en" NEW_LINE
"la columna de" NEW_LINE
"accion ahora" NEW_LINE
"se muestra a lado" NEW_LINE
"del numero del" NEW_LINE
"sensor de humedad" NEW_LINE
"en la columna de" NEW_LINE
"Sensor de Humedad." NEW_LINE
NEW_LINE
"Continue hasta que" NEW_LINE
"haya asignado" NEW_LINE
"todos los sensores" NEW_LINE
"de humedad" NEW_LINE
"a sus numeros.",

//--------------------
//4.3.2 Device Setup  (Moisture Sensor)
//--------------------
"Configuracion de" NEW_LINE
"Dispositivos" NEW_LINE
"(Sensor de Humedad)" NEW_LINE
NEW_LINE
"Despues de asignar" NEW_LINE
"un dispositivo" NEW_LINE
"para un uso" NEW_LINE
"especifico en el" NEW_LINE
"controlador, el" NEW_LINE
"dispositivo esta" NEW_LINE
"habilitado" NEW_LINE
"automaticamente." NEW_LINE
"Puede deshabilitar" NEW_LINE
"ese dispositivo si" NEW_LINE
"desea ponerlo" NEW_LINE
"fuera de servicio" NEW_LINE
"temporalmente y" NEW_LINE
"puede habilitarlo" NEW_LINE
"despues si es" NEW_LINE
"necesario." NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para resaltar el" NEW_LINE
"numero del sensor" NEW_LINE
"de humedad en el cual" NEW_LINE
"desea cambiar la" NEW_LINE
"configuracion y" NEW_LINE
"despues presione" NEW_LINE
"el " RIGHT_ARROW " boton" NEW_LINE
"para moverse" NEW_LINE
"al campo de" NEW_LINE
"habilitado." NEW_LINE
NEW_LINE
"Si el dispositivo" NEW_LINE
"esta actualmente" NEW_LINE
"habilitado, una" NEW_LINE
"marca de" NEW_LINE
"verificacion" NEW_LINE
"aparece en el" NEW_LINE
"campo de Habilitado." NEW_LINE
"Presione + o -" NEW_LINE
"para remover la" NEW_LINE
"marca de" NEW_LINE
"verificacion." NEW_LINE
NEW_LINE
"Si el dispositivo" NEW_LINE
"esta actualmente" NEW_LINE
"deshabilitado," NEW_LINE
"no aparece una" NEW_LINE
"marca de" NEW_LINE
"verificacion en el" NEW_LINE
"campo de Habilitado." NEW_LINE
"presione + o -" NEW_LINE
"para reemplazar" NEW_LINE
"la marca de" NEW_LINE
"verificacion.",

//--------------------
//4.4.1 Device Assignment (Flow Sensor)
//--------------------
"Asignacion de" NEW_LINE
"Dispositivo" NEW_LINE
"(Sensor de Flujo)" NEW_LINE
NEW_LINE
"Cuando Buscar esta" NEW_LINE
"resaltado," NEW_LINE
"presione OK para" NEW_LINE
"buscar sensores de" NEW_LINE
"flujo." NEW_LINE
NEW_LINE
"El controlador" NEW_LINE
"encuentra los" NEW_LINE
"dispositivos" NEW_LINE
"y muestra sus" NEW_LINE
"numeros de serie" NEW_LINE
"en la columna de" NEW_LINE
"Accion." NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para resaltar el" NEW_LINE
"numero de serie" NEW_LINE
"del dispositivo" NEW_LINE
"que desea asignar" NEW_LINE
"a un numero de" NEW_LINE
"sensor de flujo." NEW_LINE
NEW_LINE
"Presione la FECHA DERECHA" NEW_LINE
"para moverse a la" NEW_LINE
"columna de Sensor" NEW_LINE
"de Flujo." NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para resaltar en" NEW_LINE
"numero del sensor" NEW_LINE
"de flujo al cual" NEW_LINE
"desea asignar el" NEW_LINE
"dispositivo." NEW_LINE
NEW_LINE
"Presione OK para" NEW_LINE
"seleccionar el" NEW_LINE
"numero del sensor" NEW_LINE
"de flujo" NEW_LINE
"resaltado." NEW_LINE
"El numero de serie" NEW_LINE
"del dispositivo" NEW_LINE
"que selecciono en" NEW_LINE
"la columna de" NEW_LINE
"accion ahora" NEW_LINE
"se muestra a lado" NEW_LINE
"del numero del" NEW_LINE
"sensor de flujo en" NEW_LINE
"la columna de" NEW_LINE
"Sensor de flujo." NEW_LINE
NEW_LINE
"Continue hasta que" NEW_LINE
"haya asignado" NEW_LINE
"todos los sensores" NEW_LINE
"de flujo a sus" NEW_LINE
"numeros.",

//--------------------
//4.4.2 Device Setup  (Flow Sensor)
//--------------------
"Configuracion de" NEW_LINE
"Dispositivo" NEW_LINE
"(Sensor de Flujo)" NEW_LINE
NEW_LINE
"Despues de asignar" NEW_LINE
"un dispositivo" NEW_LINE
"para un uso" NEW_LINE
"especifico en el" NEW_LINE
"controlador, el" NEW_LINE
"dispositivo esta" NEW_LINE
"habilitado" NEW_LINE
"automaticamente." NEW_LINE
"Puede deshabilitar" NEW_LINE
"ese dispositivo si" NEW_LINE
"desea ponerlo" NEW_LINE
"fuera de servicio" NEW_LINE
"temporalmente y" NEW_LINE
"puede habilitarlo" NEW_LINE
"despues si es" NEW_LINE
"necesario." NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para resaltar el" NEW_LINE
"numero del sensor" NEW_LINE
"de flujo en el cual" NEW_LINE
"desea cambiar la" NEW_LINE
"configuracion y" NEW_LINE
"despues presione" NEW_LINE
"el " RIGHT_ARROW " boton" NEW_LINE
"para moverse" NEW_LINE
"al campo de" NEW_LINE
"habilitado." NEW_LINE
NEW_LINE
"Si el dispositivo" NEW_LINE
"esta actualmente" NEW_LINE
"habilitado," NEW_LINE
"una marca de" NEW_LINE
"verificacion" NEW_LINE
"aparece en el" NEW_LINE
"campo de Habilitado." NEW_LINE
"Presione + o -" NEW_LINE
"para remover la" NEW_LINE
"marca de" NEW_LINE
"verificacion." NEW_LINE
NEW_LINE
"Si el dispositivo" NEW_LINE
"esta actualmente" NEW_LINE
"deshabilitado," NEW_LINE
"no aparece una" NEW_LINE
"marca de" NEW_LINE
"verificacion en el" NEW_LINE
"campo de Habilitado." NEW_LINE
"presione + o -" NEW_LINE
"para reemplazar" NEW_LINE
"la marca de" NEW_LINE
"verificacion.",

//--------------------
//4.5.1 Device Assignment (Event Switch)
//--------------------
"Asignacion de" NEW_LINE
"Dispositivo" NEW_LINE
"(Interruptor de" NEW_LINE
"Evento)" NEW_LINE
NEW_LINE
"Cuando Buscar esta" NEW_LINE
"resaltado," NEW_LINE
"presione OK para" NEW_LINE
"buscar" NEW_LINE
"Interruptores de" NEW_LINE
"evento." NEW_LINE
NEW_LINE
"El controlador" NEW_LINE
"encuentra los" NEW_LINE
"dispositivos" NEW_LINE
"y muestra sus" NEW_LINE
"numeros de serie" NEW_LINE
"en la columna de" NEW_LINE
"Accion." NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para resaltar el" NEW_LINE
"numero de serie" NEW_LINE
"del dispositivo" NEW_LINE
"que desea asignar" NEW_LINE
"a un numero de" NEW_LINE
"interruptor de" NEW_LINE
"evento." NEW_LINE
NEW_LINE
"Presione la " RIGHT_ARROW NEW_LINE
"para moverse a la" NEW_LINE
"columna de" NEW_LINE
"Interruptor" NEW_LINE
"de evento." NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para resaltar en" NEW_LINE
"numero del sensor" NEW_LINE
"de flujo al cual" NEW_LINE
"desea asignar el" NEW_LINE
"dispositivo." NEW_LINE
NEW_LINE
"Presione OK para" NEW_LINE
"seleccionar el" NEW_LINE
"numero del" NEW_LINE
"interruptor de cambio" NEW_LINE
"resaltado." NEW_LINE
"El numero de serie" NEW_LINE
"del dispositivo" NEW_LINE
"que selecciono en" NEW_LINE
"la columna de" NEW_LINE
"accion ahora" NEW_LINE
"se muestra a lado" NEW_LINE
"del numero del" NEW_LINE
"interruptor de" NEW_LINE
"cambio en la" NEW_LINE
"columna del" NEW_LINE
"Interruptor de Cambio." NEW_LINE
NEW_LINE
"Continue hasta que" NEW_LINE
"haya asignado" NEW_LINE
"todos los" NEW_LINE
"interruptores de" NEW_LINE
"evento" NEW_LINE
"a sus numeros.",

//--------------------
//4.5.2 Device Setup  (Event Switch)
//--------------------
"Configuracion de" NEW_LINE
"Dispositivo" NEW_LINE
"(Interruptor de" NEW_LINE
"Evento)" NEW_LINE
NEW_LINE
"Despues de asignar" NEW_LINE
"un dispositivo" NEW_LINE
"para un uso" NEW_LINE
"especifico en el" NEW_LINE
"controlador, el" NEW_LINE
"dispositivo esta" NEW_LINE
"habilitado" NEW_LINE
"automaticamente." NEW_LINE
"Puede deshabilitar" NEW_LINE
"ese dispositivo si" NEW_LINE
"desea ponerlo" NEW_LINE
"fuera de servicio" NEW_LINE
"temporalmente y" NEW_LINE
"puede habilitarlo" NEW_LINE
"despues si es" NEW_LINE
"necesario." NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para resaltar el" NEW_LINE
"numero del sensor" NEW_LINE
"de flujo en el cual" NEW_LINE
"desea cambiar la" NEW_LINE
"configuracion y" NEW_LINE
"despues presione" NEW_LINE
"el " RIGHT_ARROW " boton" NEW_LINE
"para moverse" NEW_LINE
"al campo de" NEW_LINE
"habilitado." NEW_LINE
NEW_LINE
"Si el dispositivo" NEW_LINE
"esta actualmente" NEW_LINE
"habilitado," NEW_LINE
"una marca de" NEW_LINE
"verificacion" NEW_LINE
"aparece en el" NEW_LINE
"campo de Habilitado." NEW_LINE
"Presione + o -" NEW_LINE
"para remover la" NEW_LINE
"marca de" NEW_LINE
"verificacion." NEW_LINE
NEW_LINE
"Si el dispositivo" NEW_LINE
"esta actualmente" NEW_LINE
"deshabilitado," NEW_LINE
"no aparece una" NEW_LINE
"marca de" NEW_LINE
"verificacion en el" NEW_LINE
"campo de Habilitado." NEW_LINE
"presione + o -" NEW_LINE
"para reemplazar" NEW_LINE
"la marca de" NEW_LINE
"verificacion.",

//--------------------
//4.6.1 Device Assignment (Temp. Sensor)
//--------------------
"Asignacion de" NEW_LINE
"Dispositivo" NEW_LINE
"(Sensor de" NEW_LINE
"Temperatura)" NEW_LINE
NEW_LINE
"Cuando Buscar esta" NEW_LINE
"resaltado," NEW_LINE
"presione OK para" NEW_LINE
"buscar sensores de" NEW_LINE
"temperatura." NEW_LINE
NEW_LINE
"El controlador" NEW_LINE
"encuentra los" NEW_LINE
"dispositivos" NEW_LINE
"y muestra sus" NEW_LINE
"numeros de serie" NEW_LINE
"en la columna de" NEW_LINE
"Accion." NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para resaltar el" NEW_LINE
"numero de serie" NEW_LINE
"del dispositivo" NEW_LINE
"que desea asignar" NEW_LINE
"a un numero de" NEW_LINE
"sensor de" NEW_LINE
"temperatura." NEW_LINE
NEW_LINE
"Presione el " RIGHT_ARROW " boton" NEW_LINE
"para moverse a la" NEW_LINE
"columna de Sensor" NEW_LINE
"de Temperatura." NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para resaltar en" NEW_LINE
"numero del sensor" NEW_LINE
"de temperatura" NEW_LINE
"al cual desea" NEW_LINE
"asignar el" NEW_LINE
"dispositivo." NEW_LINE
NEW_LINE
"Presione OK para" NEW_LINE
"seleccionar el" NEW_LINE
"numero del sensor" NEW_LINE
"de temperatura" NEW_LINE
"resaltado." NEW_LINE
"El numero de serie" NEW_LINE
"del dispositivo" NEW_LINE
"que selecciono en" NEW_LINE
"la columna de" NEW_LINE
"Accion ahora" NEW_LINE
"se muestra a lado" NEW_LINE
"del numero del" NEW_LINE
"sensor de" NEW_LINE
"temperatura en la" NEW_LINE
"columna de Sensor" NEW_LINE
"de Temperatura" NEW_LINE
NEW_LINE
"Continue hasta que" NEW_LINE
"haya asignado" NEW_LINE
"todos los sensores" NEW_LINE
"de temperatura" NEW_LINE
"a sus numeros.",

//--------------------
//4.6.2 Device Setup  (Temp. Sensor)
//--------------------
"Configuracion de" NEW_LINE
"Dispositivo" NEW_LINE
"(Sensor de" NEW_LINE
"temperaura)" NEW_LINE
NEW_LINE
"Despues de asignar" NEW_LINE
"un dispositivo" NEW_LINE
"para un uso" NEW_LINE
"especifico en el" NEW_LINE
"controlador, el" NEW_LINE
"dispositivo esta" NEW_LINE
"habilitado" NEW_LINE
"automaticamente." NEW_LINE
"Puede deshabilitar" NEW_LINE
"ese dispositivo si" NEW_LINE
"desea ponerlo" NEW_LINE
"fuera de servicio" NEW_LINE
"temporalmente y" NEW_LINE
"puede habilitarlo" NEW_LINE
"despues si es" NEW_LINE
"necesario." NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para resaltar el" NEW_LINE
"numero del sensor" NEW_LINE
"de temperatura" NEW_LINE
"en el cual" NEW_LINE
"desea cambiar la" NEW_LINE
"configuracion y" NEW_LINE
"despues presione" NEW_LINE
"el " RIGHT_ARROW " boton" NEW_LINE
"para moverse" NEW_LINE
"al campo de" NEW_LINE
"habilitado." NEW_LINE
NEW_LINE
"Si el dispositivo" NEW_LINE
"esta actualmente" NEW_LINE
"habilitado," NEW_LINE
"una marca de" NEW_LINE
"verificacion" NEW_LINE
"aparece en el" NEW_LINE
"campo de Habilitado." NEW_LINE
"Presione + o -" NEW_LINE
"para remover la" NEW_LINE
"marca de" NEW_LINE
"verificacion." NEW_LINE
NEW_LINE
"Si el dispositivo" NEW_LINE
"esta actualmente" NEW_LINE
"deshabilitado," NEW_LINE
"no aparece una" NEW_LINE
"marca de" NEW_LINE
"verificacion en el" NEW_LINE
"campo de Habilitado." NEW_LINE
"presione + o -" NEW_LINE
"para reemplazar" NEW_LINE
"la marca de" NEW_LINE
"verificacion.",
NEW_LINE
//####################
//5.0 Manual Run Menu
//####################
"Menu de Ejecucion" NEW_LINE
"Manual" NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para moverse a la" NEW_LINE
"opcion que desee" NEW_LINE
"utilizar y despues" NEW_LINE
"presione OK.",

//--------------------
//5.1 Run Zone(s) Screen
//--------------------
"Pantalla de" NEW_LINE
"Ejecucion de Zona(s)" NEW_LINE
NEW_LINE
"Si desea permitir" NEW_LINE
"que mas de una" NEW_LINE
"zona funcione al" NEW_LINE
"mismo tiempo durante" NEW_LINE
"la ejecucion" NEW_LINE
"manual, presione" NEW_LINE
"el " DOWN_ARROW " boton para" NEW_LINE
"resaltar el" NEW_LINE
"Concurrente Maximo" NEW_LINE
"y despues presione" NEW_LINE
"+ o - para cambiar" NEW_LINE
"el numero." NEW_LINE
NEW_LINE
"SI desea retrasar" NEW_LINE
"el inicio del la" NEW_LINE
"ejecucion manual," NEW_LINE
"presione el " DOWN_ARROW " boton" NEW_LINE
"para seleccionar" NEW_LINE
"Retraso de Inicio" NEW_LINE
"y despues presione" NEW_LINE
"+ o - para cambiar" NEW_LINE
"el tiempo." NEW_LINE
NEW_LINE
"Si desea un" NEW_LINE
"retraso entre las" NEW_LINE
"zonas de la" NEW_LINE
"ejecucion manual" NEW_LINE
"presione el " DOWN_ARROW " boton" NEW_LINE
"para resaltar" NEW_LINE
"Retraso de Zona," NEW_LINE
"y despues presione" NEW_LINE
"+ o - para" NEW_LINE
"cambiar el tiempo" NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " boton" NEW_LINE
"para resaltar la" NEW_LINE
"opcion de los" NEW_LINE
"tiempos de" NEW_LINE
"ejecucion manual" NEW_LINE
"establecidos" NEW_LINE
"y despues presione OK" NEW_LINE
NEW_LINE
"Si desea iniciar" NEW_LINE
"otra ejecucion manual" NEW_LINE
"con diferentes" NEW_LINE
"tiempos de ejecucion," NEW_LINE
"Resalte la opcion" NEW_LINE
"de Limpiar Tiempos" NEW_LINE
"de Ejecucion" NEW_LINE
"Manual y despues" NEW_LINE
"presione OK." NEW_LINE
NEW_LINE
"El controlador" NEW_LINE
"automaticamente borra" NEW_LINE
"todos los tiempos" NEW_LINE
"de ejecucion" NEW_LINE
"introducidos" NEW_LINE
"en los campos de" NEW_LINE
"la pantalla de" NEW_LINE
"Ejecucion de" NEW_LINE
"Zona(s).",

//--------------------
//5.1.1 Set Manual Runtimes Screen
//--------------------
"Pantalla de ajuste" NEW_LINE
"de Tiempos de" NEW_LINE
"Ejecucion Manual" NEW_LINE
NEW_LINE
"El marcador de" NEW_LINE
"minutos en el" NEW_LINE
"campo de la Zona 1" NEW_LINE
"esta resaltado." NEW_LINE
NEW_LINE
"Para movese al" NEW_LINE
"marcador de horas" NEW_LINE
"presione el " LEFT_ARROW " boton." NEW_LINE
NEW_LINE
"Para moverse al" NEW_LINE
"marcador de segundos" NEW_LINE
"presione el " RIGHT_ARROW " boton." NEW_LINE
NEW_LINE
"Para cambiar el" NEW_LINE
"tiempo presione +" NEW_LINE
"o -." NEW_LINE
NEW_LINE
"Para aumentar o" NEW_LINE
"disminuir el valor" NEW_LINE
"rapidamente" NEW_LINE
"mantenga" NEW_LINE
"presionado + o -." NEW_LINE
NEW_LINE
"Para moverse a una" NEW_LINE
"zona diferente" NEW_LINE
"pesione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
NEW_LINE
"Despues de" NEW_LINE
"ajaustar la" NEW_LINE
"cantidad de tiempo" NEW_LINE
"para cada zona que" NEW_LINE
"desea ejecuatar" NEW_LINE
"manualmente" NEW_LINE
"presione OK.",

//--------------------
//5.2 Run All Zones Screen
//--------------------
"Pantalla de" NEW_LINE
"Ejecucion de Todas" NEW_LINE
"las Zonas" NEW_LINE
NEW_LINE
"La opcion para" NEW_LINE
"Ejecutar" NEW_LINE
"Manualmente Todas" NEW_LINE
"las Zonas ejecuta" NEW_LINE
"zonas por programas" NEW_LINE
NEW_LINE
"Verifique que el" NEW_LINE
"numero correcto" NEW_LINE
"del programa aparezca" NEW_LINE
"en la esquina" NEW_LINE
"superior" NEW_LINE
"izquierda." NEW_LINE
"Presione PRG" NEW_LINE
"para cambiarlo si" NEW_LINE
"es necesario." NEW_LINE
NEW_LINE
"El marcador de" NEW_LINE
"minutos en el" NEW_LINE
"campo de la Zona 1" NEW_LINE
"esta resaltado." NEW_LINE
NEW_LINE
"Para movese al" NEW_LINE
"marcador de horas" NEW_LINE
"presione el " LEFT_ARROW " boton." NEW_LINE
NEW_LINE
"Para moverse al" NEW_LINE
"marcador de segundos" NEW_LINE
"presione el " RIGHT_ARROW " boton." NEW_LINE
NEW_LINE
"Para cambiar el" NEW_LINE
"tiempo presione +" NEW_LINE
"o -." NEW_LINE
NEW_LINE
"Para aumentar o" NEW_LINE
"disminuir el valor" NEW_LINE
"rapidamente" NEW_LINE
"mantenga" NEW_LINE
"presionado + o -." NEW_LINE
NEW_LINE
"Si desea permitir" NEW_LINE
"que mas de una" NEW_LINE
"zona funcione al" NEW_LINE
"mismo tiempo durante" NEW_LINE
"la ejecucion" NEW_LINE
"manual, presione" NEW_LINE
"el " DOWN_ARROW " boton para" NEW_LINE
"resaltar el" NEW_LINE
"Concurrente Maximo" NEW_LINE
"y despues presione" NEW_LINE
"+ o - para cambiar" NEW_LINE
"el numero." NEW_LINE
NEW_LINE
"SI desea retrasar" NEW_LINE
"el inicio del la" NEW_LINE
"ejecucion manual," NEW_LINE
"presione el " DOWN_ARROW " boton" NEW_LINE
"para seleccionar" NEW_LINE
"Retraso de Inicio" NEW_LINE
"y despues presione" NEW_LINE
"+ o - para cambiar" NEW_LINE
"el tiempo." NEW_LINE
NEW_LINE
"Si desea un" NEW_LINE
"retraso entre las" NEW_LINE
"zonas de la" NEW_LINE
"ejecucion manual" NEW_LINE
"presione el " DOWN_ARROW " boton" NEW_LINE
"para resaltar" NEW_LINE
"Retraso de Zona," NEW_LINE
"y despues presione" NEW_LINE
"+ o - para" NEW_LINE
"cambiar el tiempo" NEW_LINE
NEW_LINE
"Despues de ajustar" NEW_LINE
"el tiempo de" NEW_LINE
"ejecucion para las" NEW_LINE
"zonas presione OK.",

//--------------------
//5.3 Run MVs/Pumps Screen
//--------------------
"Pantalla de" NEW_LINE
"Funcionamiento de" NEW_LINE
"MVs/ Bombas",

//--------------------
//5.4 Start/Stop Program
//--------------------
"Programa de" NEW_LINE
"Iniciar/Detener" NEW_LINE
NEW_LINE
"En la lista de" NEW_LINE
"programas" NEW_LINE
"presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para moverse al" NEW_LINE
"programa que desea" NEW_LINE
"iniciar o detener." NEW_LINE
NEW_LINE
"Si el programa" NEW_LINE
"esta inactivo la" NEW_LINE
"opcion de Inicio?" NEW_LINE
"aparece en el lado" NEW_LINE
"derecho de la" NEW_LINE
"pantalla. Para" NEW_LINE
"iniciar al" NEW_LINE
"programa resalte" NEW_LINE
"la opcion de Inicio?" NEW_LINE
"y despues presione" NEW_LINE
"OK." NEW_LINE
NEW_LINE
"Si el programa" NEW_LINE
"esta ejecutandose," NEW_LINE
"la opcion Detener?" NEW_LINE
"Aparece en el lado" NEW_LINE
"derecho de la" NEW_LINE
"pantalla. Para" NEW_LINE
"detener el" NEW_LINE
"Programa, resalte" NEW_LINE
"la opcion Detener?" NEW_LINE
"y despues presione" NEW_LINE
"OK.",

//####################
//6.0 Test Menu
//####################
"Menu de Prueba" NEW_LINE
NEW_LINE
"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
"para moverse a la" NEW_LINE
"opcion que desee" NEW_LINE
"utilizar" NEW_LINE
"y despues presione" NEW_LINE
"OK.",

////--------------------
////6.1 Test Zone Screen
////--------------------
//"Pantalla de Prueba" NEW_LINE
//"de Zona" NEW_LINE
//NEW_LINE
//"La prueba de zona" NEW_LINE
//"verifica la" NEW_LINE
//"comunicacion entre" NEW_LINE
//"el controlador" NEW_LINE
//"y una valvula" NEW_LINE
//"biCoder. El" NEW_LINE
//"controlador" NEW_LINE
//"activa el" NEW_LINE
//"solenoide y mide" NEW_LINE
//"la corriente" NEW_LINE
//"y el voltage." NEW_LINE
//"Desactiva el" NEW_LINE
//"solenoide y mide" NEW_LINE
//"la disminucion" NEW_LINE
//"de voltage entre" NEW_LINE
//"el controlador y la" NEW_LINE
//"valvula biCoder" NEW_LINE
//NEW_LINE
//"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
//"para seleccionar" NEW_LINE
//"el numero de zona" NEW_LINE
//"que desea probar." NEW_LINE
//NEW_LINE
//"Presione OK para" NEW_LINE
//"probar la zona" NEW_LINE
//NEW_LINE
//"Consulte el Manual" NEW_LINE
//"de Usuario" NEW_LINE
//"para obtener" NEW_LINE
//"informacion sobre" NEW_LINE
//"los resultados de" NEW_LINE
//"prueba.",
//
////--------------------
////6.2 Test MV/Pump Screen
////--------------------
//"Pantalla de Prueba" NEW_LINE
//"de MV/Bomba" NEW_LINE
//NEW_LINE
//"La prueba de MV/Bomba" NEW_LINE
//"verifica la" NEW_LINE
//"comunicacion entre" NEW_LINE
//"el controlador" NEW_LINE
//"y el dispositivo." NEW_LINE
//"El controlador" NEW_LINE
//"activa el" NEW_LINE
//"solenoide y mide" NEW_LINE
//"la corriente" NEW_LINE
//"y el voltage." NEW_LINE
//"Desactiva el" NEW_LINE
//"solenoide y mide" NEW_LINE
//"la disminucion" NEW_LINE
//"de voltage entre" NEW_LINE
//"el controlador y el" NEW_LINE
//"dispositivo." NEW_LINE
//NEW_LINE
//"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
//"para seleccionar" NEW_LINE
//"la MV/Bomba" NEW_LINE
//"que desea probar." NEW_LINE
//NEW_LINE
//"Presione OK para" NEW_LINE
//"probar la MV/Bomba" NEW_LINE
//NEW_LINE
//"Consulte el Manual" NEW_LINE
//"de Usuario" NEW_LINE
//"para obtener" NEW_LINE
//"informacion sobre" NEW_LINE
//"los resultados de" NEW_LINE
//"prueba.",
//
////--------------------
////6.3 Test Moisture Sensor Screen
////--------------------
//"Pantalla de Prueba" NEW_LINE
//"de Sensor de Humedad" NEW_LINE
//NEW_LINE
//"La prueba de" NEW_LINE
//"Sensor de Humedad" NEW_LINE
//"de Suelo" NEW_LINE
//"verifica la" NEW_LINE
//"comunicacion entre" NEW_LINE
//"el controlador" NEW_LINE
//"y el biSensor y" NEW_LINE
//"proporciona el" NEW_LINE
//"estado del" NEW_LINE
//"biSensor, el" NEW_LINE
//"porcentage actual" NEW_LINE
//"de humedad" NEW_LINE
//"del suelo, la" NEW_LINE
//"temperatura del" NEW_LINE
//"suelo del sensor" NEW_LINE
//"en grados" NEW_LINE
//"Fahrenheit y otra" NEW_LINE
//"informacion." NEW_LINE
//NEW_LINE
//"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
//"para seleccionar" NEW_LINE
//"el sensor de" NEW_LINE
//"humedad del suelo" NEW_LINE
//"que desea probar." NEW_LINE
//NEW_LINE
//"Presione OK para" NEW_LINE
//"probar el sensor" NEW_LINE
//"de humedad" NEW_LINE
//"del suelo" NEW_LINE
//NEW_LINE
//"Consulte el Manual" NEW_LINE
//"de Usuario" NEW_LINE
//"para obtener" NEW_LINE
//"informacion sobre" NEW_LINE
//"los resultados de" NEW_LINE
//"prueba.",
//
////--------------------
////6.4 Test Flow Sensor Screen
////--------------------
//"Pantalla de Prueba" NEW_LINE
//"del Sensor de Flujo" NEW_LINE
//NEW_LINE
//"La prueba del" NEW_LINE
//"Sensor de Flujo" NEW_LINE
//"verifica la" NEW_LINE
//"comunicacion entre" NEW_LINE
//"el controlador" NEW_LINE
//"y el dispositivo." NEW_LINE
//"El controlador" NEW_LINE
//"activa el" NEW_LINE
//"dispositivo y mide" NEW_LINE
//"la corriene y el" NEW_LINE
//"voltage. Desactiva" NEW_LINE
//"el dispositivo y" NEW_LINE
//"mide la disminucion" NEW_LINE
//"del voltage entre" NEW_LINE
//"el controlador y el" NEW_LINE
//"dispositivo." NEW_LINE
//NEW_LINE
//"La prueba" NEW_LINE
//"proporciona el" NEW_LINE
//"estado del" NEW_LINE
//"dispositivo, el" NEW_LINE
//"numero de serie," NEW_LINE
//"la velocidad del" NEW_LINE
//"flujo y el uso total." NEW_LINE
//NEW_LINE
//"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
//"para seleccionar" NEW_LINE
//"el sensor de flujo" NEW_LINE
//"que desea probar." NEW_LINE
//NEW_LINE
//"Presione OK para" NEW_LINE
//"probar el sensor" NEW_LINE
//"de flujo.",
//
////--------------------
////6.5 Test Event Switch Screen
////--------------------
//"Pantalla de Prueba" NEW_LINE
//"de Interruptor de" NEW_LINE
//"Evento" NEW_LINE
//NEW_LINE
//"La Prueba de" NEW_LINE
//"Interruptor de Evento" NEW_LINE
//"verifica la" NEW_LINE
//"comunicacion entre" NEW_LINE
//"el controlador" NEW_LINE
//"y el dispositivo." NEW_LINE
//"El controlador" NEW_LINE
//"activa el" NEW_LINE
//"dispositivo y mide" NEW_LINE
//"la corriene y el" NEW_LINE
//"voltage. Desactiva" NEW_LINE
//"el dispositivo y" NEW_LINE
//"mide la disminucion" NEW_LINE
//"del voltage entre" NEW_LINE
//"el controlador y el" NEW_LINE
//"dispositivo." NEW_LINE
//NEW_LINE
//"La prueba" NEW_LINE
//"proporciona el" NEW_LINE
//"estado del" NEW_LINE
//"dispositivo, el" NEW_LINE
//"numero de serie," NEW_LINE
//"y el estado del" NEW_LINE
//"interruptor." NEW_LINE
//NEW_LINE
//"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
//"para seleccionar" NEW_LINE
//"el interruptor de" NEW_LINE
//"evento que desea" NEW_LINE
//"probar." NEW_LINE
//NEW_LINE
//"Presione OK para" NEW_LINE
//"probar interruptor" NEW_LINE
//"de evento.",
//
////--------------------
////6.6 Test Temp. Sensor Screen
////--------------------
//"Pantalla de Prueba" NEW_LINE
//"de Sensor de" NEW_LINE
//"Temperatura" NEW_LINE
//NEW_LINE
//"La Prueba de" NEW_LINE
//"Sensor de Temperatura" NEW_LINE
//"verifica la" NEW_LINE
//"comunicacion entre" NEW_LINE
//"el controlador" NEW_LINE
//"y el dispositivo." NEW_LINE
//"El controlador" NEW_LINE
//"activa el" NEW_LINE
//"dispositivo y mide" NEW_LINE
//"la corriene y el" NEW_LINE
//"voltage." NEW_LINE
//"Desactiva el" NEW_LINE
//"dispositivo y mide" NEW_LINE
//"la disminucion" NEW_LINE
//"del voltage entre" NEW_LINE
//"el controlador y el" NEW_LINE
//"dispositivo." NEW_LINE
//NEW_LINE
//"La prueba" NEW_LINE
//"proporciona el" NEW_LINE
//"estado del" NEW_LINE
//"dispositivo, el" NEW_LINE
//"numero de serie," NEW_LINE
//"y la temperatura" NEW_LINE
//"del aire." NEW_LINE
//NEW_LINE
//"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
//"para seleccionar" NEW_LINE
//"el sensor de" NEW_LINE
//"temperatura" NEW_LINE
//"que desea probar." NEW_LINE
//NEW_LINE
//"Presione OK para" NEW_LINE
//"probar el sensor" NEW_LINE
//"de temperatura.",
//
////--------------------
////6.7 Test Two-Wire Screen
////--------------------
//"Panalla de Prueba" NEW_LINE
//"de Doble Cableado",
//
////--------------------
////6.8 Test All Devices Screen
////--------------------
//"Pantalla de Prueba" NEW_LINE
//"de Todos los" NEW_LINE
//"Dispositivos",

////####################
////7.0 System Setup Menu
////####################
//"Menu de" NEW_LINE
//"Configuracion del" NEW_LINE
//"Sistema" NEW_LINE
//NEW_LINE
//"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
//"para moverse a la" NEW_LINE
//"opcion que desee" NEW_LINE
//"utilizar" NEW_LINE
//"y despues presione" NEW_LINE
//"OK.",
//
////--------------------
////7.1 Time & Date Setup Screen
////--------------------
//"Pantalla de" NEW_LINE
//"Configuracion de" NEW_LINE
//"Tiempo y Fecha" NEW_LINE
//NEW_LINE
//"Asegurece la fecha" NEW_LINE
//"y el tiempo en el" NEW_LINE
//"controlador" NEW_LINE
//"coincida la fecha" NEW_LINE
//"y tiempo actual" NEW_LINE
//"para que su" NEW_LINE
//"calendario de" NEW_LINE
//"riego sea preciso." NEW_LINE
//NEW_LINE
//"El Marcador de" NEW_LINE
//"horas esta" NEW_LINE
//"resaltado en el" NEW_LINE
//"campo de Tiempo." NEW_LINE
//"Para cambiar el" NEW_LINE
//"tiempo presione +" NEW_LINE
//"o -." NEW_LINE
//NEW_LINE
//"Para moverse al" NEW_LINE
//"marcado de minutos," NEW_LINE
//"presione el " RIGHT_ARROW " boton." NEW_LINE
//NEW_LINE
//"Presione el " DOWN_ARROW " boton" NEW_LINE
//"para moverse al" NEW_LINE
//"campo de Fecha." NEW_LINE
//"Presione la" NEW_LINE
//LEFT_ARROW " o " RIGHT_ARROW " para moverse" NEW_LINE
//"al campo de fecha" NEW_LINE
//"que desea cambiar." NEW_LINE
//NEW_LINE
//"Presione + o -" NEW_LINE
//"para cambiar el" NEW_LINE
//"valor en el campo." NEW_LINE
//NEW_LINE
//"Presione el " DOWN_ARROW " boton" NEW_LINE
//"para moverse al" NEW_LINE
//"campo de Dias de" NEW_LINE
//"la Semana y" NEW_LINE
//"despues presione" NEW_LINE
//"+ o - para cambiar" NEW_LINE
//"el valor en el campo." NEW_LINE
//NEW_LINE
//"Presione el " DOWN_ARROW " boton" NEW_LINE
//"para moverse al" NEW_LINE
//"campo de Formato" NEW_LINE
//"de Tiempo" NEW_LINE
//"y presione + o -" NEW_LINE
//"para cambiar el" NEW_LINE
//"valor en el campo." NEW_LINE
//"Este campo permite" NEW_LINE
//"cambiar el formato" NEW_LINE
//"del tiempo entre" NEW_LINE
//"AM/PM y ajustes de" NEW_LINE
//"24 horas.",
//
////--------------------
////7.2 Network Setup Menu
////--------------------
//"Menu de" NEW_LINE
//"Configuracion de Red" NEW_LINE
//NEW_LINE
//"Presione el " UP_ARROW " o " DOWN_ARROW NEW_LINE
//"para moverse a la" NEW_LINE
//"opcion que desee" NEW_LINE
//"utilizar" NEW_LINE
//"y despues presione" NEW_LINE
//"OK.",
//
////--------------------
////7.3 Display Setup Screen
////--------------------
//"Pantalla de" NEW_LINE
//"Configuracion de" NEW_LINE
//"Visualizacion" NEW_LINE
//NEW_LINE
//"Puede ajustar el" NEW_LINE
//"brillo y el" NEW_LINE
//"contraste en la" NEW_LINE
//"pantalla del" NEW_LINE
//"controlador para" NEW_LINE
//"proporcionar" NEW_LINE
//"una mejor" NEW_LINE
//"visibilidad en una" NEW_LINE
//"variedad de" NEW_LINE
//"condiciones de" NEW_LINE
//"iluminacion y para" NEW_LINE
//"acomodar una" NEW_LINE
//"variedad de" NEW_LINE
//"angulos de" NEW_LINE
//"visualizacion." NEW_LINE
//"Tambien puede" NEW_LINE
//"establecer un" NEW_LINE
//"tiempo limite" NEW_LINE
//"para que la" NEW_LINE
//"pantalla entre en" NEW_LINE
//"el modo de reposo." NEW_LINE
//NEW_LINE
//"Presione + o -" NEW_LINE
//"para cambiar" NEW_LINE
//"el numero en el" NEW_LINE
//"campo de Contraste" NEW_LINE
//"de La Pantalla." NEW_LINE
//NEW_LINE
//"Un numero menor" NEW_LINE
//"indica menos" NEW_LINE
//"contraste" NEW_LINE
//"y un numero mayor" NEW_LINE
//"indica mas contraste." NEW_LINE
//NEW_LINE
//"Presione el " DOWN_ARROW " boton" NEW_LINE
//"para moverse al" NEW_LINE
//"campo de Brillo," NEW_LINE
//"y presione + o -" NEW_LINE
//"para cambiar el" NEW_LINE
//"numero." NEW_LINE
//NEW_LINE
//"Presione el " DOWN_ARROW " boton" NEW_LINE
//"para moverse al" NEW_LINE
//"Tiempo de Espera" NEW_LINE
//"y despues presione" NEW_LINE
//"+ o - para cambiar" NEW_LINE
//"el numero.",
//
////--------------------
////7.4 Security Setup Screen
////--------------------
//"Pantalla de" NEW_LINE
//"Configuracion de" NEW_LINE
//"Seguridad" NEW_LINE
//NEW_LINE
//"Puede establecer" NEW_LINE
//"los siguientes" NEW_LINE
//"niveles de" NEW_LINE
//"acceso de seguridad." NEW_LINE
//NEW_LINE
//"Admin: Permite el" NEW_LINE
//"acceso a todas" NEW_LINE
//"las funciones del" NEW_LINE
//"controlador" NEW_LINE
//"Programador:" NEW_LINE
//"Permite el acceso" NEW_LINE
//"a todas" NEW_LINE
//"las funciones del" NEW_LINE
//"controlador" NEW_LINE
//"excepto la funcion" NEW_LINE
//"de seguridad." NEW_LINE
//NEW_LINE
//"Operador: Permite" NEW_LINE
//"acceso al" NEW_LINE
//"menu de ejecucion," NEW_LINE
//"al menu de" NEW_LINE
//"Ejecucion Manual" NEW_LINE
//"y al menu de Prueba." NEW_LINE
//"Cuando el usuario" NEW_LINE
//"accesa al controlador" NEW_LINE
//"con un PIN, el" NEW_LINE
//"controlador" NEW_LINE
//"mantendra ese nivel" NEW_LINE
//"de acceso por una" NEW_LINE
//"hora. Cuando ese" NEW_LINE
//"tiempo se acabe," NEW_LINE
//"cualquier persona" NEW_LINE
//"que trate de usar" NEW_LINE
//"el controlador se" NEW_LINE
//"le pedira que" NEW_LINE
//"introdusca un PIN." NEW_LINE
//NEW_LINE
//"Presione + para" NEW_LINE
//"mostrar una marca" NEW_LINE
//"de verificacion" NEW_LINE
//"en el campo de" NEW_LINE
//"activado. Los" NEW_LINE
//"campos de PIN para" NEW_LINE
//"los niveles de" NEW_LINE
//"seguridad se muetran." NEW_LINE
//NEW_LINE
//"Presione el " DOWN_ARROW " boton" NEW_LINE
//"para seleccionar" NEW_LINE
//"el primer digito" NEW_LINE
//"del PIN de Admin," NEW_LINE
//"y despues presione" NEW_LINE
//"+ o - para cambiar" NEW_LINE
//"el numero." NEW_LINE
//NEW_LINE
//"Presione el " RIGHT_ARROW " boton" NEW_LINE
//"para moverse al" NEW_LINE
//"siguiente digito," NEW_LINE
//"y despues" NEW_LINE
//"presione + o -" NEW_LINE
//"para cambiar el" NEW_LINE
//"numero. Repita" NEW_LINE
//"este paso hasta" NEW_LINE
//"que haya configurado" NEW_LINE
//"todos los digitos" NEW_LINE
//"del PIN." NEW_LINE
//NEW_LINE
//"Presione el " DOWN_ARROW " boton" NEW_LINE
//"para resaltar el" NEW_LINE
//"siguiene PIN" NEW_LINE
//"y repita el paso" NEW_LINE
//"anterior hasta que" NEW_LINE
//"haya configurado" NEW_LINE
//"todos los PINs.",
//
////--------------------
////7.5 Clear Programming Screen
////--------------------
//"Pantalla de" NEW_LINE
//"Borrado de" NEW_LINE
//"Programacion" NEW_LINE
//NEW_LINE
//"Esta funcion" NEW_LINE
//"limpiara o borrara" NEW_LINE
//"toda la informacion" NEW_LINE
//"de programacion" NEW_LINE
//"del controlador." NEW_LINE
//"Recomendamos que" NEW_LINE
//"use esta funcion" NEW_LINE
//"solamante" NEW_LINE
//"como lo indica el" NEW_LINE
//"Soporte del Baseline." NEW_LINE
//NEW_LINE
//"NOTA IMPORTANTE!" NEW_LINE
//"Nunca borre sus" NEW_LINE
//"datos de programacion" NEW_LINE
//"sin tener una" NEW_LINE
//"copia de seguridad" NEW_LINE
//"actual disponible" NEW_LINE
//"para una" NEW_LINE
//"restauracion." NEW_LINE
//NEW_LINE
//"Para borrar todos" NEW_LINE
//"los datos de" NEW_LINE
//"programacion" NEW_LINE
//"presione +.",
//
////--------------------
////7.6 Backup & Restore Menu
////--------------------
//"Menu de Copia de" NEW_LINE
//"Seguridad" NEW_LINE
//"(Respaldo) y" NEW_LINE
//"Restaurado" NEW_LINE
//NEW_LINE
//"Presione la " UP_ARROW " o " DOWN_ARROW NEW_LINE
//"para moverse a la" NEW_LINE
//"opcion que desea" NEW_LINE
//"utilizar y despues" NEW_LINE
//"presione OK." NEW_LINE
//NEW_LINE
//"Copia de Seguridad" NEW_LINE
//"USB: Copia" NEW_LINE
//"(Respalda)toda la" NEW_LINE
//"informacion de" NEW_LINE
//"programacion del" NEW_LINE
//"controlador a" NEW_LINE
//"la memoria USB." NEW_LINE
//"Utilice esta" NEW_LINE
//"opcion si planea" NEW_LINE
//"guardar esta copia" NEW_LINE
//"de seguridad a" NEW_LINE
//"largo plazo." NEW_LINE
//NEW_LINE
//"Copia de seguridad" NEW_LINE
//"Local: Copia" NEW_LINE
//"(Respalda) toda la" NEW_LINE
//"informacion" NEW_LINE
//"de programacion a" NEW_LINE
//"la memoria del" NEW_LINE
//"controlador." NEW_LINE
//"Esta opcion es" NEW_LINE
//"util para guardar" NEW_LINE
//"la copia de" NEW_LINE
//"seguridad por un" NEW_LINE
//"corto tiempo." NEW_LINE
//NEW_LINE
//"Si respaldo" NEW_LINE
//"(copio) su" NEW_LINE
//"programacion a una" NEW_LINE
//"memoria USB, usted" NEW_LINE
//"restaurara con esa" NEW_LINE
//"misma memoria USB" NEW_LINE
//"usando la opcion" NEW_LINE
//"de restauracion USB." NEW_LINE
//NEW_LINE
//"Si respaldo" NEW_LINE
//"(copio) en la" NEW_LINE
//"memoria del" NEW_LINE
//"controlador, usara" NEW_LINE
//"la opcion de" NEW_LINE
//"Restauracion Local.",
//
////--------------------
////7.7 Firmware Update Menu
////--------------------
//"Menu de" NEW_LINE
//"Actualizacion del" NEW_LINE
//"Firmware" NEW_LINE
//NEW_LINE
//"Use la" NEW_LINE
//"actualizacion USB" NEW_LINE
//"cuando su" NEW_LINE
//"controlador no" NEW_LINE
//"este conectado al" NEW_LINE
//"Internet." NEW_LINE
//NEW_LINE
//"Use la" NEW_LINE
//"actualizacion" NEW_LINE
//"BaseManager" NEW_LINE
//"cuando el" NEW_LINE
//"controlador este" NEW_LINE
//"conectado" NEW_LINE
//"al Internet.",
//
////--------------------
////7.8 Export Data Screen
////--------------------
//"Pantalla de" NEW_LINE
//"Exportacion de Datos" NEW_LINE
//NEW_LINE
//"Conecte la memoria" NEW_LINE
//"USB en le puerto" NEW_LINE
//"USB del" NEW_LINE
//"controlador" NEW_LINE
//NEW_LINE
//"Presione + para" NEW_LINE
//"exportar los datos." NEW_LINE
//"la pantalla" NEW_LINE
//"muestra las fechas" NEW_LINE
//"de los datos que" NEW_LINE
//"estan siendo" NEW_LINE
//"exportados. Cuando" NEW_LINE
//"la exportacion" NEW_LINE
//"es completada se" NEW_LINE
//"muestra la" NEW_LINE
//"pantalla original." NEW_LINE
//NEW_LINE
//"Desconecte la" NEW_LINE
//"memoria USB del" NEW_LINE
//"puerto USB" NEW_LINE
//"de controlador." NEW_LINE
//"Puede encontrar" NEW_LINE
//"los archivos" NEW_LINE
//"exportados en la" NEW_LINE
//"memoria USB en un" NEW_LINE
//"folder marcado con" NEW_LINE
//"el numero de serie" NEW_LINE
//"del controlador.",

////--------------------
////7.2.1 Ethernet Setup Screen
////--------------------
//"Pantalla de" NEW_LINE
//"Configuracion del" NEW_LINE
//"Ethernet" NEW_LINE
//NEW_LINE
//"Si desea utilizar" NEW_LINE
//"LiveView de Baseline" NEW_LINE
//"para manejar el" NEW_LINE
//"controlador  a" NEW_LINE
//"control remoto," NEW_LINE
//"necesita habilitar" NEW_LINE
//"DHCP (siglas en" NEW_LINE
//"Ingles, Protocolo" NEW_LINE
//"de Configuracion" NEW_LINE
//"Dinamica de Huesped)" NEW_LINE
//"para establecer la" NEW_LINE
//"coneccion de" NEW_LINE
//"Internet." NEW_LINE
//NEW_LINE
//"Presione + o -" NEW_LINE
//"para cambiar el" NEW_LINE
//"valor en el campo" NEW_LINE
//"del DHCP habilitado." NEW_LINE
//NEW_LINE
//"Si el DHCP esta" NEW_LINE
//"actualmente" NEW_LINE
//"habilitado," NEW_LINE
//"una marca de" NEW_LINE
//"verificacion se" NEW_LINE
//"muestra en el" NEW_LINE
//"campo de" NEW_LINE
//"habilitado." NEW_LINE
//"Presione + o -" NEW_LINE
//"para remover la" NEW_LINE
//"marca." NEW_LINE
//NEW_LINE
//"Si el DHCP esta" NEW_LINE
//"actualmente" NEW_LINE
//"deshabilitado" NEW_LINE
//"no aparece una" NEW_LINE
//"marca de verificacion" NEW_LINE
//"en el campo de" NEW_LINE
//"habilitado." NEW_LINE
//"+ o - para" NEW_LINE
//"reemplazar la marca" NEW_LINE
//"de verificacion" NEW_LINE
//NEW_LINE
//"Para ver la forma" NEW_LINE
//"en que el" NEW_LINE
//"protocolo DHCP" NEW_LINE
//"automaticamente" NEW_LINE
//"realizo la" NEW_LINE
//"configuracion de" NEW_LINE
//"la red revise la" NEW_LINE
//"pantalla de" NEW_LINE
//"informacion de la" NEW_LINE
//"red.",
//
////--------------------
////7.2.2 Ethernet Info Screen
////--------------------
//"Pantalla de" NEW_LINE
//"Informacion de" NEW_LINE
//"Ethernet" NEW_LINE
//NEW_LINE
//"La pantalla de" NEW_LINE
//"informacion de la red" NEW_LINE
//"muestra la" NEW_LINE
//"siguente informacion:" NEW_LINE
//NEW_LINE
//"Estado: Indica si" NEW_LINE
//"el controlador" NEW_LINE
//"esta conectado al" NEW_LINE
//"Internet atravez" NEW_LINE
//"del cable de Ethernet" NEW_LINE
//NEW_LINE
//"Direccion IP: Una" NEW_LINE
//"direccion de" NEW_LINE
//"Protocolo de" NEW_LINE
//"Intenet (IP" NEW_LINE
//"address) es un" NEW_LINE
//"numero asignado" NEW_LINE
//"al dispositivo" NEW_LINE
//"(tal como al" NEW_LINE
//"controlador)" NEW_LINE
//"que esta conectado" NEW_LINE
//"a un equipo de red y" NEW_LINE
//"utiliza el" NEW_LINE
//"protocolo de" NEW_LINE
//"internet para" NEW_LINE
//"comunicarse en la" NEW_LINE
//"red." NEW_LINE
//NEW_LINE
//"Mascara: La" NEW_LINE
//"mascara de subred" NEW_LINE
//"es usada con una" NEW_LINE
//"direccion IP para" NEW_LINE
//"indicar que el" NEW_LINE
//"trafico en la red" NEW_LINE
//"debe ser permitido" NEW_LINE
//"o negado." NEW_LINE
//NEW_LINE
//"Puerta de enlace" NEW_LINE
//"(Gateway): una" NEW_LINE
//"direccion IP" NEW_LINE
//"que permite el" NEW_LINE
//"trafico en la red" NEW_LINE
//"desde su" NEW_LINE
//"controlador para" NEW_LINE
//"pasar atravez de" NEW_LINE
//"una red mas grande." NEW_LINE
//NEW_LINE
//"DNS 1 (Sistema de" NEW_LINE
//"Nombres de Dominio):" NEW_LINE
//"Un nombramiento de" NEW_LINE
//"sitema para" NEW_LINE
//"dispositivos" NEW_LINE
//"tal como su" NEW_LINE
//"controlador, que" NEW_LINE
//"estan conectados" NEW_LINE
//"al Internet." NEW_LINE
//NEW_LINE
//"DNS 2: Un" NEW_LINE
//"adicional nombre" NEW_LINE
//"de dominio" NEW_LINE
//"que esta asignado" NEW_LINE
//"a su controlador" NEW_LINE
//NEW_LINE
//"MAC: Control de" NEW_LINE
//"acceso al medio" NEW_LINE
//"(MAC address)" NEW_LINE
//"es un" NEW_LINE
//"identificador" NEW_LINE
//"unico para su" NEW_LINE
//"controlador.",
//
////--------------------
////7.2.3 BaseManager Setup Screen
////--------------------
//"Pantalla de" NEW_LINE
//"Configuracion de" NEW_LINE
//"BaseManager" NEW_LINE
//NEW_LINE
//"Si desea oprear su" NEW_LINE
//"controlador" NEW_LINE
//"a control remoto" NEW_LINE
//"usando LiveView," NEW_LINE
//"necesita activar" NEW_LINE
//"la coneccion" NEW_LINE
//"del BaseManager" NEW_LINE
//NEW_LINE
//"Presione + o -" NEW_LINE
//"para cambiar" NEW_LINE
//"el valor en el" NEW_LINE
//"campo de Activacion" NEW_LINE
//"del BaseManager" NEW_LINE
//NEW_LINE
//"Si el BaseManager" NEW_LINE
//"esta actualmente" NEW_LINE
//"acitivado, una" NEW_LINE
//"marca de" NEW_LINE
//"verificacion" NEW_LINE
//"aparece en" NEW_LINE
//"el campo de" NEW_LINE
//"Activacion." NEW_LINE
//NEW_LINE
//"Si el BaseManager" NEW_LINE
//"esta actualmente" NEW_LINE
//"desactivado, no" NEW_LINE
//"aparece una marca de" NEW_LINE
//"verificacion en el" NEW_LINE
//"campo de Activacion." NEW_LINE
//NEW_LINE
//"Despues de que" NEW_LINE
//"haya activado la" NEW_LINE
//"coneccion" NEW_LINE
//"del BaseManager," NEW_LINE
//"para conectarse" NEW_LINE
//"utilice la opcion" NEW_LINE
//"de informacion del" NEW_LINE
//"BaseManager." NEW_LINE
//NEW_LINE
//"Si los tecnicos" NEW_LINE
//"del Baseline le" NEW_LINE
//"han dado una" NEW_LINE
//"direrente" NEW_LINE
//"direccion de IP" NEW_LINE
//"(IP Address)" NEW_LINE
//"para conectarse al" NEW_LINE
//"BasaManager," NEW_LINE
//"presione el " DOWN_ARROW " boton" NEW_LINE
//"para moverse al" NEW_LINE
//"campo de Uso de IP" NEW_LINE
//"Alternativo." NEW_LINE
//NEW_LINE
//"Presione + para" NEW_LINE
//"para poner una" NEW_LINE
//"marca de" NEW_LINE
//"Verificacion en el" NEW_LINE
//"campo." NEW_LINE
//NEW_LINE
//"Presione el " DOWN_ARROW " boton" NEW_LINE
//"para moverse al" NEW_LINE
//"campo de Direccion IP" NEW_LINE
//"(IP Address)" NEW_LINE
//NEW_LINE
//"Presione + o -" NEW_LINE
//"para cambiar los" NEW_LINE
//"valores en la" NEW_LINE
//"Direccion IP." NEW_LINE
//NEW_LINE
//"Para incrementar o" NEW_LINE
//"disminuir" NEW_LINE
//"rapidamente el" NEW_LINE
//"valor, mantega" NEW_LINE
//"presionado + o -.",
//
////--------------------
////7.2.4 BaseManager Info Screen
////--------------------
//"Pantalla de" NEW_LINE
//"Informacion del" NEW_LINE
//"BaseManager" NEW_LINE
//NEW_LINE
//"La pantalla de" NEW_LINE
//"Informacion del" NEW_LINE
//"BaseManager" NEW_LINE
//"muestra la" NEW_LINE
//"siguiente" NEW_LINE
//"informacion:" NEW_LINE
//NEW_LINE
//"Estado: indica si" NEW_LINE
//"el controlador" NEW_LINE
//"esta conectado" NEW_LINE
//"al BaseManager" NEW_LINE
//NEW_LINE
//"MAC: Control de" NEW_LINE
//"acceso al medio" NEW_LINE
//"(MAC address)" NEW_LINE
//"es un" NEW_LINE
//"identificador" NEW_LINE
//"unico para su" NEW_LINE
//"controlador." NEW_LINE
//NEW_LINE
//"Numero de Serie:" NEW_LINE
//"El numero de serie" NEW_LINE
//"de su controlador" NEW_LINE
//NEW_LINE
//"Servidor: Si su" NEW_LINE
//"controlador esta" NEW_LINE
//"conectado al" NEW_LINE
//"BaseManager, este" NEW_LINE
//"campo muestra el" NEW_LINE
//"servidor al que el" NEW_LINE
//"controlador esta" NEW_LINE
//"conectado." NEW_LINE
//NEW_LINE
//"Si su controlador" NEW_LINE
//"no esta conectado al" NEW_LINE
//"BaseManager," NEW_LINE
//"presione OK para" NEW_LINE
//"conectarse.",
//
////--------------------
////7.6.1 USB Backup Screen
////--------------------
//"Pantalla de Copia" NEW_LINE
//"de Seguridad" NEW_LINE
//"(Respaldo) USB" NEW_LINE
//NEW_LINE
//"Esta Funcion copia" NEW_LINE
//"(Respalda) toda la" NEW_LINE
//"informacion" NEW_LINE
//"de programacion" NEW_LINE
//"del controlador a" NEW_LINE
//"una memoria USB." NEW_LINE
//"Recomendamos que" NEW_LINE
//"cree una copia" NEW_LINE
//"de seguridad" NEW_LINE
//"(respaldo) siempre" NEW_LINE
//"que haga cambios" NEW_LINE
//"significantes a la" NEW_LINE
//"programacion" NEW_LINE
//"en el controlador." NEW_LINE
//NEW_LINE
//"Conecte la memoria" NEW_LINE
//"USB en el puerto" NEW_LINE
//"USB en el" NEW_LINE
//"controlador." NEW_LINE
//"Presione + para" NEW_LINE
//"copiar los datros" NEW_LINE
//"de programacion a" NEW_LINE
//"la memoria USB. El" NEW_LINE
//"controlador" NEW_LINE
//"escribe los datos" NEW_LINE
//"en un folder en" NEW_LINE
//la memoria USB" NEW_LINE
//"que es nombrado" NEW_LINE
//"con el numero de" NEW_LINE
//"serie des" NEW_LINE
//"controlador.",
//
////--------------------
////7.6.2 USB Restore Screen
////--------------------
//"Pantalla de" NEW_LINE
//"Restaurado USB" NEW_LINE
//NEW_LINE
//"Conecte la memoria" NEW_LINE
//"USB que tiene el" NEW_LINE
//"archivo" NEW_LINE
//"de la copia de" NEW_LINE
//"seguridad (Respaldo)" NEW_LINE
//"en el puerto USB" NEW_LINE
//"en el controlador." NEW_LINE
//NEW_LINE
//"Verifique que la" NEW_LINE
//"fecha y la hora de" NEW_LINE
//"la copia de" NEW_LINE
//"seguridad coincida" NEW_LINE
//"con la" NEW_LINE
//"programacion que" NEW_LINE
//"desea restaurar." NEW_LINE
//NEW_LINE
//"Presione + para" NEW_LINE
//"restaurar los" NEW_LINE
//"datos de" NEW_LINE
//"programacion de la" NEW_LINE
//"copia de seguridad" NEW_LINE
//"almacenada" NEW_LINE
//"en la memoria USB." NEW_LINE
//NEW_LINE
//"Cuando el" NEW_LINE
//"controlador ha" NEW_LINE
//"terminado" NEW_LINE
//"de restaurar la" NEW_LINE
//"programacion del" NEW_LINE
//"archivo de" NEW_LINE
//"la copia de" NEW_LINE
//"seguridad," NEW_LINE
//"desconecte la" NEW_LINE
//"memoria USB" NEW_LINE
//"del puerto del" NEW_LINE
//"controlador.",
//
////--------------------
////7.6.3 BaseManager Backup Screen
////--------------------
//"Pantalla de Copia" NEW_LINE
//"de Seguridad Local" NEW_LINE
//NEW_LINE
//"Presione + para" NEW_LINE
//"copiar los datos de" NEW_LINE
//"programacion a la" NEW_LINE
//"memoria del" NEW_LINE
//"controlador.",
//
////--------------------
////7.6.4 BaseManager Restore Screen
////--------------------
//"Pantalla de" NEW_LINE
//"Restauracion Local" NEW_LINE
//NEW_LINE
//"Verifique que la" NEW_LINE
//"fecha y la hora de" NEW_LINE
//"la copia de" NEW_LINE
//"seguridad local" NEW_LINE
//"coincida con la" NEW_LINE
//"programacion que" NEW_LINE
//"desea restaurar" NEW_LINE
//NEW_LINE
//"Presione + para" NEW_LINE
//"restaurar los" NEW_LINE
//"datos de" NEW_LINE
//"programacion de la" NEW_LINE
//"copia de seguridad" NEW_LINE
//"almacenada en la" NEW_LINE
//"memoria del" NEW_LINE
//"controlador.",
//
////--------------------
////7.7.1 USB Update Screen
////--------------------
//"Pantalla de" NEW_LINE
//"Actializacion USB" NEW_LINE
//NEW_LINE
//"La version actual" NEW_LINE
//"del Firmware en su" NEW_LINE
//"conrolador" NEW_LINE
//"se muestra en la" NEW_LINE
//"parte superior de" NEW_LINE
//"la pantalla" NEW_LINE
//NEW_LINE
//"Si ha descargado" NEW_LINE
//"una actualizacion" NEW_LINE
//"del Firmware" NEW_LINE
//"a una memoria USB," NEW_LINE
//"conecte la memoria" NEW_LINE
//"en el puerto de" NEW_LINE
//"USB en el" NEW_LINE
//"controlador, y" NEW_LINE
//"despues presione +" NEW_LINE
//"para iniciar la" NEW_LINE
//"actializacion del" NEW_LINE
//"Firmware." NEW_LINE
//NEW_LINE
//"Cuando se complete" NEW_LINE
//"la actualizacion," NEW_LINE
//"el controlador" NEW_LINE
//"reinicia y muestra" NEW_LINE
//"brevemente" NEW_LINE
//"la nueva version" NEW_LINE
//"de Firmware." NEW_LINE
//NEW_LINE
//"Desconecte la" NEW_LINE
//"memoria USB del" NEW_LINE
//"puerto del" NEW_LINE
//"controlador.",
//
////--------------------
////7.7.2 BaseManager Update Screen
////--------------------
//"Pantalla de" NEW_LINE
//"Actualizacion del" NEW_LINE
//"BaseManager" NEW_LINE
//NEW_LINE
//"La version actual" NEW_LINE
//"del Firmware en su" NEW_LINE
//"conrolador se" NEW_LINE
//"muestra en la" NEW_LINE
//"parte superior de" NEW_LINE
//"la pantalla." NEW_LINE
//NEW_LINE
//"Si su controlador" NEW_LINE
//"esta conectado" NEW_LINE
//"al BaseManager, el" NEW_LINE
//"controlador indica si" NEW_LINE
//"alguna" NEW_LINE
//"actualizacion del" NEW_LINE
//"BaseManager" NEW_LINE
//"firmware esta" NEW_LINE
//"disponible." NEW_LINE
//NEW_LINE
//"Si la" NEW_LINE
//"actualizacion esta" NEW_LINE
//"disponible," NEW_LINE
//"presione + para" NEW_LINE
//"descargarla al" NEW_LINE
//"controlador.",

//####################
//8.0 OFF Screen
//####################
//Apagar Pantalla" NEW_LINE
//NEW_LINE
//Cuando presiona el boton de Apagado (OFF)," NEW_LINE
//el controlador detiene todo el riego" NEW_LINE
//por un periodo de tiempo indefinido" NEW_LINE
//NEW_LINE
//Todos los ciclos de regado son detenidos." NEW_LINE
//Ningun ciclo de riego comenzara." NEW_LINE
//NEW_LINE
//Use este boton para" NEW_LINE
//cerrar por la temporada" NEW_LINE
//NO APAGUE" NEW_LINE
//la BaseStation." NEW_LINE

//--------------------
//6.1 Test Zone Screen
//--------------------
"",

//--------------------
//6.2 Test MV/Pump Screen
//--------------------
"",

//--------------------
//6.3 Test Moisture Sensor Screen
//--------------------
"",

//--------------------
//6.4 Test Flow Sensor Screen
//--------------------
"",

//--------------------
//6.5 Test Event Switch Screen
//--------------------
"",

//--------------------
//6.6 Test Temp. Sensor Screen
//--------------------
"",

//--------------------
//6.7 Test Two-Wire Screen
//--------------------
"",

//--------------------
//6.8 Test All Devices Screen
//--------------------
"",

//####################
//7.0 System Setup Menu
//####################
"",

//--------------------
//7.1 Time & Date Setup Screen
//--------------------
"",

//--------------------
//7.2 Network Setup Menu
//--------------------
"",

//--------------------
//7.3 Display Setup Screen
//--------------------
"",

//--------------------
//7.4 Security Setup Screen
//--------------------
"",

//--------------------
//7.5 Clear Programming Screen
//--------------------
"",

//--------------------
//7.6 Backup & Restore Menu
//--------------------
"",

//--------------------
//7.7 Firmware Update Menu
//--------------------
"",

//--------------------
//7.8 Export Data Screen
//--------------------
"",


//--------------------
//7.2.1 Ethernet Setup Screen
//--------------------
"",

//--------------------
//7.2.2 Ethernet Info Screen
//--------------------
"",

//--------------------
//7.2.3 Ethernet Info Screen
//--------------------
"",

//--------------------
//7.2.4 BaseManager Setup Screen
//--------------------
"",

//--------------------
//7.2.5 BaseManager Info Screen
//--------------------
"",

//--------------------
//7.6.1 USB Backup Screen
//--------------------
"",

//--------------------
//7.6.2 USB Restore Screen
//--------------------
"",

//--------------------
//7.6.3 BaseManager Backup Screen
//--------------------
"",

//--------------------
//7.6.4 BaseManager Restore Screen
//--------------------
"",

//--------------------
//7.7.1 USB Update Screen
//--------------------
"",

//--------------------
//7.7.2 BaseManager Update Screen
//--------------------
"",

//####################
//8.0 OFF screen
//####################
//"When you press the" NEW_LINE
//"OFF button, the" NEW_LINE
//"controller halts" NEW_LINE
//"all watering for" NEW_LINE
//"an indefinite period" NEW_LINE
//"of time." NEW_LINE
//NEW_LINE
//"All current watering" NEW_LINE
//"cycles are stopped." NEW_LINE
//"No watering cycles" NEW_LINE
//"will be started." NEW_LINE
//NEW_LINE
//"Use this button for" NEW_LINE
//"seasonal shutdown." NEW_LINE
//"DO NOT power down" NEW_LINE
//"the BaseStation.",

};
