﻿\header
PRUEBA
\footer
Presione NEXT/PREVIOUS para mas
ayuda, BACK para salir
\menu 0
\page
TEST SINGLE TWO-WIRE DEVICE (PRUEBA
DE UN SOLO DISPOSITIVO DE DOS- 
CABLES): Pruebe un dispositivo 
especifico.

CHECK TWO-WIRE ADDRESSES (CHECAR 
DIRECCIONES DE DOS-CABLES): Pruebe 
cada dispositivo de Dos-cables y
solucione cualquier problema de 
direccion.
\page
TEST ALL TWO-WIRE DEVICES (PROBAR 
TODOS LOS DISPOSITIVOS DE DOS-CABLES):
Pruebe todos los dispositivos de
dos-cables en el controlador. Ver los
resultados completos en la posicion de
Reports (Reportes) del marcador.

TWO-WIRE BLINK TEST (PRUEBA DE 
PARPADEO DE DOS-CABLES): Encuentra 
connecciones eléctricas malas, cables 
rotos o fallas de dispositivos.
\menu 1
\page
Prueba de un Solo Dispositivo de 
Dos-Cables

   1. En la columna Device to Test 
      (Dispositivo a Probar), presione
      el botón + o -para seleccionar 
      el numero de serie del 
      dispositivo que desea probar.

   2. Presione en botón Enter para
      probar el dispositivo.
\page
   3. El dispositivo se activara por
      menos de un segundo para medir 
      el voltaje y la corriente
      atravez del dispositivo. Esta 
      prueba arrojara información
      relacionada al tipo de 
      dispositivo que fue probado. 

   4. Cuando haya terminado de ver los
      resultados, cambie el marcador a
      la posición de RUN.
\menu 2
\page
Checar Direcciones de Dos-Cables

Presione el botón Enter para iniciar
la prueba. Mientras la prueba esta 
en progreso, los estatuses y los 
resultados se muestran en la pantalla.
\menu 3
\page
Probar Todos los Dispositivos de Dos-
Cables

   1. Presione el botón Enter para
      iniciar la prueba.

   2. Cuando la prueba esta terminada,
      cambie el marcadora la posición 
      de Reports (Reportes).
\page

   3. Asegúrese que Test All (Probar
      Todos) se muestra en campo de 
      Select Report (Reporte 
      Seleccionado).

   4. Para desplazarse atravez del 
      reporte, presione el botón 
      Next para resaltar el campo 
      Scroll (Desplazar) y después 
      presione el botón +.
\menu 4
\page
Prueba de Parpadeo de Dos-Cables

   1. En el campo de Blink Interval 
      (Intervalo de Parpadeo), 
      presione el botón + o - para 
      indicar el numero de segundos
      entre parpadeos. 

   2. Presione el botón Enter para 
      iniciar la prueba.
\page
      Mientras la prueba de parpadeo
      esta en progreso, el campo de 
      Blink Interval (Intervalo de 
      Parpadeo) muestra "Running"
      "Ejecutándose" y la casilla de
      Status (Estatus) muestra los 
      resultados.

   3. Para detener la prueba, presione 
      el botón Back.
\page
-----End of Help-----