﻿\header
Las Operaciones Manuales
\footer
Presione NEXT/PREVIOUS para mas
ayuda, BACK para salir
\menu 0
\page
RUN ZONE(S) FOR FIXED TIME (EJECUTAR 
LA ZONA(s) POR UN PERIODO DE TIEMPO 
FIJO): Use esta opción para ejecutar 
manualmente una o mas zonas por un 
tiempo especifico.

RUN PUMP OR MV FOR FIXED TIME 
(EJECUTAR LA BOMBA O MV POR UN TIEMPO
FIJO): Use esta opción para obtener 
agua en un grifo que esta unido a una 
master valve.
\page
RUN PROGRAM(S) FOR FIXED TIME 
(EJECUTAR PROGRAMA(S) POR UN TIEMPO 
FIJO): Use esta opción para ejecutar
manualmente uno o mas programas por un
tiempo especifico.
\page
START OR STOP PROGRAM(S) (INICIAR O 
DETENER PROGRAMA(S)): Use esta opción 
para iniciar un programa y hacer que 
las zonas rieguen de acuerdo al 
programa incluyendo  tiempos de 
ejecución, tiempos de remojo y las 
condiciones del sensor.
\page
START OR STOP PROGRAM ZONE(S) (INICIAR
O DETENER PROGRAMA DE ZONA(S)): Use 
esta opción para ejecutar manualmente 
una o mas zonas de un programa por sus
tiempos de ejecución programados.
\menu 1
\page
Ejecutar Zona(s) por un Tiempo Fijo.

   1. En la columna de Zone (Zona), 
      presione el botón + para 
      resaltar la zona que desee 
      ejecutar.

   2. Presione el botón Next para
      moverse al a la columna de 
      Run Time (Tiempo de Ejecución) y
      después presione el botón + o -
\page
      para establecer la cantidad de 
      tiempo para la ejecución manual.

   3. Si desea agregar otra zona a la 
      ejecución manual, presione el 
      botón Previous para regresar a
      la columna de Zone (Zona) y 
      después repita pasos los 1-2 
      para establecer el tiempo para 
      la siguiente zona.
\page
   4. Presione el botón Next para
      moverse al campo de Concurrent 
      Zones (Zonas Simultaneas). Si 
      desea que mas de una zona se 
      ejecute a un tiempo durante la 
      ejecución manual, presione el 
      botón + para cambiar el numero 
      en este campo.

   5. Cuando haya terminado de 
      realizar los cambios, cambie el 
      marcador a la posición RUN.
\page
      Cuando la ejecución manual 
      empieza, la pantalla principal
      muestra los detalles.

   6. Si desea detener la ejecución
      manual mientras esta activa, 
      presione el botón Back.

Nota: Cuando la ejecución manual esta 
completa, el controlador cambia los 
ajustes de Run Zone(s) for Fixed time 
(Ejecución de Zona(s) por un Tiempo 
Fijo) a Off (Apagado).
\menu 2
\page
Ejecutar Bomba o MV por un Tiempo Fijo

   1. En la columna de Pump/MV 
      (Bomba/MV), presione el botón + 
      para seleccionar la bomba o 
      master valve que desea ejecutar.

   2. Presione el botón Next para 
      moverse a la columna de Run Time 
      (Tiempo de Ejecución) y después 
      presione el botón + o - para 
\page
      establecer la cantidad de tiempo
      para la ejecución manual.

   3. Si desea agregar otra bomba o 
      master valve a la ejecución 
      manual, presione el botón 
      Previous para regresar a la
      columna de Pump/MV (Bomba/MV) y
      después repita los pasos 1-2 
      para establecer el tiempo para 
      la siguiente bomba o master 
      valve.
\page  
   4. Presione el botón Next para 
      moverse al campo de Concurrent 
      Zones (Zonas Simultaneas). Si 
      desea que mas de una bomba o 
      master valve se ejecute a un 
      tiempo durante la ejecución 
      manual, presione el botón + 
      para cambiar el numero en este 
      campo.
\page
   5. Cuando haya terminado de realizar
      los cambios, cambie el marcador a
      la posición RUN. Cuando la 
      ejecución manual empieza, la 
      pantalla principal muestra los 
      detalles.
\page
   6. Si desea detener la ejecución 
      manual mientras esta activa, 
      presione el botón Back.

Nota: Cuando la ejecución manual esta 
completa, el controlador cambia los 
ajustes de Run Pump or Master Valve 
for Fixed Time (Ejecución de Bomba o 
MV por un Tiempo Fijo) a Off (Apagado).
\menu 3
\page
Ejecutar Programa(s) por un Tiempo 
Fijo

   1. En la columna de Program 
      (Programa), presione el botón +
      para  seleccionar el programa 
      que desea ejecutar.

   2. Presione el botón Next para 
      moverse a la columna de Zone 
      Time (Tiempo de Zona) y después
\page
      presione el botón + o - para 
      establecer la cantidad de 
      tiempo para la ejecución manual.

   3. Si desea agregar otro programa
      a la ejecución manual, presione 
      el botón Previous para regresar
      a la columna de Program (Programa) 
      y después repita los pasos 1-2
      para establecer el tiempo para 
      la siguiente zona.
\page
   4. Presione el botón Next para 
      moverse al campo de Concurrent 
      Zones (Zonas Simultaneas). Si 
      desea que mas de una zona se 
      ejecute a un tiempo durante la 
      ejecución manual, presione el 
      botón + para cambiar el numero
      en este campo.
\page
   5. Cuando haya terminado de 
      realizar los cambios, cambie el
      marcador a la posición de RUN.
      Cuando la ejecución manual 
      empieza, la pantalla principal 
      muestra los detalles.
\page
   6. Si desea detener la ejecución 
      manual mientras esta activa, 
      presione el botón Back.

Nota: Cuando la ejecución manual esta
completa, el controlador cambia los 
ajustes de Run Program(s) for Fixed 
Time (Ejecutar Programa(s) por un 
Tiempo Fijo) a Off (Apagado).
\menu 4
\page
Iniciar o Detener Programas

Iniciar Manualmente un Programa

Cuando usa esta opción para iniciar
un programa, las zonas riegan de 
acuerdo al programa incluyendo tiempos
de ejecución, tiempos de remojo y las
condiciones del sensor.
\page
   1. En la columna de Program/Status
      (Programa/Estatus), presione el 
      botón + para resaltar un 
      programa inactivo o pausado que
      desea iniciar.

   2. Presione el botón Next para 
      moverse a la columna Start/Stop
      (Iniciar/Detener). El campo en 
      esta columna muestra Start 
      (Iniciar).
\page
   3. Presione el botón +. El campo 
      en la columna Start/Stop 
      (Iniciar/Detener) muestra 
      Stop (Detener) y el estatus 
      del programa cambia a Watering 
      (Regando).

   4. Cambie el marcador a la 
      posición RUN.
\page
Detener Manualmente un Programa Activo.

   1. En la columna de Program/Status
      (Programa/Estatus), presione el 
      botón + para resaltar un 
      programa de riego que desea 
      detener.

   2. Presione el botón Next para 
      moverse a la columna Start/Stop
      (Iniciar/Detener). El campo en 
      esta columna muestra Stop 
      (Detener).
\page
   3. Presione el botón +. El campo 
      en la columna Start/Stop 
      (Iniciar/Detener) muestra Start 
      (Iniciar) y el estatus del 
      programa cambia a Idle 
      (Inactivo).

   4. Cambie el marcador a la posición
      RUN.
\menu 5
\page
Iniciar o Detener Programa de Zona(s)

   1. En la columna de Program 
      (Programa), presione el botón +
      para resaltar el programa que 
      tiene las zonas que desea 
      ejecutar. 

   2. Presione el botón Next para 
      moverse a la columna de 
      Zone/Status (Zona/Estatus).
\page
   3. Presione el botón + para 
      resaltar la zona que desee 
      ejecutar.

   4. Presione el botón Next para
      moverse a la columna Start/Stop
      (Iniciar/Detener). El campo en 
      esta columna muestra Start 
      (Iniciar).
\page
   5. Presione el botón +. El campo en
      la columna Start/Stop 
      (Iniciar/Detener) muestra Stop 
      (Detener) y el estatus del
      programa cambia a Watering 
      (Regando).

   6. Si desea manualmente ejecutar 
      otra zona en el programa, 
      presione el boton Previous
\page
      para regresar a la columna de 
      Zone/Status (Zona/Estatus).
      Repita los paso 4-6 para iniciar
      otra zona. 

   7. Cambie el marcador a la posicion
      RUN.
\page
-----End of Help-----
