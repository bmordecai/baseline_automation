﻿\header
Water and Flow Management
\footer
Press NEXT/PREV for more help, BACK to exit  
\menu0
\page
ASSIGN WATER SOURCES TO MAINLINES: 
Assign a POC to a specific mainline 
number

ASSIGN PROGRAMS TO MAINLINES: All 
programs are automatically assigned 
to mainline #1, but you can change the
settings to match your irrigation 
system 
\page
SETUP MAINLINE OPERATING LIMITS:
Configure settings for the mainline 
in order to better manage flow in your
irrigation system

ZONE LEARN FLOW OPERATIONS: Schedule 
the controller to learn flow for a 
specific zone
\page
PROGRAM LEARN FLOW OPERATIONS: 
Schedule the controller to learn flow
for all zones associated with a 
program
\menu 1
\page
Assign Water Sources to Mainlines

The BaseStation 3200 supports 8 points
of connection (water sources) and 4
mainlines. By default, the mainlines
are numbered in the controller from
1 - 4 regardless of whether your 
system has more than one. All water 
sources are automatically assigned to
\page
mainline #1, but you can change the 
settings to match your irrigation 
system. 

   1. In the Point of Connection 
      column, press the + or -
      button to highlight the POC 
      that you want to assign to a
      mainline.
\page
   2. Press the Next button to move to
      the Mainline column, and then 
      press the + or - button to 
      highlight the mainline that you
      want to assign to that POC.

      Note: If your controller is 
      connected to a FlowStation,
      you assign a POC to the 
      FlowStation by selecting
      FlowStn in the Mainline 
\page
      column. Refer to the 
      FlowStation User Guide for
      specific instructions.

   3. Press the Previous button to 
      return to the Point of 
      Connection column.

   4. Repeat these steps until you
      have assigned all POCs to the
      corresponding mainlines.
\menu 2
\page
Assign Programs to Mainlines

All programs are automatically 
assigned to mainline #1, but you 
can change the settings to match 
your irrigation system.

   1. In the Program column, press the
      + or - button to highlight the 
      program that you want to assign
      to a mainline.
\page
   2. Press the Next button to move to
      the Mainline column, and then 
      press the + or - button to 
      highlight the mainline that you 
      want to assign to that program.

      Note: If your controller is 
      connected to a FlowStation,
      you assign a program to the 
      FlowStation by selecting
      FlowStn in the Mainline 
\page
      column. Refer to the 
      FlowStation User Guide for
      specific instructions.

   3. Press the Previous button to 
      return to the Program column.

   4. Repeat these steps until you have
      assigned all programs to the 
      corresponding mainlines.
\menu 3
\page
Set Up Mainline Operating Limits

Refer to the User Manual for a 
description of the limits that you 
can configure in this screen.

   1. In the Mainline field, press the
      + or - button to select the 
      number of the mainline that you
      want to set up.
\page   
   2. Press the Next button to go to
      the Design Flow (GPM) field, 
      and then press the + button to
      enter the amount of flow in 
      gallons per minute (GPM) that
      is allowed through the mainline
      for the irrigation system.

      Note: Press and hold the + 
      button or the - button to 
\page
      rapidly increase or decrease
      the number in the field.

   3. Press the Next button to go to 
      the Pipe Fill/Stabilize field, 
      and then press the + button to 
      enter the amount of time in 
      minutes that it takes to fill
      the pipe and achieve a steady 
      flow rate after a valve change.
\page
   4. Press the Next button to go to 
      the Concurrent Zones by Flow 
      field. If you want the 
      controller to use the mainline
      design flow to determine how 
      many zones can run concurrently,
      press the + button to enter YES. 

   5. Press the Next button to go to 
      the High Flow Variance (Alarm) 
      field. If you want to set an 
\page
      alarm for a high flow variance
      condition, press the + button 
      to enter a percentage in the 
      field. 

   6. Press the Next button to go to 
      the High Flow Shutdown field. If 
      you want the system to shut down
      when a high flow is detected, 
      press the + button to enter 
      Y (yes).
\page
   7. Press the Next button to go to 
      the Low Flow Variance (Alarm) 
      field. If you want to set an 
      alarm for a low flow variance
      condition, press the + button 
      to enter a percentage in the 
      field.

   8. Press the Next button to go to 
      the Low Flow Shutdown field. If
      you want the system to shut down
\page
      when a low flow is detected, 
      press the + button to enter 
      Y (yes).

   9. When you have finished making 
      changes, turn the dial to the 
      RUN position.
\menu 4
\page
Zone Learn Flow Operations

Schedule the controller to learn flow 
for a specific zone

   1. In the Zone field, press the 
      + button to select the zone that
      you want to schedule a learn 
      flow cycle for. 
\page
   2. Press the Next button to go to 
      the Learn Flow Cycle field. If
      the system already ran a learn
      flow cycle for this zone, the
      status of that cycle (complete
      or failed) displays in the 
      field. To schedule a learn flow
      cycle, press the + button to 
      advance the delay time by 5 
      minutes.
\page
   3. When you have finished 
      scheduling the learn flow
      cycle, turn the dial to the 
      RUN position.

IMPORTANT NOTE! The learn flow cycle 
does not start until you turn the dial
to the RUN position.
\menu 5
\page
Program Learn Flow Operations

Schedule the controller to learn flow
for all zones associated with a 
program

   1. In the Program column, press 
      the + button to move to the 
      program that you want to 
      schedule a learn flow cycle for.
\page
   2. Press the Next button to move to
      the Status column, and then 
      press the + button to toggle 
      the setting between Pending and
      Not Yet.

   3. Press the Next button to move to
      the Date column, and then press 
      the + button set the learn time 
      in 5 minute increments in the 
      future, up to about 24 hours. 
\page
      Note: At the 24 hour point the
      status will return to Not Yet.

   4. When you have finished setting 
      up the learn flow cycle, turn 
      the dial to the RUN position.
\page
-----End of Help-----