﻿\header
RUN Screen
\footer
Press NEXT/PREV for more help, BACK to exit  
\page
When the controller dial is in the RUN
position, press the Next or Previous 
button on the main screen to display 
the status screen for all POCs, zones,
and programs.

The cells in the POCs grid show the 
real-time flow meter value and status
for each POC that you have configured
in the controller.
\page
The colored cells in the Zones grid 
show the real-time status of all the
zones you have configured in the 
controller. 

The colored cells in the Progs grid 
show the real-time status of all the
programs you have configured in the 
controller.
\page
Refer to the User Manual for an 
explanation of the status colors.

If you leave the all zones status 
screen visible, it will continue 
to display When the controller 
dial is in the RUN position. When 
the controller is reset, the Zone 
Status Report redisplays.
\page
-----End of Help-----