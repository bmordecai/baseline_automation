Zone=1	ON	Timed	14	A	3	55	A004714	Timed		[0.0,0.29]	0
Zone=2	ON	Timed	60	A	7	56	A004715	Timed		[0.0,0.26]	0
Zone=3	ON	Master	60	B	10	48	A004662	Lower Limit	SB00309	[0.0,0.26]	0
Zone=4	ON	Linked=3	51	B	8	50	A004663	Linked		[0.0,0.29]	0
Zone=5	ON	Linked=3	30	B	5	53	A004706	Linked		[0.0,0.27]	0
Zone=6	ON	Timed	45	D	9	51	A004707	Timed		[0.0,0.25]	0
Zone=7	ON	Timed	30	C	10	33	00D4548	Timed		[0.0,0.13]	0
Prog=A	ON	Max Valves=1/1	Starts=00:15,30:00,30:00,30:00,30:00,30:00,30:00,30:00	Intervals=7,6,6,6,4,3,3,3,3,2,2,2,2,2,2,2,3,3,3,4,6,6,6,7	Skip=FRI	Window=Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y
Prog=B	ON	Max Valves=1/1	Starts=00:30,30:00,30:00,30:00,30:00,30:00,30:00,30:00	Week Days=SMTWT-S	Window=Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y
Prog=C	ON	Max Valves=1/1	Starts=07:00,12:00,17:00,30:00,30:00,30:00,30:00,30:00	Week Days=SMTWT-S	Window=Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y
Prog=D	ON	Max Valves=1/1	Starts=02:00,30:00,30:00,30:00,30:00,30:00,30:00,30:00	Week Days=S-T-T-S	Window=Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y
Prog=E	OFF
Prog=F	OFF
Prog=G	OFF
Prog=H	OFF
Prog=I	OFF
Prog=J	OFF
biSensor=SB00308/221	master=none	[-]
biSensor=SB00309/222	master=3	strategy=Lower Limit	limit=24.0 %	[-]
