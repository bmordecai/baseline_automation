Zone=1	ON	Timed	14	A	3	55	A004714	Timed		[0.0,0.28]	8
Zone=2	ON	Timed	60	A	7	56	A004715	Timed		[0.0,0.28]	6
Zone=3	ON	Master	80	B	12	48	A004662	Lower Limit	SB00309	[0.0,0.26]	3
Zone=4	ON	Linked=3	68	B	10	50	A004663	Linked		[0.0,0.29]	2
Zone=5	ON	Linked=3	60	B	9	51	A004706	Linked		[0.0,0.26]	2
Zone=6	ON	Timed	90	D	18	72	A004707	Timed		[0.0,0.26]	2
Zone=7	ON	Master	19	C	10	33	00D4548	Lower Limit	SB00308	[0.0,0.14]	1
Prog=A	ON	Max Valves=1/3	Starts=00:15,30:00,30:00,30:00,30:00,30:00,30:00,30:00	Intervals=7,6,6,6,4,3,3,3,3,2,2,2,2,2,2,2,3,3,3,4,6,6,6,7	Skip=SAT	Window=Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y
Prog=B	ON	Max Valves=3/3	Starts=00:30,30:00,30:00,30:00,30:00,30:00,30:00,30:00	Week Days=SMTWTF-	Window=Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y
Prog=C	ON	Max Valves=3/3	Starts=07:00,12:00,17:00,30:00,30:00,30:00,30:00,30:00	Week Days=SMTWTFS	Window=Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y
Prog=D	ON	Max Valves=1/3	Starts=00:15,30:00,30:00,30:00,30:00,30:00,30:00,30:00	Intervals=7,6,6,6,4,3,3,3,3,2,2,2,2,2,2,2,3,3,3,4,6,6,6,7	Skip=SAT	Window=Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y,Y
Prog=E	OFF
Prog=F	OFF
Prog=G	OFF
Prog=H	OFF
Prog=I	OFF
Prog=J	OFF
biSensor=SB00308/221	master=7	strategy=Lower Limit	limit=29.0 %	[3.1]
biSensor=SB00309/222	master=3	strategy=Lower Limit	limit=27.0 %	[3.2]
