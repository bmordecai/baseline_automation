﻿\header
Manual Operations
\footer
Press NEXT/PREV for more help, BACK to exit
\menu 0
\page
RUN ZONE(S) FOR FIXED TIME: Use this
option to manually run one or more 
zones for a specified time.

RUN PUMP OR MV FOR FIXED TIME: Use 
this option to get water to a hose bib
that is attached to a master valve.

RUN PROGRAM(S) FOR FIXED TIME: Use 
this option to manually run one or 
more programs for a specified time.

START OR STOP PROGRAM(S): Use this 
option to start a program and have the
zones water according to the program 
including run times, soak times, and 
sensor conditions.

START OR STOP PROGRAM ZONE(S): Use 
this option to manually run one or 
more zones of a program for their
programmed run times.
\menu 1
\page
Run Zone(s) for Fixed Time

\menu 2
\page
Run Pump or MV for Fixed Time

\menu 3
\page
Run Program(s) for Fixed Time

\menu 4
\page
Start or Stop Program(s)

\menu 5
\page
Start or Stop Program Zone(s)

\page
-----End of Help-----