Components:
BL-1000
4-valve decoder: Q005751 on 2-wire
2-valve decoder: E041261 on 2-wire
2-valve decoder: E001101 on 2-wire

Note that this configuration was done on a controller that did not have two 4-valve decoders, so one 4-valve and 2 2-valve decoders were used instead.