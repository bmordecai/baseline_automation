Components:

�	1000-C controller
�	5224 board
�	2x 1-valve decoder (D001068, D003987)
�	2x 2-valve decoder (E011541, E041261)
�	2x 4-valve decoder (Q011801, Q016381)
�	2x moisture sensors (SB01183, SB01429)
�	Air temperature sensor (AT01098)
�	2x pause decoders (RP00218, RP00223)

Components used were attached to the test board made by Devan.
Note that when configuring this test, Moisture Sensor 1 (SB01183) was attached to the 2-wire path and not to the VB000001 terminal.