Testing matricies can be found in the excel files in this directory.

Testing procedures may seem organized strangely at first, but it is really quite simple.

First, test numbers are selected (i.e. 1-20). The name will remain like this if the test is cross-platform reusable (i.e. the procedures do not change between, say, desktop and tablet).

If the test is only for a specific platform, however, an integer number is appended to the test number to designate the platform (1 for desktop, 2 for tablet, 3 for phone). For example, test 5.1 would be test 5 specifically for the desktop.

In addition, if the test is only for a specific controller type, a letter is appended to the name of the test (a for BL-1000, b for BL-3200). For example, test 15.b would be a cross-platform test designed specifically for the BL-3200.

These can be combined as needed. For example, test 20.3a would be a test only for phones and the BL-1000.

In this way, test procedures can be reused.

The folder structure for the tests is as follows:

Cross-platform tests
|
Desktop - Tablet - Phone cross-controller tests & 1000 - 3200 cross-platform tests

If a test is controller- and platform-specific, the directory will always follow PLATFORM -> CONTROLLER and not CONTROLLER -> PLATFORM.