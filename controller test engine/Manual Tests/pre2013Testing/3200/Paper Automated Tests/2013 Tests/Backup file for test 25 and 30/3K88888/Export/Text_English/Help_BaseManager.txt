﻿\header
BaseManager Setup
\footer
Press NEXT/PREV for more help, BACK to exit  
\menu 0
\page
BASEMANAGER SERVER SETUP: Configure 
your controller to connect to a 
BaseManager server

ETHERNET SETUP: Configure your 
controller to connect to a network 
with the built-in Ethernet connection
\menu 1
\page
BaseManager Server Setup

BaseManager is Baseline’s central 
control solution, which gives you 
remote access to your BaseStation 
3200. In order to use BaseManager,
your controller needs to be connected 
to a network with a supported 
communication module such as built-in
Ethernet, Wi-Fi, cell modem, or 
Ethernet radio. 
\page
You also need access to a BaseManager
server--either on Baseline’s server 
or on your organization’s server.

If you are going to use BaseManager on
Baseline’s server, the BaseStation 
3200 is preconfigured to connect to 
that server. After you connect your
controller to the network using a
\page
supported communication module, you
can check the status of your 
connection on the RUN screen, and 
then review the settings on the 
BaseManager Server screen. 

Refer to the User Manual for specific
instructions.
\menu 2
\page
Ethernet Setup

After you have plugged an Ethernet 
cable into the Ethernet port on the
controller and plugged the other end
of the cable into a live Ethernet jack,
the controller will attempt to connect 
to the network with the Dynamic Host 
Configuration Protocol (DHCP).

Refer to the User Manual.

\menu 3
\page
AT&T Cellular Modem Setup

If your BaseStation 3200 is running
firmware version 12.4 or higher and 
you have a cell modem installed and 
connected to the controller, the
AT&T Cellular Modem Setup option
shows the status of the connection.

Refer to the Baseline Cell Modem
Installation and User Guide.
\page
-----End of Help-----