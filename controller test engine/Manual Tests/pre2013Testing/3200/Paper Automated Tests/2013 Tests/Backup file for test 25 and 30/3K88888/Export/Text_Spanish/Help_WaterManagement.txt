﻿\header
Manejo del Agua y Flujo
\footer
Presione NEXT/PREVIOUS para mas
ayuda, BACK para salir
\menu 0
\page
ASSIGN WATER SOURCES TO MAINLINES 
(ASIGNACION FUENTES DE AGUA PARA 
LINEAS PRINCIPALES): Asigne un POC
al numero especifico de la línea 
principal.
\page
ASSIGN PROGRAMS TO MAINLINES 
(ASIGNACION DE PROGRAMAS PARA LINEAS 
PRINCIPALES): Todos los programas 
están asignados automáticamente a la 
línea principal #1, pero puede cambiar
los ajustes para que coincidan con su 
sistema de riego.
\page
SETUP MAINLINE OPERATING LIMITS 
(CONFIGURACION DE LIMITES DE 
FUNCIONAMIENTO DE LINEAS PRINCIPALES):
Configure las opciones de la línea 
principal con el fin de mejorar el 
manejo del flujo de su sistema de 
riego.
\page
ZONE LEARN FLOW OPERATIONS 
(OPERACIONES DE ZONA DE APRENDIZAJE 
DE FLUJO): Programe el controlador
para prender el flujo para una 
zona especifica.

PROGRAM LEARN FLOW OPERATIONS 
(OPERACIONES DE PROGRAMACION DE 
APRENDIZAJE DE FLUJO): Programe el 
controlador para aprender el flujo 
para todas las zonas asociadas con el
programa.
\menu 1
\page
Asignar Fuentes de Agua a Líneas 
Principales

La BaseStation 3200 acepta 8 puntos 
de conneccion (fuentes de agua) y 4 
líneas principales. De forma 
predeterminada, las líneas principales 
están enumeradas del 1 al 4 
independientemente si su sistema tiene
mas de una. Todas las fuentes de agua 
están asignadas automáticamente a la 
\page
línea principal #1, pero puede cambiar 
los ajustes para que coincidan con su 
sistema de riego.

   1. En la columna Point of 
      Connection (Punto de Conneccion), 
      presione el botón + o - para 
      resaltar el POC que desee 
      asignar a la línea principal.
\page
   2. Presione el botón Next para 
      moverse a la columna Mainline 
      (Línea Principal) y después 
      presione el botón + o - para 
      resaltar la línea principal que 
      desea asignar a ese POC (punto 
      de conneccion).

   3. Presione el botón Previous para
      regresar a la columna Point of
      Connection (Punto de Conneccion).
\page
   4. Repita esos pasos hasta que haya
      asignado todos los POCs a las 
      líneas principales 
      correspondientes.
\menu 2
\page
Asignar Programas a las Líneas 
Principales

Todos los programas están asignados 
automáticamente a la línea principal 
#1, pero puede cambiar los ajustes
para que coincidan con su sistema de 
riego.
\page
   1. En la columna Program (Programa),
      presione el botón + o - para 
      resaltar el programa que desee 
      asignar a la línea principal.

   2. Presione el botón Next para
      moverse a la columna Mainline 
      (Línea Principal) y después 
      presione el botón + o - para 
      resaltar la línea principal que
      desea asignar a ese programa.
\page
   3. Presione el botón Previous para
      regresar a la columna Program 
      (Programa).

   4. Repita esos pasos hasta que haya
      asignado todos los programas a 
      las líneas principales 
      correspondientes.
\menu 3
\page
Configurar Limites de funcionamiento 
de Líneas Principales

Consulte el Manual de Usuario para una 
descripción de los limites que puede 
configurar en esta pantalla.

   1. En el campo Mainline (Línea 
      Principal), presione el botón +
      o - para seleccionar el numero 
      de la line principal que desea 
      configurar.
\page
   2. Presione el botón Next para 
      ir al campo Design Flow (Diseño 
      de Flujo (GPM)), y presione el 
      botón + para introducir la 
      cantidad de flujo en galones por 
      minuto (GPM) que es permitido a 
      travez de la línea principal del
      sistema de riego.
\page
      Nota: Mantenga presionado el 
      botón + o - para rápidamente 
      incrementar o disminuir el 
      numero en el campo.

   3. Presione el botón Next para ir 
      al campo Pipe Fill/Stabilize 
      (Llenado/Estabilizado de 
      Tubería), y después presione el 
      botón + para introducir la 
      cantidad de tiempo en minutos
\page
      que toma el llenado de la 
      tubería y alcanzar un velocidad 
      estable de flujo después de un 
      cambio de válvula.

   4. Presione el botón Next para ir 
      al campo Concurrent Zones by 
      Flow (Zonas Simultaneas por 
      Flujo). Si desea que el 
      controlador utilice el diseño 
      de flujo de la línea principal
\page
      para que determine cuantas zonas
      puede ejecutar simultáneamente,
      presione el botón + para 
      introducir YES (SI).

   5. Presione el botón Next para
      ir al campo High Flow Variance 
      (Alarm) (Varianza de Alto Flujo
      (Alarma). Si desea configurar 
      una alarma por una varianza alta
\page
      de flujo, presione el botón + 
      para introducir el porcentaje 
      en el campo.

   6. Presione el botón Next para ir
      al campo High Flow Shutdown 
      (Apagar por Alto Flujo). Si 
      desea que el sistema se apague
      cuando un flujo alto es 
      detectado, presione el botón 
      + para introducir Y (SI)
\page
   7. Presione el botón Next para ir 
      al campo Low Flow Variance 
      (Alarm) (Varianza de Bajo Flujo
      (Alarma) ). Si desea configurar 
      una alarma por una varianza baja
      de flujo, presione el botón + 
      para introducir el porcentaje 
      en el campo.
\page
   8. Presione el botón Next para ir 
      al campo Low Flow Shutdown 
      (Apagar por Bajo Flujo). Si 
      desea que el sistema se apague 
      cuando un flujo bajo es 
      detectado, presione el botón 
      + para introducir Y (SI).

   9. Cuando haya terminado de hacer
      los cambios, cambie el marcador
      a la posición RUN.
\menu 4
\page
Operaciones de Zona de Aprendizaje de 
Flujo

Programe el controlador para aprender
el flujo para una zona especifica.

   1. En el campo Zone (Zona), 
      presióne el botón + para 
      seleccionar la zona en la cual
      desea programar un ciclo de 
      aprendizaje de flujo.
\page
   2. Presione el botón Next para ir 
      al campo Learn Flow Cycle (Ciclo
      de Aprendizaje de Zona). Si el 
      sistema ya ejecuta un ciclo de 
      flujo en esa zona, el estatus de
      ese ciclo (completo o fallido) 
      aparece en el campo. Para 
      programar un ciclo de aprendizaje
      de flujo, presione el botón + 
      para adelantar el retraso de 
      tiempo por 5 minutos.
\page
   3. Cuando haya terminado de 
      programar el ciclo de 
      aprendizaje de flujo, cambie el 
      marcador a la posición RUN.

NOTA IMPORTANTE! El ciclo de 
aprendizaje de zona no inicia hasta 
que el  marcador sea cambiado a 
la posición RUN.
\menu 5
\page
Operaciones de Programación de 
Aprendizaje de Flujo

Programe el controlador para que 
aprenda el flujo de todas las zonas 
asociadas con el programa.

   1. En la columna Program (Programa),
      presione el botón + para moverse 
      al programa al cual desea 
      programar en la cual desea 
\page
      programar el ciclo de 
      aprendizaje de flujo.

   2. Presione el botón Next para 
      moverse a la columna Status 
      (Estatus), y después presione
      el botón + para cambiar el 
      ajuste entre Pending 
      (Pendiente) y Not Yet 
      (Aun no).
\page
   3. Presione el botón Next para 
      moverse a la columna Date 
      (Fecha), y presione el botón +
      establezca el tiempo de 
      aprendizaje en incrementos de 
      5 minutos en el futuro, hasta 
      aproximadamente 24 horas.

      Nota: Al punto de 24 horas el 
      estatus regresara a Not Yet 
      (Aun no).
\page
   4. Cuando haya terminado de 
      establecer el ciclo de 
      aprendizaje de flujo, cambie
      el marcador a la posición RUN.
\page
-----End of Help-----