\header
Configuraci�n de Programa
\footer
Presione NEXT/PREVIOUS para mas
ayuda, BACK para salir
\menu 0  
\page
PROGRAM START TIMES SETUP 
(CONFIGURACION DE TIEMPOS DE INICIO DE
PROGRAMA): Instale hasta 8 tiempos de 
inicio para un programa.

PROGRAM START DAYS SETUP 
(CONFIGURACION DE DIAS DE INICIO DE 
PROGRAMA): Seleccione los d�as en que 
el programa se ejecutara
\page
PROGRAM WATER WINDOWS SETUP 
(CONFIGURACION DE PROGRAMA DE VENTANAS 
DE RIEGO): Establezca los tiempos en 
que el regado ser� o no ser� permitido.

CONCURRENT ZONES SETUP (CONFIGURACION 
DE ZONAS SIMULTANEAS): Establezca el 
numero de zonas que pueden ejecutarse 
a la vez.
\page
ADVANCED PROGRAMS SETUP (CONFIGURACION
AVANZADA DE PROGRAMAS): Establezca la 
prioridad del riego para los programas
\menu 1
\page
Configuraci�n de Tiempos de Inicio de 
Programa

   1. Verifique que el numero que 
      aparece en el campo Program 
      (Programa) coincida con el del 
      programa en el cual desea 
      establecer los tiempos de 
      inicio. Si desea seleccionar 
\page
      un programa diferente, presione 
      el bot�n + o - para cambiar el 
      numero de programa.

   2. Presione el bot�n Next para 
      moverse a la cuadricula de 
      tiempos de inicio.

   3. Presione el bot�n + o - para 
      establecer el tiempo en la 
      primera celda.
\page
   4. Presione el bot�n Next para 
      moverse a la siguiente celda 
      en la cuadricula de tiempos de 
      inicio, y despu�s presione el 
      bot�n + o - para establecer 
      tiempos adicionales como sean 
      necesarios.
\menu 2
\page
Configuraci�n de D�as de Inicio de 
Programa

   1. Verifique que el numero que 
      aparece en el campo Program 
      (Programa) coincida con el del 
      programa en el cual desea 
      establecer los d�as de inicio. 
      Si desea seleccionar un 
\page
      programa diferente, presione el 
      bot�n + o - para cambiar el 
      numero de programa.

   2. Presione el bot�n Next para 
      moverse al campo Schedule Type 
      (Tipo de Programaci�n) en el 
      cual puede seleccionar uno de 
      los siguiente intervalos:
\page
      DAYS OF THE WEEK (DIAS DE LA 
      SEMANA) - Despu�s de que haya 
      la opci�n D�as de la Semana, 
      presione el bot�n Next para 
      establecer los d�as en los 
      cuales desea que el programa 
      inicie. De manera predeterminada, 
      las casillas de los d�as de la 
      semana est�n marcados con un Y
      (SI), lo cual indica que el 
      programa iniciara todos los d�as.
\page
      Si desea cambiar un d�a de 
      inicio, presione el bot�n Next
      para seleccionar el d�a y 
      despu�s presione el bot�n + o -
      para cambiar el ajuste.

      INTERVAL DAYS (DIAS DE 
      INTERVALO): Esta opci�n le 
      permite configurar un intervalo
      personalizado para los d�as de 
      inicio de programa. Presione el 
\page
      bot�n Next para moverse al campo
      Watering Interval Days 
      (Intervalo de D�as de Riego) y 
      presione el bot�n + o - para 
      cambiar la configuraci�n. La 
      siguiente fecha de inicio esta 
      calculada e introducida en el 
      campo Next Start (Siguiente 
      Inicio). Si desea cambiar la 
      siguiente fecha de inicio, 
\page
      presione el bot�n Previous para
      resaltar el campo Next Start 
      (Siguiente Inicio), y despu�s 
      presione el bot�n - para cambiar
      la fecha.

      EVEN DAYS (DIAS PARES) - 
      Seleccione Even (PARES) para 
      iniciar el programa solo en los 
      d�as PARES del mes.
\page
      ODD DAYS (DIAS IMPARES) - 
      Seleccione (IMPARES) para 
      iniciar el programa solo en los
      d�as IMPARES del mes.

      ODD SKIP 31 (SALTA IMPAR 31) - 
      Seleccione SALTAR IMPAR 31 para
      iniciar el programa solo en los 
      d�as IMPARES del mes, pero 
      saltando el d�a 31 a fin de 
      mantener un calendario de cada 
\page
      dos d�as al cruzar a un nuevo 
      mes.

      HISTORICAL CALENDAR (CALENDARIO
      HISTORICO) - Configure d�as de
      intervalos personalizados para
      la primera y segunda mitad de 
      cada mes del a�o. Este programa 
      de riego funciona mejor en 
      regiones en las cuales los 
      terrenos son regados todo el 
      a�o.
\menu 3
\page
Configuraci�n de Programas de Ventanas
de Riego

   1. Presione el bot�n + o - para 
      seleccionar el programa para 
      configurar ventanas de riego.

   2. Presione el bot�n Next para 
      moverse al campo Water Window 
      Type (Tipo de Ventana de Riego),
      y despu�s presione el bot�n + 
\page
      o - para seleccionar Weekly 
      (Semanalmente) o Daily 
      (Diariamente).

   3. Presione el bot�n Next para 
      moverse al campo Day of the Week
      (D�a de la Semana). Si esta 
      usando el tipo de ventana de 
      riego Weekly (Semanalmente),
      no puede cambiar el ajuste en 
      este campo, pero si esta usando
\page
      Daily (Diariamente), presione 
      el bot�n + o - para mostrar el
      d�a el cual desea establecer
      ventanas de riego.

   4. Presione el bot�n Next para 
      moverse al campo ON/OFF 
      (ENCENDIDO/APAGADO).
\page
      Si las casillas en la cuadricula
      son color azul (permitir regado),
      el campo muestra OFF (APAGADO).
      Presione el bot�n Enter para 
      apagar todas las ventanas de 
      riego.

      Si las casillas en la cuadricula 
      son color blanco (no permitir 
      regado), el campo muestra ON
      (ENCENDIDO). Presione el bot�n 
\page
      Enter para prender todas las 
      ventanas de riego.

   5. Para hacer cambios en casillas 
      individuales, presione el bot�n 
      Next. El color del numero en la
      primera casilla cambia a rojo.

      Para cambiar la configuraci�n  
      de tiempo/d�a de regado 
      representado por esa casilla, 
      presione el bot�n +.
\page
      Para moverse a la siguiente 
      casilla en la cuadricula,
      presione el bot�n Next.

   6. Cuando haya terminado de hacer
      cambios, cambie el marcado a la
      posici�n RUN.
\menu 4
\page
Configuraci�n de Zonas Simultaneas

   1. En la columna Program (Programa),
      presione el bot�n + o - para 
      seleccionar el programa en el 
      cual desea configurar las zonas 
      simultaneas.

   2. Presione el bot�n Next para 
      moverse a la columna Concurrent 
      Zones (Zonas Simultaneas), y 
\page
      despu�s presione el bot�n + o 
      - para cambiar el valor en el 
      campo.

      Nota: Tome en cuenta 
      restricciones el�ctricas y de 
      riego cuando este configurando 
      zonas simultaneas.
\page
   3. Presione el bot�n Next para 
      moverse al campo Total Allowed 
      (Total Permitido), y despu�s 
      presione el bot�n + o - para 
      cambiar el valor en le campo.
      Este campo indica el numero 
      total de zonas simultaneas para 
      el controlador.

   4. Cuando haya terminado de hacer 
      cambios, cambie el marcador a 
      la posici�n RUN.
\menu 5
\page
Configuraci�n Avanzada de Programas

Cuando un evento de inicio es 
alcanzado, el programa con mayor  
prioridad recibe el agua primero.
El agua restante puede ser usada 
por programas con menor prioridad.
\page
Un programa de mayor prioridad 
"reemplaza" programas de menor 
prioridad y es ejecutado hasta estar 
completo antes que los de menor 
prioridad.

   1. En el campo Program (Programa),
      presione el bot�n + o - para 
      seleccionar el programa al cual
      desea configurar con la 
      prioridad.
\page
   2. Presione el bot�n Next para 
      moverse al campo Water Priority 
      (Prioridad de Riego), y despu�s
      presione el bot�n + para 
      seleccionar una de las 
      siguientes prioridades:

      High (Alta) - El programa que 
      desea que se ejecute primero
\page
      Medium (Media) - El programa que
      desea ejecutar despu�s de que 
      los programas de alta prioridad
      hayan terminado de ser 
      ejecutados.

      Low (Baja) - El programa que 
      desea ejecutar despu�s de que
      todos los dem�s programas se han
      ejecutado.
\page
   3. Cuando haya terminado de hacer 
      cambios, cambie el marcador a 
      la posici�n RUN.
\page
-----End of Help-----