﻿\header
Fecha y Tiempo
\footer
Presione NEXT/PREVIOUS para mas
ayuda, BACK para salir
\page
Nota: Cuando el controlador esta 
conectado al BaseManager, la fecha y
el tiempo son automáticamente 
ajustados.

   1. En el campo Time (Tiempo), el 
      lugar de las Horas esta 
      resaltado.

   2. Presione el botón + o - para 
      cambiar el valor.
\page
   3. Presione el botón Next para 
      moverse al lugar de los minutos,
      y después presione el botón + 
      o - para cambiar el valor.

   4. Presione el botón Next para 
      seguir moviéndose atravez de 
      los campos y después presione 
      + o - para cambiar el valor.
\page
   5. Presione el botón Enter para 
      guardar los ajustes.
\page
-----End of Help-----