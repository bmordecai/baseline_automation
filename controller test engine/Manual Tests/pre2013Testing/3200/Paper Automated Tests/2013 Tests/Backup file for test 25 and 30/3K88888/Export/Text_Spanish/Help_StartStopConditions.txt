﻿\header
Iniciar/Detener Condiciones
\footer
Presione NEXT/PREVIOUS para mas
ayuda, BACK para salir
\menu 0
\page
EVENT SWITCHES SETUP (CONFIGURACION 
DE INTERRUPTORES DE EVENTO): Encuentre
y configure cualquier interruptor de 
evento que haya instalado.

MOISTURE CONTROL SETUP (CONFIGURACION
DE CONTROL DE HUMEDAD): Encuentre y
configure cualquier sensor de humedad
del suelo que desee usar para iniciar,
detener o pausar condiciones
\page
TEMPERATURE CONTROL SETUP 
(CONFIGURACION DE CONTROL DE 
TEMPERATURA): Encuentre y configure 
cualquier sensor de temperatura que
haya instalado

EVENT DATES (FECHAS DE EVENTO): La 
BaseStation 3200 acepta ocho días de
evento. Un día de evento detiene todo 
el regado y previene que todos los 
programas inicien en esa fecha.
\menu 1
\page
Configuración de Interruptores de 
Evento

   1. All (Todo) esta resaltado en la 
      columna de Program (Programa).
      Realice una de las siguientes
      acciones:

      Para asociar el interruptor de
      evento con el controlador 
      completo, continúe con el 
      paso 2.
\page
      Para asociar el interruptor de 
      evento con un programa 
      especifico, presione el botón +
      para moverse al numero de ese
      programa. Continúe con el 
      paso 2.

   2. Presione Next para resaltar 
      Search (Búsqueda).
\page
   3. Presione el botón Enter para 
      buscar interruptores de eventos.
      El numero de serie de los 
      decoders son mostrados en la 
      columna de Event Decoders 
      (Decoders de Eventos).

   4. Presione el botón + o - para
      resaltar el numero de serie del
      interruptor de evento que desea
      asignar.
\page
   5. Presione el botón Next para
      moverse a la columna Enable
      (Activar) del campo de Start 
      (Inicio). Presione el botón +
      para cambiar los ajustes
      entre los siguientes valores:

      Off (Apagado) - Indica al 
      controlador ignorar esta 
      condición.
\page
      Equals (Igual) - Inicia el 
      programa cuando la condición 
      del interruptor coincide con 
      el ajuste en la columna del 
      Value (Valor).

   6. Presione el botón Next para
      moverse a la columna de Value 
      (Valor) del campo de Start 
      (Inicio). Presione el botón +
\page
      para cambiar el ajuste entre 
      Open (Abierto) y Closed 
      (Cerrado).

   7. Presione el botón Next para 
      moverse a la columna Enable 
      (Activar) del campo Stop 
      (Detener). Presione el botón
      + para cambiar el ajuste entre 
      Off (Apagado) y Equals (Igual).
\page
   8. Presione el botón Next para 
      moverse a la columna Value 
      (Valor) del campo Stop 
      (Detener). Presione el botón
      + para cambiar el ajuste entre 
      Open (Abierto) y Closed 
      (Cerrado).

   9. Presione el botón Next para 
      moverse a la columna Enable 
      (Activar) del campo Pause 
\page
      (Pausar). Presione el botón 
      + para cambiar el ajuste entre 
      Off (Apagado) y Equals (Igual).

  10. Presione el botón Next para 
      moverse a la columna Value
      (Valor) del campo Pause 
      (Pausar). Presione el botón
      + para cambiar el ajuste entre 
      Open (Abierto) y Closed 
      (Cerrado).
\page
  11. Presione el botón Next para 
      moverse a campo Pause Time 
      (Pausar Tiempo). Presione el 
      botón + o - para cambiar el 
      valor en el campo.

  12. Cuando haya terminado de hacer
      los cambios, cambie el marcador
      a la posición RUN.
\menu 2
\page
Configuración del Control de Humedad

   1. All (Todo) esta resaltado en la 
      columna de Program (Programa).
      Realice una de las siguientes 
      acciones:

      Para asociar el sensor de 
      humedad con el controlador
      completo, continúe con el 
      paso 2.
\page
      Para asociar el sensor de 
      humedad con un programa 
      especifico, presione el botón
      + para moverse al numero de ese
      programa. Continúe con el 
      paso 2.

   2. Presione Next para resaltar 
      Search (Búsqueda).
\page
   3. Presione el botón Enter para
      buscar sensores de humedad. El
      numero de serie de los sensores 
      son mostrados en la columna de 
      Moisture Sensors (Sensores de 
      Humedad).

   4. Presione el botón + o - para
      resaltar el numero de serie del 
      sensor de humedad que desea 
      asignar.
\page
   5. Presione el botón Next para
      moverse a la columna Enable 
      (Activar) del campo de Start 
      (Inicio). Presione el botón + 
      para cambiar los ajustes entre 
      los siguientes valores:

      Off (Apagado) - Indica al 
      controlador ignorar esta 
      condición.
\page
      Below (Por debajo) - Inicia el
      programa cuando el sensor de 
      humedad detecta una lectura 
      que esta por debajo del ajuste
      en la columna Value (Valor).

      Above (Por encima) - Inicia el
      programa cuando el sensor de 
      humedad detecta una lectura que
      esta por encima del ajuste en 
      la columna Value (Valor).
\page
      Equals (Igual) - Inicia el 
      programa cuando el sensor de
      humedad detecta una lectura 
      que es igual que el ajuste en 
      la columna Value (Valor).

   6. Presione el botón Next para
      moverse a la columna Value 
      (Valor) del campo Start 
      (Iniciar). Presione el botón
      + para cambiar el valor.
\page
   7. Presione el botón Next para 
      moverse a la columna Enable 
      (Activar) del campo Stop 
      (Detener). Presione el botón 
      + para cambiar el ajuste
      entre Off (Apagado), Below (Por
      debajo), Above (Por encima), e
      Equals (Igual).
\page
   8. Presione el botón Next para 
      moverse a la columna Value 
      (Valor) del campo Stop 
      (Detener). Presione el botón
      + para cambiar el valor.

   9. Presione el botón Next para 
      moverse a la columna Enable 
      (Activar) del campo Pause 
      (Pausar). Presione el botón
      + para cambiar el ajuste entre 
\page
      Off (Apagado), Below (Por
      debajo), Above (Por encima), e
      Equals (Igual).

  10. Presione el botón Next para 
      moverse a la columna Value 
      (Valor) del campo Pause 
      (Pausar). Presione el botón
      + para cambiar el valor.
\page
  11. Presione el botón Next para 
      moverse al campo Pause Time 
      (Pausar Tiempo). Presione el 
      botón + o - para cambiar el 
      valor.

  12. Cuando haya terminado de hacer 
      cambios, cambie el marcador a 
      la posición RUN.
\menu 3
\page
Configuración del Control de 
Temperatura

   1. Todo esta resaltado en la 
      columna de Programa. Realice una
      de las siguientes acciones:

      Para asociar el sensor de 
      temperatura con el controlador 
      completo, continúe con el 
      paso 2.
\page
      Para asociar el sensor de 
      temperatura con un programa 
      especifico, presione el botón 
      + para moverse al numero de ese
      programa. Continúe con el 
      paso 2.

   2. Presione Next para resaltar
      Search (Búsqueda).
\page
   3. Presione el botón Enter para 
      buscar sensores de temperatura. 
      El numero de serie de los 
      sensores son mostrados en la 
      columna de Temperature Sensors 
      (Sensores de Temperatura).

   4. Presione el botón + o - para
      resaltar el numero de serie del
      sensor de temperatura que desea 
      asignar.
\page
   5. Presione el botón Next para 
      moverse a la columna Enable 
      (Activar) del campo de Start 
      (Inicio). Presione el botón + 
      para cambiar los ajustes entre 
      los siguientes valores:

      Off (Apagado) - Indica al 
      controlador ignorar esta 
      condición.
\page
      Below (Por debajo) - Inicia el 
      programa cuando el sensor de 
      temperatura del aire detecta 
      una lectura que esta por debajo
      del ajuste en la columna Value 
      (Valor).

      Above (Por encima) - Inicia el
      programa cuando el sensor de 
      temperatura de aire detecta una
\page
      lectura que esta por encima del
      ajuste en la columna Value 
      (Valor).

      Equals (Igual) - Inicia el 
      programa cuando el sensor de
      temperatura detecta una lectura 
      que es igual que el ajuste en 
      la columna Value (Valor).
\page
   6. Presione el botón Next para 
      moverse a la columna Value 
      (Valor) del campo Start 
      (Iniciar). Presione el botón
      + para cambiar el valor.

   7. Presione el botón Next para 
      moverse a la columna Enable
      (Activar) del campo Stop 
      (Detener). Presione el botón 
      + para cambiar el ajuste 
\page
      entre Off (Apagado), Below 
      (Por debajo), Above (Por 
      encima), e Equals (Igual).

   8. Presione el botón Next para 
      moverse a la columna Value 
      (Valor) del campo Stop 
      (Detener). Presione el botón
      + para cambiar el valor.
\page
   9. Presione el botón Next para 
      moverse a la columna Enable 
      (Activar) del campo Pause 
      (Pausar). Presione el botón
      + para cambiar el ajuste entre 
      Off (Apagado), Below (Por
      debajo), Above (Por encima), e
      Equals (Igual).
\page
  10. Presione el botón Next para 
      moverse a la columna Value 
      (Valor) del campo Pause 
      (Pausar). Presione el botón
      + para cambiar el valor.

  11. Presione el botón Next para 
      moverse al campo Pause Time 
      (Pausar Tiempo). Presione el
      botón + o - para cambiar el 
      valor.
\page
  12. Cuando haya terminado de hacer 
      cambios, cambie el marcador a la
      posición RUN.
\menu 4
\page
Días de Evento

   1. En la columna Program 
      (Programa), presione el botón +
      para resaltar el programa en el
      cual desea establecer días de 
      evento.

   2. Presione el botón Next para 
      moverse a la columna Stop 
      Watering Date (Día para 
      Detener el Regado).
\page
   3. Presione el botón + para 
      establecer la fecha deseada.

   4. Si desea establecer otro día de
      evento, presione el botón Enter 
      para moverse a la siguiente 
      línea y después repita el 
      paso 3.
\page
   5. Si desea establecer otro día de
      evento para otro programa, 
      presione el botón Previous para
      regresar a la columna Program 
      (Programa) y después presione 
      el botón + para resaltar el 
      programa en el cual desea 
      establecer fechas de eventos.
\page
   6. Repita pasos 3 y 4 para 
      establecer la fecha.

   7. Cuando haya terminado de hacer 
      los cambios, cambie el marcador 
      a la posición RUN.
\page
-----End of Help-----