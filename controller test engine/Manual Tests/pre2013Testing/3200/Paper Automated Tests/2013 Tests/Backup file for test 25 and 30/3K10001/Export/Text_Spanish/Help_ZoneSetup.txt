﻿\header
Configuración de Zona
\footer
Presione NEXT/PREVIOUS para mas
ayuda, BACK para salir
\page
   1. En el campo Zone (Zona), 
      presione el botón + o - para
      cambiar el numero de la zona en
      la cual desea modificar los a
      justes.

   2. Presione el botón Next para 
      moverse al campo Zone Mode 
      (Modalidad de Zona). Presione el
      botón + o - para cambiar el 
\page
      valor en el campo a alguno de 
      los siguientes ajustes:

      Nota: Si cambia la modalidad 
      de una zona a Primaria o de 
      Primaria a otra modalidad, 
      potencialmente afectara la 
      programación de otras zonas.
\page
      Primary (Primaria) - La zona 
      dentro de un grupo de 
      programación a la que todas las 
      demás zonas están vinculadas.
      Cambiando la programación en 
      esta zona ajustara la 
      programación en todas las 
      zonas vinculadas, resultando
      en ahorro de tiempo y 
      consistencia. Si esta usando 
      un biSensor, la zona primaria 
\page
      es la zona a la cual el sensor 
      esta conectado.

      Linked (Vinculada) - Dentro de 
      un grupo de zona, hay una zona 
      primaria y todas las demás zonas
      están "vinculadas" a la primaria
      y obtendrán su información de 
      programación de la primaria. 
      (tiempo de riego, programación,
      calendario, etc.)
\page
      Timed (Temporizada) - Cualquier
      zona programada para que riegue 
      en un día/tiempo programado, en
      lugar de un programado 
      inteligente de regado.

   3. Presione el botón Next para 
      moverse al campo Water Time 
      (Tiempo de Regado).
\page
      Si desea cambiar el valor en 
      las horas, presione el botón 
      + o -.

      Si desea cambiar el valor en 
      los minutos, presione el botón 
      Next, y después presione el 
      botón + o -.

      Nota: Cuando cambiar en ajuste 
      de tiempo de regado en una zona 
      vinculada, note que un 
\page
      porcentaje aparece a la derecha
      del campo de Water Time (Tiempo 
      de Regado). Este porcentaje 
      compara el nuevo valor con el 
      valor en el campo de tiempo de
      regado de la zona primaria y 
      establece una relación de 
      seguimiento. La relación se 
      mantendrá cuando se haya 
      cambios en el tiempo de regado 
      de la zona primaria.
\page
   4. Presione el botón Next para 
      moverse al campo Cycle Time 
      (Tiempo de Ciclo). Si esta 
      usando ciclos de remojado para 
      esta zona, el tiempo de ciclo 
      define la duración de cada 
      ciclo de regado intercalados 
      con los tiempos de remojado.

      Nota: No puede cambiar el 
      ajuste de tiempo de ciclo de 
      una zona vinculada. 
\page
   5. Presione el botón Next para 
      moverse al campo de Soak Time 
      (Tiempo de Remojado). Si esta
      usando ciclos de remojado 
      para esta zona, el tiempo de 
      remojado define la duración de
      cada ciclo de remojado
      intercalados con los tiempos 
      de regado.
\page
      Nota: No puede cambiar el ajuste
      de tiempo de remojado de una 
      zona vinculada.

   6. Presione el botón Next para 
      moverse al campo de Flow (Flujo).
      Si sabe el flujo en galones por
      minuto de esta zona, puede 
      presionar el botón + o - para
      introducirlo en este campo.
      También puede realizar una 
\page
      operación de aprendizaje de 
      flujo para actualizar este 
      campo. Consulte el manual de 
      usuario para obtener mas
      información sobre el 
      aprendizaje de flujo.

   7. Presione el botón Next para 
      moverse al campo Enabled 
      (Activar). Presione el botón 
      + o - para cambiar el valor en
      el campo entre YES (SI) y NO.
\page
      Nota: Si desea evitar que una 
      zona sea ejecutada, puede 
      configurar el ajuste a NO en el
      campo Enabled (Activar). Cuando
      desee que la zona se ejecute 
      nuevamente, cambie el marcador
      a la posición de Zones (Zonas)
      y cambiar el ajuste a YES (SI).
      Este ajuste desactiva la zona 
      en todos los programas.
\page
   8. El numero en campo Program 
      (Programa) en la parte superior 
      izquierda de la pantalla indica
      para que programa son estos 
      ajustes. Una zona puede ser 
      removida de un programa, 
      asignada a un diferente 
      programa, o asignada a mas de
      un programa. Si desea hacer un
      cambio en como la zona es 
\page
      asignada a un programa, realice
      una de las siguientes acciones; 
      de lo contrario valla al paso 9
      de este procedimiento.

      Nota: Para moverse al campo 
      Program (Programa), presione el
      botón Next o Previous.

      Para remover la zona del 
      programa actual, presione el 
\page
      botón Previous para regresar 
      al campo Water Time (Tiempo de
      Riego) y después presione el 
      botón - para remover el tiempo
      de riego en el campo.

      Para asignar la zona a otro
      diferente programa, realice la
      acción anterior  para remover 
      la zona del programa actual. 
      Pase al campo Program (Programa),
\page
      y presione el botón + o - para 
      cambiar el numero del nuevo 
      programa al cual desea asignar 
      la zona.

      Para asignar la zona a un 
      programa adicional, pase al 
      campo Program (Programa) y 
      presione el botón + o - para 
      cambiar el numero del programa 
      adicional al cual desea asignar 
\page
      la zona. Repita los pasos 2 al 
      6 para configurar los ajustes de
      la zona en el nuevo programa.

      Nota: En la esquina inferior 
      izquierda de la pantalla se 
      muestra a que programa la zona
      esta asignada.

   9. Cuando haya terminado de hacer 
      los cambios, cambie el marcador 
      a la posición RUN.
\page
-----End of Help-----