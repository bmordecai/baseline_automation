﻿\header
Asignaciones de Zona
\footer
Presione NEXT/PREVIOUS para mas
ayuda, BACK para salir
\page
Use la posición de marcador (Assign) 
para buscar los dispositivos de 
dos-cables que tiene instalados y 
para asignar cada dispositivo a una 
"dirección" en el controlador.

BUSQUEDA DE DISPOSITIVOS

   1. Presione el botón + o - para 
      seleccionar Buscar en la 
      pantalla de Assign (Asignación).
\page
   2. Presione el botón Enter para 
      buscar por dispositivos. La 
      búsqueda puede tomar algunos 
      minutos para completarse.

      La pantalla muestra el número 
      de dispositivos encontrados y 
      el numero de puertos o 
      direcciones disponibles.
\page
      Nota: Para cancelar una búsqueda
      en progreso, presione el botón 
      Back.

ASIGNACIÓN DE DISPOSITIVOS

   1. Cuando la lista de dispositivos
      es mostrada, presione el botón 
      + o - para seleccionar el 
      numero de serie de cualquier 
      dispositivo sin asignar.
\page
   2. Presione el botón Previous para
      seleccionar una dirección 
      disponible.

   3. Presione el botón Enter para 
      asignar el dispositivo 
      seleccionado a esa dirección.

   4. Cuando haya terminado de hacer 
      los cambios, cambien el marcador
      a la posición RUN.
\page
DISPOSITIVOS SIN ASIGNAR

   1. Cuando la lista de dispositivos
      es mostrada, presione el botón 
      + o - para seleccionar sin 
      asignar.

   2. Presione el botón Next para 
      moverse a la columna 
      Zone Assignments (Asignaciones
      de Zona).
\page
   3. Presione el botón + o - para 
      seleccionar la zona que desea 
      borrar.

   4. Presione el botón Enter.
\page
-----End of Help-----