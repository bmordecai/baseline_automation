﻿\header
OFF
\footer
Presione NEXT/PREVIOUS para mas
ayuda, BACK para salir
\page
Cambie el marcador a la posición OFF 
(Apagado) cuando desee detener el 
riego por un periodo de tiempo 
indefinido. Ningún ciclo de riego se 
iniciara.

Utilice esta posición del marcador para
el cerrado temporal. No apague la 
BaseStation.
\page
Cuando esta en OFF (Apagado), el 
controlador aplica las siguientes 
condiciones:

   - Todos los ciclos de riego actual
     serán detenidos y todas las zonas
     serán establecidas en Terminado.

   - Apagara el dos-cables (doble 
     cableado) (incluso si ha sido 
     ajustado para siempre estar 
     prendido).
\page
-----End of Help-----