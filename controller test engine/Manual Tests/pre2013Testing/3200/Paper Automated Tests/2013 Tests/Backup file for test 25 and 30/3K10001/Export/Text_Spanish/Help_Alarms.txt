﻿\header
Alarmas/Mensajes
\footer
Presione NEXT/PREVIOUS para mas
ayuda, BACK para salir
\page
Los mensajes del operador son usados 
para indicar errores de programación 
y proporcionan ayuda para la solución 
de instalación y problemas de 
operación.

Los mensajes pueden incluir estatus o 
mensajes de advertencia, o serias 
alarmas del controlador. El mensaje 
\page
muestra el tiempo y la fecha de la 
alerta. El mensaje también incluye 
sugerencias para corregir el problema.

   1. Si hay varios mensajes, presione
      el botón + o - para desplazarse
      atravez de los mensajes.

   2. Para borrar el mensaje, presione
      el botón Next para seleccionar
      el campo Delete (Borrar), y 
      después presione El botón Enter.
\page
-----End of Help-----