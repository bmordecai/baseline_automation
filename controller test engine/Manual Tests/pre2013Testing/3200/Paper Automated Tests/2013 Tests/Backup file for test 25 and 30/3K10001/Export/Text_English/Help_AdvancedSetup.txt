﻿\header
Advanced Setup
\footer
Use NEXT/PREV for more help, BACK to exit  
\menu 0
\page
SELECT LANGUAGE FOR HELP DISPLAYS: Set
the the language for online help to 
English or Spanish

SECURITY SETUP: Set 3 levels of 
security for the controller
\page
DATA MANAGEMENT (USB STORAGE): Plug a
USB drive into the controller in order
to back up programming, restore 
programming, and export data

VALVE POWER SETUP: Adjust the valve 
power level of biCoders that are 
attached to the zones associated with 
solenoids that have different power
requirements
\page
TWO-WIRE SETTINGS: Indicate whether 
the power to the two-wire should 
always be on

UPDATE FIRMWARE (BASEMANAGER): If 
the controller is connected to 
BaseManager, use this option to
check for and install firmware
updates

SYSTEM SETTINGS: Change the serial
number of the controller and update
the software on the controller 
\menu 1
\page
Select Language for Help Displays

   1. In the Help Language field, 
      press the + button to toggle
      the setting between English and
      Spanish.
 
   2. If you make a change to the 
      language setting and you want
      to test the Online Help, press
\page
      the Next button to highlight 
      the Help field and then press 
      the Enter button. The Online 
      Help displays in the selected
      language. 

      To return to the Help Language
      screen, press the Back button. 
\menu 2
\page
Security Setup

You can set up the following levels of
security access:

   - Administrator: Grants access to 
     all controller functions

   - Programmer: Grants access to
     all controller functions except
     the Security function
\page
   - Operator: Grants access to the
     Run menu, the Manual Run menu, 
     and the Test menu

After you enable the security and set 
up the PINs, restart the controller. 
Users will be prompted to enter their 
PIN when they press one of the menu 
buttons on the controller. If the 
\page
user does not have access to the 
menu, he/she will see the message 
“Invalid PIN.” 

IMPORTANT NOTE! When a user accesses
the controller with a PIN, the 
controller will maintain that level 
of access until the dial is turned
to RUN. 
\page
   1. In the Enable Security Levels 
      field, press the + button to 
      toggle between Enabled and 
      Disabled. 

   2. Press the Next button to 
      highlight the first digit of 
      the Admin PIN, and then press
      the + or - button to change 
      the number.
\page
   3. Press the Next button to move to
      the next digit, and then press 
      the + or - button to change the
      number. Repeat this step until 
      you have configured all digits 
      of the PIN. 

   4. Press the Next button to 
      highlight the next PIN, and 
      then continue until you have
      configured all of the PINs.
\page
      Note: Be sure to remember which 
      PIN is associated with which 
      access level so you are certain 
      to give the correct PIN to the 
      correct users.

   5. When you have finished making 
      changes, return the dial to the
      RUN position. 
\menu 3
\page
Data Management (USB Storage)

   1. Plug a USB drive into the 
      controller.

   2. Press the Next button to 
      highlight the option that you
      want to perform, and then press
      the Enter button. 
\page
      IMPORTANT! Never erase your
      programming or files without 
      having a current backup 
      available for a restore.

      Refer to the User Manual for 
      more information about these 
      options.
\menu 4
\page
Valve Power Setup

Adjust the valve power level of 
biCoders that are attached to the 
zones associated with solenoids that 
have different power requirements.

   1. Press the + or - button to 
      select the zone and biCoder
      that you want to adjust.
\page
   2. Press the Next button to move
      to the Valve Drive Power field,
      and then press the + or - button
      to change the value.

      You can adjust the valve power 
      for any biCoder from a minimum 
      of 1 to a maximum of 3. If a 
      biCoder does not support this 
\page
      feature, the word “Fixed” 
      displays in the Valve Drive 
      Power field. 

   3. When you have finished making
      changes, turn the dial to the
      RUN position.
\menu 5
\page
Two-wire Settings

In its default condition, the 
controller powers down the two-wire 
after 1 minute of idle time. However, 
if you have a normally open master 
valve that is not connected to a 
separate power source or you have a 
coach’s button that can be used at 
any time, you should set the two-wire 
to be always on.
\page 
Additionally, if you are using a 
testing device to troubleshoot an 
issue with the two-wire, you should 
set the two-wire to be always on.

In the Two-wire Always On field, 
press the + or - button to toggle 
between YES and NO.
\menu 6
\page
Update Firmware (BaseManager)

If an update is available from 
BaseManager, the controller 
displays the information on 
the screen.

Note: If the controller is not
connected to the Internet, you
can update the firmware with
\page
a USB drive. Go to the Baseline
web site (www.baselinesystems.com)
for more information. 

The Update option is highlighted
in the lower-right corner of the
screen. Press the Enter button to
start the firmware update.
\page
The controller will be locked 
while the update is being applied.
When the update is complete, the 
controller restarts and briefly 
displays the new firmware version.
 
Note: If the firmware did not 
update as expected, please call
Baseline Support at 866.294.5847.
\menu 7
\page
System Settings

We recommend that you use the System
Setting options only under direction 
from Baseline Support. 

Refer to the User Manual for further
information or call Baseline Support
at 866-294-5847. 
\page
-----End of Help-----