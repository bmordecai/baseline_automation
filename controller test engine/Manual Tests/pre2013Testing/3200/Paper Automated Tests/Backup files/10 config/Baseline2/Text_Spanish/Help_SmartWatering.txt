﻿\header
Smart Watering Setup
\footer
Press NEXT/PREV for more help, BACK to exit  
\menu 0
\page
MOISTURE SENSOR ASSIGNMENTS: If you 
want to use soil moisture sensors
(biSensors) to monitor your zones, 
you need to assign a biSensor to a 
primary zone. If you have not 
configured any primary zones, you 
will not be able to complete the 
following procedure. Refer to Setting 
Up a Primary Zone in the User Manual.
\page
MOISTURE SENSOR SETUP: Configure your
soil moisture sensors for a watering 
strategy, set up limits, and 
calibration cycles

PROGRAM SEASONAL ADJUST: If your area
is experiencing unusually wet or dry
weather and you want to adjust an 
entire program either up or down, you
can adjust the watering by a 
percentage of normal.
\menu 1
\page
Moisture Sensor Assignments

   1. The Search option should be
      highlighted in the column on the
      left. If it is not highlighted, 
      press the + or - button to 
      highlight Search.

   2. Press the Enter button. The 
      system lists the serial numbers 
      of all biSensors that it finds.
\page
      Note: The BaseStation 3200 
      supports 25 biSensors.

   3. Press the + or - button to 
      highlight the serial number
      of the biSensor that you want
      to assign, and then press the
      Enter button. The serial number
      moves from the list on the left
      to zone address on the right.
\page
   4. When you have finished making 
      changes, turn the dial to the 
      RUN position.
\menu 2
\page
Moisture Sensor Setup

If you select the Upper or Lower 
Limit strategies, be sure to read 
the information the User Manual for 
a complete explanation of these 
strategies.

   1. If you have more than one 
      biSensor installed and assigned,
\page
      verify that the correct sensor 
      is selected. If the biSensor 
      that you want to configure is 
      not displayed in the field, 
      press the + or - button to 
      select the correct one.

   2. Press the Next button to move to
      the Water Strategy field, and 
      then press the + or - button to 
\page
      select the water strategy that 
      you want to use.

   3. Press the Next button to move to
      the Upper/Lower Limit field (the
      field that displays depends on 
      the water strategy that you 
      selected). Press the + or - 
      button to change the number in 
      the field. 
\page
   4. Press the Next button to move to
      the Limit Adjustment field. You
      can set the limit wetter or 
      drier to adjust the moisture to 
      your specific needs.

   5. Press the Next button to move to
      the Calibration Cycle field.

      Never - No calibration cycle 
      will be performed
\page
      One Time - A single calibration
      cycle will be performed

      Monthly - An additional 
      calibration cycle will be 
      performed during the month

      Note: Calibration cycles require 
      that soak cycles be enabled. 
      When you set these watering
      strategies, soak cycles will be
\page
      automatically enabled. You may 
      change the settings to meet your
      specific needs, but if you 
      disable the soak cycles, the 
      calibration cycle will fail.

   6. When you have finished making 
      changes, turn the dial to the 
      RUN position.
\menu 3
\page
Program Seasonal Adjust

If your area is experiencing unusually
wet or dry weather and you want to 
adjust an entire program either up or
down, you can adjust the watering by a 
percentage of normal.

   1. Press the + button to move to 
      the program where you want to 
      adjust the percentage. 
\page
   2. Press the Next button to move to
      the Budget % column.

   3. Press the + or - button to 
      change the percentage. You can 
      set the percentage between 10 
      and 500%.

   4. When you have finished making 
      changes, turn the dial to the 
      RUN position.
\page
-----End of Help-----