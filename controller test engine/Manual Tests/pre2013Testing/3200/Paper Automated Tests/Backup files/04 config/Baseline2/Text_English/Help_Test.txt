﻿\header
TEST
\footer
Press NEXT/PREV for more help, BACK to exit  
\menu 0
\page
TEST SINGLE TWO-WIRE DEVICE: Test a 
specific device

CHECK TWO-WIRE ADDRESSES: Test each 
two-wire device and fix any address
problems.

TEST ALL TWO-WIRE DEVICES: Test all 
two-wire devices on the controller.
View the full test results in the 
Reports dial position.
\page
TWO-WIRE BLINK TEST: Finds bad 
electrical connections, broken wires
or device failures. 

\menu 1
\page
Test Single Two-wire Device

   1. In the Device to Test column, 
      press the + or - button to 
      select the serial number of the
      device that you want to test.

   2. Press the Enter button to test 
      the device. 
\page
      The device will activate for 
      less than a second to measure 
      the voltage and current through
      the device. This test will 
      return information related to 
      the type of device that was 
      tested.

   3. When you have finished viewing 
      the test results, turn the dial
      to the RUN position. 
\menu 2
\page
Check Two-wire Addresses

Press the Enter button to start the 
test. While the test is in progress,
the statuses and results display
on the screen. 
\menu 3
\page
Test All Two-wire Devices

   1. Press the Enter button to start 
      the test.

   2. When the test is finished, turn 
      the dial to the Reports 
      position.

   3. Make sure that Test All displays
      in the Select Report field.
\page
   4. To scroll through the report, 
      press the Next button to 
      highlight the Scroll field, 
      and then press the + button. 
\menu 4
\page
Two-wire Blink Test

   1. In the Blink Interval field, 
      press the + or - button to 
      indicate the number of seconds 
      between blinks.

   2. Press the Enter button to start 
      the test.
\page
      While the blink test is in 
      progress, the Blink Interval 
      field displays “Running” and 
      the Status box displays the 
      results.

   3. To stop the test, press the 
      Back button. 
\page
-----End of Help-----