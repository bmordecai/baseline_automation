﻿\header
Date and Time
\footer
Press NEXT/PREV for more help, BACK to exit  
\page
Note: When the controller is connected
to BaseManager, the date and time are automatically set.

   1. In the Time field, the hours 
      place is highlighted.

   2. Press the + or - button to 
      change the value.
\page
   3. Press the Next button to move
      to the minutes place, and then
      press the + or - button to 
      change the value.

   4. Press Next to continue moving
      through the fields, and then
\page
      press the + or - button to 
      change the value.

   5. Press the Enter button to save
      the settings.
\page
-----End of Help-----