-------------------------
Zone 1: 00W0178
Status: OK
Valve Current: 0.23
Valve Voltage: 30.9
Two-wire Drop: 1.2
-------------------------
Zone 2: 00W0179
Status: OK
Valve Current: 0.23
Valve Voltage: 30.9
Two-wire Drop: 1.2
-------------------------
Zone 3: D600501
Status: OK
Valve Current: 0.21
Valve Voltage: 30.6
Two-wire Drop: 1.2
-------------------------
Zone 4: D600502
Status: OK
Valve Current: 0.21
Valve Voltage: 30.4
Two-wire Drop: 1.4
-------------------------
Zone 5: D600503
Status: OK
Valve Current: 0.21
Valve Voltage: 30.6
Two-wire Drop: 1.4
-------------------------
Zone 11: VA00001
Status: OK
Valve Current: 0.20
Valve Voltage: 24.0
Two-wire Drop: NA
-------------------------
Zone 12: VA00002
Status: OK
Valve Current: 0.20
Valve Voltage: 24.0
Two-wire Drop: NA
-------------------------
Zone 13: Q016381
Status: OK
Valve Current: 0.22
Valve Voltage: 29.2
Two-wire Drop: 1.2
-------------------------
Zone 14: Q016382
Status: OK
Valve Current: 0.22
Valve Voltage: 29.2
Two-wire Drop: 1.2
-------------------------
Zone 15: Q016383
Status: OK
Valve Current: 0.22
Valve Voltage: 29.2
Two-wire Drop: 1.2
