﻿\header
Zone Assignments
\footer
Press NEXT/PREV for more help, BACK to exit  
\page
Use this dial position to search for 
the two-wire devices that you have 
installed and to assign each device
to an "address" in the controller.

SEARCHING FOR DEVICES

   1. Press the + or - button to 
      select Search in the Assign 
      screen.
\page
   2. Press the Enter button to search
      for devices. The search may take
      several minutes to complete.

      The display shows the number of
      devices found and the number of
      ports or addresses available.

      Note: To cancel a search in 
      progress, press the Back button.
\page
ASSIGNING DEVICES

   1. When the list of devices is 
      displayed, press the + or - 
      button to select the serial 
      number of any unassigned device.

   2. Press the Next or Previous 
      button to select an available
      address.
\page
   3. Press the Enter button to assign 
      the selected device to that 
      address.

   4. When you have finished making
      changes, turn the dial to the 
      RUN position.
\page
UNASSIGNING DEVICES

   1. When the list of devices is 
      displayed, press the + or - 
      button to select Unassign.

   2. Press the Next button to move 
      to the Zone Assignments column.
\page
   3. Press the + or - button to 
      select the zone that you want 
      to clear.

   4. Press the Enter button.
\page
-----End of Help-----