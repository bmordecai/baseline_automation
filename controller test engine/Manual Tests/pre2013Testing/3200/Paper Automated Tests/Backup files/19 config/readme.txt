Components:
3200 with 5224 PCB and resistor test board attached.
RP00218 on 2-wire
RP00223 on 2-wire
D600501 on 2-wire
Q016381 on 2-wire

Config 1 starts after "set the time to 7:00 AM" on page 2, mid-page.
Config 2 starts after programming the second pause decoder on page 4, mid-page.