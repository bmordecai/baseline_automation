﻿\header
REPORTS
\footer
Press NEXT/PREV for more help, BACK to exit  
\page
Test All

Displays the results of the Test All 
Two-wire Devices in the Self Test dial
position.

   1. Make sure that Test All displays
      in the Select Report field.
\page
   2. To scroll through the report, 
      press the Next button to 
      highlight the Scroll field, 
      and then press the + button. 

Moisture Sensor Data

   1. In the Select Report field, 
      press the + button until 
      Moisture displays in the box. 
\page
   2. Perform any of the following 
      options:

      - To view the report for a 
        different sensor, press the
        Next button to highlight the 
        soil moisture sensor serial
        number, and then press the 
        + or - button.
\page 
      - To view the report for a 
        different date, press the 
        Next button to highlight 
        the Report Date field, and
        then press the + or - button. 

Program Run Time

   1. In the Select Report field, 
      press the + button until Run
\page
      Time displays in the box. The 
      report displays for Program 1.

   2. Perform any of the following 
      options:

      - To view the report for a 
        different program, press 
\page
        the Next button to highlight 
        the program number field, and 
        then press the + or - button 
        to change the program number.

      - To view the report for a 
        different date, press the 
        Next button to highlight the 
        Report Date field, and then 
        press the - button to change 
        the date.
\page
Water Used

   1. In the Select Report field, 
      press the + button until Water
      Used displays in the box. If you
      have a flow sensor installed, 
      the report displays for that 
      sensor.
\page
   2. Perform any of the following 
      options:

      - To view the report for a 
        different sensor, press the
        Next button to highlight the 
        sensor serial number field, 
        and then press the + or - 
        button to change the serial 
        number.
\page
      - To view the report for a 
        different date, press the 
        Next button to highlight the 
        Report Date field, and then 
        press the - button to change 
        the date.
\page
-----End of Help-----