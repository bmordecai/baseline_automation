===============================================
RUN THE .BAT FILES TO RUN THE MACRO
===============================================

===========
> Baseline FactoryReset.bat
===========
-Default macro, used in MFG.

===========
> Baseline FactoryReset_+Prompts.bat
===========
-Macro now asks if you want to reset factory defaults for each "section"
-Sections are: **WiFi settings, Networking settings, and Serial Port settings

===========
> Baseline WIFIKIT Reset.bat
===========
-Macro includes OPTION to restore WiFi Kit default settings.
-Can also do standard factory reset!

===========
> Baseline WIFIKIT Reset_+Prompts.bat
===========
-Macro includes OPTION to restore WiFi Kit default settings.
-Can also do standard factory reset!
-Macro also asks if you want to reset to defaults for each "section"
-Sections are: **WiFi settings, Networking settings, and Serial Port settings

===========
**Obviously only the WiFi unit will prompt for resetting WiFi settings.
===========